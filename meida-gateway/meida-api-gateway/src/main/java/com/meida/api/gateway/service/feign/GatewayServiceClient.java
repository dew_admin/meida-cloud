package com.meida.api.gateway.service.feign;

import com.meida.module.admin.client.api.GatewayRemoteApi;
import com.meida.common.base.constants.CommonConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * @author: zyf
 * @date: 2018/10/24 16:49
 * @description:
 */
@Component
@FeignClient(value = CommonConstants.BASE_SERVICE)
public interface GatewayServiceClient extends GatewayRemoteApi {


}
