package com.meida.api.gateway.fallback;

import com.meida.common.constants.ErrorCode;
import com.meida.common.mybatis.model.ResultBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * 响应超时熔断处理器
 * @author zyf
 */
@RestController
public class FallbackController {

    @RequestMapping("/fallback")
    public Mono<ResultBody> fallback() {
        return Mono.just(ResultBody.failed(ErrorCode.GATEWAY_TIMEOUT.getCode(), "访问超时，请稍后再试!"));
    }
}
