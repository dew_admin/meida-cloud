package com.meida.zuul.exception;


import com.meida.common.exception.OpenGlobalExceptionHandler;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.WebUtils;
import com.meida.zuul.service.AccessLogService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义签名错误处理器
 *
 * @author zyf
 */
@Slf4j
public class JsonSignatureDeniedHandler implements SignatureDeniedHandler {
    private AccessLogService accessLogService;

    public JsonSignatureDeniedHandler(AccessLogService accessLogService) {
        this.accessLogService = accessLogService;
    }
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       Exception exception) throws IOException, ServletException {
        ResultBody resultBody = OpenGlobalExceptionHandler.resolveException(exception,request.getRequestURI());
        response.setStatus(resultBody.getHttpStatus());
        // 保存日志
        accessLogService.sendLog(request, response,exception);
        WebUtils.writeJson(response, resultBody);
    }
}