package com.meida.zuul.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 *  网关属性配置类
 *
 * @author: zyf
 * @date: 2018/11/23 14:40
 * @description:
 */
@Data
@ConfigurationProperties(prefix = "meida.api")
public class ApiProperties {
    /**
     * 是否开启签名验证
     */
    private Boolean checkSign = true;
    /**
     * 是否开启动态访问控制
     */
    private Boolean accessControl = true;

    /**
     * 是否开启swagger调试
     */
    private Boolean apiDebug = false;

    /**
     * 始终放行
     */
    private Set<String> permitAll;

    /**
     * 无需鉴权的请求
     */
    private Set<String> authorityIgnores;

    /**
     * 签名忽略请求
     */
    private Set<String> signIgnores;



}
