package com.meida.zuul.service.feign;

import com.meida.module.admin.client.api.BaseApiRemoteApi;
import com.meida.common.base.constants.CommonConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@Component
@FeignClient(value = CommonConstants.BASE_SERVICE)
public interface BaseApiRemoteService extends BaseApiRemoteApi {


}