package com.meida.zuul.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.zuul.service.feign.BaseApiRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 接口文档 参数说明
 */
@RestController
public class SwaggerParamPlusController {

    @Autowired
    private BaseApiRemoteService baseApiService;
    /**
     *接口文档参数说明
     * @return
     */
    @PostMapping("/swagger/paramDetail")
    public ResultBody paramDetail(@RequestParam(value = "serviceId") String serviceId,
                                  @RequestParam(value = "path") String path) {
        return baseApiService.getApiInfo(serviceId,path);
    }
}
