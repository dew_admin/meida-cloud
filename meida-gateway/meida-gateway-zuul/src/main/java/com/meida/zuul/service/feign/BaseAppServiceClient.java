package com.meida.zuul.service.feign;

import com.meida.module.admin.client.api.IBaseAppServiceClient;
import com.meida.common.base.constants.CommonConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 * @description:
 */
@Component
@FeignClient(value = CommonConstants.BASE_SERVICE)
public interface BaseAppServiceClient extends IBaseAppServiceClient {


}
