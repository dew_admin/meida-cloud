package com.meida.zuul.filter;

import com.meida.common.base.interceptor.FeignRequestInterceptor;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

/**
 * 修改Zuul header
 * @author zyf
 */
public class ModifyHeaderFilter extends ZuulFilter {

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader(FeignRequestInterceptor.X_REQUEST_ID, ctx.getRequest().getHeader(FeignRequestInterceptor.X_REQUEST_ID)
        );
        return null;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }
}
