$(function(){
    layui.config({
        base: '/static/layui/module/'
    }).extend({
        treetable: 'treetable-lay/treetable'
    }).use(['treetable'], function () {
        var treetable = layui.treetable;

    });
    //openParamInfo("sdfdsf","/sdf/sdf/sdf");
});
var defaultTableData = {
    head: [{field: 'name', title: '参数名称'},
        {field: 'type', title: '参数类型'},
        {field: 'desc', title: '参数描述'},
        {field: 'example', title: '示例'},],
    data:[{
        title:"handlerName=com.aaa.sss",
        list: [{
            "name": "username",
            "type": "String",
            "desc": "姓名",
            "example": "张三",
            "p_name": "0"
        }, {
            "name": "account",
            "type": "JSON",
            "desc": "账户",
            "example": "",
            "p_name": "0"
        }, {
            "name": "account_name",
            "type": "String",
            "desc": "账户名",
            "example": "111",
            "p_name": "account"
        }, {
            "name": "account_card",
            "type": "String",
            "desc": "卡号",
            "example": "222",
            "p_name": "account"
        }, {
            "name": "account_bank",
            "type": "String",
            "desc": "开户行",
            "example": "平安银行",
            "p_name": "account"
        }, {
            "name": "pwd",
            "type": "String",
            "desc": "密码",
            "example": "123456",
            "p_name": "0"
        }]
    },{
        title:"handlerName=com.aaa.sss",
        list: [{
            "name": "username",
            "type": "String",
            "desc": "姓名",
            "example": "张三",
            "p_name": "0"
        }, {
            "name": "account",
            "type": "JSON",
            "desc": "账户",
            "example": "",
            "p_name": "0"
        }, {
            "name": "account_name",
            "type": "String",
            "desc": "账户名",
            "example": "111",
            "p_name": "account"
        }, {
            "name": "account_card",
            "type": "String",
            "desc": "卡号",
            "example": "222",
            "p_name": "account"
        }, {
            "name": "account_bank",
            "type": "String",
            "desc": "开户行",
            "example": "平安银行",
            "p_name": "account"
        }, {
            "name": "pwd",
            "type": "String",
            "desc": "密码",
            "example": "123456",
            "p_name": "0"
        }]
    }]};
//data 如defaultTableData
//如参数中有handler    属性则根据不同的handler，形成多套文档
function genPupHtml(requestData,responseData){
    console.log(requestData);
    console.log(responseData);
    var html = '<div><p style="line-height: 2em;background-color:#eee;font-weight: bold;margin-bottom: 0px;"><strong>&nbsp;&nbsp;&nbsp;请求参数说明</strong></p>';
    if(requestData.length>1){//多个handlerName
        //折叠面板
        /**<div class="layui-collapse">
            <div class="layui-colla-item">
            <h2 class="layui-colla-title">杜甫</h2>
            <div class="layui-colla-content layui-show">内容区域</div>
            </div>
        </div>**/
        html += '<div class="layui-collapse">\n' +
                    '<div class="layui-colla-item">';
        $.each(requestData,function(index,obj){
            html += '<h2 class="layui-colla-title">'+obj.title+'</h2>';
            html += '<div class="layui-colla-content layui-show"><table id="requestParam'+index+'"></table></div>';
        });
        html += "</div>";
        html += "</div>";
    }else{
        html+= '<table id="requestParam0"></table>';
    }
    html +=  '<p style="line-height: 2em;background-color:#eee;font-weight: bold;margin-bottom: 0px;"><strong>&nbsp;&nbsp;&nbsp;响应参数说明</strong></p>';
    if(responseData.length>1){
        html += '<div class="layui-collapse">\n' +
            '<div class="layui-colla-item">';
        $.each(responseData,function(index,obj){
            html += '<h2 class="layui-colla-title">'+obj.title+'</h2>';
            html += '<div class="layui-colla-content layui-show"><table id="responseParam'+index+'"></table></div>';
        });
        html += "</div>";
        html += "</div>";
    }else{
        html+= '<table id="responseParam0"></table>';
    }
    html += '</div>';
    return html;
}




function openParamInfo(serviceId,path){
    $.ajax({
        type: 'POST',
        url: "/swagger/paramDetail" ,
        data: {"serviceId":serviceId,"path":path},
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        dataType: "json",
        success: function(data){
            if(data.code==0){
                var msg = data.data;
                console.log("requestParamCfg="+msg.requestParamCfg);
                console.log("responseParamCfg="+msg.responseParamCfg);
                console.log("requestParam="+msg.requestParam);
                console.log("responseParam="+msg.responseParam);
                var requestParamCfg = eval("("+msg.requestParamCfg+")");//[{field: 'name', title: '参数名称'},{field: 'type', title: '参数类型'},{field: 'desc', title: '参数描述'},{field: 'example', title: '示例'},]
                var responseParamCfg = eval("("+msg.responseParamCfg+")");//[{field: 'name', title: '参数名称'},{field: 'type', title: '参数类型'},{field: 'desc', title: '参数描述'},{field: 'example', title: '示例'},]
                var requestParam = eval("("+msg.requestParam+")");//[{title:"handler=com.sdfds",list:[{"name": "account_card","type": "String","desc": "卡号","example": "222", "p_name": "account"}]}];
                var responseParam = eval("("+msg.responseParam+")");//[{title:"handler=com.sdfds",list:[{"name": "account_card","type": "String","desc": "卡号","example": "222", "p_name": "account"}]}];
                console.log(requestParam);
                console.log(responseParam);
                var html = genPupHtml(requestParam,responseParam);
                layer.open({
                    title:'参数说明',
                    type: 1,
                    area: ['1000px', '600px'],
                    content: html, //注意，如果str是object，那么需要字符拼接。
                    success: function(layero, index){
                        //初始化入参列表
                        $.each(requestParam,function (index,obj) {
                            loadParamTable("requestParam"+index,requestParamCfg,obj.list);
                        })
                        //初始化出参列表
                        $.each(responseParam,function (index,obj) {
                            loadParamTable("responseParam"+index,requestParamCfg,obj.list);
                        })
                    }
                });
            }else{
                layer.msg("获取参数说明失败，请确认后台已经配置了自定义参数");
            }
        } ,
        error:function(){
            var requestParamCfg = defaultTableData.head;//[{field: 'name', title: '参数名称'},{field: 'type', title: '参数类型'},{field: 'desc', title: '参数描述'},{field: 'example', title: '示例'},]
            var responseParamCfg = defaultTableData.head;//[{field: 'name', title: '参数名称'},{field: 'type', title: '参数类型'},{field: 'desc', title: '参数描述'},{field: 'example', title: '示例'},]
            var requestParam = defaultTableData.data;//[{title:"handler=com.sdfds",list:[{"name": "account_card","type": "String","desc": "卡号","example": "222", "p_name": "account"}]}];
            var responseParam = defaultTableData.data//[{title:"handler=com.sdfds",list:[{"name": "account_card","type": "String","desc": "卡号","example": "222", "p_name": "account"}]}];
            var html = genPupHtml(requestParam,responseParam);
            layer.open({
                title:'参数说明',
                type: 1,
                area: ['1000px', '600px'],
                content: html, //注意，如果str是object，那么需要字符拼接。
                success: function(layero, index){
                    //初始化入参列表
                    $.each(requestParam,function (index,obj) {
                        loadParamTable("requestParam"+index,requestParamCfg,obj.list);
                    })
                    //初始化出参列表
                    $.each(responseParam,function (index,obj) {
                        loadParamTable("responseParam"+index,requestParamCfg,obj.list);
                    })
                }
            });
            layer.msg("获取参数说明异常");
        }
    });


}



function loadParamTable(eleId,head,list){
    //eleId = "request"
    //head = [{field: 'name', title: '参数名称'},{field: 'type', title: '参数类型'},{field: 'desc', title: '参数描述'},{field: 'example', title: '示例'}];
    //list = [{"name": "account_card","type": "String","desc": "卡号","example": "222", "p_name": "account"}];
    //console.log("eleId="+eleId);
    layui.use(['treetable'], function () {
        var treetable = layui.treetable;
        // 渲染表格
        treetable.render({
            treeColIndex: 0,//第几列折叠
            treeSpid: '0',//
            treeIdName: 'name',
            treePidName: 'p_name',
            treeDefaultClose: true,
            treeLinkage: true,
            elem: '#'+eleId,
            //url: 'json/data1.json',
            data:list,
            cols:[head]
        });
    });
}

//页面加载完成后后 添加详细参数说明页面
