package com.meida.mq.configuration;

import com.meida.common.base.config.SpringCloudCondition;
import com.meida.mq.adapter.MqListenerAdapter;
import com.meida.mq.annotation.MsgListener;

import com.meida.mq.annotation.MsgListenerObj;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

/**
 * <b>功能名：MpListenerConfiguration</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-13 jiabing
 */

@Component
@AllArgsConstructor
@EnableConfigurationProperties(MqProviderProperties.class)
public class MpListenerConfiguration implements BeanPostProcessor {

    @Autowired
    private MqListenerAdapter mqListenerAdapter;

    @Autowired
    private MqProviderProperties mqProviderProperties;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> type = ClassUtils.getUserClass(bean);
        if(beanName.contains("resourceScanHandler")){
            System.out.println("resourceScanHandler");
        }
        ReflectionUtils.doWithMethods(type, method -> {
            AnnotationAttributes msgListenerAttrs = AnnotatedElementUtils.getMergedAnnotationAttributes(method, MsgListener.class);
            if (CollectionUtils.isEmpty(msgListenerAttrs)) {
                return;
            }else{
                mqListenerAdapter.initMqListener(type,beanName,
                        new MsgListenerObj(msgListenerAttrs.getString("id")
                                ,msgListenerAttrs.getStringArray("queues")
                                ,msgListenerAttrs.getString("topic")
                                ,bean
                                ,method)
                );
            }
        });

        return bean;
    }
}   