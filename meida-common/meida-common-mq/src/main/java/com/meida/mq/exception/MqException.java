package com.meida.mq.exception;

/**
 * <b>功能名：MqException</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-06 jiabing
 */
public class MqException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    /**
     * 缺省构造函数
     */
    public MqException() {
        super();
    }

    /**
     * 使用message构造一个新的Exception
     *
     * @param message 详细的例外信息
     */
    public MqException(String message) {
        super(message);
    }

    /**
     * 使用message和throwable构造例外
     *
     * @param message   详细的例外信息
     * @param throwable
     */
    public MqException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * 使用cause构造例外
     *
     * @param cause
     */
    public MqException(Throwable cause) {
        super(cause);
    }
}   