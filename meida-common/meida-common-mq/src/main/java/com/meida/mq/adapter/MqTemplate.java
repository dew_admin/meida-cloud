package com.meida.mq.adapter;

import com.meida.mq.exception.MqException;

/**
 * <b>功能名：Mq</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-06 jiabing
 */
public interface MqTemplate {

    void convertAndSend(Object var1) throws MqException;

    void convertAndSend(String var1, Object var2) throws MqException;

    /**
     * 延迟发送消息
     *
     * @param queueName  队列名称
     * @param params     消息内容params
     * @param expiration 延迟时间 单位毫秒
     */
    public void sendMessage(String queueName, Object params, Integer expiration);
    public void sendMessage(String queueName, Object params);

    public Object convertSendAndReceive(String queueName,Object param);

    default  public void publishEvent(Object object){

    };
}   