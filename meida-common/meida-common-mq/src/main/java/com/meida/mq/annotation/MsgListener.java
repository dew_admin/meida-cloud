package com.meida.mq.annotation;


import java.lang.annotation.*;
import java.util.Map;
import java.util.Queue;

/**
 * @author zyf
 */
@Documented
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MsgListener {

    String id() default "";

    String[] queues() default {};

    String topic() default "";


}
