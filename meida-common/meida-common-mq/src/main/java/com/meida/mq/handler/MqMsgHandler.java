package com.meida.mq.handler;

import com.meida.mq.annotation.MsgListenerObj;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <b>功能名：MqMsgHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-15 jiabing
 */
@Component
public class MqMsgHandler {

    private List<MsgListenerObj> listenerObjs = new CopyOnWriteArrayList<>();

    public void addMsgListenerObj(MsgListenerObj obj){
        listenerObjs.add(obj);
    }

    public void doMsgListener(MsgListenerObj obj, Object param, Map expandParam){
        try{
//            MsgListenerObj obj = null;
//            for(int i=0;i<listenerObjs.size();i++){
//                MsgListenerObj temp = listenerObjs.get(i);
//                if(Arrays.asList(temp.getQueues()).contains(queue)){
//                    obj = temp;
//                    break;
//                }
//            }
            if(obj!=null){
                //执行
                int paramCount = obj.getMethod().getParameterCount();
                Object[] params = new Object[paramCount];
                for(int i=0;i<paramCount;i++){
                    if(i==0){
                        params[i] = param;
                    }else if(obj.getMethod().getParameterTypes()[i].getSimpleName().contains("long")){
                        params[i] = 0L;
                    }else{
                        params[i] = null;
                    }
                }
                ReflectionUtils.invokeMethod(obj.getMethod(),obj.getBean(),params);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<MsgListenerObj> getListenerObjs() {
        return listenerObjs;
    }

    public void setListenerObjs(List<MsgListenerObj> listenerObjs) {
        this.listenerObjs = listenerObjs;
    }

}   