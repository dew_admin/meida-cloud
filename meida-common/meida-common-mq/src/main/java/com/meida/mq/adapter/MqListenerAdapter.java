package com.meida.mq.adapter;

import com.meida.mq.annotation.MsgListenerObj;

/**
 * <b>功能名：MqListenerAdapter</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-14 jiabing
 */
public interface MqListenerAdapter {
    void initMqListener(Class<?> clazz, String methodName, MsgListenerObj param);
}   