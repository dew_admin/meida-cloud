package com.meida.mq.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 默认消息队列
 *
 * @author:
 * @date: 2018/11/23 14:40
 * @description:
 */
//@SuppressWarnings("ConfigurationProperties")
@ConfigurationProperties(prefix = "meida.mq")
@Data
public class MqProviderProperties {
    /**
     * 默认消息队列实现
     */
    private String provider = "rabbitmq";


}
