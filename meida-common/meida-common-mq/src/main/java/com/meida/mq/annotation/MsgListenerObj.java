package com.meida.mq.annotation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

/**
 * <b>功能名：MsgListenerParm</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-14 jiabing
 */
@Data
@AllArgsConstructor
public class MsgListenerObj {
    private String id;

    private String[] queues;

    private String topic;

    private Object bean;

    private Method method;

}   