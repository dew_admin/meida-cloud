package com.meida.common.base.service;

public interface BaseShopService {

    /**
     * 删除企业店铺
     */
    Boolean deletByCompanyIds(Long[] companyIds);

}
