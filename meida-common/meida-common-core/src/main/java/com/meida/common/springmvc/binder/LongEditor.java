package com.meida.common.springmvc.binder;

import com.meida.common.base.utils.FlymeUtils;
import org.springframework.beans.propertyeditors.PropertiesEditor;

public class LongEditor extends PropertiesEditor {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text != null && text != "") {
            if (FlymeUtils.isNumber(text)) {
                setValue(Long.parseLong(text));
            } else {
                setValue(-1L);
            }
        }
    }

    @Override
    public String getAsText() {
        return getValue().toString();
    }
}  
