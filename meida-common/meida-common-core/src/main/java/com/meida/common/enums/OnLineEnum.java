package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 上架下架
 *
 * @author zyf
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OnLineEnum {
    NO(0, "未上架", "下架"),
    ON(1, "已上架", "上架"),
    OFF(2, "已下架", "下架"),
    VIO(3, "违规下架", "违规");

    OnLineEnum(Integer code, String name, String remark) {
        this.code = code;
        this.name = name;
        this.remark = remark;
    }


    @EnumValue
    private final Integer code;
    private final String name;
    private final String remark;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getRemark() {
        return remark;
    }

    public static String getValue(int value) {
        String v = "";
        for (OnLineEnum onLineEnum : OnLineEnum.values()) {
            if (onLineEnum.getCode() == value) {
                v = onLineEnum.getRemark();
                break;
            }
        }
        return v;
    }
}
