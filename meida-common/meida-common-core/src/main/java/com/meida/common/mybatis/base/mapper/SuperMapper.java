package com.meida.common.mybatis.base.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.meida.common.base.entity.EntityMap;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */
public interface SuperMapper<T> extends BaseMapper<T> {
    /**
     * 自定义分页查询
     *
     * @param wrapper
     * @param  page
     * @return
     */
    @Select("select ${ew.select} from ${ew.tableName} as ${ew.tableAlias} ${ew.joinSql} ${ew.customSqlSegment}")
    IPage<EntityMap> pageList(Page<EntityMap> page, @Param("ew") Wrapper<?> wrapper);

    /**
     * 自定义查询List<EntityMap>
     *
     * @param wrapper
     * @return
     */
    @Select("select ${ew.select} from ${ew.tableName} as ${ew.tableAlias} ${ew.joinSql} ${ew.customSqlSegment}")
    List<EntityMap> selectEntityMap(@Param("ew") Wrapper<?> wrapper);

    /**
     * 忽略逻辑删除
     *
     * @param wrapper
     * @return
     */
    @Delete("delete from ${ew.tableName} ${ew.customSqlSegment}")
    int deleteByCq(@Param(Constants.WRAPPER) Wrapper wrapper);


    /**
     * description: 批量插入
     * date: 2023年-08月-01日 15:39
     * author: ldd
     *
     * @param tableName
     * @param fieldKeyList
     * @param list
     * @return java.lang.Integer
     */
    @Select("<script>" +
            "INSERT INTO ${tableName}" +
            " <foreach collection=\"fieldKeyList\" item=\"field\" open=\"(\" close=\")\" separator=\",\">" +
            "   ${field}" +
            " </foreach>" +
            " values" +
            " <foreach collection=\"list\" item=\"obj\" open=\"\" close=\"\" separator=\",\">" +
            "      <foreach collection=\"obj.values\" item=\"value\" open=\"(\" close=\")\" separator=\",\">" +
            "           #{value}" +
            "      </foreach>" +
            " </foreach>" +
            "</script>")
    Integer mutiInsert(@Param("tableName") String tableName, @Param("fieldKeyList") List<String> fieldKeyList, @Param("list") List<Map> list);
}
