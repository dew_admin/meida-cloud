package com.meida.common.utils.event;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.event.FlymeEvent;
import com.meida.common.base.event.FlymeSyncEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 发布事件
 *
 * @author zyf
 */
@Component
public class FlymeEventClient {
    @Resource
    private ApplicationEventPublisher publisher;

    public void publishEvent(String handlerName, EntityMap params) {
        publisher.publishEvent(new FlymeEvent(handlerName, params));
    }

    public void publishSyncEvent(String handlerName, EntityMap params) {
        publisher.publishEvent(new FlymeSyncEvent(handlerName, params));
    }

    public void pushMinioSyncUserEvent(EntityMap map) {
        publishEvent("minioAddUserListener", map);
    }
}
