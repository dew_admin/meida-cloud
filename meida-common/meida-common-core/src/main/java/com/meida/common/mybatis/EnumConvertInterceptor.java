package com.meida.common.mybatis;

import com.meida.common.base.entity.EntityMap;

public interface EnumConvertInterceptor {
    boolean convert(EntityMap map, String key, Object v);
}
