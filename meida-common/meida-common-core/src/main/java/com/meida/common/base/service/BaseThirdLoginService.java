package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;

/**
 * 后台账户全局接口
 *
 * @author zyf
 */
public interface BaseThirdLoginService {


    /**
     * 注册账号
     *
     * @param username
     * @param avatar
     * @param accontType
     * @param nickName
     * @return
     */
    Object register(String username, String avatar, String accontType, String nickName);


}
