package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.module.EmailInfo;
import com.meida.common.mybatis.model.ResultBody;

/**
 * @author zyf
 */
public interface SendEmailCodeHandler {

    /**
     * 设置邮件对象
     *
     * @return
     */
    default EmailInfo initEmailInfo(EmailInfo emailInfo) {
        return emailInfo;
    }


    /**
     * 发送邮件验证码
     *
     * @param email
     * @return
     */
    default void sendSuccess(String email) {
    }


    /**
     * 校验邮件验证码
     *
     * @param email
     * @param params
     * @return
     */
    default ResultBody validSuccess(String email, EntityMap params) {
        return ResultBody.ok();
    }


}
