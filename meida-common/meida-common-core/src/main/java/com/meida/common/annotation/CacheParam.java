package com.meida.common.annotation;

import java.lang.annotation.*;

/**
 * @author flyme
 * @date 2019/10/10 14:30
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheParam {
    /**
     * 字段名称
     *
     * @return String
     */
    String name() default "";
}
