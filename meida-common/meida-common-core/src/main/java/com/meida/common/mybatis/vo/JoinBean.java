package com.meida.common.mybatis.vo;

import cn.hutool.db.sql.SqlBuilder;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.ReflectionUtils;
import org.springframework.core.annotation.AnnotationUtils;

import java.io.Serializable;

/**
 * 连接查询对象
 */
public class JoinBean implements Serializable {
    /**
     * 连接表
     */
    private String joinTableName;
    /**
     * 连接类型(默认左连接)
     */
    private SqlBuilder.Join joinType = SqlBuilder.Join.LEFT;
    /**
     * 连接表字段
     */
    private String joinField;
    /**
     * 主表连接字段
     */
    private String mainField;
    /**
     * 左表连接别名
     */
    private String joinAlias;
    /**
     * 主表连接别名
     */
    private String mainAlias;

    private Class mainCls;

    public JoinBean(Class mainCls, Class joinCls) {
        TableName joinTable = AnnotationUtils.getAnnotation(joinCls, TableName.class);
        TableAlias mainTableAlias = AnnotationUtils.getAnnotation(mainCls, TableAlias.class);
        TableAlias joinTableAlias = AnnotationUtils.getAnnotation(joinCls, TableAlias.class);
        this.mainCls = mainCls;
        this.mainAlias = mainTableAlias.value();
        this.joinTableName = joinTable.value();
        this.joinAlias = joinTableAlias.value();
        this.joinField = ReflectionUtils.getTableField(joinCls);
        if (FlymeUtils.isNotEmpty(this.joinField)) {
            this.mainField = this.joinField;
        } else {
            this.mainField = ReflectionUtils.getTableField(mainCls);
        }
        this.joinType = SqlBuilder.Join.LEFT;
    }

    public JoinBean(Class mainCls, Class joinCls, String joinField) {
        TableName joinTable = AnnotationUtils.getAnnotation(joinCls, TableName.class);
        TableAlias mainTableAlias = AnnotationUtils.getAnnotation(mainCls, TableAlias.class);
        TableAlias joinTableAlias = AnnotationUtils.getAnnotation(joinCls, TableAlias.class);
        this.mainAlias = mainTableAlias.value();
        this.joinTableName = joinTable.value();
        this.joinAlias = joinTableAlias.value();
        this.joinField = joinField;
        this.mainField = this.joinField;
    }

    public JoinBean(Class mainCls, Class joinCls, String joinAlias, String joinField) {
        TableName joinTable = AnnotationUtils.getAnnotation(joinCls, TableName.class);
        TableAlias mainTableAlias = AnnotationUtils.getAnnotation(mainCls, TableAlias.class);
        this.mainAlias = mainTableAlias.value();
        this.joinTableName = joinTable.value();
        this.joinAlias = joinAlias;
        this.joinField = joinField;
        this.mainField = this.joinField;
    }

    public String getJoinTableName() {
        return joinTableName;
    }

    public JoinBean setJoinTableName(String joinTableName) {
        this.joinTableName = joinTableName;
        return this;
    }

    public SqlBuilder.Join getJoinType() {
        return joinType;
    }

    public JoinBean setJoinType(SqlBuilder.Join joinType) {
        this.joinType = joinType;
        return this;
    }

    public String getJoinField() {
        return joinField;
    }

    public JoinBean setJoinField(String joinField) {
        this.joinField = joinField;
        return this;
    }

    public String getMainField() {
        return mainField;
    }

    public JoinBean setMainField(String mainField) {
        this.mainField = mainField;
        return this;
    }

    public String getJoinAlias() {
        return joinAlias;
    }

    public JoinBean setJoinAlias(String joinAlias) {
        this.joinAlias = joinAlias;
        return this;
    }

    public String getMainAlias() {
        return mainAlias;
    }

    public JoinBean setMainAlias(String mainAlias) {
        this.mainAlias = mainAlias;
        return this;
    }

    public JoinBean right() {
        this.joinType = SqlBuilder.Join.RIGHT;
        this.mainField = ReflectionUtils.getTableField(mainCls);
        this.joinField = this.mainField;
        return this;
    }

    public JoinBean inner() {
        this.joinType = SqlBuilder.Join.INNER;
        return this;
    }

    public JoinBean full() {
        this.joinType = SqlBuilder.Join.FULL;
        return this;
    }


}
