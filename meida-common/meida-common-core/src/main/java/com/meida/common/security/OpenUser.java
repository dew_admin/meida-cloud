package com.meida.common.security;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * 自定义认证用户信息
 *
 * @author zyf
 */
@Data
public class OpenUser implements UserDetails {
    private static final long serialVersionUID = -123308657146774881L;

    /**
     * 账户Id
     */
    private Long accountId;

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 登录名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 用户权限
     */
    private Collection<? extends GrantedAuthority> authorities;
    /**
     * 是否已锁定
     */
    private boolean accountNonLocked;
    /**
     * 是否已过期
     */
    private boolean accountNonExpired;
    /**
     * 是否启用
     */
    private boolean enabled;
    /**
     * 密码是否已过期
     */
    private boolean credentialsNonExpired;
    /**
     * 认证客户端ID
     */
    private String clientId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;


    /***
     * 账户类型
     */
    private String accountType;

    /***
     * 用户类型
     */
    private String userType;

    /***
     * 企业ID
     */
    private Long companyId;

    /**
     * 机构ID
     */
    private Long organizationId;

    /**
     * 认证中心域,适用于区分多用户源,多认证中心域
     */
    private String domain;

    /**
     * 用户附加属性
     */
    private Map<String, Object> attrs;

    /**
     * 超级管理员
     */
    private Integer superAdmin;

    /**
     * 只是客户端模式.不包含用户信息
     *
     * @return
     */
    public Boolean isClientOnly() {
        return clientId != null && userId == null;
    }

    public OpenUser() {
    }

    public OpenUser(String clientId,String domain, Long accountId, Long organizationId,Long companyId,Long userId, String username, String password, Collection<OpenAuthority> authorities, boolean accountNonLocked, boolean accountNonExpired, boolean enabled, boolean credentialsNonExpired, String nickName, String avatar,String accountType,Integer superAdmin,String userType) {
        this.domain = domain;
        this.clientId=clientId;
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.accountNonLocked = accountNonLocked;
        this.accountNonExpired = accountNonExpired;
        this.enabled = enabled;
        this.credentialsNonExpired = credentialsNonExpired;
        this.avatar = avatar;
        this.nickName = nickName;
        this.accountId = accountId;
        this.superAdmin=superAdmin;
        this.companyId = companyId;
        this.accountType = accountType;
        this.organizationId=organizationId;
        this.userType=userType;
    }



    public OpenUser(String clientId,String domain, Long accountId, Long companyId, Long userId, String username, String password, boolean accountNonLocked, boolean accountNonExpired, boolean enabled, boolean credentialsNonExpired, String nickName, String avatar, String accountType) {
        this.domain = domain;
        this.userId = userId;
        this.clientId=clientId;

        this.username = username;
        this.accountId = accountId;
        this.companyId = companyId;
        this.password = password;
        this.accountNonLocked = accountNonLocked;
        this.accountNonExpired = accountNonExpired;
        this.enabled = enabled;
        this.credentialsNonExpired = credentialsNonExpired;
        this.avatar = avatar;
        this.nickName = nickName;
        this.accountType = accountType;

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (authorities == null) {
            return Collections.EMPTY_LIST;
        }
        return this.authorities;
    }

    @JsonIgnore
    @JSONField(serialize = false)
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public Map<String, Object> getAttrs() {
        if (attrs == null) {
            return Maps.newHashMap();
        }
        return attrs;
    }

    public void setAttrs(Map<String, Object> attrs) {
        this.attrs = attrs;
    }


}
