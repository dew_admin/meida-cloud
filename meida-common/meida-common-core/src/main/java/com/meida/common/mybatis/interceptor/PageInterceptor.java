package com.meida.common.mybatis.interceptor;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;

import java.util.List;

/**
 * 扩展分页处理器接口拦截器
 *
 * @author Administrator
 */
public interface PageInterceptor<T> {

    /**
     * 条件验证
     *
     * @return
     */
    default ResultBody validate(CriteriaQuery<T> cq, EntityMap params) {
        return ResultBody.ok();
    }

    /**
     * 条件预处理
     *
     * @return
     */
    default void prepare(CriteriaQuery<T> cq, PageParams pageParams, EntityMap params) {

    }


    /**
     * 扩展返回结果
     *
     * @param result 分页结果集
     * @param extra  扩展数据存储
     * @return
     */
    default void complete(CriteriaQuery<T> cq, List<EntityMap> result, EntityMap extra) {

    }


}
