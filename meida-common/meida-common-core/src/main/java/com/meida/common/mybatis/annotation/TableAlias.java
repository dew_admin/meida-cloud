package com.meida.common.mybatis.annotation;

import java.lang.annotation.*;

/**
 * table别名
 * @author zyf
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})

public @interface TableAlias {
    String value();

}