package com.meida.common.constants;

/**
 * @author: zyf
 * @date: 2019/2/21 17:46
 * @description:
 */
public class QueueConstants {
    public static final String QUEUE_SCAN_API_RESOURCE = "meida.scan.api.resource";
    public static final String QUEUE_ACCESS_LOGS = "meida.access.logs";
    public static final String QUEUE_BIND_MOBILE = "flyme_bind_mobile";

    /**
     * app用户注册通知
     */
    public static final String QUEUE_USER_REG = "flyme_user_reg";
    /**
     * 订单检查通知
     */
    public static final String QUEUE_ORDER_CHECK = "flyme_order_check";

    /**
     * 店铺注册通知
     */
    public static final String QUEUE_SHOP_REG = "flyme_shop_reg";
    /**
     * 账户绑定企业ID通知
     */
    public static final String QUEUE_BIND_COMPANY = "flyme_bind_company";
    /**
     * 企业审核成功通知
     */
    public static final String QUEUE_COMPANY_AUTH = "flyme_company_auth";
    public static final String QUEUE_UNBIND_COMPANY = "flyme_unbind_company";
    /**
     * 收藏点击
     */
    public static final String QUEUE_COLLECON = "flyme_collecon";
    /**
     * 删除文件
     */
    public static final String QUEUE_UPLOADFILE = "flyme_uploadfile";
    /**
     * 删除文件队列
     */
    public static final String QUEUE_DELFILE = "flyme_delfile";
    /**
     * 上传文件队列
     */
    public static final String QUEUE_UPLOADOSS = "flyme_uploadoss";
    /**
     * 文件转换成PDF
     */
    public static final String QUEUE_UPLOAD_CONVERT = "flyme-upload-convert";

    /**
     * 文字识别
     */
    public static final String QUEUE_OCR = "flyme_ocr";

    /**
     * WebSocket
     */
    public static final String QUEUE_WEBSOCKET_MSG = "flyme_websocket_msg";


    /**
     * D档案到期提醒
     */
    public static final String QUEUE_REMIND = "flyme-remind";

}
