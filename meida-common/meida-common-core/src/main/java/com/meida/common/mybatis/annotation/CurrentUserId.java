package com.meida.common.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 当前用户id
 *
 * @author flyme
 * @date 2019/12/6 17:03
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface CurrentUserId {
    String value() default "userId";
}
