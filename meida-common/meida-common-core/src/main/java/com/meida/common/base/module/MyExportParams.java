package com.meida.common.base.module;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MyExportParams extends ExportParams {
    /**
     * 导出文件名
     */
    private String fileName;

    public MyExportParams(String fileName, String title, String sheetName, ExcelType type) {
        super(title, sheetName, type);
        this.fileName = fileName;
    }
}
