package com.meida.common.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 支付状态
 * @author zyf
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@JSONType(serializeEnumAsJavaBean = true)
public enum PayStatusEnum {
    /**
     * 未支付
     */
    NOPAY(0, "未支付"),
    /**
     * 已支付
     */
    PAY(1, "已支付"),
    /**
     * 已退款
     */
    refund(2, "已退款");
    public String text;
    @EnumValue
    public Integer code;

    PayStatusEnum(Integer code, String text) {
        this.text = text;
        this.code = code;
    }

    PayStatusEnum(Integer code) {
        this.text = getText(code);
        this.code = code;
    }

    public static String getText(Object code) {
        String v = "";
        for (PayStatusEnum accountTypeEnum : values()) {
            if (accountTypeEnum.code.equals(code)) {
                v = accountTypeEnum.text;
                break;
            }
        }
        return v;
    }

    public static Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>();
        for (PayStatusEnum compNatureEnum : values()) {
            map.put(compNatureEnum.text, compNatureEnum.code);
        }
        return map;
    }

    public static PayStatusEnum getEnum(String code) {
        for (PayStatusEnum payStatusEnum : values()) {
            if (payStatusEnum.code.equals(code)) {
                return payStatusEnum;
            }
        }
        return null;
    }


}
