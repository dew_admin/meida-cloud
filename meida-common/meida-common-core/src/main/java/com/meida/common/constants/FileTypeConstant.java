package com.meida.common.constants;

/**
 * 文件类型
 *
 * @author zyf
 */
public interface FileTypeConstant {
    /**
     * 图片
     */
    Integer FILETYPE_IMAGE = 1;

    /**
     * 视频
     */
    Integer FILETYPE_VIDEO = 2;

    /**
     * 文件
     */
    Integer FILETYPE_FILE = 3;

    String RedisKey = "UPLOAD:FILETYPE";
}
