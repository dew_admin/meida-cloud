package com.meida.common.mybatis.interceptor;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaDelete;
import org.springframework.transaction.annotation.Transactional;

/**
 * 删除扩展拦截器
 */
public interface DeleteInterceptor {

    /**
     * 条件验证
     *
     * @return
     */
    default ResultBody validate(CriteriaDelete cd, EntityMap params) {
        return ResultBody.ok();
    }


    /**
     * 删除前扩展
     *
     * @return
     */
    void prepare(CriteriaDelete cq, EntityMap params);


    /**
     * 删除后扩展
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    default void complete(CriteriaDelete cq, EntityMap params) {

    }


}
