package com.meida.common.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.Date;

/**
 * @author zyf
 */
@Slf4j
public class OpenRedisTokenService implements ResourceServerTokenServices {

	private TokenStore tokenStore;

	@Override
	public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException{
		OAuth2Authentication oAuth2Authentication = tokenStore.readAuthentication(accessToken);
		return oAuth2Authentication;
	}

	@Override
	public OAuth2AccessToken readAccessToken(String accessToken) {

		OAuth2AccessToken token = tokenStore.readAccessToken(accessToken);
		OAuth2Authentication oAuth2Authentication = tokenStore.readAuthentication(accessToken);

		int expiresIn = tokenStore.getAccessToken(oAuth2Authentication).getExpiresIn();
		int second = 60 * 60 * 30;
		if (expiresIn < second) {
			DefaultOAuth2AccessToken oAuth2AccessToken = (DefaultOAuth2AccessToken) token;
			oAuth2AccessToken.setExpiration(new Date(System.currentTimeMillis() + (second * 1000L)));
			tokenStore.storeAccessToken(oAuth2AccessToken, oAuth2Authentication);

		}
		return token;
	}

	public TokenStore getTokenStore() {
		return tokenStore;
	}

	public void setTokenStore(TokenStore tokenStore) {
		this.tokenStore = tokenStore;
	}
}
