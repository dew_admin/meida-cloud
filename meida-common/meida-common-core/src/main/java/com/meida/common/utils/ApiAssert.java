package com.meida.common.utils;

import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.exception.OpenAlertException;
import com.meida.common.mybatis.model.ResultBody;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

/**
 * API 断言
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiAssert {

    /**
     * obj1 eq obj2
     *
     * @param obj1
     * @param obj2
     * @param j
     */
    public static void eq(String info, Object obj1, Object obj2) {
        if (!Objects.equals(obj1, obj2)) {
            failure(info);
        }
    }

    public static void isTrue(String info, boolean condition) {
        if (!condition) {
            failure(info);
        }
    }

    public static void isFalse(String info, boolean condition) {
        if (condition) {
            failure(info);
        }
    }


    public static void gtzero(String info, int n) {
        if (n <= 0) {
            failure(info);
        }
    }

    public static void lezero(String info, int n) {
        if (n > 0) {
            failure(info);
        }
    }

    public static void gtzero(String info, BigDecimal n) {
        if (FlymeUtils.le(n, new BigDecimal(0))) {
            failure(info);
        }
    }

    public static void gezero(String info, BigDecimal n) {
        if (FlymeUtils.le(n, new BigDecimal(0))) {
            failure(info);
        }
    }

    public static void isEmpty(String info, Object obj) {
        if (ObjectUtils.isNotEmpty(obj)) {
            failure(info);
        }
    }

    public static void isNotEmpty(String info, Object obj) {
        if (ObjectUtils.isEmpty(obj)) {
            failure(info);
        }
    }

    public static void anyOneIsNull(String info, Object... conditions) {
        if (FlymeUtils.allNotNull(conditions)) {
            failure(info);
        }
    }

    public static void anyOneIsNotNull(String info, Object... conditions) {
        if (FlymeUtils.allIsNull(conditions)) {
            failure(info);
        }
    }


    public static void allNotNull(String info, Object... conditions) {
        if (FlymeUtils.anyOneIsNull(conditions)) {
            failure(info);
        }
    }

    /**
     * <p>
     * 失败结果
     * </p>
     *
     * @param j 异常错误码
     */
    private static void failure(ResultBody j) {
        throw new OpenAlertException(1001, j.getMessage());
    }

    /**
     * <p>
     * 失败结果
     * </p>
     */
    public static void failure(String info) {
        throw new OpenAlertException(1001, info);
    }

    public static void notEmpty(ResultBody j, Object[] array) {
        if (ArrayUtils.isEmpty(array)) {
            failure(j);
        }
    }

    /**
     *
     */
    public static void noNullElements(ResultBody j, Object[] array) {
        if (array != null) {
            for (Object element : array) {
                if (element == null) {
                    failure(j);
                }
            }
        }
    }

    public static void notEmpty(ResultBody j, Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            failure(j);
        }
    }


}
