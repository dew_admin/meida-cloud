package com.meida.common.mybatis.interceptor;

import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.module.ExportField;
import com.meida.common.base.module.MyExportParams;
import com.meida.common.mybatis.model.ResultBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导出拦截器
 *
 * @author Administrator
 */
public interface ExportInterceptor<T> {

    /**
     * 导出参数配置
     *
     * @return
     */
    default void initExportParams(MyExportParams exportParams) {
    }

    /**
     * 导出字段列表
     *
     * @return
     */
    default void initExportFieldList(List<ExportField> exportFieldList) {
    }

    /**
     * 导出字段配置
     *
     * @return
     */
    default void initExcelExportEntity(ExportField exportField, List<ExcelExportEntity> entityList) {

    }


    /**
     * 数据源设置
     *
     * @return
     */
    default ResultBody initData(Map params) {
        return ResultBody.ok();
    }

    /**
     * 模板路径
     *
     * @return
     */
    default TemplateExportParams initTemplateExportParams() {
        TemplateExportParams templateExportParams = new TemplateExportParams();
        return templateExportParams;
    }

    /**
     * 模板数据
     *
     * @return
     */
    default Map initTemplateDataMap() {
        HashMap<Object, Object> dataMap = new HashMap<>();
        return dataMap;
    }

    /**
     * 对导出结果处理
     * @param dataMap
     * @param list
     */
    default void initExportData(Map dataMap, List<EntityMap> list) {

    }

    /**
     * 文件放到服务器的路径
     * @return
     * @param params
     */
    default String serverPath(Map params) {
        return "";
    }

    /**
     * 导出成功
     * @param params
     * @param fileKey
     * @param exportName
     * @return
     */
    default Boolean exportSuccess(Map params,String exportName,String fileKey) {
        return true;
    }
}
