package com.meida.common.utils;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author zyf
 */
public class GetPayParams {

    public EntityMap params;

    public GetPayParams(Map<String, Object> params) {
        Object attach = params.get("attach");
        if (FlymeUtils.isNotEmpty(attach)) {
            this.params = JsonUtils.jsonToEntityMap(attach.toString());
        } else {
            this.params = new EntityMap();
        }
    }

    public <T> T get(String key) {
        return params.get(key);
    }

    public BigDecimal getBigDecimal(String key) {
        return params.getBigDecimal(key);
    }

    public Integer getInt(String key, Integer def) {
        return params.getInt(key, def);
    }

    public Long getLong(String key) {
        return params.getLong(key);
    }
}
