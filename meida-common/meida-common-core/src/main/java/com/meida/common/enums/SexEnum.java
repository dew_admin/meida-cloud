package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 性别枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SexEnum {
    MAN(1, "男"),
    WOMAN(0, "女"),
    UK(2, "保密");

    SexEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
