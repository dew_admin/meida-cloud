package com.meida.common.utils;

import org.apache.ibatis.io.Resources;

import java.io.IOException;
import java.io.Reader;

public class ResourcesUtil {

    public static Reader getResource(String resource) {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reader;
    }
}
