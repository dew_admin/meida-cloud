package com.meida.common.mybatis.query;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.ReflectionUtils;
import lombok.Data;

import java.util.Map;

/**
 * @author Administrator
 */
@Data
public class CriteriaDelete<T> extends QueryWrapper<T> {
    private EntityMap requestMap = new EntityMap();
    /**
     * 扩展事件名称
     */
    private String handlerName;

    private Class cls;
    /**
     * 主键字段
     */
    private String idField;
    /**
     * 主键值
     */
    private Long idValue;
    /**
     * 是否使用逻辑删除(必须添加逻辑删除字段)
     */
    private Boolean logicalDelete = true;

    /**
     * 是否删除关联文件
     */
    private Boolean delFile = false;

    /**
     * 表名
     */
    private String tableName;


    private Long[] ids;

    /**
     * 传递到afterDelete中的参数
     */
    private EntityMap extra = new EntityMap();

    public EntityMap putExtra(String key, Object value) {
        this.extra.put(key, value);
        return this.extra;
    }

    public Object getExtra(String key) {
        return this.extra.get(key);
    }

    public Long getLongExtra(String key) {
        return this.extra.get(key);
    }

    public EntityMap getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(EntityMap requestMap) {
        this.requestMap = requestMap;
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public CriteriaDelete() {

    }

    public EntityMap put(String key, Object value) {
        this.requestMap.put(key, value);
        return this.requestMap;
    }


    public CriteriaDelete(Map map, Class cls) {
        this.requestMap.putAll(map);
        this.cls = cls;
        this.handlerName = (String) map.getOrDefault(CommonConstants.HANDLER_NAME, "");
        this.idField = ReflectionUtils.getTableField(cls);
        this.tableName = ReflectionUtils.getTableName(cls);
        this.ids = requestMap.getLongIds("ids");
        this.idValue=requestMap.getLong(idField);
        if (FlymeUtils.isEmpty(ids) && FlymeUtils.isEmpty(idValue)) {
            ApiAssert.failure("参数不能为空");
        }
        //添加按主键删除
        if (FlymeUtils.isNotEmpty(this.ids)) {
            if (ids.length == 1) {
                this.eq(idField, this.ids[0]);
                this.idValue = this.ids[0];
            } else {
                this.in(idField, this.ids);
            }
        } else {
            this.eq(FlymeUtils.isNotEmpty(this.idValue), idField, this.idValue);
        }
    }

    /**
     * 设置是否是逻辑删除
     *
     * @param count
     */
    public void setCount(int count) {
        if (count == 0) {
            this.setLogicalDelete(false);
        }
    }

    public Long getIdValue() {
        return idValue;
    }

    public void setIdValue(Long idValue) {
        this.idValue = idValue;
    }

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }
}
