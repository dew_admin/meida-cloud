package com.meida.common.base.handler;

/**
 * 用户,部门,机构(organization),企业(company)最大添加数量检测
 *
 * @author zyf
 */
public interface BaseCheckAuthHandler {
    /**
     * 检测用户当前数量
     *
     * @param currentUserCount 当前用户数量
     * @return
     */
    default Boolean checkUserCount(Integer currentUserCount) {
        return true;
    }

    /**
     * 检测在线用户数量
     *
     * @param currentOnLineCount 当前在线用户数量
     * @return
     */
    default Boolean checkOnlineCount(Integer currentOnLineCount) {
        return true;
    }

    /**
     * 检测企业数量
     *
     * @param currentCompanyCount 当前企业数量
     * @return
     */
    default Boolean checkCompanyCount(Integer currentCompanyCount) {
        return true;
    }

    /**
     * 检测机构数量
     *
     * @param currentOrganizationCount 当前机构数量
     * @return
     */
    default Boolean checkOrganizationCount(Integer currentOrganizationCount) {
        return true;
    }

    /**
     * 检测部门数量
     *
     * @param currentDeptCount 当前部门数量
     * @return
     */
    default Boolean checkDeptCount(Integer currentDeptCount) {
        return true;
    }
}
