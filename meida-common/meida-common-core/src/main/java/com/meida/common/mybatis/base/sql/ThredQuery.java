package com.meida.common.mybatis.base.sql;

import com.meida.common.base.entity.EntityMap;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author zyf
 */
public class ThredQuery implements Callable<List<EntityMap>> {



    private SearchHandler searchHandler;

    private Integer pageNo;

    /**
     * 重新构造方法

     * @param searchHandler
     */
    public ThredQuery(int pageNo,SearchHandler searchHandler) {
        this.searchHandler = searchHandler;
        this.pageNo=pageNo;
    }

    @Override
    public List<EntityMap> call() throws Exception {
        return searchHandler.handler(pageNo);
    }
}