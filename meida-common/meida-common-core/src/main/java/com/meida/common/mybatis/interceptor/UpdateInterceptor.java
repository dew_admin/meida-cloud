package com.meida.common.mybatis.interceptor;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaUpdate;

/**
 * 扩展更新拦截器
 *
 * @author zyf
 */

public interface UpdateInterceptor<T> {
    /**
     * 条件验证
     *
     * @param cu
     * @param params
     * @return
     */
    default ResultBody validate(CriteriaUpdate cu, EntityMap params) {
        return ResultBody.ok();
    }


    /**
     * 更新条件扩展
     *
     * @param cu
     * @param params
     */
    default void prepare(CriteriaUpdate cu, EntityMap params, T t) {

    }




    /**
     * 更新后扩展
     *
     * @param cu
     * @param params
     */
    default void complete(CriteriaUpdate cu, EntityMap params, T t) {

    }

    /**
     * 返回保存对象
     *
     * @param cs         对象模型
     * @param requestMap 参数接收
     * @return
     */
    default T getEntity(CriteriaUpdate<T> cu, EntityMap requestMap) {
        return null;
    }

    /**
     * 返回自定义Mapper,当不使用自定义对象时可返回null
     *
     * @return
     */
    default SuperMapper getMapper() {
        return null;
    }

}
