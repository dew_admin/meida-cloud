package com.meida.common.base.service;


public interface BaseProductService {

    /**
     * 统计企业产品数量
     */
    Long countByCompanyId(Long companyId);

}
