package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;

public interface UserDeptAssignHandler {
    EntityMap getAuthDeptList(Long userId, Long organizationId);
}
