package com.meida.common.constants;

/**
 * @author zyf
 */
public interface SettingConstant {
    /**
     * 当前使用OSS
     */
    String OSS_USED = "OSS_USED";

    /**
     * 商品分类最大级别
     */
    String CATEGORY_LEVEL = "CATEGORY_LEVEL";

    /**
     * 是否启用真实短信验证码
     */
    String OPEN_SMS = "enable";
    /**
     * 短信验证码自定义处理器
     */
    String SMSCODE_HANDLER = "smsCodeHander";

    /**
     * 是否启用验证码手机号加密
     */
    String SMS_ENCRYPTION = "encryption";


    /**
     * 平台名称
     */
    String PLATFORM_NAME = "platformName";
    /**
     * 本地OSS配置
     */
    String LOCAL_OSS = "LOCAL_OSS";

    /**
     * 阿里内容安全配置
     */
    String ALI_GREEN = "ALI_GREEN";

    /**
     * 登录二维码redis前缀
     */
    String LOGIN_QRCODE="LQ:";
    /**
     * 用户扫码token
     */
    String LOGIN_QRCODE_TOKEN="LQT:";
}
