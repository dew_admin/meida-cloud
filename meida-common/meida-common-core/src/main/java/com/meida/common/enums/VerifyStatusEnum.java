package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 审核状态
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VerifyStatusEnum {
    NO(0, "未审核"),
    YES(1, "已审核"),
    FAIL(2, "审核失败");

    VerifyStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
