package com.meida.common.thread;

import com.meida.common.base.entity.EntityMap;

import java.util.List;

/**
 * 线程service
 *
 * @author Administrator
 */
public interface IThreadPoolExecutorService {

    /**
     * 单线程线程池
     *
     * @param runnable
     */
    void singleExecute(Runnable runnable);

    /**
     * 多线程线程池
     *
     * @param runnable
     */
    void execute(Runnable runnable);


    /**
     * 多线程线程池
     */
    <T> List<EntityMap> handlerListUseFutureTask(List<T> list, Integer count, ThreadHandler threadHandler);

    /**
     * 开启threadNum个线程处理数据
     *
     * @param data
     * @param threadNum
     * @param threadHandler
     * @param <T>
     */
    <T> void handlerByThreadNum(List<T> data, Integer threadNum, ThreadHandler threadHandler);

    /**
     * 每threadSize条数据开启一条线程
     *
     * @param data
     * @param threadSize
     * @param threadHandler
     * @param <T>
     */
    <T> void handlerByThreadSize(List<T> data, Integer threadSize, ThreadHandler threadHandler);

    <T> List<EntityMap> handlerList(List<T> list, Integer count, ThreadHandler threadHandler);

    <T> void submitList(List<T> list, Integer count, ThreadHandler threadHandler);
}
