package com.meida.common.base.event;

import com.meida.common.base.entity.EntityMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author zyf
 */
@Getter
@Setter
public class FlymeSyncEvent extends ApplicationEvent {

    private EntityMap entityMap;
    private String handlerName;

    public FlymeSyncEvent(String handlerName, EntityMap params) {
        super(params);
        this.entityMap = params;
        this.handlerName = handlerName;
    }
}
