package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenUser;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author zyf
 */
public interface AdminUserInfoHandler {


    /**
     * 扩展openUser
     *
     * @param openUser
     */
    default void initOpenUser(OpenUser openUser) {

    }

    /**
     * 更新用户状态前检查
     * @param map
     * @param userId
     * @return
     */
    default ResultBody beforeUpdateState(EntityMap map, Long userId) {
        return ResultBody.ok();
    }


}
