package com.meida.common.base.service;

/**
 * 即时通讯service
 */
public interface BaseImService {
    /**
     * 注册IM账户, 返回token
     *
     * @param id
     * @param name
     * @param avatar
     * @return
     */
    String register(String id, String name, String avatar);

    /**
     * 刷新用户
     *
     * @param id
     * @param name
     * @param avatar
     */
    void userRefresh(String id, String name, String avatar);
}
