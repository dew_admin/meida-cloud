package com.meida.common.jsonp;

import com.meida.common.base.utils.FlymeUtils;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zhua1 {

    public static String LOGIN_URL = "http://datahub.crpcg.com:83/";
    public static String USER_AGENT = "User-Agent";
    public static String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36";

    public static void main(String[] args) throws Exception {
        simulateLogin();
    }


    /**
     * 获取验证码
     */
    public static String getCheckCode() throws IOException {
        Connection con = Jsoup.connect("http://datahub.crpcg.com:83/checkcode.ashx");
        Response rs = con.ignoreContentType(true).execute();
        Map<String, String> cookies = rs.cookies();
        if (FlymeUtils.isNotEmpty(cookies)) {
            return cookies.get("checkcode");
        }
        return null;
    }


    /**
     * @param userName 用户名
     * @param pwd      密码
     * @throws Exception
     */
    public static void simulateLogin() throws Exception {

        Map<String, String> datas = new HashMap<>();

        datas.put("txtname", "1001778a");
        datas.put("txtpwd", "Xzyy*1234");
        datas.put("txtcheckcode", getCheckCode());
        datas.put("chkRemember", "false");
        datas.put("__VIEWSTATE", "/wEPDwUJMjgwMjgyODcwD2QWAgIBD2QWAgIDD2QWAmYPZBYCAgsPDxYCHgRUZXh0BRjpqozor4HnoIHovpPlhaXplJnor6/vvIFkZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAQULY2hrUmVtZW1iZXJc+xQWvnSo1PuAbzpul+4mMidoQO2SmdvV25t5iUi1gQ==");
        datas.put("__VIEWSTATEGENERATOR", "90059987");
        datas.put("__EVENTVALIDATION", "/wEdAAa2moCV0f7cVlXmb231qA/UVCDn2pfngd5PSl9R1fd0WGGcPOO2LAX9axRe6vMQj2Ewq1PKp6+/1yNGng6H71UxayAS+FutzojPRSQSNJduwhLeNW105wAmxvMhUTxvEW0a/2D6dwFT9qh1YnqNtVccQgPqsG4P95viej9vNTSATw==");
        /*
         * 第二次请求，以post方式提交表单数据以及cookie信息
         */
        Connection con2 = Jsoup.connect("http://datahub.crpcg.com:83/index.aspx");
        con2.header(USER_AGENT, USER_AGENT_VALUE);
        // 设置cookie和post上面的map数据
        Response login = con2.ignoreContentType(true).method(Method.POST)
                .data(datas).execute();
        // 打印，登陆成功后的信息
        System.out.println(login.body());

        // 登陆成功后的cookie信息，可以保存到本地，以后登陆时，只需一次登陆即可
        Map<String, String> map = login.cookies();
        for (String s : map.keySet()) {
            System.out.println(s + " : " + map.get(s));
        }
        //httpPost(map, "http://flow.jztit.com/_yvanui/query");
    }

    private static String httpPost(Map<String, String> sessionId, String url) throws IOException {
        //获取请求连接
        Connection con = Jsoup.connect(url);
        con.header(USER_AGENT, USER_AGENT_VALUE);
        con.header("Content-Type", "application/json;charset=utf-8");
        con.cookies(sessionId).timeout(10000);
        con.data("a", "b");
        //请求参数设置
        // con.requestBody("{\"db\":\"db\",\"sqlId\":\"/log/log@insert\",\"params\":{\"fbranchorgcode\":\"FDJ\",\"foperatercode\":\"cgb311\",\"foperatername\":\"山西华卫药业有限公司\",\"flogtype\":\"8\",\"flogtext\":\"查询:日期从【2021-10-01】至【2021-10-26】药品名称【】批号【】生产厂家【】销往单位【】销往区域【全部】\"}}");
        con.requestBody("{\"db\":\"db\",\"filterModel\":{},\"sortModel\":[],\"limit\":100,\"limitOffset\":0,\"needCount\":true,\"sqlId\":\"/flowquery/flowquery@query\",\"params\":{\"appUserName\":\"cgb311\",\"org\":[\"FDJ\"],\"productName\":\"\",\"batchnumber\":\"\",\"factory\":\"\",\"customerName\":\"\",\"area\":\"-1\",\"startDate\":\"2021-10-01\",\"endDate\":\"2021-10-26\"}}");
        Response login = con.method(Method.POST).ignoreContentType(true).execute();
        System.out.println(login.body());
        return null;
    }
}
