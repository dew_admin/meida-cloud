package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 第三方账号类型
 */
public enum ThirdAccountEnum {
    WEIXIN("WEIXIN", "微信"),
    WEIBO("WEIBO", "微博"),
    QQ("QQ", "QQ");

    ThirdAccountEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final String code;
    private final String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
