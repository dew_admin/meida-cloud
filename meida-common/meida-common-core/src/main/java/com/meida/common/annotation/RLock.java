package com.meida.common.annotation;

import java.lang.annotation.*;

/**
 * @Author:yexg
 * @Date:2019-07-31 10:43
 * @Description: 注解锁
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface RLock {
    /****加锁关键字***/
    String value() default "";

    /*****加锁前缀***/
    String prefix() default "";

    /****加锁关键字方法字段索引**/
    int paramIndex() default -1;

    /****加锁关键字map参数字段名**/
    String[] paramName() default "";

}
