package com.meida.common.base.client;

import com.meida.common.base.service.BasePushService;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author:yexg
 * @Date:2019-12-13 16:06
 * @Description:
 **/
public class BasePushClient<K extends Object> {
    /*****推送列表***/
    private ConcurrentHashMap<String, BasePushService<K>> map = new ConcurrentHashMap<>();

    public ConcurrentHashMap<String, BasePushService<K>> getMap() {
        return map;
    }

    /******获取推送实例***/
    public BasePushService getService(String key) {
        return map.get(key);
    }
}
