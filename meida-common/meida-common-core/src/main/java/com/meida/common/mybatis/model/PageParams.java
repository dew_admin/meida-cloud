package com.meida.common.mybatis.model;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.StringUtils;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 请求参数
 *
 * @author zyf
 * @date 2018/07/10
 */
public class PageParams extends Page implements Serializable {
    private static final long serialVersionUID = -1710273706052960025L;
    /**
     * 查询主键
     */
    private Long id;
    /**
     * 当前页
     */
    private int page = CommonConstants.DEFAULT_PAGE;
    /**
     * 每页显示条数
     */
    private int limit = CommonConstants.DEFAULT_LIMIT;
    /**
     * 排序方式
     */
    private String sort;


    /**
     * 排序字段
     */
    private String order;
    /**
     * 接口扩展handler名称
     */
    private String handlerName;
    /**
     * 表单参数
     */
    private ConcurrentHashMap<String, Object> requestMap = new ConcurrentHashMap<>();

    /**
     * 查询开始日期
     */
    private String beginDate;

    /**
     * 查询结束日期
     */
    private String endDate;

    /**
     * 数据结果集是否展示分页总条数等数据
     */
    private Integer simpleData = 1;


    /**
     * 排序
     */
    private String orderBy;

    public PageParams() {
        requestMap = new ConcurrentHashMap<>();
    }

    public PageParams(Map map) {
        if (map == null) {
            map = new ConcurrentHashMap<>();
        }
        this.page = Integer.parseInt(map.getOrDefault(CommonConstants.PAGE_KEY, CommonConstants.DEFAULT_PAGE).toString());
        this.limit = Integer.parseInt(map.getOrDefault(CommonConstants.PAGE_LIMIT_KEY, CommonConstants.DEFAULT_LIMIT).toString());
        this.sort = (String) map.getOrDefault(CommonConstants.PAGE_SORT_KEY, "");
        this.simpleData = Integer.parseInt(map.getOrDefault(CommonConstants.SIMPLEDATA_KEY, 1).toString());
        if (FlymeUtils.isNotEmpty(map.getOrDefault(CommonConstants.PARAMS_ID_KEY, ""))) {
            this.id = Long.parseLong(map.get(CommonConstants.PARAMS_ID_KEY).toString());
        }
        this.handlerName = (String) map.getOrDefault(CommonConstants.HANDLER_NAME, null);
        this.order = (String) map.getOrDefault(CommonConstants.PAGE_ORDER_KEY, "");
        this.beginDate = (String) map.getOrDefault(CommonConstants.QUERY_BEGINDATE, "");
        this.endDate = (String) map.getOrDefault(CommonConstants.QUERY_ENDDATE, "");
        super.setCurrent(page);
        super.setSize(limit);
        String searchCount = map.getOrDefault(CommonConstants.SEARCH_COUNT,"1").toString();
        if(searchCount.equals("false")||"0".equals(searchCount)){
            this.searchCount = false;
        }
        map.remove(CommonConstants.PAGE_KEY);
        map.remove(CommonConstants.PAGE_LIMIT_KEY);
        map.remove(CommonConstants.PAGE_SORT_KEY);
        map.remove(CommonConstants.PAGE_ORDER_KEY);
        map.remove(CommonConstants.HANDLER_NAME);
        map.remove(CommonConstants.SEARCH_COUNT);
        //map.remove(CommonConstants.PARAMS_ID_KEY);
        this.requestMap.putAll(map);
    }


    public PageParams(int page, int limit) {
        this(page, limit, "", "");
    }

    public PageParams(int page, int limit, String sort, String order) {
        this.page = page;
        this.limit = limit;
        this.sort = sort;
        this.order = order;
    }

    public synchronized int getPage() {
        if (page <= CommonConstants.MIN_PAGE) {
            page = 1;
        }
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public synchronized int getLimit() {
        if (limit > CommonConstants.MAX_LIMIT) {
            limit = CommonConstants.MAX_LIMIT;
        }
        return limit;
    }

    public long getOffset() {
        return (this.getCurrent()-1) * (long) getLimit();
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderBy() {
        if (StringUtils.isBlank(order)) {
            order = "asc";
        }
        if (StringUtils.isNotBlank(sort)) {
            this.setOrderBy(String.format("%s %s", StringUtils.camelToUnderline(sort), order));
        }
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public <T> T mapToObject(Class<T> t) {
        return  BeanUtil.mapToBean(this.requestMap,t,false);
    }

    public Map<String, Object> getRequestMap() {
        return requestMap;
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSimpleData() {
        return simpleData;
    }

    public void setSimpleData(Integer simpleData) {
        this.simpleData = simpleData;
    }
}
