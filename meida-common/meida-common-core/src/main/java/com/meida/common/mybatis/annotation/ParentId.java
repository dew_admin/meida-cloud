package com.meida.common.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 实体类父节点字段注解
 * @author flyme
 * @date 2019/10/24 16:06
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ParentId {
    String value() default "parentId";
}
