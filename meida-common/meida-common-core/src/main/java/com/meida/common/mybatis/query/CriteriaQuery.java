package com.meida.common.mybatis.query;

import cn.hutool.core.collection.ConcurrentHashSet;
import cn.hutool.core.util.ClassUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlKeyword;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.utils.*;
import org.springframework.core.annotation.AnnotationUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: zyf
 * @date: 2018/9/4 8:42
 * @@desc: 自定义查询构造器
 */
public class CriteriaQuery<T> extends QueryWrapper<T> {

    /**
     * 外键表别名对象
     */
    protected ConcurrentHashMap<String, String> aliasMap = new ConcurrentHashMap<>();

    protected List<JoinBean> joinSqList = Collections.synchronizedList(new ArrayList());

    /**
     * 查询字段
     */
    protected ConcurrentHashSet<String> select = new ConcurrentHashSet();
    /**
     * 分页对象
     */
    private PageParams pageParams;
    /**
     * 请求参数对象
     */
    private EntityMap requestMap = new EntityMap();

    private ConcurrentHashMap exportParams;
    /**
     * 扩展事件名称
     */
    private String handlerName;
    /**
     * 查询主表实体类Class
     */
    private Class<T> cls;

    private T model;
    /**
     * 表名
     */
    private String tableName;

    /**
     * 表别名
     */
    private String tableAlias;

    /**
     * 主键字段
     */
    private String idField;

    /**
     * 是否分页
     */
    private Boolean isPager = true;

    private Boolean initExportData = false;

    private String exportKey;

    private Boolean isQuery;


    public CriteriaQuery(PageParams pageParams) {
        this.pageParams = pageParams;
        pageParams.setCurrent(pageParams.getPage());
        pageParams.setSize(pageParams.getLimit());
        String order = pageParams.getOrder();
        apply("1=1");
        if (FlymeUtils.isNotEmpty(order)) {
            //自动添加ordery by
            String sort = pageParams.getSort();
            List<String> sortList = new ArrayList<>();
            if(FlymeUtils.isNotEmpty(sort)) {
                sortList.addAll(Arrays.asList(sort.split(",")));
            }
            String[] orderArr = order.split(",");
            for(int i=0;i<orderArr.length;i++){
                boolean isAsc = false;
                if(sortList.size()>i){
                    isAsc = StringUtils.equalsIgnoreCase(SqlKeyword.ASC.name(), sortList.get(i));
                }
                orderBy(true, isAsc, orderArr[i]);
            }
        }
    }

    public CriteriaQuery(Map map, Class<T> cls, Boolean tag) {
        this.init(map, cls, tag);
    }

    public CriteriaQuery(Map map, Class<T> cls) {
        this.init(map, cls, true);
    }

    public void init(Map map, Class<T> cls, Boolean tag) {
        this.exportParams = new ConcurrentHashMap(map);
        this.cls = cls;
        this.pageParams = new PageParams(map);
        this.tableName = ReflectionUtils.getTableName(cls);
        this.tableAlias = ReflectionUtils.getAlias(cls);
        this.idField = ReflectionUtils.getTableField(cls);
        ApiAssert.isNotEmpty(cls.getSimpleName() + "实体类未设置主键策略", this.idField);
        String handlerName = pageParams.getHandlerName();
        pageParams.setCurrent(pageParams.getPage());
        pageParams.setSize(pageParams.getLimit());
        String order = pageParams.getOrder();
        T t = pageParams.mapToObject(cls);
        this.setHandlerName(handlerName);
        this.setRequestMap(pageParams);
        this.setModel(t);
        //apply("1=1");
        //如果有排序设置排序字段
        if (FlymeUtils.isNotEmpty(order)) {
            String sort = pageParams.getSort();
            List<String> sortList = new ArrayList<>();
            if(FlymeUtils.isNotEmpty(sort)) {
                sortList.addAll(Arrays.asList(sort.split(",")));
            }
            String[] orderArr = order.split(",");
            for(int i=0;i<orderArr.length;i++){
                boolean isAsc = false;
                if(sortList.size()>i){
                    isAsc = StringUtils.equalsIgnoreCase(SqlKeyword.ASC.name(), sortList.get(i));
                }
                orderBy(true, isAsc, orderArr[i]);
            }

            //添加ordery by
//            String sort = FlymeUtils.getString(pageParams.getSort(), SqlKeyword.DESC.name());
//            this.orderBy(order, sort);
        }
        Long idValue = Optional.ofNullable(requestMap.getLong(idField)).orElse(FlymeUtils.getLong(requestMap.get("id")));
        if (FlymeUtils.isNotEmpty(idValue) && tag) {
            this.eq(true, tableAlias + "." + idField, idValue);
        }
    }

    public CriteriaQuery(Map map) {
        this.pageParams = new PageParams(map);
        String handlerName = pageParams.getHandlerName();
        pageParams.setCurrent(pageParams.getPage());
        pageParams.setSize(pageParams.getLimit());
        this.setHandlerName(handlerName);
        this.setRequestMap(pageParams);
    }


    public CriteriaQuery(Class<T> cls) {
        this.cls = cls;
        this.tableName = ReflectionUtils.getTableName(cls);
        this.tableAlias = ReflectionUtils.getAlias(cls);
        this.idField = ReflectionUtils.getTableField(cls);
    }


    /**
     * 设置targetEntity
     */
    public CriteriaQuery<T> setTargetEntity(Class cls) {
        TableAlias tableAlias = AnnotationUtils.getAnnotation(cls, TableAlias.class);
        super.eq(true, Objects.requireNonNull(tableAlias).value() + ".targetEntity", cls.getSimpleName());
        return this;
    }


    /**
     * 创建外键表关联对象,需要在mapper(xml)中编写join
     */
    @Deprecated
    public void createAlias(String entiry, String alias) {
        this.aliasMap.put(entiry, alias);
    }


    /**
     * 创建外键表关联对象,需要在mapper(xml)中编写join
     */
    @Deprecated
    public void createAlias(Class right) {
        TableAlias tableAlias = AnnotationUtils.getAnnotation(right, TableAlias.class);
        if (FlymeUtils.isNotEmpty(tableAlias)) {
            this.aliasMap.put(tableAlias.value(), tableAlias.value());
        }
    }

    /**
     * 创建连接查询
     *
     * @param joinCls 要连接查询的表对象
     */
    public JoinBean createJoin(Class joinCls) {
        ApiAssert.isNotEmpty("请调用CriteriaQuery(PageModel pageModel, Class cls)方法", cls);
        JoinBean joinBean = new JoinBean(cls, joinCls);
        this.joinSqList.add(joinBean);
        return joinBean;
    }


    /**
     * 创建连接查询
     *
     * @param strCls 要连接查询的表对象
     */
    public JoinBean createJoin(String strCls) {
        Class joinCls = ClassUtil.loadClass(strCls);
        ApiAssert.isNotEmpty("请调用CriteriaQuery(PageModel pageModel, Class cls)方法", joinCls);
        JoinBean joinBean = new JoinBean(cls, joinCls);
        this.joinSqList.add(joinBean);

        return joinBean;
    }

    /**
     * 创建用户连接查询
     */
    public JoinBean createJoinAppUser() {
        Class joinCls = ClassUtil.loadClass("com.meida.module.user.client.entity.AppUser");
        JoinBean joinBean = new JoinBean(cls, joinCls);
        this.joinSqList.add(joinBean);
        return joinBean;
    }


    /**
     * 等于
     */
    @Override
    public CriteriaQuery<T> eq(String column, Object val) {
        super.eq(FlymeUtils.isNotEmpty(val) && !val.equals(-1) && !val.equals(-1L), column, val);
        return this;
    }


    /**
     * 等于
     */
    public CriteriaQuery<T> eq(String column) {
        Object val = requestMap.get(column);
        super.eq(FlymeUtils.isNotEmpty(val) && !val.equals(-1) && !val.equals(-1L), column, val);
        return this;
    }


    /**
     * eq条件且字段会作为查询条件
     */
    public CriteriaQuery<T> eq(Class cls, String field) {
        String alias = ReflectionUtils.getAlias(cls);
        Object v = requestMap.get(field);
        super.eq(FlymeUtils.isNotEmpty(v), alias + "." + field, requestMap.get(field));
        return this;
    }


    /**
     * eq条件且字段会作为查询条件
     */
    public CriteriaQuery<T> eq(Class cls, String field, Object v) {
        String alias = ReflectionUtils.getAlias(cls);
        super.eq(true, alias + "." + field, v);
        return this;
    }


    /**
     * like
     */
    @Override
    public CriteriaQuery<T> like(String column, Object val) {
        super.like(FlymeUtils.isNotEmpty(val), column, val);
        return this;
    }

    /**
     * like
     */
    public CriteriaQuery<T> like(Class cls, String field) {
        String alias = ReflectionUtils.getAlias(cls);
        Object v = requestMap.get(field);
        super.like(FlymeUtils.isNotEmpty(v), alias + "." + field, requestMap.get(field));
        return this;
    }

    /**
     * like
     */
    public CriteriaQuery<T> like(Class cls, String column, Object val) {
        String alias = ReflectionUtils.getAlias(cls);
        super.like(FlymeUtils.isNotEmpty(val), alias + "." + column, val);
        return this;
    }

    public CriteriaQuery<T> orderBy(String order, String sort) {
        if (FlymeUtils.isNotEmpty(order)) {
            Boolean isAsc = StringUtils.equalsIgnoreCase(SqlKeyword.ASC.name(), sort);
            super.orderBy(true, isAsc, order);
        }
        return this;
    }

    /**
     * like并设置查询字段
     */
    public CriteriaQuery<T> likeByField(Class cls, String field) {
        String alias = ReflectionUtils.getAlias(cls);
        Object v = requestMap.get(field);
        super.like(FlymeUtils.isNotEmpty(v), alias + "." + field, v);
        return this;
    }

    /**
     * in
     */
    @Override
    public CriteriaQuery<T> in(String column, Object... val) {
        super.in(FlymeUtils.isNotEmpty(val) && FlymeUtils.isNotEmpty(val[0]), column, val);
        return this;
    }


    /**
     * ge
     */
    @Override
    public CriteriaQuery<T> ge(String column, Object val) {
        super.ge(FlymeUtils.isNotEmpty(val), column, val);
        return this;
    }

    /**
     * ge
     */
    public CriteriaQuery<T> ge(Class cls, String column, Object val) {
        String alias = ReflectionUtils.getAlias(cls);
        super.ge(FlymeUtils.isNotEmpty(val), alias + "." + column, val);
        return this;
    }


    /**
     * ge
     */
    public CriteriaQuery<T> ge(Class cls, String field) {
        String alias = ReflectionUtils.getAlias(cls);
        Object v = requestMap.get(field);
        super.ge(FlymeUtils.isNotEmpty(v), alias + "." + field, v);
        return this;
    }


    /**
     * le
     */
    @Override
    public CriteriaQuery<T> le(String column, Object val) {
        super.le(FlymeUtils.isNotEmpty(val), column, val);
        return this;
    }

    /**
     * le
     */
    public CriteriaQuery<T> le(Class cls, String column, Object val) {
        String alias = ReflectionUtils.getAlias(cls);
        super.le(FlymeUtils.isNotEmpty(val), alias + "." + column, val);
        return this;
    }

    /**
     * le
     */
    public CriteriaQuery<T> le(Class cls, String field) {
        String alias = ReflectionUtils.getAlias(cls);
        Object v = requestMap.get(field);
        super.le(FlymeUtils.isNotEmpty(v), alias + "." + field, v);
        return this;
    }

    /**
     * lt
     */
    @Override
    public CriteriaQuery<T> lt(String column, Object val) {
        super.lt(FlymeUtils.isNotEmpty(val), column, val);
        return this;
    }

    /**
     * lt
     */
    public CriteriaQuery<T> lt(Class cls, String column, Object val) {
        String alias = ReflectionUtils.getAlias(cls);
        super.lt(FlymeUtils.isNotEmpty(val), alias + "." + column, val);
        return this;
    }

    /**
     * gt
     */
    @Override
    public CriteriaQuery<T> gt(String column, Object val) {
        super.gt(FlymeUtils.isNotEmpty(val), column, val);
        return this;
    }

    /**
     * gt
     */
    public CriteriaQuery<T> gt(Class cls, String column, Object val) {
        String alias = ReflectionUtils.getAlias(cls);
        super.gt(FlymeUtils.isNotEmpty(val), alias + "." + column, val);
        return this;
    }

    /**
     * limit
     */
    public CriteriaQuery<T> limit(Integer num) {
        if (num > 0) {
            super.last("limit " + num);
        }
        return this;
    }


    /**
     * or
     */
    @Override
    public CriteriaQuery<T> or() {
        super.or();
        return this;
    }

    /**
     * likeLeft
     */
    @Override
    public QueryWrapper<T> likeLeft(String column, Object val) {
        return super.likeLeft(FlymeUtils.isNotEmpty(val), column, val);
    }

    /**
     * likeRight
     */
    @Override
    public QueryWrapper<T> likeRight(String column, Object val) {
        return super.likeRight(FlymeUtils.isNotEmpty(val), column, val);
    }

    /**
     * 追加查询查询
     */
    public CriteriaQuery<T> addSelect(String... field) {
        for (String s : field) {
            this.select.add(s);
        }
        return this;
    }

    /**
     * 字符串匹配
     * where ids regexp '1|6|5';
     * @param column
     * @param val
     * @return
     */
    public CriteriaQuery<T> regexp(String column, String val) {
        if (FlymeUtils.isNotEmpty(val)) {
            val = val.replaceAll(",", "|");
            super.apply(column + " regexp " + "'" + val + "'");
        }
        return this;
    }

    /**
     * 字符串匹配
     * where ids regexp '1|6|5';
     * @param column
     * @param val
     * @return
     */
    public CriteriaQuery<T> regexp(Class cls, String column, String val) {
        if (FlymeUtils.isNotEmpty(val)) {
            val = val.replaceAll(",", "|");
            String alias = ReflectionUtils.getAlias(cls);
            super.apply(alias + "." + column + " regexp " + "'" + val + "'");
        }
        return this;
    }

    /**
     * 追加查询查询
     */
    public CriteriaQuery<T> addSelect(Class cls, String... field) {
        TableAlias tableAlias = AnnotationUtils.getAnnotation(cls, TableAlias.class);
        String alias = tableAlias.value();
        for (String s : field) {
            this.select.add(alias + "." + s);
        }
        return this;
    }

    /**
     * 追加查询
     */
    public CriteriaQuery<T> select(Class cls, String... field) {
        TableAlias tableAlias = AnnotationUtils.getAnnotation(cls, TableAlias.class);
        String alias = tableAlias.value();
        for (String s : field) {
            StringBuffer stringBuilder1 = new StringBuffer(s);
            if (s.indexOf("(") > -1) {
                stringBuilder1.insert(s.indexOf("(") + 1, alias + ".");
            } else {
                stringBuilder1.insert(0, alias + ".");
            }
            this.select.add(stringBuilder1.toString());
        }
        return this;
    }

    /**
     * 追加链接查询,默认left
     */
    public CriteriaQuery<T> joinSelect(Class cls, String... field) {
        createJoin(cls);
        TableAlias tableAlias = AnnotationUtils.getAnnotation(cls, TableAlias.class);
        String alias = tableAlias.value();
        for (String s : field) {
            this.select.add(alias + "." + s);
        }
        return this;
    }

    public PageParams getPageParams() {
        return pageParams;
    }

    public String getSelect() {
        StringBuffer str = new StringBuffer();
        String sqlSelect = getSqlSelect();
        if (FlymeUtils.isNotEmpty(sqlSelect)) {
            select.add(String.join(",", sqlSelect));
        }
        if (CollectionUtils.isEmpty(select)) {
            select.add("*");
        }
        return String.join(",", select);
    }

    public String getJoinSql() {
        StringBuilder builder = new StringBuilder();
        String sql = "%s JOIN %s %s ON %s.%s= %s.%s\n";
        if (FlymeUtils.isNotEmpty(joinSqList)) {
            joinSqList.forEach(item -> builder.append(String.format(sql, item.getJoinType().name(), item.getJoinTableName(), item.getJoinAlias(), item.getMainAlias(), item.getMainField(), item.getJoinAlias(), item.getJoinField())));
        }
        return builder.toString();
    }

    public EntityMap getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(EntityMap requestMap) {
        this.requestMap = requestMap;
    }

    public void setRequestMap(PageParams pageParams) {
        this.requestMap.putAll(pageParams.getRequestMap());
    }

    public void setRequestMap(PageParams pageParams, EntityMap requestMap) {
        this.requestMap.putAll(pageParams.getRequestMap());
        this.requestMap.putAll(requestMap);
    }

    public <T> T getEntity(Class<T> t) {
        return JsonUtils.getEntity(t, this.requestMap);
    }


    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }


    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public Class<T> getCls() {
        return cls;
    }

    public void setCls(Class<T> cls) {
        this.cls = cls;
    }

    /**
     * 清空查询字段
     */
    public void clear() {
        this.select.clear();
    }

    public String getIdField() {
        return idField;
    }

    public ConcurrentHashSet<String> getSetSelect() {
        return select;
    }

    public String getParams(String key) {
        return (String) requestMap.get(key);
    }


    public String getParams(String key, String def) {
        return requestMap.get(key, def);
    }


    public Long getLong(String key) {
        return requestMap.getLong(key);
    }

    public Integer getInt(String key, Integer def) {
        return requestMap.getInt(key, def);
    }

    public Boolean getPager() {
        return isPager;
    }

    public void setPager(Boolean pager) {
        isPager = pager;
    }

    public Boolean getInitExportData() {
        return initExportData;
    }

    public void setInitExportData(Boolean initExportData) {
        this.initExportData = initExportData;
    }

    public ConcurrentHashMap getExportParams() {
        return exportParams;
    }

    public void setExportParams(ConcurrentHashMap exportParams) {
        this.exportParams = exportParams;
    }

    public String getExportKey() {
        return exportKey;
    }

    public void setExportKey(String exportKey) {
        this.exportKey = exportKey;
    }
}

