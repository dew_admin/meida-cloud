package com.meida.common.utils;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.base.utils.FlymeUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * 日期工具类,
 * 继承org.apache.commons.lang.time.DateUtils类
 *
 * @author zyf
 * @version 2014-4-15
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {


    private static final long ONE_MILLIS = 1000;
    private static final long ONE_MINUTE = 60;
    private static final long ONE_HOUR = 3600;
    private static final long ONE_DAY = 86400;
    private static final long ONE_MONTH = 2592000;
    private static final long ONE_YEAR = 31104000;

    public static DateTimeFormatter yyyyMMdd = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter yyyyMd = DateTimeFormatter.ofPattern("yyyy-M-d");
    public static DateTimeFormatter yyyyMM = DateTimeFormatter.ofPattern("yyyy-MM");
    public static DateTimeFormatter dd = DateTimeFormatter.ofPattern("dd");
    public static DateTimeFormatter MMdd = DateTimeFormatter.ofPattern("MM-dd");
    public static DateTimeFormatter yyyy年MM月dd日 = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
    public static DateTimeFormatter nyyyMMdd = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static DateTimeFormatter nyyyyMMddHHmmss = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static DateTimeFormatter yyyyMMddHHmmssSSS = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    public static DateTimeFormatter yyyyMMddHHmmss = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter yyyyMMddHHmmss2 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    public static DateTimeFormatter HHmm = DateTimeFormatter.ofPattern("HH:mm");
    public static DateTimeFormatter HH = DateTimeFormatter.ofPattern("HH");

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM",
            "yyyyMMdd", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMM"};

    /**
     * 日期型字符串转化为日期 格式
     * {
     * "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
     * "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM",
     * "yyyyMMdd", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMM"}
     */
    public static Date parseDate(String str) {
        if (str == null) {
            return null;
        }
        try {
            return parseDate(str, parsePatterns);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd）
     */
    public static String formatDate() {
        return formatDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date data) {
        return DateFormatUtils.format(data, "yyyy-MM-dd");
    }

    /**
     * 获取当前时间戳（yyyyMMddHHmmss）
     *
     * @return nowTimeStamp
     */
    public static long getCurrentTimestamp() {
        long nowTimeStamp = Long.parseLong(getCurrentTimestampStr());
        return nowTimeStamp;
    }

    /**
     * 获取当前时间戳（yyyyMMddHHmmss）
     *
     * @return
     */
    public static String getCurrentTimestampStr() {
        return formatDate(new Date(), "yyyyMMddHHmmss");
    }


    /**
     * date大于等于当前日期
     */
    public static Boolean geToday(Date date) {
        Boolean tag = false;
        Date today = parseDate(formatDate());
        if (date.after(today) || date.equals(today)) {
            tag = true;
        }
        return tag;
    }

    /**
     * date 小于当前日期
     */
    public static Boolean ltToday(Date date) {
        Date today = parseDate(formatDate());
        return date.before(today);
    }

    /**
     * 字符串转换成Date格式
     *
     * @param dateStr 日期型字符串
     * @param pattern 日期格式
     * @return
     */
    public static Date strToDate(String dateStr, String pattern) {
        try {
            if ((dateStr == null) || (dateStr.length() == 0)) {
                return null;
            }

            if (pattern == null) {
                pattern = "yyyy-MM-dd";
            }

            SimpleDateFormat simpleDateFormat = getSimpleDateFormat(pattern);
            return simpleDateFormat.parse(dateStr);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 锁对象
     */
    private static final Object LOCK_OBJ = new Object();
    /**
     * 存放不同的日期模板格式的sdf的Map
     */
    private static Map<String, ThreadLocal<SimpleDateFormat>> simpleDateFormatMap = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    /**
     * 返回一个ThreadLocal的sdf,每个线程只会new一次sdf
     *
     * @param pattern
     * @return
     */
    public static SimpleDateFormat getSimpleDateFormat(final String pattern) {
        ThreadLocal<SimpleDateFormat> threadLocalSimpleDateFormat = simpleDateFormatMap.get(pattern);
        // 此处的双重判断和同步是为了防止simpleDateFormatMap这个单例被多次put重复的sdf
        if (threadLocalSimpleDateFormat == null) {
            synchronized (LOCK_OBJ) {
                threadLocalSimpleDateFormat = simpleDateFormatMap.get(pattern);
                if (threadLocalSimpleDateFormat == null) {
                    // 只有Map中还没有这个pattern的sdf才会生成新的sdf并放入map
                    // 这里是关键,使用ThreadLocal<SimpleDateFormat>替代原来直接new SimpleDateFormat
                    threadLocalSimpleDateFormat = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(pattern);
                        }
                    };
                    simpleDateFormatMap.put(pattern, threadLocalSimpleDateFormat);
                }
            }
        }
        return threadLocalSimpleDateFormat.get();
    }

    /**
     * 字符串转换的带time的Date格式
     *
     * @param dateStr
     * @return
     */
    public static Date strToDateTime(String dateStr) {
        try {
            return strToDate(dateStr, "yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 字符串转换的带time的Date格式
     *
     * @param dateStr
     * @return
     */
    public static Date strToDate(String dateStr) {
        try {
            return strToDate(dateStr, "yyyy-MM-dd");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date date, String pattern) {
        String formatDate = null;
        if (FlymeUtils.isNotEmpty(date)) {
            if (pattern != null) {
                formatDate = DateFormatUtils.format(date, pattern);
            } else {
                formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
            }
        }
        return formatDate;
    }


    /**
     * 获取当前时间戳（yyyyMMddHHmmss）
     *
     * @return nowTimeStamp
     */
    public static long getTimestamp() {
        long nowTimeStamp = Long.parseLong(getTimestampStr());
        return nowTimeStamp;
    }

    /**
     * 获取时间
     */
    public static LocalDateTime getLocalDateTime() {
        LocalTime localtime = LocalTime.now();
        LocalDateTime localDateTime = localtime.atDate(getLocalDate());
        return localDateTime;
    }

    /**
     * 获取当前日期yyyy年MM月dd日
     */
    public static LocalDate getLocalDate() {
        LocalDate localDate = LocalDate.now();
        return localDate;
    }

    /**
     * 获取当前时间戳（yyyyMMddHHmmss）
     *
     * @return
     */
    public static String getTimestampStr() {
        return formatDate(new Date(), "yyyyMMddHHmmss");
    }

    /**
     * 获取Unix时间戳
     *
     * @return
     */
    public static long getUnixTimestamp() {
        long nowTimeStamp = System.currentTimeMillis() / 1000;
        return nowTimeStamp;
    }

    /**
     * 获取Unix时间戳
     *
     * @return
     */
    public static String getUnixTimestampStr() {
        return String.valueOf(getUnixTimestamp());
    }

    /**
     * 转换Unix时间戳
     *
     * @return nowTimeStamp
     */
    public static long parseUnixTimeStamp(long time) {
        return time / ONE_MILLIS;
    }


    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime() {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String getDateTime() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     */
    public static String getWeek() {
        return formatDate(new Date(), "E");
    }

    /**
     * 获取过去的天数
     *
     * @param date
     * @return
     */
    public static long getBeforeDays(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (ONE_DAY * ONE_MILLIS);
    }

    /**
     * 获取过去的小时
     *
     * @param date
     * @return
     */
    public static long getBeforeHours(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (ONE_HOUR * ONE_MILLIS);
    }

    /**
     * 获取过去的分钟
     *
     * @param date
     * @return
     */
    public static long getBeforeMinutes(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (ONE_MINUTE * ONE_MILLIS);
    }

    /**
     * 获取过去的秒
     *
     * @param date
     * @return
     */
    public static long getBeforeSeconds(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / ONE_MILLIS;
    }

    /**
     * 获取两个日期之间的天数
     *
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (ONE_MILLIS * ONE_DAY);
    }

    /**
     * 距离今天多久
     *
     * @param date
     * @return
     */
    public static String formatFromToday(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            long time = date.getTime() / ONE_MILLIS;
            long now = System.currentTimeMillis() / ONE_MILLIS;
            long ago = now - time;
            if (ago <= ONE_HOUR) {
                return ago / ONE_MINUTE + "分钟前";
            } else if (ago <= ONE_DAY) {
                return ago / ONE_HOUR + "小时" + (ago % ONE_HOUR / ONE_MINUTE)
                        + "分钟前";
            } else if (ago <= ONE_DAY * 2) {
                return "昨天" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                        + calendar.get(Calendar.MINUTE) + "分";
            } else if (ago <= ONE_DAY * 3) {
                return "前天" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                        + calendar.get(Calendar.MINUTE) + "分";
            } else if (ago <= ONE_MONTH) {
                long day = ago / ONE_DAY;
                return day + "天前" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                        + calendar.get(Calendar.MINUTE) + "分";
            } else if (ago <= ONE_YEAR) {
                long month = ago / ONE_MONTH;
                long day = ago % ONE_MONTH / ONE_DAY;
                return month + "个月" + day + "天前"
                        + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                        + calendar.get(Calendar.MINUTE) + "分";
            } else {
                long year = ago / ONE_YEAR;
                // JANUARY which is 0 so month+1
                int month = calendar.get(Calendar.MONTH) + 1;
                return year + "年前" + month + "月" + calendar.get(Calendar.DATE)
                        + "日";
            }

        } else {
            return "";
        }

    }

    /**
     * 距离今天多久
     *
     * @param createAt
     * @return
     */
    public static String formatFromTodayCn(Date createAt) {
        // 定义最终返回的结果字符串。
        String interval = null;
        if (createAt == null) {
            return "";
        }
        long millisecond = System.currentTimeMillis() - createAt.getTime();

        long second = millisecond / ONE_MILLIS;

        if (second <= 0) {
            second = 0;
        }
        //*--------------微博体（标准）
        if (second == 0) {
            interval = "刚刚";
        } else if (second < ONE_MINUTE / 2) {
            interval = second + "秒以前";
        } else if (second >= ONE_MINUTE / 2 && second < ONE_MINUTE) {
            interval = "半分钟前";
        } else if (second >= ONE_MINUTE && second < ONE_MINUTE * ONE_MINUTE) {
            //大于1分钟 小于1小时
            long minute = second / ONE_MINUTE;
            interval = minute + "分钟前";
        } else if (second >= ONE_HOUR && second < ONE_DAY) {
            //大于1小时 小于24小时
            long hour = (second / ONE_MINUTE) / ONE_MINUTE;
            interval = hour + "小时前";
        } else if (second >= ONE_DAY && second <= ONE_DAY * 2) {
            //大于1D 小于2D
            interval = "昨天";
        } else if (second >= ONE_DAY * 2 && second <= ONE_DAY * 7) {
            //大于2D小时 小于 7天
            long day = ((second / ONE_MINUTE) / ONE_MINUTE) / 24;
            interval = day + "天前";
        } else if (second > ONE_DAY * 7 && second <= ONE_DAY * 30) {
            //大于2D小时 小于 7天
            long day = ((second / ONE_MINUTE) / ONE_MINUTE) / 24;
            interval = day + "天前";
        }else if (second <= ONE_DAY * 365 && second > ONE_DAY * 30) {
            //大于30天小于365天
            interval = formatDate(createAt, "MM-dd");
        } else if (second >= ONE_DAY * 365) {
            //大于365天
            interval = formatDate(createAt, "yyyy-MM-dd");
        } else {
            interval = "0";
        }
        return interval;
    }


    /**
     * 距离截止日期还有多长时间
     *
     * @param date
     * @return
     */
    public static String formatFromDeadline(Date date) {
        long deadline = date.getTime() / ONE_MILLIS;
        long now = (System.currentTimeMillis()) / ONE_MILLIS;
        long remain = deadline - now;
        if (remain <= ONE_HOUR) {
            return "只剩下" + remain / ONE_MINUTE + "分钟";
        } else if (remain <= ONE_DAY) {
            return "只剩下" + remain / ONE_HOUR + "小时"
                    + (remain % ONE_HOUR / ONE_MINUTE) + "分钟";
        } else {
            long day = remain / ONE_DAY;
            long hour = remain % ONE_DAY / ONE_HOUR;
            long minute = remain % ONE_DAY % ONE_HOUR / ONE_MINUTE;
            return "只剩下" + day + "天" + hour + "小时" + minute + "分钟";
        }

    }


    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis
     * @return
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (ONE_DAY * ONE_MILLIS);
        long hour = (timeMillis / (ONE_HOUR * ONE_MILLIS) - day * 24);
        long min = ((timeMillis / (ONE_MINUTE * ONE_MILLIS)) - day * 24 * ONE_MINUTE - hour * ONE_MINUTE);
        long s = (timeMillis / ONE_MILLIS - day * 24 * ONE_MINUTE * ONE_MINUTE - hour * ONE_MINUTE * ONE_MINUTE - min * ONE_MINUTE);
        long sss = (timeMillis - day * 24 * ONE_MINUTE * ONE_MINUTE * ONE_MILLIS - hour * ONE_MINUTE * ONE_MINUTE * ONE_MILLIS - min * ONE_MINUTE * ONE_MILLIS - s * ONE_MILLIS);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }


    public  static String  formatStrTime(long millis){
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);

        return  MessageFormat.format("{0}小时{1}分钟{2}秒", hours, minutes, seconds);
    }


    /**
     * Unix时间戳转换成指定格式日期字符串
     *
     * @param timestampString 时间戳 如："1473048265";
     * @param formats         要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static String unixTimeStamp2Date(String timestampString, String formats) {
        if (StringUtils.isBlank(formats)) {
            formats = "yyyy-MM-dd HH:mm:ss";
        }
        Long timestamp = Long.parseLong(timestampString) * ONE_MINUTE;
        String date = new SimpleDateFormat(formats, Locale.CHINA).format(new Date(timestamp));
        return date;
    }

    /**
     * 日期格式字符串转换成Unix时间戳
     *
     * @param dateStr 字符串日期
     * @param format  如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2UnixTimeStamp(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(dateStr).getTime() / ONE_MINUTE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 毫秒转化时分秒毫秒 10000 - 10秒
     *
     * @param ms
     * @return
     */
    public static String formatTime(Long ms) {
        long ss = ONE_MINUTE;
        long mi = ss * ONE_MINUTE;
        long hh = mi * ONE_MINUTE;
        long dd = hh * 24;

        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = (ms - day * dd - hour * hh) / mi;
        Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        StringBuffer sb = new StringBuffer();
        if (day > 0) {
            sb.append(day + "天");
        }
        if (hour > 0) {
            sb.append(hour + "小时");
        }
        if (minute > 0) {
            sb.append(minute + "分钟");
        }
        if (second > 0) {
            sb.append(second + "秒");
        }
        return sb.toString();
    }

    /**
     * 获取现在时间
     *
     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
     */
    public static Date getNowDateTime() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        ParsePosition pos = new ParsePosition(0);
        Date currentTime_2 = formatter.parse(dateString, pos);
        return currentTime_2;
    }

    public static Date getNowDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        ParsePosition pos = new ParsePosition(0);
        Date currentTime_2 = formatter.parse(dateString, pos);
        return currentTime_2;
    }

    /**
     * 从年份0开始到现在的天数 ,类似mysql的to_days函数结果
     *
     * @return
     */
    public static long toDays() {
        long millis = System.currentTimeMillis();
        long nowDays = millis / 1000 / 60 / 60 / 24;
        return nowDays + 719528;
    }

    /**
     * 获取本月第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfMonth() {
        LocalDate today = LocalDate.now();
        return today.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取本月最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfMonth() {
        LocalDate today = LocalDate.now();
        return today.with(TemporalAdjusters.lastDayOfMonth());
    }

    /**
     * 获取本年第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfYear() {
        LocalDate today = LocalDate.now();
        return today.with(TemporalAdjusters.firstDayOfYear());
    }


    /**
     * 获取本年最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfYear() {
        LocalDate today = LocalDate.now();
        return today.with(TemporalAdjusters.lastDayOfYear());
    }

    /**
     * 获取某日期所在年份第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfYear(LocalDate localDate) {
        return localDate.with(TemporalAdjusters.firstDayOfYear());
    }

    /**
     * 获取某日期所在年份最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfYear(LocalDate localDate) {
        return localDate.with(TemporalAdjusters.lastDayOfYear());
    }


    /**
     * 获取某日期所在年份第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfYear(String localDate) {
        return parseLocalDate(localDate).with(TemporalAdjusters.firstDayOfYear());
    }

    /**
     * 获取某日期所在年份最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfYear(String localDate) {
        return parseLocalDate(localDate).with(TemporalAdjusters.lastDayOfYear());
    }

    /**
     * 获取某日期所在月份第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfMonth(LocalDate localDate) {
        return localDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取某日期所在月份最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfMonth(LocalDate localDate) {
        return localDate.with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 获取某日期所在月份第一天
     *
     * @return
     */
    public static LocalDate getFirstDayOfMonth(String localDate) {
        return parseLocalDate(localDate).with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获取某日期所在月份最后一天
     *
     * @return
     */
    public static LocalDate getLastDayOfMonth(String localDate) {
        return parseLocalDate(localDate).with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 日期天加减
     */
    public static String plusDays(LocalDate date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = date.plusDays(n).toString();
        }
        return result;
    }

    /**
     * 日期天加减
     */
    public static String plusDays(Date date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = dateToLocalDate(date).plusDays(n).toString();
        }
        return result;
    }

    /**
     * 日期天加减
     */
    public static Date plusDateDays(Date date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = dateToLocalDate(date).plusDays(n).toString();
        }
        return strToDate(result);
    }


    /**
     * 日期周加减
     */
    public static String plusWeeks(LocalDate date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = date.plusWeeks(n).toString();
        }
        return result;
    }

    /**
     * 日期月份加减
     */
    public static String plusMonth(LocalDate date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = date.plusMonths(n).toString();
        }
        return result;
    }


    /**
     * 日期年度加减
     */
    public static String plusYears(LocalDate date, int n) {
        String result = "";
        if (ObjectUtils.isNotEmpty(date)) {
            result = date.plusYears(n).toString();
        }
        return result;
    }

    /**
     * 当前日期往后推算n年
     */
    public static LocalDate minusYears(long add) {
        LocalDate today = getLocalDate();
        return today.minusYears(add);
    }

    /**
     * 当前日期往后推算n年
     */
    public static LocalDate plusYears(int add) {
        LocalDate today = getLocalDate();
        return today.plusYears(add);
    }


    /**
     * 当前日期往后推算n月
     */
    public static LocalDate minusMonths(long add) {
        LocalDate today = getLocalDate();
        return today.minusMonths(add);
    }

    /**
     * 当前日期往后推算n月
     */
    public static LocalDate plusMonths(int add) {
        LocalDate today = getLocalDate();
        return today.plusMonths(add);
    }

    /**
     * 日期往后推算n个月
     */
    public static String nextMonth(String date, int n) {
        LocalDate today = LocalDate.parse(date);
        return today.plusMonths(n).toString();
    }

    /**
     * 当前日期往后推算n天
     */
    public static LocalDate minusDays(long add) {
        LocalDate today = getLocalDate();
        return today.minusDays(add);
    }


    /**
     * 当前日期往后推算n天
     */
    public static LocalDate plusDays(int add) {
        LocalDate today = getLocalDate();
        return today.plusDays(add);
    }

    /**
     * 当前日期往后推算n周
     */
    public static LocalDate minusWeeks(long add) {
        LocalDate today = getLocalDate();
        return today.minusWeeks(add);
    }

    /**
     * 当前日期往后推算n周
     */
    public static LocalDate plusWeeks(int add) {
        LocalDate today = getLocalDate();
        return today.plusWeeks(add);
    }


    /**
     * 格式化日期
     */
    public static String format(LocalDate date, DateTimeFormatter formatter) {
        return date.format(formatter);
    }

    /**
     * 格式化日期
     */
    public static String format(LocalDate date) {
        return date.format(yyyyMMdd);
    }

    /**
     * 日期转字符串 yyyy-MM-dd
     */
    public static LocalDate parseLocalDate(String date) {
        LocalDate localDate = LocalDate.parse(date.replaceAll("\\.", "-").trim(), yyyyMMdd);
        return localDate;
    }

    /**
     * 日期转字符串 yyyy-MM-dd
     */
    public static LocalDate parseLocalDate(String date, DateTimeFormatter format) {
        TemporalAccessor temporalAccessor = format.parse(date);
        return LocalDate.from(temporalAccessor);
    }

    /**
     * localDate转Date
     *
     * @param localDate
     * @return
     */
    public static Date localDateToDate(LocalDate localDate) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * localDateTime 转Date
     *
     * @param localDateTime
     * @return
     */
    public static Date LocalDateTimeToDate(LocalDateTime localDateTime) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    public static LocalDate dateToLocalDate(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.toLocalDate();
    }

    /**
     * 获取当前月份对应同比月份第一天
     *
     * @return
     */
    public static String getMonthFirstDayByTongBi() {
        LocalDate monthFirstDay = getFirstDayOfMonth();
        return plusYears(monthFirstDay, -1);
    }

    /**
     * 获取当前月份对应环比月份第一天
     *
     * @return
     */
    public static String getMonthFirstDayByHuanBi() {
        LocalDate monthFirstDay = getFirstDayOfMonth();
        return plusMonth(monthFirstDay, -1);
    }

    /**
     * 获取当前月份对应同比月份最后一天
     *
     * @return
     */
    public static String getMonthLastDayByTongBi() {
        LocalDate monthFirstDay = getLastDayOfMonth();
        return plusYears(monthFirstDay, -1);
    }

    /**
     * 获取当前月份对应环比月份最后一天
     *
     * @return
     */
    public static String getMonthLastDayByHuanBi() {
        LocalDate monthFirstDay = getLastDayOfMonth();
        return plusMonth(monthFirstDay, -1);
    }

    /**
     * 获取本周第一天
     */
    public static LocalDate getFirstDayOfWeek() {
        return getLocalDate().with(DayOfWeek.of(1));
    }

    /**
     * 获取本周最后一天
     */
    public static LocalDate getLastDayOfWeek() {
        return getLocalDate().with(DayOfWeek.of(7));
    }

    /**
     * 获取当前日期所在周
     */
    public static String getDayOfWeek(String date) {
        if (ObjectUtils.isEmpty(date)) {
            return "";
        }
        int dw = parseLocalDate(date).getDayOfWeek().getValue();// 星期几
        return "周" + ConvertUtils.numToWeek(dw);
    }

    /**
     * 字符串(yyyy-MM-dd HH:mm:ss型)LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(String date) {
        String[] d = date.split(":");
        if (d.length == 2) {
            date += ":00";
        }
        TemporalAccessor temporalAccessor = yyyyMMddHHmmss.parse(date);
        return LocalDateTime.from(temporalAccessor);
    }


    /**
     * 获取一个月有多少天
     */
    public static int getMonthDaysNum(LocalDate data) {
        return getLastDayOfMonth(data).getDayOfMonth();
    }

    /**
     * 获取当月有多少天
     */
    public static int getMonthDaysNum() {
        return getLastDayOfMonth().getDayOfMonth();
    }


    public static Date convertToDate(Object obj, Date defaultVal) {
        return obj == null ? defaultVal : DateUtils.strToDate(FlymeUtils.convertToString(obj), null);
    }

    public static Date convertToDate(Object obj) {
        return convertToDate(obj, null);
    }

    public static Date convertToDatetime(Object obj, Date defaultVal) {
        return obj == null ? defaultVal : DateUtils.strToDateTime(FlymeUtils.convertToString(obj));
    }

    /**
     * 判断当前日期是否是datetime
     */
    public static Boolean isDataTime(String date) {
        Boolean tag = false;
        if (date.indexOf(":") != -1) {
            tag = true;
        }
        return tag;
    }

    /**
     * 日期转字符串 yyyy-MM-dd
     */
    public static LocalDateTime parseDateTime(String date, DateTimeFormatter format) {
        String[] d = date.split(":");
        if (d.length == 2) {
            date += ":00";
        }
        TemporalAccessor temporalAccessor = format.parse(date);
        return LocalDateTime.from(temporalAccessor);
    }

    /**
     * 日期转字符串 yyyy-MM-dd
     */
    public static LocalDateTime parseDateTime(String date) {
        String[] d = date.split(":");
        if (d.length == 2) {
            date += ":00";
        }
        TemporalAccessor temporalAccessor = yyyyMMddHHmmss.parse(date);
        return LocalDateTime.from(temporalAccessor);
    }

    /**
     * 日期转字符串 yyyy-MM-dd
     */
    public static LocalDate parseDate(String date, DateTimeFormatter format) {
        TemporalAccessor temporalAccessor = format.parse(date);
        return LocalDate.from(temporalAccessor);
    }

    /**
     * 计算两个日期之间的间隔天数
     */
    public static int daysBetween(LocalDate s, LocalDate e) {
        Long n = ChronoUnit.DAYS.between(s, e);
        return n.intValue();
    }

    /**
     * 计算两个日期之间的间隔天数
     */
    public static int daysBetween(Date s, Date e) {
        return daysBetween(DateUtils.formatDate(s), DateUtils.formatDate(e));
    }


    /**
     * a<b
     */
    public static Boolean lt(LocalDate a, LocalDate b) {
        return a.isBefore(b);
    }

    /**
     * a<b
     */
    public static Boolean le(LocalDate a, LocalDate b) {
        return a.isBefore(b) || a.isEqual(b);
    }

    /**
     * a>b
     */
    public static Boolean gt(LocalDate a, LocalDate b) {
        return a.isAfter(b);
    }

    /**
     * a>=b
     */
    public static Boolean ge(LocalDate a, LocalDate b) {
        return a.isAfter(b) || a.isEqual(b);
    }

    /**
     * a<b
     */
    public static Boolean lt(LocalDateTime a, LocalDateTime b) {
        return a.isBefore(b);
    }

    /**
     * a<=b
     */
    public static Boolean le(LocalDateTime a, LocalDateTime b) {
        return a.isBefore(b) || a.isEqual(b);
    }


    /**
     * a>b
     */
    public static Boolean gt(LocalDateTime a, LocalDateTime b) {
        return a.isAfter(b);
    }

    /**
     * a>=b
     */
    public static Boolean ge(LocalDateTime a, LocalDateTime b) {
        return a.isAfter(b) || a.isEqual(b);
    }


    /**
     * a>=b
     */
    public static Boolean ge(String a, String b) {
        Boolean tag = false;
        if (FlymeUtils.allNotNull(a, b)) {
            if (isDataTime(a)) {
                LocalDateTime a1 = parseDateTime(a);
                LocalDateTime b1 = parseDateTime(b);
                if (a1.isAfter(b1) || a1.isEqual(b1)) {
                    tag = true;
                }
            } else {
                LocalDate a1 = parseLocalDate(a.trim());
                LocalDate b1 = parseLocalDate(b.trim());
                if (a1.isAfter(b1) || a1.isEqual(b1)) {
                    tag = true;
                }
            }
        }
        return tag;
    }

    /**
     * a<=b
     */
    public static Boolean le(String a, String b) {
        Boolean tag = false;
        if (FlymeUtils.allNotNull(a, b)) {
            if (isDataTime(a)) {
                LocalDateTime a1 = parseDateTime(a);
                LocalDateTime b1 = parseDateTime(b);
                if (a1.isBefore(b1) || a1.isEqual(b1)) {
                    tag = true;
                }
            } else {
                LocalDate a1 = parseLocalDate(a.trim());
                LocalDate b1 = parseLocalDate(b.trim());
                if (a1.isBefore(b1) || a1.isEqual(b1)) {
                    tag = true;
                }
            }
        }
        return tag;
    }

    /**
     * a>b
     */
    public static Boolean gt(String a, String b) {
        Boolean tag = false;
        if (FlymeUtils.allNotNull(a, b)) {
            if (isDataTime(a)) {
                LocalDateTime a1 = parseDateTime(a.trim());
                LocalDateTime b1 = parseDateTime(b.trim());
                if (a1.isAfter(b1)) {
                    tag = true;
                }
            } else {
                LocalDate a1 = parseLocalDate(a.trim());
                LocalDate b1 = parseLocalDate(b.trim());
                if (a1.isAfter(b1)) {
                    tag = true;
                }
            }
        }
        return tag;
    }

    /**
     * a<b
     */
    public static Boolean lt(String a, String b) {
        Boolean tag = false;
        if (FlymeUtils.allNotNull(a, b)) {
            if (isDataTime(a)) {
                LocalDateTime a1 = parseDateTime(a.trim());
                LocalDateTime b1 = parseDateTime(b.trim());
                if (a1.isBefore(b1)) {
                    tag = true;
                }
            } else {
                LocalDate a1 = parseLocalDate(a.trim());
                LocalDate b1 = parseLocalDate(b.trim());
                if (a1.isBefore(b1)) {
                    tag = true;
                }
            }
        }
        return tag;
    }

    /**
     * 计算两个日期之间的间隔天数
     */
    public static int daysBetween(String s, String e) {
        Long n = 0L;
        if (isDataTime(s)) {
            LocalDateTime a1 = parseDateTime(s, yyyyMMddHHmmss);
            LocalDateTime b1 = parseDateTime(e, yyyyMMddHHmmss);
            n = ChronoUnit.DAYS.between(a1, b1);
        } else {
            LocalDate a1 = parseDate(s, yyyyMMdd);
            LocalDate b1 = parseDate(e, yyyyMMdd);
            n = ChronoUnit.DAYS.between(a1, b1);
        }
        return n.intValue();
    }

    public static Date convertToDatetime(Object obj) {
        return convertToDatetime(obj, null);
    }

    public static Timestamp convertToTimestamp(Object obj, Timestamp defaultVal) {
        return obj == null ? defaultVal : Timestamp.valueOf(FlymeUtils.convertToString(obj));
    }

    public static Timestamp convertToTimestamp(Object obj) {
        return convertToTimestamp(obj, null);
    }

    public static void main(String[] args) {
        try {
            System.out.println(getNowDateTime());
            System.out.println(formatTime(10000L));
            Date date1 = parseDate("2019-07-16");
            System.out.println(getMonthFirstDayByTongBi());
            System.out.println(getMonthFirstDayByHuanBi());
            System.out.println(getMonthLastDayByTongBi());
            System.out.println(getMonthLastDayByHuanBi());
            System.out.println(getLastDayOfYear());

            System.out.println(minusDays(1));
            System.out.println(getLastDayOfYear());
            System.out.println(getLastDayOfMonth().getDayOfMonth());
            System.out.println(plusDays(getNowDate(),-1099));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
