package com.meida.common.mybatis.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * @author zyf
 */
public interface IBaseService<T> extends IService<T> {



    ResultBody listEntityMap(Map map);



    /**
     * 自定义分页(xml中配置外键链接查询)
     *
     * @param map
     * @return
     */
    abstract ResultBody pageList(Map map);

    /**
     * 导出数据
     *
     * @param map
     * @param request
     * @param response
     * @param config   [fileName，title,handlerName,sheetName]
     */
    String  export(ModelMap modelMap, Map map, HttpServletRequest request, HttpServletResponse response, String... config);

    /**
     * 导出数据
     *
     * @param map
     * @param request
     * @param response
     * @param config   [fileName，title,handlerName,sheetName]
     */
    void  templateExport(Map map, HttpServletRequest request, HttpServletResponse response, String... config);



    /**
     * 自定义更新带扩展事件
     */
    ResultBody edit(Map map);



    ResultBody add(Map map);


    ResultBody delete(Map map);



    /**
     * 自定义查询单个实体(返回自定义Map类型,便于枚举转换)
     */

    ResultBody get(Map map);


    List<EntityMap> selectEntityMap(CriteriaQuery<?> cq);

    EntityMap findOne(CriteriaQuery cq);

    T getByProperty(String propertyName, Object value);

    EntityMap findById(Serializable id);

    /**
     * 设置状态
     *
     * @param state
     * @return
     */
    ResultBody setState(Map map, String fieldName, Integer state);

    /**
     * 设置推荐状态
     *
     * @param ids
     * @param recommend
     * @return
     */
    ResultBody setRecommend(Long[] ids, Integer recommend);

    //批量保存
    Boolean mutiInsert(List<?> objectList);

}
