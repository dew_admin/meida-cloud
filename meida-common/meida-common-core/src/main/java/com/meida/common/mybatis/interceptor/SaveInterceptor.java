package com.meida.common.mybatis.interceptor;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

/**
 * 扩展保存拦截器
 *
 * @author zyf
 */
public interface SaveInterceptor<T> {


    /**
     * 条件验证
     *
     * @return
     */
    default ResultBody validate(CriteriaSave cs, EntityMap params) {
        return ResultBody.ok();
    }


    /**
     * 保存前预处理
     *
     * @param cs     对象模型
     * @param params 参数接收
     * @return
     */
    void prepare(CriteriaSave cs, EntityMap params, T t);


    /**
     * 保存后扩展
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    default void complete(CriteriaSave cs, EntityMap params, T entity) {

    }


    /**
     * 返回保存对象
     *
     * @param cs         对象模型
     * @param requestMap 参数接收
     * @return
     */
    default <T> T getEntity(CriteriaSave cs, EntityMap requestMap) {
        return null;
    }

    /**
     * 返回自定义Mapper,当不使用自定义对象时可返回null
     *
     * @return
     */
    default SuperMapper getMapper() {
        return null;
    }


}
