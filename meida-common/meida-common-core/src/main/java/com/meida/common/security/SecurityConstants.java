package com.meida.common.security;

/**
 * @author zyf
 */
public class SecurityConstants {
    public final static String OPEN_ID = "openid";
    public final static String DOMAIN = "domain";
    public final static String ACCOUNTID = "accountId";
    public final static String AUTHORITY_PREFIX_MENU = "MENU_";
    public final static String AUTHORITY_PREFIX_ROLE = "ROLE_";
    public final static String AUTHORITY_PREFIX_API = "API_";
    public final static String AUTHORITY_PREFIX_ACTION = "ACTION_";
}
