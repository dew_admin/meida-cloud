package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.event.FlymeEvent;
import com.meida.common.base.event.FlymeSyncEvent;
import com.meida.common.base.listener.FlymeListener;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.SpringContextHolder;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @author zyf
 */
@Component
public class FlymeEventHandler {
    /**
     * 监听FlymeEvent 并分发事件
     */
    @EventListener
    public void event(FlymeEvent flymeEvent) {
        EntityMap entityMap = flymeEvent.getEntityMap();
        String handlerName = flymeEvent.getHandlerName();
        if (FlymeUtils.isNotEmpty(handlerName)) {
            handlerEvent(handlerName, entityMap);
        }
    }

    /**
     * 监听FlymeEvent 并分发事件
     */
    @Async(value = "taskExecutor")
    @EventListener
    public void snycEvent(FlymeSyncEvent flymeSyncEvent) {
        EntityMap entityMap = flymeSyncEvent.getEntityMap();
        String handlerName = flymeSyncEvent.getHandlerName();
        if (FlymeUtils.isNotEmpty(handlerName)) {
            handlerEvent(handlerName, entityMap);
        }
    }

    private void handlerEvent(String handlerName, EntityMap entityMap) {
        if (FlymeUtils.isNotEmpty(handlerName)) {
            FlymeListener flymeListener = SpringContextHolder.getHandler(handlerName, FlymeListener.class);
            if (FlymeUtils.isNotEmpty(flymeListener)) {
                flymeListener.onMessage(entityMap);
            }
        }
    }
}
