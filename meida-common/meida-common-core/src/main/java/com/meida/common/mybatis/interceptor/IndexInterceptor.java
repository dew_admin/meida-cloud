package com.meida.common.mybatis.interceptor;

import com.meida.common.base.entity.EntityMap;

/**
 * 首页接口拦截器
 * @author zyf
 */
public interface IndexInterceptor {
    EntityMap complete(EntityMap map,Long userId);
}
