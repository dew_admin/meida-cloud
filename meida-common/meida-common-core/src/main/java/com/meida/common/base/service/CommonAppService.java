package com.meida.common.base.service;

import com.meida.common.security.OpenClientDetails;

public interface CommonAppService {

    /**
     * 获取app和应用信息
     *
     * @param clientId
     * @return
     */
    OpenClientDetails getAppClientInfo(String clientId);
}
