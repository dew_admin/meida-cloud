package com.meida.common.springmvc.binder;

import org.springframework.beans.propertyeditors.PropertiesEditor;

/**
 * @author zyf
 */
public class IntegerEditor extends PropertiesEditor {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text == null || text.equals("") || text.equals("off") || text.equals("NaN") || text.equals("false")) {
            text = "0";
        }
        if (text.equals("on") || text.equals("true")) {
            text = "1";
        }
        setValue(Integer.parseInt(text));
    }

    @Override
    public String getAsText() {
        return getValue().toString();
    }
}
