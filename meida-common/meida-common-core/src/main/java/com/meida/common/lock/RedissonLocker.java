package com.meida.common.lock;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author flyme
 * @date 2019/10/26 18:25
 */
public interface RedissonLocker {
    /**
     * 拿不到锁就不罢休线程就一直block 没有超时时间,默认30s
     *
     * @param lockKey
     * @return
     */
    RLock lock(Object lockKey);

    /**
     * 自己设置超时时间
     *
     * @param lockKey 锁的key
     * @param timeout 秒 如果是-1，直到自己解锁，否则不会自动解锁
     * @return
     */
    RLock lock(String lockKey, int timeout);

    /**
     * 自己设置超时时间
     *
     * @param lockKey 锁的key
     * @param unit    锁时间单位
     * @param timeout 超时时间
     */
    RLock lock(String lockKey, TimeUnit unit, int timeout);

    /**
     * 尝试加锁，最多等待waitTime，上锁以后leaseTime自动解锁
     *
     * @param lockKey 锁key
     * @param unit    锁时间单位
     * @return 如果获取成功，则返回true，如果获取失败（即锁已被其他线程获取），则返回false
     */
    boolean tryLock(String lockKey, TimeUnit unit, LockConstant lockTime);

    boolean tryLock(final String lockKey, final String value, final long time, final TimeUnit unit);


    boolean fairLock(String lockKey, TimeUnit unit, LockConstant lockTime);

    boolean fairLock(String lockKey, TimeUnit unit, int leaseTime);

    void unlock(String lockKey);

    void unlock(RLock lock);


    long lock(String key, Long timeoutSecond);

    void unLock(String key, long lockValue);

}
