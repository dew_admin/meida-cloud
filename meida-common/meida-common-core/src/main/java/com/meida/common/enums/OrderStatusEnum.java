package com.meida.common.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

/**
 * 订单状态
 *
 * @author Administrator
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
@JSONType(serializeEnumAsJavaBean = true)
public enum OrderStatusEnum {
    /**
     * 内置订单状态
     */
    UNPAY(1, "待付款"),
    UNSEND(20, "待发货"),
    UNRECEIVE(21, "待收货"),
    FINISHED(3, "已完成"),
    CLOSE(4, "已取消"),
    REFUND(5, "已退款");
    public String text;

    public Integer code;

    OrderStatusEnum(Integer code, String text) {
        this.text = text;
        this.code = code;
    }

    OrderStatusEnum(Integer code) {
        this.text = getText(code);
        this.code = code;
    }


    public static String getText(Object code) {
        String v = "";
        for (OrderStatusEnum accountTypeEnum : values()) {
            if (accountTypeEnum.code.equals(code)) {
                v = accountTypeEnum.text;
                break;
            }
        }
        return v;
    }

    public static String getName(String code) {
        String v = "";
        for (OrderStatusEnum accountTypeEnum : values()) {
            if (accountTypeEnum.code.equals(code)) {
                v = accountTypeEnum.name();
                break;
            }
        }
        return v;
    }

    public static OrderStatusEnum getEnum(Integer code) {
        for (OrderStatusEnum accountTypeEnum : values()) {
            if (accountTypeEnum.code.equals(code)) {
                return accountTypeEnum;
            }
        }
        return null;
    }


}
