package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;

import java.util.Map;

/**
 * 支付信息构建处理器
 *
 * @author zyf
 */
public interface PayInfoHandler {


    /**
     * 条件验证
     *
     * @return
     */
    default ResultBody validate(CriteriaSave cs, EntityMap requestMap) {
        return ResultBody.ok();
    }


    /**
     * 保存前预处理
     *
     * @param cs         对象模型
     * @param requestMap 参数接收
     * @return
     */
    void prepare(CriteriaSave cs, EntityMap requestMap);


    /**
     * 保存后扩展
     *
     * @return
     */
    default void complete(CriteriaSave cs, EntityMap requestMap, Object entity) {

    }


    /**
     * 返回保存对象
     *
     * @param cs         对象模型
     * @param requestMap 参数接收
     * @param outTradeNo 订单号
     * @return
     */
    default <T> T getPayInfo(CriteriaSave cs, EntityMap requestMap, String outTradeNo) {
        return null;
    }



}
