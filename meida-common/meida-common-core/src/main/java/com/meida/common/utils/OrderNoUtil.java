package com.meida.common.utils;

import com.meida.common.base.utils.FlymeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author flyme
 * @date 2019/12/4 10:17
 */
@Component
public class OrderNoUtil {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final String ORDER_NUMBER_PREFIX = "0";
    private static final String REDIS_ORDER_NUMBER_KEY = "orderNo";
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private static final Lock lock = new ReentrantLock();
    private static List<String> list = Collections.synchronizedList(new ArrayList<>());


    public String getOrderNo() {
        lock.lock();
        String orderNo = null;
        try {
            // 获取redis此时订单号
            if (FlymeUtils.isEmpty(stringRedisTemplate.opsForValue().get(REDIS_ORDER_NUMBER_KEY))) {
                // 初始化
                stringRedisTemplate.opsForValue().set(REDIS_ORDER_NUMBER_KEY, LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault()).format(dateTimeFormatter) + "000");
            }
            // 获取当前订单号2019 0628 173739 001
            long currentOrder = stringRedisTemplate.opsForValue().increment(REDIS_ORDER_NUMBER_KEY, 1L);
            long current = Long.valueOf(String.valueOf(currentOrder).substring(0, 14));
            // 当前时间
            long now = Long.valueOf(LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault()).format(dateTimeFormatter));

            if (current != now) {
                if (now < current) {

                    // 循环等待到下一秒 重新从001 开始
                    while (true) {
                        now = Long.valueOf(LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.systemDefault()).format(dateTimeFormatter));
                        if (now == current) {
                            // 重新 001 开始
                            stringRedisTemplate.opsForValue().set(REDIS_ORDER_NUMBER_KEY, now + "000");
                            orderNo = ORDER_NUMBER_PREFIX + stringRedisTemplate.opsForValue().increment(REDIS_ORDER_NUMBER_KEY, 1L);
                            break;
                        }
                    }

                } else {
                    // 重新 001 开始
                    stringRedisTemplate.opsForValue().set(REDIS_ORDER_NUMBER_KEY, now + "000");
                    orderNo = ORDER_NUMBER_PREFIX + stringRedisTemplate.opsForValue().increment(REDIS_ORDER_NUMBER_KEY, 1L);
                }

            } else {
                // 直接返回当前的订单号
                orderNo = ORDER_NUMBER_PREFIX + currentOrder;
            }


            System.out.println(LocalDateTime.now() + ":orderNo = " + orderNo + ":flag=" + list.contains(orderNo));
            list.add(orderNo);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            lock.unlock();
        }

        return orderNo;

    }

    public String getIncrementNum() {
        String currentDate = new SimpleDateFormat("yyyy").format(new Date());
        String key = "IncrementNum" + "flyme" + currentDate;
        // 不存在准备创建 键值对
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, stringRedisTemplate.getConnectionFactory());
        Long counter = entityIdCounter.incrementAndGet();
        return currentDate+"88"+SequenceUtils.getSequence(counter);
    }

}