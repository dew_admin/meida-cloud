package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 收藏&点赞&关注枚举
 */
public enum ColleconEnum {
    SC(1, "收藏"),
    DZ(2, "点赞"),
    GZ(3, "关注");

    ColleconEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
