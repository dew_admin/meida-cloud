package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;

import java.util.List;

/**
 * 文件服务公共接口
 *
 * @author flyme
 * @date 2019-06-03
 */
public interface CommonFileService {

    /**
     * 获取文件
     *
     * @param busId
     * @param cls
     * @return
     */
    List<EntityMap> selectFileList(Long busId, Class cls);


    /**
     * 获取文件
     *
     * @param busId
     * @param cls
     * @param fileType
     * @return
     */
    List<EntityMap> selectFileListByFileType(Long busId, Class cls, String fileType);


    /**
     * 获取文件
     *
     * @param busId
     * @param clsName
     * @param fileType
     * @return
     */
    List<EntityMap> selectFileListByFileType(Long busId, String clsName, String fileType);


    /**
     * 根据fileIds获取
     *
     * @param fileIds
     * @param callBack
     * @return
     */
    List<EntityMap> selectFileList(List<Long> fileIds, CriteriaQueryCallBack callBack);

    /**
     * 根据业务Id,业务类型获取
     *
     * @param busId
     * @param clsName
     * @param fileType
     * @return
     */
    List<EntityMap> selectFileListLikeFileType(Long busId, String clsName, String fileType);

    /**
     * 根据业务Id,业务类型获取
     *
     * @param busId
     * @param clsName
     * @param fileType
     * @return
     */
    List<EntityMap> selectFileListNotLikeFileType(Long busId, String clsName, String fileType);

    /**
     * 根据业务Id,业务类型获取
     *
     * @param busId
     * @param clsName
     * @param callBack
     * @return
     */
    List<EntityMap> selectFileList(Long busId, String clsName, CriteriaQueryCallBack callBack);

    /**
     * 根据业务Id,业务类型获取
     *
     * @param busId
     * @param clsName
     * @param fileGroup
     * @return
     */
    List<EntityMap> selectFileListByFileGroup(Long busId, String clsName, Integer fileGroup);
    /**
     * 根据业务Id,业务类型获取
     *
     * @param busId
     * @param clsName
     * @param fileGroup
     * @param callBack
     * @return
     */
    List<EntityMap> selectFileListByFileGroup(Long busId, String clsName, Integer fileGroup, CriteriaQueryCallBack callBack);

    /**
     * 获取封面图（文件列表第一张）
     * @param fileIds
     * @return
     */
     String getCoverImg(String fileIds);
}
