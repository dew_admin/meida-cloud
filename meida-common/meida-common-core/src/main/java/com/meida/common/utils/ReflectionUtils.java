package com.meida.common.utils;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.annotation.NotDelete;
import com.meida.common.mybatis.annotation.ParentId;
import com.meida.common.mybatis.annotation.TableAlias;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 获取父类中的所有属性和方法 工具类
 */
public class ReflectionUtils {

    /**
     * 循环向上转型, 获取对象的 DeclaredMethod
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @return 父类中的方法对象
     */
    public static Method getDeclaredMethod(Object object, String methodName, Class<?>... parameterTypes) {
        Method method = null;
        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                method = clazz.getDeclaredMethod(methodName, parameterTypes);
                return method;
            } catch (Exception e) {
                //这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                //如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(),最后就不会进入到父类中了
            }
        }
        return null;
    }

    /**
     * 直接调用对象方法, 而忽略修饰符(private, protected, default)
     *
     * @param object         : 子类对象
     * @param methodName     : 父类中的方法名
     * @param parameterTypes : 父类中的方法参数类型
     * @param parameters     : 父类中的方法参数
     * @return 父类中方法的执行结果
     */
    public static Object invokeMethod(Object object, String methodName, Class<?>[] parameterTypes,
                                      Object[] parameters) {
        //根据 对象、方法名和对应的方法参数 通过反射 调用上面的方法获取 Method 对象
        Method method = getDeclaredMethod(object, methodName, parameterTypes);

        //抑制Java对方法进行检查,主要是针对私有方法而言
        method.setAccessible(true);

        try {
            if (null != method) {
                //调用object 的 method 所代表的方法，其方法的参数是 parameters
                return method.invoke(object, parameters);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredField
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return 父类中的属性对象
     */
    public static Field getDeclaredField(Object object, String fieldName) {
        Field field = null;
        Class<?> clazz = object.getClass();
        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (Exception e) {
                //这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                //如果这里的异常打印或者往外抛，则就不会执行clazz = clazz.getSuperclass(),最后就不会进入到父类中了
            }
        }
        return null;
    }

    /**
     * 获取实体类主键
     */
    public static String getTableField(Class cls) {
        Field[] fields = ReflectUtil.getFields(cls);
        for (Field field : fields) {
            TableId tableId = field.getAnnotation(TableId.class);
            if (ObjectUtils.isNotEmpty(tableId)) {
                return tableId.value();
            }
        }
        return null;
    }

    /**
     * 获取实体类父节点主键
     */
    public static String getParentId(Class cls) {
        Field[] fields = ReflectUtil.getFields(cls);
        for (Field field : fields) {
            ParentId parentId = field.getAnnotation(ParentId.class);
            if (ObjectUtils.isNotEmpty(parentId)) {
                return parentId.value();
            }
        }
        return null;
    }

    /**
     * 获取实体类父节点主键
     */
    public static String getUserId(Class cls) {
        Field[] fields = ReflectUtil.getFields(cls);
        for (Field field : fields) {
            TableField userId = field.getAnnotation(TableField.class);
            if (ObjectUtils.isNotEmpty(userId) && userId.fill().equals(FieldFill.INSERT)) {
                return userId.value();
            }
        }
        return null;
    }

    /**
     * 获取删除标记字段
     */
    public static String getAllowDelField(Class cls) {
        Field[] fields = ReflectUtil.getFields(cls);
        for (Field field : fields) {
            NotDelete notDelete = field.getAnnotation(NotDelete.class);
            if (ObjectUtils.isNotEmpty(notDelete)) {
                return notDelete.value();
            }
        }
        return null;
    }

    /**
     * 获取表明
     */
    public static String getTableName(Class cls) {
        TableName tableName = AnnotationUtils.getAnnotation(cls, TableName.class);
        if (FlymeUtils.isNotEmpty(tableName)) {
            return tableName.value();
        } else {
            return null;
        }
    }

    /**
     * 获取表别名
     */
    public static String getAlias(Class cls) {
        TableAlias tableAlias = AnnotationUtils.getAnnotation(cls, TableAlias.class);
        if (FlymeUtils.isNotEmpty(tableAlias)) {
            return tableAlias.value();
        } else {
            return null;
        }
    }

    /**
     * 直接设置对象属性值, 忽略 private/protected 修饰符, 也不经过 setter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @param value     : 将要设置的值
     */
    public static void setFieldValue(Object object, String fieldName, Object value) {
        //根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);
        //抑制Java对其的检查
        field.setAccessible(true);
        try {
            //将 object 中 field 所代表的值 设置为 value
            field.set(object, value);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 直接读取对象的属性值, 忽略 private/protected 修饰符, 也不经过 getter
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return : 父类中的属性值
     */
    public static Object getFieldValue(Object object, String fieldName) {

        //根据 对象和属性名通过反射 调用上面的方法获取 Field对象
        Field field = getDeclaredField(object, fieldName);
        //抑制Java对其的检查
        field.setAccessible(true);
        try {
            //获取 object 中 field 所代表的属性值
            return field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
