package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;

import java.util.List;

/**
 * 后台用户全局接口
 *
 * @author zyf
 */
public interface BaseAdminUserService {

    /**
     * 删除企业后台账户
     *
     * @param companyId
     * @return
     */
    Boolean deleteByCompanyId(Long companyId);

    /**
     * 根据角色code查询用户ID
     *
     * @param roleCode
     * @param companyId
     * @return
     */
    List<Long> selectUserIdByRoleCode(String roleCode, Long companyId);

    /**
     * 统计用户数量
     *
     * @param companyId
     * @param deptId
     * @return
     */
    Long countByCompanyIdAndDeptId(Long companyId, Long deptId);

    /**
     * 统计用户数量
     *
     * @param companyId
     * @return
     */
    Long countByCompanyId(Long companyId);


    /**
     * 根据companyId和状态查询
     *
     * @param companyId
     * @return
     */
    Integer countByCompanyIdAndStatus(Long companyId,Integer status);

    /**
     * 统计用户数量
     *
     * @param deptId
     * @return
     */
    Long countByDeptId(Long deptId);

    /**
     * 查询用户信息
     *
     * @param userId
     * @return
     */
    EntityMap getAdminUserById(Long userId, String userType);


}
