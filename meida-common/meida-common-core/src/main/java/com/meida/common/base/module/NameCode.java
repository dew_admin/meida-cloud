package com.meida.common.base.module;

import lombok.Data;

import java.io.Serializable;

/**
 * @author flyme
 * @date 2019/10/21 20:16
 */
@Data
public class NameCode implements Serializable {
    private String label;
    private String value;
}
