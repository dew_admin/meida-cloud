package com.meida.common.mybatis.base.sql;

import com.meida.common.base.entity.EntityMap;

import java.util.List;

/**
 * @author zyf
 */
public interface SearchHandler {

    /**
     * 查询回调
     * @param pageNo
     * @return
     */
    List<EntityMap> handler(Integer pageNo);
}
