package com.meida.common.mybatis.model;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.meida.common.constants.ErrorCode;
import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * @author admin
 */
@ApiModel(value = "响应结果")
public class ResultBody<T> implements Serializable {
    private static final long serialVersionUID = -6190689122701100762L;

    /**
     * 响应编码
     */
    @ApiModelProperty(value = "响应编码:100-请求处理成功")
    private int code = ErrorCode.OK.getCode();
    /**
     * 提示消息
     */
    @ApiModelProperty(value = "提示消息")
    private String message;

    /**
     * 请求路径
     */
    @ApiModelProperty(value = "请求路径")
    private String path;

    /**
     * 响应数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;

    /**
     * http状态码
     */
    private int httpStatus;

    /**
     * 附加数据
     */
    @ApiModelProperty(value = "附加数据")
    private EntityMap extra = new EntityMap();

    /**
     * 响应时间
     */
    @ApiModelProperty(value = "响应时间")
    private long timestamp = System.currentTimeMillis();

    public ResultBody() {
        super();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @JsonIgnore
    public String getPath() {
        return path;
    }

    public T getData() {
        return data;
    }

    public EntityMap getExtra() {
        return extra;
    }

    public ResultBody setExtra(EntityMap map) {
        this.extra = map;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @JSONField(serialize = false, deserialize = false)
    @JsonIgnore
    public int getHttpStatus() {
        return httpStatus;
    }

    @JSONField(serialize = false, deserialize = false)
    @JsonIgnore
    public boolean isOk() {
        return this.code == ErrorCode.OK.getCode();
    }


    public static ResultBody ok() {
        return new ResultBody().code(ErrorCode.OK.getCode()).setMsg(ErrorCode.OK.getMessage());
    }

    public static ResultBody ok(Object object) {
        return new ResultBody().code(ErrorCode.OK.getCode()).data(object);
    }

    public static ResultBody msg(String msg) {
        return new ResultBody().code(ErrorCode.OK.getCode()).setMsg(msg);
    }


    public static ResultBody result(Boolean tag) {
        if (tag) {
            return ok("操作成功", true);
        } else {
            return failed("操作失败");
        }
    }

    /**
     * @param tag    成功标记
     * @param action 动作,如授权,退回,审批
     * @return
     */
    public static ResultBody result(String action, Boolean tag) {
        if (tag) {
            return ok(action + "成功", true);
        } else {
            return failed(action + "失败");
        }
    }

    /**
     * @param result 返回结果
     * @param action 动作,如授权,退回,审批
     * @return
     */
    public static ResultBody result(String action, Object result) {
        return ok(action + "成功", result);
    }

    public static ResultBody ok(String msg, Object data) {
        return new ResultBody().code(ErrorCode.OK.getCode()).setMsg(msg).data(data);
    }

    public static ResultBody result(Boolean result, Object data) {
        if (result) {
            return new ResultBody().code(ErrorCode.OK.getCode()).setMsg("操作成功").data(data);
        } else {
            return new ResultBody().code(ErrorCode.FAIL.getCode()).setMsg("操作失败").data(data);
        }
    }

    public static ResultBody failed() {
        return new ResultBody().code(ErrorCode.FAIL.getCode()).setMsg(ErrorCode.FAIL.getMessage());
    }

    public static ResultBody failed(String msg) {
        return new ResultBody().code(ErrorCode.FAIL.getCode()).setMsg(msg);
    }

    public static ResultBody failed(int code, String msg) {
        return new ResultBody().code(code).setMsg(msg);
    }

    public ResultBody code(int code) {
        this.code = code;
        return this;
    }

    public ResultBody setMsg(String message) {
        this.message = i18n(ErrorCode.getResultEnum(this.code).getMessage(), message);
        return this;
    }

    public ResultBody data(T data) {
        this.data = data;
        return this;
    }

    public ResultBody path(String path) {
        this.path = path;
        return this;
    }

    public ResultBody httpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
        return this;
    }

    public ResultBody put(String key, Object value) {
        if (this.extra == null) {
            this.extra = new EntityMap();
        }
        this.extra.put(key, value);
        return this;
    }

    @Override
    public String toString() {
        return "ResultBody{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                ", data=" + data +
                ", httpStatus=" + httpStatus +
                ", extra=" + extra +
                ", timestamp=" + timestamp +
                '}';
    }

    /**
     * 错误信息配置
     */
    @JSONField(serialize = false, deserialize = false)
    @JsonIgnore
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("error");

    /**
     * 提示信息国际化
     *
     * @param message
     * @param defaultMessage
     * @return
     */
    @JSONField(serialize = false, deserialize = false)
    @JsonIgnore
    private static String i18n(String message, String defaultMessage) {
        return resourceBundle.containsKey(message) ? resourceBundle.getString(message) : defaultMessage;
    }


}
