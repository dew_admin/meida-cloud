package com.meida.common.base.module;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator
 */
@ApiModel(value = "LoginParams", description = "登录参数描述")
@Data
public class LoginParams implements Serializable {
    @ApiModelProperty(value = "账户名", required = true)
    private String username;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty(value = "账户类型", required = true, notes = "test")
    private String accountType = "USERNAME";
    @ApiModelProperty(value = "handlerName")
    private String handlerName;
    @ApiModelProperty(value = "用户类型")
    private String userType = "USER";
    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "邀请码")
    private String inviter;

}
