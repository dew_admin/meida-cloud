package com.meida.common.base.listener;

import com.meida.common.base.entity.EntityMap;

/**
 * @author zyf
 */
public interface FlymeListener<T> {
    /**
     * 消息回调
     *
     * @param params
     * @return
     */
    T onMessage(EntityMap params);
}
