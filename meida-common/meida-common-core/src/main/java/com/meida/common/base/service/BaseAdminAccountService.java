package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Collection;

/**
 * 后台账户全局接口
 *
 * @author zyf
 */
public interface BaseAdminAccountService {

    /**
     * 查询后台账户
     *
     * @param accountName
     * @param accountType
     * @return
     */
    EntityMap getAccount(String accountName, String accountType);

    /**
     * 查询系统用户token列表
     * @param clientId
     * @return
     */
    Collection<OAuth2AccessToken> selectTokenList(String clientId);

}
