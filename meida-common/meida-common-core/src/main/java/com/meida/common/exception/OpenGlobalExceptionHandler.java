package com.meida.common.exception;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.ErrorCode;
import com.meida.common.mybatis.model.ResultBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.web.util.ThrowableAnalyzer;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 统一异常处理器
 *
 * @author LYD
 * @date 2017/7/3
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class OpenGlobalExceptionHandler {

    private static ThrowableAnalyzer throwableAnalyzer = new ThrowableAnalyzer();

    /**
     * 统一异常处理
     * AuthenticationException
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({AuthenticationException.class})
    public static ResultBody authenticationException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json; charset=utf-8");
        ResultBody resultBody = resolveException(ex, request.getRequestURI());
        response.setStatus(resultBody.getHttpStatus());
        return resultBody;
    }

    /**
     * OAuth2Exception
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({OAuth2Exception.class, InvalidTokenException.class})
    public static ResultBody oauth2Exception(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json; charset=utf-8");
        ResultBody resultBody = resolveException(ex, request.getRequestURI());
        response.setStatus(resultBody.getHttpStatus());
        return resultBody;
    }

    /**
     * 自定义异常
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({OpenException.class})
    public static ResultBody openException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json; charset=utf-8");
        ResultBody resultBody = resolveException(ex, request.getRequestURI());
        response.setStatus(resultBody.getHttpStatus());
        return resultBody;
    }


 /*   *//**
     * 自定义异常
     *
     * @param ex
     * @param request
     * @param response
     * @return
     *//*
    @ExceptionHandler({OpenRestResponseErrorHandler.class})
    public static ResultBody customException(OpenRestResponseErrorHandler ex, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json; charset=utf-8");
        String body = ex.getBody();
        Map<String, Object> map = JSONUtil.toBean(body, Map.class);
        ResultBody resultBody = ResultBody.failed(101, map.get("message").toString());
        return resultBody;
    }*/

    /**
     * 其他异常
     *
     * @param ex
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({Exception.class})
    public static ResultBody exception(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json; charset=utf-8");
        ResultBody resultBody = resolveException(ex, request.getRequestURI());
        response.setStatus(resultBody.getHttpStatus());
        return resultBody;
    }



    /**
     * 静态解析异常。可以直接调用
     *
     * @param ex
     * @return
     */
    public static ResultBody resolveException(Exception ex, String path) {
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        String message = FlymeUtils.getString(ex.getMessage(),"");
        ErrorCode code = ErrorCode.getResultEnum(message);
        Throwable[] causeChain = throwableAnalyzer.determineCauseChain(ex);
        Exception ase = (OAuth2Exception) throwableAnalyzer.getFirstThrowableOfType(OAuth2Exception.class, causeChain);

        //认证失败
        if (FlymeUtils.isNotEmpty(ase)) {
            if(code.equals(ErrorCode.ERROR)){
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
            return buildBody(ex, code, path, httpStatus);
        }

        ase = (AuthenticationException) throwableAnalyzer.getFirstThrowableOfType(AuthenticationException.class,
                causeChain);;
        if (ase != null) {
            httpStatus = HttpStatus.UNAUTHORIZED;
            return buildBody(ex, code, path, httpStatus);
        }
        //权限验证失败
        ase = (AccessDeniedException) throwableAnalyzer
                .getFirstThrowableOfType(AccessDeniedException.class, causeChain);

        if (ase != null && ase instanceof AccessDeniedException) {
            httpStatus = HttpStatus.FORBIDDEN;
            return buildBody(ex, code, path, httpStatus);
        }

        //自定义异常
        ase = (OpenException) throwableAnalyzer
                .getFirstThrowableOfType(OpenException.class, causeChain);

        if (ase != null && ase instanceof OpenException) {
            OpenException baseException = (OpenException) ex;
            code.setMessage(baseException.getMessage());
            return buildBody(ex, code, path, httpStatus);
        }

        //签名异常
        ase = (OpenSignatureException) throwableAnalyzer
                .getFirstThrowableOfType(OpenSignatureException.class, causeChain);
        if (ase != null && ase instanceof OpenSignatureException) {
            httpStatus = HttpStatus.BAD_REQUEST;
            code = ErrorCode.SIGNATURE_DENIED;
            return buildBody(ex, code, path, httpStatus);
        }
        return buildBody(ex, code, path, httpStatus);
    }

    /**
     * 构建返回结果对象
     *
     * @param exception
     * @return
     */
    private static ResultBody buildBody(Exception exception, ErrorCode errorCode, String path, HttpStatus httpStatus) {
        ResultBody resultBody = ResultBody.failed().code(errorCode.getCode()).setMsg(errorCode.getMessage()).path(path).httpStatus(httpStatus.value());
        log.error("==> 错误:{} exception: {}", resultBody, exception);
        return resultBody;
    }

}
