package com.meida.common.annotation;

import java.lang.annotation.*;

/**
 * @Author:zyf
 * @Date:2019-07-31 10:43
 * @Description: 按钮注解
 **/
@Documented
@Inherited
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {
    /**
     * 按钮名称
     */
    String title() default "";

    /**
     * 按钮code
     */
    String code() default "";

    /**
     * 是否是按钮
     *
     * @return
     */
    boolean isBtn() default true;


}
