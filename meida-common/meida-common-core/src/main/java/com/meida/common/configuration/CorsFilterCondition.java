package com.meida.common.configuration;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 跨域配置加载条件
 */
public class CorsFilterCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        //是否是单机模式
        Boolean object = context.getEnvironment().getProperty(CommonConstants.MEIDA_SINGLE, Boolean.class);
        if (FlymeUtils.isEmpty(object)) {
            return false;
        }
        return object;
    }
}
