package com.meida.common.base.service;

import java.util.List;

public interface BaseSysRoleService {

    /**
     * 获取用户角色ID列表
     *
     * @param userId
     * @return
     */
    List<Long> getRoleIds(Long userId);

    /**
     * 获取用户角色ID
     *
     * @param userId
     * @return
     */
    Long getRoleId(Long userId);


}
