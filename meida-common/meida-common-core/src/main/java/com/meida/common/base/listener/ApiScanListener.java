package com.meida.common.base.listener;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meida.common.annotation.Action;
import com.meida.common.annotation.LoginRequired;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.EncryptUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.common.utils.ReflectionUtils;
import com.meida.common.utils.StringUtils;
import com.meida.mq.adapter.MqTemplate;
import com.meida.starter.rabbitmq.client.RabbitMqClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 扫描接口
 *
 * @author zyf
 */
@Slf4j
public class ApiScanListener implements ApplicationListener<ApplicationReadyEvent> {
    private MqTemplate mqTemplate;
    private static final AntPathMatcher pathMatch = new AntPathMatcher();

    public ApiScanListener(MqTemplate mqTemplate) {
        this.mqTemplate = mqTemplate;
    }

    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Autowired
    private RedisUtils redisUtils;

    @Autowired(required = false)
    private RabbitMqClient rabbitMqClient;

    @Autowired
    public ApplicationContext applicationContext;

    /**
     * 初始化方法
     *
     * @param event
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ConfigurableApplicationContext applicationContext = event.getApplicationContext();
        Map<String, Object> resourceServer = applicationContext.getBeansWithAnnotation(EnableResourceServer.class);
        mqTemplate = applicationContext.getBean(MqTemplate.class);
        if (mqTemplate == null) {
            return;
        }
        if (resourceServer == null || resourceServer.isEmpty()) {
            // 只扫描资源服务器
            return;
        }
        Environment env = applicationContext.getEnvironment();
        // 服务名称
        String serviceId = env.getProperty("spring.application.name", "application");


        String apiCodeKey = serviceId + ":APIKEY";
        // 所有接口映射
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        // 获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        List<RequestMatcher> permitAll = Lists.newArrayList();
        try {
            // 获取所有安全配置适配器
            Map<String, WebSecurityConfigurerAdapter> securityConfigurerAdapterMap = applicationContext.getBeansOfType(WebSecurityConfigurerAdapter.class);
            Iterator<Map.Entry<String, WebSecurityConfigurerAdapter>> iterable = securityConfigurerAdapterMap.entrySet().iterator();
            while (iterable.hasNext()) {
                WebSecurityConfigurerAdapter configurer = iterable.next().getValue();
                HttpSecurity httpSecurity = (HttpSecurity) ReflectionUtils.getFieldValue(configurer, "http");
                FilterSecurityInterceptor filterSecurityInterceptor = httpSecurity.getSharedObject(FilterSecurityInterceptor.class);
                FilterInvocationSecurityMetadataSource metadataSource = filterSecurityInterceptor.getSecurityMetadataSource();
                Map<RequestMatcher, Collection<ConfigAttribute>> requestMap = (Map) ReflectionUtils.getFieldValue(metadataSource, "requestMap");
                Iterator<Map.Entry<RequestMatcher, Collection<ConfigAttribute>>> requestIterable = requestMap.entrySet().iterator();
                while (requestIterable.hasNext()) {
                    Map.Entry<RequestMatcher, Collection<ConfigAttribute>> match = requestIterable.next();
                    if (match.getValue().toString().contains("permitAll")) {
                        permitAll.add(match.getKey());
                    }
                }
            }
        } catch (Exception e) {
            log.error("error:{}", e);
        }
        List<Map<String, Object>> list = new ArrayList<>();
        Map<Object, Object> apiCodeMaps = redisUtils.getMapByProject(apiCodeKey);
        Map<Object, Object> newApiCodeMaps = new HashMap<>(16);
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            Class declaringClass = method.getMethod().getDeclaringClass();
            if (declaringClass.getAnnotation(Api.class) == null) {
                // 只扫描Api
                continue;
            }
            if (method.getMethodAnnotation(ApiIgnore.class) != null) {
                // 忽略的接口不扫描
                continue;
            }
            Set<MediaType> mediaTypeSet = info.getProducesCondition().getProducibleMediaTypes();
            for (MethodParameter params : method.getMethodParameters()) {
                if (params.hasParameterAnnotation(RequestBody.class)) {
                    mediaTypeSet.add(MediaType.APPLICATION_JSON_UTF8);
                    break;
                }
            }
            String mediaTypes = getMediaTypes(mediaTypeSet);
            // 请求类型
            RequestMethodsRequestCondition methodsCondition = info.getMethodsCondition();
            String methods = getMethods(methodsCondition.getMethods());
            // 请求路径
            PatternsRequestCondition p = info.getPatternsCondition();
            String urls = getUrls(p.getPatterns());
            Map<String, Object> api = Maps.newHashMap();
            // 类名
            String className = method.getMethod().getDeclaringClass().getName();
            // 方法名
            String methodName = method.getMethod().getName();
            String fullName = className + "." + methodName;
            String name = "";
            String desc = "";
            // 是否需要安全认证 默认:1-是 0-否
            Integer isAuth = 1;
            LoginRequired loginRequired = method.getMethodAnnotation(LoginRequired.class);
            /*// 匹配项目中.permitAll()配置
            for (String url : p.getPatterns()) {
                for (RequestMatcher requestMatcher : permitAll) {
                    if (requestMatcher instanceof AntPathRequestMatcher) {
                        AntPathRequestMatcher pathRequestMatcher = (AntPathRequestMatcher) requestMatcher;
                        if (pathMatch.match(pathRequestMatcher.getPattern(), url)) {
                            // 忽略验证
                            isAuth = "0";
                        }
                    }
                }
            }*/
            if (FlymeUtils.isNotEmpty(loginRequired) && !loginRequired.required()) {
                isAuth = 0;
            }
            ApiOperation apiOperation = method.getMethodAnnotation(ApiOperation.class);
            if (ObjectUtils.isNotEmpty(apiOperation)) {
                name = apiOperation.value();
                desc = apiOperation.notes();
            }
            Action action = method.getMethodAnnotation(Action.class);
            if (ObjectUtils.isNotEmpty(action)) {
                Api apiAnnotat = AnnotationUtils.getAnnotation(declaringClass, Api.class);
                api.put("menuName", apiAnnotat.value());
                api.put("actionName", action.title());
            }
            name = StringUtils.isBlank(name) ? methodName : name;
            // md5码
            String md5 = EncryptUtils.md5Hex(serviceId + name + desc);

            String apiKey = EncryptUtils.md5Hex(serviceId + methodName + urls);
            api.put("apiName", name);
            api.put("apiCode", apiKey);
            api.put("apiDesc", desc);
            api.put("path", urls);
            api.put("className", className);
            api.put("methodName", methodName);
            api.put("md5", apiKey);
            api.put("requestMethod", methods);
            api.put("serviceId", serviceId);
            api.put("contentType", mediaTypes);
            api.put("isAuth", isAuth);
            if (!apiCodeMaps.containsKey(apiKey) || !apiCodeMaps.containsValue(md5)) {
                //添加已修改的api
                list.add(api);
            }
            newApiCodeMaps.put(apiKey, md5);
        }
        List<String> result = new ArrayList(apiCodeMaps.keySet());
        List<String> result2 = new ArrayList(newApiCodeMaps.keySet());
        //计算无效的apiKey
        result.removeAll(result2);
        EntityMap resource = new EntityMap();
        resource.put("application", serviceId);
        resource.put("codes", result);
        resource.put("mapping", list);
        redisUtils.setProjectMap(apiCodeKey, newApiCodeMaps);
        log.info("扫描资源ApplicationReadyEvent:[{}]", serviceId);
        if (FlymeUtils.isNotEmpty(list) || FlymeUtils.isNotEmpty(result)) {
            //判断是否是微服务
            Object object = applicationContext.getEnvironment().getProperty(CommonConstants.CLOUD_SERVER_KEY);
            if (ObjectUtils.isNotEmpty(object)) {
                //rabbitMqClient.sendMessage(QueueConstants.QUEUE_SCAN_API_RESOURCE, resource, 3000);
            }
        }
    }


    private String getMediaTypes(Set<MediaType> mediaTypes) {
        StringBuilder sbf = new StringBuilder();
        for (MediaType mediaType : mediaTypes) {
            sbf.append(mediaType.toString()).append(",");
        }
        if (mediaTypes.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }

    private String getMethods(Set<RequestMethod> requestMethods) {
        StringBuilder sbf = new StringBuilder();
        for (RequestMethod requestMethod : requestMethods) {
            sbf.append(requestMethod.toString()).append(",");
        }
        if (requestMethods.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }

    private String getUrls(Set<String> urls) {
        StringBuilder sbf = new StringBuilder();
        for (String url : urls) {
            sbf.append(url).append(",");
        }
        if (urls.size() > 0) {
            sbf.deleteCharAt(sbf.length() - 1);
        }
        return sbf.toString();
    }


}
