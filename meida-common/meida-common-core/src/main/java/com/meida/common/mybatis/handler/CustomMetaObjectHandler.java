package com.meida.common.mybatis.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ReflectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;
import java.util.Optional;

/**
 * 自定义sql字段填充器,本类默认在default-config.properties中配置
 * <p>
 * 若实际项目中，字段名称不一样，可以新建一个此类，在yml配置中覆盖mybatis-plus.global-config.metaObject-handler配置即可
 * <p>
 * 注意默认获取的userId为空
 *
 * @author fengshuonan
 */
@Slf4j
public class CustomMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Object delFlag = getFieldValByName(getDeleteFlagFieldName(), metaObject);
        if (delFlag == null) {
            setFieldValByName(getDeleteFlagFieldName(), getDefaultDelFlagValue(), metaObject);
        }

        Object createTime = getFieldValByName(getCreateTimeFieldName(), metaObject);
        if (createTime == null) {
            setFieldValByName(getCreateTimeFieldName(), new Date(), metaObject);
        }

        Object createUser = getFieldValByName(getCreateUserFieldName(), metaObject);
        if (createUser == null) {
            //获取当前登录用户
            Long accountId = OpenHelper.getUserId();
            setFieldValByName(getCreateUserFieldName(), accountId, metaObject);
        }

        Object userId = getFieldValByName(getUserId(metaObject), metaObject);
        if (userId == null) {
            //获取当前登录用户ID
            setFieldValByName(getUserId(metaObject), OpenHelper.getUserId(), metaObject);
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName(getUpdateTimeFieldName(), new Date(), metaObject);

        Object updateUser = getFieldValByName(getUpdateUserFieldName(), metaObject);
        if (updateUser == null) {

            //获取当前登录用户
            Long accountId = OpenHelper.getUserId();
            setFieldValByName(getUpdateUserFieldName(), accountId, metaObject);
        }
    }

    /**
     * 获取逻辑删除字段的名称（非数据库中字段名称）
     */
    protected String getDeleteFlagFieldName() {
        return "deleted";
    }

    /**
     * 获取逻辑删除字段的默认值
     */
    protected Integer getDefaultDelFlagValue() {
        return 0;
    }

    /**
     * 获取创建时间字段的名称（非数据库中字段名称）
     */
    protected String getCreateTimeFieldName() {
        return "createTime";
    }


    /**
     * 获取创建人
     */
    protected String getUserId(MetaObject metaObject) {
        return Optional.ofNullable(ReflectionUtils.getUserId(metaObject.getOriginalObject().getClass())).orElse("userId");
    }


    /**
     * 获取创建用户字段的名称（非数据库中字段名称）
     */
    protected String getCreateUserFieldName() {
        return "createUser";
    }

    /**
     * 获取更新时间字段的名称（非数据库中字段名称）
     */
    protected String getUpdateTimeFieldName() {
        return "updateTime";
    }

    /**
     * 获取更新用户字段的名称（非数据库中字段名称）
     */
    protected String getUpdateUserFieldName() {
        return "updateUser";
    }


}