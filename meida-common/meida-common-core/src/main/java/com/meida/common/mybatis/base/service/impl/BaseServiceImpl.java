package com.meida.common.mybatis.base.service.impl;

import cn.afterturn.easypoi.entity.vo.MapExcelConstants;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import cn.afterturn.easypoi.view.PoiBaseView;
import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.module.ExportField;
import com.meida.common.base.module.MyExportParams;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.common.lock.RedissonLocker;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.base.sql.SearchHandler;
import com.meida.common.mybatis.base.sql.ThredQuery;
import com.meida.common.mybatis.interceptor.*;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.thread.IThreadPoolExecutorService;
import com.meida.common.utils.*;
import com.meida.mq.adapter.MqTemplate;
import com.meida.starter.rabbitmq.client.RabbitMqClient;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.usermodel.Workbook;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.cloud.bus.BusProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * @author: zyf
 * @date: 2018/12/24 12:49
 * @desc: 父类service
 */
public abstract class BaseServiceImpl<M extends SuperMapper<T>, T> extends ServiceImpl<M, T> implements IBaseService<T> {

    @Autowired
    public ApplicationContext applicationContext;
    @Autowired
    public RedisUtils redisUtils;
    @Resource
    public AutowireCapableBeanFactory spring;

    @Autowired
    public RedissonLocker redisLock;

    @Resource
    public SqlSessionTemplate sqlSession;

    /**
     *
     */
    @Autowired
    public ApplicationEventPublisher publisher;

    @Autowired
    public BusProperties busProperties;

    @Autowired(required = false)
    public MqTemplate mqTemplate;

    @Autowired(required = false)
    public RabbitMqClient rabbitMqClient;


    public Class<T> entityClass = null;


    @Autowired
    private IThreadPoolExecutorService threadPoolExecutorService;


    /**
     * 事务管理器
     */
    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    /**
     * 事务的一些基础信息如超时时间、隔离级别、传播属性等
     */
    @Autowired
    private TransactionDefinition transactionDefinition;
    private ResultBody resultBody;


    public void pushEvent(ApplicationEvent applicationEvent) {
        applicationContext.publishEvent(applicationEvent);
    }

    public BaseServiceImpl() {
        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
            Type[] p = ((ParameterizedType) t).getActualTypeArguments();
            entityClass = (Class<T>) p[1];
        }
    }

    /**
     * 自定分页方法,可通过自定义处理器扩展
     */
    public ResultBody basePageList(CriteriaQuery<?> cq) {
        ResultBody resultBody = new ResultBody();
        EntityMap extra = resultBody.getExtra();
        Boolean hanHandler = false;
        //扩展事件
        PageInterceptor pageInterceptor = SpringContextHolder.getHandler(cq.getHandlerName(), PageInterceptor.class);
        if (ObjectUtils.isNotEmpty(pageInterceptor)) {
            hanHandler = true;
            //执行校验
            resultBody = pageInterceptor.validate(cq, cq.getRequestMap());
        }
        if (resultBody.isOk()) {
            if (hanHandler) {
                pageInterceptor.prepare(cq, cq.getPageParams(), cq.getRequestMap());
            }
            PageParams pageParams = cq.getPageParams();

            IPage<EntityMap> result = this.baseMapper.pageList(new Page(pageParams.getCurrent(), pageParams.getSize(),pageParams.searchCount()), cq);
            if (ObjectUtils.isNotNull(result)) {
                //扩展查询结果集
                if (hanHandler) {
                    pageInterceptor.complete(cq, result.getRecords(), extra);
                    //pageInterceptor.complete(cq, result, extra);
                }
            }
            if (pageParams.getSimpleData().equals(CommonConstants.INT_1)) {
                resultBody = resultBody.data(result).setExtra(extra);
            } else {
                EntityMap data = new EntityMap();
                data.put("records", result.getRecords());
                if (FlymeUtils.isNotEmpty(extra)) {
                    data.put("extra", extra);
                }
                resultBody = resultBody.data(data);
            }
            String exportKey = cq.getExportKey();
            if (FlymeUtils.isNotEmpty(exportKey)) {
                String obj = redisUtils.getString(exportKey);
                if (FlymeUtils.isEmpty(obj)) {
                    Thread t = new Thread(() -> {
                        initExportData(exportKey, result.getPages(), result.getSize(), cq.getExportParams());
                    });
                    t.start();
                }
            }
        }
        return resultBody;
    }

    /**
     * selectEntityMap方法带扩展事件
     */
    public ResultBody baseGet(CriteriaQuery<?> cq) {
        EntityMap result = new EntityMap();
        boolean hasHandler = false;
        ResultBody resultBody = new ResultBody();
        //获取扩展事件名称
        GetInterceptor getInterceptor = SpringContextHolder.getHandler(cq.getHandlerName(), GetInterceptor.class);
        //扩展查询条件
        if (FlymeUtils.isNotEmpty(getInterceptor)) {
            hasHandler = true;
            resultBody = getInterceptor.validate(cq, cq.getRequestMap());
        }
        if (resultBody.isOk()) {
            if (hasHandler) {
                getInterceptor.prepare(cq, cq.getRequestMap());
            }
            String condition = cq.getCustomSqlSegment();
            if (FlymeUtils.isEmpty(condition)) {
                ApiAssert.failure("查询条件不存在");
            }
            List<EntityMap> maps = baseMapper.selectEntityMap(cq);
            if (ObjectUtils.isNotNull(maps)) {
                result = maps.get(0);
                //扩展查询结果集
                if (hasHandler) {
                    getInterceptor.complete(cq, result);
                }
            }
        }
        return resultBody.data(result);
    }


    /**
     * 查询单个实体
     *
     * @param cq
     * @return
     */
    @Override
    public EntityMap findOne(CriteriaQuery cq) {
        EntityMap result = new EntityMap();
        List<EntityMap> maps = selectEntityMap(cq);
        if (ObjectUtils.isNotNull(maps)) {
            if (maps.size() > 1) {
                ApiAssert.failure("数据异常");
            } else {
                return maps.get(0);
            }
        }
        return result;
    }

    /**
     * 根据单个属性查询
     *
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public T getByProperty(String propertyName, Object value) {
        CriteriaQuery cq = new CriteriaQuery(entityClass);
        cq.eq(true, propertyName, value);
        cq.limit(1);
        return (T) getOne(cq);
    }

    /**
     * 查询单个实体
     *
     * @param id
     * @return
     */
    @Override
    public EntityMap findById(Serializable id) {
        ApiAssert.isNotEmpty("ID不能为空", id);
        EntityMap result = new EntityMap();
        CriteriaQuery cq = new CriteriaQuery(entityClass);
        cq.eq(true, cq.getIdField(), id);
        return findOne(cq);
    }

    public ResultBody baseEdit(CriteriaUpdate cu) {
        T t = (T) cu.getEntity(entityClass);
        return baseEdit(cu, t);
    }

    /**
     * 自定义更新
     */
    public ResultBody baseEdit(CriteriaUpdate cu, T obj) {
        boolean isUpdate;
        //获取扩展事件
        UpdateInterceptor handler = SpringContextHolder.getHandler(cu.getHandlerName(), UpdateInterceptor.class);
        //更新前验证
        BaseMapper mapper = null;
        if (ObjectUtils.isNotNull(handler)) {
            ResultBody resultBody = handler.validate(cu, cu.getRequestMap());
            if (!resultBody.isOk()) {
                return resultBody;
            }
            handler.prepare(cu, cu.getRequestMap(), obj);
            //获取自定义mapper
            mapper = handler.getMapper();
        }
        if (FlymeUtils.isNotEmpty(mapper)) {
            obj = (T) handler.getEntity(cu, cu.getRequestMap());
            isUpdate = handler.getMapper().update(obj, cu) > 0;
        } else {
            Long idValue = cu.getIdValue();
            ApiAssert.isNotEmpty("主键不能为空", idValue);
            isUpdate = saveOrUpdate(obj, cu);
        }

        if (!isUpdate) {
            return ResultBody.failed("更新失败");
        }
        //更新成功后调用扩展事件
        if (FlymeUtils.isNotEmpty(handler)) {
            handler.complete(cu, cu.getRequestMap(), obj);
        }

        Long[] fileIds = new Long[0];
        Long[] removeIds = new Long[0];
        fileIds = FlymeUtils.StringToLongArray(cu.getParams("fileIds"));
        removeIds = FlymeUtils.StringToLongArray(cu.getParams("removeIds"));
        //关联文件对应业务ID
        saveFile(fileIds, removeIds, obj, cu.getIdField(), cu.getCls().getSimpleName());
        return ResultBody.ok("更新成功", obj);
    }

    /**
     * 自定义更新局部字段
     */
    public ResultBody baseUpdate(CriteriaUpdate cu) {
        boolean isUpdate;
        //获取扩展事件
        UpdateInterceptor handler = SpringContextHolder.getHandler(cu.getHandlerName(), UpdateInterceptor.class);
        //更新前验证
        if (ObjectUtils.isNotNull(handler)) {
            ResultBody resultBody = handler.validate(cu, cu.getRequestMap());
            if (!resultBody.isOk()) {
                return resultBody;
            }
            handler.prepare(cu, cu.getRequestMap(), null);
        }
        isUpdate = update(cu);
        if (!isUpdate) {
            return ResultBody.failed("更新失败");
        }
        //更新成功后调用扩展事件
        if (FlymeUtils.isNotEmpty(handler)) {
            handler.complete(cu, cu.getRequestMap(),null);
        }
        return ResultBody.ok("更新成功");
    }

    /**
     * 自定义删除
     *
     * @param cd
     * @return
     */
    public ResultBody baseDelete(CriteriaDelete cd) {
        //自定义扩展事件
        DeleteInterceptor handler = SpringContextHolder.getHandler(cd.getHandlerName(), DeleteInterceptor.class);
        if (FlymeUtils.isNotEmpty(handler)) {
            //执行校验
            ResultBody resultBody = handler.validate(cd, cd.getRequestMap());
            //验证失败直接返回
            if (!resultBody.isOk()) {
                return resultBody;
            }
            //执行条件预处理扩展
            handler.prepare(cd, cd.getRequestMap());
        }
        //检查是否有子节点
        checkHasChild(cd.getIdValue());
        //追加是否允许删除条件
        checkAllowDel(cd);
        ApiAssert.isNotEmpty("删除条件不存在", cd.getCustomSqlSegment());
        int n;
        if (cd.getLogicalDelete()) {
            n = baseMapper.delete(cd);
        } else {
            //强制删除
            n = baseMapper.deleteByCq(cd);
        }
        if (n <= 0) {
            return ResultBody.failed("删除失败");
        }
        if (FlymeUtils.isNotEmpty(handler)) {
            handler.complete(cd, cd.getRequestMap());
        }
        if (cd.getDelFile()) {
            Long[] fileIds = new Long[0];
            fileIds = cd.getRequestMap().getLongs("fileIds");
            delFiles(fileIds, cd.getCls().getSimpleName());
        }
        return ResultBody.ok("删除成功", n);

    }

    public ResultBody baseAdd(CriteriaSave cs) {
        return baseAdd(cs, cs.getEntity(entityClass));
    }


    /**
     * save方法带扩展事件
     */
    public ResultBody baseAdd(CriteriaSave cs, T obj) {
        SaveInterceptor handler = SpringContextHolder.getHandler(cs.getHandlerName(), SaveInterceptor.class);
        ResultBody resultBody = new ResultBody();
        //是否支持扩展条件
        boolean hasHandler = false;
        Boolean isSave = false;
        //获取自定义mapper
        BaseMapper mapper = null;
        if (ObjectUtils.isNotNull(handler)) {
            hasHandler = true;
            resultBody = handler.validate(cs, cs.getRequestMap());
            if (!resultBody.isOk()) {
                return resultBody;
            }
            handler.prepare(cs, cs.getRequestMap(), obj);
            mapper = handler.getMapper();
        }
        if (FlymeUtils.isNotEmpty(mapper)) {
            obj = (T) handler.getEntity(cs, cs.getRequestMap());
            isSave = handler.getMapper().insert(obj) > 0;
        } else {

            isSave = this.save(obj);
        }
        if (isSave) {
            resultBody = resultBody.ok("保存成功", obj);
            if (hasHandler) {
                //保存成功后调用扩展事件
                handler.complete(cs, cs.getRequestMap(), obj);
            }

            Long[] fileIds = new Long[0];
            Long[] removeIds = new Long[0];

            fileIds = FlymeUtils.StringToLongArray(cs.getParams("fileIds"));
            removeIds = FlymeUtils.StringToLongArray(cs.getParams("removeIds"));

            //上传文件
            saveFile(fileIds, removeIds, obj, cs.getIdField(), cs.getCls().getSimpleName());
        }
        return resultBody.ok("保存成功", obj);

    }

    /**
     * List<EntityMap>
     *
     * @param cq
     * @return
     */
    @Override
    public List<EntityMap> selectEntityMap(CriteriaQuery<?> cq) {
        ApiAssert.isNotEmpty("服务器异常", cq.getCls());
        return baseMapper.selectEntityMap(cq);
    }


    /**
     * 根据条件构造器查询List<EntityMap>
     */
    public ResultBody baseList(CriteriaQuery<?> cq) {
        ResultBody resultBody = new ResultBody();
        EntityMap extra = resultBody.getExtra();
        Boolean hanHandler = false;
        //获取扩展事件名称
        PageInterceptor handler = SpringContextHolder.getHandler(cq.getHandlerName(), PageInterceptor.class);
        if (ObjectUtils.isNotEmpty(handler)) {
            hanHandler = true;
            //执行校验
            resultBody = handler.validate(cq, cq.getRequestMap());
        }
        if (resultBody.isOk()) {
            //扩展查询条件
            if (hanHandler) {
                handler.prepare(cq, cq.getPageParams(), cq.getRequestMap());
            }
            List<EntityMap> result = baseMapper.selectEntityMap(cq);
            //扩展查询结果集
            if (hanHandler) {
                handler.complete(cq, result, extra);
            }
            resultBody = resultBody.data(result).setExtra(extra);
        }
        return resultBody;
    }

    /**
     * 自定义sql查询List<EntityMap>
     */
    public List<EntityMap> listEntityMap(String statement, EntityMap map) {
        if (ObjectUtils.isEmpty(map)) {
            return null;
        }
        return sqlSession.selectList(getMapperName() + statement, map);
    }

    /**
     * 自定义sql查询List<EntityMap>
     */
    public List<EntityMap> listEntityMap(String statement, @Param("ew") CriteriaQuery<?> cq) {
        return sqlSession.selectList(getMapperName() + statement, cq);
    }


    /**
     * 获取mapperName
     */
    private String getMapperName() {
        String mapperName = "";
        Class cl = baseMapper.getClass();
        Class<?> interfaces[] = cl.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            mapperName = anInterface.getName();
        }
        return mapperName + ".";
    }

    public List<T> selectAll() {
        return this.baseMapper.selectList(null);
    }


    /**
     * 自定义分页前扩展方法
     *
     * @param cq
     * @return
     */
    public ResultBody beforeListEntityMap(CriteriaQuery<T> cq, T t, EntityMap requestMap) {
        return ResultBody.ok();
    }


    /**
     * 自定义分页后扩展方法
     *
     * @param cq
     * @return
     */
    public ResultBody afterListEntityMap(CriteriaQuery<T> cq, List<EntityMap> data, ResultBody resultBody) {
        return resultBody;
    }


    @Override
    public ResultBody listEntityMap(Map map) {
        CriteriaQuery<T> cq = new CriteriaQuery(map, entityClass, false);
        ResultBody resultBody = beforeListEntityMap(cq, cq.getEntity(entityClass), cq.getRequestMap());
        if (resultBody.isOk()) {
            if (FlymeUtils.isEmpty(cq.getSetSelect())) {
                cq.select(entityClass, "*");
            }
            resultBody = baseList(cq);
            if (resultBody.isOk()) {
                List<EntityMap> data = (List<EntityMap>) resultBody.getData();
                resultBody = afterListEntityMap(cq, data, resultBody);
            }
        }
        return resultBody;
    }


    @Override
    public ResultBody pageList(Map map) {
        CriteriaQuery<T> cq = new CriteriaQuery(map, entityClass, false);
        ResultBody resultBody = beforePageList(cq, cq.getEntity(entityClass), cq.getRequestMap());
        if (resultBody.isOk()) {
            if (FlymeUtils.isEmpty(cq.getSetSelect())) {
                cq.select(entityClass, "*");
            }
            resultBody = basePageList(cq);
            if (resultBody.isOk()) {
                PageParams pageParams = cq.getPageParams();
                Object result = resultBody.getData();
                if (pageParams.getSimpleData().equals(CommonConstants.INT_1)) {
                    IPage<EntityMap> list = (IPage<EntityMap>) result;
                    EntityMap pageModel = new EntityMap();
                    List<EntityMap> records = list.getRecords();
                    pageModel.put("size", list.getSize());
                    pageModel.put("total", list.getTotal());
                    pageModel.put("current", list.getCurrent());
                    pageModel.put("pages", list.getPages());
                    records = afterPageList(cq, records, resultBody);
                    pageModel.put("records", FlymeUtils.isNotEmpty(records) ? records : new ArrayList<>());
                    if (FlymeUtils.isNotEmpty(resultBody.getExtra())) {
                        pageModel.put("extra", resultBody.getExtra());
                    }
                    resultBody.setExtra(null);
                    list.setRecords(records);
                    resultBody.data(pageModel);
                } else {
                    EntityMap resultMap = (EntityMap) result;
                    List<EntityMap> records = resultMap.get("records");
                    result = afterPageList(cq, records, resultBody);
                    resultBody.data(result);
                }
            }
        }

        return resultBody;
    }


    /**
     * 自定义分页前扩展方法
     *
     * @param cq
     * @return
     */
    public ResultBody beforePageList(CriteriaQuery<T> cq, T t, EntityMap requestMap) {
        return ResultBody.ok();
    }


    /**
     * 自定义分页后置方法
     *
     * @param cq
     * @param data
     * @param resultBody
     */
    public List<EntityMap> afterPageList(CriteriaQuery<T> cq, List<EntityMap> data, ResultBody resultBody) {
        return data;
    }


    /**
     * 自定义保存前置方法
     *
     * @param cs
     * @param t
     * @param extra
     * @return
     */
    public ResultBody beforeAdd(CriteriaSave cs, T t, EntityMap extra) {
        return ResultBody.ok();

    }


    /**
     * 自定义保存后置方法
     *
     * @param cs
     * @param extra
     * @return
     */
    public ResultBody afterAdd(CriteriaSave cs, T t, EntityMap extra) {

        return ResultBody.ok("保存成功", t);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultBody add(Map map) {
        CriteriaSave cs = new CriteriaSave(map, entityClass);
        T obj = cs.getEntity(entityClass);
        EntityMap extra = new EntityMap();
        ApiAssert.isNotEmpty("保存对象不能为空", obj);
        ResultBody resultBody = beforeAdd(cs, obj, extra);
        if (resultBody.isOk()) {
            resultBody = baseAdd(cs, obj);
            if (resultBody.isOk()) {
                T t = (T) resultBody.getData();
                resultBody = afterAdd(cs, t, extra);
            }
        }
        return resultBody;
    }


    /**
     * 自定义更新前扩展方法
     *
     * @param cu
     * @return
     */
    public ResultBody beforeEdit(CriteriaUpdate<T> cu, T t, EntityMap extra) {
        return ResultBody.ok();
    }


    /**
     * 自定义更新后扩展方法
     *
     * @param cu
     * @return
     */
    public ResultBody afterEdit(CriteriaUpdate cu, T t, EntityMap extra) {
        return ResultBody.ok("更新成功", t);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultBody edit(Map map) {
        CriteriaUpdate cu = new CriteriaUpdate(map, entityClass);
        T obj = (T) cu.getEntity(entityClass);
        EntityMap extra = new EntityMap();
        ResultBody resultBody = beforeEdit(cu, obj, extra);
        if (resultBody.isOk()) {
            resultBody = baseEdit(cu, obj);
            if (resultBody.isOk()) {
                T t = (T) resultBody.getData();
                resultBody = afterEdit(cu, t, extra);
            }
        }
        return resultBody;
    }


    /**
     * 自定义删除前扩展方法
     *
     * @param cd
     * @return
     */
    public ResultBody beforeDelete(CriteriaDelete<T> cd) {
        return ResultBody.ok();
    }


    /**
     * 自定义删除后扩展方法
     *
     * @param cd
     * @return
     */
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        return ResultBody.msg("删除成功");
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultBody delete(Map map) {
        CriteriaDelete cd = new CriteriaDelete(map, entityClass);
        ResultBody resultBody = beforeDelete(cd);
        if (resultBody.isOk()) {
            resultBody = baseDelete(cd);
            if (resultBody.isOk()) {
                resultBody = afterDelete(cd, cd.getIds());
            }


        }
        return resultBody;
    }


    /**
     * 自定义get扩展方法
     *
     * @param cq
     * @return
     */
    public ResultBody beforeGet(CriteriaQuery<T> cq, T t, EntityMap requestMap) {
        return ResultBody.ok();
    }


    /**
     * 自定义get扩展方法
     *
     * @param cq
     * @return
     */
    public void afterGet(CriteriaQuery cq, EntityMap result) {

    }


    @Override
    public ResultBody get(Map map) {
        CriteriaQuery<T> cq = new CriteriaQuery(map, entityClass);
        ResultBody resultBody = beforeGet(cq, cq.getEntity(entityClass), cq.getRequestMap());
        if (resultBody.isOk()) {
            resultBody = baseGet(cq);
            if (resultBody.isOk()) {
                EntityMap result = (EntityMap) resultBody.getData();
                afterGet(cq, result);
                resultBody.data(result);
            }
        }
        return resultBody;
    }

    /**
     * 更新文件业务ID
     *
     * @param fileIds
     * @param obj
     * @param idField
     * @param entityEname
     */
    private void saveFile(Long[] fileIds, Long[] removeIds, Object obj, String idField, String entityEname) {
        if (FlymeUtils.anyOneIsNotNull(fileIds, removeIds)) {
            Long busId = (Long) ReflectionUtils.getFieldValue(obj, idField);
            EntityMap map = new EntityMap();
            map.put("busId", busId);
            map.put("entityName", entityEname);
            map.put("fileIds", fileIds);
            map.put("removeIds", removeIds);
            //发布收藏点击事件
            mqTemplate.convertAndSend(QueueConstants.QUEUE_UPLOADFILE, map);
        }
    }

    /**
     * 删除业务文件
     *
     * @param busIds
     * @param entityEname
     */
    private void delFiles(Long[] busIds, String entityEname) {
        if (FlymeUtils.isNotEmpty(busIds)) {
            EntityMap map = new EntityMap();
            map.put("busIds", busIds);
            map.put("entityName", entityEname);
            //发布收藏点击事件
            mqTemplate.convertAndSend(QueueConstants.QUEUE_DELFILE, map);
        }
    }

    /**
     * 自定义状态更新前扩展
     *
     * @param cq
     * @return
     */
    public ResultBody beforeSetState(CriteriaUpdate cq) {
        return ResultBody.ok();
    }


    /**
     * 自定义状态更新完成扩展
     *
     * @param cq
     * @return
     */
    public ResultBody afterSetState(CriteriaUpdate cq) {
        return ResultBody.ok();
    }

    /**
     * 根据主键通用状态更新方法
     *
     * @param map
     * @param state
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultBody setState(Map map, String fieldName, Integer state) {
        CriteriaUpdate cq = new CriteriaUpdate(map, entityClass);
        ApiAssert.isNotEmpty("主键不能为空", cq.getIds());
        cq.set(fieldName, state);
        ResultBody resultBody = beforeSetState(cq);
        if (resultBody.isOk()) {
            ResultBody result = baseUpdate(cq);
            if (result.isOk()) {
                ResultBody after = afterSetState(cq);
                String text = redisUtils.getStateFieldLabel(fieldName, state.toString());
                if (FlymeUtils.isNotEmpty(after.getMessage())) {
                    return ResultBody.ok(after.getMessage(), state);
                } else {
                    return ResultBody.result(text, state);
                }

            }
        }
        return ResultBody.failed("操作失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultBody setRecommend(Long[] ids, Integer recommend) {
        Map<String, Object> map = new HashMap<>();
        map.put("ids", ids);
        CriteriaUpdate cq = new CriteriaUpdate(map, entityClass);
        ApiAssert.isNotEmpty("主键不能为空", ids);
        String idField = cq.getIdField();
        UpdateWrapper uw = new UpdateWrapper();
        uw.set("recommend", recommend);
        uw.in(idField, ids);
        boolean result = update(uw);
        return ResultBody.result(result, recommend);
    }

    /**
     * 查询数组
     *
     * @param cq
     * @return
     */
    public List<Long> selectLongs(CriteriaQuery cq) {
        return listObjs(cq, e -> new Long(e.toString()));
    }

    /**
     * 检查是否有子节点
     */
    private ResultBody checkHasChild(Long id) {
        String parentId = ReflectionUtils.getParentId(entityClass);
        if (FlymeUtils.isNotEmpty(parentId)) {
            CriteriaQuery cq = new CriteriaQuery(entityClass);
            Long n = count(cq.eq("parentId", id));
            if (n > 0) {
                ApiAssert.failure("请先删除子节点");
            }
        }
        return ResultBody.ok();
    }

    /**
     * 检查是否允许删除
     */
    private ResultBody checkAllowDel(CriteriaDelete cd) {
        String allowDel = ReflectionUtils.getAllowDelField(entityClass);
        if (FlymeUtils.isNotEmpty(allowDel)) {
            cd.eq("allowDel", CommonConstants.ENABLED);
        }
        return ResultBody.ok();
    }

    public String getProject() {
        Environment env = applicationContext.getEnvironment();
        String project = FlymeUtils.getString(env.getProperty("spring.application.project", "application"), "meida");
        return project;
    }

    private void initExportData(String key, long pages, long size, ConcurrentHashMap paramsMap) {
        redisUtils.set(key, "exporting");
        long start = System.currentTimeMillis();
        //添加任务
        List<Callable<List<EntityMap>>> tasks = new CopyOnWriteArrayList<Callable<List<EntityMap>>>();
        for (int i = 1; i <= pages; i++) {
            Callable<List<EntityMap>> qfe = new ThredQuery(i, new SearchHandler() {
                @Override
                public List<EntityMap> handler(Integer pageNo) {
                    ConcurrentHashMap searchParmas = new ConcurrentHashMap(paramsMap);
                    searchParmas.put("page", pageNo);
                    searchParmas.put("limit", size);
                    ResultBody resultBody = pageList(searchParmas);
                    EntityMap data = (EntityMap) resultBody.getData();
                    List<EntityMap> records = data.get("records");
                    return records;
                }
            });
            tasks.add(qfe);
        }
        //定义固定长度的线程池  防止线程过多
        ExecutorService execservice = Executors.newFixedThreadPool(30);
        //返回结果
        List<EntityMap> result = new ArrayList();
        List<Future<List<EntityMap>>> futures = Collections.synchronizedList(new ArrayList());
        try {
            futures = execservice.invokeAll(tasks);
            // 处理线程返回结果
            if (futures != null && futures.size() > 0) {
                for (Future<List<EntityMap>> future : futures) {
                    List<EntityMap> list = future.get();
                    if (FlymeUtils.isNotEmpty(list)) {
                        result.addAll(list);
                    }
                }
            }
            redisUtils.set(key, result, 86400);
        } catch (Exception e) {
            execservice.shutdown();
            e.printStackTrace();
        }
        // 关闭线程池
        execservice.shutdown();
        long end = System.currentTimeMillis();
        System.out.println("用时" + (start - end));

    }


    @Override
    public String export(ModelMap modelMap, Map params, HttpServletRequest request, HttpServletResponse response, String... config) {
        long start = System.currentTimeMillis();
        String fileName = config[0];
        String title = config[1];
        String handlerName = "";
        String sheetName = "";
        if (config.length == 3) {
            handlerName = config[2];
        }
        if (config.length == 4) {
            sheetName = config[3];
        }
        //获取导出列
        String columns = params.get("columns").toString();
        boolean hasHandler = false;
        String serverPath = "";
        //获取扩展事件名称
        ExportInterceptor handler = SpringContextHolder.getHandler(handlerName, ExportInterceptor.class);
        if (FlymeUtils.isNotEmpty(handler)) {
            hasHandler = true;
            serverPath = handler.serverPath(params);
        }
        MyExportParams exportParams = new MyExportParams(fileName, title, sheetName, ExcelType.XSSF);
        if (hasHandler) {
            //初始化导出参数
            handler.initExportParams(exportParams);
        }
        List<ExcelExportEntity> entityList = new ArrayList<>();
        if (FlymeUtils.isNotEmpty(columns)) {
            List<ExportField> exportFieldList = JsonUtils.toList(columns, ExportField.class);
            for (ExportField exportField : exportFieldList) {
                if (hasHandler) {
                    //初始化导出字段配置
                    handler.initExcelExportEntity(exportField, entityList);
                }
                entityList.add(exportField);
            }
        } else {
            if (hasHandler) {
                handler.initExcelExportEntity(null, entityList);
            }
        }
        List<EntityMap> list = new ArrayList<>();
        //初始化导出参数
        ResultBody resultBody = new ResultBody();
        if (hasHandler) {
            //初始化导出数据
            resultBody = handler.initData(params);
        }
        if(!resultBody.isOk()){
            return "data is empty";
        }
        Object key = params.get("exportKey");
        if (FlymeUtils.isEmpty(resultBody.getData())) {

            if (FlymeUtils.isNotEmpty(key)) {
                Object data = redisUtils.get(key.toString());
                if (FlymeUtils.isNotEmpty(data) && !data.equals("exporting")) {
                    resultBody.data(data);
                } else {
                    resultBody.data(new ArrayList<>());
                    if(FlymeUtils.isEmpty(data)){
                        fileName = "请先执行查询数据";
                    }
                    if(FlymeUtils.isNotEmpty(data)&& data.equals("exporting")){
                        fileName = "数据正在准备中请稍后在试";
                    }
                    return fileName;
                }
            } else {
                resultBody = listEntityMap(params);
            }
        }
        Object object = resultBody.getData();
        if (object instanceof Page) {
            IPage resultMap = (IPage) resultBody.getData();
            list = resultMap.getRecords();
        } else {
            list = (List<EntityMap>) object;
        }
        if (FlymeUtils.isEmpty(serverPath)) {
            try {
                if (FlymeUtils.isEmpty(modelMap)) {
                    Map<String, Object> model = new HashMap<>();
                    model.put(MapExcelConstants.MAP_LIST, list);
                    model.put(MapExcelConstants.ENTITY_LIST, entityList);
                    model.put(MapExcelConstants.PARAMS, exportParams);
                    model.put(MapExcelConstants.FILE_NAME, fileName);
                    PoiBaseView.render(model, request, response, MapExcelConstants.EASYPOI_MAP_EXCEL_VIEW);
                    if(FlymeUtils.isNotEmpty(key)) {
                        redisUtils.del(key.toString());
                    }
                } else {
                    modelMap.put(MapExcelConstants.MAP_LIST, list);
                    modelMap.put(MapExcelConstants.ENTITY_LIST, entityList);
                    modelMap.put(MapExcelConstants.PARAMS, exportParams);
                    modelMap.put(MapExcelConstants.FILE_NAME, fileName);
                }

            } catch (Exception e) {
            }
            return MapExcelConstants.EASYPOI_MAP_EXCEL_VIEW;
        } else {
            String fileKey = fileName + ".xls";
            try {
                String exportName = serverPath + fileKey;
                if(FlymeUtils.isNotEmpty(list)){
                    Workbook workbook = ExcelExportUtil.exportExcel(exportParams, entityList, list);
                    String basePath=serverPath;
                    FileOutputStream fos = new FileOutputStream(exportName);
                    workbook.write(fos);
                    fos.close();
                    if (FlymeUtils.isNotEmpty(handler)) {
                        handler.exportSuccess(params,exportName,fileKey);
                    }
                }
                return exportName;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }
    }

    @Override
    public void templateExport(Map params, HttpServletRequest request, HttpServletResponse response, String... config) {
        String fileName = config[0] + ".xlsx";
        String handlerName = "";
        if (config.length == 3) {
            handlerName = config[2];
        }
        boolean hasHandler = false;
        //获取扩展事件名称
        ExportInterceptor handler = SpringContextHolder.getHandler(handlerName, ExportInterceptor.class);
        if (FlymeUtils.isNotEmpty(handler)) {
            hasHandler = true;
        }
        if (hasHandler) {
            TemplateExportParams templateExportParams = handler.initTemplateExportParams();
            Map dataMap = handler.initTemplateDataMap();
            //初始化导出数据
            ResultBody resultBody = handler.initData(params);
            if (FlymeUtils.isEmpty(resultBody.getData())) {
                Object key = params.get("exportKey");
                if (FlymeUtils.isNotEmpty(key)) {
                    Object data = redisUtils.get(key.toString());
                    if (FlymeUtils.isNotEmpty(data) && !data.equals("exporting")) {
                        resultBody.data(data);
                    } else {
                        resultBody.data(new ArrayList<>());
                        fileName = "数据正在准备中请稍后在试";
                    }
                } else {
                    resultBody = listEntityMap(params);
                }
            }
            List<EntityMap> list = new ArrayList<>();
            Object object = resultBody.getData();
            if (object instanceof Page) {
                IPage resultMap = (IPage) resultBody.getData();
                list = resultMap.getRecords();
            } else {
                list = (List<EntityMap>) object;
            }
            try {
                dataMap.put("list", list);
                handler.initExportData(dataMap, list);
                Workbook workbook = ExcelExportUtil.exportExcel(templateExportParams, dataMap);
                response.reset();
                response.setContentType("application/octet-stream;charset=UTF-8");
                fileName = URLEncoder.encode(fileName, "UTF-8");
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
                OutputStream fos = response.getOutputStream();
                workbook.write(fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Boolean mutiInsert(List<?> objList) {
        if (null == objList || objList.size() == 0) {
            return false;
        }
        // 获取表名
        Object obj = objList.get(0);
        String tableName = AnnotationUtil.getAnnotationValue(obj.getClass(), TableName.class);
        if (StrUtil.isEmpty(tableName)) {
            return false;
        }
        Integer count = 1000;
        for (int i = 0; i < objList.size(); i = i + count) {
            List<?> tempList = ListUtil.sub(objList, i, i + count);
            List<Map> mapList = tempList.parallelStream()
                    .map(o -> Convert.convert(Map.class, o))
                    .collect(Collectors.toList());

            if (null == mapList || mapList.size() == 0) {
                return false;
            }
            Map dataMap = mapList.get(0);
            List<String> fieldKeyList = (List<String>) dataMap.keySet().stream()
                    .map(o -> o.toString())
                    .collect(Collectors.toList());

            baseMapper.mutiInsert(tableName, fieldKeyList, mapList);
        }

        return true;

    }

}
