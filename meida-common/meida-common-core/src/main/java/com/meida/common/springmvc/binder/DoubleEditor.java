package com.meida.common.springmvc.binder;

import com.meida.common.base.utils.FlymeUtils;
import org.springframework.beans.propertyeditors.PropertiesEditor;

public class DoubleEditor extends PropertiesEditor {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (FlymeUtils.isNotEmpty(text)) {
            setValue(Double.parseDouble(text));
        }

    }

    @Override
    public String getAsText() {
        return getValue().toString();
    }
} 
