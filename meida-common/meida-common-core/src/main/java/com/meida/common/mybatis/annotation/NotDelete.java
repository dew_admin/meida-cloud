package com.meida.common.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 禁止删除
 *
 * @author flyme
 * @date 2019/10/25 18:00
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface NotDelete {
    String value() default "allowDel";
}
