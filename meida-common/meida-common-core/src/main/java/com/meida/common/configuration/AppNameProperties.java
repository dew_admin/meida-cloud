package com.meida.common.configuration;

import lombok.Data;

/**
 * 应用名称属性spring.application.name
 *
 * @author fengshuonan
 * @Date 2018/5/8 下午2:24
 */
@Data
public class AppNameProperties {

    private String name;

}
