package com.meida.common.base.event;

import com.meida.mq.adapter.MqListenerAdapter;
import com.meida.mq.annotation.MsgListenerObj;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 */
@Component
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "localmq")
public class LocalMqListenerAdapter implements MqListenerAdapter {
    @Override
    public void initMqListener(Class<?> clazz, String methodName, MsgListenerObj param) {

    }
}
