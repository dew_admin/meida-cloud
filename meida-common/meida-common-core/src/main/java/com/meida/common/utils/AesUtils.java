package com.meida.common.utils;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * AES加解密
 * @author zyf
 */
public class AesUtils {

    /**
     * 密钥算法
     */
    private static final String ALGORITHM = "AES";
    /**
     * 加解密算法/工作模式/填充方式
     */
    private static final String ALGORITHM_STR = "AES/ECB/PKCS5Padding";

    /**
     * SecretKeySpec类是KeySpec接口的实现类,用于构建秘密密钥规范
     */
    private SecretKeySpec key;

    public AesUtils(String hexKey) {
        key = new SecretKeySpec(hexKey.getBytes(), ALGORITHM);
    }

    /**
     * AES加密
     *
     * @param data
     * @return
     * @throws Exception
     */
    public String encryptData(String data) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM_STR); // 创建密码器
        cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
        Base64.Encoder encoder = Base64.getMimeEncoder();
        return encoder.encodeToString(cipher.doFinal(data.getBytes()));
    }

    /**
     * AES解密
     *
     * @param base64Data
     * @return
     * @throws Exception
     */
    public String decryptData(String base64Data) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM_STR);
        cipher.init(Cipher.DECRYPT_MODE, key);
        Base64.Decoder decoder = Base64.getMimeDecoder();
        return new String(cipher.doFinal(decoder.decode(base64Data)));
    }

    /**
     * hex字符串 转 byte数组
     *
     * @param s
     * @return
     */
    private static byte[] hex2byte(String s) {
        if (s.length() % 2 == 0) {
            return hex2byte(s.getBytes(), 0, s.length() >> 1);
        } else {
            return hex2byte("0" + s);
        }
    }

    private static byte[] hex2byte(byte[] b, int offset, int len) {
        byte[] d = new byte[len];
        for (int i = 0; i < len * 2; i++) {
            int shift = i % 2 == 1 ? 0 : 4;
            d[i >> 1] |= Character.digit((char) b[offset + i], 16) << shift;
        }
        return d;
    }

    public static void main(String[] args) throws Exception {
        AesUtils util = new AesUtils("1234567812345678"); // 密钥
        System.out.println("cardNo:" + util.encryptData("{\"isFirst\":\"否\",\"txnSamt\":\"4877.2\",\"txnday\":\"20210409\",\"hrtMid\":\"864000253114633\",\"txnType\":\"刷卡\",\"sn\":\"HYB90000002\",\"txntime\":\"151201\",\"txnFee\":\"31.7\",\"txnCard\":\"622424******0021\"}")); // 加密
        System.out.println("exp:" + util.decryptData("w80H2E9VeTYpYglq2xT4wki2kuu85ZvbuiYo+JcHr3Xx0UBzEfItdhjx+S44RHzMQbLkqCQprC/nwXS1kidVFmlwVkpdnPHmWUr/VdzgLF7Y0lBFxtJZtfpzEt6cH79CV1pmf7zrfziEYpcC0fFy/Rre3aSRJEi+a8GnjNv4laumCg0NUK97Z1rsD8PBkZd0zWYO9dJA9HcLrnU/gppWJpV2WhETgna3LTobmcrPG1T3eKWDxfrZrZFFrHN2Y74H")); // 解密
    }
}