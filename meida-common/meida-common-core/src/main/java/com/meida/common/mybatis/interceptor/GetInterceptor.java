package com.meida.common.mybatis.interceptor;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;

import com.meida.common.mybatis.query.CriteriaQuery;

/**
 * 扩展查询单个对象拦截器
 */
public interface GetInterceptor {

    /**
     * 条件验证
     *
     * @return
     */
    default ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }


    /**
     * getEntityMap方法扩展查询条件
     *
     * @return
     */
    void prepare(CriteriaQuery cq, EntityMap params);


    /**
     * getEntityMap方法扩展返回结果
     *
     * @return
     */
    default void complete(CriteriaQuery cq, EntityMap map) {

    }


}
