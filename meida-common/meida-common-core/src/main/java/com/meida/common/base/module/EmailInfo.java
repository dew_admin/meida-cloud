package com.meida.common.base.module;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zyf
 */
@Data
public class EmailInfo implements Serializable {
    /**
     * 标题
     */
    private String setSubject = "验证码";
    /**
     * 模板代码
     */
    private String tplCode = "email";

    /**
     * 发件人
     */
    private String from="";
}
