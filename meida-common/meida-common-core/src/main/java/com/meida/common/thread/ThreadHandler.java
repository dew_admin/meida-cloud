package com.meida.common.thread;

import com.meida.common.base.entity.EntityMap;

import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */
public interface ThreadHandler<T> {
    /**
     * 业务逻辑
     *
     * @param list
     */
    default List<Map<String, Object>> handler(String threadName, Integer threadNo, List<T> list){
        return null;
    }

    /**
     * 业务逻辑
     *
     * @param list
     */
    default EntityMap handlerObj(String threadName, Integer threadNo, T list){
        return null;
    }

}
