package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 审批状态
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AppStatusEnum {
    NO(0, "未审批"),
    YES(1, "已审批"),
    BACK(2, "已退回");

    AppStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
