package com.meida.common.mybatis.query;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.JsonUtils;
import com.meida.common.utils.ReflectionUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author zyf
 */
@Data
public class CriteriaUpdate<T> extends UpdateWrapper<T> implements Serializable {
    private EntityMap requestMap = new EntityMap();

    private String handlerName;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表别名
     */
    private String tableAlias;

    /**
     * 主键字段
     */
    private String idField;

    /**
     * 主键值
     */
    private Long idValue;

    private Long[] ids;

    private Class<T> cls;

    /**
     * 传递到afterEdit中的参数
     */
    private EntityMap extra = new EntityMap();


    public EntityMap put(String key, Object value) {
        this.requestMap.put(key, value);
        return this.requestMap;
    }

    public EntityMap putExtra(String key, Object value) {
        this.extra.put(key, value);
        return this.extra;
    }

    public Object getExtra(String key) {
        return this.extra.get(key);
    }

    public Long getLongExtra(String key) {
        return this.extra.get(key);
    }


    public CriteriaUpdate() {

    }


    public CriteriaUpdate(Map map, Class<T> cls) {
        this.cls = cls;
        this.handlerName = (String) map.getOrDefault(CommonConstants.HANDLER_NAME, "");
        this.setHandlerName(handlerName);
        this.tableName = ReflectionUtils.getTableName(cls);
        this.tableAlias = ReflectionUtils.getAlias(cls);
        this.idField = ReflectionUtils.getTableField(cls);
        this.requestMap.putAll(map);

        Long idValue = Optional.ofNullable(this.requestMap.getLong(idField)).orElse(FlymeUtils.getLong(map.get("id")));
        if (FlymeUtils.isNotEmpty(idValue)) {
            this.ids = new Long[]{idValue};
            this.eq(true, idField, idValue);
            this.requestMap.put(idField, idValue);
            this.idValue = idValue;
        } else {
            Long[] idsValue = requestMap.getLongIds("ids");
            if (FlymeUtils.isNotEmpty(idsValue)) {
                this.ids = idsValue;
                if (idsValue.length > 1) {
                    this.in(true, idField, idsValue);
                    this.idValue = idsValue[0];
                    this.requestMap.put("ids", idsValue);

                } else {
                    this.ids[0] = idsValue[0];
                    this.idValue = idsValue[0];
                    this.requestMap.put(idField, idsValue[0]);
                    this.eq(true, idField, idsValue[0]);
                }
            }
        }
    }


    public List<Long> getArrayLong(String key) {
        return requestMap.getListLong(key);

    }


    public Long getLong(String key) {
        return requestMap.getLong(key);
    }

    public <T> T getParams(String key) {
        return (T) requestMap.get(key);
    }

    public <T> T getInt(String key, Integer def) {
        return (T) requestMap.getInt(key, def);
    }

    public <T> T getEntity(Class<T> cls) {
        T obj = JsonUtils.jsonToBean(JSONUtil.toJsonStr(requestMap), cls);
        return obj;
    }

    public Long[] getIds() {
        return ids;
    }
}
