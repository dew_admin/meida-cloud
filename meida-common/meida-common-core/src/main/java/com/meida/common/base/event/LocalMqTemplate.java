package com.meida.common.base.event;

import com.meida.mq.adapter.MqTemplate;
import com.meida.mq.exception.MqException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 */
@Component
@ConditionalOnProperty(prefix = "meida.mq", name = "provider", havingValue = "localmq")
public class LocalMqTemplate implements MqTemplate {
    @Override
    public void convertAndSend(Object var1) throws MqException {

    }

    @Override
    public void convertAndSend(String var1, Object var2) throws MqException {

    }

    @Override
    public void sendMessage(String queueName, Object params, Integer expiration) {

    }

    @Override
    public void sendMessage(String queueName, Object params) {

    }

    @Override
    public Object convertSendAndReceive(String queueName, Object param) {
        return null;
    }
}
