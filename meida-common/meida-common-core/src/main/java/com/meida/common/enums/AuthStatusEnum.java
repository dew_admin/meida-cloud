package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 认证状态
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AuthStatusEnum {
    NOAUTH(0, "未认证"),
    AUTHING(1, "待审核"),
    AUTHSUCCESS(2, "已认证"),
    AUTHFAIL(10, "审核失败");

    AuthStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
