package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 显示隐藏
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VisiableEnum {
    SHOW(1, "显示"),
    HIDE(0, "隐藏");

    VisiableEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
