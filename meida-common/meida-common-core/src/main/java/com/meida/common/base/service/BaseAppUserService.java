package com.meida.common.base.service;

/**
 * app用户全局接口
 *
 * @author zyf
 */
public interface BaseAppUserService {

    /**
     * 删除APP用户
     *
     * @param companyId
     * @return
     */
    Boolean deleteByCompanyId(Long companyId);

}
