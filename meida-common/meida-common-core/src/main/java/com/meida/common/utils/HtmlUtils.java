package com.meida.common.utils;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ReUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.meida.common.base.utils.FlymeUtils;

import java.util.List;

/**
 * 网址工具类
 *
 * @author zyf
 */
public class HtmlUtils {

    /**
     * 获取网址title
     *
     * @param navUrl
     * @return
     */
    public static JSONObject findTitleAndIco(String navUrl) {
        JSONObject result = new JSONObject(true);
        String body = HttpUtil.createGet(navUrl).execute().toString();
        //使用正则获取所有标题
        List<String> titles = ReUtil.findAll("<title>(.*?)</title>", body, 1);
        if (FlymeUtils.isNotEmpty(titles)) {
            result.put("title", titles.get(0));
        }
        String str = body.split("/favicon.ico")[0];
        int http = str.indexOf("https://", str.length() - 100);
        if (http == -1) {
            http = str.indexOf("http://", str.length() - 100);
        }
        if (http == -1) {
            //说明没有指定 走拼接逻辑
            //获取网址 拼接 favicon.ico
            int i = navUrl.indexOf("/", 8);
            if (i > 0) {
                navUrl = navUrl.substring(0, i);
            }
        } else {
            navUrl = str.substring(http);
        }
        String favicon = navUrl + "/favicon.ico";
        result.put("favicon", favicon);
        result.put("url", navUrl);
        return result;
    }
    public static void main(String [] args){
        System.out.println(findTitleAndIco("http://slide.news.sina.com.cn/w/slide_1_86058_524223.html#p=1"));
    }
}
