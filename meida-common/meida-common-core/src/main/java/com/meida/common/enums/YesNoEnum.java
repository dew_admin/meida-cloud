package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 显示隐藏
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum YesNoEnum {
    NO(0, "否"),
    YES(1, "是");

    YesNoEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getValue(int value) {
        String v = "";
        for (OnLineEnum onLineEnum : OnLineEnum.values()) {
            if (onLineEnum.getCode() == value) {
                v = onLineEnum.getRemark();
                break;
            }
        }
        return v;
    }
}
