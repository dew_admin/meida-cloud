package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;

import java.util.List;
import java.util.Map;

/**
 * websocket 消息发送接口
 *
 * @author flyme
 * @date 2019-06-03
 */
public interface WebSocketMsgService {

    /**
     * 发送消息
     *
     * @param userId
     * @param map
     * @param sendType
     * @return
     */
    void sendMessage(Long userId, Integer sendType, EntityMap map);

    /**
     * 发送消息
     *
     * @param userId
     * @param map
     * @param sendType
     * @return
     */
    void sendMessage(Long userId, Integer sendType, Map map);

}
