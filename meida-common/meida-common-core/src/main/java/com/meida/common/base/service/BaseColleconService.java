package com.meida.common.base.service;

import com.meida.common.enums.ColleconEnum;

public interface BaseColleconService {
    /**
     * 查询收藏标记
     *
     * @param targetId
     * @param colleconEnum
     * @param cls
     * @return
     */
    long countCollecon(Long targetId, ColleconEnum colleconEnum, Class cls);
}
