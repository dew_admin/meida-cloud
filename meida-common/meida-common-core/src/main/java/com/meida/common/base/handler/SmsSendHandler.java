package com.meida.common.base.handler;

import java.util.Map;

/**
 * @author Administrator
 */
public interface SmsSendHandler {


    /**
     * 发送验证码
     *
     * @param mobile
     * @return
     */
    void send(String mobile, Integer code, String areaCode);

    void sendSms(String mobile, String tplCode, Map<String, Object> params);


}
