package com.meida.common.lock;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author flyme
 * @date 2019/10/10 14:30
 */
public interface CacheKeyGenerator {
    /**
     * 获取AOP参数,生成指定缓存Key
     *
     * @param pjp PJP
     * @return 缓存KEY
     */
    String getLockKey(ProceedingJoinPoint pjp);
}
