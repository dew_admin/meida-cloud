package com.meida.common.mybatis.base.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: flyme
 * @date: 2018/3/7 15:01
 * @desc: 实体类父类带添加和更新用户
 */
@Data
public abstract class AbstractAllEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(hidden = true)
    public Long createUser;
    /**
     * 修改用户
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(hidden = true)
    public Long updateUser;
    /**
     * 逻辑删除字段
     */
    public Integer deleted = 0;
}