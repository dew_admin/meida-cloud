package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *  系统用户账号
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AccountTypeEnum {
    USERNAME("USERNAME", "账户名"),
    EMAIL("EMAIL", "邮箱"),
    MOBILE("MOBILE", "手机号"),
    WEIXIN("WEIXIN", "微信"),
    WEIBO("WEIBO", "微博"),
    FACEBOOK("FACEBOOK", "FACEBOOK"),
    SIGN("SIGN", "新浪"),
    OTHER("OTHER", "其他"),
    QQ("QQ", "QQ");

    AccountTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final String code;
    private final String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
