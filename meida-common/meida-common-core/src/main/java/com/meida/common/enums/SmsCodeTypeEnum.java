package com.meida.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 验证码用途
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SmsCodeTypeEnum {
    REG(1, "注册"),
    UPDATE(2, "找回密码"),
    BIND(3, "绑定手机号"),
    LOGIN(4, "验证码登录");

    SmsCodeTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
