package com.meida.common.mybatis.callback;

import com.meida.common.mybatis.query.CriteriaQuery;

/**
 * 查询回调接口
 *
 * @author zyf
 */
public interface CriteriaQueryCallBack<T> {
    /**
     * 初始化查询条件
     *
     * @param cq
     */
    void init(CriteriaQuery<T> cq);
}
