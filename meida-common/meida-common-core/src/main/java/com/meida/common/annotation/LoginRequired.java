package com.meida.common.annotation;

import java.lang.annotation.*;

/**
 * 是否需要登录验证
 *
 * @author zyf
 */
@Documented
@Inherited
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginRequired {
    boolean required() default true;
}
