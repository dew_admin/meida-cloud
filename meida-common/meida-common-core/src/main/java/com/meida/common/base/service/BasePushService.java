package com.meida.common.base.service;


import java.util.List;
import java.util.Map;

/***抽象推送接口**/
public interface BasePushService<K> {
    /****初始化客户端***/
    void init(K jPushClient, boolean ambient);

    /**
     * 推送给设备标识参数的用户
     *
     * @param deviceToken       设备标识
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendByDeviceToken(Object deviceToken, String notificationTitle, String title, String content, Map extra);


    /**
     * 推送给设备标识参数的用户
     *
     * @param deviceToken       设备标识
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendByListTokey(List<String> deviceToken, String notificationTitle, String title, String content, Map extra);

    /**
     * 发送给所有安卓用户
     *
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendAllAndroid(String notificationTitle, String title, String content, Map extra);

    /**
     * 发送给所有IOS用户
     *
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendAllIos(String notificationTitle, String title, String content, Map extra);

    /**
     * 发送给所有用户
     *
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendAll(String notificationTitle, String title, String content, Map extra);


    /**
     * 根据类型发送
     *
     * @param deviceType        0 全部 1安卓 2 IOS
     * @param notificationTitle 通知内容标题
     * @param title             消息内容标题
     * @param content           消息内容
     * @param extra             扩展字段
     * @return 0推送失败，1推送成功
     */
    int sendByDeviceType(int deviceType, String notificationTitle, String title, String content, Map extra);

}
