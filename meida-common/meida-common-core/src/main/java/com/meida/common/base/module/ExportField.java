package com.meida.common.base.module;

import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ExportField extends ExcelExportEntity {

    @ApiModelProperty("实体映射的字段")
    private String entityName;

    @ApiModelProperty("sql-group-by去除字段：0 否 1是")
    private Integer groupBy;



    @ApiModelProperty("合并单元格")
    private List<ExportField> children;
}
