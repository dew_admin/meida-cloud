package com.meida.common.base.service;

import com.meida.common.base.entity.EntityMap;

/**
 * APP账户全局接口
 * @author Administrator
 */
public interface BaseAppAccountService {

    /**
     * 查询account
     *
     * @param userId
     * @param accountType
     * @return
     */
    EntityMap getAppAccount(Long userId, String accountType);
}
