package com.meida.common.springmvc.base;


import cn.afterturn.easypoi.util.WebFilenameUtils;
import cn.hutool.core.util.ObjectUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.springmvc.binder.*;
import com.meida.common.utils.RedisUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author: Administrator
 * @date: 2019/1/4 22:02
 * @desc: 类描述：基础控制器
 */
public class BaseController<Biz extends IBaseService<T>, T> {

    @Autowired
    protected Biz bizService;


    @Autowired
    protected RedisUtils redisUtils;
    private static final String CONTENT_TYPE = "text/html;application/vnd.ms-excel";

    protected static final String HSSF = ".xls";
    protected static final String XSSF = ".xlsx";


    /**
     * 构建Cascader数据
     */
    protected List<Map<String, Object>> toCascader(List<Map<String, Object>> maps) {
        maps.forEach(map -> {
            Object obj = map.get("isParent");
            Object label = map.get("label");
            Object id = map.get("value");
            map.put("title", label);
            map.put("id", id);
            Integer isParent = (ObjectUtil.isNotNull(obj) && obj.equals(1)) ? 1 : 0;
            if (isParent.equals(1)) {
                map.put("children", new ArrayList<>());
                map.put("loading", false);
            }
        });
        return maps;
    }


    /**
     * 后台接收数据转换
     */
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(true);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        datetimeFormat.setLenient(true);

        binder.registerCustomEditor(java.sql.Timestamp.class, new CustomTimestampEditor(datetimeFormat, true));
        binder.registerCustomEditor(Integer.class, new IntegerEditor());
        binder.registerCustomEditor(String.class, new StringEditor());
        binder.registerCustomEditor(long.class, new LongEditor());
        binder.registerCustomEditor(double.class, new DoubleEditor());
        binder.registerCustomEditor(float.class, new FloatEditor());
    }

    public void out(Workbook workbook, String codedFileName, HttpServletResponse response) {
        if (workbook instanceof HSSFWorkbook) {
            codedFileName += HSSF;
        } else {
            codedFileName += XSSF;
        }
        // 用工具类生成符合RFC 5987标准的文件名header, 去掉UA判断
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", WebFilenameUtils.disposition(codedFileName));
        response.setHeader("fileName", WebFilenameUtils.disposition(codedFileName));
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
            workbook.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}