package com.meida.common.base.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;

/**
 * @author zyf
 */
public interface AppProxyHandler {

    /**
     * 校验
     *
     * @return
     */
    default ResultBody validate(String url, EntityMap params) {
        return ResultBody.ok();
    }

    /**
     * 保存后扩展
     *
     * @return
     */
    default void complete(String url, EntityMap params) {

    }


}
