package com.meida.common.base.constants;

/**
 * @author zyf
 */
public class FlymeConstants {
    /**
     * 公共消息通道
     */
    public static String COMMON_TOPIC_NAME = "flyme_redis_message_topic";
}
