package com.meida.common.base.constants;

/**
 * @author zyf
 */
public class CommonConstants {


    /**
     * 微服务读取配置文件属性 服务地址
     */
    public final static String MEIDA_SINGLE = "meida.single";
    public final static String CLOUD_SERVER_KEY = "spring.cloud.nacos.discovery.server-addr";
    /**
     * 服务名称
     */
    public static final String BASE_SERVICE = "meida-base-provider";
    /**
     * 接口服务
     */
    public static final String APP_SERVICE = "meida-app-provider";


    //免登录鉴权key
    public final static String NOTAUTH_KEY = "NOTAUTH_KEY:";

    public static final String ACCOUNT_LOCK_ERROR_COUNT = "ACCOUNT_LOCK_ERROR_COUNT:";
    public static final String ACCOUNT_LOCK_ERROR_KEY = "ACCOUNT_LOCK_ERROR_KEY:";
    public static final String ACCOUNT_LOCK_ERROR_TIME = "ACCOUNT_LOCK_ERROR_TIME:";
    public static final int ACCOUNT_LOCK_ERROR_MAX_COUNT = 5;
    /**
     * 锁定时长
     */
    public static final int ACCOUNT_LOCK_ERROR_LOCK_TIME = 60 * 60 * 3;
    /**
     * 几分钟内错误次数（如10分钟内错误3次锁定）
     */
    public static final int ACCOUNT_LOCK_ERROR_CHECK_TIME = 60 * 10;



    /**
     * 默认注册密码
     */
    public final static String DEF_PWD = "123456";


    /**
     * 不校验密码的登录类型
     */
    public final static String NO_AUTH_TYPE = "QQ,WEIXIN,WEIBO,MOBILE,FACEBOOK,SINA,GOOGLE";

    /**
     * 默认校验密码
     */
    public final static String AUTH_TYPE_PWD = "PASSWORD";
    /**
     * 第三方登录类型
     */
    public final static String SMS_AUTH_TYPE = "QQ,WEIXIN,WEIBO,FACEBOOK,SINA,GOOGLE";

    public final static String[] PERMIT_AUTH = new String[]{"/**/common/**", "/login/**", "/logout/**", "/oauth/**", "/apple-app-site-association", "/designer"};

    /**
     * 获取验证码类型:1：注册,换绑手机号 2：修改密码,3:绑定手机号 4：验证码登录
     */
    public final static Integer SMS_USETYPE_REG = 1;
    public final static Integer SMS_USETYPE_UPDATE = 2;
    public final static Integer SMS_USETYPE_BIND = 3;
    public final static Integer SMS_USETYPE_LOGIN = 4;


    public final static Integer INT_0 = 0;
    public final static Integer INT_1 = 1;
    public final static Integer INT_2 = 2;
    public final static Integer INT_3 = 3;

    public final static Integer LEVEL_1 = 1;
    public final static Integer LEVEL_2 = 2;
    public final static Integer LEVEL_3 = 3;
    /**
     * 状态:0-无效 1-有效
     */
    public final static Integer ENABLED = 1;
    public final static Integer DISABLED = 0;

    /**
     * 状态:0-未删除 1-已删除
     */
    public final static Integer DEL_NO = 0;
    public final static Integer DEL_YES = 1;


    /**
     * 状态:0-无效 1-有效
     */
    public final static Boolean TRUE = true;
    public final static Boolean FALSE = false;
    public final static String SUCCESS = "SUCCESS";
    public final static String FAIL = "FAIL";


    /**
     * 逻辑删除:0-未删除 1-已删
     */
    public final static int LOGIC_DELETE = 1;
    public final static int LOGIC_NOT_DELETE = 0;

    /**
     * 状态:0-无效 1-有效
     */
    public final static String ENABLED_STR = "1";
    public final static String DISABLED_STR = "0";
    /**
     * 默认超级管理员账号
     */
    public final static String ROOT = "superadmin";
    /**
     * 默认头像
     */
    public final static String DEFAULT_USERHEAD = "https://s1.ax1x.com/2018/05/19/CcdVQP.png";


    /**
     * 短信验证码key前缀
     */
    public final static String PRE_SMS = "FLYME_PRE_SMS:";


    /**
     * 邮箱验证码key前缀
     */
    public final static String PRE_EMAIL = "FLYME_PRE_EMAIL:";


    /**
     * 默认最小页码
     */
    public static final int MIN_PAGE = 0;
    /**
     * 最大显示条数
     */
    public static final int MAX_LIMIT = 999;
    /**
     * 默认页码
     */
    public static final int DEFAULT_PAGE = 1;
    /**
     * 默认显示条数
     */
    public static final int DEFAULT_LIMIT = 10;
    /**
     * 页码 KEY
     */
    public static final String PAGE_KEY = "page";
    /**
     * 显示条数 KEY
     */
    public static final String PAGE_LIMIT_KEY = "limit";

    public static final String HANDLER_NAME = "handlerName";
    /**
     * 排序字段 KEY
     */
    public static final String PAGE_SORT_KEY = "sort";


    /**
     * 分页类别返回数据是否包含分页数据
     */
    public static final String SIMPLEDATA_KEY = "simpleData";

    /**
     * 主键字段KEY
     */
    public static final String PARAMS_ID_KEY = "id";
    /**
     * 排序方向 KEY
     */
    public static final String PAGE_ORDER_KEY = "order";


    /**
     * 查询开始日期
     */
    public static final String QUERY_BEGINDATE = "beginDate";
    /**
     * 查询结束日期
     */
    public static final String QUERY_ENDDATE = "endDate";

    /**
     * 客户端ID KEY
     */
    public static final String SIGN_APP_ID_KEY = "appId";

    /**
     * 客户端秘钥 KEY
     */
    public static final String SIGN_SECRET_KEY = "secretKey";

    /**
     * 随机字符串 KEY
     */
    public static final String SIGN_NONCE_KEY = "nonce";
    /**
     * 时间戳 KEY
     */
    public static final String SIGN_TIMESTAMP_KEY = "timestamp";
    /**
     * 签名类型 KEY
     */
    public static final String SIGN_SIGN_TYPE_KEY = "signType";
    /**
     * 签名结果 KEY
     */
    public static final String SIGN_SIGN_KEY = "sign";
    /**
     * 区域KEY
     */
    public static final String REDIS_KEY_ALLAREA = "KEY_ALLAREA";

    public static final String SEARCH_COUNT = "searchCount";
}
