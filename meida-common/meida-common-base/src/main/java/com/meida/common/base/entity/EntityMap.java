package com.meida.common.base.entity;


import com.meida.common.base.utils.FlymeUtils;
import org.apache.commons.beanutils.ConvertUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 自定义Map
 *
 * @author zyf
 */

public class EntityMap extends ConcurrentHashMap<String, Object> {

    private static final long serialVersionUID = 1L;


    public EntityMap() {

    }

    public EntityMap(Map<String, Object> map) {
        this.putAll(map);
    }


    @Override
    public EntityMap put(String key, Object value) {
        super.put(key, Optional.ofNullable(value).orElse(""));
        return this;
    }

    public EntityMap add(String key, Object value) {
        super.put(key, Optional.ofNullable(value).orElse(""));
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        Object obj = super.get(key);
        if (FlymeUtils.isNotEmpty(obj)) {
            return (T) obj;
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public Boolean getBoolean(String key) {
        Object obj = super.get(key);
        if (FlymeUtils.isNotEmpty(obj)) {
            return Boolean.valueOf(obj.toString());
        } else {
            return false;
        }
    }

    public Long getLong(String key) {
        if(FlymeUtils.isNotEmpty(key)) {
            Object v = get(key);
            if (FlymeUtils.isNotEmpty(v)) {
                return new Long(v.toString());
            }
        }
        return null;
    }

    public Long[] getLongs(String key) {
        Object v = get(key);
        if (FlymeUtils.isNotEmpty(v)) {
            return (Long[]) v;
        }
        return null;
    }

    public List<Long> getListLong(String key) {
        List<String> list = get(key);
        if (FlymeUtils.isNotEmpty(list)) {
            return list.stream().map(e -> new Long(e)).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    public Long[] getLongIds(String key) {
        Object ids = get(key);
        if (FlymeUtils.isNotEmpty(ids)) {
            return (Long[]) ConvertUtils.convert(ids.toString().split(","), Long.class);
        } else {
            return null;
        }
    }


    public Integer getInt(String key, Integer def) {
        Object v = get(key);
        if (FlymeUtils.isNotEmpty(v)) {
            return Integer.parseInt(v.toString());
        } else {
            return def;
        }
    }

    public Integer getInt(String key) {
        Object v = get(key);
        if (FlymeUtils.isNotEmpty(v)) {
            return Integer.parseInt(v.toString());
        } else {
            return 0;
        }
    }

    public BigDecimal getBigDecimal(String key) {
        Object v = get(key);
        if (FlymeUtils.isNotEmpty(v)) {
            return new BigDecimal(v.toString());
        }
        return new BigDecimal("0");
    }


    @SuppressWarnings("unchecked")
    public <T> T get(String key, T def) {
        Object obj = super.get(key);
        if (FlymeUtils.isEmpty(obj)) {
            return def;
        }
        return (T) obj;
    }

    public static EntityMap castTomap(Object obj) {
        EntityMap map = new EntityMap();
        map.putAll((Map<? extends String, ?>) obj);
        return map;
    }

    public static void main(String[] args) {
        System.out.println(FlymeUtils.isNotEmpty(new EntityMap()));
    }

}
