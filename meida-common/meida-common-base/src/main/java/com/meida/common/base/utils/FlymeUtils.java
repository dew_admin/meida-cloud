package com.meida.common.base.utils;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;


import java.io.InputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zyf
 */
public class FlymeUtils {
    public FlymeUtils() {
    }

    /**
     * 其中一个为空
     *
     * @param objs
     * @return
     */
    public static boolean anyOneIsNull(Object... objs) {
        int n = objs.length;
        for (int i = 0; i < n; ++i) {
            Object o = objs[i];
            if (isEmpty(o)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 其中一个不为空
     *
     * @param objs
     * @return
     */
    public static boolean anyOneIsNotNull(Object... objs) {
        int n = objs.length;
        for (int i = 0; i < n; ++i) {
            Object o = objs[i];
            if (isNotEmpty(o)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNotEmptyAfterTrim(String str) {
        return !isEmptyAfterTrim(str);
    }

    public static boolean isEmptyAfterTrim(String str) {
        return org.apache.commons.lang3.StringUtils.isEmpty(StringUtils.trimToEmpty(str));
    }

    /**
     * 都为空
     *
     * @param objs
     * @return
     */
    public static boolean allIsNull(Object... objs) {
        int n = objs.length;
        int t = 0;
        for (int i = 0; i < n; ++i) {
            Object o = objs[i];
            if (isEmpty(o)) {
                t++;
            }
        }
        if (t == n) {
            return true;
        }
        return false;
    }

    /**
     * List转换为数组
     */
    public static <T> Long[] listToLong(List<T> list) {
        Long[] strs = list.toArray(new Long[list.size()]);
        return strs;
    }

    /**
     * v1>v2
     */
    public static Boolean gt(BigDecimal v1, BigDecimal v2) {
        Boolean tag = false;
        if (v1.compareTo(v2) == 1) {
            tag = true;
        }
        return tag;
    }

    /**
     * v1>=v2
     */
    public static Boolean ge(BigDecimal v1, BigDecimal v2) {
        Boolean tag = false;
        if (v1.compareTo(v2) == 1) {
            tag = true;
        }
        if (v1.compareTo(v2) == 0) {
            tag = true;
        }
        return tag;
    }

    /**
     * v1<v2
     */
    public static Boolean lt(BigDecimal v1, BigDecimal v2) {
        Boolean tag = false;
        if (v1.compareTo(v2) == -1) {
            tag = true;
        }
        return tag;
    }

    /**
     * v1=v2
     */
    public static Boolean eq(BigDecimal v1, BigDecimal v2) {
        Boolean tag = false;
        if (FlymeUtils.isNotEmpty(v1)) {
            if (v1.compareTo(v2) == 0) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是否小于等于0
     */
    public static Boolean le(BigDecimal v1, BigDecimal v2) {
        Boolean tag = false;
        if (FlymeUtils.isNotEmpty(v1)) {
            if (v1.compareTo(v2) == -1) {
                tag = true;
            }
            if (v1.compareTo(v2) == 0) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是否大于0
     */
    public static Boolean gtzero(BigDecimal v) {
        Boolean tag = false;
        if (isNotEmpty(v)) {
            if (v.doubleValue() > 0) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是否大于等于0
     */
    public static Boolean gezero(BigDecimal v) {
        Boolean tag = false;
        if (isNotEmpty(v)) {
            if (v.compareTo(BigDecimal.ZERO) == 0 || v.compareTo(BigDecimal.ZERO) == 1) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是等于0
     */
    public static Boolean eqzero(BigDecimal v) {
        Boolean tag = false;
        if (isNotEmpty(v)) {
            if (v.compareTo(BigDecimal.ZERO) == 0) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal不等于0
     */
    public static Boolean nezero(BigDecimal v) {
        Boolean tag = true;
        if (isNotEmpty(v)) {
            if (v.compareTo(BigDecimal.ZERO) == 0) {
                tag = false;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是否xiao于0
     */
    public static Boolean ltzero(BigDecimal v) {
        Boolean tag = false;
        if (isNotEmpty(v)) {
            if (v.doubleValue() < 0) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断BigDecimal是否小于等于0
     */
    public static Boolean lezero(BigDecimal v) {
        Boolean tag = false;
        if (isNotEmpty(v)) {
            if (v.compareTo(BigDecimal.ZERO) == 0) {
                tag = true;
            }
            if (v.compareTo(BigDecimal.ZERO) == -1) {
                tag = true;
            }
        }
        return tag;
    }

    /**
     * 判断某个字符串是否存在于数组中
     */
    public static boolean contains(String[] stringArray, String source) {
        List<String> tempList = Arrays.asList(stringArray);
        if (tempList.contains(source)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断某个字符串是否存在于数组中
     */
    public static boolean contains(List<String> stringArray, String source) {
        if (stringArray.contains(source)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }


    /**
     * 都不为空
     *
     * @param obj
     * @return
     */
    public static boolean allNotNull(Object... obj) {
        return !anyOneIsNull(obj);
    }

    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if ((obj instanceof String)) {
            return ((String) obj).trim().equals("");
        } else if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        } else {
            return obj instanceof Map ? ((Map) obj).isEmpty() : false;
        }
    }

    /**
     * 以UUID重命名
     *
     * @param fileName
     * @return
     */
    public static String renameFile(String fileName) {
        String extName = fileName.substring(fileName.lastIndexOf("."));
        return UUID.randomUUID().toString().replace("-", "") + extName;
    }


    /**
     *
     */
    public static BigDecimal getBigDecimal(BigDecimal v, String def) {
        if (isEmpty(v)) {
            return new BigDecimal(def);
        }
        return v;
    }

    /**
     *
     */
    public static BigDecimal getBigDecimal(BigDecimal v, BigDecimal def) {
        if (isEmpty(v)) {
            return def;
        }
        return v;
    }

    /**
     *
     */
    public static String getString(String v) {
        if (isEmpty(v)) {
            return "";
        }
        return v;
    }

    /**
     *
     */
    public static String getString(String v, String defval) {
        if (isEmpty(v)) {
            return defval;
        }
        return v;
    }

    /**
     *
     */
    public static Integer getInteger(Integer v, Integer defval) {
        if (isEmpty(v)) {
            return defval;
        }
        return v;
    }

    /**
     *
     */
    public static Integer getInteger(String v, Integer defval) {
        if (isEmpty(v)) {
            return defval;
        }
        return Integer.parseInt(v);
    }

    /**
     *
     */
    public static Boolean getBoolean(String v, Boolean defval) {
        if (isEmpty(v)) {
            return defval;
        }
        return Boolean.parseBoolean(v);
    }

    /**
     *
     */
    public static Long getLong(Object v) {
        if (isEmpty(v)) {
            return null;
        }
        return Long.parseLong(v.toString());
    }

    /**
     *
     */
    public static Long getLong(Object v, Long def) {
        if (isEmpty(v)) {
            return def;
        }
        return Long.parseLong(v.toString());
    }

    /**
     * 中文數字转阿拉伯数组【十万九千零六十  --> 109060】
     */
    @SuppressWarnings("unused")
    public static String chineseNumber2Int(String chineseNumber) {
        String[] s1 = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"};
        String[] s2 = {"十", "百", "千", "万", "十", "百", "千", "亿", "十", "百", "千"};
        String result = "";
        int n = chineseNumber.length();
        for (int i = 0; i < n; i++) {
            int num = chineseNumber.charAt(i) - '0';
            if (i != n - 1 && num != 0) {
                result += s1[num] + s2[n - 2 - i];
            } else {
                result += s1[num];
            }
            System.out.println("  " + result);
        }
        return result;
    }


    public static String getAlias(String tableName) {
        String[] str = tableName.split("_");
        String alias = "";
        if (str.length > 2) {
            for (String s : str) {
                alias += s.substring(0, 1);
            }
        } else {
            alias = str[str.length - 1];
        }
        return alias;
    }

    /**
     * 字符串转换unicode
     */
    public static String string2Unicode(String string) {
        String result = "";
        if (isNotEmpty(string)) {
            StringBuffer unicode = new StringBuffer();
            for (int i = 0; i < string.length(); i++) {
                // 取出每一个字符
                char c = string.charAt(i);
                // 转换为unicode
                unicode.append("\\u" + Integer.toHexString(c));
            }
            result = unicode.toString();
        }
        return result;
    }

    public static Long[] StringArray2LongArray(String[] stringArray) {
        List<Long> list = new ArrayList<>();
        for (String str : stringArray) {
            try {
                list.add(Long.parseLong(str));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        Long[] longArray = list.toArray(new Long[list.size()]);
        return longArray;
    }

    public static Long[] StringToLongArray(Object stringArray) {
        if (FlymeUtils.isNotEmpty(stringArray)) {
            String[] idsArr = stringArray.toString().split(",");
            if (FlymeUtils.isNotEmpty(idsArr)) {
                return (Long[]) ConvertUtils.convert(idsArr, Long.class);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    /**
     * unicode 转字符串
     */
    public static String unicode2String(String unicode) {
        String result = "";
        if (isNotEmpty(unicode)) {
            StringBuffer string = new StringBuffer();
            String[] hex = unicode.split("\\\\u");
            for (int i = 1; i < hex.length; i++) {
                // 转换出每一个代码点
                int data = Integer.parseInt(hex[i], 16);
                // 追加成string
                string.append((char) data);
            }
            result = string.toString();
        }
        return result;
    }

    public static String getClassPathResoruce(String path) {
        ClassPathResource resource = new ClassPathResource(path);
        return IoUtil.read(resource.getStream(), "UTF-8");
    }


    public static String convertToString(Object obj, String defaultVal) {
        try {
            return (obj != null) ? String.valueOf(obj) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static String convertToString(Object obj) {
        return convertToString(obj, "");
    }

    public static Boolean convertToBoolean(Object obj, boolean defaultVal) {
        try {
            return (obj != null) ? Boolean.parseBoolean(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Boolean convertToBoolean(Object obj) {
        return convertToBoolean(obj, false);
    }

    public static Integer convertToInteger(Object obj, Integer defaultVal) {
        try {
            return (obj != null) ? Integer.parseInt(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Integer convertToInteger(Object obj) {
        return convertToInteger(obj, 0);
    }

    public static Short convertToShort(Object obj, Short defaultVal) {
        try {
            return (obj != null) ? Short.parseShort(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Short convertToShort(Object obj) {
        return convertToShort(obj, (short) 0);
    }

    public static Byte convertToByte(Object obj, Byte defaultVal) {
        try {
            return (obj != null) ? Byte.parseByte(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Byte convertToByte(Object obj) {
        return convertToByte(obj, (byte) 0);
    }

    public static Float convertToFloat(Object obj, Float defaultVal) {
        try {
            return (obj != null) ? Float.parseFloat(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Float convertToFloat(Object obj) {
        return convertToFloat(obj, 0F);
    }

    public static Long convertToLong(Object obj, Long defaultVal) {
        try {
            return (obj != null) ? Long.parseLong(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Long convertToLong(Object obj) {
        return convertToLong(obj, 0L);
    }

    public static Double convertToDouble(Object obj, Double defaultVal) {
        try {
            return (obj != null) ? Double.parseDouble(convertToString(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }


    public static <T> List<T> arrayToList(Object[] arrStr) {
        Stream<T> stream = (Stream<T>) Arrays.stream(arrStr);
        List<T> list = stream.distinct().collect(Collectors.toList());
        return list;
    }

    public static Double convertToDouble(Object obj) {
        return convertToDouble(obj, 0D);
    }

    public static BigDecimal convertToBigDecimal(Object obj, BigDecimal defaultVal) {
        try {
            return (obj != null) ? BigDecimal.valueOf(convertToDouble(obj)) : defaultVal;
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static BigDecimal convertToBigDecimal(Object obj) {
        return convertToBigDecimal(obj, BigDecimal.ZERO);
    }


    public static String convertToLike(String obj) {
        if (FlymeUtils.isNotEmpty(obj)) {
            return "%" + obj + "%";
        } else {
            return "";
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Object> convertToList(Object object) {
        List<Object> result = new ArrayList<>();
        if (isNotEmpty(object) && object instanceof List) {
            result = (List<Object>) object;
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    public static String listToString(List list, char separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i)).append(separator);
        }
        return sb.toString().substring(0, sb.toString().length() - 1);
    }

    /**
     * IOUtils.toString没有关流，这里统一处理，避免业务中捕获异常以及关闭流.
     */
    public static String streamToString(InputStream input) {
        try (InputStream sourceIn = input) {
            return IOUtils.toString(sourceIn, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean isInherit(Class<?> cls, Class<?> parentClass) {
        return parentClass.isAssignableFrom(cls);
    }


    public static boolean isWindows() {
        return System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1;
    }

    public static Boolean contains(Object[] objs, Object obj) {
        return Arrays.asList(objs).contains(obj);
    }

    public static int[] stringToIntArray(String input) {
        String[] inputArray = input.split(",");
        int[] result = new int[inputArray.length];

        for (int i = 0; i< inputArray.length; i++) {
            result[i] = Integer.parseInt(inputArray[i]); // 将每个字符串转换为int并存储在数组中
        }

        return result;
    }

    public static void main(String[] args) {

    }

}
