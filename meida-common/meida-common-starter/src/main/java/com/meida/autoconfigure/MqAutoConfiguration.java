package com.meida.autoconfigure;

import com.meida.common.constants.QueueConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zyf
 */
@Slf4j
@Configuration
public class MqAutoConfiguration {



    @Bean
    public Queue accessLogsQueue() {
        Queue queue = new Queue(QueueConstants.QUEUE_ACCESS_LOGS);
        log.info("Query {} [{}]", QueueConstants.QUEUE_ACCESS_LOGS, queue);
        return queue;
    }



    @Bean
    public Queue bindMobile() {
        return new Queue(QueueConstants.QUEUE_BIND_MOBILE);
    }

    @Bean
    public Queue delProduct() {
        return new Queue(QueueConstants.QUEUE_ORDER_CHECK);
    }

    @Bean
    public Queue userReg() {
        return new Queue(QueueConstants.QUEUE_USER_REG);
    }


    @Bean
    public Queue shopReg() {
        return new Queue(QueueConstants.QUEUE_SHOP_REG);
    }

    @Bean
    public Queue bingCompany() {
        return new Queue(QueueConstants.QUEUE_BIND_COMPANY);
    }

    @Bean
    public Queue unBingCompany() {
        return new Queue(QueueConstants.QUEUE_UNBIND_COMPANY);
    }


    @Bean
    public Queue collecon() {
        return new Queue(QueueConstants.QUEUE_COLLECON);
    }

    @Bean
    public Queue upload() {
        return new Queue(QueueConstants.QUEUE_UPLOADFILE);
    }

    @Bean
    public Queue uploadOss() {
        return new Queue(QueueConstants.QUEUE_UPLOADOSS);
    }

    @Bean
    public Queue delFile() {
        return new Queue(QueueConstants.QUEUE_DELFILE);
    }


    @Bean
    public Queue arcRamind() {
        return new Queue(QueueConstants.QUEUE_REMIND);
    }


}
