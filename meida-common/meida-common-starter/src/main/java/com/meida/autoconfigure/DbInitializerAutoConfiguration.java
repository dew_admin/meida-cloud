package com.meida.autoconfigure;

import com.meida.common.dbinit.listener.InitTableListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: zyf
 * @date: 2018/12/17 14:45
 * @desc: 数据库初始化默认配置
 */
@Configuration
public class DbInitializerAutoConfiguration {



    @Bean
    public InitTableListener initTableListener() {
        return new InitTableListener();
    }

}


