package com.meida.autoconfigure;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.meida.common.base.interceptor.FeignRequestInterceptor;
import feign.Request;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.Retryer;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Map;

/**
 * Feign OAuth2 request interceptor.
 *
 * @author zyf
 */
@Slf4j
@Configuration
public class FeignAutoConfiguration {
    public static int connectTimeOutMillis = 12000;
    public static int readTimeOutMillis = 12000;

    @Bean
    public Encoder springFormEncoder(ObjectMapper objectMapper) {
        Encoder encoder = new SpringMultipartEncoder(new SpringEncoder(new ObjectFactory<HttpMessageConverters>() {
            @Override
            public HttpMessageConverters getObject() throws BeansException {
                /*
                 由于 WebMvcConfiguration.httpMessageConverters
                 已经配置针对json的序列化处理, 空值转换。返回给客户端不能出现 null值。
                 而feign传递@RequestBody obj 对象参数时，序列化参数不需要对null值处理。确保null值不会被设置默认值
                 则新创建HttpMessageConverters并引用全局的objectMapper
                */
                MappingJackson2HttpMessageConverter httpMessageConverter = new MappingJackson2HttpMessageConverter();
                httpMessageConverter.setObjectMapper(objectMapper);
                return new HttpMessageConverters(httpMessageConverter);
            }
        }));
        log.info("FeignSpringFormEncoder [{}]", encoder);
        return encoder;
    }

//    @Bean
//    public Encoder feignFormEncoder(ObjectFactory<HttpMessageConverters> messageConverters) {
//        Encoder encoder = new FeignSpringFormEncoder(new SpringEncoder(messageConverters));
//        log.info("FeignSpringFormEncoder [{}]", encoder);
//        return encoder;
//    }

    @Bean
    @ConditionalOnMissingBean(FeignRequestInterceptor.class)
    public RequestInterceptor feignRequestInterceptor() {
        FeignRequestInterceptor interceptor = new FeignRequestInterceptor();
        log.info("FeignRequestInterceptor [{}]", interceptor);
        return interceptor;
    }


    @Bean
    public Request.Options options() {
        return new Request.Options(connectTimeOutMillis, readTimeOutMillis);
    }


    @Bean
    public Retryer feignRetryer(ApplicationContext context) {
        Environment env = context.getEnvironment();
        String active = env.getProperty("spring.profiles.active");
        if (active.equals("dev") || active.equals("test")) {
            log.info("禁用feign重试,当前环境:{}", active);
            return Retryer.NEVER_RETRY;
        } else {
            log.info("开启feign重试,当前环境:{}", active);
            return Retryer.NEVER_RETRY;
        }
    }
    /**
     * 多文件上传拓展
     */
    public class SpringMultipartEncoder extends SpringFormEncoder {
        public SpringMultipartEncoder() {
            super();
        }

        public SpringMultipartEncoder(Encoder delegate) {
            super(delegate);
        }

        @Override
        public void encode(Object object, Type bodyType, RequestTemplate template) throws EncodeException {
            // 单MultipartFile判断
            if (bodyType.equals(MultipartFile.class)) {
                MultipartFile file = (MultipartFile) object;
                Map data = Collections.singletonMap(file.getName(), object);
                super.encode(data, MAP_STRING_WILDCARD, template);
                return;
            } else if (bodyType.equals(MultipartFile[].class)) {
                // MultipartFile数组处理
                MultipartFile[] file = (MultipartFile[]) object;
                if (file != null) {
                    Map data = Collections.singletonMap(file.length == 0 ? "" : file[0].getName(), object);
                    super.encode(data, MAP_STRING_WILDCARD, template);
                    return;
                }
            }
            // 其他类型调用父类默认处理方法
            super.encode(object, bodyType, template);
        }
    }

}
