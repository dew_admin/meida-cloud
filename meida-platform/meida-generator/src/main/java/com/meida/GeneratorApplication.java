package com.meida;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author zyf
 */
@SpringBootApplication(scanBasePackages = "com.meida")
@Slf4j
public class GeneratorApplication {

    public static void main(String[] args) {
        start(args,GeneratorApplication.class);
    }

    public static void start(String[] args,Class cls) {

        ConfigurableApplicationContext application = SpringApplication.run(cls, args);
        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");
        log.info("\n----------------------------------------------------------\n\t" +
                "代码生成器启动成功:\n\t" +
                "访问地址: \t\thttp://localhost:" + port + "/generate/index\n\t" +
                "----------------------------------------------------------");
    }

}
