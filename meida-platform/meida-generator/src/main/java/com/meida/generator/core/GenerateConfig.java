
package com.meida.generator.core;

import lombok.Data;

/**
 * 代码生成参数配置
 *
 * @author zyf
 */
@Data
public class GenerateConfig {

    /**
     * 表名称
     */
    private String projectPath = "";

    private Boolean baseProject = true;

    /**
     * 父模块名称
     */
    private String baseModule = "meida-module";

    /**
     * 页面所属模块
     */
    private String pageModule = "business";
    /**
     * 模块名称
     */
    private String moduleName = "";
    /**
     * 表名称
     */
    private String tableName = "";

    /**
     * 生成代码里，注释的作者
     */
    private String author = "flyme";

    /**
     * web代码生成输出的目录
     */
    private String webDirectory = "E:\\WebPorject\\wanleme-admin";

    /**
     * jdbc驱动
     */
    private String jdbcDriver = "com.mysql.cj.jdbc.Driver";

    /**
     * 数据库连接地址
     */
    private String jdbcUrl = "jdbc:mysql://localhost:3300/mulu_cloud?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";

    /**
     * 数据库账号
     */
    private String jdbcUserName = "root";

    /**
     * 数据库密码
     */
    private String jdbcPassword = "123456";

    /**
     * 去掉表的前缀
     */
    private String[] removeTablePrefix = {""};

    /**
     * 代码生成包含的表，可为空，为空默认生成所有
     */
    private String[] includeTables;

    /**
     * 代码生成的类的父包名称
     */
    private String parentPackage = "com.meida.module";


    /**
     * service是否生成接口，这个根据自己项目情况决定
     */
    private Boolean generatorInterface = true;

    private Boolean override = false;

    /**
     * 字段
     */
    private String fields;
    /**
     * 主键字段
     */
    private String rowKey;
    /**
     * 是否生成添加方法
     */
    private Boolean addMethod;
    /**
     * 是否生成修改方法
     */
    private Boolean editMethod;
    /**
     * 是否生成删除方法
     */
    private Boolean delMethod;
    /**
     * 批量删除
     */
    private Boolean batchDelMethod;
    /**
     * 导出按钮
     */
    private Boolean exportMethod;

    /**
     * 是否生成菜单
     */
    private Boolean createMenu;
    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 父菜单名称
     */
    private String parentMenuName;
    /**
     * 父菜单ID
     */
    private Long parentId;



    /**
     * 生成controller
     */
    private Boolean createController=true;

    /**
     * 生成service
     */
    private Boolean createService=true;

    /**
     * 生成实体类
     */
    private Boolean createEntity=true;

    /**
     * 生成index页面
     */
    private Boolean createIndex=true;


    /**
     * 生成form页面
     */
    private Boolean createForm=true;

}
