package com.meida.generator.core;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.meida.generator.core.GenField;
import com.meida.generator.core.GenerateConfig;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.ibatis.jdbc.SqlRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 */
@Component
public class GeneratorCode {

    @Value("${meida.generator.database:master}")
    private String database;

    @Resource
    private DynamicDataSourceProperties dynamicDataSourceProperties;


    @Resource
    private DataSource dataSource;


    public void init(GenerateConfig generateConfig) {
        AutoGenerator mpg = new AutoGenerator();
        String tableName = generateConfig.getTableName();
        if (ObjectUtils.isEmpty(tableName)) {
            System.err.println("错误:请设置要生成的数据表名称");
            return;
        }
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //gc.setOutputDir(baseOutPath + "/src/main/java");
        gc.setFileOverride(generateConfig.getOverride());
        //ActiveRecord特性
        gc.setActiveRecord(false);
        // XML ResultMap
        gc.setBaseResultMap(false);
        // XML columList
        gc.setBaseColumnList(false);
        gc.setEnableCache(false);
        gc.setSwagger2(true);
        // 自动打开输出目录
        gc.setOpen(false);
        gc.setAuthor(generateConfig.getAuthor());
        //主键策略
        gc.setIdType(IdType.ASSIGN_ID);

        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        if (generateConfig.getGeneratorInterface()) {
            gc.setServiceName("%sService");
            gc.setServiceImplName("%sServiceImpl");
        } else {
            gc.setServiceName("%sService");
            gc.setServiceImplName("%sService");
        }
        mpg.setGlobalConfig(gc);

        Map<String, DataSourceProperty> dataSourceMap = dynamicDataSourceProperties.getDatasource();
        DataSourceProperty dataSourceProperty= (DataSourceProperty) dataSourceMap.get(database);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setDriverName(dataSourceProperty.getDriverClassName());
        dsc.setUsername(dataSourceProperty.getUsername());
        dsc.setPassword(dataSourceProperty.getPassword());
        dsc.setUrl(dataSourceProperty.getUrl());
        dsc.setTypeConvert(new MySqlTypeConvert() {
            // 自定义数据库表字段类型转换【可选】
            @Override
            public DbColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                //tinyint转换成Boolean
                if (fieldType.toLowerCase().contains("tinyint")) {
                    return DbColumnType.BOOLEAN;
                }
                //将数据库中datetime转换成date
                if (fieldType.toLowerCase().contains("datetime")) {
                    return DbColumnType.DATE;
                }
                //将数据库中datetime转换成date
                if (fieldType.toLowerCase().contains("date")) {
                    return DbColumnType.DATE;
                }
                return (DbColumnType) super.processTypeConvert(globalConfig, fieldType);
            }
        });
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setCapitalMode(false);
        strategy.setEntityLombokModel(true);
        strategy.setEntityTableFieldAnnotationEnable(false);
        strategy.setControllerMappingHyphenStyle(true);
        // 此处可以移除表前缀表前缀
        strategy.setTablePrefix(generateConfig.getRemoveTablePrefix());


        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setRestControllerStyle(true);
        // 字段生成策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setSuperEntityColumns("createTime", "updateTime", "createUser", "updateUser");
        // mapper 父类
        strategy.setSuperMapperClass("com.meida.common.mybatis.base.mapper.SuperMapper");
        // 实体父类
        strategy.setSuperEntityClass("com.meida.common.mybatis.base.entity.AbstractEntity");
        strategy.setColumnNaming(NamingStrategy.no_change);
        // 接口父类
        strategy.setSuperServiceClass("com.meida.common.mybatis.base.service.IBaseService");
        // 接口实现类父类
        strategy.setSuperServiceImplClass("com.meida.common.mybatis.base.service.impl.BaseServiceImpl");

        strategy.setSuperControllerClass("com.meida.common.springmvc.base.BaseController");

        // 需要生成的表
        // strategy.setInclude(generateConfig.getIncludeTables());
        // 需要生成的表
        strategy.setInclude(tableName);
        // 公共字段填充
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(new TableFill("createTime", FieldFill.INSERT));
        tableFills.add(new TableFill("updateTime", FieldFill.UPDATE));
        tableFills.add(new TableFill("createUser", FieldFill.INSERT));
        tableFills.add(new TableFill("updateUser", FieldFill.UPDATE));
        strategy.setTableFillList(tableFills);


        mpg.setStrategy(strategy);


        // 包配置
        PackageConfig pc = new PackageConfig();
        //父包名
        pc.setParent(generateConfig.getParentPackage());
        //父包模块名
        //pc.setModuleName(generateConfig.getModuleName());
        //实体类父包
        pc.setModuleName(generateConfig.getModuleName().substring(generateConfig.getModuleName().lastIndexOf("-") + 1));
        pc.setEntity("client.entity");
        //controller父包
        pc.setController("provider.controller");
        //mapper父包
        pc.setMapper("provider.mapper");
        //xml父包
        pc.setXml("provider.mapper.xml");
        //service父包
        if (generateConfig.getGeneratorInterface()) {
            pc.setServiceImpl("provider.service.impl");
            pc.setService("provider.service");
        } else {
            pc.setServiceImpl("provider.service");
            pc.setService("provider.service");
        }

        // 自定义配置
        // 注入自定义配置，可以在 模板 中使用 cfg.alias 【可无】
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map map = new HashMap<String, Object>(1);
                String alias = getAlias(tableName);
                initCustomMap(generateConfig, map);
                map.put("alias", alias);
                map.put("apiPackName", pc.getParent() + ".client.api");
                this.setMap(map);
            }
        };
        String jsPath = "/templates/vm/api.js.vm";
        String indexPath = "/templates/vm/index.vue.vm";
        String drawerPath = "/templates/vm/drawer.vue.vm";
        String formPath = "/templates/vm/form.vue.vm";
        List<FileOutConfig> focList = new ArrayList<>();

        String webDirectory = generateConfig.getWebDirectory();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(jsPath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return webDirectory + "/src/views/module/" + generateConfig.getPageModule()
                        + "/" + getAlias(tableName) + "/js/" + "api.js";
            }
        });
        if (generateConfig.getCreateIndex()) {
            focList.add(new FileOutConfig(indexPath) {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    return webDirectory + "/src/views/module/" + generateConfig.getPageModule()
                            + "/" + getAlias(tableName) + "/index.vue";
                }
            });
        }
        if (generateConfig.getCreateForm()) {
            focList.add(new FileOutConfig(formPath) {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    return webDirectory + "/src/views/module/" + generateConfig.getPageModule()
                            + "/" + getAlias(tableName) + "/" + getAlias(tableName) + "_form.vue";
                }
            });
        }
        focList.add(new FileOutConfig(drawerPath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return webDirectory + "/src/views/module/" + generateConfig.getPageModule()
                        + "/" + getAlias(tableName) + "/" + getAlias(tableName) + "_drawer.vue";
            }
        });
        String projectPath = generateConfig.getProjectPath();
        if (ObjectUtils.isEmpty(projectPath)) {
            String path = System.getProperty("user.dir");
            if (generateConfig.getBaseProject()) {
                projectPath = path.substring(0, path.lastIndexOf("\\") - 10);
            } else {
                projectPath = path.substring(0, path.lastIndexOf("\\"));
            }
        }

        String finalProjectPath = projectPath;
        // 调整 xml 生成目录
        focList.add(new FileOutConfig("/templates/vm/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return finalProjectPath + getModuleName(generateConfig) + "-provider/" + getPackName(pc, pc.getXml()) + tableInfo.getXmlName() + ".xml";
            }
        });

        // 调整 service 生成目录
        focList.add(new FileOutConfig("/templates/vm/service.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return finalProjectPath + getModuleName(generateConfig) + "-provider/" + getPackName(pc, pc.getService()) + tableInfo.getServiceName() + ".java";
            }
        });

        if (generateConfig.getCreateService()) {
            // 调整 serviceImpl 生成目录
            focList.add(new FileOutConfig("/templates/vm/serviceImpl.java.vm") {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    return finalProjectPath + getModuleName(generateConfig) + "-provider/" + getPackName(pc, pc.getServiceImpl()) + tableInfo.getServiceImplName() + ".java";
                }
            });
        }
        // 调整 mapper 生成目录
        focList.add(new FileOutConfig("/templates/vm/mapper.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return finalProjectPath + getModuleName(generateConfig) + "-provider/" + getPackName(pc, pc.getMapper()) + tableInfo.getMapperName() + ".java";
            }
        });

        if (generateConfig.getCreateController()) {
            // 调整 controller 生成目录
            focList.add(new FileOutConfig("/templates/vm/controller.java.vm") {
                @Override
                public String outputFile(TableInfo tableInfo) {
                    return finalProjectPath + getModuleName(generateConfig) + "-provider/" + getPackName(pc, pc.getController()) + tableInfo.getControllerName() + ".java";
                }
            });
        }
        if (generateConfig.getCreateEntity()) {
            // 调整 entity 生成目录
            focList.add(new FileOutConfig("/templates/vm/entity.java.vm") {
                @Override
                public String outputFile(TableInfo tableInfo) {

                    return finalProjectPath + getModuleName(generateConfig) + "-client/" + getPackName(pc, pc.getEntity()) + tableInfo.getEntityName() + ".java";
                }
            });
        }

        // 调整 entity 生成目录演示
        focList.add(new FileOutConfig("/templates/vm/interface.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return finalProjectPath + getModuleName(generateConfig) + "-client/" + getPackName(pc, "client.api") + tableInfo.getEntityName() + "RemoteApi.java";
            }
        });


        // 自定义模板配置
        // 放置自己项目的 src/main/resources/templates/vm 目录下, 默认名称可以不配置，也可以自定义模板名称
        TemplateConfig tc = new TemplateConfig();
        tc.setService(null);
        tc.setServiceImpl(null);
        tc.setMapper(null);
        tc.setController(null);
        tc.setEntity(null);
        tc.setXml(null);
        //如上任何一个模块如果设置 空 OR Null 将不生成该模块。
        mpg.setTemplate(tc);
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
        mpg.setPackageInfo(pc);
        // 执行生成
        mpg.execute();
        initMenu(generateConfig);
    }


    public static String getPackName(PackageConfig pc, String name) {
        String clientBasePath = pc.getParent() + "." + name;
        return "src/main/java/" + clientBasePath.replace(".", "/") + "/";
    }

    public static String getModuleName(GenerateConfig config) {
        String path = "";
        if (ObjectUtils.isEmpty(config.getModuleName())) {
            path = "/" + config.getBaseModule() + "/" + config.getBaseModule();
        } else {
            path = "/" + config.getBaseModule() + "/" + config.getModuleName() + "/" + config.getModuleName();
        }
        return path;
    }

    private void initCustomMap(GenerateConfig generateConfig, Map map) {
        String fields = generateConfig.getFields();
        List<GenField> listField = new ArrayList<>();
        List<GenField> formList = new ArrayList<>();
        List<GenField> searchList = new ArrayList<>();
        List<GenField> slotList = new ArrayList<>();
        List<GenField> methodList = new ArrayList<>();
        Boolean hasUpload = false;
        if (ObjectUtils.isNotEmpty(fields)) {
            List<GenField> genFieldList = toList(fields, GenField.class);
            for (GenField genField : genFieldList) {
                Boolean isList = genField.getIsList();
                Boolean isSearch = genField.getIsSearch();
                String listSlot = genField.getListSlot();
                Boolean isAdd = genField.getIsAdd();
                String showType = genField.getShowType();
                String propertyType = genField.getPropertyType();
                if (isAdd) {
                    formList.add(genField);
                }
                if (isList) {
                    listField.add(genField);
                }
                if (isSearch) {
                    searchList.add(genField);
                }
                if (ObjectUtils.isNotEmpty(listSlot) && !listSlot.equals("text")) {
                    slotList.add(genField);
                }
                if (listSlot.equals("setState") || listSlot.equals("setSortOrder")) {
                    methodList.add(genField);
                }
                if (showType.equals("upload")) {
                    hasUpload = true;
                }
                if (propertyType.equals("Integer")) {
                    genField.setDataType("number");
                }
            }
            map.put("rowKey", generateConfig.getRowKey());
            map.put("addMethod", generateConfig.getAddMethod());
            map.put("editMethod", generateConfig.getEditMethod());
            map.put("delMethod", generateConfig.getDelMethod());
            map.put("batchDelMethod", generateConfig.getBatchDelMethod());
            map.put("exportMethod", generateConfig.getExportMethod());
            map.put("multiple", generateConfig.getBatchDelMethod());
            map.put("listField", listField);
            map.put("slotList", slotList);
            map.put("searchList", searchList);
            map.put("methodList", methodList);
            map.put("formList", formList);
            map.put("hasUpload", hasUpload);
        }
    }


    /**
     * json 转lsit
     *
     * @param json
     * @param c
     * @return
     */
    public static <T> List<T> toList(String json, Class<T> c) {
        JSONArray jsonArray = JSONUtil.parseArray(json);
        return JSONUtil.toList(jsonArray, c);
    }
    private void initMenu(GenerateConfig generateConfig) {
        Boolean createMenu = generateConfig.getCreateMenu();
        if (createMenu) {
            String tableName = generateConfig.getTableName();
            String parentMenuName = generateConfig.getParentMenuName();
            String menuName = generateConfig.getMenuName();
            Long parentId = generateConfig.getParentId();
            if(ObjectUtils.isEmpty(parentId)){
                parentId=0L;
            }

            String path = generateConfig.getPageModule() + "/" + getAlias(tableName) + "/index";
            String sql = "INSERT INTO `base_menu`  VALUES (?, ?,?, ?, ?,?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?);";
            String sql2 = "INSERT INTO `base_authority`(`authorityId`, `authority`, `menuId`,  `status`, `createTime`, `updateTime`) VALUES (?, ?, ?, ?, ?, ?);";
            try {
                SqlRunner sqlRunner = new SqlRunner(dataSource.getConnection());
                String dateTime = "";
                Long id = IdWorker.getId();
                if (ObjectUtils.isNotEmpty(parentMenuName) && parentId.equals(0L)) {
                    String parentsql = "INSERT INTO `base_menu`  VALUES (?, ?,?, ?, ?,?, ?, ?, ?,?, ?, ?, ?, ?, ?,?,?);";
                    String parentsql2 = "INSERT INTO `base_authority`(`authorityId`, `authority`, `menuId`,  `status`, `createTime`, `updateTime`) VALUES (?, ?, ?, ?, ?, ?);";
                    Long pId = IdWorker.getId();
                    sqlRunner.insert(parentsql, pId, 0, generateConfig.getPageModule(), menuName, menuName, "/", "", "bars", "_self", "PageView", 1, 99, 1, 0, "meida-base-provider", dateTime, dateTime);
                    sqlRunner.insert(parentsql2, pId, "MENU_" + generateConfig.getPageModule(), pId, 1, dateTime, dateTime);
                    parentId = pId;
                }
                sqlRunner.insert(sql, id, parentId, getAlias(tableName), menuName, menuName, "/", path, "bars", "_self", "PageView", 1, 99, 1, 0, "meida-base-provider", dateTime, dateTime);
                sqlRunner.insert(sql2, id, "MENU_" + getAlias(tableName), id, 1, dateTime, dateTime);
                sqlRunner.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static String getAlias(String tableName) {
        String[] str = tableName.split("_");
        String alias = "";
        if (str.length > 2) {
            for (String s : str) {
                alias += s.substring(0, 1);
            }
        } else {
            alias = str[str.length - 1];
        }
        return alias;
    }
}