package com.meida.generator.core;

import lombok.Data;

import java.io.Serializable;
@Data
public class GenField  implements Serializable {

     /**
     * 描述
     */
    private  String comment;

    /**
     * 是否主键
     */
    private  Boolean keyFlag;
    /**
     * 数据库字段名
     */
    private String name;
    /**
     * java属性名
     */
    private  String propertyName;

    /**
     * java属性名(首字母大写)
     */
    private  String capitalName;
    /**
     * java类型
     */
    private  String  propertyType;
    /**
     * 是否添加
     */
    private Boolean isAdd;
    /**
     * 是否可编辑
     */
    private  Boolean isEdit;
    /**
     * 查询条件
     */
    private Boolean isSearch;
    /**
     * 列表展示
     */
    private Boolean isList;

    /**
     * 列表字段渲染插槽
     */
    private String listSlot;
    /**
     * 是否必填
     */
    private Boolean isRequired;

    /**
     * 导出是否合计
     */
    private Boolean isStatistics;
    /**
     * 查询方式
     */
    private  String searchType;
    /**
     * 表单控件
     */
    private String showType;

    /**
     * 数据类型
     */
    private String dataType;


}
