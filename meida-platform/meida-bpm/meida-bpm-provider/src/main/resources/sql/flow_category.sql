/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 23/07/2020 23:09:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for flow_category
-- ----------------------------
DROP TABLE IF EXISTS `flow_category`;
CREATE TABLE `flow_category`  (
  `categoryId` bigint(20) NOT NULL COMMENT '主键',
  `categoryName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `comapnyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`categoryId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程分类表' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (702, 700, 'category', '流程分类', '流程分类', '/', 'flowable/category/index', 'bars', '_self', 'PageView', 1, 99, 1, 0, 'meida-base-provider', '2020-06-14 21:58:48', '2020-06-14 21:58:48');
INSERT INTO `base_authority` VALUES (702, 'MENU_category', 702, NULL, NULL, 1, '2020-06-14 21:58:48', '2020-06-14 21:58:48');

SET FOREIGN_KEY_CHECKS = 1;
