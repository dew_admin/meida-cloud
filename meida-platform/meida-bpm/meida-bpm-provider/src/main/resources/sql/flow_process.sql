/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 23/07/2020 23:10:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for flow_process
-- ----------------------------
DROP TABLE IF EXISTS `flow_process`;
CREATE TABLE `flow_process`  (
  `processId` bigint(20) NOT NULL COMMENT '主键',
  `processKey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程key',
  `processTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程标题',
  `processTypeId` bigint(20) NULL DEFAULT NULL COMMENT '分类ID',
  `processState` int(1) NULL DEFAULT NULL COMMENT '流程状态',
  `processVersion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `formId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单Id',
  `categoryId` bigint(20) NULL DEFAULT NULL COMMENT '分类Id',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `procDefId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程定义ID',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程描述',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`processId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '流程表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_process
-- ----------------------------

INSERT INTO `base_menu` VALUES (700, 0, 'processManager', '流程管理', '', '/', '', 'share-alt', '_self', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-06-11 15:55:50', NULL);
INSERT INTO `base_authority` VALUES (700, 'MENU_processManager', 700, NULL, NULL, 1, '2020-06-11 15:55:50', NULL);

INSERT INTO `base_menu` VALUES (701, 700, 'process', '流程设计', '流程设计', '/', 'flowable/process/index', 'bars', '_self', 'PageView', 1, 99, 1, 0, 'meida-base-provider', '2020-06-11 15:50:34', '2020-06-11 15:50:34');
INSERT INTO `base_authority` VALUES (701, 'MENU_process', 701, NULL, NULL, 1, '2020-06-11 15:50:34', '2020-06-11 15:50:34');

INSERT INTO `base_menu` VALUES (702, 700, 'processInstance', '流程实例', '', '/', 'flowable/processInstance/index', 'bars', '_self', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-07-14 14:17:16', NULL);
INSERT INTO `base_authority` VALUES (702, 'MENU_processInstance', 702, NULL, NULL, 1, '2020-07-14 14:17:16', NULL);


INSERT INTO `base_menu` VALUES (800, 0, 'task', '任务管理', '', '/', '', 'bars', '_self', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-07-02 21:58:30', NULL);
INSERT INTO `base_authority` VALUES (800, 'MENU_task', 800, NULL, NULL, 1, '2020-07-02 21:58:30', NULL);

INSERT INTO `base_menu` VALUES (801, 800, 'taskTodo', '我的待办', '', '/', 'flowable/tasktodo/index', 'bars', '_self', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-07-02 22:06:00', NULL);
INSERT INTO `base_authority` VALUES (801, 'MENU_taskTodo', 801, NULL, NULL, 1, '2020-07-02 22:06:00', NULL);

INSERT INTO `base_menu` VALUES (803, 800, 'startProcess', '发起流程', '', '/', 'flowable/startProcess/index', 'bars', '_self', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-07-14 14:46:22', NULL);
INSERT INTO `base_authority` VALUES (803, 'MENU_startProcess', 803, NULL, NULL, 1, '2020-07-14 14:46:22', NULL);


SET FOREIGN_KEY_CHECKS = 1;
