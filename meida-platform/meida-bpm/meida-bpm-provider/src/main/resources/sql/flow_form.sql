/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 23/07/2020 23:10:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for flow_form
-- ----------------------------
DROP TABLE IF EXISTS `flow_form`;
CREATE TABLE `flow_form`  (
  `formId` bigint(20) NOT NULL COMMENT '注解',
  `formKey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单key',
  `formName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单名称',
  `formJson` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '表单json',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `createUser` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
  `updateUser` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
  PRIMARY KEY (`formId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form
-- ----------------------------
INSERT INTO `flow_form` VALUES (1, 'leaveApply', '请假表单', '{\r\n	\"list\": [\r\n		{\r\n			\"type\": \"select\",\r\n			\"label\": \"请假类型\",\r\n			\"options\": {\r\n				\"width\": \"100%\",\r\n				\"multiple\": false,\r\n				\"disabled\": false,\r\n				\"clearable\": false,\r\n				\"hidden\": false,\r\n				\"placeholder\": \"请选择请假类型\",\r\n				\"dynamicKey\": \"\",\r\n				\"dynamic\": false,\r\n				\"options\": [\r\n					{\r\n						\"value\": \"1\",\r\n						\"label\": \"婚假\"\r\n					},\r\n					{\r\n						\"value\": \"2\",\r\n						\"label\": \"事假\"\r\n					}\r\n				],\r\n				\"showSearch\": false\r\n			},\r\n			\"model\": \"levelType\",\r\n			\"key\": \"select_1594775954208\",\r\n			\"rules\": [\r\n				{\r\n					\"required\": false,\r\n					\"message\": \"必填项\"\r\n				}\r\n			]\r\n		},\r\n		{\r\n			\"type\": \"date\",\r\n			\"label\": \"开始日期\",\r\n			\"options\": {\r\n				\"width\": \"100%\",\r\n				\"defaultValue\": \"\",\r\n				\"rangeDefaultValue\": [],\r\n				\"range\": false,\r\n				\"showTime\": false,\r\n				\"disabled\": false,\r\n				\"hidden\": false,\r\n				\"clearable\": false,\r\n				\"placeholder\": \"请选择开始日期\",\r\n				\"rangePlaceholder\": [\r\n					\"开始时间\",\r\n					\"结束时间\"\r\n				],\r\n				\"format\": \"YYYY-MM-DD\"\r\n			},\r\n			\"model\": \"beginDate\",\r\n			\"key\": \"date_1594775924261\",\r\n			\"rules\": [\r\n				{\r\n					\"required\": true,\r\n					\"message\": \"必填项\"\r\n				}\r\n			]\r\n		},\r\n		{\r\n			\"type\": \"date\",\r\n			\"label\": \"结束日期\",\r\n			\"options\": {\r\n				\"width\": \"100%\",\r\n				\"defaultValue\": \"\",\r\n				\"rangeDefaultValue\": [],\r\n				\"range\": false,\r\n				\"showTime\": false,\r\n				\"disabled\": false,\r\n				\"hidden\": false,\r\n				\"clearable\": false,\r\n				\"placeholder\": \"请选择结束日期\",\r\n				\"rangePlaceholder\": [\r\n					\"开始时间\",\r\n					\"结束时间\"\r\n				],\r\n				\"format\": \"YYYY-MM-DD\"\r\n			},\r\n			\"model\": \"endDate\",\r\n			\"key\": \"date_1594775987308\",\r\n			\"rules\": [\r\n				{\r\n					\"required\": true,\r\n					\"message\": \"必填项\"\r\n				}\r\n			]\r\n		},\r\n		{\r\n			\"type\": \"textarea\",\r\n			\"label\": \"请假事项\",\r\n			\"options\": {\r\n				\"width\": \"100%\",\r\n				\"minRows\": 4,\r\n				\"maxRows\": 6,\r\n				\"maxLength\": null,\r\n				\"defaultValue\": \"\",\r\n				\"clearable\": false,\r\n				\"hidden\": false,\r\n				\"disabled\": false,\r\n				\"placeholder\": \"请输入请假缘由\"\r\n			},\r\n			\"model\": \"remark\",\r\n			\"key\": \"textarea_1594776010749\",\r\n			\"rules\": [\r\n				{\r\n					\"required\": true,\r\n					\"message\": \"必填项\"\r\n				}\r\n			]\r\n		}\r\n	],\r\n	\"config\": {\r\n		\"layout\": \"horizontal\",\r\n		\"labelCol\": {\r\n			\"span\": 4\r\n		},\r\n		\"wrapperCol\": {\r\n			\"span\": 18\r\n		},\r\n		\"hideRequiredMark\": false,\r\n		\"customStyle\": \"\"\r\n	}\r\n}', '2020-06-11 17:28:53', NULL, '2020-06-11 17:28:55', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
