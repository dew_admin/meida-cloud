package com.meida.bpm.provider.custom.entity;

import lombok.Data;

import java.util.List;

/**
 * 条件分组
 *
 * @author zyf
 */
@Data
public class ConditionGroup {
    /**
     * 条件ID
     */
    private String id;
    /**
     * 条件表达式
     */
    private String matchType;
    /**
     * 条件集合
     */
    private List<Conditions> queryItems;

}
