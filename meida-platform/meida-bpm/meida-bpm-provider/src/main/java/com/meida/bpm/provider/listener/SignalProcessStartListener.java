package com.meida.bpm.provider.listener;

import com.meida.bpm.provider.service.FlowBusinessService;
import com.meida.common.utils.SpringContextHolder;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 信号启动执行监听
 */
@Component
public class SignalProcessStartListener implements ExecutionListener {
    @Autowired
    private FlowBusinessService flowBusinessService;

    @Autowired
    private RuntimeService runtimeService;

    @Override
    public void notify(DelegateExecution delegateExecution) {

        //流程实例ID
        String processInstanceId = delegateExecution.getProcessInstanceId();
        //业务主键
        Long businessKey = delegateExecution.getVariable("businessKey", Long.class);
        //发起人
        Long applyUserId = delegateExecution.getVariable("applyUserId", Long.class);
        //更新业务主键
        runtimeService.updateBusinessKey(processInstanceId, businessKey+"");
        //关联流程实例ID
        flowBusinessService.updateProcessInstanceId(processInstanceId,businessKey,0);

    }
}
