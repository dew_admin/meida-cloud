package com.meida.bpm.provider.custom.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.provider.custom.entity.*;
import com.meida.bpm.provider.custom.enums.QueryRuleEnum;
import com.meida.bpm.provider.custom.model.ApproverModel;
import com.meida.bpm.provider.custom.model.CustomProcessForm;
import com.meida.bpm.provider.custom.model.ListenerModel;
import com.meida.bpm.provider.custom.node.GateWayNodeCreator;
import com.meida.bpm.provider.custom.node.TaskNodeCreator;
import com.meida.bpm.provider.custom.node.TimeEventCreator;
import com.meida.bpm.provider.custom.node.UserTaskNodeCreator;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.exception.OpenAlertException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 流程设计json转换
 *
 * @Author: zyf
 * @Date: 2022/07/26
 */
public class CustomFlowProcessUtil {
    public static String JSONToBPMN(FlowProcess flowProcess, CustomProcessForm customProcessForm, List<FlowProcessNode> processNodes) {
        ProcessData process = JSON.parseObject(flowProcess.getProcessJson(), ProcessData.class);
        //是否创建发起人节点
        Boolean createStartNode = process.getCreateStartNode();
        if (ObjectUtils.isNotEmpty(createStartNode)&&createStartNode) {
            createApplyUserNode(process);
        }


        // 解析json
        List<ChildNode> allChildNodes = new ArrayList<>();

        List<ConditionNode> conditions = new ArrayList<>();

        if (process.getConditionNodes() != null) {
            getConditionNode("",process.getConditionNodes(), allChildNodes, conditions);
        }
        if (process.getChildNode() != null) {
            getChildNode("","", process.getChildNode(), allChildNodes, conditions);
        }


        // 用户任务
        List<ChildNode> userTaskNodes = allChildNodes.stream().filter(childNode -> "approver".equals(childNode.getType()) || childNode.getType().equals("edit")).collect(Collectors.toList());

        // 服务任务
        List<ChildNode> serviceTaskNodes = allChildNodes.stream().filter(childNode -> "service".equals(childNode.getType())).collect(Collectors.toList());

        // 消息服务节点
        List<ChildNode> messageNodes = allChildNodes.stream().filter(childNode -> childNode.getType().indexOf("message") > -1 || childNode.getType().indexOf("data_") > -1).collect(Collectors.toList());

        // 脚本任务
        List<ChildNode> scriptTaskNodes = allChildNodes.stream().filter(childNode -> "script".equals(childNode.getType())).collect(Collectors.toList());

        // 子流程
        List<ChildNode> callActivityNodes = allChildNodes.stream().filter(childNode -> "callActivity".equals(childNode.getType())).collect(Collectors.toList());


        //互斥网关
        List<ChildNode> exclusiveGateWayNodes = allChildNodes.stream().filter(childNode -> "exclusive".equals(childNode.getType()) || "databranch".equals(childNode.getType())).collect(Collectors.toList());

        //聚合网关
        List<ChildNode> polymerizeGateWayNodes = allChildNodes.stream().filter(childNode -> ("polymerize".equals(childNode.getType()) || "parallel".equals(childNode.getType()))).collect(Collectors.toList());

        //相容网关
        List<ChildNode> inclusiveGateWayNodes = allChildNodes.stream().filter(childNode -> ("inclusive".equals(childNode.getType()) || "inclusive_end".equals(childNode.getType()))).collect(Collectors.toList());

        //延时节点
        List<ChildNode> timerEventNodes = allChildNodes.stream().filter(childNode -> ("timer_event".equals(childNode.getType()))).collect(Collectors.toList());


        Map<String, ChildNode> approverMap = userTaskNodes.stream().collect(Collectors.toMap(ChildNode::getId, ChildNode -> ChildNode));
        Map<String, ConditionNode> conditionMap = conditions.stream().collect(Collectors.toMap(ConditionNode::getId, ConditionNode -> ConditionNode));


        // 创建userTask
        List<UserTask> userTasks = UserTaskNodeCreator.createUserTask(process,userTaskNodes,processNodes);

        // 创建排他网关
        List<ExclusiveGateway> gateways = GateWayNodeCreator.createExclusiveGateway(exclusiveGateWayNodes);

        //服务任务
        List<ServiceTask> serviceTasks = TaskNodeCreator.createServiceTask(serviceTaskNodes,processNodes);

        //消息节点
        List<ServiceTask> messageTasks = TaskNodeCreator.createMessageTask(messageNodes,processNodes);

        //脚本任务
        List<ScriptTask> scriptTasks = TaskNodeCreator.createScripTask(scriptTaskNodes,processNodes);

        //子流程
        List<CallActivity> callActivitys = TaskNodeCreator.createCallActivity(callActivityNodes);

        //聚合节点
        List<ParallelGateway> polymerizeGateWays = GateWayNodeCreator.createParallelGateway(polymerizeGateWayNodes);

        //相容节点
        List<InclusiveGateway> inclusiveGateways = GateWayNodeCreator.createInclusiveGateway(inclusiveGateWayNodes);

        //延时节点
        List<IntermediateCatchEvent> timerEvents = TimeEventCreator.createIntermediateCatchEvent(timerEventNodes);

        // 创建连线
        List<SequenceFlow> taskFlows = new ArrayList<SequenceFlow>();
        createTaskFlow(process, userTaskNodes, allChildNodes, conditions, taskFlows);
        createTaskFlow(process, serviceTaskNodes, allChildNodes, conditions, taskFlows);
        createTaskFlow(process, messageNodes, allChildNodes, conditions, taskFlows);
        createTaskFlow(process, scriptTaskNodes, allChildNodes, conditions, taskFlows);
        createTaskFlow(process, callActivityNodes, allChildNodes, conditions, taskFlows);
        createConditionFlow(process, allChildNodes, conditions, taskFlows);
        createGateWaysFlow(process, polymerizeGateWayNodes, allChildNodes, conditions, taskFlows);
        createGateWaysFlow(process, inclusiveGateWayNodes, allChildNodes, conditions, taskFlows);
        createEventFlow(process, timerEventNodes, allChildNodes, conditions, taskFlows);

        // 创建start节点的箭头
        String startNextId = nextNodeId(process, process, allChildNodes, conditions);
        SequenceFlow startFlow = new SequenceFlow();
        startFlow.setSourceRef(process.getId());
        startFlow.setTargetRef(startNextId);
        taskFlows.add(startFlow);

        return BpmnCreator.createXML(processNodes,flowProcess,customProcessForm,process, userTasks, serviceTasks, gateways, polymerizeGateWays, inclusiveGateways, scriptTasks, callActivitys, taskFlows, timerEvents, messageTasks);
    }

    /**
     * 查询指定的childNode
     *
     * @param json
     * @param taskDefKey
     * @return
     */
    public static ChildNode getChildNode(String json, String taskDefKey) {
        FlowDesignerModel model = JSON.parseObject(json, FlowDesignerModel.class);
        ProcessData process = model.getProcessData();
        List<ChildNode> allChildNodes = new ArrayList<ChildNode>();
        List<ConditionNode> conditions = new ArrayList<ConditionNode>();
        getChildNode("","", process.getChildNode(), allChildNodes, conditions);
        for (ChildNode node : allChildNodes) {
            if (node.getId().equals(taskDefKey)) {
                return node;
            }
        }
        return null;
    }

    /**
     * 创建分支连线
     *
     * @param process
     * @param allChildNodes
     * @param conditions
     * @param taskFlows
     */
    private static void createConditionFlow(ProcessData process, List<ChildNode> allChildNodes, List<ConditionNode> conditions, List<SequenceFlow> taskFlows) {
        for (ConditionNode condition : conditions) {
            SequenceFlow flow = new SequenceFlow();
            flow.setSourceRef(condition.getPid());
            flow.setTargetRef(nextNodeId(condition, process, allChildNodes, conditions));
            flow.setName(condition.getName());
            if (!condition.isDefault()) {
                String content = getConditions(condition);
                if (StringUtils.isNotEmpty(content)) {
                    if (content.contains("@@@")) {
                        flow.setConditionExpression("${branchUtils.getConditionValue(execution.getId(),execution.getProcessInstanceId(),'" + content + "')}");
                    } else {
                        flow.setConditionExpression(content);
                    }
                }
            }
            taskFlows.add(flow);
        }
    }

    /**
     * 创建任务连线
     *
     * @param process
     * @param taskNodes
     * @param allChildNodes
     * @param conditions
     * @param taskFlows
     */
    private static void createTaskFlow(ProcessData process, List<ChildNode> taskNodes, List<ChildNode> allChildNodes, List<ConditionNode> conditions,
                                       List<SequenceFlow> taskFlows) {
        for (ChildNode node : taskNodes) {
            String targetRef = nextNodeId(node, process, allChildNodes, conditions);
            SequenceFlow flow = new SequenceFlow();
            flow.setSourceRef(node.getId());
            flow.setTargetRef(targetRef);
            if(!node.getId().equals(targetRef)) {
                taskFlows.add(flow);
            }
        }
    }

    /**
     * 创建网关连线
     *
     * @param process
     * @param gateWayNodes
     * @param allChildNodes
     * @param conditions
     * @param taskFlows
     */
    private static void createGateWaysFlow(ProcessData process, List<ChildNode> gateWayNodes, List<ChildNode> allChildNodes, List<ConditionNode> conditions, List<SequenceFlow> taskFlows) {
        for (ChildNode node : gateWayNodes) {
            String targetRef = nextNodeId(node, process, allChildNodes, conditions);
            SequenceFlow flow = new SequenceFlow();
            flow.setSourceRef(node.getId());
            flow.setTargetRef(targetRef);
            //避免自己连自己
            if (!node.getId().equals(targetRef)) {
                taskFlows.add(flow);
            }
        }
    }

    /**
     * 创建事件连线
     *
     * @param process
     * @param timerEvents
     * @param allChildNodes
     * @param conditions
     * @param taskFlows
     */
    private static void createEventFlow(ProcessData process, List<ChildNode> timerEvents, List<ChildNode> allChildNodes, List<ConditionNode> conditions, List<SequenceFlow> taskFlows) {
        for (ChildNode node : timerEvents) {
            String targetRef = nextNodeId(node, process, allChildNodes, conditions);
            SequenceFlow flow = new SequenceFlow();
            flow.setSourceRef(node.getId());
            flow.setTargetRef(targetRef);
            //避免自己连自己
            if (!node.getId().equals(targetRef)) {
                taskFlows.add(flow);
            }
        }
    }


    /**
     * 获取node的下一个节点
     **/
    private static String nextNodeId(Node currentNode, ProcessData process, List<ChildNode> allChildNodes, List<ConditionNode> conditions) {
        String nextId = "";

        // 当前节点有条件分流，则优先指向条件分流
        if (currentNode.getConditionNodes() != null && currentNode.getConditionNodes().size() > 0) {
            nextId = currentNode.getId();
        } else if (currentNode.getChildNode() != null) {
            // 当前节点没有条件分流，有childNode
            ChildNode nextNode = currentNode.getChildNode();
            if (nextNode.getType().equals("approver") || nextNode.getType().equals("edit")) {
                nextId = nextNode.getId();
            } else if (nextNode.getType().equals("empty")) {
                throw new OpenAlertException("json数据格式错误，父节点没有condition，这个不该是empty");
            } else {
                nextId = nextNode.getId();
                //throw new JeecgBootException("暂未实现");
            }
        } else {
            // 既没有childNode，也没有conditionNode.跳转到外层
            String outerNodeId = "";
            if (currentNode instanceof ChildNode) {
                outerNodeId = ((ChildNode) currentNode).getOuterNodeId();
            } else if (currentNode instanceof ConditionNode) {
                outerNodeId = ((ConditionNode) currentNode).getPid();
            }
            return getOuterNextNode(outerNodeId, process, allChildNodes, conditions);
        }

        return nextId;
    }

    /**
     * outer的下一个节点就是当前跳出的conditons, 所以这个next应该是找下下一个节点
     *
     * @return
     */
    private static String getOuterNextNode(String outerId, ProcessData process, List<ChildNode> allChildNodes, List<ConditionNode> conditions) {
        // 如果没有条件分支，也没有childNode,查询outerNextNode
        Map<String, ChildNode> childNodeMap = allChildNodes.stream().collect(Collectors.toMap(ChildNode::getId, ChildNode -> ChildNode));
        Map<String, ConditionNode> conditionNodeMap = conditions.stream().collect(Collectors.toMap(ConditionNode::getId, ConditionNode -> ConditionNode));
        if (StringUtils.isEmpty(outerId)) {
            // 当前节点是最外层，直接指向end
            return "end";
        } else {
            ChildNode outerNode = childNodeMap.get(outerId);
            ConditionNode condition = conditionNodeMap.get(outerId);
            ChildNode outerChildNode = null;
            String outerOuterNodeId = "";
            if (outerNode != null) {
                outerChildNode = outerNode.getChildNode();
                outerOuterNodeId = outerNode.getOuterNodeId();
            } else if (condition != null) {
                outerChildNode = condition.getChildNode();
                outerOuterNodeId = condition.getPid();
            } else {
                // 既非conditionNode ,又非childNode. 那只能是startNode
                if (process.getChildNode() != null) {
                    outerChildNode = process.getChildNode();
                    outerOuterNodeId = "";
                }
            }

            if (outerChildNode != null&&!outerChildNode.getType().equals("suggest")) {
                if (outerChildNode.getType().equals("empty")) {
                    // emptyNode本身不存在，所以需要查找下一个nextNodeId
                    return nextNodeId(outerChildNode, process, allChildNodes, conditions);
                } else {
                    // 非emptyNode，则返回nodeId
                    return outerChildNode.getId();
                }
            } else {
                // 跳转到外层的外层
                return getOuterNextNode(outerOuterNodeId, process, allChildNodes, conditions);
            }
        }
    }

    /**
     * 获取所有的任务节点
     *
     * @param outerNodeId
     * @param child
     * @param allChildNodes
     * @param conditions
     */
    private static void getChildNode(String pid,String outerNodeId, ChildNode child, List<ChildNode> allChildNodes, List<ConditionNode> conditions) {

        if (child.getType().equals("copy")) {
            throw new OpenAlertException("抄送功能暂未实现，请不要选择抄送");
        }
        //意见分支逻辑实现
        if(!child.getType().equals("suggest")) {
            allChildNodes.add(child);
        }else{
            pid=child.getPid();
        }
        //当节点是按钮时修改父id
        if(child.getPid().indexOf("btn")>-1){
            child.setPid(pid);
        }
        else{
            child.setOuterNodeId(outerNodeId);
        }
        //当节点是多分支时修改当前节点ID
        if(child.getId().indexOf("suggest")>-1){
            child.setId(pid);
        }
        //意见分支逻辑实现
        if (child.getChildNode() != null) {
            getChildNode(pid,outerNodeId, child.getChildNode(), allChildNodes, conditions);
        }
        if (child.getConditionNodes() != null && child.getConditionNodes().size() > 0) {
            getConditionNode(pid,child.getConditionNodes(), allChildNodes, conditions);
        }
    }

    /**
     * 获取条件节点
     *
     * @param conditionNodes
     * @param allChildNodes
     * @param conditions
     */
    private static void getConditionNode(String pid,List<ConditionNode> conditionNodes, List<ChildNode> allChildNodes, List<ConditionNode> conditions) {
        for (ConditionNode c : conditionNodes) {
            // 意见分支逻辑实现
            if(c.getId().indexOf("btn")>-1){
                c.setPid(pid);
            }
            conditions.add(c);
            if (c.getChildNode() != null) {
               getChildNode(pid,c.getPid(), c.getChildNode(), allChildNodes, conditions);
            }
            if (c.getConditionNodes() != null) {
                getConditionNode(pid,c.getConditionNodes(), allChildNodes, conditions);
            }
        }
    }

    /**
     * 创建发起人节点
     *
     * @param processData
     */
    private static void createApplyUserNode(ProcessData processData) {
        ChildNode childNode = new ChildNode();
        childNode.setId(processData.getStartTaskId());
        childNode.setName("发起人");
        childNode.setStatus(-1);
        childNode.setType("approver");
        childNode.getAttr().setApprovalMode("1");
        childNode.setAssigneeType("assigneeByExp");
        childNode.setApproverType("candidateUser");
        childNode.setPid(processData.getId());
        ApproverModel approverModel=new ApproverModel();
        approverModel.setApproverType("candidateUser");
        approverModel.setAssigneeType("assigneeByExp");
        approverModel.setExpressionsIds(new String[]{"${applyUserId}"});
        approverModel.setExpressionsNames(new String[]{"获取发起人"});
        approverModel.setLevelMode("1");
        List<ApproverModel> approverModels=new ArrayList<>();
        approverModels.add(approverModel);
        childNode.setApproverGroups(approverModels);
        ChildNode oldChildNode = processData.getChildNode();
        if(ObjectUtils.isNotEmpty(oldChildNode)) {
            oldChildNode.setPid(childNode.getId());
            childNode.setChildNode(oldChildNode);
        }
        List<ListenerModel> listenerModels = processData.getTaskListenerData();
        if (ObjectUtils.isNotEmpty(listenerModels)) {
            childNode.setTaskListenerData(listenerModels);
        }
        processData.setChildNode(childNode);
    }

    /**
     * 根据设置的表单规则计算分支条件
     *
     * @param conditionNode
     * @return
     */
    public static String getConditions(ConditionNode conditionNode) {
        List<ConditionGroup> groups = conditionNode.getAttr().getConditionGroup();
        return ObjectUtils.isNotEmpty(groups) ? getCondition(groups) : conditionNode.getContent();
    }

    /**
     * 获取条件表达式
     *
     * @param groups
     * @return
     */
    public static String getCondition(List<ConditionGroup> groups) {
        StringBuilder sb = new StringBuilder();
        String content = "";
        sb.append("${");
        int i = 1;
        if(ObjectUtils.isNotEmpty(groups)) {
            for (ConditionGroup group : groups) {
                int groupSize = groups.size();
                if (groupSize > 1) {
                    sb.append("(");
                }
                List<Conditions> conditions = group.getQueryItems();
                if (ObjectUtils.isNotEmpty(conditions)) {
                    int j = 1;
                    for (Conditions c : conditions) {
                        if (StringUtils.isNotEmpty(c.getField())) {
                            boolean valIsObj = false;
                            String conditionValue = c.getVal();
                            try {
                                JSONObject.parseObject(conditionValue);
                                valIsObj = true;
                            } catch (Exception e) {
                                valIsObj = false;
                            }
                            QueryRuleEnum optType = c.getRule();
                            if (ObjectUtils.isNotEmpty(optType)) {
                                if (optType.equals("empty")) {
                                    sb.append("(" + c.getField());
                                    sb.append("==null");
                                    sb.append(" || ");
                                    sb.append(c.getField());
                                    sb.append("==''");
                                    sb.append(")");
                                } else if (optType.equals("not_empty")) {
                                    sb.append("(" + c.getField());
                                    sb.append("!=null");
                                    sb.append(" && ");
                                    sb.append(c.getField());
                                    sb.append("!=''");
                                    sb.append(")");
                                } else {
                                    if(QueryRuleEnum.IN.equals(optType)) {
                                        sb.append("variables:containsAny('"+c.getField()+"',"+conditionValue+")");
                                    }else{
                                        sb.append(c.getField());
                                        if (QueryRuleEnum.EQ.equals(optType)) {
                                            sb.append("==");
                                        } else {
                                            sb.append(optType.getValue());
                                        }

                                        if (valIsObj) {
                                            sb.append("@@@" + conditionValue + "###");
                                        } else {
                                            if (QueryRuleEnum.IN.equals(optType)) {
                                                sb.append(conditionValue + ")");
                                            } else {
                                                String type = c.getType();
                                                if (FlymeUtils.isNumber(type) || StringUtils.isNumeric(conditionValue)) {
                                                    sb.append(conditionValue);
                                                } else {
                                                    sb.append("'" + conditionValue + "'");
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                            if (j < conditions.size()) {
                                sb.append(" && ");
                            }
                            j++;
                        }
                    }
                }
                if (groupSize > 1) {
                    sb.append(")");
                }
                if (i < groupSize) {
                    sb.append(" || ");
                }
                i++;
            }
            sb.append("}");
            content = sb.toString();
            //分支条件不存在时content需要置空
            if (content.equals("${}")) {
                content = "";
            }
        }
        return content;
    }


}
