package com.meida.bpm.provider.custom.util;

import com.meida.common.utils.ApiAssert;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("minFlowUtils")
@AllArgsConstructor
@Slf4j
public class MinFlowUtils {

    public List<String> stringToList(String content) {
        log.info("------stringToList------" + content);
        ApiAssert.isNotEmpty("下一任务是会签节点，请选择会签人员",content);
        String[] userIds = content.split(",");
        return Arrays.asList(userIds);
    }
}
