package com.meida.bpm.provider.custom.node;

import cn.hutool.core.util.IdUtil;
import com.meida.bpm.provider.custom.entity.ChildAttr;
import com.meida.bpm.provider.custom.entity.ChildNode;
import com.meida.bpm.provider.custom.enums.TimeTypeEnums;
import org.apache.commons.lang3.ObjectUtils;
import org.flowable.bpmn.model.BoundaryEvent;
import org.flowable.bpmn.model.IntermediateCatchEvent;
import org.flowable.bpmn.model.TimerEventDefinition;


import java.util.ArrayList;
import java.util.List;

public class TimeEventCreator {

    /**
     * 创建时间边界事件
     *
     * @param boundaryEventNodes
     * @return
     */
    public static List<BoundaryEvent> createBoundaryEvent(List<ChildNode> boundaryEventNodes) {
        List<BoundaryEvent> events = new ArrayList<>();
        for (ChildNode node : boundaryEventNodes) {
            ChildAttr childAttr = node.getAttr();
            BoundaryEvent boundaryEvent = new BoundaryEvent();
            boundaryEvent.setId(node.getId());
            boundaryEvent.setName(node.getName());
            events.add(boundaryEvent);
        }
        return events;
    }

    /**
     * 创建时间捕获事件
     *
     * @param childNodes
     * @return
     */
    public static List<IntermediateCatchEvent> createIntermediateCatchEvent(List<ChildNode> childNodes) {
        List<IntermediateCatchEvent> events = new ArrayList<>();
        for (ChildNode node : childNodes) {
            ChildAttr childAttr = node.getAttr();
            //指定日期
            String timeDate = childAttr.getTimeDate();
            //循环周期值
            String timeCycle = childAttr.getTimeCycle();
            //定时方式
            String timeType = childAttr.getTimeType();

            IntermediateCatchEvent intermediateCatchEvent = new IntermediateCatchEvent();
            intermediateCatchEvent.setId(node.getId());
            intermediateCatchEvent.setName(node.getName());
            TimerEventDefinition timerEventDefinition = new TimerEventDefinition();
            timerEventDefinition.setId("TimerEventDefinition_" + IdUtil.simpleUUID().substring(0,6));
            //指定时间
            if (ObjectUtils.isNotEmpty(timeType)) {
                if (TimeTypeEnums.timeDate.name().equals(timeType)) {
                    if (!"custom".equals(timeCycle)) {
                        timerEventDefinition.setTimeCycle(timeCycle);
                    } else {
                        timerEventDefinition.setTimeCycle(timeDate);
                    }
                }
                if (TimeTypeEnums.timeDuration.name().equals(timeType)) {
                    if (!"custom".equals(timeCycle)) {
                        timerEventDefinition.setTimeDuration(timeCycle);
                    } else {
                        timerEventDefinition.setTimeDuration(timeDate);
                    }
                }
                if (TimeTypeEnums.timeDate.name().equals(timeType)) {
                    timerEventDefinition.setTimeDate(timeDate);
                }
            }
            intermediateCatchEvent.addEventDefinition(timerEventDefinition);
            events.add(intermediateCatchEvent);
        }
        return events;
    }
}
