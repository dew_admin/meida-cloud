package com.meida.bpm.provider.mapper;

import com.meida.bpm.client.entity.FlowCategory;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 流程分类表 Mapper 接口
 * @author flyme
 * @date 2020-06-14
 */
public interface FlowCategoryMapper extends SuperMapper<FlowCategory> {

}
