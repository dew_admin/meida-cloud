package com.meida.bpm.provider.handler;

import com.meida.bpm.provider.model.TaskRequest;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.security.OpenUser;
import org.flowable.common.engine.api.query.Query;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;

import java.util.Map;

/**
 * 任务表单完成处理器
 *
 * @author zyf
 */
public interface TaskCompleteHandler {

    /**
     * 完成前处理
     *
     * @return
     */
    default Boolean completeBefore(TaskRequest taskRequest, ProcessInstance processInstance, Task task, Map<String, Object> completeVariables, OpenUser user) {
        return true;
    }


    /**
     * 获取任务表单数据
     *
     * @param processInstance
     * @param task
     * @param variables
     * @return
     */
    default  Map<String, Object> initTaskData(ProcessInstance processInstance, Task task, Map<String, Object> variables){
        return variables;
    }


    /**
     * 任务完成后处理器
     *
     * @param taskRequest
     * @param task
     * @param completeVariables
     */
    default void completeAfter(TaskRequest taskRequest, ProcessInstance processInstance, Task task, Map<String, Object> completeVariables, OpenUser user) {

    }
}
