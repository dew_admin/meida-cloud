package com.meida.bpm.provider.common.handler;

import com.meida.bpm.provider.model.ProcessInstanceRequest;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;

import java.util.List;
import java.util.Map;

/**
 * 流程启动业务扩展器
 *
 * @author zyf
 */
public interface ProcessStartHandler {

    /**
     * 条件验证
     *
     * @param processInstanceRequest 请求参数
     * @return
     */
    default String init(ProcessInstanceRequest processInstanceRequest) {
        return processInstanceRequest.getBusinessKey();
    }

    /**
     * 设置流程变量
     *
     * @param processInstanceRequest 请求参数
     * @param definition             流程定义
     * @return
     */
    default Map<String, Object> initVariables(ProcessInstanceRequest processInstanceRequest, ProcessDefinition definition) {
        return processInstanceRequest.getFormMap();
    }


    /**
     * 启动完成
     *
     * @param processInstanceRequest 请求参数
     * @param definition             流程定义
     * @param processInstance        流程实例
     * @param tasks                  任务列表
     * @param variables              流程变量
     * @return
     */
    default void afterComplete(ProcessInstanceRequest processInstanceRequest, ProcessDefinition definition, ProcessInstance processInstance, List<Task> tasks, Map<String, Object> variables) {

    }

}
