package com.meida.bpm.provider.handler;

import com.meida.bpm.provider.common.ResponseFactory;
import com.meida.bpm.provider.listener.FlowListInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zyf
 */
@Component
public class ProcInsListHandler implements FlowListInterceptor {
    @Autowired
    private ResponseFactory responseFactory;

    @Override
    public List execute(List list) {
        return responseFactory.createHistoricProcessInstanceResponseList(list);
    }
}
