package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowComment;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 流程评论表 接口
 *
 * @author flyme
 * @date 2024-12-16
 */
public interface FlowCommentService extends IBaseService<FlowComment> {

}
