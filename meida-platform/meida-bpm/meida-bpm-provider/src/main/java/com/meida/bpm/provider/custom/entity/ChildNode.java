/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import com.meida.bpm.provider.custom.model.ApproverModel;
import lombok.Data;


import java.util.List;

/**
 * 子节点
 * @author zyf
 */
@Data
public class ChildNode extends Node {

    /***
     * 审批人
     */
    private List<ApproverModel> approverGroups;
    /**
     * 审批人选择类型
     */
    private String assigneeType;

    /**
     * 审批人类型
     */
    private String approverType;

    /**
     * 表单配置
     */
    private List<FormOperate> formOperates;
    /**
     * 外层id
     */
    private String outerNodeId;
    /**
     * 其他属性
     */
    private ChildAttr attr;



}