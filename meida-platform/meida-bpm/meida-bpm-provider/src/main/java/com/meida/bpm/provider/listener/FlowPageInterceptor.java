package com.meida.bpm.provider.listener;

import com.meida.common.base.entity.EntityMap;
import org.flowable.common.engine.api.query.Query;

import java.util.List;

/**
 * 扩展分页处理器接口拦截器
 *
 * @author Administrator
 */
public interface FlowPageInterceptor<T> {


    /**
     * 条件预处理
     *
     * @return
     */
    default void prepare(Query query, EntityMap params) {

    }


    /**
     * 扩展返回结果
     *
     * @param query
     * @param data
     * @param params
     * @return
     */
    List<EntityMap> complete(Query query, List<T> data, EntityMap params);


}
