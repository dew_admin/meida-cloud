package com.meida.bpm.provider.listener;

import java.util.List;

public interface FlowListInterceptor {
    /**
     * 执行List转换封装
     *
     * @param list
     * @return
     */
    List execute(List list);
}
