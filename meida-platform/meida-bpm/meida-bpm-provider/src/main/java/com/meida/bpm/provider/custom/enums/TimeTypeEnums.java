package com.meida.bpm.provider.custom.enums;

/**
 * 定时方式枚举
 *
 * @author zyf
 */
public enum TimeTypeEnums {
    /**
     * 指定日期
     */
    timeDate,
    /**
     * 指定某个日期之后
     */
    timeDuration,
    /**
     * 循环执行
     */
    timeCycle
}
