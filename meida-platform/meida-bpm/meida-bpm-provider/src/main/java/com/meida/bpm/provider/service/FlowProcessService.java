package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.engine.impl.persistence.entity.DeploymentEntityImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * 流程表 接口
 *
 * @author flyme
 * @date 2020-06-11
 */
public interface FlowProcessService extends IBaseService<FlowProcess> {
    /**
     * 上传流程后添加自定义流程定义记录
     *
     * @param deploymentEntity
     * @param processXml
     */
    void addProcess(DeploymentEntityImpl deploymentEntity,String processXml);

    /**
     * 上传流程后添加自定义流程定义记录
     *
     * @param deploymentEntity
     * @param processXml
     */
    void importProcess(DeploymentEntityImpl deploymentEntity,String processXml);

    /**
     * 删除流程
     *
     * @param procDefId
     * @return
     */
    ResultBody deleteByProcDefId(String procDefId);

    /**
     * 部署流程
     *
     * @param procModelId
     * @param processId
     * @return
     */
    ResultBody deployModel(String procModelId, String processId);

    /**
     * 部署流程
     * @param processKey
     * @return
     */
    ResultBody deployModel(String processKey);

    /**
     * 根据key查询流程定义
     *
     * @param processKey
     * @return
     */
    FlowProcess getByProcessKey(String processKey);

    /**
     * 仿钉钉流程保存
     * @param flowProcess
     * @return
     */
    ResultBody saveProcess(FlowProcess flowProcess);

    /**
     * 更新流程状态
     * @param processId
     * @param processState
     * @return
     */
    Boolean setStatus(Long processId,Integer processState) ;

    /**
     * 克隆流程
     * @param processId
     * @return
     */
    Boolean copyProcess(Long processId) ;

    /**
     * 根据类型和企业ID查询
     * @param processTypeId
     * @param companyId
     * @return
     */
    FlowProcess findByTypeId(Long processTypeId,Long companyId);

    /**
     * 根据流程类型code查询
     * @param processTypeCode
     * @param companyId
     * @return
     */
    FlowProcess findByTypeCode(String processTypeCode, Long companyId);

    /**
     * 根据流程类型code和状态值查询
     * @param processTypeCode
     * @param processState
     * @param companyId
     * @return
     */
    FlowProcess findByTypeCodeAndState(String processTypeCode,Integer processState, Long companyId);

}
