package com.meida.bpm.provider.common.handler;

import com.meida.common.mybatis.model.ResultBody;
import org.flowable.engine.history.HistoricProcessInstance;

/**
 * 删除流程实例
 */
public interface ProcInstDeleteHandler {


    /**
     * 判断是否使用
     *
     * @param processDefinitionKey 参数
     * @return tag
     */

    boolean support(String processDefinitionKey);

    /**
     * 删除前置处理
     */

    default ResultBody prepare(HistoricProcessInstance historicProcessInstance) {
        return ResultBody.ok();
    }

    /**
     * 删除后置处理
     */

    ResultBody success(String businessKey, String processInstanceId);

}
