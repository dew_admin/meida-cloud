/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import lombok.Data;

/**
 * 条件节点
 *
 * @author zyf
 */
@Data
public class ConditionNode extends Node {


    private String initiator;
    private int priority;
    private boolean isDefault;

    /**
     * 其他属性
     */
    private ChildAttr attr;
}