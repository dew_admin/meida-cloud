package com.meida.bpm.provider.custom.constant;

/**
 * 简版流程常量定义
 *
 * @author zyf
 */
public class FlowProcessConstant {

    /**
     * 流程命名空间
     */
    public static final String TargetNamespace = "http://www.jeecg.com";

    /**
     * 监听类型
     */
    public static final String javaClass = "javaClass";


    /**
     * 排序 降序
     */
    public static final String DESC = "desc";

    /**
     * 排序 升序
     */
    public static final String ASC = "asc";

    /**
     * 模板变量-流程标题
     */
    public static final String FW_TITLE = "_fw_title";
    /**
     * 模板变量-流程名称
     */
    public static final String FW_NAME = "_fw_name";
    /**
     * 模板变量-节点名称
     */
    public static final String FW_NODE_NAME = "_fw_node_name";

    /**
     * 模板变量-地址 //TODO url规则
     */
    public static final String FW_URL = "_fw_url";

    /**
     * 添加数据节点数据ID流程变量key
     */
    public static final String FORM_DATA_KEY="flow:task:form_data:{0}:{1}";

    /**
     * 获取数据节点数据ID流程变量key
     */
    public static final String GET_DATA_KEY="flow:task:get_data:{0}:{1}";

    /**
     * 数据分支判断节点流程变量
     */
    public static final String FLOW_HAS_DATA="flow_has_data";

    /**
     * 获取多条数据节点数据redis key前缀
     */
    public static final String GET_DATA_REDIS_KEY_PREFIX="flow:task:redis_get_data";
    /**
     * 获取多条数据节点数据redis key
     */
    public static final String GET_DATA_REDIS_KEY=GET_DATA_REDIS_KEY_PREFIX+":{0}:{1}";

    /**
     * 信号名称常量
     */
    public static final String BPM_SIGNAL_NAME="BPM_SIGNAL_NAME";

    /**
     * 数字类型
     */
    public static final String[] NUMBER_TYPE=new String[]{"number","integer","money"};

    /**
     * 数据分支节点名称
     */
    public static final String DATA_BRANCH_NAME="数据分支";

    /**
     * 按钮信号前缀
     */
    public static final String BUTTON_SIGNAL_PRE="button_signal:";

    /**
     * 人员入职信号
     */
    public static final String USER_SIGNAL_ADD="user_signal:add";

    /**
     * 人员离职信号
     */
    public static final String USER_SIGNAL_LEAVE="user_signal:leave";

}
