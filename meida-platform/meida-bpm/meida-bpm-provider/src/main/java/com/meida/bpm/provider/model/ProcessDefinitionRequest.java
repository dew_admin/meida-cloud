package com.meida.bpm.provider.model;

import lombok.Data;

import java.util.Date;

/**
 * @author zyf
 */
@Data
public class ProcessDefinitionRequest {
    private String processDefinitionId;
    private boolean includeProcessInstances = false;
    private Date date;
}
