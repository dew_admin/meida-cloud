/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import lombok.Data;

/**
 * 基础配置
 *
 * @author zyf
 */
@Data
public class BasicSetting {

    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程图标
     */
    private String flowIcon;
    /**
     * 图标颜色
     */
    private String flowIconBackground;
    /**
     * 流程分组
     */
    private int flowGroup;
    /**
     * 备注
     */
    private String flowRemark;
    /**
     * 流程编码
     */
    private String flowCode;
    /**
     * 流程ID
     */
    private String id;

}