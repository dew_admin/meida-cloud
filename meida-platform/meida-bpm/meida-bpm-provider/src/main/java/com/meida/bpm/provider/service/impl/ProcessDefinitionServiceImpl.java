package com.meida.bpm.provider.service.impl;


import cn.hutool.core.text.StrFormatter;
import com.google.common.io.CharStreams;
import com.meida.bpm.client.entity.FlowForm;
import com.meida.bpm.provider.common.GetProcessDefinitionInfoCmd;
import com.meida.bpm.provider.constant.FlowableConstant;
import com.meida.bpm.provider.model.IdentityRequest;
import com.meida.bpm.provider.model.ProcessDefinitionRequest;
import com.meida.bpm.provider.service.FlowFormService;
import com.meida.bpm.provider.service.FlowProcessService;
import com.meida.bpm.provider.service.ProcessDefinitionService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.StartEvent;
import org.flowable.bpmn.model.UserTask;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.common.engine.impl.util.io.InputStreamSource;
import org.flowable.common.engine.impl.util.io.StreamSource;
import org.flowable.engine.ManagementService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.impl.persistence.entity.DeploymentEntityImpl;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.job.api.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

/**
 * @author zyf
 */
@Service
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {
    @Autowired
    protected RepositoryService repositoryService;
    @Autowired
    protected ManagementService managementService;
    @Autowired
    protected RuntimeService runtimeService;
    @Autowired
    private FlowFormService flowableFormService;

    @Autowired
    private FlowProcessService flowProcessService;

    @Override
    public ProcessDefinition getProcessDefinitionById(String processDefinitionId) {
        return managementService.executeCommand(new GetProcessDefinitionInfoCmd(processDefinitionId, null, null));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Map params) {
        EntityMap entityMap = new EntityMap(params);
        Boolean cascade = entityMap.getBoolean("cascade");
        String processDefinitionId = entityMap.get("ids");
        ProcessDefinition processDefinition = getProcessDefinitionById(processDefinitionId);
        if (processDefinition.getDeploymentId() == null) {
            ApiAssert.failure(StrFormatter.format("未部署id为{0}的进程定义", processDefinitionId));
        }
        if (cascade) {
            List<Job> jobs = managementService.createTimerJobQuery().processDefinitionId(processDefinitionId).list();
            for (Job job : jobs) {
                managementService.deleteTimerJob(job.getId());
            }
            repositoryService.deleteDeployment(processDefinition.getDeploymentId(), true);
        } else {
            long processCount = runtimeService.createProcessInstanceQuery().processDefinitionId(processDefinitionId)
                    .count();
            if (processCount > 0) {
                ApiAssert.failure(StrFormatter.format("存在进程定义id为{0}的运行实例", processDefinitionId));
            }
            long jobCount = managementService.createTimerJobQuery().processDefinitionId(processDefinitionId).count();
            if (jobCount > 0) {
                ApiAssert.failure(StrFormatter.format("存在进程定义id{0}为的运行任务", processDefinitionId));
            }
            repositoryService.deleteDeployment(processDefinition.getDeploymentId());
        }
        //删除自定义流程
        flowProcessService.deleteByProcDefId(processDefinitionId);
    }

    @Override
    public void delete(String processDefId) {
        ProcessDefinition processDefinition = getProcessDefinitionById(processDefId);
        if (processDefinition.getDeploymentId() == null) {
            ApiAssert.failure(StrFormatter.format("未部署id为{0}的进程定义", processDefId));
        }
        long processCount = runtimeService.createProcessInstanceQuery().processDefinitionId(processDefId).count();
        if (processCount > 0) {
            ApiAssert.failure(StrFormatter.format("存在进程定义id为{0}的运行实例", processDefId));
        }
        long jobCount = managementService.createTimerJobQuery().processDefinitionId(processDefId).count();
        if (jobCount > 0) {
            ApiAssert.failure(StrFormatter.format("存在进程定义id{0}为的运行任务", processDefId));
        }
        repositoryService.deleteDeployment(processDefinition.getDeploymentId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void activate(ProcessDefinitionRequest actionRequest) {
        String processDefinitionId = actionRequest.getProcessDefinitionId();
        ProcessDefinition processDefinition = getProcessDefinitionById(processDefinitionId);
        if (!processDefinition.isSuspended()) {
            ApiAssert.failure(StrFormatter.format("进程定义{0},已激活", processDefinitionId));
        }
        repositoryService.activateProcessDefinitionById(processDefinitionId, actionRequest.isIncludeProcessInstances(), actionRequest.getDate());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void suspend(ProcessDefinitionRequest actionRequest) {
        String processDefinitionId = actionRequest.getProcessDefinitionId();
        ProcessDefinition processDefinition = getProcessDefinitionById(processDefinitionId);
        if (processDefinition.isSuspended()) {
            ApiAssert.failure(StrFormatter.format("流程定义{0}已挂起", processDefinitionId));
        }
        repositoryService.suspendProcessDefinitionById(processDefinition.getId(), actionRequest.isIncludeProcessInstances(), actionRequest.getDate());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void doImport(String tenantId, HttpServletRequest request) {
        if (!(request instanceof MultipartHttpServletRequest)) {
            throw new IllegalArgumentException("request must instance of MultipartHttpServletRequest");
        }
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        if (multipartRequest.getFileMap().size() == 0) {
            throw new IllegalArgumentException("request file is empty");
        }
        MultipartFile file = multipartRequest.getFileMap().values().iterator().next();
        String fileName = file.getOriginalFilename();
        boolean isFileNameInValid = FlymeUtils.isEmpty(fileName) || !(fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn") || fileName.toLowerCase().endsWith(".bar") || fileName.toLowerCase().endsWith(".zip"));
        if (isFileNameInValid) {
            throw new IllegalArgumentException("Request file must end with .bpmn20.xml,.bpmn|,.bar,.zip");
        }
        try {
            DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
            deploymentBuilder.category("test");
            boolean isBpmnFile = fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn");
            if (isBpmnFile) {
                deploymentBuilder.addInputStream(fileName, file.getInputStream());
                StreamSource xmlSource = new InputStreamSource(file.getInputStream());
                BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xmlSource, false, false, "UTF-8");
                org.flowable.bpmn.model.Process process = bpmnModel.getMainProcess();
                Collection<FlowElement> flowElements = process.getFlowElements();
                Map<String, String> formKeyMap = new HashMap<String, String>(16);
                for (FlowElement flowElement : flowElements) {
                    String formKey = null;
                    if (flowElement instanceof StartEvent) {
                        StartEvent startEvent = (StartEvent) flowElement;
                        if (startEvent.getFormKey() != null && startEvent.getFormKey().length() > 0) {
                            formKey = startEvent.getFormKey();
                        }
                    } else if (flowElement instanceof UserTask) {
                        UserTask userTask = (UserTask) flowElement;
                        if (userTask.getFormKey() != null && userTask.getFormKey().length() > 0) {
                            formKey = userTask.getFormKey();
                        }
                    }
                    if (formKey != null && formKey.length() > 0) {
                        if (formKeyMap.containsKey(formKey)) {
                            continue;
                        } else {
                            String formKeyDefinition = formKey.replace(".form", "");
                            FlowForm form = flowableFormService.getByFormKey(formKeyDefinition);
                            if (form != null && form.getFormJson() != null && form.getFormJson().length() > 0) {
                                byte[] formJson = form.getFormJson().getBytes("UTF-8");
                                ByteArrayInputStream bi = new ByteArrayInputStream(formJson);
                                deploymentBuilder.addInputStream(formKey, bi);
                                formKeyMap.put(formKey, formKey);
                            } else {
                                ApiAssert.failure("formJson未定义");
                            }
                        }
                    }
                }
            } else if (fileName.toLowerCase().endsWith(FlowableConstant.FILE_EXTENSION_BAR)
                    || fileName.toLowerCase().endsWith(FlowableConstant.FILE_EXTENSION_ZIP)) {
                deploymentBuilder.addZipInputStream(new ZipInputStream(file.getInputStream()));
            }
            deploymentBuilder.name(fileName);
            if (tenantId != null && tenantId.length() > 0) {
                deploymentBuilder.tenantId(tenantId);
            }
            DeploymentEntityImpl deployment = (DeploymentEntityImpl) deploymentBuilder.deploy();
            if (FlymeUtils.isNotEmpty(deployment)) {
                //添加自定义流程定义
                String processXml = CharStreams.toString(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8));
                flowProcessService.addProcess(deployment, processXml);
            }
        } catch (FlowableObjectNotFoundException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            ApiAssert.failure("流程导入失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importProcess(String tenantId, HttpServletRequest request) {
        if (!(request instanceof MultipartHttpServletRequest)) {
            throw new IllegalArgumentException("request must instance of MultipartHttpServletRequest");
        }
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        if (multipartRequest.getFileMap().size() == 0) {
            throw new IllegalArgumentException("request file is empty");
        }
        MultipartFile file = multipartRequest.getFileMap().values().iterator().next();
        String fileName = file.getOriginalFilename();
        boolean isFileNameInValid = FlymeUtils.isEmpty(fileName) || !(fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn") || fileName.toLowerCase().endsWith(".bar") || fileName.toLowerCase().endsWith(".zip"));
        if (isFileNameInValid) {
            throw new IllegalArgumentException("Request file must end with .bpmn20.xml,.bpmn|,.bar,.zip");
        }
        try {
            DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
            boolean isBpmnFile = fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn");
            if (isBpmnFile) {
                deploymentBuilder.addInputStream(fileName, file.getInputStream());
                StreamSource xmlSource = new InputStreamSource(file.getInputStream());
                BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xmlSource, false, false, "UTF-8");
                Process process = bpmnModel.getMainProcess();
                if (FlymeUtils.isNotEmpty(process)) {
                    deploymentBuilder.key(process.getId());
                    DeploymentEntityImpl deployment = (DeploymentEntityImpl) deploymentBuilder.deploy();
                    //添加自定义流程定义
                    String processXml = CharStreams.toString(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8));
                    flowProcessService.importProcess(deployment, processXml);
                }
            }

        } catch (FlowableObjectNotFoundException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            ApiAssert.failure("流程导入失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveProcessDefinitionIdentityLink(IdentityRequest identityRequest) {
        ProcessDefinition processDefinition = getProcessDefinitionById(identityRequest.getProcessDefinitionId());
        validateIdentityArguments(identityRequest.getIdentityId(), identityRequest.getIdentityType());
        if (FlowableConstant.IDENTITY_GROUP.equals(identityRequest.getIdentityType())) {
            repositoryService.addCandidateStarterGroup(processDefinition.getId(), identityRequest.getIdentityId());
        } else if (FlowableConstant.IDENTITY_USER.equals(identityRequest.getIdentityType())) {
            repositoryService.addCandidateStarterUser(processDefinition.getId(), identityRequest.getIdentityId());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteProcessDefinitionIdentityLink(String processDefinitionId, String identityId, String type) {
        validateIdentityArguments(identityId, type);
        //ProcessDefinition processDefinition = getProcessDefinitionById(processDefinitionId);
        if (FlowableConstant.IDENTITY_GROUP.equals(type)) {
            repositoryService.deleteCandidateStarterGroup(processDefinitionId, identityId);
        } else if (FlowableConstant.IDENTITY_USER.equals(type)) {
            repositoryService.deleteCandidateStarterUser(processDefinitionId, identityId);
        }
    }

    private void validateIdentityArguments(String identityId, String type) {
        if (identityId == null || identityId.length() == 0) {
            throw new FlowableException("IdentityId may not be null");
        }
        if (!FlowableConstant.IDENTITY_GROUP.equals(type) && !FlowableConstant.IDENTITY_USER.equals(type)) {
            throw new FlowableException(
                    "Type must be " + FlowableConstant.IDENTITY_GROUP + " or " + FlowableConstant.IDENTITY_USER);
        }
    }
}
