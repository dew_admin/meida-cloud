package com.meida.bpm.provider.custom.model;

import lombok.Data;

@Data
public class CustomProcessForm {

    private String id;
    private String relationCode;
    private String bizName;
    private String processId;
    private String formTableName;
    private String formType;
    private String titleExp;
    private String formDealStyle;
    private String flowStatusCol;
    private String triggerAction;
    private String lowAppId;
    private Integer tenantId;
}
