package com.meida.bpm.provider.base;

import com.meida.bpm.provider.common.handler.ProcInstDeleteHandler;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.engine.history.HistoricProcessInstance;

/**
 * 删除流程实例回调
 */
public class AbstractProcInstDeleteService implements ProcInstDeleteHandler {
    @Override
    public boolean support(String processDefinitionKey) {
        return false;
    }

    @Override
    public ResultBody prepare(HistoricProcessInstance historicProcessInstance) {
        return ResultBody.ok();
    }

    @Override
    public ResultBody success(String businessKey, String processInstanceId) {
        return ResultBody.ok();
    }
}
