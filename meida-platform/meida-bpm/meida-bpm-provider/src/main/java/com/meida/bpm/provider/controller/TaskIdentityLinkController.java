package com.meida.bpm.provider.controller;


import com.meida.bpm.provider.common.BaseFlowableController;
import com.meida.bpm.provider.model.IdentityRequest;
import com.meida.bpm.provider.service.FlowableTaskService;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.identitylink.api.history.HistoricIdentityLink;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 任务授权管理
 * @author zyf
 */
@RestController
@RequestMapping("/flowable/taskIdentityLink")
public class TaskIdentityLinkController extends BaseFlowableController {
    @Autowired
    protected FlowableTaskService flowableTaskService;


    @GetMapping(value = "/list")
    public ResultBody list(@RequestParam String taskId) {
        HistoricTaskInstance task = flowableTaskService.getHistoricTaskInstanceNotNull(taskId);
        List<HistoricIdentityLink> historicIdentityLinks = historyService.getHistoricIdentityLinksForTask(task.getId());
        return ResultBody.ok(responseFactory.createTaskIdentityResponseList(historicIdentityLinks));
    }

    /**
     * 新增任务授权
     * @param taskIdentityRequest
     * @return
     */
    @PostMapping(value = "/save")
    public ResultBody save(@RequestBody IdentityRequest taskIdentityRequest) {
        flowableTaskService.saveTaskIdentityLink(taskIdentityRequest);
        return ResultBody.ok();
    }


    /**
     * 删除任务授权
     * @param taskId
     * @param identityId
     * @param identityType
     * @return
     */
    @DeleteMapping(value = "/delete")
    public ResultBody deleteIdentityLink(@RequestParam String taskId, @RequestParam String identityId,
                                     @RequestParam String identityType) {
        flowableTaskService.deleteTaskIdentityLink(taskId, identityId, identityType);
        return ResultBody.ok();
    }
}
