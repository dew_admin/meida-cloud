package com.meida.bpm.provider.identity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseUserService;
import org.flowable.idm.api.User;
import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.UserQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityManagerImpl;
import org.flowable.idm.engine.impl.persistence.entity.data.UserDataManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 扩展自定义用户管理，使用系统用户
 *
 * @author zyf
 */
public class FlowUserEntityManager extends UserEntityManagerImpl {
    @Autowired
    private BaseUserService userService;

    public FlowUserEntityManager(IdmEngineConfiguration idmEngineConfiguration, UserDataManager userDataManager) {
        super(idmEngineConfiguration, userDataManager);
    }

    @Override
    public UserEntity findById(String entityId) {
        BaseUser baseUser = userService.getById(entityId);
        if (baseUser == null) {
            return null;
        }
        UserEntity userEntity = new UserEntityImpl();
        userEntity.setId(baseUser.getUserId().toString());
        userEntity.setFirstName(baseUser.getUserName());
        userEntity.setLastName(baseUser.getUserName());
        userEntity.setDisplayName(baseUser.getNickName());
        userEntity.setEmail(baseUser.getEmail());
        return userEntity;
    }

    @Override
    public List<User> findUserByQueryCriteria(UserQueryImpl query) {
        List<BaseUser> baseUserList = userService.list(getQueryToWrapper(query));
        if (FlymeUtils.isEmpty(baseUserList)) {
            return new ArrayList<>();
        }
        List<User> users = new ArrayList<>();
        for (BaseUser baseUser : baseUserList) {
            User user = new UserEntityImpl();
            user.setId(baseUser.getUserId().toString());
            user.setFirstName(baseUser.getUserName());
            user.setLastName(baseUser.getUserName());
            user.setDisplayName(baseUser.getNickName());
            user.setEmail(baseUser.getEmail());
            users.add(user);
        }
        return users;
    }

    @Override
    public long findUserCountByQueryCriteria(UserQueryImpl query) {
        return userService.count(getQueryToWrapper(query));
    }

    /**
     * 构建查询条件
     *
     * @param query
     * @return
     */
    private QueryWrapper<BaseUser> getQueryToWrapper(UserQueryImpl query) {
        QueryWrapper<BaseUser> queryWrapper = new QueryWrapper<>();
        if (query.getId() != null) {
            queryWrapper.eq("userId", query.getId());
        }
        if (query.getIds() != null) {
            queryWrapper.in("userId", query.getIds());
        }
        if (query.getFirstName() != null) {
            queryWrapper.eq("userName", query.getFirstName());
        }
        if (query.getFirstNameLike() != null) {
            queryWrapper.like("userName", query.getFirstName());
        }
        return queryWrapper;
    }

}
