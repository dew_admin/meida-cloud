package com.meida.bpm.provider.mapper;

import com.meida.bpm.client.entity.FlowComment;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 流程评论表 Mapper 接口
 * @author flyme
 * @date 2024-12-16
 */
public interface FlowCommentMapper extends SuperMapper<FlowComment> {

}
