package com.meida.bpm.provider.controller;


import com.meida.bpm.client.entity.FlowForm;
import com.meida.bpm.provider.service.FlowFormService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 表单管理
 *
 * @author zyf
 */
@RestController
@RequestMapping("/flowable/form")
public class FlowFormController extends BaseController<FlowFormService, FlowForm> {

    @ApiOperation(value = "流程表-分页列表", notes = "流程表分页列表")
    @GetMapping(value = "/page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }
    /**
     * form列表
     * @return
     */
    @GetMapping(value = "/list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);

    }

    @GetMapping(value = "/get")
    public ResultBody queryById(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    /**
     * 添加表单
     */
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    /**
     * 修改表单
     */
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    /**
     * 删除表单
     */
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }
    @GetMapping(value = "/getFormTableList")
    public ResultBody getFormTableList(@RequestParam(required = false) Map params) {
        EntityMap form=new EntityMap();
        form.put("label","归档表");
        form.put("value","archive_info");
        form.put("id","1");

        EntityMap form3=new EntityMap();
        form3.put("label","借阅表");
        form3.put("value","arc_use_record");
        form3.put("id","3");

        EntityMap form2=new EntityMap();
        form2.put("label","销毁表");
        form2.put("value","arc_destory");
        form2.put("id","2");

        List<EntityMap> list=new ArrayList<>();
        list.add(form);
        list.add(form2);
        list.add(form3);
        return ResultBody.ok(list);
    }

    @GetMapping(value = "/getFormField")
    public ResultBody getFormField(@RequestParam(required = false) Map params) {
        EntityMap result = new EntityMap();
        EntityMap field=new EntityMap();
        field.put("label","档案门类");
        field.put("field","cnName");
        field.put("type","select-category");
        field.put("options",new EntityMap());
        List<EntityMap> fields=new ArrayList<>();
        fields.add(field);
        result.put("fields",fields);
        result.put("titleField","");
        return ResultBody.ok(result);
    }
}
