package com.meida.bpm.provider.model;

import lombok.Data;

/**
 * @author zyf
 */
@Data
public class CcToVo {
    private String userId;
    private String userName;

    @Override
    public String toString(){
        return String.format("%s[%s]",this.userName,this.userId);
    }
}
