package com.meida.bpm.provider.model;

import lombok.Data;

/**
 * 流程设计器请求参数模型
 */
@Data
public class FlowProcessRequest {
    /**
     * 流程ID
     */
    private Long processId;

    /**
     * 企业ID
     */
    private Long companyId;

    /**
     * 用户请求token
     */
    private String accessToken;
}
