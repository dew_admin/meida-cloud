package com.meida.bpm.provider.model;

import lombok.Data;

import java.util.List;

@Data
public class AppRoverUserVo {
    private  List<String> userIds;
    private  Boolean isSequential;
    private String approverType;
}
