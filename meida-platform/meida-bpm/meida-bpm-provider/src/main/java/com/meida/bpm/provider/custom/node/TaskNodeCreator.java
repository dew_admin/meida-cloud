/**
 * 服务任务构建
 *
 * @author zyf
 */
package com.meida.bpm.provider.custom.node;


import com.alibaba.fastjson.JSON;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.provider.custom.entity.ChildAttr;
import com.meida.bpm.provider.custom.entity.ChildNode;
import com.meida.bpm.provider.custom.model.CustomProcessNode;
import com.meida.bpm.provider.custom.model.ListenerModel;
import com.meida.bpm.provider.custom.util.BpmnCreator;
import org.apache.commons.lang3.ObjectUtils;
import org.flowable.bpmn.model.*;


import java.util.ArrayList;
import java.util.List;

/**
 * 任务节点创建
 *
 * @author zyf
 */
public class TaskNodeCreator {
    /**
     * 创建服务任务
     *
     * @param serviceNodes
     * @return
     */
    public static List<ServiceTask> createServiceTask(List<ChildNode> serviceNodes, List<FlowProcessNode> extActProcessNodes) {
        List<ServiceTask> tasks = new ArrayList<>();
        for (ChildNode node : serviceNodes) {
            FlowProcessNode extActProcessNode = new FlowProcessNode();
            ChildAttr childAttr = node.getAttr();
            String id = node.getId();
            String name = node.getName();
            ServiceTask serviceTask = new ServiceTask();
            serviceTask.setId(id);
            serviceTask.setName(name);
            extActProcessNode.setProcessNodeCode(id);
            extActProcessNode.setProcessNodeName(name);
            if (ObjectUtils.isNotEmpty(childAttr)) {
                serviceTask.setResultVariableName(childAttr.getResultVariable());
                serviceTask.setImplementationType(childAttr.getExpressionType());
                serviceTask.setImplementation(childAttr.getExpressionValue());
                serviceTask.setDocumentation(childAttr.getDescription());
                extActProcessNode.setNodeConfigJson(JSON.toJSONString(childAttr));
            }
            tasks.add(serviceTask);
            extActProcessNodes.add(extActProcessNode);
        }
        return tasks;
    }

    /**
     * 创建脚本任务
     *
     * @param serviceNodes
     * @return
     */
    public static List<ScriptTask> createScripTask(List<ChildNode> serviceNodes, List<FlowProcessNode> customProcessNodes) {
        List<ScriptTask> tasks = new ArrayList<>();
        for (ChildNode node : serviceNodes) {
            FlowProcessNode customProcessNode = new FlowProcessNode();
            String id = node.getId();
            String name = node.getName();
            ChildAttr childAttr = node.getAttr();
            ScriptTask scriptTask = new ScriptTask();
            scriptTask.setId(id);
            scriptTask.setName(name);
            customProcessNode.setProcessNodeCode(id);
            customProcessNode.setProcessNodeName(name);
            if (ObjectUtils.isNotEmpty(childAttr)) {
                scriptTask.setScript(childAttr.getScriptContent());
                scriptTask.setScriptFormat(childAttr.getScriptFormat());
                scriptTask.setDocumentation(childAttr.getDescription());
                customProcessNode.setNodeConfigJson(JSON.toJSONString(childAttr));
            }
            tasks.add(scriptTask);
            customProcessNodes.add(customProcessNode);
        }
        return tasks;
    }

    /**
     * 创建消息服务节点任务
     *
     * @param messageNodes
     * @return
     */
    public static List<ServiceTask> createMessageTask(List<ChildNode> messageNodes,List<FlowProcessNode> extActProcessNodes) {
        List<ServiceTask> tasks = new ArrayList<>();
        for (ChildNode node : messageNodes) {
            ServiceTask serviceTask = new ServiceTask();
            FlowProcessNode extActProcessNode = new FlowProcessNode();
            String id = node.getId();
            String name = node.getName();
            ChildAttr childAttr = node.getAttr();
            serviceTask.setId(id);
            serviceTask.setName(name);
            extActProcessNode.setProcessNodeCode(id);
            extActProcessNode.setProcessNodeName(name);
            if (ObjectUtils.isNotEmpty(childAttr)) {
                serviceTask.setResultVariableName(childAttr.getResultVariable());
                serviceTask.setImplementationType(childAttr.getExpressionType());
                serviceTask.setImplementation(childAttr.getExpressionValue());
                serviceTask.setDocumentation(childAttr.getDescription());
                extActProcessNode.setNodeConfigJson(JSON.toJSONString(childAttr));
            }
            tasks.add(serviceTask);
            extActProcessNodes.add(extActProcessNode);
        }
        return tasks;
    }

    /**
     * 创建调动活动子流程
     *
     * @param serviceNodes
     * @return
     */
    public static List<CallActivity> createCallActivity(List<ChildNode> serviceNodes) {
        List<CallActivity> callActivities = new ArrayList<>();
        for (ChildNode node : serviceNodes) {
            ChildAttr childAttr = node.getAttr();
            String approvalMode = childAttr.getApprovalMode();
            //是否是多实例
            Boolean isMuti=childAttr.getIsMulti();
            List<IOParameter> inVariableModels = node.getInVariableModels();
            List<IOParameter> outVariableModels = node.getOutVariableModels();
            CallActivity callActivity = new CallActivity();
            callActivity.setId(node.getId());
            callActivity.setName(node.getName());
            if (ObjectUtils.isNotEmpty(childAttr)) {
                callActivity.setCalledElement(childAttr.getCalledElement());
                if (!UserTaskNodeCreator.COUNTER_SIGN.equals(approvalMode)) {
                    MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = new MultiInstanceLoopCharacteristics();
                    // 循环数量
                    multiInstanceLoopCharacteristics.setLoopCardinality(childAttr.getLoopCardinality());
                    // 迭代变量
                    multiInstanceLoopCharacteristics.setElementVariable(childAttr.getElementVariable());
                    // 完成条件 已完成数等于实例数
                    multiInstanceLoopCharacteristics.setCompletionCondition(childAttr.getCompletionCondition());
                    // 会签类型
                    multiInstanceLoopCharacteristics.setSequential(childAttr.getIsSequential());
                    // 循环集合
                    multiInstanceLoopCharacteristics.setInputDataItem(childAttr.getCollection());
                    // 设置多实例属性
                    callActivity.setLoopCharacteristics(multiInstanceLoopCharacteristics);
                }
                if(ObjectUtils.isNotEmpty(isMuti) && isMuti){
                    MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = new MultiInstanceLoopCharacteristics();
                    multiInstanceLoopCharacteristics.setInputDataItem("${minFlowUtils.stringToList(assigneeDataIdList)}");
                    multiInstanceLoopCharacteristics.setElementVariable("dataId");
                    // 设置多实例属性
                    callActivity.setLoopCharacteristics(multiInstanceLoopCharacteristics);
                }
            }
            if (ObjectUtils.isNotEmpty(inVariableModels)) {
                callActivity.setInParameters(inVariableModels);
            }
            if (ObjectUtils.isNotEmpty(outVariableModels)) {
                callActivity.setOutParameters(outVariableModels);
            }
            callActivities.add(callActivity);
            setListener(callActivity, node);
        }
        return callActivities;
    }

    /**
     * 设置监听
     *
     * @param callActivity
     * @param node
     */
    private static void setListener(CallActivity callActivity, ChildNode node) {
        //执行监听
        List<ListenerModel> listenerModel = node.getListenerData();
        if (ObjectUtils.isNotEmpty(listenerModel)) {
            List<FlowableListener> executionListeners = new ArrayList<>();
            for (ListenerModel listener : listenerModel) {
                FlowableListener activitiListener = BpmnCreator.createActivitiListener(listener);
                executionListeners.add(activitiListener);
            }
            callActivity.setExecutionListeners(executionListeners);
        }
    }
}
