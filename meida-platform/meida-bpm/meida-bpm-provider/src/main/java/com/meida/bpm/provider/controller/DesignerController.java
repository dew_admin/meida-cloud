package com.meida.bpm.provider.controller;

import com.meida.bpm.provider.model.FlowProcessRequest;
import com.meida.common.base.utils.FlymeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 流程设计器
 *
 * @author zyf
 */
@Controller
@RequestMapping("/bpm/designer/")
public class DesignerController {


    /**
     * 设计器首页
     *
     * @return
     */
    @GetMapping("/index")
    public String index(HttpServletRequest request, FlowProcessRequest flowProcessRequest, Model model) {
        String path = request.getContextPath() + "/";
        //上下文路径
        model.addAttribute("contextPath", path);
        //获取请求父路径
        String baseURL = this.getBaseURL(request);
        model.addAttribute("baseURL", baseURL);
        model.addAttribute("flowProcess", flowProcessRequest);
        return "designer/index";
    }

    /**
     * 获取服务器接口路径
     *
     * @param request
     * @return
     */
    private String getBaseURL(HttpServletRequest request) {
        String scheme = request.getHeader("X-Forwarded-Scheme");
        if (FlymeUtils.isEmpty(scheme)) {
            scheme = request.getScheme();
        }
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        String contextPath = request.getContextPath()+"/";
        if(contextPath.indexOf("base")==-1){
            contextPath="/base/";
        }
        //返回 host domain
        String baseDomainPath = null;
        if (80 == serverPort) {
            baseDomainPath = scheme + "://" + serverName + contextPath;
        } else {
            baseDomainPath = scheme + "://" + serverName + ":" + serverPort + contextPath;
        }
        return baseDomainPath;
    }

}
