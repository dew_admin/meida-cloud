package com.meida.bpm.provider.controller;

import com.meida.bpm.client.entity.FlowCategory;
import com.meida.bpm.provider.service.FlowCategoryService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 流程分类表控制器
 *
 * @author flyme
 * @date 2020-06-14
 */
@RestController
@RequestMapping("/bpm/category/")
public class FlowCategoryController extends BaseController<FlowCategoryService, FlowCategory>  {

    @ApiOperation(value = "流程分类表-分页列表", notes = "流程分类表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "流程分类表-列表", notes = "流程分类表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "流程分类表-列表", notes = "流程分类表列表")
    @GetMapping(value = "selectTypeList")
    public ResultBody selectTypeList() {
        List<EntityMap> list= bizService.selectTypeList();
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "流程分类表-添加", notes = "添加流程分类表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "流程分类表-更新", notes = "更新流程分类表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "流程分类表-删除", notes = "删除流程分类表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "流程分类表-详情", notes = "流程分类表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


}
