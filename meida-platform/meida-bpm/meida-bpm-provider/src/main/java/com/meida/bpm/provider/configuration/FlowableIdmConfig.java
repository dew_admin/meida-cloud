package com.meida.bpm.provider.configuration;

import com.meida.bpm.provider.identity.FlowGroupEntityManager;
import com.meida.bpm.provider.identity.FlowUserEntityManager;
import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.spring.SpringIdmEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * @author zyf
 * &nullCatalogMeansCurrent=true
 */
@Configuration
public class FlowableIdmConfig implements EngineConfigurationConfigurer<SpringIdmEngineConfiguration> {

    @Override
    public void configure(SpringIdmEngineConfiguration configuration) {
        configuration.setGroupEntityManager(customGroupEntityManager(configuration));
        configuration.setUserEntityManager(customUserEntityManager(configuration));
    }

    @Bean
    public FlowGroupEntityManager customGroupEntityManager(IdmEngineConfiguration configuration) {
        return new FlowGroupEntityManager(configuration, configuration.getGroupDataManager());
    }

    @Bean
    public FlowUserEntityManager customUserEntityManager(IdmEngineConfiguration configuration) {
        return new FlowUserEntityManager(configuration, configuration.getUserDataManager());
    }
}
