package com.meida.bpm.provider.controller;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.provider.service.FlowProcessService;
import com.meida.common.annotation.LoginRequired;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 流程表控制器
 *
 * @author flyme
 * @date 2020-06-11
 */
@RestController
@RequestMapping("/bpm/process/")
public class FlowProcessController extends BaseController<FlowProcessService, FlowProcess> {


    @ApiOperation(value = "流程表-分页列表", notes = "流程表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "流程表-列表", notes = "流程表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "流程表-添加", notes = "添加流程表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "流程表-更新", notes = "更新流程表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "流程表-删除", notes = "删除流程表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "流程表-详情", notes = "流程表详情")
    @GetMapping(value = "get")
    @LoginRequired(required = false)
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }



    /**
     * 更新流程状态
     */
    @ApiOperation(value = "更新流程状态", notes = "更新流程状态")
    @PostMapping(value = "setStatus")
    public ResultBody setStatus(@RequestParam(value = "processId") Long processId,@RequestParam(value = "processState") Integer processState) {
        Boolean tag= bizService.setStatus(processId,processState);
        return ResultBody.ok(tag);
    }

    /**
     * 流程部署
     *
     * @param procModelId
     * @param processId
     * @return
     */
    @PostMapping(value = "/deploy")
    @Transactional(rollbackFor = Exception.class)
    public ResultBody deployModel(String procModelId,String processId) {
        return bizService.deployModel(procModelId,processId);
    }

    /**
     * 流程部署
     *
     * @param processKey
     * @return
     */
    @PostMapping(value = "/deployByKey")
    @Transactional(rollbackFor = Exception.class)
    public ResultBody deployByKey(String processKey) {
        return bizService.deployModel(processKey);
    }


}
