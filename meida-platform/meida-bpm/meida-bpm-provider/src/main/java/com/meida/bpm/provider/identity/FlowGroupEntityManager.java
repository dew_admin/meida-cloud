package com.meida.bpm.provider.identity;


import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.provider.service.BaseRoleService;
import org.flowable.idm.api.Group;
import org.flowable.idm.engine.IdmEngineConfiguration;
import org.flowable.idm.engine.impl.GroupQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityManagerImpl;
import org.flowable.idm.engine.impl.persistence.entity.data.GroupDataManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 扩展自定义角色管理，使用系统角色
 *
 * @author zyf
 */
public class FlowGroupEntityManager extends GroupEntityManagerImpl {
    @Autowired
    private BaseRoleService roleService;

    public FlowGroupEntityManager(IdmEngineConfiguration idmEngineConfiguration, GroupDataManager groupDataManager) {
        super(idmEngineConfiguration, groupDataManager);
    }

    @Override
    public GroupEntity findById(String entityId) {
        BaseRole baseRole = roleService.getById(entityId);
        if (baseRole == null) {
            return null;
        }
        GroupEntity groupEntity = new GroupEntityImpl();
        //此处用角色code,对应流程xml候选角色可使用code值
        groupEntity.setId(baseRole.getRoleCode());
        groupEntity.setName(baseRole.getRoleName());
        return groupEntity;
    }

    @Override
    public List<Group> findGroupByQueryCriteria(GroupQueryImpl query) {
        List<BaseRole> roleList = roleService.getRoleByFlowableGroupQueryImpl(getParams(query));
        if (FlymeUtils.isEmpty(roleList)) {
            return new ArrayList<>();
        }
        List<Group> groups = new ArrayList<>();
        for (BaseRole role : roleList) {
            Group group = new GroupEntityImpl();
            group.setId(role.getRoleCode());
            group.setName(role.getRoleName());
            groups.add(group);
        }
        return groups;
    }

    @Override
    public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
        return roleService.getRoleByFlowableGroupQueryImpl(getParams(query)).size();
    }

    private Map<String, Object> getParams(GroupQueryImpl query) {
        Map<String, Object> params = new HashMap<>();
        String id = query.getId();
        List<String> ids = query.getIds();
        String name = query.getName();
        String nameLike = query.getNameLike();
        String nameLikeIgnoreCase = query.getNameLikeIgnoreCase();
        String type = query.getType();
        String userId = query.getUserId();
        List<String> userIds = query.getUserIds();
        if (FlymeUtils.isNotEmpty(id)) {
            params.put("id", id);
        }
        if (FlymeUtils.isNotEmpty(ids)) {
            params.put("ids", ids);
        }
        if (FlymeUtils.isNotEmpty(name)) {
            params.put("name", name);
        }
        if (FlymeUtils.isNotEmpty(nameLike)) {
            params.put("nameLike", nameLike);
        }
        if (FlymeUtils.isNotEmpty(nameLikeIgnoreCase)) {
            params.put("nameLikeIgnoreCase", nameLikeIgnoreCase);
        }
        if (FlymeUtils.isNotEmpty(userId)) {
            params.put("userId", userId);
        }
        if (FlymeUtils.isNotEmpty(userIds)) {
            params.put("userIds", userIds);
        }
        return params;

    }
}
