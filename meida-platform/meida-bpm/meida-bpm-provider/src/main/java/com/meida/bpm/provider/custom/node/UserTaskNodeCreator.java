
package com.meida.bpm.provider.custom.node;


import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.provider.custom.constant.FlowProcessConstant;
import com.meida.bpm.provider.custom.entity.ChildAttr;
import com.meida.bpm.provider.custom.entity.ChildNode;
import com.meida.bpm.provider.custom.entity.ProcessData;
import com.meida.bpm.provider.custom.model.ApproverModel;

import com.meida.bpm.provider.custom.model.ListenerModel;
import com.meida.bpm.provider.custom.util.BpmnCreator;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.exception.OpenAlertException;
import com.meida.common.utils.JsonUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.model.*;
import org.flowable.engine.delegate.ExecutionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 用户任务节点创建
 *
 * @author zyf
 */
public class UserTaskNodeCreator {
    public static final String OR_SIGN = "orSign";
    //1非会签节点
    /**
     * {
     * name: "或签(一人通过或否决)",
     * value: 1
     * },
     * {
     * name: "会签(所有审批人通过)",
     * value: 3
     * },
     * {
     * name: "会签(通过只需一人)",
     * value: 4
     * },
     * {
     * name: "会签(半数通过)",
     * value: 5
     * },
     * {
     * name: "会签(按比例投票)",
     * value: 6
     */
    public static final String COUNTER_SIGN = "1";

    /**
     * 候选部门
     */
    public static final String APPROVRE_TYPE_DEPT = "candidateDepts";

    /**
     * 候选角色
     */
    public static final String APPROVRE_TYPE_ROLE = "candidateGroups";

    /**
     * 候选用户租
     */
    public static final String APPROVRE_TYPE_USERS = "candidateUsers";

    /**
     * 指定用户
     */
    public static final String APPROVRE_TYPE_USER = "candidateUser";

    /**
     * 候选用户
     */
    public static final String ASSIGNEE_TYPE_CANDIDATE_USERS = "candidateUsers";

    /**
     * 按名称
     */
    public static final String ASSIGNEE_TYPE_BY_NAME = "assigneeByName";

    /**
     * 按表达式
     */
    public static final String ASSIGNEE_TYPE_BY_EXP = "assigneeByExp";

    /**
     * 候选角色
     */
    public static final String ASSIGNEE_TYPE_CANDIDATE_GROUPS = "candidateGroups";

    /**
     * 指定发起人自己
     */
    public static final String ASSIGNEE_TYPE_MYSELF = "myself";

    public static List<UserTask> createUserTask(ProcessData processData, List<ChildNode> approverNodes, List<FlowProcessNode> customProcessNodes) {
        List<UserTask> tasks = new ArrayList<>();
        int i=1;
        for (ChildNode node : approverNodes) {
            FlowProcessNode customProcessNode = new FlowProcessNode();
            ChildAttr childAttr = node.getAttr();
            //审批对象
            List<ApproverModel> groups = node.getApproverGroups();
            String approvalMode = FlymeUtils.getString(node.getAttr().getApprovalMode(),"1");
            String id = node.getId();
            String name = node.getName();
            customProcessNode.setProcessNodeCode(id);
            customProcessNode.setProcessNodeName(name);

            String type = node.getType();
            customProcessNode.setSortNo(i);
            i++;
            if (type.equals("edit")) {
                ChildAttr processDataAttr = processData.getAttr();
                if (ObjectUtils.isNotEmpty(processDataAttr)) {
                    String formCode = processDataAttr.getFormTableCode();
                    if (StringUtils.isNotEmpty(formCode)) {
                        //TODO 数据节点里保存数据节点ID
                        //TODO 移动端地址
                        //customProcessNode.setModelAndView("{{DOMAIN_URL}}/desform/edit/" + formCode + "/${BPM_DES_DATA_ID}?token={{TOKEN}}&taskId={{TASKID}}");
                    }
                }
            }
            if (ObjectUtils.isNotEmpty(childAttr)) {
                customProcessNode.setNodeConfigJson(JSON.toJSONString(childAttr));
            }
            // 用户节点
            UserTask userTask = new UserTask();
            userTask.setId(id);
            userTask.setName(name);


            if (!UserTaskNodeCreator.COUNTER_SIGN.equals(approvalMode)) {
                MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = new MultiInstanceLoopCharacteristics();
                // 循环数量
                multiInstanceLoopCharacteristics.setLoopCardinality(childAttr.getLoopCardinality());
                // 迭代变量
                multiInstanceLoopCharacteristics.setElementVariable(childAttr.getElementVariable());
                // 完成条件 已完成数等于实例数
                multiInstanceLoopCharacteristics.setCompletionCondition(childAttr.getCompletionCondition());
                // 循环集合
                multiInstanceLoopCharacteristics.setInputDataItem(childAttr.getCollection());
                // 并行
                multiInstanceLoopCharacteristics.setSequential(childAttr.getIsSequential());
                // 设置多实例属性
                userTask.setLoopCharacteristics(multiInstanceLoopCharacteristics);
                if (ObjectUtils.isNotEmpty(groups)) {
                    ApproverModel approverModel = groups.get(0);
                    String[] approverIds = approverModel.getApproverIds();
                    if(ObjectUtils.isNotEmpty(approverIds)){
                        String ids=StringUtils.join(approverIds,",");
                        multiInstanceLoopCharacteristics.setInputDataItem("${minFlowUtils.stringToList('"+ids+"')}");
                    }
                    String[] expressionsIds = approverModel.getExpressionsIds();
                    if(ObjectUtils.isNotEmpty(expressionsIds)){
                        userTask.setAssignee(expressionsIds[0]);
                    }else{
                        userTask.setAssignee("${"+childAttr.getElementVariable()+"}");
                    }
                }
            }
            if (ObjectUtils.isNotEmpty(groups)) {
                //取第一组数据(前端传递的是数组)
                ApproverModel approverModel = groups.get(0);
                customProcessNode.setApproveModel(JsonUtils.beanToJson(approverModel));
                //取值方式(按名称,按表达式)
                String assigneeType = approverModel.getAssigneeType();
                //审批对象类型(用户(单个),候选用户(多个),角色,部门)
                String approverType = approverModel.getApproverType();
                if(UserTaskNodeCreator.COUNTER_SIGN.equals(approvalMode)) {
                    //指定用户
                    if (APPROVRE_TYPE_USER.equals(approverType)) {
                        //按用户名
                        if (ASSIGNEE_TYPE_BY_NAME.equals(assigneeType)) {
                            String[] approverIds = approverModel.getApproverIds();
                            if (ObjectUtils.isNotEmpty(approverIds)) {
                                userTask.setAssignee(approverIds[0]);
                            }
                        }
                        //按表达式
                        if (ASSIGNEE_TYPE_BY_EXP.equals(assigneeType)) {
                            String[] expressionsIds = approverModel.getExpressionsIds();
                            if (ObjectUtils.isNotEmpty(expressionsIds)) {
                                userTask.setAssignee(expressionsIds[0]);
                            }
                        }
                        //指定自己
                        if (ASSIGNEE_TYPE_MYSELF.equals(assigneeType)) {
                            userTask.setAssignee("${applyUserId}");
                        }
                    }

                    //候选用户
                    if (APPROVRE_TYPE_USERS.equals(approverType)) {
                        //用户名
                        if (ASSIGNEE_TYPE_BY_NAME.equals(assigneeType)) {
                            String[] approverIds = approverModel.getApproverIds();
                            if (ObjectUtils.isNotEmpty(approverIds)) {
                                if (approverIds.length > 1) {
                                    userTask.setCandidateUsers(Arrays.asList(approverIds));
                                } else {
                                    //选择一个用户时是指定人
                                    userTask.setAssignee(approverIds[0]);
                                }
                            }
                        }
                        //表达式
                        if (ASSIGNEE_TYPE_BY_EXP.equals(assigneeType)) {
                            String[] expressionsIds = approverModel.getExpressionsIds();
                            List<String> expressions = new ArrayList<>();
                            if (ObjectUtils.isNotEmpty(expressionsIds)) {
                                expressions.add(expressionsIds[0]);
                                userTask.setCandidateUsers(expressions);
                            }
                        }
                    }
                    //候选角色
                    if (APPROVRE_TYPE_ROLE.equals(approverType)) {
                        //按名称
                        if (ASSIGNEE_TYPE_BY_NAME.equals(assigneeType)) {
                            String[] roleIds = approverModel.getRoleIds();
                            if (ObjectUtils.isNotEmpty(roleIds)) {
                                userTask.setCandidateGroups(Arrays.asList(roleIds));
                            }
                        }
                        //按表达式
                        if (ASSIGNEE_TYPE_BY_EXP.equals(assigneeType)) {
                            String[] expressionsIds = approverModel.getExpressionsIds();
                            List<String> expressions = new ArrayList<>();
                            if (ObjectUtils.isNotEmpty(expressionsIds)) {
                                expressions.add(expressionsIds[0]);
                                userTask.setCandidateGroups(expressions);
                            }
                        }
                    }

                    //候选部门
                    if (APPROVRE_TYPE_DEPT.equals(approverType)) {
                        //按名称
                        if (ASSIGNEE_TYPE_BY_NAME.equals(assigneeType)) {
                            String[] deptIds = approverModel.getDeptIds();
                            if (ObjectUtils.isNotEmpty(deptIds)) {
                                userTask.setCandidateGroups(Arrays.asList(deptIds));
                            }
                        }
                        //按表达式
                        if (ASSIGNEE_TYPE_BY_EXP.equals(assigneeType)) {
                            String[] expressionsIds = approverModel.getExpressionsIds();
                            List<String> expressions = new ArrayList<>();
                            if (ObjectUtils.isNotEmpty(expressionsIds)) {
                                expressions.add(expressionsIds[0]);
                                userTask.setCandidateGroups(expressions);
                            }
                        }
                    }
                }
                //设置监听
                setListener(userTask, node,approverType);
            }
            //设置时间边界
            setBoundaryEvent(userTask, node);
            tasks.add(userTask);
            //添加流程节点用于保存节点
            customProcessNodes.add(customProcessNode);
        }
        return tasks;
    }

    private static List<FlowableListener> getGroupCounterSignListener(String list, String listType) {
        ArrayList<FlowableListener> listeners = new ArrayList<FlowableListener>();
        FlowableListener activitiListener = new FlowableListener();
        // 事件类型,
        activitiListener.setEvent(ExecutionListener.EVENTNAME_START);
        // 监听器类型
        activitiListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
        // 设置实现了，这里设置监听器的类型是delegateExpression，这样可以在实现类注入Spring bean.
        activitiListener.setImplementation("${groupCounterSignListener}");

        List<FieldExtension> fieldExtensions = new ArrayList<FieldExtension>();

        if (listType.equals(UserTaskNodeCreator.ASSIGNEE_TYPE_CANDIDATE_USERS)) {
            FieldExtension userListField = new FieldExtension();
            userListField.setFieldName("userList");
            userListField.setStringValue(list);
            fieldExtensions.add(userListField);
        } else if (listType.equals(UserTaskNodeCreator.ASSIGNEE_TYPE_CANDIDATE_GROUPS)) {
            FieldExtension roleListField = new FieldExtension();
            roleListField.setFieldName("roleList");
            roleListField.setStringValue(list);
            fieldExtensions.add(roleListField);
        } else {
            throw new OpenAlertException("请求的参数不合法");
        }


        activitiListener.setFieldExtensions(fieldExtensions);
        listeners.add(activitiListener);
        return listeners;
    }

    /**
     * 设置监听
     *
     * @param userTask
     * @param node
     * @param approverType
     */
    private static void setListener(UserTask userTask, ChildNode node,String approverType) {
        //任务监听
        List<ListenerModel> taskListenerModel = node.getTaskListenerData();
        ChildAttr childAttr = node.getAttr();
        //审批人与发起人是同一人时的业务处理且只有当审批人类型是指定人人员时 加入自动审批监听
        if (ObjectUtils.isNotEmpty(childAttr) && APPROVRE_TYPE_USER.equals(approverType)) {
            Integer sameMode = childAttr.getSameMode();
            //不需要自己审批时设置审批监听处理
            if (ObjectUtils.isNotEmpty(sameMode) && !sameMode.equals(1)) {
                ListenerModel listenerModel = new ListenerModel();
                listenerModel.setListenerType(FlowProcessConstant.javaClass);
                listenerModel.setEventType("create");
                listenerModel.setValue("org.jeecg.modules.listener.easyoa.TaskApprovalListener");
                listenerModel.setId("listener" + IdWorker.getId());
                taskListenerModel.add(listenerModel);
            }
        }
        if (ObjectUtils.isNotEmpty(taskListenerModel)) {
            List<FlowableListener> taskListeners = new ArrayList<>();
            for (ListenerModel taskListener : taskListenerModel) {
                FlowableListener activitiListener = BpmnCreator.createActivitiListener(taskListener);
                taskListeners.add(activitiListener);
            }
            userTask.setTaskListeners(taskListeners);
        }
        //执行监听
        List<ListenerModel> listenerModel = node.getListenerData();
        if (ObjectUtils.isNotEmpty(listenerModel)) {
            List<FlowableListener> executionListeners = new ArrayList<FlowableListener>();
            for (ListenerModel listener : listenerModel) {
                FlowableListener activitiListener = BpmnCreator.createActivitiListener(listener);
                executionListeners.add(activitiListener);
            }
            userTask.setExecutionListeners(executionListeners);
        }
    }

    /**
     * 设置边界事件
     *
     * @param userTask
     * @param childNode
     */
    private static void setBoundaryEvent(UserTask userTask, ChildNode childNode) {
        ChildAttr childAttr = childNode.getAttr();
        if (ObjectUtils.isNotEmpty(childAttr)) {
            String timeDate = childAttr.getTimeDate();
            if (ObjectUtils.isNotEmpty(timeDate)) {
                List<BoundaryEvent> boundaryEvents = new ArrayList<>();
                BoundaryEvent boundaryEvent = new BoundaryEvent();
                boundaryEvent.setId("Event_" + IdUtil.simpleUUID().substring(0, 7));
                boundaryEvent.setAttachedToRef(userTask);
                TimerEventDefinition timerEventDefinition = new TimerEventDefinition();
                timerEventDefinition.setId("TimerEventDefinition_" + IdUtil.simpleUUID().substring(0, 7));
                timerEventDefinition.setTimeDate(timeDate);
                boundaryEvent.addEventDefinition(timerEventDefinition);
                boundaryEvents.add(boundaryEvent);
                userTask.setBoundaryEvents(boundaryEvents);
            }
        }

    }
}
