package com.meida.bpm.provider.service.impl;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.bpm.client.entity.FlowCategory;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.provider.common.cmd.ProcessModelDeployCmd;
import com.meida.bpm.provider.common.cmd.ProcessModelSaveCmd;
import com.meida.bpm.provider.custom.model.CustomProcessForm;
import com.meida.bpm.provider.custom.util.CustomFlowProcessUtil;
import com.meida.bpm.provider.mapper.FlowProcessMapper;
import com.meida.bpm.provider.service.*;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtils;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.ManagementService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.impl.persistence.entity.DeploymentEntityImpl;
import org.flowable.engine.impl.persistence.entity.ProcessDefinitionEntityImpl;
import org.flowable.engine.repository.Model;
import org.flowable.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程表接口实现类
 *
 * @author flyme
 * @date 2020-06-11
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowProcessServiceImpl extends BaseServiceImpl<FlowProcessMapper, FlowProcess> implements FlowProcessService {

    @Autowired
    protected RepositoryService repositoryService;

    @Autowired
    protected ManagementService managementService;

    @Autowired
    protected ProcessDefinitionService processDefinitionService;

    @Autowired
    protected FlowNodeService flowNodeService;

    @Autowired
    protected FlowCategoryService flowCategoryService;

    @Autowired
    protected FlowBusinessService flowBusinessService;

    @Override
    public ResultBody basePageList(CriteriaQuery<?> cq) {
        return super.basePageList(cq);
    }

    @Override
    public List<EntityMap> afterPageList(CriteriaQuery<FlowProcess> cq, List<EntityMap> data, ResultBody resultBody) {
        for (EntityMap datum : data) {
            String procDefId = datum.get("procDefId");
            if (FlymeUtils.isNotEmpty(procDefId)) {
                ProcessDefinition processDefinition = processDefinitionService.getProcessDefinitionById(procDefId);
                datum.put("suspended", processDefinition.isSuspended());
            }
        }
        return  super.afterPageList(cq, data, resultBody);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, FlowProcess process, EntityMap extra) {
        Long companyId=process.getCompanyId();
        process.setCompanyId(companyId);
        Long processTypeId=process.getProcessTypeId();
        FlowProcess check=findByTypeId(processTypeId,companyId);
        ApiAssert.isEmpty("该审批流程已配置,无法创建！", check);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<FlowProcess> cq, FlowProcess flowProcess, EntityMap requestMap) {
        cq.eq(FlowProcess.class,"companyId");
        return super.beforeListEntityMap(cq, flowProcess, requestMap);
    }



    @Override
    public ResultBody afterAdd(CriteriaSave cs, FlowProcess flowProcess, EntityMap extra) {
        return super.afterAdd(cs, flowProcess, extra);
    }


    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, FlowProcess flowProcess, EntityMap extra) {
        String procModelId=flowProcess.getProcModelId();
        if(FlymeUtils.isNotEmpty(procModelId)) {
            Model model = getModelById(procModelId);
            String processXml = flowProcess.getProcessXml();
            model.setKey(flowProcess.getProcessKey());
            model.setName(flowProcess.getProcessName());
            model.setTenantId(OpenHelper.getCompanyId().toString());
            model.setMetaInfo(getMetaInfo(flowProcess));
            repositoryService.saveModel(model);
            if (FlymeUtils.isNotEmpty(processXml)) {
                //保存流程文件
                managementService.executeCommand(new ProcessModelSaveCmd(model.getId(), processXml));
            }
        }
        return super.afterEdit(cu, flowProcess, extra);
    }

    protected Model getModelById(String modelId) {
        Model model = repositoryService.getModel(modelId);
        ApiAssert.isNotEmpty("流程模型不存在", model);
        return model;
    }

    private String getMetaInfo(FlowProcess flowProcess) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode metaInfo = objectMapper.createObjectNode();
        metaInfo.put("name", flowProcess.getProcessName());
        metaInfo.put("description", flowProcess.getDescription());
        return metaInfo.toString();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<FlowProcess> cq, FlowProcess process, EntityMap requestMap) {
        cq.orderByDesc("process.createTime");
        return ResultBody.ok();
    }

    @Override
    public ResultBody deleteByProcDefId(String procDefId) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq(true, "procDefId", procDefId);
        Boolean tag = remove(cd);
        return ResultBody.ok(tag);
    }
    @Override
    public ResultBody deployModel(String processKey) {
        FlowProcess flowProcess = getByProcessKey(processKey);
        String procModelId=flowProcess.getProcModelId();
        if(FlymeUtils.isNotEmpty(procModelId)){
            Model model = repositoryService.getModel(procModelId);
            if(FlymeUtils.isNotEmpty(model)){
                String processXml = flowProcess.getProcessXml();
                model.setKey(flowProcess.getProcessKey());
                model.setName(flowProcess.getProcessName());
                model.setMetaInfo(getMetaInfo(flowProcess));
                flowProcess.setVersionTag(model.getVersion());
                repositoryService.saveModel(model);
                if (FlymeUtils.isNotEmpty(processXml)) {
                    //保存流程文件
                    managementService.executeCommand(new ProcessModelSaveCmd(model.getId(), processXml));
                }
            }
        }else{
            Model model = saveModel(flowProcess);
            flowProcess.setProcModelId(model.getId());
            flowProcess.setVersionTag(model.getVersion());
        }
        return  deployModel(flowProcess.getProcModelId(),flowProcess);
    }

    @Override
    public ResultBody deployModel(String procModelId, String processId) {
        FlowProcess flowProcess = getById(processId);
        if(FlymeUtils.isEmpty(procModelId)) {
            procModelId=flowProcess.getProcModelId();
        }
        return  deployModel(procModelId,flowProcess);

    }

    private ResultBody deployModel(String  procModelId,FlowProcess flowProcess) {
        DeploymentEntityImpl obj = (DeploymentEntityImpl) managementService.executeCommand(new ProcessModelDeployCmd(procModelId));
        List<ProcessDefinitionEntityImpl> processDefList = obj.getDeployedArtifacts(ProcessDefinitionEntityImpl.class);
        if (FlymeUtils.isNotEmpty(processDefList)) {
            for (ProcessDefinitionEntityImpl entity : processDefList) {
                //关联流程定义id
                flowProcess.setProcDefId(entity.getId());
                //已部署
                flowProcess.setDeployState(1);
                updateById(flowProcess);
            }
        }
        return ResultBody.ok();
    }

    @Override
    public FlowProcess getByProcessKey(String processKey) {
        CriteriaQuery cq = new CriteriaQuery(FlowProcess.class);
        cq.eq(true, "processKey", processKey);
        return getOne(cq);
    }




    @Override
    public void addProcess(DeploymentEntityImpl deploymentEntity, String processXml) {
        List<ProcessDefinitionEntityImpl> process = deploymentEntity.getDeployedArtifacts(ProcessDefinitionEntityImpl.class);
        if (FlymeUtils.isNotEmpty(process)) {
            for (ProcessDefinitionEntityImpl entity : process) {
                FlowProcess flowProcess = new FlowProcess();
                flowProcess.setProcessName(entity.getName());
                flowProcess.setProcessKey(entity.getKey());
                flowProcess.setProcessXml(processXml);
                flowProcess.setProcessState(entity.getSuspensionState());
                flowProcess.setProcDefId(entity.getId());
                flowProcess.setDescription(entity.getDescription());
                flowProcess.setVersionTag(entity.getVersion());
                save(flowProcess);
            }
        }
    }

    @Override
    public void importProcess(DeploymentEntityImpl deploymentEntity, String processXml) {
        List<ProcessDefinitionEntityImpl> process = deploymentEntity.getDeployedArtifacts(ProcessDefinitionEntityImpl.class);
        if (FlymeUtils.isNotEmpty(process)) {
            for (ProcessDefinitionEntityImpl entity : process) {
                FlowProcess flowProcess = new FlowProcess();
                flowProcess.setProcessName(entity.getName());
                flowProcess.setProcessKey(entity.getKey());
                flowProcess.setProcessXml(processXml);
                flowProcess.setProcessState(entity.getSuspensionState());
                flowProcess.setDescription(entity.getDescription());
                flowProcess.setVersionTag(entity.getVersion());
                flowProcess.setCompanyId(OpenHelper.getCompanyId());
                //保存流程模型
                Model model = saveModel(flowProcess);
                flowProcess.setProcModelId(model.getId());
                save(flowProcess);
                //删除流程定义
                processDefinitionService.delete(entity.getId());

            }
        }
    }

    protected void checkModelKeyExists(String modelKey) {
        long countNum = repositoryService.createModelQuery().modelKey(modelKey).count();
        if (countNum > 0) {
            throw new FlowableObjectNotFoundException("ModelKey already exists with id " + modelKey);
        }
    }

    @Override
    public  ResultBody saveProcess(FlowProcess flowProcess){
        Long companyId=FlymeUtils.isEmpty(flowProcess.getCompanyId())?OpenHelper.getCompanyId():flowProcess.getCompanyId();
        flowProcess.setCompanyId(companyId);
        Long flowProcessTypeId=flowProcess.getProcessTypeId();
        ApiAssert.isNotEmpty("请选择流程分类", flowProcessTypeId);
        CustomProcessForm customProcessForm = new CustomProcessForm();
        List<FlowProcessNode> customProcessNodes=new ArrayList<>();
        String processJson=flowProcess.getProcessJson();
        if(FlymeUtils.isNotEmpty(processJson)){
            //json转xml
            String xml = CustomFlowProcessUtil.JSONToBPMN(flowProcess, customProcessForm, customProcessNodes);
            flowProcess.setProcessXml(xml);
        }
        String processKey=flowProcess.getProcessKey();
        //校验流程是否存在
        FlowProcess check=getByProcessKey(processKey);
        if(FlymeUtils.isNotEmpty(check)){
            flowProcess.setProcessId(check.getProcessId());
            flowProcess.setDeployState(check.getDeployState());
            String oldJson=check.getProcessJson();
            try {
                JSONObject jsonObj1 = new JSONObject(processJson);
                JSONObject jsonObj2 = new JSONObject(oldJson);
                boolean isEqual = jsonObj1.equals(jsonObj2);
                if(!isEqual){
                    //未部署
                    flowProcess.setDeployState(0);
                }
            }catch (Exception e){

            }
            updateById(flowProcess);
        }else{
            Long processTypeId=flowProcess.getProcessTypeId();
            FlowProcess check1=findByTypeId(processTypeId,companyId);
            ApiAssert.isEmpty("该审批流程已配置,无法创建！", check1);
            //启用
            flowProcess.setProcessState(1);
            //未部署
            flowProcess.setDeployState(0);
            //允许删除
            flowProcess.setAllowDel(1);
            flowProcess.setProcessId(IdWorker.getId());
            save(flowProcess);
        }
        //保存流程节点
        flowNodeService.saveProcessNode(flowProcess.getProcessId(), customProcessNodes);
        return ResultBody.ok("保存成功",flowProcess);
    }

    @Override
    public Boolean setStatus(Long processId,Integer processState) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set("processState", processState);
        cu.eq("processId", processId);
        return  update(cu);
    }



    @Override
    public Boolean copyProcess(Long processId) {
        FlowProcess flowProcess=getById(processId);
        if(FlymeUtils.isNotEmpty(flowProcess)){
            flowProcess.setProcessId(IdWorker.getId());
            flowProcess.setProcessName(flowProcess.getProcessName()+"-拷贝");
            flowProcess.setCreateTime(DateUtils.getNowDateTime());
            flowProcess.setUpdateTime(null);
            flowProcess.setProcDefId("");
            flowProcess.setProcModelId("");
            flowProcess.setProcessState(1);
            flowProcess.setDeployState(0);
            save(flowProcess);
        }
        return true;
    }

    @Override
    public FlowProcess findByTypeId(Long processTypeId, Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(FlowProcess.class);
        cq.eq(true, "processTypeId", processTypeId);
        cq.eq(true, "companyId", companyId);
        return getOne(cq,false);
    }

    @Override
    public FlowProcess findByTypeCode(String processTypeCode, Long companyId) {
        FlowCategory category=flowCategoryService.findByCode(processTypeCode);
        if(FlymeUtils.isNotEmpty(category)){
            CriteriaQuery cq = new CriteriaQuery(FlowProcess.class);
            cq.eq(true, "processTypeId", category.getCategoryId());
            cq.eq(true, "companyId", companyId);
            return getOne(cq,false);
        }
        return null;
    }

    @Override
    public FlowProcess findByTypeCodeAndState(String processTypeCode,Integer processState, Long companyId) {
        FlowCategory category=flowCategoryService.findByCode(processTypeCode);
        if(FlymeUtils.isNotEmpty(category)){
            CriteriaQuery cq = new CriteriaQuery(FlowProcess.class);
            cq.eq(true, "processTypeId", category.getCategoryId());
            cq.eq(true, "companyId", companyId);
            cq.eq(true, "processState", processState);
            return getOne(cq,false);
        }
        return null;
    }


    private Model saveModel(FlowProcess flowProcess) {
        String processKey = flowProcess.getProcessKey();
        checkModelKeyExists(processKey);
        Model model = repositoryService.newModel();
        model.setKey(flowProcess.getProcessKey());
        model.setName(flowProcess.getProcessName());
        model.setVersion(1);
        model.setTenantId(flowProcess.getCompanyId().toString());
        model.setMetaInfo(getMetaInfo(flowProcess));
        repositoryService.saveModel(model);
        //保存流程文件
        managementService.executeCommand(new ProcessModelSaveCmd(model.getId(), flowProcess.getProcessXml()));
        return model;
    }

    @Override
    public ResultBody beforeDelete(CriteriaDelete<FlowProcess> cd) {
        Long processId=cd.getIdValue();
        Long count= flowBusinessService.countByProcessId(processId);
        if(count>0L){
            ApiAssert.failure("有审批业务正在审批中!");
        }
        return super.beforeDelete(cd);
    }
}
