package com.meida.bpm.provider.model;

import lombok.Data;

import java.util.Map;

/**
 * @author zyf
 */
@Data
public class ProcessInstanceRequest {
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 流程key
     */
    private String processDefinitionKey;
    /**
     * 租户ID
     */
    private String tenantId;
    /**
     * 表单数据
     */
    private Map<String, Object> formMap;
    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 业务handler
     */
    private String handlerName;
    /**
     * 业务主键
     */
    private String businessKey;

    /**
     * 自定义流程key
     */
    private String processKey;

    private CcToVo[] ccToVos;

    /**
     * 审批标记
     */
    private Integer approve;
}
