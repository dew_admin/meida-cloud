package com.meida.bpm.provider.handler;

import com.meida.bpm.provider.common.handler.ProcessStartHandler;
import com.meida.bpm.provider.model.ProcessInstanceRequest;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 */
@Component
public class TestStartProcessHandler implements ProcessStartHandler {


    @Override
    public String init(ProcessInstanceRequest processInstanceRequest) {
        return "1";
    }
}
