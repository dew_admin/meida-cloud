package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowCategory;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 流程分类表 接口
 *
 * @author flyme
 * @date 2020-06-14
 */
public interface FlowCategoryService extends IBaseService<FlowCategory> {
    FlowCategory findByCode(String categoryCode);

    List<EntityMap> selectTypeList();
}
