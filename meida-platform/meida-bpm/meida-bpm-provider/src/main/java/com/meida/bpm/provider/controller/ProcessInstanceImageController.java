package com.meida.bpm.provider.controller;

import com.meida.bpm.provider.common.BaseFlowableController;
import com.meida.bpm.provider.configuration.CustomProcessDiagramGenerator;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import org.apache.commons.io.IOUtils;
import org.flowable.bpmn.constants.BpmnXMLConstants;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/**
 * @author zyf
 */
@RestController
@RequestMapping("/bpm/processInstance")
public class ProcessInstanceImageController extends BaseFlowableController {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private ProcessEngineConfiguration processEngineConfiguration;

    @GetMapping(value = "/processInstanceImage")
    public ResultBody image(@RequestParam String processInstanceId) {
        HistoricProcessInstance processInstance = permissionService
                .validateReadPermissionOnProcessInstance(OpenHelper.getStrUserId(), processInstanceId);
        ProcessDefinition pde = repositoryService.getProcessDefinition(processInstance.getProcessDefinitionId());
        if (pde == null || !pde.hasGraphicalNotation()) {
            throw new FlowableException(messageFormat("Process instance image is not found with id {0}", processInstanceId));
        }
        List<String> highLightedFlows = new ArrayList<>();
        List<String> highLightedActivities = new ArrayList<>();
        List<HistoricActivityInstance> allHistoricActivityIntances = historyService
                .createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();
        allHistoricActivityIntances.forEach(historicActivityInstance -> {
            if (BpmnXMLConstants.ELEMENT_SEQUENCE_FLOW.equals(historicActivityInstance.getActivityType())) {
                highLightedFlows.add(historicActivityInstance.getActivityId());
            } else {
                highLightedActivities.add(historicActivityInstance.getActivityId());
            }
        });

        List<String> runningActivitiIdList = null;
        // 流程已结束
        if (processInstance != null && processInstance.getEndTime() != null) {
            runningActivitiIdList = Arrays.asList();
        } else {
            runningActivitiIdList = runtimeService.getActiveActivityIds(processInstanceId);
        }

        BpmnModel bpmnModel = repositoryService.getBpmnModel(pde.getId());
        CustomProcessDiagramGenerator diagramGenerator = (CustomProcessDiagramGenerator) processEngineConfiguration.getProcessDiagramGenerator();
        InputStream resource = diagramGenerator.generateCustomDiagram(bpmnModel, "png", highLightedActivities,
                runningActivitiIdList, highLightedFlows, processEngineConfiguration.getActivityFontName(),
                processEngineConfiguration.getLabelFontName(), processEngineConfiguration.getAnnotationFontName(),
                processEngineConfiguration.getClassLoader(), 1.0, true);
        byte[] bytes = new byte[0];
        try {
            bytes = IOUtils.toByteArray(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String encoded = Base64.getEncoder().encodeToString(bytes);
        return ResultBody.ok(encoded);
    }
}
