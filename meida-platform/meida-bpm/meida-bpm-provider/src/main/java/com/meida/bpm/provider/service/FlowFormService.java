package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowForm;
import com.meida.common.mybatis.base.service.IBaseService;


/**
 * 流程表单Service
 * 
 * @author zyf
 */
public interface FlowFormService extends IBaseService<FlowForm> {

    FlowForm getByFormKey(String formKey);
}
