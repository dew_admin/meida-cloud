package com.meida.bpm.provider.service.impl;

import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.bpm.client.entity.FlowComment;
import com.meida.bpm.provider.mapper.FlowCommentMapper;
import com.meida.bpm.provider.service.FlowBusinessService;
import com.meida.bpm.provider.service.FlowCommentService;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 流程评论表接口实现类
 *
 * @author flyme
 * @date 2024-12-16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowCommentServiceImpl extends BaseServiceImpl<FlowCommentMapper, FlowComment> implements FlowCommentService {

    @Autowired
    private FlowBusinessService flowBusinessService;

    @Autowired
    private BaseUserService baseUserService;

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<FlowComment> cq, FlowComment flowComment, EntityMap requestMap) {
        cq.eq(FlowComment.class,"flowBusId");
        cq.isNull("replyCommentId");
        cq.orderByDesc("createTime");
        return super.beforeListEntityMap(cq, flowComment, requestMap);
    }

    @Override
    public ResultBody afterListEntityMap(CriteriaQuery<FlowComment> cq, List<EntityMap> data, ResultBody resultBody) {
        for (EntityMap map : data) {
            Long userId = map.getLong("userId");
            BaseUser baseUser=baseUserService.getById(userId);
            if(FlymeUtils.isNotEmpty(baseUser)){
                map.put("nickName", baseUser.getNickName());
                map.put("avatar", baseUser.getAvatar());
            }
            Long commentId=map.getLong("commentId");
            List replyCommentList=listByCommentId(commentId);
            map.put("replyCommentList",replyCommentList);
        }
        return super.afterListEntityMap(cq, data, resultBody);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, FlowComment comment, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        Long flowBusId=comment.getFlowBusId();
        comment.setUserId(userId);
        FlowBusiness flowBusiness=flowBusinessService.getById(flowBusId);
        comment.setProcessId(flowBusiness.getFlowProcessId());
        comment.setProcessInstanceId(flowBusiness.getProcessInstanceId());
        Long replyCommentId = comment.getReplyCommentId();
        if (FlymeUtils.isNotEmpty(replyCommentId)) {
            FlowComment parent=getById(replyCommentId);
            comment.setReplyToUserId(parent.getUserId());
            comment.setReplyCommentId(parent.getCommentId());
        }
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<FlowComment> cq, FlowComment comment, EntityMap requestMap) {
      cq.orderByDesc("comment.createTime");
      return ResultBody.ok();
    }

    public  List<EntityMap> listByCommentId(Long commentId){
        CriteriaQuery cq=new CriteriaQuery(FlowComment.class);
        cq.eq(true,"replyCommentId",commentId);
        cq.orderByAsc("createTime");
        List<EntityMap> list=selectEntityMap(cq);
        if(FlymeUtils.isNotEmpty(list)){
            for (EntityMap entityMap : list) {
                Long userId=entityMap.getLong("userId");
                BaseUser baseUser=baseUserService.getById(userId);
                if(FlymeUtils.isNotEmpty(baseUser)){
                    entityMap.put("nickName", baseUser.getNickName());
                    entityMap.put("avatar", baseUser.getAvatar());
                }
            }
        }
        return  list;


    }
}
