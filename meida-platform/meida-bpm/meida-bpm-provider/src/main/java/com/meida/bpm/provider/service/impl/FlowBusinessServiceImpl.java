package com.meida.bpm.provider.service.impl;

import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.bpm.provider.common.CommentTypeEnum;
import com.meida.bpm.provider.handler.TaskCompleteHandler;
import com.meida.bpm.provider.handler.TaskUrgingHandler;
import com.meida.bpm.provider.mapper.FlowBusinessMapper;
import com.meida.bpm.provider.model.AppRoverUserVo;
import com.meida.bpm.provider.service.FlowBusinessService;
import com.meida.bpm.provider.service.FlowNodeService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.security.OpenUser;
import com.meida.common.utils.DateUtils;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.admin.provider.service.BaseUserService;
import org.flowable.engine.TaskService;
import org.flowable.engine.task.Comment;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 流程业务数据接口实现类
 *
 * @author flyme
 * @date 2024-12-12
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowBusinessServiceImpl extends BaseServiceImpl<FlowBusinessMapper, FlowBusiness> implements FlowBusinessService {

    @Autowired
    private FlowNodeService flowNodeService;

    @Autowired
    protected TaskService taskService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, FlowBusiness business, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<FlowBusiness> cq, FlowBusiness business, EntityMap requestMap) {
      cq.orderByDesc("business.createTime");
      return ResultBody.ok();
    }

    @Override
    public void updateProcessInstanceId(String processInstanceId, Long flowBusId,Integer flowState) {
        CriteriaUpdate cu=new CriteriaUpdate();
        cu.set("processInstanceId", processInstanceId);
        //审批中
        cu.set("flowState", flowState);
        cu.eq(true,"flowBusId", flowBusId);
        update(cu);
    }

    @Override
    public Long countMySend(){
        CriteriaQuery cq=new CriteriaQuery(FlowBusiness.class);
        Long userId= OpenHelper.getUserId();
        Long companyId= OpenHelper.getCompanyId();
        cq.eq("companyId", companyId);
        cq.eq("applyUserId", userId);
        return count(cq);
    }

    @Override
    public void logicDelete(Long flowBusId) {
        CriteriaUpdate cu=new CriteriaUpdate();
        //逻辑删除
        cu.set("deleted", 1);
        cu.eq(true,"flowBusId", flowBusId);
        update(cu);
    }

    @Override
    public Boolean setUrging(Long flowBusId,  Integer urging,String taskDefinitionKey,String taskId,String handlerName) {
        OpenUser openUser=OpenHelper.getUser();
        FlowBusiness flowBusiness=getById(flowBusId);
        flowBusiness.setUrging(urging);
        flowBusiness.setUrgingDate( DateUtils.getNowDate());
        updateById(flowBusiness);
        List<String> userIds=new ArrayList<>();
        //该节点审批用户
        AppRoverUserVo appRoverUserVo=flowNodeService.getAppRoverUser(taskId,flowBusiness.getFlowProcessId(),taskDefinitionKey);
        if(FlymeUtils.isNotEmpty(appRoverUserVo)){
            userIds=appRoverUserVo.getUserIds();
        }
        //当前环节已审批人
        List<Comment> commentList = taskService.getTaskComments(taskId, CommentTypeEnum.WC.toString());
        //当前环节认领人
        List<Comment> rlCommentList = taskService.getTaskComments(taskId, CommentTypeEnum.RL.toString());

        //当前环节转办人
        List<Comment> zbCommentList = taskService.getTaskComments(taskId, CommentTypeEnum.ZB.toString());

        if(FlymeUtils.isNotEmpty(zbCommentList)){
            if(FlymeUtils.isNotEmpty(commentList)){
                List<String> ids=commentList.stream().map(Comment::getUserId).collect(Collectors.toList());
                zbCommentList.removeAll(ids);
            }
            //添加转办人
            if(FlymeUtils.isNotEmpty(zbCommentList)){
                for (Comment comment : zbCommentList) {
                    String[] msg = comment.getFullMessage().split("#_#");
                    String userId = msg[1];
                    userIds.add(userId);
                    userIds.remove(comment.getUserId());
                }
            }
        }
        if(FlymeUtils.isNotEmpty(userIds)){
            String approverType=appRoverUserVo.getApproverType();
            if(FlymeUtils.isNotEmpty(rlCommentList)){
                //排除认领人
                List<String> ids=rlCommentList.stream().map(Comment::getUserId).collect(Collectors.toList());
                userIds.removeAll(ids);
            }
            //角色模式下只给认领人发
            if(approverType.equals("candidateGroups")){
                if(FlymeUtils.isNotEmpty(rlCommentList)){
                    userIds.clear();
                    for (Comment comment : rlCommentList) {
                        userIds.add(comment.getUserId());
                    }
                }
            }
            if(FlymeUtils.isNotEmpty(commentList)){
                //排除已完成的人
                List<String> ids=commentList.stream().map(Comment::getUserId).collect(Collectors.toList());
                userIds.removeAll(ids);
            }

            TaskUrgingHandler taskUrgingHandler = SpringContextHolder.getHandler(handlerName, TaskUrgingHandler.class);
            if(FlymeUtils.isNotEmpty(taskUrgingHandler)){
                taskUrgingHandler.completeAfter(userIds,flowBusiness,openUser);
            }
        }
        return true;
    }

    @Override
    public Long countByProcessId(Long processId) {
        CriteriaQuery cq=new CriteriaQuery(FlowBusiness.class);
        cq.eq(true,"flowProcessId",processId);
        return count(cq);
    }
}
