package com.meida.bpm.provider.mapper;

import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 流程业务数据 Mapper 接口
 * @author flyme
 * @date 2024-12-12
 */
public interface FlowBusinessMapper extends SuperMapper<FlowBusiness> {

}
