package com.meida.bpm.provider.service.impl;


import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.bpm.provider.common.CommentTypeEnum;
import com.meida.bpm.provider.common.FlowableUtils;
import com.meida.bpm.provider.common.ResponseFactory;
import com.meida.bpm.provider.common.cmd.AddCcIdentityLinkCmd;
import com.meida.bpm.provider.common.cmd.BackUserTaskCmd;
import com.meida.bpm.provider.common.cmd.CompleteTaskReadCmd;
import com.meida.bpm.provider.common.service.impl.BaseFlowableServiceImpl;
import com.meida.bpm.provider.constant.FlowableConstant;
import com.meida.bpm.provider.handler.TaskCompleteHandler;
import com.meida.bpm.provider.model.*;
import com.meida.bpm.provider.service.FlowableTaskService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.security.OpenUser;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.DateUtils;
import com.meida.common.utils.SpringContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.flowable.bpmn.constants.BpmnXMLConstants;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.*;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.api.FlowableIllegalArgumentException;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.editor.language.json.converter.util.CollectionUtils;
import org.flowable.engine.*;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ActivityInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.identitylink.api.IdentityLink;
import org.flowable.identitylink.api.IdentityLinkType;
import org.flowable.identitylink.api.history.HistoricIdentityLink;
import org.flowable.idm.api.Group;
import org.flowable.idm.api.User;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskInfo;
import org.flowable.task.api.TaskQuery;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.flowable.task.service.impl.HistoricTaskInstanceQueryProperty;
import org.flowable.task.service.impl.TaskQueryProperty;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zyf
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowableTaskServiceImpl extends BaseFlowableServiceImpl implements FlowableTaskService {
    @Autowired
    protected IdentityService identityService;
    @Autowired
    protected RepositoryService repositoryService;
    @Autowired
    protected TaskService taskService;
    @Autowired
    protected RuntimeService runtimeService;
    @Autowired
    protected HistoryService historyService;
    @Autowired
    protected PermissionServiceImpl permissionService;
    @Autowired
    protected ResponseFactory responseFactory;
    @Autowired
    protected ManagementService managementService;
    @Autowired
    protected FormService formService;

    protected HistoricTaskInstanceQuery createHistoricTaskInstanceQuery(Map<String, String> requestParams) {
        HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery();
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_ID))) {
            query.taskId(requestParams.get(FlowableConstant.TASK_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID))) {
            query.processInstanceId(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_INSTANCE_BUSINESS_KEY))) {
            query.processInstanceBusinessKeyLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_INSTANCE_BUSINESS_KEY)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY))) {
            query.processDefinitionKeyLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID))) {
            query.processDefinitionId(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME))) {
            query.processDefinitionNameLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.EXECUTION_ID))) {
            query.executionId(requestParams.get(FlowableConstant.EXECUTION_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_NAME))) {
            query.taskNameLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.TASK_NAME)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_DESCRIPTION))) {
            query.taskDescriptionLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.TASK_DESCRIPTION)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_DEFINITION_KEY))) {
            query.taskDefinitionKeyLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.TASK_DEFINITION_KEY)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_ASSIGNEE))) {
            query.taskAssignee(requestParams.get(FlowableConstant.TASK_ASSIGNEE));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_OWNER))) {
            query.taskOwner(requestParams.get(FlowableConstant.TASK_OWNER));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_INVOLVED_USER))) {
            query.taskInvolvedUser(requestParams.get(FlowableConstant.TASK_INVOLVED_USER));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_PRIORITY))) {
            query.taskPriority(FlymeUtils.convertToInteger(requestParams.get(FlowableConstant.TASK_PRIORITY)));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.FINISHED))) {
            boolean isFinished = FlymeUtils.convertToBoolean(requestParams.get(FlowableConstant.FINISHED));
            if (isFinished) {
                query.finished();
            } else {
                query.unfinished();
            }
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_FINISHED))) {
            boolean isProcessFinished = FlymeUtils.convertToBoolean(requestParams.get(FlowableConstant.PROCESS_FINISHED));
            if (isProcessFinished) {
                query.processFinished();
            } else {
                query.processUnfinished();
            }
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.PARENT_TASK_ID))) {
            query.taskParentTaskId(requestParams.get(FlowableConstant.PARENT_TASK_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TENANT_ID))) {
            query.taskTenantId(requestParams.get(FlowableConstant.TENANT_ID));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CANDIDATE_USER))) {
            query.taskCandidateUser(requestParams.get(FlowableConstant.TASK_CANDIDATE_USER));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CANDIDATE_GROUP))) {
            query.taskCandidateGroup(requestParams.get(FlowableConstant.TASK_CANDIDATE_GROUP));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CANDIDATE_GROUPS))) {
            query.taskCandidateGroupIn(Arrays.asList(requestParams.get(FlowableConstant.TASK_CANDIDATE_GROUPS).split(",")));
        }
        if (ObjectUtils.isNotEmpty(requestParams.get(FlowableConstant.DUE_DATE_AFTER))) {
            query.taskDueAfter(DateUtils.convertToDate(requestParams.get(FlowableConstant.DUE_DATE_AFTER)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.DUE_DATE_BEFORE))) {
            query.taskDueBefore(DateUtils.convertToDate(requestParams.get(FlowableConstant.DUE_DATE_BEFORE)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CREATED_BEFORE))) {
            query.taskCreatedBefore(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_CREATED_BEFORE)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CREATED_AFTER))) {
            query.taskCreatedAfter(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_CREATED_AFTER)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_COMPLETED_BEFORE))) {
            query.taskCompletedBefore(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_COMPLETED_BEFORE)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_COMPLETED_AFTER))) {
            query.taskCompletedAfter(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_COMPLETED_AFTER)));
        }
        return query;
    }

    protected TaskQuery createTaskQuery(Map<String, String> requestParams) {
        TaskQuery query = taskService.createTaskQuery();
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID))) {
            query.processInstanceId(requestParams.get(FlowableConstant.PROCESS_INSTANCE_ID));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_NAME))) {
            query.taskNameLike(requestParams.get(FlowableConstant.TASK_NAME));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_INSTANCE_BUSINESS_KEY))) {
            query.processInstanceBusinessKeyLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_INSTANCE_BUSINESS_KEY)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY))) {
            query.processDefinitionKeyLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_DEFINITION_KEY)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID))) {
            query.processDefinitionId(requestParams.get(FlowableConstant.PROCESS_DEFINITION_ID));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME))) {
            query.processDefinitionNameLike(FlymeUtils.convertToLike(requestParams.get(FlowableConstant.PROCESS_DEFINITION_NAME)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.DUE_DATE_AFTER))) {
            query.taskDueAfter(DateUtils.convertToDate(requestParams.get(FlowableConstant.DUE_DATE_AFTER)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.DUE_DATE_BEFORE))) {
            query.taskDueBefore(DateUtils.convertToDate(requestParams.get(FlowableConstant.DUE_DATE_BEFORE)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CREATED_BEFORE))) {
            query.taskCreatedBefore(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_CREATED_BEFORE)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TASK_CREATED_AFTER))) {
            query.taskCreatedAfter(DateUtils.convertToDatetime(requestParams.get(FlowableConstant.TASK_CREATED_AFTER)));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.TENANT_ID))) {
            query.taskTenantId(requestParams.get(FlowableConstant.TENANT_ID));
        }
        if (FlymeUtils.isNotEmpty(requestParams.get(FlowableConstant.SUSPENDED))) {
            boolean isSuspended = FlymeUtils.convertToBoolean(requestParams.get(FlowableConstant.SUSPENDED));
            if (isSuspended) {
                query.suspended();
            } else {
                query.active();
            }
        }
        return query;
    }


    @Override
    public ResultBody list(Map<String, String> requestParams) {
        HistoricTaskInstanceQuery query = createHistoricTaskInstanceQuery(requestParams);
        return this.pageList(requestParams, query, taskSortMap, HistoricTaskInstanceQueryProperty.START, list -> createTaskResponseList(list));
    }

    @Override
    public ResultBody listTodo(Map<String, String> requestParams) {
        String userId = OpenHelper.getStrUserId();
        TaskQuery query = createTaskQuery(requestParams);
        query.or().taskCandidateOrAssigned(userId).taskOwner(userId).endOr();
        return this.pageList(requestParams, query, taskSortMap, TaskQueryProperty.CREATE_TIME, list -> createTaskResponseList(list));
    }

    public Long listTotoCount(){
        String userId = OpenHelper.getStrUserId();
        Long companyId = OpenHelper.getCompanyId();
        TaskQuery query = taskService.createTaskQuery();
        //排除发起人节点
        query.taskDefinitionKeyLike("task_%");
        //关联租户ID
        query.taskTenantId(companyId+"");
        query.or().taskCandidateOrAssigned(userId).taskOwner(userId).endOr();
        return query.count();
    }

    @Override
    public ResultBody listToRead(Map<String, String> requestParams) {
        String userId = OpenHelper.getStrUserId();
        TaskQuery query = createTaskQuery(requestParams);
        query.taskCategory(FlowableConstant.CATEGORY_TO_READ);
        query.or().taskAssignee(userId).taskOwner(userId).endOr();
        return this.pageList(requestParams, query, taskSortMap, TaskQueryProperty.CREATE_TIME, list -> createTaskResponseList(list));
    }

    @Override
    public ResultBody listDone(Map<String, String> requestParams) {
        String userId=OpenHelper.getStrUserId();
        HistoricTaskInstanceQuery query = createHistoricTaskInstanceQuery(requestParams);
        query.finished().or().taskAssignee(userId).taskOwner(userId).endOr();
        return this.pageList(requestParams, query, taskSortMap, HistoricTaskInstanceQueryProperty.START, list -> createHisTaskResponseList(list));
    }


    /**
     * 任务列表数据处理
     *
     * @param tasks
     * @return
     */
    private List<TaskResponse> createTaskResponseList(List<TaskInfo> tasks) {
        Map<String, String> processInstanceNames = new HashMap<>(16);
        Set<String> processInstanceIds = new HashSet<>();
        for (TaskInfo task : tasks) {
            if (task.getProcessInstanceId() != null) {
                processInstanceIds.add(task.getProcessInstanceId());
            }
        }
        if (processInstanceIds != null && processInstanceIds.size() > 0) {
            historyService.createHistoricProcessInstanceQuery().processInstanceIds(processInstanceIds).list().forEach(processInstance -> processInstanceNames.put(processInstance.getId(), processInstance.getName()));
        }
        List<TaskResponse> responseList = new ArrayList<>();
        for (TaskInfo task : tasks) {
            TaskResponse result = new TaskResponse(task, processInstanceNames.get(task.getProcessInstanceId()));
            result.setAssignee(getUserName(result.getAssignee()));
            if (FlymeUtils.isNotEmpty(task.getProcessInstanceId())) {
                ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
                if (FlymeUtils.isNotEmpty(processInstance)) {
                    result.setBusinessKey(processInstance.getBusinessKey());
                }
            }
            responseList.add(result);
        }
        return responseList;
    }


    /**
     * 任务列表数据处理
     *
     * @param tasks
     * @return
     */
    private List<TaskResponse> createHisTaskResponseList(List<TaskInfo> tasks) {
        Map<String, String> processInstanceNames = new HashMap<>(16);
        Set<String> processInstanceIds = new HashSet<>();
        for (TaskInfo task : tasks) {
            if (task.getProcessInstanceId() != null) {
                processInstanceIds.add(task.getProcessInstanceId());
            }
        }
        if (processInstanceIds != null && processInstanceIds.size() > 0) {
            historyService.createHistoricProcessInstanceQuery().processInstanceIds(processInstanceIds).list().forEach(processInstance -> processInstanceNames.put(processInstance.getId(), processInstance.getName()));
        }
        List<TaskResponse> responseList = new ArrayList<>();
        for (TaskInfo task : tasks) {
            TaskResponse result = new TaskResponse(task, processInstanceNames.get(task.getProcessInstanceId()));
            result.setAssignee(getUserName(result.getAssignee()));
            if (FlymeUtils.isNotEmpty(task.getProcessInstanceId())) {
                HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
                if (FlymeUtils.isNotEmpty(processInstance)) {
                    result.setBusinessKey(processInstance.getBusinessKey());
                }
            }
            responseList.add(result);
        }
        return responseList;
    }

    @Override
    public TaskResponse getTask(String taskId) {
        String userId = OpenHelper.getStrUserId();
        HistoricTaskInstance taskHis = permissionService.validateReadPermissionOnTask(taskId, userId, true, true);
        TaskResponse rep = null;
        ProcessDefinition processDefinition = null;
        String formKey = null;
        Object renderedTaskForm = null;
        HistoricTaskInstance parentTask = null;
        if (StringUtils.isNotEmpty(taskHis.getProcessDefinitionId())) {
            processDefinition = repositoryService.getProcessDefinition(taskHis.getProcessDefinitionId());
            formKey = formService.getTaskFormKey(processDefinition.getId(), taskHis.getTaskDefinitionKey());
            if (taskHis.getEndTime() == null && formKey != null && formKey.length() > 0) {
                renderedTaskForm = formService.getRenderedTaskForm(taskId);
            }
        }
        if (StringUtils.isNotEmpty(taskHis.getParentTaskId())) {
            parentTask = historyService.createHistoricTaskInstanceQuery().taskId(taskHis.getParentTaskId())
                    .singleResult();
        }
        rep = new TaskResponse(taskHis, processDefinition, parentTask, null);
        rep.setFormKey(formKey);
        rep.setRenderedTaskForm(renderedTaskForm);

        fillPermissionInformation(rep, taskHis, userId);
        // Populate the people
        populateAssignee(taskHis, rep);
        rep.setInvolvedPeople(getInvolvedUsers(taskId));

        Task task = null;
        if (taskHis.getEndTime() == null) {
            task = taskService.createTaskQuery().taskId(taskId).singleResult();
            rep.setSuspended(task.isSuspended());
            rep.setDelegationState(task.getDelegationState());
        }
        rep.setOwnerName(this.getUserName(taskHis.getOwner()));
        rep.setAssigneeName(this.getUserName(taskHis.getAssignee()));
        return rep;
    }

    @Override
    public List<TaskResponse> getSubTasks(String taskId) {
        String userId = OpenHelper.getStrUserId();
        HistoricTaskInstance parentTask = permissionService.validateReadPermissionOnTask(taskId, userId, true, true);
        List<Task> subTasks = this.taskService.getSubTasks(taskId);
        List<TaskResponse> subTasksRepresentations = new ArrayList<>(subTasks.size());
        for (Task subTask : subTasks) {
            TaskResponse representation = new TaskResponse(subTask, parentTask);
            fillPermissionInformation(representation, subTask, userId);
            populateAssignee(subTask, representation);
            representation.setInvolvedPeople(getInvolvedUsers(subTask.getId()));
            subTasksRepresentations.add(representation);
        }
        return subTasksRepresentations;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TaskResponse updateTask(TaskUpdateRequest taskUpdateRequest) {
        String userId = OpenHelper.getStrUserId();
        permissionService.validateReadPermissionOnTask(taskUpdateRequest.getId(), userId, false, false);
        Task task = getTaskNotNull(taskUpdateRequest.getId());
        task.setName(taskUpdateRequest.getName());
        task.setDescription(taskUpdateRequest.getDescription());
        task.setAssignee(taskUpdateRequest.getAssignee());
        task.setOwner(taskUpdateRequest.getOwner());
        task.setDueDate(taskUpdateRequest.getDueDate());
        task.setPriority(taskUpdateRequest.getPriority());
        task.setCategory(taskUpdateRequest.getCategory());
        taskService.saveTask(task);
        return new TaskResponse(task);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void assignTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        String assignee = taskRequest.getUserId();
        OpenUser user = OpenHelper.getUser();
        Task task = permissionService.validateAssignPermissionOnTask(taskId, user.getUserId()+"", assignee);
        this.addComment(taskId, task.getProcessInstanceId(), user.getUserId()+"", CommentTypeEnum.ZB, taskRequest.getMessage()+"#_#"+assignee+"#_#"+task.getTaskDefinitionKey());
        taskService.setAssignee(task.getId(), assignee);
        String handlerName = taskRequest.getCompleteTaskHandler();
        //完成后置处理
        if (FlymeUtils.isNotEmpty(handlerName)) {
            TaskCompleteHandler taskCompleteHandler = SpringContextHolder.getHandler(handlerName, TaskCompleteHandler.class);
            if (FlymeUtils.isNotEmpty(taskCompleteHandler)) {
                ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                        .processInstanceId(task.getProcessInstanceId()).singleResult();
                taskCompleteHandler.completeAfter(taskRequest, processInstance, task, null, user);
            }
        }
        // 暂时转办人员不作为参与者
        // String oldAssignee = task.getAssignee();
        // // If the old assignee user wasn't part of the involved users yet, make it so
        // addIdentiyLinkForUser(task, oldAssignee, IdentityLinkType.PARTICIPANT);
        // // If the current user wasn't part of the involved users yet, make it so
        // addIdentiyLinkForUser(task, userId, IdentityLinkType.PARTICIPANT);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void involveUser(String taskId, String involveUserId) {
        Task task = getTaskNotNull(taskId);
        String userId = OpenHelper.getStrUserId();
        permissionService.validateReadPermissionOnTask(task.getId(), userId, false, false);
        if (involveUserId != null && involveUserId.length() > 0) {
            taskService.addUserIdentityLink(taskId, involveUserId, IdentityLinkType.PARTICIPANT);
        } else {
            throw new FlowableException("User id is required");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeInvolvedUser(String taskId, String involveUserId) {
        Task task = getTaskNotNull(taskId);
        String userId = OpenHelper.getStrUserId();
        permissionService.validateReadPermissionOnTask(task.getId(), userId, false, false);
        if (involveUserId != null && involveUserId.length() > 0) {
            taskService.deleteUserIdentityLink(taskId, involveUserId, IdentityLinkType.PARTICIPANT);
        } else {
            throw new FlowableException("User id is required");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void claimTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        String userId = OpenHelper.getStrUserId();
        TaskInfo task = permissionService.validateReadPermissionOnTask2(taskId, userId, false, false);
        if (task.getAssignee() != null && task.getAssignee().length() > 0) {
            ApiAssert.failure("没有执行权限");
        }
        this.addComment(taskId, task.getProcessInstanceId(), userId, CommentTypeEnum.RL, taskRequest.getMessage());
        taskService.claim(taskId, userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void unclaimTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        String userId = OpenHelper.getStrUserId();
        TaskInfo task = this.getTaskNotNull(taskId);
        if (!userId.equals(task.getAssignee())) {
            ApiAssert.failure("没有执行权限");
        }
        if (FlowableConstant.INITIATOR.equals(task.getTaskDefinitionKey())) {
            ApiAssert.failure("Initiator cannot unclaim the task");
        }

        this.addComment(taskId, task.getProcessInstanceId(), userId, CommentTypeEnum.QXRL, taskRequest.getMessage());
        taskService.unclaim(taskId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addIdentiyLinkForUser(Task task, String userId, String linkType) {
        List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(task.getId());
        boolean isOldUserInvolved = false;
        for (IdentityLink identityLink : identityLinks) {
            isOldUserInvolved = userId.equals(identityLink.getUserId())
                    && (identityLink.getType().equals(IdentityLinkType.PARTICIPANT)
                    || identityLink.getType().equals(IdentityLinkType.CANDIDATE));
            if (isOldUserInvolved) {
                break;
            }
        }
        if (!isOldUserInvolved) {
            taskService.addUserIdentityLink(task.getId(), userId, linkType);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delegateTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        String delegater = taskRequest.getUserId();
        String userId = OpenHelper.getStrUserId();
        Task task = permissionService.validateDelegatePermissionOnTask(taskId, userId, delegater);
        this.addComment(taskId, task.getProcessInstanceId(), userId, CommentTypeEnum.WP, taskRequest.getMessage());
        taskService.delegateTask(task.getId(), delegater);
        // 暂时委派人员不作为参与者
        // String oldAssignee = task.getAssignee();
        // // If the old assignee user wasn't part of the involved users yet, make it so
        // addIdentiyLinkForUser(task, oldAssignee, IdentityLinkType.PARTICIPANT);
        // // If the current user wasn't part of the involved users yet, make it so
        // addIdentiyLinkForUser(task, userId, IdentityLinkType.PARTICIPANT);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void completeTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        OpenUser user = OpenHelper.getUser();
        String userId = user.getUserId().toString();
        String nickName = user.getNickName();
        String handlerName = taskRequest.getCompleteTaskHandler();
        Task task = getTaskNotNull(taskId);
        if (!permissionService.isTaskOwnerOrAssignee(userId, task)) {
            if (StringUtils.isEmpty(task.getScopeType()) && !permissionService.validateIfUserIsInitiatorAndCanCompleteTask(userId, task)) {
                ApiAssert.failure("没有执行权限");
            }
        }
        runtimeService.setVariable(task.getProcessInstanceId(),"threadUserId",user.getUserId());
        runtimeService.setVariable(task.getProcessInstanceId(),"threadNickName",user.getNickName());
        Map<String, Object> completeVariables = null;
        if (taskRequest.getFormMap() != null && !taskRequest.getFormMap().isEmpty()) {
            completeVariables = taskRequest.getFormMap();
            // 允许任务表单修改流程表单场景 begin
            // 与前端约定：流程表单变量名为 processInstanceFormData，且只有流程表单startFormKey=taskFormKey时才允许修改该变量的值，防止恶意节点修改流程表单内容
            if (completeVariables.containsKey(FlowableConstant.PROCESS_INSTANCE_FORM_DATA)) {
                String startFormKey = formService.getStartFormKey(task.getProcessDefinitionId());
                String taskFormKey = formService.getTaskFormKey(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
                boolean modifyProcessInstanceFormData = FlymeUtils.isNotEmpty(startFormKey) && FlymeUtils.isNotEmpty(taskFormKey) && startFormKey.equals(taskFormKey);
                if (!modifyProcessInstanceFormData) {
                    ApiAssert.failure("没有执行权限");
                }
            }
        }
        CommentTypeEnum commentTypeEnum= FlowableConstant.INITIATOR.equals(task.getTaskDefinitionKey()) ? CommentTypeEnum.CXTJ : CommentTypeEnum.WC;
        if(taskRequest.getApprove().equals(0)){
            commentTypeEnum=CommentTypeEnum.ZZ;
        }
        //添加审批意见(审批消息格式为内容_审批人_审批节点)
        this.addComment(taskId, task.getProcessInstanceId(), userId, commentTypeEnum, taskRequest.getMessage() + "#_#" + nickName + "#_#" + task.getTaskDefinitionKey());
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
        boolean completeTag=true;
        if (FlymeUtils.isNotEmpty(handlerName)) {
            //完成前置处理
            TaskCompleteHandler taskCompleteHandler = SpringContextHolder.getHandler(handlerName, TaskCompleteHandler.class);
            if (FlymeUtils.isNotEmpty(taskCompleteHandler)) {
                //任务完成前处理
                completeTag=taskCompleteHandler.completeBefore(taskRequest, processInstance, task, completeVariables, user);
            }
        }

        // 处理抄送
        if (FlymeUtils.isNotEmpty(taskRequest.getCcToVos())) {
            managementService.executeCommand(new AddCcIdentityLinkCmd(task.getProcessInstanceId(), task.getId(), userId, taskRequest.getCcToVos()));
        }

        //允许执行完成动作
        if(completeTag) {
            if (task.getAssignee() == null || !task.getAssignee().equals(userId)) {
                taskService.setAssignee(taskId, userId);
            }
            // 判断是否是协办完成还是正常流转
            if (permissionService.isTaskPending(task)) {
                taskService.resolveTask(taskId, completeVariables);
                // 如果当前执行人是任务所有人，直接完成任务
                if (userId.equals(task.getOwner())) {
                    taskService.complete(taskId, completeVariables);
                }
            } else {
                taskService.complete(taskId, completeVariables);
            }
        }
        //完成后置处理
        if (FlymeUtils.isNotEmpty(handlerName)) {
            TaskCompleteHandler taskCompleteHandler = SpringContextHolder.getHandler(handlerName, TaskCompleteHandler.class);
            if (FlymeUtils.isNotEmpty(taskCompleteHandler)) {
                taskCompleteHandler.completeAfter(taskRequest, processInstance, task, completeVariables, user);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTask(String taskId) {
        HistoricTaskInstance task = this.getHistoricTaskInstanceNotNull(taskId);
        if (task.getEndTime() == null) {
            throw new FlowableException("任务审批中,禁止删除");
        }
        historyService.deleteHistoricTaskInstance(task.getId());
    }

    /**
     * 终止流程
     *
     * @param taskRequest
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void stopProcessInstance(TaskRequest taskRequest) {
       String userId=OpenHelper.getStrUserId();
       stopProcessInstance(taskRequest.getTaskId(),taskRequest.getMessage(),taskRequest.getApprove(),userId);
    }

    @Override
    public void stopProcessInstance(String taskId,String message,Integer approve, String userId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) {
            throw new FlowableObjectNotFoundException("Task with id: " + taskId + " does not exist");
        }
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId()).singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processInstance.getProcessDefinitionId());
        if (bpmnModel != null) {
            Process process = bpmnModel.getMainProcess();
            List<EndEvent> endNodes = process.findFlowElementsOfType(EndEvent.class, false);
            if (endNodes != null && endNodes.size() > 0) {
                if(FlymeUtils.isNotEmpty(message)) {
                    this.addComment(taskId, processInstance.getProcessInstanceId(), userId, CommentTypeEnum.ZZ, message);
                }
                //设置标记
                runtimeService.setVariable(processInstance.getProcessInstanceId(), "approve", approve);
                String endId = endNodes.get(0).getId();
                List<Execution> executions = runtimeService.createExecutionQuery().parentId(processInstance.getProcessInstanceId()).list();
                List<String> executionIds = new ArrayList<>();
                executions.forEach(execution -> executionIds.add(execution.getId()));
                runtimeService.createChangeActivityStateBuilder().moveExecutionsToSingleActivityId(executionIds, endId).changeState();
            }
        }
    }

    /**
     * 查询可退回节点
     *
     * @param taskId
     * @return
     */
    @Override
    public List<FlowNodeResponse> getBackNodes(String taskId) {
        String userId = OpenHelper.getStrUserId();
        TaskEntity taskEntity = (TaskEntity) permissionService.validateExcutePermissionOnTask(taskId, userId);
        String processInstanceId = taskEntity.getProcessInstanceId();
        String currActId = taskEntity.getTaskDefinitionKey();
        String processDefinitionId = taskEntity.getProcessDefinitionId();
        Process process = repositoryService.getBpmnModel(processDefinitionId).getMainProcess();
        FlowNode currentFlowElement = (FlowNode) process.getFlowElement(currActId, true);
        List<ActivityInstance> activitys = runtimeService.createActivityInstanceQuery().processInstanceId(processInstanceId).finished().orderByActivityInstanceStartTime().asc().list();
        List<String> activityIds = activitys.stream()
                .filter(activity -> activity.getActivityType().equals(BpmnXMLConstants.ELEMENT_TASK_USER))
                .filter(activity -> !activity.getActivityId().equals(currActId)).map(ActivityInstance::getActivityId)
                .distinct().collect(Collectors.toList());
        List<FlowNodeResponse> result = new ArrayList<>();
        for (String activityId : activityIds) {
            FlowNode toBackFlowElement = (FlowNode) process.getFlowElement(activityId, true);
            if (FlowableUtils.isReachable(process, toBackFlowElement, currentFlowElement)) {
                FlowNodeResponse vo = new FlowNodeResponse();
                vo.setNodeId(activityId);
                vo.setNodeName(toBackFlowElement.getName());
                result.add(vo);
            }
        }
        return result;
    }

    /**
     * 退回流程(指定节点)
     *
     * @param taskRequest
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void backTask(TaskRequest taskRequest) {
        String taskId = taskRequest.getTaskId();
        String userId = OpenHelper.getStrUserId();
        Task task = permissionService.validateExcutePermissionOnTask(taskId, userId);
        String backSysMessage = taskRequest.getMessage();
        this.addComment(taskId, task.getProcessInstanceId(), userId, CommentTypeEnum.TH, backSysMessage);
        String targetRealActivityId = managementService.executeCommand(new BackUserTaskCmd(runtimeService, taskRequest.getTaskId(), taskRequest.getActivityId()));
        // 退回发起者处理,退回到发起者,默认设置任务执行人为发起者
        if (FlowableConstant.INITIATOR.equals(targetRealActivityId)) {
            String initiator = runtimeService.createProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId()).singleResult().getStartUserId();
            List<Task> newTasks = taskService.createTaskQuery().processInstanceId(task.getProcessInstanceId()).list();
            for (Task newTask : newTasks) {
                // 约定：发起者节点为 __initiator__
                if (FlowableConstant.INITIATOR.equals(newTask.getTaskDefinitionKey())) {
                    if (FlymeUtils.isEmpty(newTask.getAssignee())) {
                        taskService.setAssignee(newTask.getId(), initiator);
                    }
                }
            }
        }
    }


    private void fillPermissionInformation(TaskResponse taskResponse, TaskInfo task, String userId) {
        verifyProcessInstanceStartUser(taskResponse, task);
        List<HistoricIdentityLink> taskIdentityLinks = historyService.getHistoricIdentityLinksForTask(task.getId());
        verifyCandidateGroups(taskResponse, userId, taskIdentityLinks);
        verifyCandidateUsers(taskResponse, userId, taskIdentityLinks);
    }

    private void verifyProcessInstanceStartUser(TaskResponse taskResponse, TaskInfo task) {
        if (task.getProcessInstanceId() != null) {
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId()).singleResult();
            if (historicProcessInstance != null && StringUtils.isNotEmpty(historicProcessInstance.getStartUserId())) {
                taskResponse.setProcessInstanceStartUserId(historicProcessInstance.getStartUserId());
                BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
                FlowElement flowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
                if (flowElement instanceof UserTask) {
                    UserTask userTask = (UserTask) flowElement;
                    List<ExtensionElement> extensionElements = userTask.getExtensionElements()
                            .get("initiator-can-complete");
                    if (CollectionUtils.isNotEmpty(extensionElements)) {
                        String value = extensionElements.get(0).getElementText();
                        if (StringUtils.isNotEmpty(value)) {
                            taskResponse.setInitiatorCanCompleteTask(value);
                        }
                    }
                }
            }
        }
    }

    private void verifyCandidateGroups(TaskResponse taskResponse, String userId,
                                       List<HistoricIdentityLink> taskIdentityLinks) {
        List<Group> userGroups = identityService.createGroupQuery().groupMember(userId).list();
        taskResponse.setMemberOfCandidateGroup(
                String.valueOf(userGroupsMatchTaskCandidateGroups(userGroups, taskIdentityLinks)));
    }

    private boolean userGroupsMatchTaskCandidateGroups(List<Group> userGroups,
                                                       List<HistoricIdentityLink> taskIdentityLinks) {
        for (Group group : userGroups) {
            for (HistoricIdentityLink identityLink : taskIdentityLinks) {
                if (identityLink.getGroupId() != null && identityLink.getType().equals(IdentityLinkType.CANDIDATE)
                        && group.getId().equals(identityLink.getGroupId())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void verifyCandidateUsers(TaskResponse taskResponse, String userId,
                                      List<HistoricIdentityLink> taskIdentityLinks) {
        taskResponse.setMemberOfCandidateUsers(
                String.valueOf(currentUserMatchesTaskCandidateUsers(userId, taskIdentityLinks)));
    }

    private boolean currentUserMatchesTaskCandidateUsers(String userId, List<HistoricIdentityLink> taskIdentityLinks) {
        for (HistoricIdentityLink identityLink : taskIdentityLinks) {
            if (identityLink.getUserId() != null && identityLink.getType().equals(IdentityLinkType.CANDIDATE)
                    && identityLink.getUserId().equals(userId)) {
                return true;
            }
        }
        return false;
    }

    private String getUserName(String userId) {
        if (FlymeUtils.isEmpty(userId)) {
            return null;
        }
        User user = identityService.createUserQuery().userId(userId).singleResult();
        if (user != null) {
            return user.getDisplayName();
        }
        return null;
    }

    private List<String> getInvolvedUsers(String taskId) {
        List<HistoricIdentityLink> idLinks = historyService.getHistoricIdentityLinksForTask(taskId);
        List<String> result = new ArrayList<>(idLinks.size());

        for (HistoricIdentityLink link : idLinks) {
            // 仅包括用户和非受让人链接
            if (link.getUserId() != null && !IdentityLinkType.ASSIGNEE.equals(link.getType())) {
                result.add(link.getUserId());
            }
        }
        return result;
    }

    private void populateAssignee(TaskInfo task, TaskResponse rep) {
        if (task.getAssignee() != null) {
            rep.setAssignee(task.getAssignee());
        }
    }

    @Override
    public Task getTaskNotNull(String taskId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) {
            throw new FlowableObjectNotFoundException("Task with id: " + taskId + " does not exist");
        }
        return task;
    }

    @Override
    public HistoricTaskInstance getHistoricTaskInstanceNotNull(String taskId) {
        HistoricTaskInstance task = historyService.createHistoricTaskInstanceQuery().taskId(taskId).singleResult();
        if (task == null) {
            throw new FlowableObjectNotFoundException("Task with id: " + taskId + " does not exist");
        }
        return task;
    }

    @Override
    public void addComment(String taskId, String processInstanceId, String userId, CommentTypeEnum type, String message) {
        Authentication.setAuthenticatedUserId(userId);
        type = type == null ? CommentTypeEnum.SP : type;
        message = (message == null || message.length() == 0) ? type.getName() : message;
        taskService.addComment(taskId, processInstanceId, type.toString(), message);
    }

    @Override
    public List<Comment> getComments(String taskId, String processInstanceId, String type, String userId) {
        List<Comment> comments = null;
        if (type == null || type.length() == 0) {
            // 以taskId为优先
            if (taskId != null && taskId.length() > 0) {
                comments = taskService.getTaskComments(taskId);
            } else if (processInstanceId != null && processInstanceId.length() > 0) {
                comments = taskService.getProcessInstanceComments(processInstanceId);
            } else {
                throw new FlowableIllegalArgumentException("taskId processInstanceId type are all empty");
            }
        } else {
            // 以taskId为优先
            if (taskId != null && taskId.length() > 0) {
                comments = taskService.getTaskComments(taskId, type);
            } else if (processInstanceId != null && processInstanceId.length() > 0) {
                comments = taskService.getProcessInstanceComments(processInstanceId, type);
            } else {
                comments = taskService.getCommentsByType(type);
            }
        }
        if (userId != null && userId.length() > 0 && comments != null && comments.size() > 0) {
            comments = comments.stream().filter(comment -> userId.equals(comment.getUserId()))
                    .collect(Collectors.toList());
        }
        return comments;
    }

    private void validateIdentityLinkArguments(String identityId, String identityType) {
        if (identityId == null || identityId.length() == 0) {
            throw new FlowableIllegalArgumentException("identityId is null");
        }
        if (!FlowableConstant.IDENTITY_GROUP.equals(identityType)
                && !FlowableConstant.IDENTITY_USER.equals(identityType)) {
            throw new FlowableIllegalArgumentException("type must be group or user");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveTaskIdentityLink(IdentityRequest taskIdentityRequest) {
        Task task = getTaskNotNull(taskIdentityRequest.getTaskId());
        validateIdentityLinkArguments(taskIdentityRequest.getIdentityId(), taskIdentityRequest.getIdentityType());
        if (FlowableConstant.IDENTITY_GROUP.equals(taskIdentityRequest.getIdentityType())) {
            taskService.addGroupIdentityLink(task.getId(), taskIdentityRequest.getIdentityId(),
                    IdentityLinkType.CANDIDATE);
        } else if (FlowableConstant.IDENTITY_USER.equals(taskIdentityRequest.getIdentityType())) {
            taskService.addUserIdentityLink(task.getId(), taskIdentityRequest.getIdentityId(),
                    IdentityLinkType.CANDIDATE);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTaskIdentityLink(String taskId, String identityId, String identityType) {
        Task task = getTaskNotNull(taskId);
        validateIdentityLinkArguments(identityId, identityType);
        if (FlowableConstant.IDENTITY_GROUP.equals(identityType)) {
            taskService.deleteGroupIdentityLink(task.getId(), identityId, IdentityLinkType.CANDIDATE);
        } else if (FlowableConstant.IDENTITY_USER.equals(identityType)) {
            taskService.deleteUserIdentityLink(task.getId(), identityId, IdentityLinkType.CANDIDATE);
        }
    }

    /**
     * @Author cf
     * @Description 驳回
     * proInstanceId  需要驳回的流程实例id(当前发起节点的流程实例id)
     * currTaskKeys   驳回发起的当前节点key 为  act_ru_task 中TASK_DEF_KEY_ 字段的值
     * targetKey  目标节点的key  为act_hi_taskinst 中 TASK_DEF_KEY_
     **/
    public void rollback(String proInstanceId, List<String> currTaskKeys, String targetKey) {
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId(proInstanceId)
                .moveActivityIdsToSingleActivityId(currTaskKeys, targetKey)
                .changeState();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void readTask(TaskRequest taskRequest) {
        String[] taskIds = taskRequest.getTaskIds();
        if (taskIds == null || taskIds.length == 0) {
            throw new FlowableException("taskIds is null or empty");
        }
        String userId = OpenHelper.getStrUserId();
        for (String taskId : taskIds) {
            managementService.executeCommand(new CompleteTaskReadCmd(taskId, userId));
        }
    }

    @Override
    public ResultBody getTaskData(String taskId, String handlerName) {
        //流程启动业务拦截器
        TaskCompleteHandler taskCompleteHandler = SpringContextHolder.getHandler(handlerName, TaskCompleteHandler.class);
        //任务节点
        Task task = permissionService.validateReadPermissionOnTask2(taskId, OpenHelper.getStrUserId(), true, true);
        //流程发起表单key
        String startFormKey = formService.getStartFormKey(task.getProcessDefinitionId());
        //任务表单key
        String taskFormKey = formService.getTaskFormKey(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
        //发起表单json
        Object startFormJson = formService.getRenderedStartForm(task.getProcessDefinitionId());
        //任务表单json
        Object taskFormJson = formService.getRenderedTaskForm(taskId);
        //流程变量
        Map<String, Object> variables = runtimeService.getVariables(task.getProcessInstanceId());
        //流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
        Map<String, Object> ret = new HashMap(7);
        ret.put("startUserId", processInstance.getStartUserId());
        ret.put("startFormKey", startFormKey);
        ret.put("taskFormKey", taskFormKey);
        ret.put("startFormJson", startFormJson);
        ret.put("taskFormJson", taskFormJson);
        ret.put("variables", variables);
        ret.put(FlowableConstant.BUSINESS_KEY, processInstance.getBusinessKey());
        // 当前任务是发起者
        if (FlowableConstant.INITIATOR.equals(task.getTaskDefinitionKey())) {
            ret.put("isInitiator", true);
        }
        if (FlymeUtils.isNotEmpty(taskCompleteHandler)) {
            Map<String, Object> taskFormData = taskCompleteHandler.initTaskData(processInstance, task, variables);
            ret.put("taskFormData", taskFormData);
            //设置表单提交完成事件
            ret.put("completeTaskHandler", handlerName);
        }
        return ResultBody.ok(ret);
    }


}