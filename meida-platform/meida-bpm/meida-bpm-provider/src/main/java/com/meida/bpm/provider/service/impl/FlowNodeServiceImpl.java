package com.meida.bpm.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.provider.common.CommentTypeEnum;
import com.meida.bpm.provider.custom.entity.ChildAttr;
import com.meida.bpm.provider.custom.model.ApproverModel;
import com.meida.bpm.provider.mapper.FlowNodeMapper;
import com.meida.bpm.provider.model.AppRoverUserVo;
import com.meida.bpm.provider.service.FlowNodeService;
import com.meida.bpm.provider.service.ProcessInstanceService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.JsonUtils;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.admin.provider.service.BaseUserService;
import org.apache.commons.lang.StringUtils;
import org.flowable.bpmn.constants.BpmnXMLConstants;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.task.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 流程节点表接口实现类
 *
 * @author flyme
 * @date 2024-12-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowNodeServiceImpl extends BaseServiceImpl<FlowNodeMapper, FlowProcessNode> implements FlowNodeService {

    @Autowired
    private ProcessInstanceService processInstanceService;
    @Autowired
    private BaseUserService baseUserService;

    @Autowired
    private BaseRoleService baseRoleService;

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected HistoryService historyService;
    @Autowired
    protected RuntimeService runtimeService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, FlowProcessNode node, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<FlowProcessNode> cq, FlowProcessNode node, EntityMap requestMap) {
        cq.orderByDesc("node.createTime");
        return ResultBody.ok();
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public void saveProcessNode(Long processId, List<FlowProcessNode> processNodes) {
        //已存在的节点
        List<Long> updateIds = new ArrayList<>();
        //原节点
        List<Long> nodesIds = selectProcessNodeIds(processId);
        //添加流程节点
        if (ObjectUtils.isNotEmpty(processNodes)) {
            for (FlowProcessNode flowProcessNode : processNodes) {
                String nodeCode = flowProcessNode.getProcessNodeCode();
                Long flowNodeId = flowProcessNode.getFlowNodeId();
                FlowProcessNode check = queryByNodeCodeAndProcessId(processId, nodeCode);
                if (check == null) {
                    flowProcessNode.setProcessId(processId);
                    save(flowProcessNode);
                } else {
                    flowProcessNode.setFlowNodeId(check.getFlowNodeId());
                    flowProcessNode.setProcessId(processId);
                    updateIds.add(check.getFlowNodeId());
                    updateById(flowProcessNode);
                }
            }
        }
        //删除已删除的节点
        if (FlymeUtils.isNotEmpty(processId)) {
            List<Long> delIds = nodesIds.stream().filter(e -> !updateIds.contains(e)).collect(Collectors.toList());
            if (ObjectUtils.isNotEmpty(delIds)) {
                removeByIds(delIds);
            }
        }
    }

    /**
     * 查询流程节点ID
     *
     * @param processId
     * @return
     */
    public List<Long> selectProcessNodeIds(Long processId) {
        QueryWrapper<FlowProcessNode> qw = new QueryWrapper();
        qw.select("flowNodeId");
        qw.lambda().eq(true, FlowProcessNode::getProcessId, processId);
        List<Long> list = listObjs(qw, e -> Long.parseLong(e.toString()));
        return list;
    }

    /**
     * 查询流程节点ID
     *
     * @param processId
     * @return
     */
    public FlowProcessNode queryByNodeCodeAndProcessId(Long processId, String nodeCode) {
        QueryWrapper<FlowProcessNode> qw = new QueryWrapper();
        qw.select("*");
        qw.lambda().eq(true, FlowProcessNode::getProcessId, processId).eq(true, FlowProcessNode::getProcessNodeCode, nodeCode);
        return getOne(qw);
    }
    @Override
    public List<EntityMap> selectProcessNode(Long processId, String processInstanceId) {
        CriteriaQuery cq = new CriteriaQuery(FlowProcessNode.class);
        cq.eq(true, "processId", processId);
        cq.orderByAsc("sortNo");
        List<EntityMap> list = selectEntityMap(cq);
        HistoricProcessInstance historicProcessInstance = processInstanceService.getHistoricProcessInstanceById(processInstanceId);

        for (EntityMap entityMap : list) {
            String processNodeCode = entityMap.get("processNodeCode");
            String approveModel = entityMap.get("approveModel");
            if (processNodeCode.equals("start")) {
                if (FlymeUtils.isNotEmpty(historicProcessInstance)) {
                    String startUserId = historicProcessInstance.getStartUserId();
                    BaseUser baseUser = baseUserService.getById(startUserId);
                    if (FlymeUtils.isNotEmpty(baseUser)) {
                        List<EntityMap> userList = new ArrayList<>();
                        EntityMap userNap = new EntityMap();
                        userNap.put("assigneeName", baseUser.getNickName());
                        userNap.put("avatar", baseUser.getAvatar());
                        userNap.put("processNodeState", 1);
                        userNap.put("startTime", historicProcessInstance.getStartTime());
                        userList.add(userNap);
                        entityMap.put("approveUserList", userList);
                    }
                }
                entityMap.put("processNodeName", "发起人");
            }
            entityMap.put("processNodeState", 0);
            if (FlymeUtils.isNotEmpty(approveModel)) {
                List<Comment> commentList = taskService.getProcessInstanceComments(processInstanceId, CommentTypeEnum.WC.toString());
                ApproverModel approverModel = JsonUtils.jsonToBean(approveModel, ApproverModel.class);
                String approveType = approverModel.getApproverType();
                //转办记录
                List<Comment> zbCommentList = taskService.getProcessInstanceComments(processInstanceId, CommentTypeEnum.ZB.toString());
                if (approveType.equals("candidateUsers")) {
                    //用户ID
                    String[] approveIds = approverModel.getApproverIds();
                    List<String> approveIdList = new ArrayList<>(Arrays.asList(approveIds));
                    List<String> zbUserIds = new ArrayList<>();
                    if (FlymeUtils.isNotEmpty(zbCommentList)) {
                        for (Comment comment : zbCommentList) {
                            String message = comment.getFullMessage();
                            //原审批人
                            String userId = comment.getUserId();
                            String[] msg = message.split("#_#");
                            //转办接收人
                            String assignee = msg[1];
                            String taskKey = msg[2];
                            if (taskKey.equals(processNodeCode)) {
                                int index = approveIdList.indexOf(userId);
                                if (index > -1) {
                                    approveIdList.add(index + 1, assignee);
                                    zbUserIds.add(assignee);
                                }
                            }
                        }
                    }
                    if (FlymeUtils.isNotEmpty(approveIdList)) {
                        if (FlymeUtils.isNotEmpty(zbCommentList)) {
                            commentList.addAll(zbCommentList);
                        }
                        List<BaseUser> approveUsers = baseUserService.listByUserIds(approveIdList);
                        if (FlymeUtils.isNotEmpty(approveUsers)) {
                            List<EntityMap> userList = new ArrayList<>();
                            for (BaseUser approveUser : approveUsers) {
                                EntityMap user = new EntityMap();
                                user.put("assigneeName", approveUser.getNickName());
                                user.put("avatar", approveUser.getAvatar());
                                user.put("userId", approveUser.getUserId());
                                user.put("processNodeState", 0);
                                if (zbUserIds.indexOf(approveUser.getUserId().toString()) > -1) {
                                    user.put("isZb", 1);
                                } else {
                                    user.put("isZb", 0);
                                }
                                getNodeComment(commentList, user, processNodeCode, processInstanceId);
                                userList.add(user);
                            }

                            //创建id到索引的映射
                            Map<String, Integer> userIdMap = new HashMap<>();
                            for (int i = 0; i < approveIdList.size(); i++) {
                                userIdMap.put(approveIdList.get(i), i);
                            }
                            userList.sort((p1, p2) -> {
                                Integer index1 = userIdMap.getOrDefault(p1.getLong("userId").toString(), Integer.MIN_VALUE);
                                Integer index2 = userIdMap.getOrDefault(p2.getLong("userId").toString(), Integer.MIN_VALUE);
                                return index1.compareTo(index2);
                            });
                            entityMap.put("approveUserList", userList);
                        }
                    }
                }
                if (approveType.equals("candidateGroups")) {
                    if (FlymeUtils.isNotEmpty(zbCommentList)) {
                        commentList.addAll(zbCommentList);
                    }
                    Collections.sort(commentList, Comparator.comparing(Comment::getTime));
                    if (FlymeUtils.isNotEmpty(commentList)) {
                        for (Comment comment : commentList) {
                            String message = comment.getFullMessage();
                            String type=comment.getType();
                            //审批人
                            String userId = comment.getUserId();
                            String[] msg = message.split("#_#");
                            //转办接收人
                            String assignee = msg[1];
                            String taskKey = msg[2];
                            List<EntityMap> userList=new ArrayList<>();
                            if (taskKey.equals(processNodeCode)) {
                                BaseUser baseUser = baseUserService.getById(userId);
                                EntityMap user=new EntityMap();
                                user.put("assigneeName", baseUser.getNickName());
                                user.put("avatar", baseUser.getAvatar());
                                user.put("userId", baseUser.getUserId());
                                user.put("processNodeState", 1);
                                user.put("content", msg[0]);
                                user.put("startTime", comment.getTime());
                                user.put("isZb",0);
                                if(type.equals("ZB")){
                                    user.put("isZb",1);
                                }
                                userList.add(user);
                            }
                            entityMap.put("approveUserList", userList);
                        }
                    }
                }

            }

        }
        //设置流程节点状态
        setFlowNodeState(historicProcessInstance, processInstanceId, list);
        
        return list;
    }

    private void getNodeComment(List<Comment> commentList, EntityMap userMap, String processNodeCode, String processInstanceId) {
        String nodeUserId = userMap.get("userId").toString();
        if (FlymeUtils.isNotEmpty(commentList)) {
            for (Comment comment : commentList) {
                String userId = comment.getUserId();
                if (nodeUserId.equals(userId)) {
                    String message = comment.getFullMessage();
                    if (FlymeUtils.isNotEmpty(message) && message.indexOf("#_#") != -1) {
                        String[] msg = message.split("#_#");
                        String taskKey = msg[2];
                        String content = msg[0];
                        if (taskKey.equals(processNodeCode)) {
                            //已办理
                            userMap.put("processNodeState", 1);
                            userMap.put("content", content);
                            userMap.put("startTime", comment.getTime());
                        }
                    }
                }
            }
        }
    }


    private List<EntityMap> setFlowNodeState(HistoricProcessInstance historicProcessInstance, String processInstanceId, List<EntityMap> processNodeList) {
        List<String> highLightedNow = new ArrayList<>();
        List<String> highLightedActivities = new ArrayList<>();
        List<HistoricActivityInstance> allHistoricActivityIntances = historyService
                .createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();
        allHistoricActivityIntances.forEach(historicActivityInstance -> {
            if (!BpmnXMLConstants.ELEMENT_SEQUENCE_FLOW.equals(historicActivityInstance.getActivityType())) {
                highLightedActivities.add(historicActivityInstance.getActivityId());
            }
        });
        List<String> runningActivitiIdList = null;
        // 流程已结束
        if (historicProcessInstance != null && historicProcessInstance.getEndTime() != null) {
            runningActivitiIdList = Arrays.asList();
            processNodeList.removeIf(item->!highLightedActivities.contains(item.get("processNodeCode")));

        } else {
            runningActivitiIdList = runtimeService.getActiveActivityIds(processInstanceId);
        }
        for (EntityMap entityMap : processNodeList) {
            String nodeKey = entityMap.get("processNodeCode");
            if (highLightedActivities.indexOf(nodeKey) > -1) {
                entityMap.put("processNodeState", 2);
            }
            if (runningActivitiIdList.indexOf(nodeKey) > -1) {
                entityMap.put("processNodeState", 1);
            }
        }
        return processNodeList;
    }

    @Override
    public AppRoverUserVo getAppRoverUser(String taskId, Long processId, String processNodeCode) {
        AppRoverUserVo appRoverUserVo=new AppRoverUserVo();
        CriteriaQuery cq = new CriteriaQuery(FlowProcessNode.class);
        cq.eq(true, "processId", processId);
        cq.eq(true, "processNodeCode", processNodeCode);
        EntityMap map = findOne(cq);
        if (FlymeUtils.isNotEmpty(map)) {
            String approveModel = map.get("approveModel");
            String nodeConfigJson = map.get("nodeConfigJson");
            Boolean isSequential=false;
            if (FlymeUtils.isNotEmpty(approveModel)) {
                ChildAttr childAttr=JsonUtils.jsonToBean(nodeConfigJson, ChildAttr.class);
                if(FlymeUtils.isNotEmpty(childAttr.getIsSequential())){
                    isSequential=childAttr.getIsSequential();
                }
            }
            appRoverUserVo.setIsSequential(isSequential);

            if (FlymeUtils.isNotEmpty(approveModel)) {
                ApproverModel approverModel = JsonUtils.jsonToBean(approveModel, ApproverModel.class);
                String approverType = approverModel.getApproverType();
                appRoverUserVo.setApproverType(approverType);
                if (approverType.equals("candidateUsers")) {
                    String[] approverIds = approverModel.getApproverIds();
                    if (FlymeUtils.isNotEmpty(approverIds)) {
                        if(isSequential){
                            appRoverUserVo.setUserIds(new ArrayList<>(Arrays.asList(approverIds[0])));
                            return appRoverUserVo;
                        }else{
                            appRoverUserVo.setUserIds(new ArrayList<>(Arrays.asList(approverIds)));
                            return appRoverUserVo;
                        }
                    }
                }
                if (approverType.equals("candidateGroups")) {


                    String[] roleIds = approverModel.getRoleIds();
                    List<String> userIds = baseRoleService.selectUserIdByRoleCode(roleIds);
                    if (FlymeUtils.isNotEmpty(userIds)) {
                        if(isSequential) {
                            appRoverUserVo.setUserIds(new ArrayList<>(Arrays.asList(userIds.get(0))));
                            return appRoverUserVo;
                        }else{
                            appRoverUserVo.setUserIds(userIds);
                            return appRoverUserVo;
                        }
                    }
                }
            }
        }
        return null;
    }
}
