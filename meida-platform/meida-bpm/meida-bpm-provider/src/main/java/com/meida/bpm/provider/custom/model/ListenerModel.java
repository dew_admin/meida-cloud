package com.meida.bpm.provider.custom.model;

import lombok.Data;

/**
 * 监听对象模型
 *
 * @author zyf
 */
@Data
public class ListenerModel {
    /**
     * 监听ID
     */
    private String id;
    /**
     * 事件类型
     */
    private String eventType;
    /**
     * 监听类型
     */
    private String listenerType;
    /**
     * 监听值
     */
    private String value;
}
