package com.meida.bpm.provider.controller;


import cn.hutool.core.map.MapUtil;
import com.meida.bpm.provider.common.BaseFlowableController;
import com.meida.bpm.provider.model.FlowNodeResponse;
import com.meida.bpm.provider.model.TaskRequest;
import com.meida.bpm.provider.model.TaskResponse;
import com.meida.bpm.provider.model.TaskUpdateRequest;
import com.meida.bpm.provider.service.FlowableTaskService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 任务管理
 *
 * @author zyf
 */
@RestController
@RequestMapping("/bpm/task")
public class FlowableTaskController extends BaseFlowableController {
    @Autowired
    protected FlowableTaskService flowableTaskService;

    /**
     * 任务列表
     *
     * @param requestParams
     * @return
     */
    @GetMapping(value = "/list")
    public ResultBody list(@RequestParam Map<String, String> requestParams) {
        return flowableTaskService.list(requestParams);
    }

    /**
     * 已办任务
     *
     * @param requestParams
     * @return
     */
    @GetMapping(value = "/listDone")
    public ResultBody listDone(@RequestParam Map<String, String> requestParams) {
        return flowableTaskService.listDone(requestParams);
    }

    /**
     * 待办任务
     *
     * @param requestParams
     * @return
     */
    @GetMapping(value = "/listTodo")
    public ResultBody listTodo(@RequestParam Map<String, String> requestParams) {
        return flowableTaskService.listTodo(requestParams);
    }

    /**
     * 我的待阅
     *
     * @param requestParams
     * @return
     */
    @GetMapping(value = "/listToRead")
    public ResultBody listToRead(@RequestParam Map<String, String> requestParams) {
        return flowableTaskService.listToRead(requestParams);
    }

    /**
     * 查询任务明细
     *
     * @param taskId
     * @return
     */
    @GetMapping(value = "/queryById")
    public ResultBody queryById(@RequestParam String taskId) {
        TaskResponse task = flowableTaskService.getTask(taskId);
        return ResultBody.ok(task);
    }

    /**
     * 修改任务
     *
     * @param taskUpdateRequest
     * @return
     */
    @PutMapping(value = "/update")
    public ResultBody update(@RequestBody TaskUpdateRequest taskUpdateRequest) {
        TaskResponse task = flowableTaskService.updateTask(taskUpdateRequest);
        return ResultBody.ok(task);
    }

    /**
     * 删除任务
     *
     * @param taskId
     * @return
     */
    @DeleteMapping(value = "/delete")
    public ResultBody delete(@RequestParam String taskId) {
        flowableTaskService.deleteTask(taskId);
        return ResultBody.ok();
    }

    /**
     * 转办任务
     *
     * @param taskRequest
     * @return
     */
    @PostMapping(value = "/assign")
    public ResultBody assign(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.assignTask(taskRequest);
        return ResultBody.ok();
    }

    /**
     * 委派任务
     *
     * @param taskRequest
     * @return
     */
    @PutMapping(value = "/delegate")
    public ResultBody delegate(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.delegateTask(taskRequest);
        return ResultBody.ok();
    }

    /**
     * 签收任务
     *
     * @param taskRequest
     * @return
     */
    @PutMapping(value = "/claim")
    public ResultBody claim(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.claimTask(taskRequest);
        return ResultBody.ok();
    }

    /**
     * 取消签收任务
     *
     * @param taskRequest
     * @return
     */
    @PutMapping(value = "/unclaim")
    public ResultBody unclaim(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.unclaimTask(taskRequest);
        return ResultBody.ok();
    }

    /**
     * 完成任务
     *
     * @param taskRequest
     * @return
     */
    @PostMapping(value = "/complete")
    public ResultBody complete(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.completeTask(taskRequest);
        return ResultBody.ok();
    }

    /**
     * 结束流程实例
     *
     * @param taskRequest
     * @return
     */
    @PutMapping(value = "/stopProcessInstance")
    public ResultBody stopProcessInstance(@RequestBody TaskRequest taskRequest) {
        flowableTaskService.stopProcessInstance(taskRequest);
        return ResultBody.ok();
    }

    @GetMapping(value = "/renderedTaskForm")
    public ResultBody renderedTaskForm(@RequestParam String taskId) {
        permissionService.validateReadPermissionOnTask2(taskId, OpenHelper.getStrUserId(), true, true);
        Object renderedTaskForm = formService.getRenderedTaskForm(taskId);
        return ResultBody.ok(renderedTaskForm);
    }

    /**
     * 查询任务办理数据
     *
     * @param taskId
     * @param handlerName
     * @return
     */
    @GetMapping(value = "/getTaskData")
    public ResultBody getTaskData(@RequestParam String taskId,@RequestParam String handlerName) {
       return flowableTaskService.getTaskData(taskId,handlerName);
    }


    /**
     * 返回节点
     *
     * @param taskId
     * @return
     */
    @GetMapping(value = "/getBackNodes")
    public ResultBody getBackNodes(@RequestParam String taskId) {
        List<FlowNodeResponse> datas = flowableTaskService.getBackNodes(taskId);
        return ResultBody.ok(datas);
    }


    /**
     * 退回任务
     */
    @PostMapping(value = "/backTask")
    public ResultBody back(TaskRequest taskRequest) {
        flowableTaskService.backTask(taskRequest);
        return ResultBody.ok();
    }



}
