package com.meida.bpm.provider.custom.model;

import lombok.Data;

@Data
public class CustomProcessNode {

    private String id;
    private String formId;
    private String processId;
    private String processNodeCode;
    private String processNodeName;
    private String modelAndView;
    private String modelAndViewMobile;
    private Integer nodeTimeout;
    private String nodeBpmStatus;
    private String formEditStatus;
    private String ccStatus;
    private String selnextUserStatus;
    private String msgStatus;
    private String nodeConfigJson;
}
