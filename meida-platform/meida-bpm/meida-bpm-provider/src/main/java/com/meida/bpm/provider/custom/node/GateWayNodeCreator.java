/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.node;

import com.meida.bpm.provider.custom.entity.ChildNode;
import com.meida.bpm.provider.custom.entity.ConditionNode;
import org.flowable.bpmn.model.ExclusiveGateway;
import org.flowable.bpmn.model.InclusiveGateway;
import org.flowable.bpmn.model.ParallelGateway;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 互斥网关创建
 *
 * @author zyf
 */
public class GateWayNodeCreator {

    public static List<ExclusiveGateway> createGateWay(List<ConditionNode> conditions) {
        List<ExclusiveGateway> gateways = new ArrayList<ExclusiveGateway>();
        Map<String, List<ConditionNode>> prev = conditions.stream().collect(Collectors.groupingBy(ConditionNode::getPid));
        for (String s : prev.keySet()) {
            ExclusiveGateway exclusiveGateway = new ExclusiveGateway();
            exclusiveGateway.setId(s);
            gateways.add(exclusiveGateway);
        }
        return gateways;
    }

    /**
     * 创建分支网关
     * @param childNodes
     * @return
     */
    public static List<ExclusiveGateway> createExclusiveGateway(List<ChildNode> childNodes) {
        List<ExclusiveGateway> exclusiveGateways = new ArrayList<>();
        for (ChildNode node : childNodes) {
            ExclusiveGateway exclusiveGateway = new ExclusiveGateway();
            exclusiveGateway.setId(node.getId());
            exclusiveGateway.setName(node.getName());
            exclusiveGateways.add(exclusiveGateway);
        }
        return exclusiveGateways;
    }

    /**
     * 创建并行网关
     * @param childNodes
     * @return
     */
    public static List<ParallelGateway> createParallelGateway(List<ChildNode> childNodes) {
        List<ParallelGateway> parallelGateways = new ArrayList<>();
        for (ChildNode node : childNodes) {
            ParallelGateway parallelGateway = new ParallelGateway();
            parallelGateway.setId(node.getId());
            parallelGateway.setName(node.getName());
            parallelGateways.add(parallelGateway);
        }
        return parallelGateways;
    }

    /**
     * 创建相容网关
     * @param childNodes
     * @return
     */
    public static List<InclusiveGateway> createInclusiveGateway(List<ChildNode> childNodes) {
        List<InclusiveGateway> inclusiveGateways = new ArrayList<>();
        for (ChildNode node : childNodes) {
            InclusiveGateway inclusiveGateway = new InclusiveGateway();
            inclusiveGateway.setId(node.getId());
            inclusiveGateway.setName(node.getName());
            inclusiveGateways.add(inclusiveGateway);
        }
        return inclusiveGateways;
    }
}
