package com.meida.bpm.provider.service;

import com.meida.bpm.provider.model.ProcessInstanceQueryVo;
import com.meida.bpm.provider.model.ProcessInstanceRequest;
import com.meida.bpm.provider.model.TaskRequest;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;

import java.util.List;


/**
 * 流程实例
 *
 * @author zyf
 */
public interface ProcessInstanceService {
    /**
     * 查询运行时流程实例
     *
     * @param processInstanceId 实例ID
     * @return
     */
    ProcessInstance getProcessInstanceById(String processInstanceId);

    /**
     * 查询历史流程实例
     *
     * @param processInstanceId
     * @return
     */
    HistoricProcessInstance getHistoricProcessInstanceById(String processInstanceId);

    /**
     * 启动流程实例
     *
     * @param processInstanceRequest
     */
    void start(ProcessInstanceRequest processInstanceRequest);

    /**
     * 删除流程实例
     *
     * @param processInstanceId
     * @param cascade
     * @param deleteReason
     */
    void delete(String processInstanceId, boolean cascade, String deleteReason);

    /**
     * 激活流程实例
     *
     * @param processInstanceId
     */
    void activate(String processInstanceId);

    /**
     * 挂起流程实例
     *
     * @param processInstanceId
     */
    void suspend(String processInstanceId);

    List listMyInvolvedSummary(ProcessInstanceQueryVo processInstanceQueryVo, String userId);


    List<HistoricActivityInstance> selectHistoricActivityInstance(String processInstanceId);

    /**
     * 撤销流程
     * @param processInstanceRequest
     */
    void callBackProcess(ProcessInstanceRequest processInstanceRequest);
}
