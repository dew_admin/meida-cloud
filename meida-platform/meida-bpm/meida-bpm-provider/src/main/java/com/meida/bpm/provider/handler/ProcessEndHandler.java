package com.meida.bpm.provider.handler;

import org.flowable.engine.delegate.DelegateExecution;

/**
 * 流程完成处理器
 *
 * @author zyf
 */
public interface ProcessEndHandler {

    /**
     * 校验
     * @param delegateExecution
     * @return
     */
    boolean supports(DelegateExecution delegateExecution);

    /**
     * 流程完成后处理器
     *
     * @param delegateExecution
     */
    void notify(DelegateExecution delegateExecution);
}
