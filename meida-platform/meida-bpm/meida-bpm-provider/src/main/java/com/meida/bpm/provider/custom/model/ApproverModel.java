package com.meida.bpm.provider.custom.model;

import lombok.Data;

/**
 * 审批人对象模型
 *
 * @author zyf
 */
@Data
public class ApproverModel {
    /**
     * ID
     */
    private String id;
    /**
     * 审批类型
     */
    private String approverType;
    /**
     * 人员类型
     */
    private String assigneeType;
    private String levelMode;
    /**
     * 候选用户ID
     */
    private String[] approverIds;
    /**
     * 候选用户名
     */
    private String[] approverNames;

    /**
     * 候选部门ID
     */
    private String[] deptIds;
    /**
     * 候选部门名
     */
    private String[] deptNames;

    /**
     * 候选角色ID
     */
    private String[] roleIds;
    /**
     * 候选角色名
     */
    private String[] roleNames;

    /**
     * 指定用户ID
     */
    private String approverId;
    /**
     * 指定用户名称
     */
    private String approverName;

    /**
     * 表达式ID集合
     */
    private String[] expressionsIds;
    /**
     * 表达式名称集合
     */
    private String[] expressionsNames;
}
