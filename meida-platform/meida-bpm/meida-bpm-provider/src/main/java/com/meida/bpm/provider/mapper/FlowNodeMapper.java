package com.meida.bpm.provider.mapper;

import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 流程节点表 Mapper 接口
 * @author flyme
 * @date 2024-12-15
 */
public interface FlowNodeMapper extends SuperMapper<FlowProcessNode> {

}
