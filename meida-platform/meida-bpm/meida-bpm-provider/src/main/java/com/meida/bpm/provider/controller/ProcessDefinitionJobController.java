package com.meida.bpm.provider.controller;


import com.meida.bpm.provider.common.BaseFlowableController;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.job.api.Job;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zyf
 */
@RestController
@RequestMapping("/flowable/processDefinitionJob")
public class ProcessDefinitionJobController extends BaseFlowableController {

    @GetMapping(value = "/list")
    public List<Job> list(@RequestParam String processDefinitionId) {
        return managementService.createTimerJobQuery().processDefinitionId(processDefinitionId).list();
    }


    /**
     * 新增流程定义定时任务
     *
     * @param jobId
     * @return
     */
    @DeleteMapping(value = "/delete")
    @Transactional(rollbackFor = Exception.class)
    public ResultBody deleteJob(@RequestParam String jobId) {
        managementService.deleteTimerJob(jobId);
        return ResultBody.ok();
    }
}
