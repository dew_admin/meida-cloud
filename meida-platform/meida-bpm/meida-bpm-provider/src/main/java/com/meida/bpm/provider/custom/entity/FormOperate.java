/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import lombok.Data;

/**
 * @author zyf
 */
@Data
public class FormOperate {
    private Integer formId;
    private boolean required;
    private String vModel;
    private String label;
    private boolean read;
    private boolean write;
}
