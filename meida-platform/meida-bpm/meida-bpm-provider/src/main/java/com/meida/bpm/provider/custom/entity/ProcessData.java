/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import com.meida.bpm.provider.custom.model.ListenerModel;
import lombok.Data;


import java.util.List;

/**
 * 流程对象
 *
 * @author zyf
 */
@Data
public class ProcessData extends Node {
    /**
     * 发起人
     */
    private String initiator;
    private List<FormOperate> formOperates;
    /**
     * 启动类型
     */
    private String startType;

    /**
     * 发起人节点ID
     */
    private String startTaskId;
    /**
     * 启动时间
     */
    private String timeDate;
    /**
     * 循环周期
     */
    private String timeCycle;

    /**
     * 其他属性
     */
    private ChildAttr attr;

    /**
     * 定时方式
     */
    private String timeType;
    /**
     * 结束时间
     */
    private String entTime;


    /**
     * 消息事件名称
     */
    private String messageEventName;

    /**
     * 是否创建发起人节点
     */
    private Boolean createStartNode;

    /**
     * 全局监听
     */
    private List<ListenerModel> eventListeners;

    /**
     * 全局执行监听
     */
    private List<ListenerModel> executeListeners;


}