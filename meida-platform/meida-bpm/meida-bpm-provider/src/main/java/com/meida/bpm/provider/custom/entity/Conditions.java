/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import com.meida.bpm.provider.custom.enums.QueryRuleEnum;
import lombok.Data;

/**
 * 条件对象
 *
 * @author zyf
 */
@Data
public class Conditions {
    /**
     * 条件值
     */
    private String val;

    /**
     * 字段类型
     */
    private String type;

    /**
     * 条件列描述
     */
    private String columnName;
    /**
     * 条件列名
     */
    private String field;
    /**
     * 条件值
     */
    private QueryRuleEnum rule;
    /**
     * 条件值描述
     */
    private String ruleName;
    /**
     * 条件值类型
     */
    private String valueType;
}