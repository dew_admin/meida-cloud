package com.meida.bpm.provider.model;

import lombok.Data;

import java.util.Map;

/**
 * @author zyf
 */
@Data
public class TaskRequest {
    private String taskId;
    private String userId;
    private String message;
    private Integer approve=1;
    private String activityId;
    private String activityName;
    private Map<String, Object> formMap;
    private CcToVo[] ccToVos;
    private String[] taskIds;
    private String completeTaskHandler;
}
