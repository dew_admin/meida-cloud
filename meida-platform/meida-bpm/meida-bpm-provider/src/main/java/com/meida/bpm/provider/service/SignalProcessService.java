package com.meida.bpm.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;

import java.util.Map;

/**
 * 信号启动接口
 */
public interface SignalProcessService {
    /**
     * 信号流程启动
     * @param companyId   企业ID
     * @param formKey   工作表key
     * @param action      动作(add/update/delete)
     * @param businessKey 业务主键
     * @param data        数据
     * @param applyUserId 发起人
     * @return
     */
    ResultBody signalStart(Long companyId, String formKey, String action, Long businessKey, Map<String,Object> data, Long applyUserId);

}
