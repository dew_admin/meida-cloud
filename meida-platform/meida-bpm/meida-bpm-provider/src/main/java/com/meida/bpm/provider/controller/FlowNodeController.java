package com.meida.bpm.provider.controller;

import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.provider.service.FlowNodeService;


/**
 * 流程节点表控制器
 *
 * @author flyme
 * @date 2024-12-15
 */
@RestController
@RequestMapping("/bpm/node/")
public class FlowNodeController extends BaseController<FlowNodeService, FlowProcessNode>  {

    @ApiOperation(value = "流程节点表-分页列表", notes = "流程节点表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "流程节点表-列表", notes = "流程节点表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "流程节点表-添加", notes = "添加流程节点表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "流程节点表-更新", notes = "更新流程节点表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "流程节点表-删除", notes = "删除流程节点表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "流程节点表-详情", notes = "流程节点表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    /**
     * 查询流程节点
     *
     * @param processId
     * @param processInstanceId
     * @return
     */
    @GetMapping(value = "/getProcessNode")
    public ResultBody getProcessNode(@RequestParam Long processId,@RequestParam(required = false) String processInstanceId) {
        List<EntityMap> list= bizService.selectProcessNode( processId,  processInstanceId);
        return ResultBody.ok(list);
    }

}
