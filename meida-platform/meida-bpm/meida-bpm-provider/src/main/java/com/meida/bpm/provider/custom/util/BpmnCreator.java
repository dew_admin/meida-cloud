package com.meida.bpm.provider.custom.util;


import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.provider.custom.constant.FlowProcessConstant;
import com.meida.bpm.provider.custom.entity.ChildAttr;
import com.meida.bpm.provider.custom.entity.ConditionGroup;
import com.meida.bpm.provider.custom.entity.ProcessData;
import com.meida.bpm.provider.custom.enums.StartTypeEnums;
import com.meida.bpm.provider.custom.enums.TimeTypeEnums;
import com.meida.bpm.provider.custom.model.CustomProcessForm;
import com.meida.bpm.provider.custom.model.ListenerModel;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.BpmnAutoLayout;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.*;


import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 流程节点元素创建
 *
 * @Author: zyf
 * @Date: 2022/07/26
 */
public class BpmnCreator {

    /**
     * 创建task
     *
     * @param id
     * @param name
     * @param assignee
     * @return
     */
    public static UserTask createUserTask(String id, String name, String assignee) {
        UserTask userTask = new UserTask();
        userTask.setName(name);
        userTask.setId(id);
        userTask.setAssignee(assignee);
        return userTask;
    }

    /**
     * 创建会签task
     *
     * @param id
     * @param name
     * @param assigneeList
     * @param assignName
     * @return
     */
    public static UserTask createCounterSign(String id, String name, String assigneeList, String assignName) {
        // 用户节点
        UserTask userTask = new UserTask();
        userTask.setId(id);
        userTask.setName(name);
        MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = new MultiInstanceLoopCharacteristics();
        // 审批人集合参数
        multiInstanceLoopCharacteristics.setInputDataItem(assigneeList);
        // 迭代集合
        multiInstanceLoopCharacteristics.setElementVariable(assignName);
        // 完成条件 已完成数等于实例数
        multiInstanceLoopCharacteristics.setCompletionCondition("${nrOfActiveInstances == nrOfInstances}");
        // 并行
        multiInstanceLoopCharacteristics.setSequential(false);
        userTask.setAssignee("${" + assignName + "}");
        // 设置多实例属性
        userTask.setLoopCharacteristics(multiInstanceLoopCharacteristics);
        // 设置监听器
        // userTask.setExecutionListeners(countersignTaskListener());
        return userTask;
    }

    /**
     * 创建箭头
     *
     * @param from
     * @param to
     * @return
     */
    public static SequenceFlow createSequenceFlow(String from, String to) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from);
        flow.setTargetRef(to);
        return flow;
    }

    /**
     * 带条件的箭头
     *
     * @param from
     * @param to
     * @param name
     * @param conditionExpression
     * @return
     */
    public static SequenceFlow createSequenceFlow(String from, String to, String name, String conditionExpression) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from);
        flow.setTargetRef(to);
        flow.setName(name);
        if (StringUtils.isNotEmpty(conditionExpression)) {
            flow.setConditionExpression(conditionExpression);
        }
        return flow;
    }

    public static StartEvent createStartEvent(String id) {
        StartEvent startEvent = new StartEvent();
        startEvent.setId(id);
        return startEvent;
    }

    /**
     * 排他网关
     *
     * @param id
     * @param name
     * @return
     */
    public static ExclusiveGateway createExclusiveGateway(String id, String name) {
        ExclusiveGateway exclusiveGateway = new ExclusiveGateway();
        exclusiveGateway.setId(id);
        exclusiveGateway.setName(name);
        return exclusiveGateway;
    }

    /**
     * 并行网关
     *
     * @param id
     * @param name
     * @return
     */
    public static ParallelGateway createParallelGateway(String id, String name) {
        ParallelGateway gateway = new ParallelGateway();
        gateway.setId(id);
        gateway.setName(name);
        return gateway;
    }

    public static ServiceTask createServiceTask(String id, String name, List<CustomProperty> properties) {
        ServiceTask service = new ServiceTask();
        service.setId(id);
        service.setName(name);
        service.setCustomProperties(properties);
        return service;
    }

    public static EndEvent createEndEvent() {
        EndEvent endEvent = new EndEvent();
        endEvent.setId("end");
        return endEvent;
    }

    /**
     * 构建监听器
     *
     * @param listenerModel
     * @return
     */
    public static FlowableListener createActivitiListener(ListenerModel listenerModel) {
        FlowableListener activitiListener = new FlowableListener();
        activitiListener.setEvent(listenerModel.getEventType());
        String listenerType = listenerModel.getListenerType();
        if (listenerType.equals(FlowProcessConstant.javaClass)) {
            listenerType = ImplementationType.IMPLEMENTATION_TYPE_CLASS;
        }
        activitiListener.setImplementationType(listenerType);
        activitiListener.setImplementation(listenerModel.getValue());
        activitiListener.setId(listenerModel.getId());
        return activitiListener;
    }

    /**
     * 创建流程xml
     *
     * @param extActProcessNodes
     * @param myExtActProcess
     * @param extActProcessForm
     * @param processData
     * @param userTasks
     * @param serviceTasks
     * @param gateways
     * @param polymerizeGateWays
     * @param inclusiveGateways
     * @param scriptTasks
     * @param callActivities
     * @param taskFlows
     * @param intermediateCatchEvents
     * @param messageTask
     * @return
     */
    public static String createXML(List<FlowProcessNode> extActProcessNodes, FlowProcess flowProcess, CustomProcessForm extActProcessForm, ProcessData processData, List<UserTask> userTasks, List<ServiceTask> serviceTasks, List<ExclusiveGateway> gateways, List<ParallelGateway> polymerizeGateWays, List<InclusiveGateway> inclusiveGateways, List<ScriptTask> scriptTasks, List<CallActivity> callActivities, List<SequenceFlow> taskFlows, List<IntermediateCatchEvent> intermediateCatchEvents, List<ServiceTask> messageTask) {
        Long companyId=flowProcess.getCompanyId();
        //根节点属性配置
        ChildAttr childAttr = processData.getAttr();
        BpmnModel bpmn = new BpmnModel();
        bpmn.setTargetNamespace(FlowProcessConstant.TargetNamespace);
        ExtensionAttribute extensionAttribute = new ExtensionAttribute();
        extensionAttribute.setName("designerJSON");
        extensionAttribute.setValue(FlowProcessConstant.TargetNamespace);
        //流程xml对象定义
        Process process = new Process();
        //process.addAttribute(extensionAttribute);
        process.setId(flowProcess.getProcessKey());
        //简版模式下的启动类型(手动,工作表,定时)
        String startType = flowProcess.getStartType();
        if (StartTypeEnums.subEvent.name().equals(startType)) {
            process.setId("process" + flowProcess.getProcessId());
        }
        process.setName(flowProcess.getProcessName());
        //设置全局执行监听
        List<FlowableListener> listeners = getExecutionListener(processData.getExecuteListeners());

        // 1.创建节点
        StartEvent start = BpmnCreator.createStartEvent(processData.getId());
        start.setName(processData.getName());
        FlowProcessNode extActProcessNode = new FlowProcessNode();
        extActProcessNode.setProcessNodeCode(start.getId());
        extActProcessNode.setProcessNodeName(processData.getName());
        extActProcessNode.setSortNo(0);
        if (ObjectUtils.isNotEmpty(childAttr)) {
            extActProcessNode.setNodeConfigJson(JSON.toJSONString(childAttr));
        }
        extActProcessNodes.add(extActProcessNode);
        //设置开始节点执行监听
        List<FlowableListener> startListeners = getExecutionListener(processData.getListenerData());
        if (ObjectUtils.isNotEmpty(startListeners)) {
            start.setExecutionListeners(startListeners);
        }
        if (StringUtils.isNotEmpty(processData.getInitiator())) {
            start.setInitiator(processData.getInitiator());
        }
        //信号定时启动监听
        FlowableListener signalProcessStartListener = getSignalProcessStartListener();
        //流程结束监听
        FlowableListener processEndListener = getProcessEndListener();
        if (StartTypeEnums.manual.name().equals(startType)) {
            String oldStartType = processData.getStartType();
            //定时启动
            if (StartTypeEnums.timerEvent.name().equals(oldStartType)) {
                //循环周期值
                String timeCycle = processData.getTimeCycle();
                //定时方式
                String timeType = processData.getTimeType();
                //指定日期值
                String timeDate = processData.getTimeDate();
                if (StringUtils.isNotEmpty(timeDate)) {
                    timeDate = timeDate.substring(0, timeDate.lastIndexOf(":"));
                }
                TimerEventDefinition timerEventDefinition = new TimerEventDefinition();
                timerEventDefinition.setId("TimerEventDefinition_" + IdUtil.simpleUUID().substring(0, 7));
                //指定时间
                if (ObjectUtils.isNotEmpty(timeType)) {
                    if (TimeTypeEnums.timeDate.name().equals(timeType)) {
                        if (!timeCycle.equals("custom")) {
                            timerEventDefinition.setTimeCycle(timeCycle);
                        } else {
                            timerEventDefinition.setTimeCycle(timeDate);
                        }
                    }
                    if (TimeTypeEnums.timeDuration.name().equals(timeType)) {
                        if (!timeCycle.equals("custom")) {
                            timerEventDefinition.setTimeDuration(timeCycle);
                        } else {
                            timerEventDefinition.setTimeDuration(timeDate);
                        }
                    }
                    if (TimeTypeEnums.timeDate.name().equals(timeType)) {
                        timerEventDefinition.setTimeDate(timeDate);
                    }
                }
                start.addEventDefinition(timerEventDefinition);
            }

            //信号启动
            if (StartTypeEnums.signal.name().equals(oldStartType)) {
                //创建信号
                String signalEventName = processData.getAttr().getSignalEventName();
                addSignal(signalEventName, start, bpmn);
            }

            //消息启动
            if (StartTypeEnums.message.name().equals(oldStartType)) {
                //创建消息
                String messageEventName = processData.getMessageEventName();
                addMessageEvent(messageEventName, start, bpmn);
            }
        } else {
            //开始事件
            String beginDate = childAttr.getBeginDateStr();
            //定时启动
            if (StartTypeEnums.timerEvent.name().equals(startType) && StringUtils.isNotEmpty(beginDate)) {
                TimerEventDefinition timerEventDefinition = new TimerEventDefinition();
                //循环周期
                String timeCycle = childAttr.getTimeCycle();
                //结束时间
                String endDate = childAttr.getEndDateStr();
                if (StringUtils.isNotEmpty(endDate)) {
                    endDate = endDate.replace(" ", "T");
                    timerEventDefinition.setEndDate(endDate);
                }
                if (StringUtils.isNotEmpty(timeCycle)) {
                    //R【循环次数】【/开始时间】/时间间隔【/结束时间】
                    beginDate = beginDate.replace(" ", "T");
                    //每分钟
                    if (timeCycle.equals("1")) {
                        timerEventDefinition.setTimeCycle("R/" + beginDate + "/PT1M");
                    }
                    //每小时
                    if (timeCycle.equals("2")) {
                        timerEventDefinition.setTimeCycle("R/" + beginDate + "/PT1H");
                    }
                    //每天
                    if (timeCycle.equals("3")) {
                        timerEventDefinition.setTimeCycle("R/" + beginDate + "/P1D");
                    }
                    //每月1号
                    if (timeCycle.equals("4")) {
                        timerEventDefinition.setTimeCycle("0 0 1 1 * ?");
                    }
                    //每周三
                    if (timeCycle.equals("5")) {
                        timerEventDefinition.setTimeCycle("0 0 1 ? * WED");
                    }
                    //周一到周五
                    if (timeCycle.equals("6")) {
                        timerEventDefinition.setTimeCycle("0 0 1 ? * MON-FRI");
                    }
                    //每年12月31日
                    if (timeCycle.equals("7")) {
                        timerEventDefinition.setTimeCycle("0 0 1 31 12 ?");
                    }
                    //自定义
                    if (timeCycle.equals("custom")) {
                        String cron = getCustomCron(childAttr);
                        timerEventDefinition.setTimeCycle(cron);
                    }
                }
                timerEventDefinition.setId("TimerEventDefinition_" + IdUtil.simpleUUID().substring(0, 7));
                start.addEventDefinition(timerEventDefinition);
                //listeners.add(signalProcessStartListener);
            }
            //信号启动
            if (StartTypeEnums.tableEvent.name().equals(startType)) {
                //表名
                String formTableCode = childAttr.getFormTableCode();
                //触发方式
                String startEventType = childAttr.getStartEventType();
                String startCondition = childAttr.getStartCondition();
                ApiAssert.isNotEmpty("启动信号不完整,请选择数据表!",formTableCode);
                //创建信号(信号名称规则:表名_企业ID_触发方式)
                String signalEventName = formTableCode+":"+companyId + ":" + startEventType;
                String[] conditionFields = childAttr.getConditionFields();
                //拼接触发字段到信号中
                if (ObjectUtils.isNotEmpty(conditionFields)) {
                    String conditionFieldKey = Md5Util.md5Encode(ArrayUtil.join(conditionFields, ","), "utf-8");
                    signalEventName += ":" + conditionFieldKey;
                }
                //拼接触发条件到信号中
                if (StringUtils.isNotEmpty(startCondition)) {
                    List<ConditionGroup> groups = JSONObject.parseArray(startCondition, ConditionGroup.class);
                    if (ObjectUtils.isNotEmpty(groups)) {
                        String conditionKey = Md5Util.md5Encode(startCondition, "utf-8");
                        signalEventName += ":" + conditionKey;

                    }
                }
                addSignal(signalEventName, start, bpmn);

                listeners.add(signalProcessStartListener);
                listeners.add(processEndListener);
            }

            //按钮启动
            if (StartTypeEnums.buttonEvent.name().equals(startType)) {
                //表名
                String formTableCode = childAttr.getFormTableCode();
                //创建按钮信号
                String signalEventName = FlowProcessConstant.BUTTON_SIGNAL_PRE + flowProcess.getProcessId();
                addSignal(signalEventName, start, bpmn);
                if (StringUtils.isNotEmpty(formTableCode)) {
                    //有表单时添加监听
                    listeners.add(signalProcessStartListener);
                }
            }

            //子流程启动
            if (StartTypeEnums.subEvent.name().equals(startType)) {
                //信号定时启动监听
                FlowableListener subEventStartListener = getSubEventStartListener();
                listeners.add(subEventStartListener);
            }

            //流程表单中间表赋值
            extActProcessForm.setTriggerAction(childAttr.getStartEventType());
            extActProcessForm.setFormTableName(childAttr.getFormTableCode());
            String titleField = childAttr.getTitleField();
            if (StringUtils.isNotEmpty(titleField)) {
                extActProcessForm.setTitleExp(childAttr.getFormTableName() + "【${" + titleField + "}】");
            }
            extActProcessForm.setFormType(childAttr.getFormType());
            extActProcessForm.setFormDealStyle("default");

            //人员事件
            if (StartTypeEnums.userEvent.name().equals(startType)) {
                //事件类型
                Integer selectType = childAttr.getSelectType();
                //事件信号名称
                String signalEventName = selectType.equals(1) ? FlowProcessConstant.USER_SIGNAL_ADD : FlowProcessConstant.USER_SIGNAL_LEAVE;
                addSignal(signalEventName, start, bpmn);
                listeners.add(signalProcessStartListener);
                extActProcessForm.setFormTableName("sys_user");
                String titleExp = selectType.equals(1) ? "人员入职" + "【${realname}】" : "人员离职" + "【${realname}】";
                extActProcessForm.setTitleExp(titleExp);
                extActProcessForm.setFormType("3");
            }
        }
        //创建结束节点
        EndEvent end = BpmnCreator.createEndEvent();
        process.addFlowElement(start);
        process.addFlowElement(end);

        StringBuilder nodes = new StringBuilder();
        for (UserTask task : userTasks) {
            process.addFlowElement(task);
            List<BoundaryEvent> boundaryEvents = task.getBoundaryEvents();
            if (ObjectUtils.isNotEmpty(boundaryEvents)) {
                for (BoundaryEvent boundaryEvent : boundaryEvents) {
                    process.addFlowElement(boundaryEvent);
                }
            }
        }

        for (ServiceTask task : serviceTasks) {
            process.addFlowElement(task);
        }

        for (ServiceTask task : messageTask) {
            process.addFlowElement(task);
        }

        for (ScriptTask task : scriptTasks) {
            process.addFlowElement(task);
        }
        for (ExclusiveGateway gateway : gateways) {
            process.addFlowElement(gateway);
        }

        for (ParallelGateway parallelGateway : polymerizeGateWays) {
            process.addFlowElement(parallelGateway);
        }
        for (CallActivity callActivity : callActivities) {
            process.addFlowElement(callActivity);
        }

        for (InclusiveGateway inclusiveGateway : inclusiveGateways) {
            process.addFlowElement(inclusiveGateway);
        }
        for (SequenceFlow flow : taskFlows) {
            process.addFlowElement(flow);
        }
        for (IntermediateCatchEvent intermediateCatchEvent : intermediateCatchEvents) {
            process.addFlowElement(intermediateCatchEvent);
        }

        if (ObjectUtils.isNotEmpty(listeners)) {
            process.setExecutionListeners(listeners);
        }
        bpmn.addProcess(process);
        //设置事件监听
        setEventListener(processData, process);
        // 2.生成BPMN自动布局
        new BpmnAutoLayout(bpmn).execute();
        // 3.输出xml
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmn);
        String bytes="";
        try{
             bytes = new String(convertToXML,"UTF-8");
        }catch (Exception e){
        }
        //赋值流程节点
        flowProcess.setNote(nodes.toString());
        return bytes;
    }

    /**
     * 设置流程全局执行监听
     *
     * @param listenerData
     */
    private static List<FlowableListener> getExecutionListener(List<ListenerModel> listenerData) {
        List<FlowableListener> executionListeners = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(listenerData)) {
            for (ListenerModel listenerModel : listenerData) {
                FlowableListener executionListener = new FlowableListener();
                executionListener.setId(listenerModel.getId());
                executionListener.setImplementation(listenerModel.getValue());
                String listenerType = listenerModel.getListenerType();
                if (listenerType.equals(FlowProcessConstant.javaClass)) {
                    executionListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_CLASS);
                } else {
                    executionListener.setImplementationType(listenerType);
                }
                executionListener.setEvent(listenerModel.getEventType());
                executionListeners.add(executionListener);
            }
        }
        return executionListeners;
    }

    /**
     * 设置事件监听
     *
     * @param processData
     * @param process
     */
    private static void setEventListener(ProcessData processData, Process process) {
        List<ListenerModel> eventListeners = processData.getEventListeners();
        if (ObjectUtils.isNotEmpty(eventListeners)) {
            List<EventListener> executionListeners = new ArrayList<>();
            for (ListenerModel listenerModel : eventListeners) {
                EventListener eventListener = new EventListener();
                eventListener.setId(listenerModel.getId());
                eventListener.setImplementation(listenerModel.getValue());
                String listenerType = listenerModel.getListenerType();
                if (listenerType.equals(FlowProcessConstant.javaClass)) {
                    eventListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_CLASS);
                } else {
                    eventListener.setImplementationType(listenerType);
                }
                String eventType = listenerModel.getEventType();
                if (StringUtils.isNotEmpty(eventType)) {
                    eventListener.setEvents(eventType);
                }
                executionListeners.add(eventListener);
            }
            process.setEventListeners(executionListeners);
        }
    }

    /**
     * 添加信号启动
     *
     * @param signalName
     * @param start
     * @param bpmn
     */
    private static void addSignal(String signalName, StartEvent start, BpmnModel bpmn) {
        String signalId = "Signal_" + IdUtil.simpleUUID().substring(0, 6);
        Signal signal = new Signal();
        signal.setName(signalName);
        signal.setId(signalId);
        //添加信号
        bpmn.addSignal(signal);
        //创建信号事件定义
        SignalEventDefinition signalEventDefinition = new SignalEventDefinition();
        signalEventDefinition.setId("SignalEventDefinition_" + IdUtil.simpleUUID().substring(0, 7));
        signalEventDefinition.setSignalRef(signalId);
        //设置信号启动事件
        start.addEventDefinition(signalEventDefinition);
    }

    /**
     * 添加消息启动
     *
     * @param messageEventName
     * @param start
     * @param bpmn
     */
    private static void addMessageEvent(String messageEventName, StartEvent start, BpmnModel bpmn) {
        String messageId = "Message_" + IdUtil.simpleUUID().substring(0, 6);
        Message message = new Message();
        message.setName(messageEventName);
        message.setId(messageId);
        //添加消息
        bpmn.addMessage(message);
        //创建消息事件定义
        MessageEventDefinition messageEventDefinition = new MessageEventDefinition();
        messageEventDefinition.setId("MessageEventDefinition_" + IdUtil.simpleUUID().substring(0, 7));
        messageEventDefinition.setMessageRef(messageId);
        //设置消息启动事件
        start.addEventDefinition(messageEventDefinition);
    }

    /**
     * 获取信号/定时流程启动监听
     */
    private static FlowableListener getSignalProcessStartListener() {
        FlowableListener executionListener = new FlowableListener();
        executionListener.setImplementation("${signalProcessStartListener}");
        executionListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
        executionListener.setEvent("start");
        return executionListener;
    }

    /**
     * 获取信号/定时流程结束监听
     */
    private static FlowableListener getProcessEndListener() {
        FlowableListener executionListener = new FlowableListener();
        executionListener.setImplementation("${flowProcessEndListener}");
        executionListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
        executionListener.setEvent("end");
        return executionListener;
    }

    /**
     * 子流程开始监听
     *
     * @return
     */
    private static FlowableListener getSubEventStartListener() {
        FlowableListener executionListener = new FlowableListener();
        executionListener.setImplementation("com.meida.bpm.provider.listener.MiniSubProcessStartListener");
        executionListener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_CLASS);
        executionListener.setEvent("start");
        return executionListener;
    }

    /**
     * 计算自定义定时表达式
     *
     * @return
     */
    private static String getCustomCron(ChildAttr childAttr) {
        String cron = "0 0 {0} {1} {2} {3}";
        //小时
        String param0 = "*";
        //天
        String param1 = "*";
        //月
        String param2 = "*";
        //星期
        String param3 = "?";
        //小时类型
        Integer hourType = FlymeUtils.getInteger(childAttr.getHourType(), 1);
        //小时 按范围触发
        if (hourType.equals(2)) {
            Integer hourValueMin = childAttr.getHourValueMin();
            Integer hourValueMax = childAttr.getHourValueMax();
            param0 = hourValueMin + "-" + hourValueMax;
        }
        //小时 按固定值触发
        if (hourType.equals(3)) {
            Integer[] hourValues = childAttr.getHourValues();
            String value = ArrayUtil.join(hourValues, ",");
            param0 = value;
        }
        //小时 按一定增量触发
        if (hourType.equals(4)) {
            Integer hourValueMin = childAttr.getHourValueMin();
            Integer hourValueMax = childAttr.getHourValueMax();
            param0 = hourValueMin + "/" + hourValueMax;
        }
        //天类型
        Integer dayValue = FlymeUtils.getInteger(childAttr.getDayValue(), 1);
        Integer dayOrWeekType = FlymeUtils.getInteger(childAttr.getDayOrWeekType(), 1);
        if (dayOrWeekType.equals(1)) {
            //天 按范围触发
            if (dayValue.equals(2)) {
                Integer dayValueMin = childAttr.getDayValueMin();
                Integer dayValueMax = childAttr.getDayValueMax();
                param1 = dayValueMin + "-" + dayValueMax;
            }
            //天 按固定值触发
            if (dayValue.equals(3)) {
                Integer[] dayValues = childAttr.getDayValues();
                String value = ArrayUtil.join(dayValues, ",");
                param1 = value;
            }
            //天 按一定增量触发
            if (dayValue.equals(4)) {
                Integer dayValueMin = childAttr.getDayValueMin();
                Integer dayValueMax = childAttr.getDayValueMax();
                param1 = dayValueMin + "/" + dayValueMax;
            }
            //天 每月最后一天触发
            if (dayValue.equals(5)) {
                Integer days = getDaysOfMonth();
                param1 = days + "";
            }
        }
        if (dayOrWeekType.equals(2)) {
            //星期选项值
            String[] weekValue = childAttr.getWeekValue();
            String value = ArrayUtil.join(weekValue, ",");
            param1 = "?";
            param3 = value;
        }
        //月类型
        Integer monthType = FlymeUtils.getInteger(childAttr.getMonthType(), 1);
        if (monthType.equals(2)) {
            Integer[] monthValues = childAttr.getMonthValues();
            String value = ArrayUtil.join(monthValues, ",");
            param2 = value;
        }
        return MessageFormat.format(cron, param0, param1, param2, param3);
    }

    /**
     * 获取当月天数
     *
     * @return
     */
    private static int getDaysOfMonth() {
        Date date = DateUtils.getNowDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
}
