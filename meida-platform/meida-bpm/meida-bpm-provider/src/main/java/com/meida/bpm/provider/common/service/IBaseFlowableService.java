package com.meida.bpm.provider.common.service;

import com.meida.bpm.provider.listener.FlowListInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.common.engine.api.query.Query;
import org.flowable.common.engine.api.query.QueryProperty;

import java.util.Map;

/**
 * @author zyf
 */
public interface IBaseFlowableService {

    /**
     * 分页列表
     *
     * @param params
     * @param query
     * @param listInterceptor
     * @param allowedSortProperties
     * @param defaultDescSortProperty
     * @return
     */
    ResultBody pageList(Map params, Query query, Map<String, QueryProperty> allowedSortProperties, QueryProperty defaultDescSortProperty, FlowListInterceptor listInterceptor);
}
