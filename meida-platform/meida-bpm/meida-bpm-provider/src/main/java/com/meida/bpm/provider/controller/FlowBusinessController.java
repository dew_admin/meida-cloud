package com.meida.bpm.provider.controller;

import cn.hutool.core.map.MapUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.bpm.provider.service.FlowBusinessService;


/**
 * 流程业务数据控制器
 *
 * @author flyme
 * @date 2024-12-12
 */
@RestController
@RequestMapping("/bpm/business/")
public class FlowBusinessController extends BaseController<FlowBusinessService, FlowBusiness>  {

    @ApiOperation(value = "流程业务数据-分页列表", notes = "流程业务数据分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "流程业务数据-列表", notes = "流程业务数据列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "流程业务数据-添加", notes = "添加流程业务数据")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "流程业务数据-更新", notes = "更新流程业务数据")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "流程业务数据-删除", notes = "删除流程业务数据")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "流程业务数据-详情", notes = "流程业务数据详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "流程业务数据-逻辑删除", notes = "逻辑删除")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(value = "flowBusId") Long flowBusId) {
         bizService.logicDelete(flowBusId);
         return ResultBody.ok("删除成功");
    }

    /**
     * 更新催办状态
     */
    @ApiOperation(value = "更新催办状态", notes = "更新催办状态")
    @PostMapping(value = "setUrging")
    public ResultBody setUrging(@RequestParam(value = "flowBusId") Long flowBusId,
                                @RequestParam(value = "taskDefinitionKey") String taskDefinitionKey,
                                @RequestParam(value = "taskId") String taskId,
                                @RequestParam(value = "handlerName") String handlerName,
                                @RequestParam(value = "urging") Integer urging) {
        Boolean tag= bizService.setUrging(flowBusId,urging,taskDefinitionKey,taskId,handlerName);
        return ResultBody.ok(tag);
    }

    /**
     * 导出数据
     *
     * @param params
     * @param request
     * @param response
     */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        String handlerName = MapUtil.getStr(params, "handlerName");
        String fileName = MapUtil.getStr(params, "fileName");
        bizService.export(null,params, request, response, fileName, fileName, handlerName);
    }
}
