package com.meida.bpm.provider.custom.enums;

/**
 * 启动方式枚举
 *
 * @author zyf
 */
public enum StartTypeEnums {
    /**
     * 手动
     */
    manual,
    /**
     * 工作表事件
     */
    tableEvent,

    /**
     * 定时
     */
    timerEvent,
    /**
     * 信号
     */
    signal,

    /**
     * 消息
     */
    message,

    /**
     * 按钮触发
     */
    buttonEvent,

    /**
     * 子流程
     */
    subEvent,

    /**
     * 人员事件触发（比如：人员离职）
     */
    userEvent,
    /**
     * 按日期字段
     */
    dateFieldEvent
}
