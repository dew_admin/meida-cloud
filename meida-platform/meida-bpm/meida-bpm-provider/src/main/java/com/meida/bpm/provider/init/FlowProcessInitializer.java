package com.meida.bpm.provider.init;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.common.dbinit.DbInitializer;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class FlowProcessInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return FlowProcess.class;
    }

}
