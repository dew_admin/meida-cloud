/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;

import com.meida.bpm.provider.custom.model.ListenerModel;
import lombok.Data;
import org.flowable.bpmn.model.IOParameter;


import java.util.List;

/**
 * 节点定义
 *
 * @author zyf
 */
@Data
public class Node {
    /**
     * 节点ID
     */
    private String id;
    /**
     * 父ID
     */
    private String pid;
    /**
     * 节点类型
     */
    private String type;
    /**
     * 节点名称
     */
    private String name;
    /**
     * 节点内容
     */
    private String content;
    /**
     * 流程节点状态(用于只读模式, 0:未进行 1:进行中  2:已完成)
     */
    private Integer status;


    /**
     * 下级节点
     */
    private ChildNode childNode;
    /**
     * 条件节点
     */
    private List<ConditionNode> conditionNodes;

    /**
     * 执行监听
     */
    private List<ListenerModel> listenerData;

    /**
     * 任务监听
     */
    private List<ListenerModel> taskListenerData;

    /**
     * 输入变量
     */
    private List<IOParameter> inVariableModels;

    /**
     * 输入变量
     */
    private List<IOParameter> outVariableModels;

}
