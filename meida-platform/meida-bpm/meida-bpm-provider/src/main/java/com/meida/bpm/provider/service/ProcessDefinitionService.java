package com.meida.bpm.provider.service;


import com.meida.bpm.provider.model.IdentityRequest;
import com.meida.bpm.provider.model.ProcessDefinitionRequest;
import org.flowable.engine.repository.ProcessDefinition;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author zyf
 * @date 2020年3月24日
 */
public interface ProcessDefinitionService {

    /**
     * 查询单一流程定义
     * 
     * @param processDefinitionId
     * @return
     */
    ProcessDefinition getProcessDefinitionById(String processDefinitionId);

    /**
     * 删除流程定义

     * @param params
     */
    void delete(Map params);

    /**
     * 删除流程定义

     * @param processDefId
     */
    void delete(String processDefId);

    /**
     * 激活流程定义
     * 
     * @param actionRequest
     */
    void activate(ProcessDefinitionRequest actionRequest);

    /**
     * 挂起流程定义
     * 
     * @param actionRequest
     */
    void suspend(ProcessDefinitionRequest actionRequest);

    /**
     * 导入流程定义
     * 
     * @param tenantId
     * @param request
     */
    void doImport(String tenantId, HttpServletRequest request);

    /**
     * 导入流程定义
     *
     * @param tenantId
     * @param request
     */
    void importProcess(String tenantId, HttpServletRequest request);

    /**
     * 保存流程授权
     * 
     * @param identityRequest
     */
    void saveProcessDefinitionIdentityLink(IdentityRequest identityRequest);

    /**
     * 删除流程授权
     * 
     * @param processDefinitionId
     * @param identityId
     * @param type
     */
    void deleteProcessDefinitionIdentityLink(String processDefinitionId, String identityId, String type);
}
