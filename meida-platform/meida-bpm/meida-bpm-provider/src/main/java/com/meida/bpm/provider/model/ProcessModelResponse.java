package com.meida.bpm.provider.model;

import lombok.Data;

import java.util.Date;

/**
 * @author zyf
 */
@Data
public class ProcessModelResponse {

    private String id;
    private String name;
    private String key;
    private String category;
    private String description;
    private String tenantId;
    private String processXml;

    private Date createTime;
    private Date lastUpdateTime;
    private Boolean deployed;
    private Integer version;
}
