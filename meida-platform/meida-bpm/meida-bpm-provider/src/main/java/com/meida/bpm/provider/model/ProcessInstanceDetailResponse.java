package com.meida.bpm.provider.model;

import lombok.Data;

/**
 * @author zyf
 */
@Data
public class ProcessInstanceDetailResponse extends HistoricProcessInstanceResponse {
    private boolean suspended;
    private String deleteReason;
    private String startUserName;

}
