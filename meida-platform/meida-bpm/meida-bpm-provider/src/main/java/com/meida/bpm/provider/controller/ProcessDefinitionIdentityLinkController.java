package com.meida.bpm.provider.controller;


import com.meida.bpm.provider.common.BaseFlowableController;
import com.meida.bpm.provider.model.IdentityRequest;
import com.meida.bpm.provider.model.IdentityResponse;
import com.meida.bpm.provider.service.ProcessDefinitionService;
import com.meida.common.mybatis.model.ResultBody;
import org.flowable.identitylink.api.IdentityLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 流程授权管理
 *
 * @author zyf
 * @date 2020年3月24日
 */
@RestController
@RequestMapping("/bpm/process/auth")
public class ProcessDefinitionIdentityLinkController extends BaseFlowableController {
    @Autowired
    private ProcessDefinitionService processDefinitionService;


    /**
     * 已授权列表
     *
     * @param processDefinitionId
     * @return
     */
    @GetMapping(value = "/list")
    public ResultBody list(@RequestParam String processDefinitionId) {
        //ProcessDefinition processDefinition = processDefinitionService.getProcessDefinitionById(processDefinitionId);
        List<IdentityLink> identityLinks = repositoryService.getIdentityLinksForProcessDefinition(processDefinitionId);
        List<IdentityResponse> list = responseFactory.createIdentityResponseList(identityLinks);
        return ResultBody.ok(list);
    }

    /**
     * 新增流程定义授权
     *
     * @param identityRequest
     * @return
     */
    @PostMapping(value = "/save")
    public ResultBody save(IdentityRequest identityRequest) {
        processDefinitionService.saveProcessDefinitionIdentityLink(identityRequest);
        return ResultBody.ok();
    }

    /**
     * 删除流程定义授权
     *
     * @param processDefinitionId
     * @param identityId
     * @param identityType
     * @return
     */
    @PostMapping(value = "/delete")
    public ResultBody delete(@RequestParam String processDefinitionId, @RequestParam String identityId, @RequestParam String identityType) {
        processDefinitionService.deleteProcessDefinitionIdentityLink(processDefinitionId, identityId, identityType);
        return ResultBody.ok();
    }
}
