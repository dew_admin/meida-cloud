package com.meida.bpm.provider.controller;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.bpm.provider.service.FlowProcessService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 仿钉钉流程设计保存
 *
 * @author flyme
 * @date 2020-06-11
 */
@RestController
@RequestMapping("/bpm/process/")
public class CustomFlowProcessController extends BaseController<FlowProcessService, FlowProcess> {

    /**
     * 流程设计保存
     *
     * @param flowProcess
     * @return
     */
    @RequestMapping({"/saveProcess"})
    @ResponseBody
    public ResultBody saveProcess(FlowProcess flowProcess) {
        return bizService.saveProcess(flowProcess);
    }

    /**
     * 克隆流程
     */
    @ApiOperation(value = "克隆流程", notes = "克隆流程")
    @PostMapping(value = "copyProcess")
    public ResultBody copyProcess(@RequestParam(value = "processId") Long processId) {
        Boolean tag= bizService.copyProcess(processId);
        return ResultBody.ok(tag);
    }


}
