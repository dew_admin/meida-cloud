package com.meida.bpm.provider.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zyf
 */
@Data
public class IdentityRequest implements Serializable {
    private String processDefinitionId;
    private String taskId;
    private String identityId;
    private String identityType;
}
