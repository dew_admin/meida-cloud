package com.meida.bpm.provider.mapper;


import com.meida.bpm.client.entity.FlowForm;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * @author zyf
 */
public interface FlowFormMapper extends SuperMapper<FlowForm> {
}
