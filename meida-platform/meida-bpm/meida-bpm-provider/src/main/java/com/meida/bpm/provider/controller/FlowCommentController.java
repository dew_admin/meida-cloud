package com.meida.bpm.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.bpm.client.entity.FlowComment;
import com.meida.bpm.provider.service.FlowCommentService;


/**
 * 流程评论表控制器
 *
 * @author flyme
 * @date 2024-12-16
 */
@RestController
@RequestMapping("/bpm/comment/")
public class FlowCommentController extends BaseController<FlowCommentService, FlowComment>  {

    @ApiOperation(value = "流程评论表-分页列表", notes = "流程评论表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "流程评论表-列表", notes = "流程评论表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "流程评论表-添加", notes = "添加流程评论表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
         bizService.add(params);
         return ResultBody.msg("评论成功");
    }

    @ApiOperation(value = "流程评论表-更新", notes = "更新流程评论表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "流程评论表-删除", notes = "删除流程评论表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "流程评论表-详情", notes = "流程评论表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
