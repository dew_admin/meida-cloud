package com.meida.bpm.provider.mapper;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 流程表 Mapper 接口
 * @author flyme
 * @date 2020-06-11
 */
public interface FlowProcessMapper extends SuperMapper<FlowProcess> {

}
