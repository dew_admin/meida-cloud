package com.meida.bpm.provider.service.impl;

import com.meida.bpm.client.entity.FlowProcess;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.bpm.client.entity.FlowCategory;
import com.meida.bpm.provider.mapper.FlowCategoryMapper;
import com.meida.bpm.provider.service.FlowCategoryService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 流程分类表接口实现类
 *
 * @author flyme
 * @date 2020-06-14
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FlowCategoryServiceImpl extends BaseServiceImpl<FlowCategoryMapper, FlowCategory> implements FlowCategoryService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, FlowCategory category, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<FlowCategory> cq, FlowCategory category, EntityMap requestMap) {
      cq.eq(FlowCategory.class,"categoryName");
      cq.orderByDesc("category.createTime");
      return ResultBody.ok();
    }

    public List<EntityMap> selectTypeList(){
        CriteriaQuery cq=new CriteriaQuery<>(FlowCategory.class);
        cq.select(FlowCategory.class,"categoryId as value","categoryCode","categoryName as label");
        cq.eq(FlowCategory.class,"status",1);
        return selectEntityMap(cq);
    }

    @Override
    public FlowCategory findByCode(String categoryCode) {
        CriteriaQuery cq = new CriteriaQuery(FlowProcess.class);
        cq.eq(true, "categoryCode", categoryCode);
        return getOne(cq,false);
    }
}
