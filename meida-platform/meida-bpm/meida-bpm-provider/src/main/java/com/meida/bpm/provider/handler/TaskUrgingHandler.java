package com.meida.bpm.provider.handler;

import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.common.security.OpenUser;

import java.util.List;

/**
 * 任务催办完成处理器
 *
 * @author zyf
 */
public interface TaskUrgingHandler {


    /**
     * 任务完成后处理器
     *
     */
    default void completeAfter(List<String> userIds, FlowBusiness flowBusiness,OpenUser openUser) {

    }
}
