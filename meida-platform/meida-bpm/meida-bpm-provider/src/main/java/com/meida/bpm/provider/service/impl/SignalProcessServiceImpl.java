package com.meida.bpm.provider.service.impl;

import com.meida.bpm.provider.service.SignalProcessService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.IdentityService;
import org.flowable.engine.RuntimeService;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@AllArgsConstructor
@Component
public class SignalProcessServiceImpl implements SignalProcessService {

    private final IdentityService identityService;
    private final RuntimeService runtimeService;

    @Override
    public ResultBody signalStart(Long companyId,String formKey, String action,Long businessKey, Map<String,Object> data, Long applyUserId) {
        data.put("businessKey", businessKey);
        data.put("applyUserId", applyUserId);
        String signalName = getSignalName(companyId,action, formKey, businessKey, data);
        // 设置流程发起人
        identityService.setAuthenticatedUserId(applyUserId+"");
        //直接发起流程
        runtimeService.signalEventReceivedWithTenantId(signalName, data,companyId+"");
        return ResultBody.ok();
    }

    private String getSignalName(Long companyId,String action,String formKey, Long businessKey, Map<String,Object>  data){
        return formKey+":"+companyId+":"+action;
    }
}
