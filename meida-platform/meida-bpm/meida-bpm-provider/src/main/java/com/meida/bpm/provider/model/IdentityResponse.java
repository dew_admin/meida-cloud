package com.meida.bpm.provider.model;

import lombok.Data;

/**
 * @author zyf
 */
@Data
public class IdentityResponse {
    private String identityId;
    private String identityType;
    private String identityName;
}
