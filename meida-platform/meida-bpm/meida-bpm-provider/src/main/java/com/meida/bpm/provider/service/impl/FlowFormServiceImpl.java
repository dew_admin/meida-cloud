package com.meida.bpm.provider.service.impl;

import com.meida.bpm.client.entity.FlowForm;
import com.meida.bpm.provider.mapper.FlowFormMapper;
import com.meida.bpm.provider.service.FlowFormService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.stereotype.Service;

/**
 * 流程Service
 *
 * @author zyf
 */
@Service
public class FlowFormServiceImpl extends BaseServiceImpl<FlowFormMapper, FlowForm> implements FlowFormService {
    @Override
    public ResultBody basePageList(CriteriaQuery<?> cq) {
        cq.eq(FlowForm.class, "formKey");
        cq.eq(FlowForm.class, "formName");
        return super.basePageList(cq);
    }

    @Override
    public FlowForm getByFormKey(String formKey) {
        CriteriaQuery cq = new CriteriaQuery(FlowForm.class);
        cq.eq(true, "formKey", formKey);
        return getOne(cq);
    }
}
