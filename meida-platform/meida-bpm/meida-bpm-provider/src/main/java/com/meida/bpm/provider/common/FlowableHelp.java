package com.meida.bpm.provider.common;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 流程辅助类
 *
 * @author zyf
 */
@Component
public class FlowableHelp {

    @Resource
    private TaskService taskService;

    /**
     * 根据流程实例ID查询任务参数
     *
     * @param processInstanceId
     * @return
     */
    public EntityMap getTaskInfo(String processInstanceId) {
        EntityMap result = new EntityMap();
        if (FlymeUtils.isNotEmpty(processInstanceId)) {
            //查询当前审批节点
            List<Task> task = taskService.createTaskQuery().processInstanceId(processInstanceId).active().list();
            if (FlymeUtils.isNotEmpty(task)) {
                result.put("taskName", task.get(0).getName().replace("\n", ""));
                result.put("taskId", task.get(0).getId());
                result.put("assignee", task.get(0).getAssignee());
                result.put("taskDefinitionKey", task.get(0).getTaskDefinitionKey());
            }
        }
        return result;
    }
}
