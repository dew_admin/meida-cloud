/**
 * Copyright 肇新智慧物业管理系统
 * <p>
 * Licensed under AGPL开源协议
 * <p>
 * gitee：https://gitee.com/fanhuibin1/zhaoxinpms
 * website：http://pms.zhaoxinms.com  wx： zhaoxinms
 */
package com.meida.bpm.provider.custom.entity;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

/**
 * 接口扩展属性
 *
 * @author zyf
 */
@Data
public class ChildAttr {
    /**
     * 是否开启审批
     */
    private Boolean approvalEnabled;
    /**
     * 审批类型(1:人工,2:自动,3:自动拒绝)
     */
    private String approvalMethod;
    /**
     * 审批方式(1:依次审批,2:多人会签(所有人都通过才到下一个环节),3:多人会签(通过只需一人,否决需全员),4:多人或签(一人通过或否决))
     */
    private String approvalMode;
    /**
     * 审批人与发起人为同一人时
     */
    private Integer sameMode;
    /**
     * 审批人为空时
     */
    private Integer noHander;

    /**
     * 调用子流程ID
     */
    private String calledElement;

    /**
     * 会签属性 循环数量
     */
    private String loopCardinality;

    /**
     * 是否串行
     */
    private Boolean isSequential;

    /**
     * 会签属性 集合
     */
    private String collection;

    /**
     * 会签属性 元素变量
     */
    private String elementVariable;

    /**
     * 会签属性 完成条件
     */
    private String completionCondition;

    /**
     * 服务任务 执行类型
     */
    private String expressionType;

    /**
     * 服务任务 执行类型值
     */
    private String expressionValue;

    /**
     * 服务任务 结果变量
     */
    private String resultVariable;

    /**
     * 服务任务 描述
     */
    private String description;

    /**
     * 脚本任务 脚本类型
     */
    private String scriptFormat;

    /**
     * 脚本任务 脚本内容
     */
    private String scriptContent;

    /**
     * 延迟执行时间
     */
    private String timeDate;

    /**
     * 执行周期
     */
    private String timeCycle;

    /**
     * 定时方式（timeDate:指定时间,timeDuration:指定定时器之前要等待多长时间,timeCycle:指定重复执行的间隔）
     */
    private String timeType;

    /**
     * 消息模板内容
     */
    private String templateContext;

    /**
     * 接收人
     */
    private String[] toUserIds;

    /**
     * 接收人姓名
     */
    private String[] toUserNames;

    /**
     * 消息类型
     */
    private String type;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 抄送人
     */
    private String[] copyToUserIds;
    /**
     * 抄送人姓名
     */
    private String[] copyToUserName;

    /**
     * 表单数据添加对象
     */
    private String formModel;

    /**
     * 工作表名称
     */
    private String formTableName;

    /**
     * 工作表编码
     */
    private String formTableCode;

    /**
     * 使用子表时对应的主表code
     */
    private String formTableMainCode;

    /**
     * 关联表编码
     */
    private String linkFormTableCode;

    /**
     * 关联字段
     */
    private String linkFormTableField;

    /**
     * 关联字段类型(1:关联记录 2:子表)
     */
    private Integer linkFormTableType;

    /**
     * 来源工作表关联字段类型(1:关联记录 2:子表)
     */
    private Integer formTableSourceType;

    /**
     * 标题表达式字段
     */
    private String titleField;

    /**
     * 工作表来源节点ID
     */
    private String formTableSourceTaskId;

    /**
     * 添加节点数据源来源工作表code
     */
    private String formTableSourceCode;

    /**
     * 工作表来源节点类型
     */
    private String formTableSourceNodeType;

    /**
     * 工作表来源节点获取数据方式
     */
    private Integer formTableSourceGetDataType;

    /**
     * 表单类型
     */
    private String formType;

    /**
     * 更新字段
     */
    private String updateFields;

    /**
     * 查询规则
     */
    private String searchFieldGroup;

    /**
     * 排序字段
     */
    private String sortField;

    /**
     * 排序方式
     */
    private String sortType;

    /**
     * 未获取到数据时
     */
    private Integer noDataType;

    /**
     * 获取数据方式
     */
    private Integer getDataType;

    /**
     * 节点获取数据方式
     */
    private Integer selectType;

    /**
     * 最大查询条数
     */
    private Long limitNum;

    /**
     * 忽略排序规则
     */
    private Boolean ignoreSortRule;

    /**
     * 流程启动方式
     */
    private String startType;

    /**
     * 开始执行时间
     */
    private String beginDate;

    /**
     * 结束执行时间
     */
    private String endDate;

    /**
     * 开始执行时间
     */
    private String beginDateStr;

    /**
     * 结束执行时间
     */
    private String endDateStr;
    /**
     * 流程启动触发方式(add_update,add,update,delete)
     */
    private String startEventType;
    /**
     * 触发条件
     */
    private String startCondition;

    /**
     * 触发字段
     */
    private String[] conditionFields;

    /**
     * 流程参数
     */
    private String updateVariable;

    /**
     * 添加节点添加数据类型
     */
    private Integer addDataType;

    /**
     * 分支条件
     */
    private List<ConditionGroup> conditionGroup;

    /**
     * 是否允许加签
     */
    private Boolean allowAddSign;

    /**
     * 信号事件名称
     */
    private String signalEventName;

    /**
     * 按日期类型触发字段
     */
    private String triggerField;

    /**
     * 日期执行方式(1:等于触发日期字段值 2:在之前 3:在之后)
     */
    private Integer executeType;

    /**
     * 执行时间前后差
     */
    private Integer plusDate;

    /**
     *  时间单位
     */
    private Integer plusDateUnit;

    /**
     *  重复周期
     */
    private Integer cycleType;

    /**
     * 定时 自定义 小时类型
     */
    private Integer hourType;

    /**
     * 定时 自定义 天或者星期类型
     */
    private Integer dayOrWeekType;

    /**
     * 定时 自定义 天选项值
     */
    private Integer dayValue;

    /**
     * 定时 自定义 星期选项值
     */
    private String[] weekValue;

    /**
     * 定时 自定义 月类型
     */
    private Integer monthType;

    /**
     * 定时 自定义 小时最小值
     */
    private Integer hourValueMin;


    /**
     * 定时 自定义 小时最大值
     */
    private Integer hourValueMax;

    /**
     * 定时 自定义 小时固定值
     */
    private Integer[] hourValues;


    /**
     * 定时 自定义 天最小值
     */
    private Integer dayValueMin;

    /**
     * 定时 自定义 小时最大值
     */
    private Integer dayValueMax;

    /**
     * 定时 自定义 天固定值
     */
    private Integer[] dayValues;

    /**
     * 定时 自定义 月最小值
     */
    private Integer monthValueMin;

    /**
     * 定时 自定义 月最大值
     */
    private Integer monthValueMax;

    /**
     * 定时 自定义 月固定值
     */
    private Integer[] monthValues;

    /**
     * 子流程工作表对象
     */
    private JSONObject subFormTableObject;

    /**
     * 是否是多实例
     */
    private Boolean isMulti;

}