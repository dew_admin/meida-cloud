package com.meida.bpm.provider.listener;

import com.meida.bpm.provider.handler.ProcessEndHandler;
import com.meida.common.base.utils.FlymeUtils;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 审批结束监听器
 *
 * @author zyf
 */
@Component
@Slf4j
public class FlowProcessEndListener implements ExecutionListener {


   @Autowired(required = false)
   private Map<String, ProcessEndHandler> processEndHandlerMap;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        if(FlymeUtils.isNotEmpty(processEndHandlerMap)){
            for(Map.Entry<String, ProcessEndHandler> entry : processEndHandlerMap.entrySet()){
                ProcessEndHandler processEndHandler= entry.getValue();
                if(processEndHandler.supports(delegateExecution)){
                    processEndHandler.notify(delegateExecution);
                }
            }
        }
    }

}
