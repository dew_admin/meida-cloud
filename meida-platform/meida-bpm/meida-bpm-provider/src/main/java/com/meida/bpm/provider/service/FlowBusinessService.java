package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowBusiness;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 流程业务数据 接口
 *
 * @author flyme
 * @date 2024-12-12
 */
public interface FlowBusinessService extends IBaseService<FlowBusiness> {

    void updateProcessInstanceId(String processInstanceId,Long flowBusId,Integer flowState);

    /**
     * 统计我的发起数量
     * @return
     */
    Long countMySend();

    /**
     * 逻辑删除
     * @param flowBusId
     */
    void logicDelete(Long flowBusId);

    /**
     * 更新催办状态
     * @param flowBusId
     * @param urging
     * @param taskDefinitionKey
     * @param handlerName
     * @return
     */
    Boolean setUrging(Long flowBusId,Integer urging,String taskDefinitionKey,String taskId,String handlerName) ;

    /**
     * 根据流程ID统计
     * @param processId
     * @return
     */
    Long countByProcessId(Long processId);
}
