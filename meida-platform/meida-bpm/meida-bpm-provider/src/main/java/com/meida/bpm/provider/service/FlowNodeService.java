package com.meida.bpm.provider.service;

import com.meida.bpm.client.entity.FlowProcessNode;
import com.meida.bpm.provider.model.AppRoverUserVo;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 流程节点表 接口
 *
 * @author flyme
 * @date 2024-12-15
 */
public interface FlowNodeService extends IBaseService<FlowProcessNode> {
     void saveProcessNode(Long processId, List<FlowProcessNode> processNodes);

     /**
      * 查询流程节点
      * @param processId
      * @param processInstanceId
      * @return
      */
     List<EntityMap> selectProcessNode(Long processId, String processInstanceId);

     AppRoverUserVo getAppRoverUser(String taskId, Long processId, String processNodeCode);
}
