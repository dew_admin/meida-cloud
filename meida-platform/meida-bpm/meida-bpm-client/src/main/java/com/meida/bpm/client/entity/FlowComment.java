package com.meida.bpm.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 流程评论表
 *
 * @author flyme
 * @date 2024-12-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_comment")
@TableAlias("comment")
@ApiModel(value="FlowComment对象", description="流程评论表")
public class FlowComment extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "commentId", type = IdType.ASSIGN_ID)
    private Long commentId;

    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    @ApiModelProperty(value = "流程ID")
    private Long processId;

    @ApiModelProperty(value = "业务ID")
    private Long flowBusId;

    @ApiModelProperty(value = "被评论ID")
    private Long replyCommentId;

    @ApiModelProperty(value = "被评论用户")
    private Long replyToUserId;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论人")
    private Long userId;

}
