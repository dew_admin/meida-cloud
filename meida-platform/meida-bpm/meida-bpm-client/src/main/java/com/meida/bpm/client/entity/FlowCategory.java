package com.meida.bpm.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 流程分类表
 *
 * @author flyme
 * @date 2020-06-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_category")
@TableAlias("category")
@ApiModel(value="FlowCategory对象", description="流程分类表")
public class FlowCategory extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "categoryId", type = IdType.ASSIGN_ID)
    private Long categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    /**
     * 分类编码
     */
    private String categoryCode;

    /**
     * 启动监听
     */
    private String endListener;

    /**
     * 结束监听
     */
    private String startListener;

    @ApiModelProperty(value = "企业ID")
    private Long comapnyId;

    @ApiModelProperty(value = "状态")
    private Integer status;

}
