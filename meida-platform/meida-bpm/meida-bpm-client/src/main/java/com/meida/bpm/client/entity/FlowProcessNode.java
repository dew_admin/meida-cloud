package com.meida.bpm.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 流程节点表
 *
 * @author flyme
 * @date 2024-12-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_node")
@TableAlias("node")
@ApiModel(value="FlowNode对象", description="流程节点表")
public class FlowProcessNode extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "flowNodeId", type = IdType.ASSIGN_ID)
    private Long flowNodeId;

    @ApiModelProperty(value = "流程ID")
    private Long processId;

    @ApiModelProperty(value = "节点编码")
    private String processNodeCode;

    @ApiModelProperty(value = "节点名称")
    private String processNodeName;

    @ApiModelProperty(value = "节点状态")
    private Integer nodeBpmStatus;

    @ApiModelProperty(value = "节点配置")
    private String nodeConfigJson;

    @ApiModelProperty(value = "审批配置")
    private String approveModel;

    @ApiModelProperty(value = "排序")
    private Integer sortNo;

}
