package com.meida.bpm.client.constants;

/**
 * @author: zyf
 * @date: 2019/3/12 11:22
 * @description:
 */
public class BpmConstants {
    /**
     * 服务名称
     */
    public static final String BPM_SERVICE = "com.meida.bpm.provider";


    public static final String TASK_OPERATE_KEY = "_OPT";
}
