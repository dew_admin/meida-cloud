package com.meida.bpm.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 流程表
 *
 * @author flyme
 * @date 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_process")
@TableAlias("process")
@ApiModel(value = "FlowProcess对象", description = "流程表")
public class FlowProcess extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "processId", type = IdType.ASSIGN_ID)
    private Long processId;


    @ApiModelProperty(value = "流程图标")
    private String processImg;

    @ApiModelProperty(value = "流程key")
    private String processKey;

    @ApiModelProperty(value = "流程名称")
    private String processName;

    @ApiModelProperty(value = "分类ID")
    private Long processTypeId;

    @ApiModelProperty(value = "流程状态")
    private Integer processState;

    @ApiModelProperty(value = "流程定义ID")
    private String procDefId;

    @ApiModelProperty(value = "流程模型ID")
    private String procModelId;

    @ApiModelProperty(value = "流程xml")
    private String processXml;

    @ApiModelProperty(value = "启动方式")
    private String startType;

    @ApiModelProperty(value = "流程json")
    private String processJson;

    @ApiModelProperty(value = "流程描述")
    private String description;

    @ApiModelProperty(value = "流程版本")
    private Integer versionTag;

    @ApiModelProperty(value = "note")
    private String note;

    @ApiModelProperty(value = "表单Id")
    private Long formId;

    @ApiModelProperty(value = "分类Id")
    private Long categoryId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    /**
     * 是否允许删除
     */
    private Integer allowDel;

    /**
     * 部署状态
     */
    private Integer deployState;

    /**
     *  自动去重(1:自动去重)
     */
    private Boolean duplicateRemoval;
    /**
     * 允许撤销(1:允许撤销)
     */
    private Boolean revokeTask;
    /**
     * 审批意见(1:必填)
     */
    private Boolean approvalMessage;


}
