package com.meida.bpm.client.entity;

import cn.hutool.db.DaoTemplate;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

import java.util.Date;

/**
 * 流程业务数据
 *
 * @author flyme
 * @date 2024-12-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_business")
@TableAlias("business")
@ApiModel(value="FlowBusiness对象", description="流程业务数据")
public class FlowBusiness extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "flowBusId", type = IdType.ASSIGN_ID)
    private Long flowBusId;

    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    @ApiModelProperty(value = "审批编号")
    private String flowNo;

    @ApiModelProperty(value = "发起人")
    private Long applyUserId;

    @ApiModelProperty(value = "审批部门")
    private Long deptId;

    @ApiModelProperty(value = "业务类型")
    private String busType;

    @ApiModelProperty(value = "申请人")
    private String appUserName;

    @ApiModelProperty(value = "分类1")
    private String flowBusType;


    @ApiModelProperty(value = "业务标题")
    private String busTitle;

    @ApiModelProperty(value = "业务内容")
    private String busContent;

    @ApiModelProperty(value = "业务数据ID")
    private String flowDataId;

    @ApiModelProperty(value = "流程参数")
    private String flowParams;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "流程状态")
    private Integer flowState;

    @ApiModelProperty(value = "流程ID")
    private Long flowProcessId;

    /**
     * 催办状态
     */
    private Integer urging;

    /**
     * 催办时间
     */
    private Date urgingDate;

    /**
     *  自动去重(1:自动去重)
     */
    private Boolean duplicateRemoval;
    /**
     * 允许撤销(1:允许撤销)
     */
    private Boolean revokeTask;
    /**
     * 审批意见(1:必填)
     */
    private Boolean approvalMessage;

    /**
     * 删除状态
     */
    private Integer deleted;

}
