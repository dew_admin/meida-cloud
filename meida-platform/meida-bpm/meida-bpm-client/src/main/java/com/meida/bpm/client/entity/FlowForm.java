package com.meida.bpm.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractAllEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author zyf
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("flow_form")
@TableAlias("form")
@ApiModel(value="FlowCategory对象", description="流程分类表")
public class FlowForm extends AbstractAllEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "formId", type = IdType.ASSIGN_ID)
    private String formId;

    @ApiModelProperty(value = "表单key")
    private String formKey;

    @ApiModelProperty(value = "表单名称")
    private String formName;

    @ApiModelProperty(value = "表单json")
    private String formJson;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;
}