package com.meida.bpm.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 工作流服务
 *
 * @author zyf
 */
@EnableFeignClients(basePackages = {"com.meida"})
@MapperScan(basePackages = "com.meida.**.mapper")
@EnableDiscoveryClient
@SpringBootApplication
public class BpmApplication {

    public static void main(String[] args) {
        SpringApplication.run(BpmApplication.class, args);
    }
}
