package com.meida.task.provider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meida.task.client.model.entity.TaskJobLogs;


/**
 * @author zyf
 */
public interface SchedulerJobLogsMapper extends BaseMapper<TaskJobLogs> {
}
