package com.meida.base.provider.listener;

import cn.hutool.core.bean.BeanUtil;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.module.admin.client.entity.GatewayAccessLogs;
import com.meida.module.admin.provider.mapper.GatewayLogsMapper;
import com.meida.module.admin.provider.service.IpRegionService;
import com.meida.mq.annotation.MsgListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.Map;

/**
 * mq消息接收者
 *
 * @author zyf
 */
@Configuration
@Slf4j
public class AccessLogsHandler {

    @Autowired
    private GatewayLogsMapper gatewayLogsMapper;

    /**
     * 临时存放减少io
     */
    @Autowired
    private IpRegionService ipRegionService;

    /**
     * 接收访问日志
     *
     * @param access
     */
    @MsgListener(queues = QueueConstants.QUEUE_ACCESS_LOGS)
    public void accessLogsQueue(@Payload Map access) {
        try {
            if (access != null) {
                GatewayAccessLogs logs = BeanUtil.mapToBean(access, GatewayAccessLogs.class, false);
                if (logs != null) {
                    String ip = logs.getIp();
                    if (FlymeUtils.isNotEmpty(ip)) {
                        if (!ip.equals("0:0:0:0:0:0:0:1")) {
                            logs.setRegion(ipRegionService.getRegion(logs.getIp()));
                        } else {
                            logs.setRegion("0:0:0:0:0:0:0:1");
                        }
                    }
                    logs.setUseTime(logs.getResponseTime().getTime() - logs.getRequestTime().getTime());
                    //Todo 关闭日志插入
                    //gatewayLogsMapper.insert(logs);
                }
            }
        } catch (Exception e) {
            log.error("error:", e);
        }
    }
}
