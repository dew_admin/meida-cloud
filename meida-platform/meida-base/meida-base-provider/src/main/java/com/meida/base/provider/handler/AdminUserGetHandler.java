package com.meida.base.provider.handler;

import cn.hutool.core.util.ObjectUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zyf
 */
@Component
@AllArgsConstructor
public class AdminUserGetHandler implements GetInterceptor {


    private final SysDeptService deptService;
    private final BaseRoleService roleService;

    @Override
    public void prepare(CriteriaQuery cq, EntityMap params) {

    }

    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long deptId = map.getLong("deptId");
        Long companyId = map.getLong("companyId");
        if (FlymeUtils.isNotEmpty(deptId)) {
            SysDept dept = deptService.getById(deptId);
            if (ObjectUtil.isNotNull(dept)) {
                map.put("deptName", dept.getDeptName());
            }
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            List<BaseRole> roleList = roleService.findByCompanyId(companyId);
            map.put("roleList", roleList);
        }
    }
}
