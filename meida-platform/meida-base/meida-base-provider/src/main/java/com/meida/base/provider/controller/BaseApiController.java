package com.meida.base.provider.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.admin.client.api.BaseApiRemoteApi;
import com.meida.module.admin.client.entity.BaseApi;
import com.meida.module.admin.client.entity.GatewayRoute;
import com.meida.module.admin.provider.service.BaseApiService;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.http.OpenRestTemplate;
import com.meida.module.admin.provider.service.GatewayRouteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */
@Api(tags = "接口管理")
@RestController
@RequestMapping("/api/")
public class BaseApiController extends BaseController<BaseApiService, BaseApi> {
    @Autowired
    private BaseApiService apiService;
    @Autowired
    private OpenRestTemplate openRestTemplate;

    /**
     * 获取分页接口列表
     *
     * @return
     */
    @ApiOperation(value = "接口-分页列表", notes = "接口-分页列表")
    @GetMapping(value = "page")
    public ResultBody<IPage<BaseApi>> getApiList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    /**
     * 获取所有接口列表
     *
     * @return
     */
    @ApiOperation(value = "接口-全部列表", notes = "获取所有接口列表")
    @GetMapping("all")
    public ResultBody<List<BaseApi>> getApiAllList(String serviceId) {
        return ResultBody.ok().data(apiService.findAllList(serviceId));
    }

    /**
     * 获取接口资源
     *
     * @param apiId
     * @return
     */
    @ApiOperation(value = "获取接口资源", notes = "获取接口资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", required = true, value = "ApiId", paramType = "path"),
    })
    @GetMapping("{apiId}/info")
    public ResultBody<BaseApi> getApi(@PathVariable("apiId") Long apiId) {
        return ResultBody.ok().data(apiService.getApi(apiId));
    }

    /**
     * 添加接口资源
     *
     * @param apiCode   接口编码
     * @param apiName   接口名称
     * @param serviceId 服务ID
     * @param path      请求路径
     * @param status    是否启用
     * @param priority  优先级越小越靠前
     * @param apiDesc   描述
     * @return
     */
    @ApiOperation(value = "添加接口资源", notes = "添加接口资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiCode", required = true, value = "接口编码", paramType = "form"),
            @ApiImplicitParam(name = "apiName", required = true, value = "接口名称", paramType = "form"),
            @ApiImplicitParam(name = "apiCategory", required = true, value = "接口分类", paramType = "form"),
            @ApiImplicitParam(name = "serviceId", required = true, value = "服务ID", paramType = "form"),
            @ApiImplicitParam(name = "path", required = false, value = "请求路径", paramType = "form"),
            @ApiImplicitParam(name = "status", required = true, defaultValue = "1", allowableValues = "0,1", value = "是否启用", paramType = "form"),
            @ApiImplicitParam(name = "priority", required = false, value = "优先级越小越靠前", paramType = "form"),
            @ApiImplicitParam(name = "apiDesc", required = false, value = "描述", paramType = "form"),
            @ApiImplicitParam(name = "isAuth", required = false, defaultValue = "0", allowableValues = "0,1", value = "是否身份认证", paramType = "form"),
            @ApiImplicitParam(name = "isOpen", required = false, defaultValue = "0", allowableValues = "0,1", value = "是否公开: 0-内部的 1-公开的", paramType = "form")
    })
    @PostMapping("add")
    public ResultBody<Long> addApi(
            @RequestParam(value = "apiCode") String apiCode,
            @RequestParam(value = "apiName") String apiName,
            @RequestParam(value = "apiCategory") String apiCategory,
            @RequestParam(value = "serviceId") String serviceId,
            @RequestParam(value = "path", required = false, defaultValue = "") String path,
            @RequestParam(value = "status", defaultValue = "1") Integer status,
            @RequestParam(value = "priority", required = false, defaultValue = "0") Integer priority,
            @RequestParam(value = "apiDesc", required = false, defaultValue = "") String apiDesc,
            @RequestParam(value = "isAuth", required = false, defaultValue = "1") Integer isAuth,
            @RequestParam(value = "isOpen", required = false, defaultValue = "0") Integer isOpen
    ) {
        BaseApi api = new BaseApi();
        api.setApiCode(apiCode);
        api.setApiName(apiName);
        api.setApiCategory(apiCategory);
        api.setServiceId(serviceId);
        api.setPath(path);
        api.setStatus(status);
        api.setPriority(priority);
        api.setApiDesc(apiDesc);
        api.setIsAuth(isAuth);
        api.setIsOpen(isOpen);
        Long apiId = null;
        apiService.addApi(api);
        openRestTemplate.refreshGateway();
        return ResultBody.ok().data(apiId);
    }

    /**
     * 编辑接口资源
     *
     * @param apiId     接口ID
     * @param apiCode   接口编码
     * @param apiName   接口名称
     * @param serviceId 服务ID
     * @param path      请求路径
     * @param status    是否启用
     * @param priority  优先级越小越靠前
     * @param apiDesc   描述
     * @return
     */
    @ApiOperation(value = "编辑接口资源", notes = "编辑接口资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", required = true, value = "接口Id", paramType = "form"),
            @ApiImplicitParam(name = "apiCode", required = true, value = "接口编码", paramType = "form"),
            @ApiImplicitParam(name = "apiName", required = true, value = "接口名称", paramType = "form"),
            @ApiImplicitParam(name = "apiCategory", required = true, value = "接口分类", paramType = "form"),
            @ApiImplicitParam(name = "serviceId", required = true, value = "服务ID", paramType = "form"),
            @ApiImplicitParam(name = "path", required = false, value = "请求路径", paramType = "form"),
            @ApiImplicitParam(name = "status", required = true, defaultValue = "1", allowableValues = "0,1", value = "是否启用", paramType = "form"),
            @ApiImplicitParam(name = "priority", required = false, value = "优先级越小越靠前", paramType = "form"),
            @ApiImplicitParam(name = "apiDesc", required = false, value = "描述", paramType = "form"),
            @ApiImplicitParam(name = "isAuth", required = false, defaultValue = "0", allowableValues = "0,1", value = "是否身份认证", paramType = "form"),
            @ApiImplicitParam(name = "isOpen", required = false, defaultValue = "0", allowableValues = "0,1", value = "是否公开: 0-内部的 1-公开的", paramType = "form")
    })
    @PostMapping("update")
    public ResultBody updateApi(
            @RequestParam("apiId") Long apiId,
            @RequestParam(value = "apiCode") String apiCode,
            @RequestParam(value = "apiName") String apiName,
            @RequestParam(value = "apiCategory") String apiCategory,
            @RequestParam(value = "serviceId") String serviceId,
            @RequestParam(value = "path", required = false, defaultValue = "") String path,
            @RequestParam(value = "status", defaultValue = "1") Integer status,
            @RequestParam(value = "priority", required = false, defaultValue = "0") Integer priority,
            @RequestParam(value = "apiDesc", required = false, defaultValue = "") String apiDesc,
            @RequestParam(value = "isAuth", required = false, defaultValue = "1") Integer isAuth,
            @RequestParam(value = "isOpen", required = false, defaultValue = "0") Integer isOpen
    ) {
        BaseApi api = new BaseApi();
        api.setApiId(apiId);
        api.setApiCode(apiCode);
        api.setApiName(apiName);
        api.setApiCategory(apiCategory);
        api.setServiceId(serviceId);
        api.setPath(path);
        api.setStatus(status);
        api.setPriority(priority);
        api.setApiDesc(apiDesc);
        api.setIsAuth(isAuth);
        api.setIsOpen(isOpen);
        apiService.updateApi(api);
        // 刷新网关
        openRestTemplate.refreshGateway();
        return ResultBody.ok();
    }


    /**
     * 移除接口资源
     *
     * @param apiId
     * @return
     */
    @ApiOperation(value = "移除接口资源", notes = "移除接口资源")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", required = true, value = "ApiId", paramType = "form"),
    })
    @PostMapping("remove")
    public ResultBody removeApi(
            @RequestParam("apiId") Long apiId
    ) {
        apiService.removeApi(apiId);
        // 刷新网关
        openRestTemplate.refreshGateway();
        return ResultBody.ok();
    }

    /**
     * 根据serviceId,path获取配置信息
     *
     * @param serviceId
     * @param path
     * @return
     */
    @ApiOperation(value = "根据serviceId_path获取配置信息", notes = "根据serviceId_path获取配置信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "serviceId", required = true, value = "serviceId", paramType = "form"),
            @ApiImplicitParam(name = "path", required = true, value = "path", paramType = "form")
    })
    @PostMapping("infoByServiceIdAndPath")
    public ResultBody<BaseApi> getApiInfo(@RequestParam(value = "serviceId") String serviceId,
                                          @RequestParam(value = "path") String path) {
        //TODO 是否在这一块处理异常
        List<BaseApi> list = apiService.findByServiceIdAndPath(serviceId, path);
        if (list == null || list.size() > 1) {
            //TODO 日志在哪记录
            return ResultBody.failed("获取api配置信息为空或有多个结果");
        } else {
            return ResultBody.ok(list.get(0));
        }
    }

    /**
     * 批量删除数据
     *
     * @return
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form")
    })
    @PostMapping("batch/remove")
    public ResultBody batchRemove(
            @RequestParam(value = "ids") String ids
    ) {
        QueryWrapper<BaseApi> wrapper = new QueryWrapper();
        wrapper.lambda().in(BaseApi::getApiId, ids.split(",")).eq(BaseApi::getIsPersist, 0);
        apiService.remove(wrapper);
        // 刷新网关
        openRestTemplate.refreshGateway();
        return ResultBody.ok();
    }


    /**
     * 批量修改公开状态
     *
     * @return
     */
    @ApiOperation(value = "批量修改公开状态", notes = "批量修改公开状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form"),
            @ApiImplicitParam(name = "open", required = true, value = "是否公开访问:0-否 1-是", paramType = "form")
    })
    @PostMapping("batch/update/open")
    public ResultBody batchUpdateOpen(
            @RequestParam(value = "ids") String ids,
            @RequestParam(value = "open") Integer open
    ) {
        Assert.isTrue((open.intValue() != 1 || open.intValue() != 0), "isOpen只支持0,1");
        QueryWrapper<BaseApi> wrapper = new QueryWrapper();
        wrapper.lambda().in(BaseApi::getApiId, ids.split(","));
        BaseApi entity = new BaseApi();
        entity.setIsOpen(open);
        apiService.update(entity, wrapper);
        // 刷新网关
        openRestTemplate.refreshGateway();
        return ResultBody.ok();
    }

    /**
     * 批量修改状态
     *
     * @return
     */
    @ApiOperation(value = "批量修改状态", notes = "批量修改状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form"),
            @ApiImplicitParam(name = "status", required = true, value = "接口状态:0-禁用 1-启用", paramType = "form")
    })
    @PostMapping("batch/update/status")
    public ResultBody batchUpdateStatus(
            @RequestParam(value = "ids") String ids,
            @RequestParam(value = "status") Integer status
    ) {
        Assert.isTrue((status.intValue() != 0 || status.intValue() != 1 || status.intValue() != 2), "status只支持0,1,2");
        QueryWrapper<BaseApi> wrapper = new QueryWrapper();
        wrapper.lambda().in(BaseApi::getApiId, ids.split(","));
        BaseApi entity = new BaseApi();
        entity.setStatus(status);
        apiService.update(entity, wrapper);
        // 刷新网关
        openRestTemplate.refreshGateway();
        return ResultBody.ok();
    }


    @ApiOperation(value = "接口-认证状态设置", notes = "设置认证状态")
    @PostMapping(value = "setAuthState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "isAuth", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setAuthState(@RequestParam(required = false) Map map, @RequestParam(value = "isAuth") Integer isAuth) {
        return bizService.setState(map, "isAuth", isAuth);
    }

}
