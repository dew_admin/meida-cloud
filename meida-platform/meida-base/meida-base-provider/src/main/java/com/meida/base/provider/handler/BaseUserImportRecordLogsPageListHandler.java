package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.admin.client.entity.BaseUserImportRecordLogs;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import org.springframework.stereotype.Component;

/**
 * 用户列表扩展(加入机构ID)
 *
 * @author zyf
 */
@Component
public class BaseUserImportRecordLogsPageListHandler implements PageInterceptor<BaseUserImportRecordLogs> {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.eq("importId", cq.getParams("importId"));
        cq.select(BaseUserImportRecordLogs.class, "*");
        cq.select(SysCompany.class, "companyName");
        cq.select(SysDept.class, "deptName");
        cq.createJoin(SysCompany.class);
        cq.createJoin(SysDept.class);

    }
}
