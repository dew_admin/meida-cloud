package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.StringUtils;
import com.meida.module.admin.client.constants.BaseConstants;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 后台用户添加扩展
 *
 * @author zyf
 */
@Component
public class BaseAdminUserAddHandler implements SaveInterceptor<BaseUser> {

    @Resource
    private SysCompanyService companyService;
    @Resource
    private BaseAccountService baseAccountService;
    @Resource
    private BaseUserService userService;

    @Resource
    private SysDeptService deptService;


    @Override
    public void prepare(CriteriaSave cs, EntityMap params, BaseUser baseUser) {
        Long companyId = baseUser.getCompanyId();
        //当不指定企业ID时获取当前用户所在的企业ID
        if (FlymeUtils.isEmpty(companyId)) {
            companyId = OpenHelper.getCompanyId();
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            SysCompany company = companyService.getById(companyId);
            if (FlymeUtils.isNotEmpty(company)) {
                baseUser.setOrganizationId(company.getOrganizationId());
            }
            baseUser.setCompanyId(companyId);
        }
        Long deptId = baseUser.getDeptId();
        if (FlymeUtils.isNotEmpty(deptId)) {
            SysDept dept = deptService.getById(deptId);
            baseUser.setCompanyId(dept.getCompanyId());
        }
    }

    @Override
    public void complete(CriteriaSave cs, EntityMap params, BaseUser baseUser) {
        Boolean mobileReg=FlymeUtils.getBoolean(baseUser.getMobileReg(),false);
        Boolean emailReg=FlymeUtils.getBoolean(baseUser.getEmailReg(),false);
        //默认注册用户名账户
        if (FlymeUtils.isNotEmpty(baseUser.getUserName()) && FlymeUtils.isEmpty(baseUser.getAccount())) {
            if (!baseUser.getUserName().equals(baseUser.getMobile())) {
                baseAccountService.register(baseUser.getUserId(), baseUser.getUserName(), baseUser.getPassword(), BaseConstants.USER_ACCOUNT_TYPE_USERNAME, baseUser.getStatus(), BaseConstants.ACCOUNT_DOMAIN_ADMIN, null);
            }
        }
        if (FlymeUtils.isNotEmpty(baseUser.getAccount())) {
            if (!baseUser.getAccount().equals(baseUser.getMobile())) {
                baseAccountService.register(baseUser.getUserId(), baseUser.getAccount(), baseUser.getPassword(), BaseConstants.USER_ACCOUNT_TYPE_USERNAME, baseUser.getStatus(), BaseConstants.ACCOUNT_DOMAIN_ADMIN, null);
            }
        }
        if (StringUtils.matchEmail(baseUser.getEmail())&&emailReg) {
            //注册email账号登陆
            baseAccountService.register(baseUser.getUserId(), baseUser.getEmail(), baseUser.getPassword(), BaseConstants.USER_ACCOUNT_TYPE_EMAIL, baseUser.getStatus(), BaseConstants.ACCOUNT_DOMAIN_ADMIN, null);

        }
        if (StringUtils.matchMobile(baseUser.getMobile())&&mobileReg) {
            //注册手机号账号登陆
            baseAccountService.register(baseUser.getUserId(), baseUser.getMobile(), baseUser.getPassword(), BaseConstants.USER_ACCOUNT_TYPE_MOBILE, baseUser.getStatus(), BaseConstants.ACCOUNT_DOMAIN_ADMIN, null);
        }
        //注册角色
        if (StringUtils.isNotEmpty(baseUser.getRoleCode())) {
            userService.saveUserRole(baseUser.getUserId(), baseUser.getRoleCode());
        }
        Long companyId = baseUser.getCompanyId();
        Long deptId = baseUser.getDeptId();
        Long userId = baseUser.getUserId();
        if (FlymeUtils.isNotEmpty(companyId) && FlymeUtils.isEmpty(deptId)) {
            //当部门ID为空是
            userService.saveCompany(baseUser.getUserId(), companyId.toString());
        }
        if (FlymeUtils.isNotEmpty(deptId)) {
            //当部门ID不为空时
            SysDept sysDept = deptService.getById(deptId);
            if (FlymeUtils.isNotEmpty(sysDept)) {
                Long deptCompanyId = sysDept.getCompanyId();
                if (FlymeUtils.isNotEmpty(deptCompanyId)) {
                    userService.saveCompany(userId, deptCompanyId.toString());
                }
                userService.saveDept(userId, deptId.toString());
            }
        }
    }
}
