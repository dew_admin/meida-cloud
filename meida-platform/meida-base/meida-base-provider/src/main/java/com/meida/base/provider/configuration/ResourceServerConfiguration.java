package com.meida.base.provider.configuration;

import com.meida.base.provider.handler.LoginSuccesssHandler;
import com.meida.common.exception.OpenAccessDeniedHandler;
import com.meida.common.exception.OpenAuthenticationEntryPoint;
import com.meida.common.security.OpenHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * oauth2资源服务器配置
 * 如过新建一个资源服务器，直接复制该类到项目中.
 *
 * @author: zyf
 * @date: 2018/10/23 10:31
 * @description:
 */
@Slf4j
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    @Autowired
    private RedisConnectionFactory redisConnectionFactory;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private OAuth2WebSecurityExpressionHandler expressionHandler;
    @Autowired
    private RedisTokenStore redisTokenStore;

    @Resource
    private PermitAllSecurityConfig permitAllSecurityConfig;

    @Resource
    private LoginSuccesssHandler loginSuccesssHandler;

    private BearerTokenExtractor tokenExtractor = new BearerTokenExtractor();




    @Bean
    public RedisTokenStore redisTokenStore() {
        //TODO 适配国产化
        //RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
        //redisTokenStore.setSerializationStrategy(new FastjsonRedisTokenStoreSerializationStrategy());
        //return redisTokenStore;
        return new RedisTokenStore(redisConnectionFactory);
    }

    @Bean
    public JdbcClientDetailsService clientDetailsService() {
        JdbcClientDetailsService jdbcClientDetailsService = new JdbcClientDetailsService(dataSource);
        jdbcClientDetailsService.setPasswordEncoder(bCryptPasswordEncoder);
        return jdbcClientDetailsService;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        // 构建redis获取token,这里是为了支持自定义用户信息转换器
        resources.authenticationEntryPoint(new OpenAuthenticationEntryPoint()).accessDeniedHandler(new OpenAccessDeniedHandler());
        resources.tokenServices(OpenHelper.buildRedisTokenServices(redisConnectionFactory));
        resources.expressionHandler(expressionHandler);
    }

    @Bean
    public OAuth2WebSecurityExpressionHandler oAuth2WebSecurityExpressionHandler(ApplicationContext applicationContext) {
        expressionHandler = new OAuth2WebSecurityExpressionHandler();
        expressionHandler.setApplicationContext(applicationContext);
        return expressionHandler;
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .authorizeRequests()
                //只有超级管理员角色可执行远程端点
                // .requestMatchers(EndpointRequest.toAnyEndpoint()).hasAnyAuthority(CommonConstants.AUTHORITY_ACTUATOR)
                // 内部feign调用直接放行
                .antMatchers(
                        "/login/**",
                        "/logout/**",
                        "/oauth/**",
                        "/**/common/**",
                        "/**/websocket/**",
                        "/**/jmreport/**",
                        "/**/generate/**",
                        "/**/report/**",
                        "/**/ureport/**",
                        "/account/logs/add",
                        "/authority/access",
                        "/authority/app",
                        "/account/reset/password",
                        "/app/*/info",
                        "/base/**",
                        "/dict/initDict",
                        "/api/*/info",
                        "/app/client/*/info",
                        "/api/client/*/info",
                        "/user/info",
                        "/gateway/api/**",
                        "/**/bpm/designer/**",
                        "/designer",
                        "/api/infoByServiceIdAndPath"
                ).permitAll()
                .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
                .anyRequest().access("@baseAuthorizationManager.check(request,authentication)")
                .and()
                .formLogin().loginPage("/login").permitAll()
                .successHandler(loginSuccesssHandler)
                .and()
                .apply(permitAllSecurityConfig)
                .and()
                .logout().permitAll()
                // /logout退出清除cookie
                .addLogoutHandler(new CookieClearingLogoutHandler("token", "remember-me"))
                .logoutSuccessHandler(new LogoutSuccessHandler())
                .and()
                //认证鉴权错误处理,为了统一异常处理。每个资源服务器都应该加上。
                .exceptionHandling()
                .accessDeniedHandler(new OpenAccessDeniedHandler())
                .authenticationEntryPoint(new OpenAuthenticationEntryPoint())
                .and()
                .csrf().disable()
                // 禁用httpBasic
                .httpBasic().disable();
    }

    public class LogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
        public LogoutSuccessHandler() {
            // 重定向到原地址
            this.setUseReferer(true);
        }

        @Override
        public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
            try {
                // 解密请求头
                authentication = tokenExtractor.extract(request);
                if (authentication != null && authentication.getPrincipal() != null) {
                    String tokenValue = authentication.getPrincipal().toString();
                    log.debug("revokeToken tokenValue:{}", tokenValue);
                    // 移除token
                    redisTokenStore.removeAccessToken(redisTokenStore.readAccessToken(tokenValue));
                }
            } catch (Exception e) {
                log.error("revokeToken error:{}", e);
            }
            super.onLogoutSuccess(request, response, authentication);
        }
    }
}

