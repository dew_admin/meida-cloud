package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.admin.client.entity.BaseUserImportRecord;
import com.meida.module.system.client.entity.SysCompany;
import org.springframework.stereotype.Component;

/**
 * 用户列表扩展(加入机构ID)
 *
 * @author zyf
 */
@Component
public class BaseUserImportRecordPageListHandler implements PageInterceptor<BaseUserImportRecord> {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.select(BaseUserImportRecord.class, "*");
        cq.select(SysCompany.class, "companyName");
        cq.createJoin(SysCompany.class);

    }
}
