package com.meida.base.provider.exception;

import com.meida.common.exception.OpenException;

/**
 * 锁定时长
 *
 * @author
 */
public class LockTimeException extends OpenException {
    public LockTimeException(int code, String msg) {
        super(code, msg);
    }

}
