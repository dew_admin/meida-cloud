package com.meida.base.provider.controller;

import com.meida.module.admin.client.api.BaseUserAccountRemoteApi;
import com.meida.module.admin.client.model.UserAccount;
import com.meida.module.admin.client.entity.BaseAccount;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.common.mybatis.model.ResultBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 账号管理
 *
 * @author zyf
 */
@Slf4j
@Api(tags = "系统用户")
@RestController
public class BaseUserAccountController implements BaseUserAccountRemoteApi {
    @Autowired
    private BaseAccountService baseAccountService;

    /**
     * 获取后台登录账号信息
     *
     * @param username 登录名
     * @return
     */
    @ApiOperation(value = "获取账号登录信息", notes = "仅限系统内部调用")
    @ApiImplicitParams({@ApiImplicitParam(name = "username", required = true, value = "登录名", paramType = "path")})
    @PostMapping("/account/localLogin")
    @Override
    public ResultBody<UserAccount> localLogin(@RequestParam(value = "username") String username) {
        UserAccount account = baseAccountService.login(username);
        return ResultBody.ok(account);
    }


    /**
     * 注册第三方登录账号
     *
     * @param account
     * @param userhead
     * @param accountType
     * @return
     */
    @ApiOperation(value = "注册第三方登录账号", notes = "仅限系统内部调用")
    @PostMapping("/account/register/thirdParty")
    @Override
    public ResultBody<UserAccount> registerThirdPartyAccount(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "avatar") String avatar,
            @RequestParam(value = "accountType") String accountType,
            @RequestParam(value = "nickName") String nickName) {
        BaseAccount baseAccount = (BaseAccount) baseAccountService.register(username, avatar, accountType, nickName);
        return ResultBody.ok(baseAccount);
    }

    /**
     * 注册手机号账号
     *
     * @param accountName
     * @return
     */
    @ApiOperation(value = "注册手机号账号", notes = "仅限系统内部调用")
    @PostMapping("/account/register/mobile")
    @Override
    public ResultBody<UserAccount> registerByMobile(
            @RequestParam(value = "accountName") String accountName,
            @RequestParam(value = "password") String password
    ) {
        BaseAccount baseAccount = baseAccountService.registerByMobile(accountName, password);
        return ResultBody.ok(baseAccount);
    }


    /**
     * 重置密码
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @ApiOperation(value = "重置密码", notes = "重置密码")
    @PostMapping("/account/reset/password")
    @Override
    public ResultBody resetPassword(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "oldPassword") String oldPassword,
            @RequestParam(value = "newPassword") String newPassword
    ) {
        baseAccountService.resetPassword(userId, oldPassword, newPassword);
        return ResultBody.ok();
    }


}
