package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.module.ExportField;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.ExportInterceptor;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import com.meida.module.admin.provider.service.BaseUserAccountLogsService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 登录日志导出扩展
 *
 * @author zyf
 */
@Component("baseUserAccountLogsPageListHandler")
public class BaseUserAccountLogsPageListHandler implements PageInterceptor<BaseUserAccountLogs>, ExportInterceptor {

    @Resource
    private BaseUserAccountLogsService baseUserAccountLogsService;

    @Override
    public void prepare(CriteriaQuery<BaseUserAccountLogs> cq, PageParams pageParams, EntityMap params) {
        String loginBeginDate = cq.getParams("loginBeginDate");
        String loginEndDate = cq.getParams("loginEndDate");
        String account = cq.getParams("account");
        String loginIp = cq.getParams("loginIp");
        if(FlymeUtils.isNotEmpty(loginBeginDate)){
            cq.ge("loginTime",loginBeginDate);
        }
        if(FlymeUtils.isNotEmpty(loginEndDate)){
            cq.le("loginTime",loginEndDate);
        }
        if(FlymeUtils.isNotEmpty(account)){
            cq.eq("account",account);
        }
        if(FlymeUtils.isNotEmpty(loginIp)){
            cq.eq("loginIp",loginIp);
        }
        cq.select(BaseUserAccountLogs.class, "*");
        cq.select(BaseUser.class, "avatar", "nickName");
        cq.createJoin(BaseUser.class).setJoinField("userId").setMainField("userId");
        cq.orderByDesc("loginTime");

        String str = cq.getParams("str");
        if (FlymeUtils.isNotEmpty(str)) {
            cq.and(qw -> qw.like("account", str).or().like("loginIp", str).or().like("loginAgent", str)
                    .or().like("accountType", str).or().like("nickName", str).or().like("domain", str));

        }
    }


    @Override
    public void initExcelExportEntity(ExportField exportField, List list) {
        Object key = exportField.getKey();
        exportField.setWidth(20);
    }

    @Override
    public void complete(CriteriaQuery<BaseUserAccountLogs> cq, List<EntityMap> result, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        if (FlymeUtils.isNotEmpty(userId)) {
            //cq.setExportKey("loginLog:" + userId);
        }
    }
}
