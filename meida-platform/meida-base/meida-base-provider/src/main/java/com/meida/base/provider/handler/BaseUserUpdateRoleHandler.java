package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.UpdateInterceptor;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.event.FlymeEventClient;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.admin.provider.service.BaseUserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 后台用户更新角色扩展
 *
 * @author zyf
 */
@Component
public class BaseUserUpdateRoleHandler implements UpdateInterceptor<BaseUser> {

    @Resource
    private BaseRoleService baseRoleService;

    @Override
    public void complete(CriteriaUpdate cu, EntityMap params, BaseUser baseUser) {
        String roleCode=baseUser.getRoleCode();
        Long userId=baseUser.getUserId();
        if (FlymeUtils.isNotEmpty(roleCode)) {
            baseRoleService.saveUserRoles(userId,roleCode.split(","));
        }
    }
}
