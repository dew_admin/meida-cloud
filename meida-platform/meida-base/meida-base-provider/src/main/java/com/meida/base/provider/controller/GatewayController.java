package com.meida.base.provider.controller;

import com.meida.module.admin.client.api.GatewayRemoteApi;
import com.meida.module.admin.client.model.IpLimitApi;
import com.meida.module.admin.client.model.RateLimitApi;
import com.meida.module.admin.client.entity.GatewayRoute;
import com.meida.module.admin.provider.service.GatewayIpLimitService;
import com.meida.module.admin.provider.service.GatewayRateLimitService;
import com.meida.module.admin.provider.service.GatewayRouteService;
import com.meida.common.mybatis.model.ResultBody;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 网关接口
 *
 * @author: zyf
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关对外接口")
@RequestMapping("/gateway/")
@RestController
public class GatewayController implements GatewayRemoteApi {

    @Autowired
    private GatewayIpLimitService gatewayIpLimitService;
    @Autowired
    private GatewayRateLimitService gatewayRateLimitService;
    @Autowired
    private GatewayRouteService gatewayRouteService;

    /**
     * 获取接口黑名单列表
     *
     * @return
     */
    @GetMapping("api/blackList")
    @Override
    public ResultBody<List<IpLimitApi>> getApiBlackList() {
        return ResultBody.ok(gatewayIpLimitService.findBlackList());
    }

    /**
     * 获取接口白名单列表
     *
     * @return
     */
    @GetMapping("api/whiteList")
    @Override
    public ResultBody<List<IpLimitApi>> getApiWhiteList() {
        return ResultBody.ok(gatewayIpLimitService.findWhiteList());
    }

    /**
     * 获取限流列表
     *
     * @return
     */
    @GetMapping("api/rateLimit")
    @Override
    public ResultBody<List<RateLimitApi>> getApiRateLimitList() {
        return ResultBody.ok(gatewayRateLimitService.findRateLimitApiList());
    }

    /**
     * 获取路由列表
     *
     * @return
     */
    @GetMapping("api/route")
    @Override
    public ResultBody<List<GatewayRoute>> getApiRouteList() {
        return ResultBody.ok(gatewayRouteService.findRouteList());
    }
}
