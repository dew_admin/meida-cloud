package com.meida.base.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.service.SysCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户列表扩展(加入机构ID)
 *
 * @author Administrator
 */
@Component
public class AdminUserPageListHandler implements PageInterceptor<BaseUser> {
    @Autowired
    private SysCompanyService companyService;

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.select(BaseUser.class,"*");
        cq.select(SysOrganization.class,"organizationName");
        Long companyId = OpenHelper.getCompanyId();
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        if (CommonConstants.INT_1.equals(superAdmin)) {
            //如果是超级管理员
            cq.eq(BaseUser.class, "organizationId");
        } else {
            SysCompany company = companyService.getById(companyId);
            if (FlymeUtils.isNotEmpty(company)) {
                cq.eq(BaseUser.class, "organizationId", company.getOrganizationId());
            }

        }
        cq.createJoin(SysOrganization.class);

    }
}
