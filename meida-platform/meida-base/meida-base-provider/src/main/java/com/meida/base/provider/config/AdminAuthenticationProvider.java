package com.meida.base.provider.config;


import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.meida.base.provider.exception.LockTimeException;
import com.meida.base.provider.exception.NoResourcesException;
import com.meida.base.provider.service.UserDetailsServiceImpl;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.security.OpenUser;
import com.meida.common.utils.RedisUtils;
import com.meida.common.utils.WebUtils;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 实现自己的AuthenticationProvider类，用来自定义用户校验机制
 *
 * @author zyf
 * @date 2018/9/5
 */
@Component
public class AdminAuthenticationProvider implements AuthenticationProvider {

    @Resource
    private UserDetailsServiceImpl userDetailsService;

    @Resource
    private SysCompanyService companyService;
    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private RedisUtils redisUtils;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取表单输入中返回的用户名;
        String accountName = (String) authentication.getPrincipal();
        // 获取登录类型
        String accountType = WebUtils.getHttpServletRequest().getParameter("accountType");
        if (FlymeUtils.isEmpty(accountType)) {
            accountType = CommonConstants.AUTH_TYPE_PWD;
        }
        // 获取表单中输入的密码；
        String password = (String) authentication.getCredentials();
        String companyId = WebUtils.getHttpServletRequest().getParameter("companyId");
        // 查询用户
        OpenUser userInfo = (OpenUser) userDetailsService.loadUserByUsername(accountName);
        if (userInfo == null) {
            throw new BadCredentialsException("用户名不存在");
        }
        // 这里还可以加一些其他信息的判断，比如用户账号已停用等判断。
        Collection<? extends GrantedAuthority> authorities = userInfo.getAuthorities();
        if (FlymeUtils.isEmpty(authorities)) {
            throw new NoResourcesException();
        }
        if(accountType.equals(CommonConstants.AUTH_TYPE_PWD)) {
            //密码检测
            checkPassWord(accountName, password, userInfo);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            Map<String, Object> attrs = new HashMap<>();
            SysCompany company = companyService.getById(companyId);
            attrs.put("companyName", company.getCompanyName());
            Long organizationId = company.getOrganizationId();
            userInfo.setAttrs(attrs);
            userInfo.setOrganizationId(organizationId);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            userInfo.setCompanyId(Long.parseLong(companyId));
        }
        // 构建返回的用户登录成功的token
        return new UsernamePasswordAuthenticationToken(userInfo, password, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        //      这里直接改成retrun true;表示是支持这个执行
        return true;
    }


    private void checkPassWord(String accountName, String password, OpenUser userInfo) {
        // 这里我们还要判断密码是否正确，这里我们的密码使用BCryptPasswordEncoder进行加密的
        boolean loginErr = passwordEncoder.matches(password, userInfo.getPassword());

        //密码输错次数
        String errorCountKey = CommonConstants.ACCOUNT_LOCK_ERROR_COUNT + accountName;
        //锁定用户KEY
        String errorKey = CommonConstants.ACCOUNT_LOCK_ERROR_KEY + accountName;
        //检测用户是否锁定
        boolean hasErrorKey = redisUtils.hasKey(errorKey);
        if (FlymeUtils.isNotEmpty(hasErrorKey) && hasErrorKey == true) {
            throw new LockedException("User account is locked");
        }
        //当前错误次数
        int errorCount = redisUtils.getInteger(errorCountKey, 0).intValue();

        // 密码已锁定，记录时间
        String lockTime = CommonConstants.ACCOUNT_LOCK_ERROR_TIME + accountName;
        boolean hasLockTimeKey = redisUtils.hasKey(lockTime);
        if (hasLockTimeKey) {
            String lockTimeStr = redisUtils.get(lockTime).toString();
            int between = Integer.parseInt(DateUtil.between(DateUtil.parse(lockTimeStr), DateUtil.date(), DateUnit.MINUTE) + "");
            int min = 1;
            if (errorCount == 5 && between < 5) {
                min = 5 - between;
                throw new LockTimeException(3007, "账号已锁定，" + min + "分钟后重试");
            }
            if (errorCount == 5 && between > 5) {
                if (loginErr) {
                    redisUtils.del(lockTime);
                    redisUtils.del(errorCountKey);
                } else {
                    //记录错误次数
                    redisUtils.set(errorCountKey, 6, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    redisUtils.set(lockTime, DateUtil.date(), CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    throw new LockTimeException(3007, "账号已锁定，10分钟后重试");
                }
            }
            if (errorCount == 6 && (between < 10)) {
                min = 10 - between;
                throw new LockTimeException(3007, "账号已锁定，" + min + "分钟后重试");
            }
            if (errorCount == 6 && between > 10) {
                if (loginErr) {
                    redisUtils.del(lockTime);
                    redisUtils.del(errorCountKey);
                } else {
                    //记录错误次数
                    redisUtils.set(errorCountKey, 7, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);

                    redisUtils.set(lockTime, DateUtil.date(), CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    throw new LockTimeException(3007, "账号已锁定，30分钟后重试");
                }
            }
            if (errorCount == 7 && (between < 30)) {
                min = 30 - between;
                throw new LockTimeException(3007, "账号已锁定，" + min + "分钟后重试");

            }
            if (errorCount == 7 && between > 30) {
                if (loginErr) {
                    redisUtils.del(lockTime);
                    redisUtils.del(errorCountKey);
                } else {
                    redisUtils.set(errorCountKey, 8, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    redisUtils.set(errorKey, 8, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    throw new LockedException("User account is locked");
                }
            }

        }


        // 这里我们还要判断密码是否正确，这里我们的密码使用BCryptPasswordEncoder进行加密的
        if (!passwordEncoder.matches(password, userInfo.getPassword())) {
            errorCount += 1;
            //记录错误次数
            redisUtils.set(errorCountKey, errorCount, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
            if (errorCount >= CommonConstants.ACCOUNT_LOCK_ERROR_MAX_COUNT) {
                if (hasLockTimeKey) {
                   /* String lockTimeStr = redisUtils.get(lockTime).toString();
                    long between = DateUtil.between(DateUtil.parse(lockTimeStr), DateUtil.date(), DateUnit.MINUTE);
                    if (between >= 45) {
                        //锁定3小时
                        redisUtils.set(errorKey, errorCount + 1, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                    }*/
                    redisUtils.set(errorKey, errorCount + 1, CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                } else {
                    redisUtils.set(lockTime, DateUtil.date(), CommonConstants.ACCOUNT_LOCK_ERROR_LOCK_TIME);
                }
                //检测用户是否锁定
                hasErrorKey = redisUtils.hasKey(errorKey);
                if (FlymeUtils.isNotEmpty(hasErrorKey) && hasErrorKey == true) {
                    throw new LockedException("User account is locked");
                }
            }


            throw new BadCredentialsException("password error");
        }

        //当错误次数不超过限定次数时,登录成功清除错误次数,重新计数
        if (errorCount > 0) {
            redisUtils.del(errorCountKey);
            redisUtils.del(lockTime);
        }
    }
}
