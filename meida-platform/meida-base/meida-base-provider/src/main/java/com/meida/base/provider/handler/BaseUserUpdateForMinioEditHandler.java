package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.UpdateInterceptor;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.event.FlymeEventClient;
import com.meida.module.admin.client.entity.BaseUser;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 后台用户添加扩展
 *
 * @author zyf
 */
@Component
public class BaseUserUpdateForMinioEditHandler implements UpdateInterceptor<BaseUser> {

    @Resource
    private FlymeEventClient flymeEventClient;

    @Override
    public void complete(CriteriaUpdate cu, EntityMap params, BaseUser baseUser) {
        if (FlymeUtils.isNotEmpty(baseUser)) {
            EntityMap map = new EntityMap();
            map.put("userName", FlymeUtils.isNotEmpty(baseUser.getAccount()) ? baseUser.getAccount() : baseUser.getUserName());
            map.put("password", baseUser.getPassword());
            map.put("optType", "delete");
            flymeEventClient.publishEvent("minioAddUserListener", map);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.put("optType", "add");
            flymeEventClient.publishEvent("minioAddUserListener", map);
        }
    }
}
