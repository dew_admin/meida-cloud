package com.meida.base.provider.controller;

import cn.hutool.core.map.MapUtil;
import com.meida.common.annotation.Action;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.StringUtils;
import com.meida.module.admin.client.api.BaseUserRemoteApi;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.client.model.UserInfo;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.admin.provider.service.BaseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 系统用户信息
 *
 * @author zyf
 */
@Api(tags = "系统用户", value = "系统用户")
@RestController
@RequestMapping("/manager/")
public class BaseUserController implements BaseUserRemoteApi {
    @Autowired
    private BaseUserService baseUserService;
    @Autowired
    private BaseAccountService baseAccountService;
    @Autowired
    private BaseRoleService baseRoleService;

    /**
     * 系统分页用户列表
     *
     * @return
     */
    @ApiOperation(value = "系统用户分页列表", notes = "平台管理用户")
    @GetMapping("/user/page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return baseUserService.pageList(params);
    }

    @ApiOperation(value = "系统用户列表", notes = "平台管理用户")
    @GetMapping("/user/list")
    public ResultBody listEntity(@RequestParam(required = false) Map params) {
        return baseUserService.listEntityMap(params);
    }

    /**
     * 获取所有用户列表
     *
     * @return
     */
    @ApiOperation(value = "获取所有用户列表", notes = "获取所有用户列表")
    @GetMapping("/user/all")
    public ResultBody<List<BaseRole>> getUserAllList() {
        return ResultBody.ok(baseUserService.findAllList());
    }

    /**
     * 根据userType查询
     *
     * @return
     */
    @ApiOperation(value = "根据userType查询", notes = "根据userType查询")
    @GetMapping("/user/finByUserType")
    public ResultBody<List<BaseRole>> getUserAllList(@RequestParam(required = false) Integer userType) {
        return ResultBody.ok(baseUserService.findByUserType(userType));
    }

    /**
     * 添加系统用户
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "添加系统用户", notes = "添加系统用户")
    @PostMapping("/user/add")
    @Action(title = "添加")
    public ResultBody<Long> add(@RequestParam(required = false) Map params) {
        return baseUserService.add(params);
    }

    /**
     * 更新系统用户
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "更新系统用户", notes = "更新系统用户")
    @PostMapping("/user/update")
    @Action(title = "修改")
    public ResultBody updateUser(@RequestParam(required = false) Map params) {
        return baseUserService.edit(params);
    }

    @ApiOperation(value = "用户-详情", notes = "用户详情")
    @GetMapping(value = "/user/get")

    public ResultBody get(@RequestParam(required = false) Map params) {
        Long userId= MapUtil.getLong(params,"userId",OpenHelper.getUserId());
        params.put("userId",userId);
        return baseUserService.get(params);
    }

    /**
     * 修改用户密码
     *
     * @param userId
     * @param password
     * @return
     */
    @ApiOperation(value = "修改用户密码", notes = "修改用户密码")
    @PostMapping("/user/update/password")
    public ResultBody updatePassword(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "password") String password
    ) {
        baseAccountService.resetPassword(userId, password);
        return ResultBody.ok();
    }

    /**
     * 修改密码
     */
    @ApiOperation(value = "用户-根据旧密码修改密码", notes = "根据旧密码修改密码")
    @PostMapping("/user/resetPwdByOldPwd")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPassword", required = true, value = "旧密码", paramType = "form"),
            @ApiImplicitParam(name = "newPassword", required = true, value = "新密码", paramType = "form")
    })
    public ResultBody resetPwdByOldPwd(@RequestParam(value = "oldPassword") String oldPassword, @RequestParam(value = "newPassword") String newPassword
    ) {
        Long accountId = OpenHelper.getAccountId();
        baseAccountService.resetPwdByOldPwd(accountId, oldPassword, newPassword);
        return ResultBody.msg("修改成功");
    }

    /**
     * 用户分配角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    @ApiOperation(value = "用户分配角色", notes = "用户分配角色")
    @PostMapping("/user/setRole")
    public ResultBody addUserRoles(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "roleIds", required = false) String roleIds
    ) {
        baseRoleService.saveUserRoles(userId, StringUtils.isNotBlank(roleIds) ? roleIds.split(",") : new String[]{});
        return ResultBody.msg("设置成功");
    }


    /**
     * 用户分配机构部门
     *
     * @param userId
     * @param deptIds
     * @param companyIds
     * @return
     */
    @ApiOperation(value = "用户分配角色", notes = "用户分配角色")
    @PostMapping("/user/setCompanyAndDept")
    public ResultBody addUserRoles(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "deptIds", required = false) String deptIds,
            @RequestParam(value = "companyIds", required = false) String companyIds
    ) {
        baseUserService.saveDeptAndCompany(userId, deptIds, companyIds);
        return ResultBody.msg("分配成功");
    }

    @ApiOperation(value = "查询用户已分配的企业ids")
    @GetMapping(value = "selectCompanyIdsByUserId")
    public ResultBody selectCompanyIdsByUserId(@RequestParam Long userId) {
        List<Long> list = baseUserService.selectCompanyIdsByUserId(userId);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "查询用户已分配的部门ids")
    @GetMapping(value = "selectDeptIdsByUserId")
    public ResultBody selectDeptIdsByUserId(@RequestParam Long userId, @RequestParam Long companyId) {
        List<Long> list = baseUserService.selectDeptIdsByUserId(userId, companyId);
        return ResultBody.ok(list);
    }


    /**
     * 获取用户角色
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "获取用户已分配角色", notes = "获取用户已分配角色")
    @GetMapping("/user/roles")
    public ResultBody<List<BaseRole>> getUserRoles(
            @RequestParam(value = "userId") Long userId
    ) {
        return ResultBody.ok(baseRoleService.getUserRoles(userId));
    }

    /**
     * 更新用户状态
     */
    @ApiOperation(value = "用户-更新状态", notes = "更新用户状态")
    @PostMapping(value = "/user/setStatus")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody setStatus(@RequestParam(value = "userId") Long userId) {
        return baseUserService.setStatus(userId);
    }

    /**
     * 移除用户token
     */
    @ApiOperation(value = "用户-移除用户token", notes = "移除用户token")
    @PostMapping(value = "/user/removeToken")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody setOnline(@RequestParam(value = "userName") String userName) {
        return baseUserService.removeToken(userName);
    }

    /**
     * 设置用户在线状态
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "用户-设置用户在线状态", notes = "设置用户在线状态")
    @PostMapping(value = "/user/setOnLine")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody setOnLine(@RequestParam(value = "userId") Long userId) {
        return baseUserService.setOnLine(userId, CommonConstants.DISABLED);
    }

    /**
     * 解除用户锁定状态
     */
    @ApiOperation(value = "用户-解除用户锁定状态", notes = "解除用户锁定状态")
    @PostMapping(value = "/user/unLock")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody unLock(@RequestParam(value = "userId") Long userId) {
        return baseUserService.unLock(userId);
    }


    /**
     * 删除用户
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "用户-删除", notes = "删除用户")
    @PostMapping(value = "/user/remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return baseUserService.delete(params);
    }


    /**
     * 获取用户详细信息
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "获取用户详细信息", notes = "获取用户详细信息")
    @Override
    public ResultBody<UserInfo> getUserInfo(@RequestParam(value = "userId") Long userId) {
        return ResultBody.ok(baseUserService.getUserWithAuthoritiesById(userId));
    }


    /**
     * 用户分配企业
     *
     * @param userId
     * @param companyIds
     * @return
     */
    @ApiOperation(value = " 用户分配企业", notes = " 用户分配企业")
    @PostMapping("/user/setCompany")
    public ResultBody saveUserCompanys(@RequestParam(value = "userId") Long userId, @RequestParam(value = "companyIds", required = false) String companyIds) {
        baseUserService.saveUserCompanys(userId, StringUtils.isNotBlank(companyIds) ? companyIds.split(",") : new String[]{});
        return ResultBody.msg("分配成功");
    }

    /**
     * 用户分配部门
     *
     * @param userId
     * @param deptIds
     * @return
     */
    @ApiOperation(value = " 用户分配部门", notes = " 用户分配部门")
    @PostMapping("/user/setDept")
    public ResultBody setDept(@RequestParam(value = "userId") Long userId, @RequestParam(value = "deptIds", required = false) String deptIds) {
        baseUserService.saveUserDepts(userId, StringUtils.isNotBlank(deptIds) ? deptIds.split(",") : new String[]{});
        return ResultBody.msg("分配成功");
    }

    @ApiOperation(value = "获取分配部门列表")
    @GetMapping(value = "user/getAuthDeptList")
    public ResultBody getAuthDeptList(@RequestParam(value = "userId") Long userId, @RequestParam(value = "organizationId") Long organizationId) {
        return baseUserService.getAuthDeptList(userId, organizationId);
    }


    /**
     * 导入账号
     *
     * @param file
     * @return
     */
    @ApiOperation(value = "批量导入账号", notes = "导入账号")
    @PostMapping(value = "user/importFile")
    public ResultBody importFile(HttpServletRequest request, @RequestParam(value = "file") MultipartFile file) {
        return ResultBody.ok(baseUserService.importFile(request.getParameter("deptId"), file));
    }


    @ApiOperation(value = "用户-查找", notes = "用户查找")
    @GetMapping(value = "/user/findByUserIds")
    public ResultBody findByUserIds(@RequestParam(required = false) String userIds) {
        return ResultBody.ok(baseUserService.findByUserIds(userIds));
    }

    @ApiOperation(value = "用户-查找", notes = "用户查找")
    @GetMapping(value = "/user/find")
    public ResultBody find(@RequestParam(required = false) Map params) {
        return ResultBody.ok(baseUserService.find(params));
    }

    /**
     * 批量修改用户密码
     */
    @ApiOperation(value = "批量修改用户密码", notes = "批量修改用户密码")
    @PostMapping("/user/batchUpdate/password")
    public ResultBody updatePassword(
            @RequestParam(value = "userIds") String userIds,
            @RequestParam(value = "password") String password
    ) {
        baseAccountService.batchResetPassword(userIds, password);
        return ResultBody.ok();
    }

    /**
     * 用户批量分配角色
     */
    @ApiOperation(value = "批量分配用户角色", notes = "批量分配用户角色")
    @PostMapping("/user/batchUpdate/setRole")
    public ResultBody updateUserRoles(
            @RequestParam(value = "userIds") String userIds,
            @RequestParam(value = "roleIds", required = false) String roleIds
    ) {
        baseRoleService.updateUserRoles(userIds, StringUtils.isNotBlank(roleIds) ? roleIds.split(",") : new String[]{});
        return ResultBody.msg("设置成功");
    }

    /**
     * 删除用户
     *
     * @param userIds
     * @return
     */
    @ApiOperation(value = "用户-批量删除", notes = "批量删除用户")
    @PostMapping(value = "/user/batchRemove")
    public ResultBody batchRemove(@RequestParam(required = true) String userIds) {
        return baseUserService.batchRemove(userIds);
    }

    /**
     * 批量授权部门
     *
     * @return
     */
    @ApiOperation(value = "用户-批量授权部门", notes = "批量授权部门")
    @PostMapping(value = "/user/batchSaveDeptAndCompany")
    public ResultBody batchSaveDeptAndCompany(
            @RequestParam(value = "userIds") String userIds,
            @RequestParam(value = "deptIds", required = false) String deptIds,
            @RequestParam(value = "companyIds", required = false) String companyIds
    ) {
        baseUserService.batchSaveDeptAndCompany(userIds, deptIds, companyIds);
        return ResultBody.msg("分配成功");
    }

}

