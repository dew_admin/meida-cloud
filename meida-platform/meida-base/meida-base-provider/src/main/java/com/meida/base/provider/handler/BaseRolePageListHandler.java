package com.meida.base.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.service.SysCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户列表扩展(加入机构ID)
 *
 * @author zyf
 */
@Component
public class BaseRolePageListHandler implements PageInterceptor<BaseRole> {
    @Autowired
    private SysCompanyService companyService;

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.select(BaseRole.class, "*");
        cq.select(SysOrganization.class, "organizationName");
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        if (CommonConstants.INT_1.equals(superAdmin)) {
            //如果是超级管理员
            cq.eq(BaseRole.class, "organizationId");
        } else {
            Long organizationId = OpenHelper.getOrganizationId();
            if (FlymeUtils.isNotEmpty(organizationId)) {
                cq.eq(BaseRole.class, "organizationId", organizationId);
            }
        }
        cq.createJoin(SysOrganization.class);

    }
}
