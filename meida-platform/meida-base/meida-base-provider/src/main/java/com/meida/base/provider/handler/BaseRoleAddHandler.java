package com.meida.base.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 后台角色添加扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class BaseRoleAddHandler implements SaveInterceptor<BaseRole> {

    private final SysCompanyService companyService;

    @Override
    public void prepare(CriteriaSave cs, EntityMap params, BaseRole baseRole) {
        Long companyId = FlymeUtils.isEmpty(baseRole.getCompanyId()) ? OpenHelper.getCompanyId() : baseRole.getCompanyId();
        if (FlymeUtils.isNotEmpty(companyId)) {
            SysCompany company = companyService.getById(companyId);
            if (FlymeUtils.isNotEmpty(company)) {
                baseRole.setOrganizationId(company.getOrganizationId());
            }
            baseRole.setCompanyId(companyId);
        }
    }

}
