package com.meida.base.provider.exception;

import com.meida.common.exception.OpenException;

/**
 * 用户没有分配资源异常
 * @author zyf
 */
public class NoResourcesException extends OpenException {
    public NoResourcesException() {
        super(3006,"no_resources");
    }

}
