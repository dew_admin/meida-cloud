package com.meida.base.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.admin.client.entity.BaseUserImportRecord;
import com.meida.module.admin.provider.service.BaseUserImportRecordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 用户导入控制器
 *
 * @author flyme
 * @date 2023-06-29
 */
@RestController
@RequestMapping("/bus/buir/")
public class BaseUserImportRecordController extends BaseController<BaseUserImportRecordService, BaseUserImportRecord> {

    @ApiOperation(value = "用户导入-分页列表", notes = "用户导入分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "用户导入-列表", notes = "用户导入列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "用户导入-添加", notes = "添加用户导入")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "用户导入-更新", notes = "更新用户导入")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "用户导入-删除", notes = "删除用户导入")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "用户导入-详情", notes = "用户导入详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
