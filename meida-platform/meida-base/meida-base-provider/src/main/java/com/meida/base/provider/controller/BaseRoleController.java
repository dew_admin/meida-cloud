package com.meida.base.provider.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.client.entity.BaseRoleUser;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.common.mybatis.model.*;
import com.meida.common.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */
@Api(tags = "系统角色管理")
@RestController
@RequestMapping("/role/")
public class BaseRoleController {
    @Autowired
    private BaseRoleService baseRoleService;

    /**
     * 获取分页角色列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页角色列表", notes = "获取分页角色列表")
    @GetMapping("page")
    public ResultBody<IPage<BaseRole>> pageList(@RequestParam(required = false) Map params) {
        return baseRoleService.pageList(params);
    }

    /**
     * 获取所有角色列表
     *
     * @return
     */
    @ApiOperation(value = "获取所有角色列表", notes = "获取所有角色列表")
    @GetMapping("all")
    public ResultBody<List<BaseRole>> getRoleAllList() {
        return ResultBody.ok(baseRoleService.findAllList());
    }

    /**
     * 获取所有角色列表
     *
     * @return
     */
    @ApiOperation(value = "获取所有角色列表", notes = "获取所有角色列表")
    @GetMapping("listByType/{roleType}")
    public ResultBody<List<BaseRole>> listByType(@PathVariable String roleType) {
        return ResultBody.ok(baseRoleService.selectByType(roleType));
    }

    /**
     * 获取所有角色列表
     *
     * @return
     */
    @ApiOperation(value = "获取所有角色列表", notes = "获取所有角色列表")
    @GetMapping("listByNotInType/{roleType}")
    public ResultBody<List<BaseRole>> listByNotInType(@PathVariable String roleType) {
        return ResultBody.ok(baseRoleService.listByNotInType(roleType));
    }

    /**
     * 获取角色详情
     *
     * @param roleId
     * @return
     */
    @ApiOperation(value = "获取角色详情", notes = "获取角色详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId", value = "角色ID", defaultValue = "", required = true, paramType = "path")
    })
    @GetMapping("{roleId}/info")
    public ResultBody<BaseRole> getRole(@PathVariable(value = "roleId") Long roleId) {
        BaseRole result = baseRoleService.getRole(roleId);
        return ResultBody.ok(result);
    }

    /**
     * 添加角色
     *
     * @param params 角色
     * @return
     */
    @ApiOperation(value = "添加角色", notes = "添加角色")
    @PostMapping("add")
    public ResultBody addRole(@RequestParam(required = false) Map params) {
        return baseRoleService.add(params);
    }

    /**
     * 编辑角色
     *
     * @param model
     * @return
     */
    @ApiOperation(value = "编辑角色", notes = "编辑角色")
    @PostMapping("update")
    public ResultBody updateRole(@RequestParam(required = false) Map params) {
        return baseRoleService.edit(params);
    }


    /**
     * 删除角色
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "删除角色", notes = "删除角色")
    @PostMapping("remove")
    public ResultBody removeRole(@RequestParam(required = false) Map params) {
        baseRoleService.removeRole(params);
        return ResultBody.ok();
    }

    /**
     * 角色添加成员
     *
     * @param roleId
     * @param userIds
     * @return
     */
    @ApiOperation(value = "角色添加成员", notes = "角色添加成员")
    @PostMapping("users/add")
    public ResultBody addUserRoles(
            @RequestParam(value = "roleId") Long roleId,
            @RequestParam(value = "userIds", required = false) String userIds
    ) {
        baseRoleService.saveRoleUsers(roleId, StringUtils.isNotBlank(userIds) ? userIds.split(",") : new String[]{});
        return ResultBody.ok();
    }

    /**
     * 查询角色成员
     *
     * @param roleId
     * @return
     */
    @ApiOperation(value = "查询角色成员", notes = "查询角色成员")
    @GetMapping("users")
    public ResultBody<List<BaseRoleUser>> getRoleUsers(
            @RequestParam(value = "roleId") Long roleId
    ) {
        return ResultBody.ok(baseRoleService.findRoleUsers(roleId));
    }


    @ApiOperation(value = "获取用户角色列表")
    @GetMapping(value = "getAuthRoleList")
    public ResultBody getAuthRoleList(@RequestParam(value = "userId") Long userId, @RequestParam(value = "organizationId",required = false) Long organizationId,@RequestParam(value = "roleType",required = false) Integer roleType) {
        return baseRoleService.getAuthRoleList(userId, organizationId,roleType);
    }

    @ApiOperation(value = "角色-详情", notes = "角色详情")
    @GetMapping(value = "get")

    public ResultBody get(@RequestParam(required = false) Map params) {
        return baseRoleService.get(params);
    }

    /**
     * 更新角色状态
     */
    @ApiOperation(value = "角色-更新状态", notes = "更新角色状态")
    @PostMapping(value = "setStatus")
    @ApiImplicitParam(name = "roleId", required = true, value = "主键", paramType = "form")
    public ResultBody setStatus(@RequestParam(value = "roleId") Long roleId) {
        return baseRoleService.setStatus(roleId);
    }

}
