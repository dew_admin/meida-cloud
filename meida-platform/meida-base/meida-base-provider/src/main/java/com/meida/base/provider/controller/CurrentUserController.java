package com.meida.base.provider.controller;

import com.meida.module.admin.client.model.AuthorityMenu;
import com.meida.module.admin.provider.service.BaseAuthorityService;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author: zyf
 * @date: 2019/5/24 13:31
 * @description:
 */
@Api(tags = "系统用户")
@RestController
public class CurrentUserController {

    @Resource
    private BaseUserService baseUserService;
    @Resource
    private BaseAccountService baseAccountService;
    @Resource
    private BaseAuthorityService baseAuthorityService;

    /**
     * 修改当前登录用户密码
     *
     * @return
     */
    @ApiOperation(value = "修改当前登录用户密码", notes = "修改当前登录用户密码")
    @GetMapping("/current/user/rest/password")
    public ResultBody restPassword(@RequestParam(value = "oldPassword") String oldPassword,
                                   @RequestParam(value = "newPassword") String newPassword
    ) {
        baseAccountService.resetPassword(OpenHelper.getUser().getUserId(), oldPassword, newPassword);
        return ResultBody.ok();
    }

    /**
     * 修改当前登录用户基本信息
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "修改当前登录用户基本信息", notes = "修改当前登录用户基本信息")
    @PostMapping("/current/user/update")
    public ResultBody updateUserInfo(@RequestParam(required = false) Map params) {
        params.put("userId", OpenHelper.getUserId());
        return baseUserService.edit(params);
    }

    /**
     * 获取登陆用户已分配权限
     *
     * @return
     */
    @ApiOperation(value = "获取当前登录用户已分配菜单权限", notes = "获取当前登录用户已分配菜单权限")
    @GetMapping("/current/user/menu")
    public ResultBody<List<AuthorityMenu>> findAuthorityMenu(@RequestParam(required = false) Integer menuGroup) {
        List<AuthorityMenu> result = baseAuthorityService.findAuthorityMenuByUser(OpenHelper.getUser().getUserId(),menuGroup, CommonConstants.ROOT.equals(OpenHelper.getUser().getUsername()));
        return ResultBody.ok(result);
    }
}
