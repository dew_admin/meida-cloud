package com.meida.base.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import com.meida.module.admin.provider.service.BaseUserAccountLogsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 网关接口
 *
 * @author: zyf
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "登录日志")
@RequestMapping("/accout/loginLog/")
@RestController
public class BaseLoginLogController extends BaseController<BaseUserAccountLogsService, BaseUserAccountLogs> {

    /**
     * 获取分页接口列表
     *
     * @return
     */
    @ApiOperation(value = "接口-分页列表", notes = "接口-分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "日志-删除", notes = "日志删除")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @PostMapping(value = "export")
    public ResultBody baseExport(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        EntityMap entityMap = new EntityMap(params);
        String title = entityMap.get("title","登录日志");
        String handlerName = entityMap.get("handlerName","baseUserAccountLogsPageListHandler");
        String message= bizService.export(null, params, request, response, title, title, handlerName);
        return ResultBody.ok(message);
    }

}
