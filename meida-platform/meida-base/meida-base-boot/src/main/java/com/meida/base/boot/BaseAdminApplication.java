package com.meida.base.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


/**
 * 平台基础服务
 * 提供系统用户、权限分配、资源、客户端管理
 *
 * @author zyf
 */
@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.meida.**.feign"})
@SpringBootApplication(scanBasePackages = "com.meida")
@RemoteApplicationEventScan(basePackages = "com.meida")
@ComponentScan(basePackages = {"com.meida"})
public class BaseAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseAdminApplication.class, args);
    }
}
