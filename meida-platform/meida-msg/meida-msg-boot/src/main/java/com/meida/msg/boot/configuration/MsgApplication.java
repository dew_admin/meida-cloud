package com.meida.msg.boot.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


/**
 * @author zyf
 */
@EnableFeignClients(basePackages = {"com.meida"})
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.meida")
@MapperScan(basePackages = "com.meida.**.mapper")
public class MsgApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsgApplication.class, args);
    }

}

