/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 18:11:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_webhook_logs
-- ----------------------------
DROP TABLE IF EXISTS `msg_webhook_logs`;
CREATE TABLE `msg_webhook_logs`  (
  `msgId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `retryNums` decimal(8, 0) NOT NULL COMMENT '重试次数',
  `totalNums` decimal(8, 0) NULL DEFAULT NULL COMMENT '通知总次数',
  `delay` decimal(16, 0) NOT NULL COMMENT '延迟时间',
  `result` decimal(1, 0) NOT NULL COMMENT '通知结果',
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '通知地址',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通知类型',
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求数据',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`msgId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息通知-异步通知日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of msg_webhook_logs
-- ----------------------------
INSERT INTO `msg_webhook_logs` VALUES ('553642196767604736', 5, 6, 54000000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-03-11 11:38:12', '2019-04-01 18:00:19');
INSERT INTO `msg_webhook_logs` VALUES ('553642668677136384', 5, 6, 54000000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-03-11 11:38:12', '2019-04-01 18:00:19');
INSERT INTO `msg_webhook_logs` VALUES ('553643723964022784', 5, 6, 54000000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-03-11 11:38:12', '2019-04-01 18:00:19');
INSERT INTO `msg_webhook_logs` VALUES ('563382527314624512', 10, 11, 54000000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-04-04 15:21:02', '2019-05-17 08:13:20');
INSERT INTO `msg_webhook_logs` VALUES ('588689288300855296', 9, 10, 21600000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-06-13 11:21:04', '2019-07-17 12:56:46');
INSERT INTO `msg_webhook_logs` VALUES ('588690845230694400', 9, 10, 21600000, 0, 'http://www.baidu.com/notity/callback', 'order_pay', '{}', '2019-06-13 11:27:15', '2019-07-17 12:56:46');

SET FOREIGN_KEY_CHECKS = 1;
