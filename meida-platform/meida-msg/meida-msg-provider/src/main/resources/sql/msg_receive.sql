/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 18:02:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_receive
-- ----------------------------
DROP TABLE IF EXISTS `msg_receive`;
CREATE TABLE `msg_receive`  (
  `msgUserId` bigint(20) NOT NULL COMMENT '主键',
  `msgId` bigint(20) NULL DEFAULT NULL COMMENT '消息ID',
  `receiveUserId` bigint(20) NULL DEFAULT NULL COMMENT '接收人',
  `readState` int(1) NULL DEFAULT NULL COMMENT '读取状态',
  `deleted` int(1) NULL DEFAULT NULL COMMENT '删除状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`msgUserId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息接收表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
