/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 18:11:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_email_logs
-- ----------------------------
DROP TABLE IF EXISTS `msg_email_logs`;
CREATE TABLE `msg_email_logs`  (
  `logId` bigint(20) NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sendTo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sendCc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `attachments` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件路径',
  `sendNums` int(11) NULL DEFAULT NULL COMMENT '发送次数',
  `error` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误信息',
  `result` tinyint(3) NULL DEFAULT NULL COMMENT '0-失败 1-成功',
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送配置',
  `tplCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板编号',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`logId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮件发送日志' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
