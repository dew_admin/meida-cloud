/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 18:11:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_email_config
-- ----------------------------
DROP TABLE IF EXISTS `msg_email_config`;
CREATE TABLE `msg_email_config`  (
  `configId` bigint(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置名称',
  `smtpHost` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发件服务器域名',
  `smtpUsername` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发件服务器账户',
  `smtpPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发件服务器密码',
  `isPersist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  `isDefault` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否为默认 0-否 1-是 ',
  `createTime` datetime(0) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`configId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮件发送配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_email_config
-- ----------------------------
INSERT INTO `msg_email_config` VALUES (1, '默认配置', 'smtp.qq.com', '515608851@qq.com', 'nmyytnzwadukcahh', 1, 1, '2019-07-17 15:42:43', '2019-07-17 15:42:46');

SET FOREIGN_KEY_CHECKS = 1;
