/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 18:02:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_content
-- ----------------------------
DROP TABLE IF EXISTS `msg_content`;
CREATE TABLE `msg_content`  (
  `msgId` bigint(20) NOT NULL COMMENT '主键',
  `msgTypeId` bigint(20) NULL DEFAULT NULL COMMENT '消息类型',
  `msgTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息标题',
  `msgImage` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息图片',
  `msgContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息内容',
  `msgState` int(1) NULL DEFAULT NULL COMMENT '消息状态',
  `msgSummary` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `pushState` int(1) NULL DEFAULT NULL COMMENT '推送状态',
  `pushDeviceType` int(1) NULL DEFAULT NULL COMMENT '推送设备类型',
  `sendScope` int(1) NULL DEFAULT NULL COMMENT '发送范围',
  `receiveConfig` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '接收人员配置',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `deleted` int(1) NULL DEFAULT NULL COMMENT '删除标记',
  `sendType` int(1) NULL DEFAULT NULL COMMENT '发送方式',
  `sendTime` datetime(0) NULL DEFAULT NULL COMMENT '发送时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`msgId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息' ROW_FORMAT = Dynamic;


INSERT INTO `base_menu` VALUES (404, 400, 'message', '消息公告', '', '/', 'message/content/index', 'message', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2020-03-05 20:33:18', NULL);
INSERT INTO `base_authority` VALUES (404, 'MENU_message', 404, NULL, NULL, 1, '2020-03-05 20:33:18', NULL);

SET FOREIGN_KEY_CHECKS = 1;
