
package com.meida.msg.provider.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Mq配置
 * @author zyf
 */
@Configuration
public class RabbitConfiguration {
    /**
     * http异步通知队列
     */
    public final static  String HTTP_NOTIFY_QUEUE = "meida.notify.http.queue";

    /**
     * http异步通知队列路由key
     */
    public final static  String HTTP_NOTIFY_QUEUE_RK = "meida.notify.http.queue.rk";



    /**
     * HTTP通知队列
     *
     * @return
     */
    @Bean
    public Queue httpNotifyQueue() {
        return new Queue(HTTP_NOTIFY_QUEUE, true);
    }

 /*   *//**
     * HTTP通知队列绑定延迟交换器
     *
     * @return
     *//*
    @Bean
    public Binding httpNotifyQueueBinding(Queue httpNotifyQueue, Exchange delayExchange) {
        return BindingBuilder.bind(httpNotifyQueue).to(delayExchange).with(HTTP_NOTIFY_QUEUE_RK).noargs();
    }*/
}
