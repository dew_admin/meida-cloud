package com.meida.msg.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.msg.client.entity.MsgType;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 消息类型 接口
 *
 * @author flyme
 * @date 2020-02-27
 */
public interface MsgTypeService extends IBaseService<MsgType> {

    MsgType findByTypeCode(String typeCode);


    List<EntityMap> selectMsgTypeList(Integer typeState);
}
