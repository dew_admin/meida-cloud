package com.meida.msg.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.msg.client.entity.MsgType;
import com.meida.msg.provider.mapper.MsgTypeMapper;
import com.meida.msg.provider.service.MsgTypeService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 消息类型接口实现类
 *
 * @author flyme
 * @date 2020-02-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MsgTypeServiceImpl extends BaseServiceImpl<MsgTypeMapper, MsgType> implements MsgTypeService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, MsgType type, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<MsgType> cq, MsgType type, EntityMap requestMap) {
        cq.eq(MsgType.class, "typeName");
        cq.and(i -> i.eq("typeCode", "SystemMsg").or(j -> j.eq("typeCode", "SystemMaintenance")));
        cq.orderByDesc("type.createTime");
        return ResultBody.ok();
    }

    @Override
    public MsgType findByTypeCode(String typeCode) {
        CriteriaQuery cq = new CriteriaQuery(MsgType.class);
        cq.eq("typeCode", typeCode);
        return getOne(cq);
    }

    @Override
    public List<EntityMap> selectMsgTypeList(Integer typeState) {
        CriteriaQuery cq = new CriteriaQuery(MsgType.class);
        cq.eq("typeState", typeState);
        return selectEntityMap(cq);
    }
}
