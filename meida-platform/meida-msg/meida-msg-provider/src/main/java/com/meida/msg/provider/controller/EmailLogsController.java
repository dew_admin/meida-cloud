package com.meida.msg.provider.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.msg.client.entity.EmailLogs;
import com.meida.msg.provider.service.EmailLogsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 邮件发送日志 前端控制器
 *
 * @author admin
 * @date 2019-07-25
 */
@Api(tags = "消息服务")
    @RestController
@RequestMapping("/emailLogs")
    public class EmailLogsController {

    @Autowired
    private EmailLogsService targetService;

    /**
     * 获取分页数据
     *
     * @return
     */
    @ApiOperation(value = "获取分页数据", notes = "获取分页数据")
    @GetMapping(value = "/list")
    public ResultBody<IPage<EmailLogs>> list(@RequestParam(required = false) Map params) {
        PageParams pageParams = new PageParams(params);
        EmailLogs query = pageParams.mapToObject(EmailLogs.class);
        QueryWrapper<EmailLogs> queryWrapper = new QueryWrapper();
        return ResultBody.ok().data(targetService.page(new PageParams(params), queryWrapper));
    }

    /**
     * 根据ID查找数据
     */
    @ApiOperation(value = "根据ID查找数据", notes = "根据ID查找数据")
    @ResponseBody
    @GetMapping("/get")
    public ResultBody<EmailLogs> get(@RequestParam("id") Long id) {
        EmailLogs entity = targetService.getById(id);
        return ResultBody.ok().data(entity);
    }

}