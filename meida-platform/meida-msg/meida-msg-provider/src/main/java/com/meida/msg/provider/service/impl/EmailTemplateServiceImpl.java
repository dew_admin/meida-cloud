package com.meida.msg.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.msg.client.entity.EmailTemplate;
import com.meida.msg.provider.mapper.EmailTemplateMapper;
import com.meida.msg.provider.service.EmailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 邮件模板配置 服务实现类
 *
 * @author zyf
 * @date 2019-07-17
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EmailTemplateServiceImpl extends BaseServiceImpl<EmailTemplateMapper, EmailTemplate> implements EmailTemplateService {
    @Autowired
    private EmailTemplateMapper emailTemplateMapper;

    /**
     * 根据模板编号获取模板
     *
     * @param code
     * @return
     */
    @Override
    public EmailTemplate getByCode(String code) {
        QueryWrapper<EmailTemplate> queryWrapper = new QueryWrapper();
        queryWrapper.eq("code", code);
        return emailTemplateMapper.selectOne(queryWrapper);
    }
}
