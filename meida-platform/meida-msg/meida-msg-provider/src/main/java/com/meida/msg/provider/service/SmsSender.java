package com.meida.msg.provider.service;


import com.meida.msg.client.model.SmsMessage;

/**
 * @author woodev
 */
public interface SmsSender {

    /**
     * 发送短信
     *
     * @param parameter
     * @return
     */
    Boolean send(SmsMessage parameter);

    /**
     * 发送短信验证码
     *
     * @param parameter
     * @return
     */
    Boolean sendSmsCode(SmsMessage parameter);

    Boolean sendSms(SmsMessage parameter);
}
