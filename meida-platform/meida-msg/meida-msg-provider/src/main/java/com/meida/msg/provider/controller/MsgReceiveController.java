package com.meida.msg.provider.controller;

import com.meida.common.security.OpenHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.msg.client.entity.MsgReceive;
import com.meida.msg.provider.service.MsgReceiveService;


/**
 * 消息接收表控制器
 *
 * @author flyme
 * @date 2020-02-28
 */
@Api(tags = "消息通知")
@RestController
@RequestMapping("/msg/receive/")
public class MsgReceiveController extends BaseController<MsgReceiveService, MsgReceive> {

    @ApiOperation(value = "消息接收表-分页列表", notes = "消息接收表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "消息接收表-列表", notes = "消息接收表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "消息接收表-添加", notes = "添加消息接收表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "消息接收表-更新", notes = "更新消息接收表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "消息接收表-删除", notes = "删除消息接收表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "消息接收表-详情", notes = "消息接收表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "消息接收表-未读消息详情", notes = "消息接收表未读消息详情")
    @GetMapping(value = "getNoReadMessageInfo")
    public ResultBody getNoReadMessageInfo() {
        Long userId = OpenHelper.getUserId();
        return bizService.getNoReadMessageInfo(userId);
    }
    @ApiOperation(value = "消息接收表-全部未读消息", notes = "全部未读消息")
    @GetMapping(value = "getAllNoReadMessageNum")
    public ResultBody getAllNoReadMessageNum() {
        Long userId = OpenHelper.getUserId();
        return bizService.getAllNoReadMessageNum(userId);
    }

    @ApiOperation(value = "更新消息表", notes = "更新消息表")
    @PostMapping(value = "setReadState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "readState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setReadState(@RequestParam(required = false) Map map, @RequestParam(value = "readState") Integer readState) {
        return bizService.setState(map, "readState", readState);
    }

    @ApiOperation(value = "更新消息表", notes = "更新消息表")
    @PostMapping(value = "setDeleted")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "deleted", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setDeleted(@RequestParam(required = false) Map map, @RequestParam(value = "deleted") Integer deleted) {
        return bizService.setState(map, "deleted", deleted);
    }

}
