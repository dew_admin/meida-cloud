package com.meida.msg.provider.configuration;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.utils.RedisUtils;
import com.meida.msg.provider.service.SmsSender;
import com.meida.msg.provider.service.impl.AliyunSmsSenderImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zyf
 */
@Configuration
@Slf4j
public class SmsConfiguration {

    @Bean
    @ConditionalOnClass({AliyunSmsSenderImpl.class})
    public SmsSender smsSender(RedisUtils redisUtils) {
        AliyunSmsProperties aliyunSmsProperties = new AliyunSmsProperties();
        EntityMap map = redisUtils.getConfigMap("SMS_CONFIG");
        String accessKey = map.get("accessKey");
        String accessSecret = map.get("accessSecret");
        String signName = map.get("signName");
        String smsTplCode = map.get("smsTplCode");
        aliyunSmsProperties.setAccessKeyId(accessKey);
        aliyunSmsProperties.setAccessKeySecret(accessSecret);
        aliyunSmsProperties.setSignName(signName);
        aliyunSmsProperties.setSmsTplCode(smsTplCode);
        AliyunSmsSenderImpl sender = new AliyunSmsSenderImpl();
        sender.setSmsProperties(aliyunSmsProperties);
        return sender;
    }

}
