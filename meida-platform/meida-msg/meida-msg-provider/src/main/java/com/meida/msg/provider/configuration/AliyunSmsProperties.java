package com.meida.msg.provider.configuration;

import lombok.Data;

/**
 * @author woodev
 */
@Data
public class AliyunSmsProperties {
    /**
     * accessKeyId
     */
    private String accessKeyId;
    /**
     * accessKeySecret
     */
    private String accessKeySecret;

    /**
     * 签名
     *
     * @return
     */
    private String signName;


    /**
     * 验证码模板code
     */
    private String smsTplCode;


}
