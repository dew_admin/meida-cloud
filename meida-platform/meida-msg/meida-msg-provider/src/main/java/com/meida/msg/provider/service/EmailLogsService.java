package com.meida.msg.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.msg.client.entity.EmailLogs;

/**
 * 邮件发送日志 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface EmailLogsService extends IBaseService<EmailLogs> {

}
