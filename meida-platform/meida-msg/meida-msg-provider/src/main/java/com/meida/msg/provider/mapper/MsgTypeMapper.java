package com.meida.msg.provider.mapper;

import com.meida.msg.client.entity.MsgType;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 消息类型 Mapper 接口
 * @author flyme
 * @date 2020-02-27
 */
public interface MsgTypeMapper extends SuperMapper<MsgType> {

}
