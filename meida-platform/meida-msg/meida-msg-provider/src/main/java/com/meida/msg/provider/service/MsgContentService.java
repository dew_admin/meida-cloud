package com.meida.msg.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.msg.client.entity.MsgContent;

import java.util.List;
import java.util.Map;

/**
 * 消息表 接口
 *
 * @author flyme
 * @date 2020-02-26
 */
public interface MsgContentService extends IBaseService<MsgContent> {
    /**
     * 查询用户未接收的消息
     *
     * @param params
     * @return
     */
    List<MsgContent> selectNotReceiveByUserId(EntityMap params);


    ResultBody sendMessage(Map params);
}
