package com.meida.msg.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.msg.client.entity.MsgContent;


import java.util.List;

/**
 * 消息表 Mapper 接口
 *
 * @author flyme
 * @date 2020-02-26
 */
public interface MsgContentMapper extends SuperMapper<MsgContent> {
    /**
     * 查询用户未接收的消息
     * @param params
     * @return
     */
     List<MsgContent> selectNotReceiveByUserId(EntityMap params);
}
