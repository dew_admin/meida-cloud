package com.meida.msg.provider.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.meida.msg.client.entity.MsgReceive;
import com.meida.common.mybatis.base.mapper.SuperMapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 消息接收表 Mapper 接口
 * @author flyme
 * @date 2020-02-28
 */
public interface MsgReceiveMapper extends SuperMapper<MsgReceive> {

    @Select({"select ${ew.select} from ${ew.tableName} as ${ew.tableAlias} ${ew.joinSql} ${ew.customSqlSegment}"})
    Integer countNoReadMessage(@Param("ew") Wrapper<?> ew);

}
