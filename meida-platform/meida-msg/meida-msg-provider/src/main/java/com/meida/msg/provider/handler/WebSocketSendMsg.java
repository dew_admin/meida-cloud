package com.meida.msg.provider.handler;

import com.meida.common.security.OpenHelper;
import com.meida.module.admin.provider.mapper.BaseUserCompanyMapper;
import com.meida.msg.client.entity.MsgContent;
import com.meida.msg.provider.websocket.WebSocketServerEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("webSocketSendMsg")
public class WebSocketSendMsg implements SendMsgHandler {

    @Autowired
    private WebSocketServerEndpoint webSocketServerEndpoint;

    @Autowired
    private BaseUserCompanyMapper baseUserCompanyMapper;

    @Override
    public void sendMsg(MsgContent msgContent) {
        Long companyId = OpenHelper.getCompanyId();
        List<Long> userIds = baseUserCompanyMapper.selectUserIdByCompanyId(companyId);
        webSocketServerEndpoint.sendMessage(msgContent, userIds, 1);
    }
}
