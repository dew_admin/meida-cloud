package com.meida.msg.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.msg.client.entity.EmailTemplate;

/**
 * @author zyf
 */
public interface EmailTemplateMapper extends SuperMapper<EmailTemplate> {
}
