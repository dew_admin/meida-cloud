package com.meida.msg.provider.listener;

import com.alibaba.fastjson.util.TypeUtils;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.mq.annotation.MsgListener;
import com.meida.msg.provider.websocket.WebSocketServerEndpoint;
import com.meida.starter.rabbitmq.config.RabbitComponent;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

import javax.annotation.Resource;
import java.util.List;

/**
 * 消息推送
 *
 * @author zyf
 */
@RabbitComponent(value = "webSocketRabbitMqListener")
public class WebSocketRabbitMqListener {

    @Resource
    private WebSocketServerEndpoint webSocketServerEndpoint;

    @MsgListener(queues = QueueConstants.QUEUE_WEBSOCKET_MSG)
    public void onMessage(EntityMap map, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {

        List<Long> receiveUserIds = (List<Long>) map.get("receiveUserIds");
        Integer sendType = TypeUtils.castToInt(map.get("sendType"));
        if (FlymeUtils.isNotEmpty(receiveUserIds)) {
             webSocketServerEndpoint.pushMessage(receiveUserIds, sendType, map);
        }
    }

}
