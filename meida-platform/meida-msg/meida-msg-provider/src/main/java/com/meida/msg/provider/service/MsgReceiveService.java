package com.meida.msg.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.msg.client.entity.MsgReceive;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 消息接收表 接口
 *
 * @author flyme
 * @date 2020-02-28
 */
public interface MsgReceiveService extends IBaseService<MsgReceive> {
    /**
     * 删除用户消息
     *
     * @param msgIds
     * @return
     */
    Boolean deleteByMsgId(Long[] msgIds);

    /**
     * 查询用户收到的信息
     *
     * @param userId
     * @return
     */
    ResultBody getNoReadMessageInfo(Long userId);

    /**
     * 统计用户未读信息数量
     *
     * @param userId
     * @return
     */
    Long countNoRead(Long userId);

    /**
     * 根据类型获取用户未读信息
     *
     * @param userId
     * @param typeCode
     * @return
     */
    List<EntityMap> selectNoReadMessage(Long userId, String typeCode);

    /**
     * 根据类型获取统计用户未读信息
     *
     * @param userId
     * @param typeCode
     * @return
     */
    Integer countNoReadMessage(Long userId, String typeCode);

    /**
     * 根据类型获取统计用户未读信息
     *
     * @param userId
     * @param msgTypeId
     * @return
     */
    Integer countNoReadMessage(Long userId, Long msgTypeId);


    /**
     * 返回全部类型未读消息数量
     *
     * @param userId
     * @return
     */
    ResultBody getAllNoReadMessageNum(Long userId);


    /**
     * 发送消息通知
     *
     * @param typeCode      消息类型
     * @param receiveUserId 接收人
     * @param msgTitle      消息标题
     * @param content       消息内容
     * @return
     */
    MsgReceive sendMsgReceiveNotice(String typeCode, Long receiveUserId, String msgTitle, String content);
}
