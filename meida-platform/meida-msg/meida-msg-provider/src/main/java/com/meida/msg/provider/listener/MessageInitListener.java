package com.meida.msg.provider.listener;

import com.meida.msg.provider.service.EmailConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * @author: flyme
 * @date: 2019/2/12 16:24
 * @desc: 初始化消息配置
 */
@Slf4j
@Component
public class MessageInitListener implements ApplicationListener<ApplicationReadyEvent>, Ordered {

    @Autowired
    private EmailConfigService emailConfigService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        log.info("消息服务已加载...");
        if (applicationReadyEvent.getApplicationContext().getDisplayName().indexOf("AnnotationConfigServletWebServerApplicationContext") > -1) {
            emailConfigService.loadCacheConfig();
        }

    }

    @Override
    public int getOrder() {
        return 2;
    }
}
