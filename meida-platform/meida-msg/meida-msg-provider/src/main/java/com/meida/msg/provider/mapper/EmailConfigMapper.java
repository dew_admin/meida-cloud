package com.meida.msg.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.msg.client.entity.EmailConfig;

/**
 * @author zyf
 */
public interface EmailConfigMapper extends SuperMapper<EmailConfig> {
}
