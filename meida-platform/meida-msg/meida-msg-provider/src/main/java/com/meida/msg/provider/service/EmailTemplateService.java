package com.meida.msg.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.msg.client.entity.EmailTemplate;

/**
 * 邮件模板配置 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface EmailTemplateService extends IBaseService<EmailTemplate> {

    EmailTemplate getByCode(String code);
}
