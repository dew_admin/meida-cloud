package com.meida.msg.provider.handler;

import com.meida.msg.client.entity.MsgContent;

public interface SendMsgHandler {

    /**
     * 发送消息处理
     *
     * @return
     */
    default void sendMsg(MsgContent msgContent) {

    }
}
