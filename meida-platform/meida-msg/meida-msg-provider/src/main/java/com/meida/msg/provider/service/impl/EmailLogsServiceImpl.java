package com.meida.msg.provider.service.impl;


import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.msg.client.entity.EmailLogs;
import com.meida.msg.provider.mapper.EmailLogsMapper;
import com.meida.msg.provider.service.EmailLogsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 邮件发送日志 服务实现类
 *
 * @author zyf
 * @date 2019-07-17
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EmailLogsServiceImpl extends BaseServiceImpl<EmailLogsMapper, EmailLogs> implements EmailLogsService {

}
