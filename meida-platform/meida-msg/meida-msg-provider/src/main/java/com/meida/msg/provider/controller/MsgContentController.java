package com.meida.msg.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.msg.client.entity.MsgContent;
import com.meida.msg.provider.service.MsgContentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 消息表控制器
 *
 * @author flyme
 * @date 2020-02-26
 */
@RestController
@RequestMapping("/msg/content/")
public class MsgContentController extends BaseController<MsgContentService, MsgContent> {


    @ApiOperation(value = "消息表-分页列表", notes = "消息表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "消息表-列表", notes = "消息表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "消息表-添加", notes = "添加消息表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "消息表-更新", notes = "更新消息表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "消息表-删除", notes = "删除消息表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "消息表-详情", notes = "消息表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "消息表-发送公告", notes = "发送公告")
    @PostMapping(value = "sendMessage")
    public ResultBody sendMessage(@RequestParam(required = false) Map params) {
        return bizService.sendMessage(params);
    }

    @ApiOperation(value = "更新消息表", notes = "更新消息表")
    @PostMapping(value = "setMsgState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "msgState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setMsgState(@RequestParam(required = false) Map map, @RequestParam(value = "msgState") Integer msgState) {
        return bizService.setState(map, "msgState", msgState);
    }

}
