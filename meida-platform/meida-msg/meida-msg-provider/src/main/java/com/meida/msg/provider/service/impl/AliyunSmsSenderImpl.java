package com.meida.msg.provider.service.impl;


import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.meida.msg.client.model.SmsMessage;
import com.meida.msg.provider.configuration.AliyunSmsProperties;
import com.meida.msg.provider.service.SmsSender;
import lombok.extern.slf4j.Slf4j;


/**
 * @author woodev
 */
@Slf4j
public class AliyunSmsSenderImpl implements SmsSender {


    private AliyunSmsProperties smsProperties;

    private final static String OK = "OK";

    private final static String CODE = "Code";

    public AliyunSmsSenderImpl() {
        log.info("初始化阿里云短信:" + this);
    }

    @Override
    public Boolean send(SmsMessage parameter) {
        boolean result = false;
        try {
            // 地域ID
            DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", smsProperties.getAccessKeyId(), smsProperties.getAccessKeySecret());
            IAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.setSysMethod(MethodType.POST);
            request.setSysDomain("dysmsapi.aliyuncs.com");
            request.setSysVersion("2017-05-25");
            request.setSysAction("SendSms");
            request.putQueryParameter("RegionId", "cn-hangzhou");
            request.putQueryParameter("PhoneNumbers", parameter.getPhoneNum());
            request.putQueryParameter("SignName", parameter.getSignName());
            request.putQueryParameter("TemplateCode", parameter.getTplCode());
            String params = JSONUtil.toJsonStr(parameter.getTplParams());
            request.putQueryParameter("TemplateParam", params);
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.toString());
            JSONObject json = JSONObject.parseObject(response.getData());
            result = OK.equalsIgnoreCase(json.getString(CODE));
            log.info("result:{}", response.getData());
        } catch (Exception e) {
            log.error("发送短信失败：{}", e.getMessage(), e);
        }
        return result;
    }

    @Override
    public Boolean sendSmsCode(SmsMessage parameter) {
        parameter.setSignName(smsProperties.getSignName());
        parameter.setTplCode(smsProperties.getSmsTplCode());
        return send(parameter);
    }

    @Override
    public Boolean sendSms(SmsMessage parameter) {
        parameter.setSignName(smsProperties.getSignName());
        return send(parameter);
    }

    public AliyunSmsProperties getSmsProperties() {
        return smsProperties;
    }

    public void setSmsProperties(AliyunSmsProperties smsProperties) {
        this.smsProperties = smsProperties;
    }
}
