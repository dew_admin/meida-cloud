package com.meida.msg.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.msg.client.entity.EmailConfig;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class EmailConfigInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return EmailConfig.class;
    }
}
