package com.meida.msg.provider.handler;

import com.meida.common.base.handler.SmsSendHandler;
import com.meida.msg.client.model.SmsMessage;
import com.meida.msg.provider.service.SmsSender;
import com.meida.msg.provider.service.impl.AliyunSmsSenderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author flyme
 * @date 2019/8/21 15:38
 */
@Component("aliSmsHandler")
public class AliSmsHandler implements SmsSendHandler {

    @Autowired
    private SmsSender smsSender;

    @Override
    public void send(String mobile, Integer code, String areaCode) {
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum(mobile);
        smsNotify.add("code", code);
        smsSender.sendSmsCode(smsNotify);
    }

    @Override
    public void sendSms(String mobile, String tplCode, Map<String, Object> params) {
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum(mobile);
        smsNotify.setTplCode(tplCode);
        smsNotify.setTplParams(params);
        smsSender.sendSms(smsNotify);
    }

    public static void main(String[] args) {
        AliyunSmsSenderImpl aliyunSmsSender = new AliyunSmsSenderImpl();
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum("18739941307");
        smsNotify.setSignName("玩了么商城");
        smsNotify.setTplCode("SMS_174814144");
        smsNotify.add("code", "1254587");
        aliyunSmsSender.send(smsNotify);
    }
}
