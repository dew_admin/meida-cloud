package com.meida.msg.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.msg.client.entity.EmailLogs;

/**
 * @author zyf
 */
public interface EmailLogsMapper extends SuperMapper<EmailLogs> {
}
