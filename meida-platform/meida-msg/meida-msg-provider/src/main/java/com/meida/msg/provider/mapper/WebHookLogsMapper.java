package com.meida.msg.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meida.msg.client.entity.WebHookLogs;

/**
 * @author zyf
 */
public interface WebHookLogsMapper extends BaseMapper<WebHookLogs> {
}
