package com.meida.msg.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.msg.client.entity.MsgType;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class MsgTypeInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return MsgType.class;
    }
}
