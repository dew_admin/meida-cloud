package com.meida.msg.provider.exchanger;


import com.meida.msg.client.model.BaseMessage;

/**
 * @author woodev
 */

public interface MessageExchanger {

    boolean support(Object message);

    boolean exchange(BaseMessage message);
}
