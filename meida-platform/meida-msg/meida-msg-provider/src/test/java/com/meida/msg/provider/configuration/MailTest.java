package com.meida.msg.provider.configuration;


import com.meida.common.test.BaseTest;
import com.meida.msg.client.model.EmailMessage;
import com.meida.msg.provider.dispatcher.MessageDispatcher;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: zyf
 * @date: 2018/11/27 14:45
 * @description:
 */
public class MailTest extends BaseTest {
    @Autowired
    private MessageDispatcher dispatcher;

    @Test
    public void testMail() {
        EmailMessage message = new EmailMessage();
        message.setTo(new String[]{"574501933@qq.com"});
        message.setSubject("测试");
        message.setContent("测试内容");
        this.dispatcher.dispatch(message);
        try {
            Thread.sleep(50000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
