package com.meida.msg.provider.configuration;

import com.meida.common.test.BaseTest;
import com.meida.msg.client.model.SmsMessage;
import com.meida.msg.provider.dispatcher.MessageDispatcher;
import com.meida.msg.provider.service.impl.AliyunSmsSenderImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: zyf
 * @date: 2018/11/27 14:45
 * @description:
 */
public class SmsTest extends BaseTest {
    @Autowired
    private MessageDispatcher dispatcher;

    @Test
    public void testSms() {
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum("18510152531");
        smsNotify.setSignName("测试");
        smsNotify.setTplCode("测试内容");
        this.dispatcher.dispatch(smsNotify);
        try {
            Thread.sleep(50000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        AliyunSmsSenderImpl aliyunSmsSender = new AliyunSmsSenderImpl();
        SmsMessage smsNotify = new SmsMessage();
        smsNotify.setPhoneNum("18739941307");
        smsNotify.setSignName("目鹿");
        smsNotify.setTplCode("SMS_171859350");
        smsNotify.add("code", "1254587");
        aliyunSmsSender.send(smsNotify);
    }
}
