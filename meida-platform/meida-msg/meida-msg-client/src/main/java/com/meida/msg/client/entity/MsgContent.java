package com.meida.msg.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 消息表
 *
 * @author flyme
 * @date 2020-02-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("msg_content")
@TableAlias("content")
@ApiModel(value = "MsgContent对象", description = "消息表")
public class MsgContent extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "msgId", type = IdType.ASSIGN_ID)
    private Long msgId;

    @ApiModelProperty(value = "消息类型")
    private Long msgTypeId;

    @ApiModelProperty(value = "消息标题")
    private String msgTitle;

    @ApiModelProperty(value = "消息图片")
    private String msgImage;

    @ApiModelProperty(value = "消息内容")
    private String msgContent;

    @ApiModelProperty(value = "消息摘要")
    private String msgSummary;

    @ApiModelProperty(value = "消息描述")
    private String msgDescribe;

    @ApiModelProperty(value = "消息状态")
    private Integer msgState;

    @ApiModelProperty(value = "推送状态")
    private Boolean pushState;

    @ApiModelProperty(value = "推送设备类型")
    private Integer pushDeviceType;

    @ApiModelProperty(value = "发送范围")
    private Integer sendScope;

    @ApiModelProperty(value = "接收人员配置")
    private String receiveConfig;

    @ApiModelProperty(value = "创建人")
    private Long userId;

    @ApiModelProperty(value = "删除标记")
    private Integer deleted;

    @ApiModelProperty(value = "发送方式")
    private Integer sendType;

    @ApiModelProperty(value = "发送时间")
    private Date sendTime;

}
