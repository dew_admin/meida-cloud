package com.meida.msg.client.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * @author woodev
 */
@ApiModel("短信消息")
public class SmsMessage extends BaseMessage {

    private static final long serialVersionUID = -8924332753124953766L;

    @ApiModelProperty("手机号码")
    private String phoneNum;
    @ApiModelProperty("短信签名")
    private String signName;
    @ApiModelProperty("模板编号")
    private String tplCode;
    @ApiModelProperty("模板参数")
    private Map<String, Object> tplParams = new HashMap<>();

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTplCode() {
        return tplCode;
    }

    public void setTplCode(String tplCode) {
        this.tplCode = tplCode;
    }

    public Map<String, Object> getTplParams() {
        return tplParams;
    }

    public void setTplParams(Map<String, Object> tplParams) {
        this.tplParams = tplParams;
    }

    public void add(String key, Object v) {
        tplParams.put(key, v);
    }
}
