package com.meida.msg.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 消息接收表
 *
 * @author flyme
 * @date 2020-02-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("msg_receive")
@TableAlias("receive")
@ApiModel(value = "MsgReceive对象", description = "消息接收表")
public class MsgReceive extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "msgUserId", type = IdType.ASSIGN_ID)
    private Long msgUserId;

    @ApiModelProperty(value = "消息ID")
    private Long msgId;

    @ApiModelProperty(value = "接收人")
    private Long receiveUserId;

    @ApiModelProperty(value = "读取状态")
    private Integer readState;

    @ApiModelProperty(value = "删除状态")
    private Integer deleted;

    @ApiModelProperty(value = "目标类型")
    private String targetClass;

    @ApiModelProperty(value = "目标id")
    private Long targetId;

}
