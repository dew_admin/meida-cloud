package com.meida.msg.client.constatns;

/**
 * @author: zyf
 * @date: 2019/3/12 11:24
 * @description:
 */
public class MsgConstants {
    /**
     * 服务名称
     */
    public static final String MSG_SERVICE = "meida-cloud-msg-server";
}
