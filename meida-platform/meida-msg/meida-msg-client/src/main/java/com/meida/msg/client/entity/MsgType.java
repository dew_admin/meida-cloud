package com.meida.msg.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 消息类型
 *
 * @author flyme
 * @date 2020-02-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("msg_type")
@TableAlias("type")
@ApiModel(value="MsgType对象", description="消息类型")
public class MsgType extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "msgTypeId", type = IdType.ASSIGN_ID)
    private Long msgTypeId;

    @ApiModelProperty(value = "类型名称")
    private String typeName;

    @ApiModelProperty(value = "类型编码")
    private String typeCode;

    @ApiModelProperty(value = "类型图标")
    private String typeIcon;

    @ApiModelProperty(value = "类型状态")
    private Integer typeState;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

}
