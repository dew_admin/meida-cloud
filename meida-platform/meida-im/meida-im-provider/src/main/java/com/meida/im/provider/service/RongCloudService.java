package com.meida.im.provider.service;

import com.meida.common.base.service.BaseImService;
import com.meida.common.base.utils.FlymeUtils;
import io.rong.RongCloud;
import io.rong.messages.BaseMessage;
import io.rong.messages.RcCmdMessage;
import io.rong.models.Result;
import io.rong.models.chatroom.ChatroomMember;
import io.rong.models.chatroom.ChatroomModel;
import io.rong.models.group.GroupMember;
import io.rong.models.group.GroupModel;
import io.rong.models.group.UserGroup;
import io.rong.models.message.*;
import io.rong.models.push.*;
import io.rong.models.response.*;
import io.rong.models.sensitiveword.SensitiveWordModel;
import io.rong.models.user.BatchTagModel;
import io.rong.models.user.GetTagModel;
import io.rong.models.user.TagModel;
import io.rong.models.user.UserModel;
import io.rong.util.GsonUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * 融云服务类
 */
@Component
public class RongCloudService implements BaseImService {

    @Resource
    private RongCloud rongCloud;

    /****广播消息撤回命令****/
    RcCmdMessage rcCmdMessage = new RcCmdMessage("BCVD-DV70-EKOC-7ES6");

    /***
     * 注册融云账户,返回token
     * @param id       id
     * @param name    名称
     * @param avatar   头像
     * @return {"code": 200,"errorMessage":"","token":"token","userId":"用户id"}
     * */
    @Override
    public String register(String id, String name, String avatar) {
        String token = "";
        UserModel user = new UserModel()
                .setId(id)
                .setName(name)
                .setPortrait(avatar);
        TokenResult result = null;
        try {
            result = rongCloud.user.register(user);
            if (FlymeUtils.isNotEmpty(result) && result.getCode().equals(200)) {
                token = result.getToken();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return token;
    }


    /***
     * 刷新用户
     * @param id       id
     * @param name    名称
     * @param avatar   头像
     * @return {"code": 200,"errorMessage":""}
     * */
    @Override
    public void userRefresh(String id, String name, String avatar) {
        String token = "";
        UserModel user = new UserModel()
                .setId(id)
                .setName(name)
                .setPortrait(avatar);
        Result result = null;
        try {
            result = rongCloud.user.update(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 获取用户信息
     * @param id       id
     * @return {"userName":'名称',"userPortrait":"头像","createTime":"创建时间"}
     * */
    public String getUserInfo(String id) throws Exception {
        UserModel user = new UserModel().setId(id);
        UserResult result = rongCloud.user.get(user);
        return result.toString();
    }

    /**
     * 用户封禁方法（每秒钟限 100 次）
     *
     * @param id id
     * @return {"code": 200,"errorMessage":""}
     */
    public String userAddBlock(String id) throws Exception {
        UserModel user = new UserModel()
                .setId(id)
                .setMinute(1000);
        Result result = (ResponseResult) rongCloud.user.block.add(user);
        return result.toString();
    }

    /**
     * 解除用户封禁方法（每秒钟限 100 次）
     *
     * @param id id
     * @return {"code": 200,"errorMessage":""}
     */
    public String userUnBlock(String id) throws Exception {
        UserModel user = new UserModel()
                .setId(id)
                .setMinute(1000);
        Result result = (ResponseResult) rongCloud.user.block.add(user);
        return result.toString();
    }

    /**
     * 获取被封禁用户方法（每秒钟限 100 次）
     *
     * @return {"code": 200,"errorMessage":"",users:[{"id":"被封禁用户id","blockEndTime":"封禁结束时间"}]}
     */
    public String userQueryBlock() throws Exception {
        BlockUserResult result = (BlockUserResult) rongCloud.user.block.getList();
        return result.toString();
    }

    /**
     * 添加用户到黑名单方法（每秒钟限 100 次）
     *
     * @param id id
     * @return {"code": 200,"errorMessage":""}
     */
    public String addBlacklist(String id) throws Exception {
        UserModel blackUser = new UserModel().setId(id);
        UserModel[] blacklist = {blackUser};
        UserModel user = new UserModel()
                .setId(id)
                .setBlacklist(blacklist);
        Result result = rongCloud.user.blackList.add(user);
        return result.toString();
    }

    /**
     * 获取某用户的黑名单列表方法（每秒钟限 100 次）
     *
     * @param id id
     * @return {"code": 200,"errorMessage":"",users:[{"id":""用户id,"name":"名称","portrait":"头像"}]}
     */
    public String getBlacklist(String id) throws Exception {
        UserModel user = new UserModel().setId(id);
        BlackListResult result = rongCloud.user.blackList.getList(user);
        return result.toString();
    }

    /**
     * 从黑名单中移除用户方法（每秒钟限 100 次）
     *
     * @param id id
     * @return {"code": 200,"errorMessage":""}
     */
    public String removeBlacklist(String id) throws Exception {
        UserModel blackUser = new UserModel().setId(id);
        UserModel[] blacklist = {blackUser};
        UserModel user = new UserModel()
                .setId(id)
                .setBlacklist(blacklist);
        Result result = (Result) rongCloud.user.blackList.remove(user);
        return result.toString();
    }

    /**
     * 添加用户到白名单方法（每秒钟限 100 次）
     *
     * @param id  用户id
     * @param ids 添加白名单用户ids
     * @return {"code": 200,"errorMessage":""}
     */
    public String addWhitelist(String id, String[] ids) throws Exception {
        UserModel[] whitelist = new UserModel[ids.length];
        int i = 0;
        for (String s : ids) {
            UserModel whiteUser = new UserModel().setId(s);
            whitelist[i] = whiteUser;
            i++;
        }
        UserModel user = new UserModel();
        user.setId(id);
        user.setWhitelist(whitelist);
        Result result = rongCloud.user.whiteList.add(user);
        return result.toString();
    }

    /**
     * 从白名单中移除用户方法（每秒钟限 100 次）
     *
     * @param id  用户id
     * @param ids 被移除用户ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeWhitelist(String id, String[] ids) throws Exception {
        UserModel[] whitelist = new UserModel[ids.length];
        int i = 0;
        for (String s : ids) {
            UserModel whiteUser = new UserModel().setId(s);
            whitelist[i] = whiteUser;
            i++;
        }
        UserModel user = new UserModel();
        user.setId(id);
        user.setWhitelist(whitelist);
        Result result = rongCloud.user.whiteList.remove(user);
        return result.toString();
    }

    /**
     * 系统消息
     *
     * @param txtMessage  文本消息内容
     * @param id          发送人id
     * @param targetIds   接收人数组
     * @param pushData    推送数据
     * @param pushContent 推送内容
     * @return {"code": 200,"errorMessage":""}
     */
    public String sendSystem(BaseMessage txtMessage, String id, String[] targetIds, String pushData, String pushContent) throws Exception {
        SystemMessage systemMessage = new SystemMessage()
                .setSenderId(id)
                .setTargetId(targetIds)
                .setObjectName(txtMessage.getType())
                .setContent(txtMessage)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setIsPersisted(0)
                .setIsCounted(0)
                .setContentAvailable(0);
        ResponseResult result = rongCloud.message.system.send(systemMessage);
        return result.toString();
    }

    /**
     * 发送系统模板消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，
     * 会话类型为 SYSTEM.每秒钟最多发送 100 条消息，每次最多同时向 100 人发送，如：
     * 一次发送 100 人时，示为 100 条消息。）
     *
     * @param templatePath 模板路径
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendSystemTemplate(String templatePath) throws Exception {
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(templatePath)));
            TemplateMessage template = (TemplateMessage) GsonUtil.fromJson(reader, TemplateMessage.class);
            ResponseResult result = rongCloud.message.system.sendTemplate(template);
            return result.toString();
        } finally {
            if (null != reader) {
                reader.close();
            }
        }
    }

    /**
     * 广播消息
     *
     * @param txtMessage  文本消息内容
     * @param id          发送人id
     * @param pushData    推送数据
     * @param pushContent 推送内容
     * @param os          系统 Android
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendBroadcast(BaseMessage txtMessage, String id, String pushData, String pushContent, String os) throws Exception {
        BroadcastMessage message = new BroadcastMessage()
                .setSenderId(id)
                .setObjectName(txtMessage.getType())
                .setContent(txtMessage)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setOs(os);
        ResponseResult result = rongCloud.message.system.broadcast(message);
        return result.toString();
    }

    /**
     * 广播消息撤回
     *
     * @param id 发送人id
     * @return {"code": 200,"errorMessage":""}
     */
    public String recallBroadcast(String id) throws Exception {
        BroadcastMessage message = new BroadcastMessage()
                .setSenderId(id)
                .setObjectName(rcCmdMessage.getType())
                .setContent(rcCmdMessage);
        ResponseResult result = rongCloud.message.system.broadcast(message);
        return result.toString();
    }

    /**
     * 单聊模板消息
     *
     * @param templatePath 模板路径
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendPrivateTemplate(String templatePath) throws Exception {
        Reader reader = null;
        // 发送单聊模板消息方法（一个用户向多个用户发送不同消息内容，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000 人。）
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(templatePath)));
            TemplateMessage template = (TemplateMessage) GsonUtil.fromJson(reader, TemplateMessage.class);
            System.out.println(template.toString());
            ResponseResult messagePublishTemplateResult = rongCloud.message.msgPrivate.sendTemplate(template);
            return messagePublishTemplateResult.toString();
        } finally {
            if (null != reader) {
                reader.close();
            }
        }
    }

    /**
     * 单聊消息
     *
     * @param voiceMessage 消息内容
     * @param id           发送人id
     * @param targetIds    接收人数组
     * @param pushData     推送数据
     * @param pushContent  推送内容
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendPrivate(String[] targetIds, String id, BaseMessage voiceMessage, String pushData, String pushContent) throws Exception {
        PrivateMessage privateMessage = new PrivateMessage()
                .setSenderId(id)
                .setTargetId(targetIds)
                .setObjectName(voiceMessage.getType())
                .setContent(voiceMessage)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setCount("4")
                .setVerifyBlacklist(0)
                .setIsPersisted(0)
                .setIsCounted(0)
                .setIsIncludeSender(0);
        //发送单聊方法
        ResponseResult publishPrivateResult = rongCloud.message.msgPrivate.send(privateMessage);
        return publishPrivateResult.toString();
    }

    /**
     * 撤回单聊消息
     *
     * @param id       发送人id
     * @param targetId 目标id
     * @param uId      消息唯一标识
     * @return {"code": 200,"errorMessage":""}
     */

    public String recallPrivate(String id, String targetId, String uId) throws Exception {
        RecallMessage message = new RecallMessage()
                .setSenderId(id)
                .setTargetId(targetId)
                .setuId(uId)
                .setSentTime(String.valueOf(System.currentTimeMillis()));
        ResponseResult result = (ResponseResult) rongCloud.message.msgPrivate.recall(message);
        return result.toString();
    }

    /**
     * 群组消息
     *
     * @param txtMessage  文本消息内容
     * @param id          发送人id
     * @param targetIds   接收人数组
     * @param pushData    推送数据
     * @param pushContent 推送内容
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendGroup(BaseMessage txtMessage, String id, String[] targetIds, String pushData, String pushContent) throws Exception {
        //群组消息
        GroupMessage groupMessage = new GroupMessage()
                .setSenderId(id)
                .setTargetId(targetIds)
                .setObjectName(txtMessage.getType())
                .setContent(txtMessage)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setIsPersisted(0)
                .setIsIncludeSender(0)
                .setContentAvailable(0);
        ResponseResult result = rongCloud.message.group.send(groupMessage);
        return result.toString();
    }

    /**
     * 群组@消息
     *
     * @param txtMessage  文本消息内容
     * @param id          发送人id
     * @param targetId    接收人数组
     * @param mentionIds  要@的人
     * @param pushData    推送数据
     * @param pushContent 推送内容
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendGroupMention(String[] targetId, String[] mentionIds, BaseMessage txtMessage, String id, String pushData, String pushContent) throws Exception {
        //@内容
        MentionedInfo mentionedInfo = new MentionedInfo(1, mentionIds, "push");
        //@消息的消息内容
        MentionMessageContent content = new MentionMessageContent(txtMessage, mentionedInfo);

        MentionMessage mentionMessage = new MentionMessage()
                .setSenderId(id)
                .setTargetId(targetId)
                .setObjectName(txtMessage.getType())
                .setContent(content)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setIsPersisted(0)
                .setIsCounted(0)
                .setIsIncludeSender(0)
                .setContentAvailable(0);
        ResponseResult result = rongCloud.message.group.sendMention(mentionMessage);
        return result.toString();
    }

    /**
     * 群组定向消息
     *
     * @param txtMessage  文本消息内容
     * @param id          发送人id
     * @param targetIds   接收人数组
     * @param toUserIds   定向用户id
     * @param pushData    推送数据
     * @param pushContent 推送内容
     * @return {"code": 200,"errorMessage":""}
     */
    public String sendGroupDirection(String id, String[] targetIds, String[] toUserIds, BaseMessage txtMessage, String pushData, String pushContent) throws Exception {
        //定向用户
        GroupMessage groupMessage = new GroupMessage()
                .setSenderId(id)
                .setTargetId(targetIds)
                .setToUserId(toUserIds)
                .setObjectName(txtMessage.getType())
                .setContent(txtMessage)
                .setPushContent(pushContent)
                .setPushData(pushData)
                .setIsPersisted(0)
                .setIsIncludeSender(0)
                .setContentAvailable(0);
        ResponseResult result = rongCloud.message.group.sendDirection(groupMessage);
        return result.toString();
    }

    /**
     * 群组撤回消息
     *
     * @param id       发送人id
     * @param targetId 接收目标id
     * @param uId      消息唯一标识
     * @return {"code": 200,"errorMessage":""}
     */

    public String recallGroup(String id, String targetId, String uId) throws Exception {
        RecallMessage message = new RecallMessage()
                .setSenderId(id)
                .setTargetId(targetId)
                .setuId(uId)
                .setSentTime(String.valueOf(System.currentTimeMillis()));
        ResponseResult result = (ResponseResult) rongCloud.message.group.recall(message);
        return result.toString();
    }

    /**
     * 聊天室消息
     *
     * @param id         发送人id
     * @param targetIds  接收人数组
     * @param txtMessage 消息内容
     * @return {"code": 200,"errorMessage":""}
     */

    public String sendChatroom(String id, String[] targetIds, BaseMessage txtMessage) throws Exception {
        //聊天室消息
        ChatroomMessage message = new ChatroomMessage()
                .setSenderId(id)
                .setTargetId(targetIds)
                .setContent(txtMessage)
                .setObjectName(txtMessage.getType());
        ResponseResult result = rongCloud.message.chatroom.send(message);
        return result.toString();
    }

    /**
     * 撤回聊天室消息
     *
     * @param id       发送人id
     * @param targetId 接收人数组
     * @param uId      消息唯一标识
     * @return {"code": 200,"errorMessage":""}
     */

    public String recallChatroom(String id, String targetId, String uId) throws Exception {
        RecallMessage message = new RecallMessage()
                .setSenderId(id)
                .setTargetId(targetId)
                .setuId(uId)
                .setSentTime(String.valueOf(System.currentTimeMillis()));
        ResponseResult result = (ResponseResult) rongCloud.message.chatroom.recall(message);
        return result.toString();
    }

    /**
     * 删除历史消息
     *
     * @param day 删除日期(2018030210)
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeHistory(String day) throws Exception {
        ResponseResult result = rongCloud.message.history.remove(day);
        return result.toString();
    }

    /**
     * 获取历史消息
     *
     * @param day 删除日期(2018030210)
     * @return {"code": 200,"errorMessage":"","url":"历史消息下载地址","date":" 历史记录时间(yyyymmddhh)"}
     */

    public String getHistory(String day) throws Exception {
        HistoryMessageResult result = (HistoryMessageResult) rongCloud.message.history.get(day);
        return result.toString();
    }

    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     *
     * @param keyword 敏感词
     * @return {"code": 200,"errorMessage":""}
     */

    public String addSensitiveword(String keyword) throws Exception {
        SensitiveWordModel sentiveWord = new SensitiveWordModel()
                .setType(0)
                .setKeyword(keyword)
                .setReplace("***");
        ResponseResult result = rongCloud.sensitiveword.add(sentiveWord);
        return result.toString();
    }

    /**
     * 查询敏感词列表方法
     *
     * @return {"code": 200,"errorMessage":"",words:["type":敏感词类型,"keyword":"敏感词","replace":"替换词"]}
     */

    public String getListSensitiveword() throws Exception {
        ListWordfilterResult result = rongCloud.sensitiveword.getList(1);
        return result.toString();
    }

    /**
     * 移除敏感词方法（从敏感词列表中，移除某一敏感词。）
     *
     * @param keyword 敏感词
     * @return {"code": 200,"errorMessage":""}
     */

    public String deleteSensitiveword(String keyword) throws Exception {
        ResponseResult result = rongCloud.sensitiveword.remove(keyword);
        return result.toString();
    }

    /**
     * 创建群组方法（创建群组，并将用户加入该群组，用户将可以收到该群的消息，同一用户最多可加入 500 个群，
     * 每个群最大至 3000 人，App 内的群组数量没有限制.注：其实本方法是加入群组方法 /group/join 的别名。）
     *
     * @param ids       成员ids
     * @param groupId   群id
     * @param groupName 群名称
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupCreate(String[] ids, String groupId, String groupName) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members)
                .setName(groupName);
        Result result = (Result) rongCloud.group.create(group);
        return result.toString();

    }

    /**
     * 同步用户所属群组方法（当第一次连接融云服务器时，需要向融云服务器提交 userId 对应的用户当前所加入的所有群组，
     * 此接口主要为防止应用中用户群信息同融云已知的用户所属群信息不同步。）
     *
     * @param groupSyncGroups 群信息    GroupModel group1 = new GroupModel().setId("groupId1").setName("groupName1");
     * @param id              用户id
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupSync(GroupModel[] groupSyncGroups, String id) throws Exception {
        UserGroup user = new UserGroup();
        user.setGroups(groupSyncGroups);
        user.setId(id);
        Result result = (Result) rongCloud.group.sync(user);
        return result.toString();
    }

    /**
     * 刷新群组信息方法
     *
     * @param groupId   群id
     * @param groupName 群名称
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupUpdate(String groupId, String groupName) throws Exception {
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setName(groupName);
        Result result = (Result) rongCloud.group.update(group);
        return result.toString();
    }

    /**
     * 将用户加入指定群组，用户将可以收到该群的消息，同一用户最多可加入 500 个群，每个群最大至 3000 人。
     *
     * @param ids       成员ids
     * @param groupId   群id
     * @param groupName 群名称
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupJoin(String[] ids, String groupId, String groupName) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members)
                .setName(groupName);
        Result result = (Result) rongCloud.group.join(group);
        return result.toString();
    }

    /**
     * 查询群成员方法
     *
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":"",members:[{id:"用户id"}]}
     */
    public String groupQueryUser(String groupId) throws Exception {
        GroupModel group = new GroupModel().setId(groupId);
        GroupUserQueryResult result = rongCloud.group.get(group);
        return result.toString();
    }

    /**
     * 退出群组方法（将用户从群中移除，不再接收该群组的消息.）
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupQuit(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members);
        Result result = (Result) rongCloud.group.quit(group);
        return result.toString();
    }

    /**
     * 添加禁言群成员方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupAddGagUser(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members)
                .setMinute(5);
        Result result = (Result) rongCloud.group.gag.add(group);
        return result.toString();
    }

    /**
     * 查询被禁言群成员方法
     *
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":"",members:[{"id":"成员id","time":"解禁时间"}]}
     */

    public String groupLisGagUser(String groupId) throws Exception {
        ListGagGroupUserResult result = rongCloud.group.gag.getList(groupId);
        return result.toString();

    }

    /**
     * 移除禁言群成员方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupRollBackGagUser(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setMembers(members)
                .setId(groupId);
        ResponseResult result = (ResponseResult) rongCloud.group.gag.remove(group);
        return result.toString();
    }

    /**
     * 解散群组方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupDismissResult(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members);
        Result result = (Result) rongCloud.group.dismiss(group);
        return result.toString();

    }

    /**
     * 添加全局禁言群方法
     *
     * @param ids 成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupBanUserResult(String[] ids) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel groupModel = new GroupModel()
                .setMembers(members)
                .setMinute(1000000);
        Result result = rongCloud.group.ban.user.add(groupModel);
        return result.toString();
    }

    /**
     * 获取全局禁言群方法
     *
     * @return {"code": 200,"errorMessage":"",members:[{"id":"成员id","time":"解禁时间"}]}
     */

    public String groupBanUserGetListResult() throws Exception {
        ListGagGroupUserResult result = (ListGagGroupUserResult) rongCloud.group.ban.user.getList();
        return result.toString();
    }

    /**
     * 移除用户全局禁言群方法
     *
     * @param ids 成员ids
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupBanUserRemoveResult(String[] ids) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel groupModel = new GroupModel()
                .setMembers(members);
        Result result = rongCloud.group.ban.user.remove(groupModel);
        return result.toString();
    }


    /**
     * 添加禁言白名单用户方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupBanWhitelistAddResult(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel groupModel = new GroupModel()
                .setId(groupId)
                .setMembers(members);
        Result result = rongCloud.group.ban.whitelist.user.add(groupModel);
        return result.toString();
    }

    /**
     * 查询禁言白名单用户
     *
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":"",userIds:[userId,userId]}
     */

    public String groupBanWhitelistGetListResult(String groupId) throws Exception {
        GroupBanWhitelistResult result = (GroupBanWhitelistResult) rongCloud.group.ban.whitelist.user.getList(groupId);
        return result.toString();
    }

    /**
     * 移除禁言白名单用户方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupBanWhitelistRemoveResult(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel groupModel = new GroupModel()
                .setMembers(members)
                .setId(groupId);
        Result result = rongCloud.group.ban.whitelist.user.remove(groupModel);
        return result.toString();

    }

    /**
     * 添加禁言群方法
     *
     * @param groupIds 群ids
     * @return {"code": 200,"errorMessage":""}
     */
    public String groupBanAddResult(String[] groupIds) throws Exception {
        Result result = rongCloud.group.ban.add(groupIds);
        return result.toString();
    }

    /**
     * 查询禁言群方法
     *
     * @param groupIds 群ids
     * @return {"code": 200,"errorMessage":"",
     * groups:[{"id":"群id","name":"群名称","minute":"禁言时间","status":禁言状态,"members":[{"id":"成员id","groupId":"群id","munite":禁言时间}]}]}
     */

    public String groupBanGetListResult(String[] groupIds) throws Exception {

        GroupBanResult result = (GroupBanResult) rongCloud.group.ban.getList();
        return result.toString();
    }

    /**
     * 移除禁言群方法
     *
     * @param groupIds 群ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupBanRemoveResult(String[] groupIds) throws Exception {
        Result result = rongCloud.group.ban.remove(groupIds);
        return result.toString();
    }

    /**
     * 创建聊天室
     * ChatroomModel[] chatrooms = {
     * new ChatroomModel().setId("chatroomId1").setName("chatroomName1"),
     * new ChatroomModel().setId("chatroomId2").setName("chatroomName2")
     * }
     *
     * @param chatrooms 聊天室信息
     * @return {"code": 200,"errorMessage":""}
     */

    public String createChatroom(ChatroomModel[] chatrooms) throws Exception {
        ResponseResult result = rongCloud.chatroom.create(chatrooms);
        return result.toString();
    }

    /**
     * 查询聊天室成员
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":"","total":聊天室用户数,
     * members:[{"id":"成员id","time":"加入聊天室时间","chatroomId":"聊天室ID","munite":禁言时间}]}
     */

    public String getChatroom(String chatroomId) throws Exception {
        ChatroomModel chatroomModel = new ChatroomModel()
                .setId(chatroomId)
                .setCount(500)
                .setOrder(1);
        ChatroomUserQueryResult chatroomQueryUserResult = rongCloud.chatroom.get(chatroomModel);
        return chatroomQueryUserResult.toString();
    }

    /**
     * 查询聊天室成员是否存在
     *
     * @param chatroomId 聊天室id
     * @param id         成员id
     * @return {"code": 200,"errorMessage":"","isInChrm":true存在}
     */

    public String chatroomIsExist(String id, String chatroomId) throws Exception {
        ChatroomMember member = new ChatroomMember()
                .setId(id)
                .setChatroomId(chatroomId);
        CheckChatRoomUserResult result = rongCloud.chatroom.isExist(member);
        return result.toString();
    }

    /**
     * 添加聊天室消息白名单
     *
     * @param messageType 消息类型   String[] messageType = {"RC:VcMsg", "RC:ImgTextMsg", "RC:ImgMsg"}
     * @return {"code": 200,"errorMessage":""}
     */

    public String addChatroomMsgWhiteList(String[] messageType) throws Exception {
        ResponseResult result = rongCloud.chatroom.whiteList.message.add(messageType);
        return result.toString();
    }

    /**
     * 删除聊天室消息白名单
     *
     * @param messageType 消息类型   String[] messageType = {"RC:VcMsg", "RC:ImgTextMsg", "RC:ImgMsg"}
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeChatroomMsgWhiteList(String[] messageType) throws Exception {
        ResponseResult result = rongCloud.chatroom.whiteList.message.remove(messageType);
        return result.toString();
    }

    /**
     * 获取聊天室消息白名单
     *
     * @return {"code": 200,"errorMessage":"","objectNames":[消息类型1,消息类型2]}
     */

    public String getChatroomMsgWhiteList() throws Exception {
        ChatroomWhitelistMsgResult result = rongCloud.chatroom.whiteList.message.getList();
        return result.toString();

    }

    /**
     * 添加聊天室用户白名单
     *
     * @param chatroomId 聊天室id
     * @param ids        成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String addChatroomUserWhiteList(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members);
        ResponseResult result = rongCloud.chatroom.whiteList.user.add(chatroom);
        return result.toString();

    }

    /**
     * 删除聊天室用户白名单
     *
     * @param chatroomId 聊天室id
     * @param ids        成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeChatroomWhiteListMsg(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members);
        ResponseResult result = rongCloud.chatroom.whiteList.user.remove(chatroom);
        return result.toString();

    }

    /**
     * 获取聊天室消息白名单
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":"",
     * "members":["id":"成员id","name":"名称","portrait":"头像","minute":禁言时间,blacklist:[黑名单],"whitelist":[白单]]}
     */

    public String getChatroomWhiteListMsg(String chatroomId) throws Exception {
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId);
        Result result = rongCloud.chatroom.whiteList.user.getList(chatroom);
        return result.toString();

    }

    /**
     * 添加聊天室全局禁言
     *
     * @param ids 成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String chatroomBan(String[] ids) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setMembers(members)
                .setMinute(5);
        ResponseResult result = rongCloud.chatroom.ban.add(chatroom);
        return result.toString();
    }

    /**
     * 获取聊天时全局禁言列表
     *
     * @return {"code": 200,"errorMessage":"",
     * "members":[{"id":"成员id","time":"加入聊天室时间","chatroomId":"聊天室ID","munite":禁言时间}]}
     */

    public String getChatroomBan() throws Exception {
        ListGagChatroomUserResult chatroomListGagUserResult = rongCloud.chatroom.ban.getList();
        return chatroomListGagUserResult.toString();
    }

    /**
     * 删除聊天时全局禁言
     *
     * @param ids 成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeChatroomBan(String[] ids) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setMembers(members)
                .setMinute(5);
        ResponseResult removeResult = rongCloud.chatroom.ban.remove(chatroom);
        return removeResult.toString();

    }

    /**
     * 添加封禁聊天室成员方法
     *
     * @param ids        成员ids
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     */

    public String addChatroomBlock(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members)
                .setMinute(5);
        ResponseResult result = rongCloud.chatroom.block.add(chatroom);
        return result.toString();
    }

    /**
     * 移除封禁聊天室成员方法
     *
     * @param ids        成员ids
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeChatroomBlock(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members);
        ResponseResult result = rongCloud.chatroom.block.remove(chatroom);
        return result.toString();
    }

    /**
     * 获取封禁聊天室成员方法
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":"",
     * "members":[{"id":"成员id","time":"加入聊天室时间","chatroomId":"聊天室ID","munite":禁言时间}]}
     */

    public String getChatroomBlock(String chatroomId) throws Exception {
        ListBlockChatroomUserResult result = rongCloud.chatroom.block.getList(chatroomId);
        return result.toString();
    }

    /**
     * 销毁聊天室
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     */

    public String destroyChatroom(String chatroomId) throws Exception {
        ChatroomModel chatroomModel = new ChatroomModel()
                .setId(chatroomId);
        ResponseResult result = rongCloud.chatroom.destroy(chatroomModel);
        return result.toString();
    }

    /**
     * 添加应用内聊天室降级消息
     *
     * @param messageType 消息類型  String[] messageType = {"RC:VcMsg", "RC:ImgTextMsg", "RC:ImgMsg"};
     * @return {"code": 200,"errorMessage":""}
     */

    public String addChatroomDemotion(String[] messageType) throws Exception {
        ResponseResult result = rongCloud.chatroom.demotion.add(messageType);
        return result.toString();
    }

    /**
     * 移除应用内聊天室降级消息
     *
     * @param messageType 消息類型  String[] messageType = {"RC:VcMsg", "RC:ImgTextMsg", "RC:ImgMsg"};
     * @return {"code": 200,"errorMessage":""}
     */

    public String rmoveChatroomDemotion(String[] messageType) throws Exception {
        ResponseResult result = rongCloud.chatroom.demotion.remove(messageType);
        return result.toString();
    }

    /**
     * 添加聊天室消息优先级
     *
     * @return {"code": 200,"errorMessage":"",objectNames:[消息类型1,消息类型2]}
     */

    public String getChatroomDemotion() throws Exception {
        ChatroomDemotionMsgResult result = rongCloud.chatroom.demotion.getList();
        return result.toString();
    }

    /**
     * 聊天室消息停止分发
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     */

    public String stopChatroomDistributio(String chatroomId) throws Exception {
        ChatroomModel chatroomModel = new ChatroomModel()
                .setId(chatroomId);
        ResponseResult result = rongCloud.chatroom.distribute.stop(chatroomModel);
        return result.toString();
    }


    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/distribute.html#resume
     * <p>
     * 聊天室消息恢复分发方法（每秒钟限 100 次）
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     */

    public String resumeChatroomDistributio(String chatroomId) throws Exception {
        ChatroomModel chatroomModel = new ChatroomModel()
                .setId(chatroomId);
        ResponseResult result = rongCloud.chatroom.distribute.resume(chatroomModel);
        return result.toString();
    }

    /**
     * 添加禁言聊天室成员方法想（在 App 中如果不让某一用户在聊天室中发言时，可将此用户在聊天室中禁言，
     * 被禁言用户可以接收查看聊天室中用户聊天信息，但不能发送消息.）获取某用户的黑名单列表方法（每秒钟限 100 次）
     *
     * @param chatroomId 聊天室id
     * @param ids        成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String addChatroomGag(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members)
                .setMinute(5);
        ResponseResult result = rongCloud.chatroom.gag.add(chatroom);
        return result.toString();
    }

    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/gag.html#remove
     * 查询被禁言聊天室成员方法
     *
     * @return {"code": 200,"errorMessage":"",
     * "members":[{"id":"成员id","time":"加入聊天室时间","chatroomId":"聊天室ID","munite":禁言时间}]}
     */

    public String getChatroomGag(String chatroomId) throws Exception {
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId);
        ListGagChatroomUserResult result = rongCloud.chatroom.gag.getList(chatroom);
        return result.toString();
    }

    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/gag.html#getList
     * <p>
     * 移除禁言聊天室成员
     *
     * @param chatroomId 聊天室id
     * @param ids        成员ids
     * @return {"code": 200,"errorMessage":""}
     */

    public String removeChatroomGag(String[] ids, String chatroomId) throws Exception {
        ChatroomMember[] members = new ChatroomMember[ids.length];
        int i = 0;
        for (String id : ids) {
            ChatroomMember chatroomMember = new ChatroomMember();
            chatroomMember.setId(id);
            members[i] = chatroomMember;
            i++;
        }
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId)
                .setMembers(members);
        ResponseResult result = rongCloud.chatroom.gag.remove(chatroom);
        return result.toString();
    }

    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/keepalive.html#add
     * <p>
     * 添加保活聊天室
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     **/

    public String addChatroomKeepalive(String chatroomId) throws Exception {
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId);
        ResponseResult result = rongCloud.chatroom.keepalive.add(chatroom);
        return result.toString();
    }

    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/keepalive.html#remove
     * <p>
     * 删除保活聊天室
     *
     * @param chatroomId 聊天室id
     * @return {"code": 200,"errorMessage":""}
     **/

    public String removeChatroomKeepalive(String chatroomId) throws Exception {
        ChatroomModel chatroom = new ChatroomModel()
                .setId(chatroomId);
        ResponseResult result = rongCloud.chatroom.keepalive.remove(chatroom);
        return result.toString();
    }

    /**
     * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/chatroom/keepalive.html#getList
     * <p>
     * 获取保活聊天室
     *
     * @return {"code": 200,"errorMessage":"",chatrooms:[聊天室1,聊天室2]}
     **/

    public String getChatroomKeepalive() throws Exception {
        ChatroomKeepaliveResult result = rongCloud.chatroom.keepalive.getList();
        return result.toString();
    }

    /**
     * 新添加禁言群成员方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupMuteMembersAdd(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setId(groupId)
                .setMembers(members)
                .setMinute(5);
        Result result = (Result) rongCloud.group.muteMembers.add(group);
        return result.toString();

    }

    /**
     * 新查询被禁言群成员方法
     *
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":"",members:[{"time":"解禁时间","id":"群成员id"}]}
     */

    public String groupMuteMemberList(String groupId) throws Exception {
        GroupMuteMembersListResult result = rongCloud.group.muteMembers.getList(groupId);
        return result.toString();
    }

    /**
     * 新移除禁言群成员方法
     *
     * @param ids     成员ids
     * @param groupId 群id
     * @return {"code": 200,"errorMessage":""}
     */

    public String groupRemoveMuteMemberr(String[] ids, String groupId) throws Exception {
        GroupMember[] members = new GroupMember[ids.length];
        int i = 0;
        for (String id : ids) {
            GroupMember groupMember = new GroupMember();
            groupMember.setId(id);
            members[i] = groupMember;
            i++;
        }
        GroupModel group = new GroupModel()
                .setMembers(members)
                .setId(groupId);
        ResponseResult result = (ResponseResult) rongCloud.group.muteMembers.remove(group);
        return result.toString();
    }

    /**
     * API 文档:
     * https://www.rongcloud.cn/docs/push_service.html#broadcast
     * <p>
     * 广播消息
     *
     * @param fromuserid 发送人id
     * @param platforms  设备 String[]{"ios", "android"}
     * @param ids        用户id
     * @param alert      推送内容
     * @param message    消息内容
     *                   Message message = new Message();
     *                   message.setContent("this is message");
     *                   message.setObjectName("RC:TxtMsg");
     * @return {"code": 200,"errorMessage":""}
     **/

    public String broadcast(String fromuserid, Message message, String[] platforms, String[] ids, String alert) throws Exception {
        BroadcastModel broadcast = new BroadcastModel();
        broadcast.setFromuserid(fromuserid);
        broadcast.setPlatform(platforms);
        Audience audience = new Audience();
        audience.setUserid(ids);
        broadcast.setAudience(audience);
        broadcast.setMessage(message);
        Notification notification = new Notification();
        notification.setAlert(alert);
        broadcast.setNotification(notification);
        PushResult result = rongCloud.push.message(broadcast);
        return result.toString();
    }

    /**
     * API 文档:
     * https://www.rongcloud.cn/docs/push_service.html#push
     * <p>
     * 推送消息
     *
     * @param platforms 设备 String[]{"ios", "android"}
     * @param ids       用户ids
     * @param alert     推送内容
     * @return {"code": 200,"errorMessage":""}
     **/

    public String push(String[] platforms, String[] ids, String alert) throws Exception {
        PushModel pushmodel = new PushModel();
        pushmodel.setPlatform(platforms);
        Audience audience = new Audience();
        audience.setUserid(ids);
        pushmodel.setAudience(audience);
        Notification notification = new Notification();
        notification.setAlert(alert);
        pushmodel.setNotification(notification);
        PushResult result = rongCloud.push.push(pushmodel);
        return result.toString();
    }

    /**
     * API 文档:
     * https://www.rongcloud.cn/docs/push_service.html#user_tag_set
     * <p>
     * 添加标签
     *
     * @param id   用户id
     * @param tags 标签
     * @return {"code": 200,"errorMessage":""}
     **/

    public String setTag(String id, String[] tags) throws Exception {
        TagModel tag = new TagModel();
        tag.setTags(tags);
        tag.setUserId(id);
        Result result = rongCloud.user.tag.set(tag);
        return result.toString();
    }

    /**
     * API 文档:
     * https://www.rongcloud.cn/docs/push_service.html#user_tag_batch_set
     * <p>
     * 批量添加标签
     *
     * @param ids  用户ids
     * @param tags 标签
     * @return {"code": 200,"errorMessage":""}
     **/

    public String batchSetTag(String[] ids, String[] tags) throws Exception {
        BatchTagModel tag = new BatchTagModel();
        tag.setTags(tags);
        tag.setUserIds(ids);
        Result result = rongCloud.user.tag.batchSet(tag);
        return result.toString();
    }

    /**
     * API 文档:
     * https://www.rongcloud.cn/docs/push_service.html#user_tags_get
     * <p>
     * 查询用户标签
     *
     * @param ids 用户ids
     * @return {"code": 200,"errorMessage":"result":{"用户id1":[标签1],"用户id2":[标签2]}}
     **/

    public String getTag(String[] ids) throws Exception {
        GetTagModel tag = new GetTagModel();
        tag.setUserIds(ids);
        GetTagResult result = rongCloud.user.tag.get(tag);
        return result.toString();
    }
}
