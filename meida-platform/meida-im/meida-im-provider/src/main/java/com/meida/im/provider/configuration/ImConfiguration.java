package com.meida.im.provider.configuration;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.RedisUtils;
import io.rong.RongCloud;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认配置类
 *
 * @author zyf
 */
@Slf4j
@Configuration
public class ImConfiguration {

    @Bean
    @ConditionalOnMissingBean(RongCloud.class)
    public RongCloud rongCloud(RedisUtils redisUtils) {
        RongCloud rongCloud = RongCloud.getInstance("", "");
        EntityMap map = redisUtils.getConfigMap("IM_CONFIG");
        String serviceName = map.get("serviceName");
        if (FlymeUtils.isNotEmpty(serviceName) && serviceName.equals("IM_RONGCLOUD")) {
            String appKey = map.get("appKey");
            String appSecret = map.get("appSecret");
            if (FlymeUtils.allNotNull(appKey, appSecret)) {
                try {
                    String api = map.get("api");
                    if (FlymeUtils.isEmpty(api)) {
                        rongCloud = RongCloud.getInstance(appKey, appSecret);
                    } else {
                        rongCloud = RongCloud.getInstance(appKey, appSecret, api);
                    }
                    log.info("IM初始化成功###############################");
                } catch (Exception e) {
                    log.error("IM初始化失败");
                }
            }
        }
        return rongCloud;
    }
}
