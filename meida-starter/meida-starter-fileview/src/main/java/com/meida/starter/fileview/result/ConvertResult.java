package com.meida.starter.fileview.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zyf
 */
@Data
@ToString
@AllArgsConstructor
public class ConvertResult {

    private String targetFile;
    private boolean success;
}
