package com.meida.starter.fileview.conver;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.starter.fileview.result.ConvertResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 音视频转换
 *
 * @author ldd
 */
@Slf4j
public class MediaUtils {
    public static ConvertResult convertMedia(File srcFile, File targetDir, String ffmpegPath, int type) {
        List<String> commands = new ArrayList<>();
        String outputDir = targetDir.getAbsolutePath();
        if (FlymeUtils.isNotEmpty(ffmpegPath) && type > 1) {
            File dirFile = new File(outputDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            if (FlymeUtils.isWindows()) {
                commands.add(ffmpegPath);
            } else {
                commands.add("ffmpeg");
            }
            commands.add("-i");
            commands.add(srcFile.getAbsolutePath());
            String suffix = "";
            if (type == 2) {
                //ffmpeg -i C:\Users\Administrator\Desktop\aa.avi -c:v copy -c:a copy -y output_filename.mp4
                //commands.add("-c:v");
                //commands.add("copy");
                //commands.add("libx264");
                //commands.add("-c:a");
                //commands.add("copy");
                commands.add("-vcodec");
                commands.add("h264");
                commands.add("-y");
                suffix = ".mp4";
            }
            if (type == 3) {
                //    ffmpeg -i C:\Users\Administrator\Desktop\20220407092943_170.wav -codec:v copy -codec:a libmp3lame -q:a 0  aa.mp3
                commands.add("-codec:v");
                commands.add("copy");
                commands.add("-codec:a");
                commands.add("libmp3lame");
                commands.add("-q:a");
                commands.add("0");
                suffix = ".mp3";
            }
            commands.add(outputDir + "\\" + getFileNameWithoutSuffix(srcFile.getName()) + suffix);
            log.info("开始转换多媒体,命令行是:{}", StringUtils.joinWith(" ", commands));
            try {
                ProcessBuilder pb = new ProcessBuilder(commands);
                pb.redirectErrorStream(true);
                Process process = pb.start();
                String result = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
                log.info("转换 from {} output {}:\n result:{}\n", srcFile.getAbsolutePath(), outputDir, result);
                process.destroy();
                File targetFile = new File(targetDir, getFileNameWithoutSuffix(srcFile.getName()) + suffix);
                log.info("目标文件是:{}", targetFile.getAbsolutePath());
                if (targetFile.exists()) {
                    log.info("{} 文件转化成功", srcFile.getAbsolutePath());
                    return new ConvertResult(targetFile.getAbsolutePath(), true);
                } else {
                    log.info("{} 文件转化失败", srcFile.getAbsolutePath());
                    return new ConvertResult(null, false);
                }
            } catch (Exception e) {
                log.error("转换多媒体异常", e);
            }
        } else {
            log.error("ffmpeg未配置", ffmpegPath);
        }
        return new ConvertResult(null, false);
    }

    private static String getFileNameWithoutSuffix(String fileName) {
        int pos = fileName.lastIndexOf(".");
        if (pos >= 0) {
            if (pos + 1 != fileName.length()) {
                return fileName.substring(0, pos);
            }
        }
        return "";
    }

    public static void main(String[] args) {

        String filePath = "c:/test.doc";
        System.out.println(filePath.substring(filePath.lastIndexOf(".") + 1));
        File file = new File(filePath);
        File targetDir = new File(file.getParentFile(), "pdf");
        //convertMedia(file, targetDir, "soffice", 1);
    }


}
