package com.meida.starter.fileview.conver;

import com.aspose.words.Document;
import com.aspose.words.SaveFormat;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.starter.fileview.result.ConvertResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 文档转换
 *
 * @author zyf
 */
@Slf4j
public class OfficeUtils {
    public static ConvertResult convertPdf(File srcFile, File targetDir, String libOfficePath) {
        try {
            Document doc = new Document(srcFile.getAbsolutePath());
            File targetFile = new File(targetDir, getFileNameWithoutSuffix(srcFile.getName()) + ".pdf");

            String absolutePath = targetFile.getAbsolutePath();
            doc.save(absolutePath, SaveFormat.PDF);

            return new ConvertResult(absolutePath, true);

        } catch (Exception e) {
            log.error("convertPdf", e);
        }
        return new ConvertResult(null, false);
    }

    public static ConvertResult convertPdfBycmd(File srcFile, File targetDir, String libOfficePath) {
        List<String> commands = new ArrayList<>();
        String outputDir = targetDir.getAbsolutePath();
        if (FlymeUtils.isNotEmpty(libOfficePath)) {
            if (FlymeUtils.isWindows()) {
                commands.add(libOfficePath);
            } else {
                commands.add("libreoffice");
            }
            commands.add("--headless");
            commands.add("--invisible");
            commands.add("--convert-to");
            commands.add("pdf");
            commands.add("--outdir");
            commands.add(outputDir);
            commands.add(srcFile.getAbsolutePath());
            log.info("开始转换pdf,命令行是:{}", StringUtils.joinWith(" ", commands));
            try {
                ProcessBuilder pb = new ProcessBuilder(commands);
                pb.redirectErrorStream(true);
                Process process = pb.start();
                String result = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
                log.info("转换PDF from {} output {}:\n result:{}\n", srcFile.getAbsolutePath(), outputDir, result);
                process.destroy();
                File targetFile = new File(targetDir, getFileNameWithoutSuffix(srcFile.getName()) + ".pdf");
                log.info("目标文件是:{}", targetFile.getAbsolutePath());
                if (targetFile.exists()) {
                    log.info("{} 文档转化成功", srcFile.getAbsolutePath());
                    return new ConvertResult(targetFile.getAbsolutePath(), true);
                } else {
                    log.info("{} 文档转化失败", srcFile.getAbsolutePath());
                    return new ConvertResult(null, false);
                }
            } catch (IOException e) {
                log.error("convertPdf", e);
            }
        }else{
            log.error("libOfficePath未配置", libOfficePath);
        }
        return new ConvertResult(null, false);
    }

    private static String getFileNameWithoutSuffix(String fileName) {
        int pos = fileName.lastIndexOf(".");
        if (pos >= 0) {
            if (pos + 1 != fileName.length()) {
                return fileName.substring(0, pos);
            }
        }
        return "";
    }

    public static  void main(String[] args){
        String filePath="d:/3.docx";
        File file = new File(filePath);
        File targetDir = new File(file.getParentFile(), "pdf");
        convertPdf(file,targetDir,"D:/worksoft/LibreOffice/program/soffice");
    }


}
