package com.meida.starter.ocr.listener;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.mq.annotation.MsgListener;
import com.meida.starter.ocr.hander.BaseOcrListener;
import com.meida.starter.ocr.utils.PdfUtils;
import com.meida.starter.ocr.utils.TikaUtils;
import com.meida.starter.rabbitmq.config.RabbitComponent;
import com.meida.starter.rabbitmq.core.BaseRabbiMqHandler;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import net.sourceforge.tess4j.ITesseract;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * 文字识别
 *
 * @author zyf
 */
@RabbitComponent(value = "ocrRabbitMqListener")
public class OcrRabbitMqListener extends BaseRabbiMqHandler<EntityMap> {

    @Autowired(required = false)
    private BaseOcrListener baseOcrListener;

    @Autowired(required = false)
    private ITesseract iTesseract;

    @MsgListener(queues = QueueConstants.QUEUE_OCR)
    public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        super.onMessage(baseMap, deliveryTag, channel, new MqListener<EntityMap>() {
            @Override
            public void handler(EntityMap map, Channel channel) {
                try {
                    Long fileId = map.getLong("fileId");
                    String filePath = map.get("filePath");
                    String fileExt = map.get("fileExt");
                    if (FlymeUtils.isNotEmpty(baseOcrListener) && FlymeUtils.isNotEmpty(filePath)) {
                        File file = new File(filePath);
                        StringBuffer sb = new StringBuffer();
                        String content = TikaUtils.parseContent(file);
                        if (FlymeUtils.isEmpty(content) && fileExt.equalsIgnoreCase("pdf")) {
                            //当pdf是单页时转图片进行识别
                            List<BufferedImage> bufferedImageList = PdfUtils.pdfToBufferedImageList(file);
                            if (FlymeUtils.isNotEmpty(bufferedImageList)) {
                                if (FlymeUtils.isNotEmpty(iTesseract)) {
                                    for (BufferedImage bufferedImage : bufferedImageList) {
                                        sb.append(iTesseract.doOCR(bufferedImage));
                                    }
                                }
                            }
                        } else {
                            sb.append(content);
                        }
                        baseOcrListener.ocrSuccess(fileId, sb.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


}
