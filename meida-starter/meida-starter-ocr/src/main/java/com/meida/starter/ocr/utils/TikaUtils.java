package com.meida.starter.ocr.utils;

import com.meida.common.base.utils.FlymeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hwpf.model.FootnoteReferenceDescriptor;
import org.apache.tika.detect.EncodingDetector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.txt.UniversalEncodingDetector;
import org.apache.tika.sax.BodyContentHandler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * TikaUtils
 */
@Slf4j
public class TikaUtils {


    public static String parseContent(File f) {
        String content = null;
        try {
            InputStream stream = FileUtils.openInputStream(f);
            content = parseContent(stream);
            if (FlymeUtils.isEmpty(content)) {
                content = parseTxt(f);
            }
            if (content != null) {
                return content.trim();
            }
            if (content.length() < 50) {
                return null;
            }
        } catch (Exception e) {
            log.error("tika parse error", e);
        }
        return content;
    }

    private static String parseTxt(File file) throws IOException {
        InputStream stream1 = FileUtils.openInputStream(file);
        EncodingDetector detector = new UniversalEncodingDetector();
        Charset charset = detector.detect(new BufferedInputStream(stream1), new Metadata());
        stream1.close();
        if (charset != null) {
            return FileUtils.readFileToString(file, charset);
        } else {
            return null;
        }
    }

    public static String parseContent(InputStream stream) {
        String content = null;
        try {
            AutoDetectParser parser = new AutoDetectParser();
            BodyContentHandler handler = new BodyContentHandler(Integer.MAX_VALUE);
            Metadata metadata = new Metadata();
            parser.parse(stream, handler, metadata);
            content = handler.toString();
        } catch (Exception e) {
            log.error("tika parse error", e);
        }
        return content;
    }

}
