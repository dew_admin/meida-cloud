package com.meida.starter.ocr.hander;

/**
 * ocr转换结果监听
 *
 * @author zyf
 */
public interface BaseOcrListener {

    /**
     * 文档识别成功回调
     *
     * @return
     */
    String ocrSuccess(Long fileId,String content);
}
