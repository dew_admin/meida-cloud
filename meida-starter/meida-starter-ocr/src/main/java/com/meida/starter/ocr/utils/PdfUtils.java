package com.meida.starter.ocr.utils;

import cn.hutool.core.util.StrUtil;
import com.meida.common.base.utils.FlymeUtils;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.ofdrw.converter.ConvertHelper;
import org.ofdrw.converter.ofdconverter.PDFConverter;
import org.ofdrw.converter.ofdconverter.TextConverter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * pdf处理
 *
 * @author zyf
 */
public class PdfUtils {


    private static String getContent(String path) throws Exception {
        //创建输入流对象
        FileInputStream fileInputStream = new FileInputStream(path);
        //创建解析器对象
        PDFParser pdfParser = new PDFParser(new RandomAccessBuffer(fileInputStream));
        pdfParser.parse();
        //pdf文档
        PDDocument pdDocument = pdfParser.getPDDocument();
        //pdf文本操作对象,使用该对象可以获取所读取pdf的一些信息
        PDFTextStripper pdfTextStripper = new PDFTextStripper();
        String content = pdfTextStripper.getText(pdDocument);
        //PDDocument对象时使用完后必须要关闭
        pdDocument.close();
        return content;
    }

    public static List<String> pdfPathToImagePaths(String pdfPath) throws IOException {
        File pdfFile = new File(pdfPath);
        PDDocument pdDocument = PDDocument.load(pdfFile);
        int pageCount = pdDocument.getNumberOfPages();
        PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);
        List<String> imagePathList = new ArrayList<>();
        String fileParent = pdfFile.getParent();
        for (int pageIndex = 0; pageIndex < pageCount; pageIndex++) {
            String imgPath = fileParent + File.separator + UUID.randomUUID().toString() + ".png";
            BufferedImage image = pdfRenderer.renderImageWithDPI(pageIndex, 80, ImageType.RGB);
            ImageIO.write(image, "png", new File(imgPath));
            imagePathList.add(imgPath);
            System.out.println("第张生成的图片路径为{}" + pageIndex + imgPath);
        }
        pdDocument.close();
        return imagePathList;
    }


    public static List<BufferedImage> pdfToBufferedImageList(String pdfPath) throws IOException {
        File pdfFile = new File(pdfPath);
        return pdfToBufferedImageList(pdfFile);
    }

    public static List<BufferedImage> pdfToBufferedImageList(File file) throws IOException {
        List<BufferedImage> bufferedImageList = new ArrayList<>();
        if (FlymeUtils.isNotEmpty(file)) {
            PDDocument pdDocument = PDDocument.load(file);
            int pageCount = pdDocument.getNumberOfPages();
            PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);
            for (int pageIndex = 0; pageIndex < pageCount; pageIndex++) {
                BufferedImage image = pdfRenderer.renderImageWithDPI(pageIndex, 80, ImageType.RGB);
                bufferedImageList.add(image);
            }
            pdDocument.close();
        }
        return bufferedImageList;
    }


    /**
     * 多个图片合成一个pdf
     */
    public static void manyImageToOnePdf(List<String> imgages, String target) throws IOException {
        PDDocument doc = new PDDocument();
        //创建一个空的pdf文件
        doc.save(target);

        PDPage page;
        PDImageXObject pdImage;
        PDPageContentStream contents;
        BufferedImage bufferedImage;
        String fileName;
        float w, h;
        File tempFile;
        int index;

        for (int i = 0; i < imgages.size(); i++) {
            tempFile = new File(imgages.get(i));
            fileName = tempFile.getName();
            index = fileName.lastIndexOf(".");
            if (index == -1) {
                continue;
            }
            bufferedImage = ImageIO.read(tempFile);
            //Retrieving the page
            pdImage = LosslessFactory.createFromImage(doc, bufferedImage);
            w = pdImage.getWidth();
            h = pdImage.getHeight();
            page = new PDPage(new PDRectangle(w, h));
            contents = new PDPageContentStream(doc, page);
            contents.drawImage(pdImage, 0, 0, w, h);
            contents.close();
            doc.addPage(page);
        }
        //保存pdf
        doc.save(target);
        //关闭pdf
        doc.close();
    }

    /**
     * description: pdf合并工具类
     * date: 2023年-05月-16日 16:01
     * author: ldd
     *
     * @param files
     * @param targetPath
     * @return java.io.File
     */
    public static File mulFile2One(List<File> files, String targetPath) throws Exception {
        PDFMergerUtility mergePdf = new PDFMergerUtility();
        for (File f : files) {
            if (f.exists() && f.isFile()) {
                // 循环添加要合并的pdf
                mergePdf.addSource(f);
            }
        }
        // 设置合并生成pdf文件名称
        mergePdf.setDestinationFileName(targetPath);
        // 合并pdf
        mergePdf.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
        return new File(targetPath);
    }

    /**
     * pdf 转换为ofd
     *
     * @param sourceFile
     * @return
     */
    public static String pdf2Ofd(String sourceFile, String targetFile) throws Exception {
        String target = StrUtil.isEmpty(targetFile) ? sourceFile.replace("pdf", "ofd") : targetFile;
        Path src = Paths.get(sourceFile);
        Path dst = Paths.get(target);
        PDFConverter converter = new PDFConverter(dst);
        converter.convert(src);

    /*   //实例化PdfDocument类的对象
        PdfDocument pdf = new PdfDocument();

        //加载PDF文档
        pdf.loadFromFile(fileUrl);

        //保存为OFD格式
        pdf.saveToFile(target, FileFormat.OFD);*/
        return target;
    }

    /**
     * 将ofd格式转换为pdf
     *
     * @param sourceFile
     * @return
     */
    public static String ofd2Pdf(String sourceFile, String targetFile) throws Exception {
       /* //加载OFD文档
        OfdConverter converter = new OfdConverter("ai创意赛.ofd");
        //保存为PDF格式
        converter.toPdf("结果文档.pdf");
*/
        String target = StrUtil.isEmpty(targetFile) ? sourceFile.replace("ofd", "pdf") : targetFile;

        Path src = Paths.get(sourceFile);
        Path dst = Paths.get(target);

        ConvertHelper.toPdf(src, dst);
        return target;

    }

    public static String txt2ofd(String fileUrl) throws Exception {
        Path src = Paths.get(fileUrl);
        String target = fileUrl.replace("txt", "ofd");
        Path dst = Paths.get(target);
        TextConverter converter = new TextConverter(dst);
        converter.convert(src);
        return target;

    }

    /**
     * 提取PDF所有内容
     *
     * @param fileStr
     * @return
     * @throws IOException
     */
    public static String ExtractAllText(String fileStr) throws IOException {
        File file = new File(fileStr);
        PDDocument doc = PDDocument.load(file);
        PDFTextStripper textStripper = new PDFTextStripper();
        String text = textStripper.getText(doc);
        doc.close();
        return text;
    }

    public static String ExtractFileAllText(File file) throws IOException {
        PDDocument doc = PDDocument.load(file);
        PDFTextStripper textStripper = new PDFTextStripper();
        String text = textStripper.getText(doc);
        doc.close();
        return text;
    }

    /**
     * 分页提取PDF内容
     *
     * @param fileStr
     * @return
     * @throws IOException
     */
    public static String ExtractPageText(String fileStr) throws IOException {
        File file = new File(fileStr);
        RandomAccessFile is = new RandomAccessFile(file, "r");
        PDFParser parser = new PDFParser((RandomAccessRead) is);
        parser.parse();
        PDDocument doc = parser.getPDDocument();
        PDFTextStripper textStripper = new PDFTextStripper();
        StringBuffer text = new StringBuffer();
        for (int i = 1; i <= doc.getNumberOfPages(); i++) {
            textStripper.setStartPage(i);
            textStripper.setEndPage(i);
            textStripper.setSortByPosition(true);//一次输出多个页时，按顺序输出
            text.append("当前页：" + i + "\n");
            text.append(textStripper.getText(doc));
        }
        doc.close();
        return text.toString();
    }

    /**
     * 提取PDF指定页码内容
     *
     * @param fileStr
     * @param page
     * @return
     * @throws IOException
     */
    public static String ExtractPageText(String fileStr, int page) throws IOException {
        File file = new File(fileStr);
        RandomAccessFile is = new RandomAccessFile(file, "r");
        PDFParser parser = new PDFParser((RandomAccessRead) is);
        parser.parse();
        PDDocument doc = parser.getPDDocument();
        PDFTextStripper textStripper = new PDFTextStripper();
        textStripper.setStartPage(page);
        textStripper.setEndPage(page);
        StringBuffer text = new StringBuffer();
        text.append("当前页：" + page + "\n");
        text.append(textStripper.getText(doc));
        doc.close();
        return text.toString();
    }

    /**
     * 提取PDF指定页码内容
     *
     * @param fileStr
     * @param startPage
     * @param endPage
     * @return
     * @throws IOException
     */
    public static String ExtractPageText(String fileStr, int startPage, int endPage) throws IOException {
        File file = new File(fileStr);
        RandomAccessFile is = new RandomAccessFile(file, "r");
        PDFParser parser = new PDFParser((RandomAccessRead) is);

        parser.parse();

        PDDocument doc = parser.getPDDocument();
        PDFTextStripper textStripper = new PDFTextStripper();
        StringBuffer text = new StringBuffer();
        if (doc.getNumberOfPages() < endPage) {
            endPage = doc.getNumberOfPages();
        }
        for (int i = startPage; i <= endPage; i++) {
            textStripper.setStartPage(i);
            textStripper.setEndPage(i);
            textStripper.setSortByPosition(true);//一次输出多个页时，按顺序输出
            text.append("当前页：" + i + "\n");
            text.append(textStripper.getText(doc));
        }
        doc.close();
        return text.toString();
    }

    public static void main(String[] agrs) {
        try {
            //String content=   getContent("D:\\test.pdf");
            //pdfPathToImagePaths("D:\\test.pdf");
            //System.out.println(content);


            File file = null;
            try {
                URL url = new URL("file:///" + "http://139.196.136.137:85/archive/2023/03/04/f6c61c9414664405b9e165a922fc694d.jpg");
                file = new File(url.toURI());
            } catch (MalformedURLException | URISyntaxException e) {
                e.printStackTrace();
            }


            String fileurl = "/Users/mhsdong/Desktop/yycx/uploadtemp/a.pdf";
            String[] split = fileurl.split("\\.");

            pdf2Ofd(fileurl, split[0] + "-转换.ofd");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
