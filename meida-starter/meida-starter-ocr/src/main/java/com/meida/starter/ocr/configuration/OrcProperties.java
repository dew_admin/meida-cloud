package com.meida.starter.ocr.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zyf
 */
@Data
@ConfigurationProperties(prefix = "meida.ocr")
public class OrcProperties {
    /**
     * 语言包路径
     */
    private String dataPath;


}
