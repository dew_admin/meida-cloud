package com.meida.starter.ocr.configuration;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zyf
 */
@Configuration
@EnableConfigurationProperties(value = {OrcProperties.class})
public class OcrConfiguration {
    @Bean
    public ITesseract tesserAct(OrcProperties orcProperties) {
        ITesseract instance = new Tesseract();
        //如果未将tessdata放在根目录下需要指定绝对路径
        instance.setDatapath(orcProperties.getDataPath());
        //如果需要识别英文之外的语种，需要指定识别语种，并且需要将对应的语言包放进项目中
        instance.setLanguage("eng+chi_sim");
        return instance;
    }
}
