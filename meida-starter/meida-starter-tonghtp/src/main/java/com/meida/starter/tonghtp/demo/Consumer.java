//package com.thp.demo.Demo;
//
//
//
//import com.tongtech.client.consumer.PullResult;
//import com.tongtech.tonglinkq.core.TLQTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//
//import javax.xml.transform.Result;
//import java.nio.charset.StandardCharsets;
//
///**
// * @author ：liuzh
// * @date ：Created in 2021/11/26
// * @description：Pull message and file test example
// */
//
//
//@Controller
//
//public class Consumer {
//
//    @Autowired
//    TLQTemplate tlqTemplate;
//
//    @Value("${tlq.consumer.ReceiveFilePath}")
//    private String ReceiveFilePath;
//
//    @Value("#{${tlq.consumer.PullMsgNums}}")
//    private Integer PullMsgNums;
//
//    @Value("#{${tlq.consumer.PullMsgBatchNums}}")
//    private Integer PullMsgBatchNums;
//
//
//    Result result=new Result();
//    PullResult pullResult = null;
//
//    @RequestMapping("/syncPull")
//    @ResponseBody
//    public Result syncPullTest() {
//
//        for (int i = 0; i < PullMsgNums; i++) {
//
//            try {
//
//                pullResult = tlqTemplate.syncPullMessage(PullType.PullContinue, 0, PullMsgBatchNums, 3000);
//
//                if ((pullResult != null) && (pullResult.getPullStatus() == PullStatus.FOUND)) {
//                    System.out.println("拉取消息成功,结果：" + pullResult);
//                    result.setCode(ResultCode.SUCCESS);
//                    result.setMsg("拉取消息成功,详细内容在终端显示");
//                }
//
//                if ((pullResult != null) && (pullResult.getPullStatus() == PullStatus.NO_NEW_MSG)) {
//                    System.out.println("拉取空消息,结果:" + pullResult);
//                    result.setCode(ResultCode.Empty);
//                    result.setMsg("拉取空消息,详细内容在终端显示");
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("拉取消息异常,详细内容在终端显示");
//            }
//        }
//
//        return result;
//    }
//
//    @RequestMapping("/syncPullFile")
//    @ResponseBody
//    public Result syncPullFileTest() {
//
//        try {
//            String NewReceiveFilePath=new String(ReceiveFilePath.getBytes(StandardCharsets.ISO_8859_1),StandardCharsets.UTF_8);
//            DownloadResult downloadRequest = tlqTemplate.syncPullFileMessage(NewReceiveFilePath,
//                    PullType.PullContinue, 0, 200000);
//
//            if ((downloadRequest != null && downloadRequest.getStatus() == DownloadFileStatus.DOWNLOAD_OK)) {
//                System.out.println("文件下载成功:" + downloadRequest);
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg("下载文件成功,详细内容在终端显示");
//            }
//
//            if ((downloadRequest != null && downloadRequest.getStatus() == DownloadFileStatus.DOWNLOAD_FAILED)) {
//                System.out.println("文件下载失败:" + downloadRequest);
//                result.setCode(ResultCode.FAIL);
//                result.setMsg("下载文件失败,详细内容在终端显示");
//
//            }
//
//            if ((downloadRequest != null && downloadRequest.getStatus() == DownloadFileStatus.DOWNLOAD_FILE_NOT_EXIST)) {
//                System.out.println("文件无法下载:" + downloadRequest);
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("无法下载文件,详细内容在终端显示");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("下载文件异常,详细内容在终端显示");
//        }
//
//        return result;
//    }
//
//    @RequestMapping("/asyncPull")
//    @ResponseBody
//    public Result asyncPullTest() {
//
//        try {
//
//            for (int i = 0; i < PullMsgNums; i++) {
//
//                tlqTemplate.asyncPullMessage(PullType.PullContinue, 0, PullMsgBatchNums, new PullCallback() {
//
//                    @Override
//                    public void onSuccess(PullResult pullResult) {
//
//                        if ((pullResult != null && pullResult.getPullStatus() == PullStatus.FOUND)) {
//                            System.out.println("拉取消息成功: " + pullResult);
//                            result.setCode(ResultCode.SUCCESS);
//                            result.setMsg("拉取消息成功,详细内容在终端显示");
//                        }
//
//                        if ((pullResult != null && pullResult.getPullStatus() == PullStatus.NO_NEW_MSG)) {
//                            System.out.println("拉取空消息: " + pullResult);
//                            result.setCode(ResultCode.Empty);
//                            result.setMsg("拉取空消息,详细内容在终端显示");
//                        }
//
//                    }
//
//                    @Override
//                    public void onException(Throwable throwable) {
//                        System.out.println("拉取消息异常: " + throwable);
//                        result.setCode(ResultCode.Unkonwn);
//                        result.setMsg("拉取消息异常,详细内容在终端显示");
//                    }
//                }, 3000);
//
//                Thread.sleep(150);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("拉取消息异常,详细内容在终端显示");
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/asyncPullFile")
//    @ResponseBody
//    public Result asyncPullFileTest() {
//
//        try {
//
//            String NewReceiveFilePath=new String(ReceiveFilePath.getBytes(StandardCharsets.ISO_8859_1),StandardCharsets.UTF_8);
//            tlqTemplate.asyncPullFileMessage(NewReceiveFilePath, PullType.PullContinue, 0,
//                    20000, new DownloadCallback() {
//
//                        @Override
//                        public void onSuccess(DownloadResult downloadResult) {
//
//                            if ((downloadResult != null && downloadResult.getStatus() == DownloadFileStatus.DOWNLOAD_OK)) {
//                                System.out.println("文件下载成功:" + downloadResult);
//                                result.setCode(ResultCode.SUCCESS);
//                                result.setMsg("下载文件成功,详细内容在终端显示");
//                            }
//
//                            if ((downloadResult != null && downloadResult.getStatus() == DownloadFileStatus.DOWNLOAD_FAILED)) {
//                                System.out.println("文件下载失败:" + downloadResult);
//                                result.setCode(ResultCode.FAIL);
//                                result.setMsg("下载文件失败,详细内容在终端显示");
//                            }
//
//                            if ((downloadResult != null && downloadResult.getStatus() == DownloadFileStatus.DOWNLOAD_FILE_NOT_EXIST)) {
//                                System.out.println("文件无法下载:" + downloadResult);
//                                result.setCode(ResultCode.Unkonwn);
//                                result.setMsg("无法下载文件,详细内容在终端显示");
//                            }
//
//                        }
//
//                        @Override
//                        public void onException(Throwable throwable) {
//                            System.out.println("文件下载异常: " + throwable);
//                            result.setCode(ResultCode.Unkonwn);
//                            result.setMsg("下载文件异常,详细内容在终端显示");
//                        }
//
//                    });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("下载文件异常,详细内容在终端显示");
//        }
//
//        return result;
//    }
//
//}
