//package com.thp.demo.Demo;
//
//import com.thp.demo.Utils.Result;
//import com.thp.demo.Utils.ResultCode;
//import com.tongtech.client.common.TopicType;
//import com.tongtech.client.exception.TLQClientException;
//import com.tongtech.client.remoting.exception.RemotingException;
//import com.tongtech.tonglinkq.core.TLQTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * @author ：liuzh
// * @date ：Created in 2021/11/26
// * @description：Operational Domain and Topic Test Example
// */
//
//
//@Controller
//public class Manager {
//
//    @Value("${tlq.manager.domain}")
//    public String domainName;
//
//    @Value("${tlq.manager.topic}")
//    public String topicName;
//
//    @Autowired
//    TLQTemplate tlqTemplate;
//
//    Result result = new Result();
//
//
//    @RequestMapping("/creatDomain")
//    @ResponseBody
//    public Result creatDomainTest() {
//
//        try {
//
//            if ((tlqTemplate.getManager().createDomain(domainName))) {
//                System.out.println(domainName + "创建成功");
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg("通信域创建成功,详细内容在终端显示");
//            } else {
//                System.out.println(domainName + "创建失败");
//                result.setCode(ResultCode.FAIL);
//                result.setMsg("通信域创建失败,详细内容在终端显示");
//            }
//
//        } catch (TLQClientException e) {
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (RemotingException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    @RequestMapping("/creatTopic")
//    @ResponseBody
//    public Result creatTopicTest() {
//
//        try {
//
//            if ((tlqTemplate.getManager().createTopic(topicName, TopicType.EVENT, domainName))) {
//                System.out.println(domainName + " 成功创建 " + TopicType.EVENT + " 主题 " + topicName);
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg("主题创建成功,详细内容在终端显示");
//            } else {
//                System.out.println(domainName + " 创建 " + TopicType.EVENT + " 主题 " + topicName + " 失败 ");
//                result.setCode(ResultCode.FAIL);
//                result.setMsg("主题创建失败,详细内容在终端显示");
//            }
//
//        } catch (TLQClientException e) {
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (RemotingException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/query")
//    @ResponseBody
//    public Result queryTest() {
//
//        try {
//
//            if ((tlqTemplate.getManager().queryDomainExist(domainName))) {
//                System.out.println(domainName + "存在!");
//            } else {
//                System.out.println(domainName + "不存在!");
//            }
//
//            if ((tlqTemplate.getManager().queryTopicExist(topicName, domainName))) {
//                System.out.println(domainName + "存在" + topicName);
//            } else {
//                System.out.println(domainName + "不存在 " + topicName);
//            }
//
//            System.out.println(domainName + "创建时间: " + tlqTemplate.getManager().queryDomainUpdateTime(domainName));
//            System.out.println("统计当前集群已创建的通信域数量: " + tlqTemplate.getManager().queryDomainCount());
//            System.out.println(domainName + "中的主题数量: " + tlqTemplate.getManager().queryTopicCount(domainName));
//            result.setCode(ResultCode.SUCCESS);
//            result.setMsg("查询成功,详细内容在终端显示");
//
//        } catch (TLQClientException e) {
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (RemotingException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/modify")
//    @ResponseBody
//    public Result modifyTest() {
//
//        try {
//
//            if ((tlqTemplate.getManager().modifyTopicType(domainName, topicName, TopicType.STATE))) {
//                System.out.println(topicName + "的类型修改为: " + TopicType.STATE + "成功");
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg("修改主题类型成功,详细内容在终端显示");
//            } else {
//                System.out.println(topicName + "的类型修改为: " + TopicType.STATE + "失败");
//                result.setCode(ResultCode.FAIL);
//                result.setMsg("修改主题失败,详细内容在终端显示");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/deleteTopic")
//    @ResponseBody
//    public Result deleteTopicTest() {
//
//        try {
//
//            if ((tlqTemplate.getManager().deleteTopic(topicName, domainName))) {
//                System.out.println(domainName + "删除" + topicName + "成功");
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg(domainName + "删除" + topicName + "成功" + ",详细内容在终端显示");
//            } else {
//                System.out.println(domainName + "删除" + topicName + "失败");
//                result.setCode(ResultCode.FAIL);
//                result.setMsg(domainName + "删除" + topicName + "失败" + ",详细内容在终端显示");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/deleteDomain")
//    @ResponseBody
//    public Result deleteDomainTest() {
//
//        try {
//
//
//            if ((tlqTemplate.getManager().deleteDomain(domainName))) {
//                System.out.println(domainName + "删除成功");
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg(domainName + "删除成功" + ",详细内容在终端显示");
//            } else {
//                System.out.println(domainName + "删除失败");
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg(domainName + "删除失败" + ",详细内容在终端显示");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("异常,详细内容在终端显示");
//        }
//
//        return result;
//
//    }
//
//    public static void main(String[] args) {
//
//    }
//
//
//}
