package com.meida.starter.tonghtp.adapter;

import com.meida.mq.adapter.MqTemplate;
import com.meida.mq.exception.MqException;
import com.meida.starter.tonghtp.converter.TonghtpMessageConverter;
import com.tongtech.client.message.Message;
import com.tongtech.client.producer.SendResult;
import com.tongtech.client.producer.SendStatus;
import com.tongtech.tonglinkq.core.TLQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * <b>功能名：TonghtpTemplate</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-17 jiabing
 */

@Component
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "tonghtp")
public class TonghtpTemplate implements MqTemplate {
    @Autowired
    TLQTemplate tlqTemplate;

    final TonghtpMessageConverter converter = new TonghtpMessageConverter();

    @Override
    public void convertAndSend(Object var1) throws MqException {
        convertAndSend("default",var1);
    }

    @Override
    public void convertAndSend(String var1, Object var2) throws MqException {
        Message message = new Message();
        message.setTopicOrQueue(var1);
        message.setBody(converter.fromMessage(var2));

        try {

            SendResult sendResult = tlqTemplate.syncSend(message, 5000);

            if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_OK)) {
                System.out.println("成功,结果: " + sendResult);

            }

            if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_FAILED)) {
                System.out.println("失败,结果: " + sendResult);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    /**
     * 延迟发送消息
     *
     * @param queueName  队列名称
     * @param params     消息内容params
     * @param expiration 延迟时间 单位毫秒
     */
    public void sendMessage(String queueName, Object params, Integer expiration) {
        Message message = new Message();
        message.setTopicOrQueue(queueName);
        /**
         * 消息延时等级，如果为消息设置延时属性，消费该消息必须等待消息延时等级对应的时间才能消
         * 费。延时消息暂定有18个时间等级，分别对应延长时间1s、5s、10s、30s、1min、2min、3min、
         * 4min、5min、6min、7min、8min、9min、10min、20min、30min、1h、2h。
         */
        Integer delay = expiration/1000;
        Integer delayStr = 18;
        if(delay<=1){
            delayStr = 1;
        }else if(delay<=5){
            delayStr = 2;
        }else if(delay<=10){
            delayStr = 3;
        }else if(delay<=30){
            delayStr = 4;
        }else if(delay<=60){
            delayStr = 5;
        }else if(delay<=120){
            delayStr = 6;
        }else if(delay<=180){
            delayStr = 7;
        }else if(delay<=240){
            delayStr = 8;
        }else if(delay<=300){
            delayStr = 9;
        }else if(delay<=360){
            delayStr = 10;
        }else if(delay<=420){
            delayStr = 11;
        }else if(delay<=480){
            delayStr = 12;
        }else if(delay<=540){
            delayStr = 13;
        }else if(delay<=600){
            delayStr = 14;
        }else if(delay<=1200){
            delayStr = 15;
        }else if(delay<=1800){
            delayStr = 16;
        }else if(delay<=3600){
            delayStr = 17;
        }else if(delay<=7200){
            delayStr = 18;
        }
        message.setDelayTimeLevel(delayStr);
        message.setBody(converter.fromMessage(params));

        try {

            SendResult sendResult = tlqTemplate.syncSend(message, 5000);

            if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_OK)) {
                System.out.println("成功,结果: " + sendResult);

            }

            if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_FAILED)) {
                System.out.println("失败,结果: " + sendResult);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String queueName, Object params) {
        convertAndSend(queueName,params);
    }

    @Override
    public Object convertSendAndReceive(String queueName, Object param) {
        convertAndSend(queueName,param);
        return param;
    }

    @Override
    public void publishEvent(Object object) {
//        EventObj eventObj = new EventObj();
//        eventObj.setEntity(object);
//        publisher.publishEvent(new MeidaRemoteApplicationEvent(eventObj, busProperties.getId()));
        //不支持该服务
    }
}