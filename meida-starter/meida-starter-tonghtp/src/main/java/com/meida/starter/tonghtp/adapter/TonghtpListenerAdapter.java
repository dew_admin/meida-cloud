package com.meida.starter.tonghtp.adapter;

import cn.hutool.core.util.ObjectUtil;
import com.meida.mq.adapter.MqListenerAdapter;
import com.meida.mq.annotation.MsgListenerObj;
import com.meida.mq.handler.MqMsgHandler;
import com.meida.starter.tonghtp.consumer.TongHtpMsgConsumer;
import com.tongtech.client.common.TopicType;
import com.tongtech.client.exception.TLQClientException;
import com.tongtech.client.remoting.exception.RemotingException;
import com.tongtech.tonglinkq.core.TLQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <b>功能名：TonghtpListenerAdapter</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-17 jiabing
 */
@Component
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "tonghtp")
public class TonghtpListenerAdapter implements MqListenerAdapter {

    @Autowired
    private MqMsgHandler mqMsgHandler;

    @Value("${tlq.domain}")
    private String domain;

    @Autowired
    TLQTemplate tlqTemplate;

    @Autowired
    TongHtpMsgConsumer tongHtpMsgConsumer;

    private ThreadPoolExecutor poolExecutor;

    public TonghtpListenerAdapter() {
        LinkedBlockingDeque<Runnable> deque = new LinkedBlockingDeque<Runnable>();
        poolExecutor = new ThreadPoolExecutor(4, 20, 5, TimeUnit.SECONDS, deque);
        poolExecutor.allowCoreThreadTimeOut(true);
    }

    @Override
    public void initMqListener(Class<?> clazz, String methodName, MsgListenerObj param) {
        mqMsgHandler.addMsgListenerObj(param);
        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                tongHtpMsgConsumer.asyncPullLoop(param);
            }
        });
        initQueue(param.getQueues());
        //消息监听
//        myMessageListenerContainer.addQueueNames(param.getQueues());
//        initQueue(param.getQueues());
    }


    /**
     * 初始化队列
     */
    private void initQueue(String[] queues) {

        //创建交换机
        if (ObjectUtil.isNotEmpty(queues)) {
            for (String queueName : queues) {
                try {
                    boolean flag = tlqTemplate.getManager().queryTopicExist(queueName,domain);
                    if(!flag){
                        continue;
                    }
                } catch (RemotingException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TLQClientException e) {
                    e.printStackTrace();
                }

                try {
                    tlqTemplate.getManager().createTopic(queueName, TopicType.STATE,domain);
                } catch (RemotingException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TLQClientException e) {
                    e.printStackTrace();
                }

            }
        }
    }




//    public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
//        try {
//            //realTodo
//            channel.basicAck(deliveryTag, false);
//        } catch (Exception e) {
//            log.info("接收消息失败,重新放回队列",e.getMessage());
//            try {
//                /**
//                 * deliveryTag:该消息的index
//                 * multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
//                 * requeue：被拒绝的是否重新入队列
//                 */
//                channel.basicNack(deliveryTag, false, true);
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//    }
//
//    private String rabbitmqListenerTemplate(){
//        String temp = " public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {";
//        return temp;
//    }
}