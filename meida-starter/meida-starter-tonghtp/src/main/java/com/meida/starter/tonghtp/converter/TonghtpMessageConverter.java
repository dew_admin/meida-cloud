package com.meida.starter.tonghtp.converter;

import com.tongtech.client.consumer.PullResult;
import com.tongtech.client.message.MessageExt;
import org.springframework.remoting.rmi.CodebaseAwareObjectInputStream;
import org.springframework.util.ClassUtils;

import java.io.*;

/**
 * <b>功能名：TonghtpMessageConverter</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-17 jiabing
 */
public class TonghtpMessageConverter{

    private ClassLoader beanClassLoader = ClassUtils.getDefaultClassLoader();

    public Object toMessage(MessageExt message){

        if (message == null) {
            return null;
        } else {
            try {
                byte[] byteBody = message.getBody();
                return createObjectInputStream(new ByteArrayInputStream(byteBody),null).readObject();
            } catch (IOException var2) {
                throw new IllegalArgumentException("Could not deserialize object", var2);
            } catch (ClassNotFoundException var3) {
                throw new IllegalStateException("Could not deserialize object type", var3);
            }
        }
//                try {
//                    content = SerializationUtils.deserialize(this.createObjectInputStream(new ByteArrayInputStream(message.getBody()), this.codebaseUrl));
//                } catch (IllegalArgumentException | IllegalStateException | IOException var7) {
//                    throw new MessageConversionException("failed to convert serialized Message content", var7);
//                }
//            }


//        return message;
    }

    public byte[] fromMessage(Object object){
        if (object == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            new ObjectOutputStream(stream).writeObject(object);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Could not serialize object of type: " + object.getClass(), e);
        }
        return stream.toByteArray();
    }

    protected ObjectInputStream createObjectInputStream(InputStream is, String codebaseUrl) throws IOException {
        return new CodebaseAwareObjectInputStream(is, this.beanClassLoader, codebaseUrl) {
            @Override
            protected Class<?> resolveClass(ObjectStreamClass classDesc) throws IOException, ClassNotFoundException {
                Class<?> clazz = super.resolveClass(classDesc);
                return clazz;
            }
        };
    }
}   