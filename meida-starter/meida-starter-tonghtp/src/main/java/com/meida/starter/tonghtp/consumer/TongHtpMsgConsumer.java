package com.meida.starter.tonghtp.consumer;

import com.meida.mq.annotation.MsgListenerObj;
import com.meida.mq.handler.MqMsgHandler;
import com.meida.starter.tonghtp.converter.TonghtpMessageConverter;
import com.tongtech.client.common.ModeType;
import com.tongtech.client.consumer.PullCallback;
import com.tongtech.client.consumer.PullResult;
import com.tongtech.client.consumer.PullStatus;
import com.tongtech.client.consumer.common.PullType;
import com.tongtech.client.consumer.common.SubscribeType;
import com.tongtech.client.consumer.impl.TLQPullConsumer;
import com.tongtech.client.message.MessageExt;
import com.tongtech.tonglinkq.core.TLQTemplate;
import com.tongtech.tonglinkq.properties.TLQProperties;
import com.tongtech.tonglinkq.support.TLQListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <b>功能名：TongHtpMsgConsumer</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-17 jiabing
 */
@Component
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "tonghtp")
public class TongHtpMsgConsumer {
    @Autowired
    TLQTemplate tlqTemplate;

    @Autowired
    MqMsgHandler mqMsgHandler;

    @Autowired
    private TLQProperties tlqProperties;



    @Value("${tlq.consumer.PullMsgBatchNums:1}")
    private Integer PullMsgBatchNums;
    @Value("${tlq.consumer.group}")
    private String consumerGroup;
    @Value("${tlq.mode-type}")
    private String modeType;

    private AtomicLong counter = new AtomicLong(0L);

//    private void registerContainer(MsgListenerObj param) {
//        String containerBeanName = String.format("%s_%s", TLQListenerContainer.class.getName(), this.counter.incrementAndGet());
//        GenericApplicationContext genericApplicationContext = (GenericApplicationContext)this.applicationContext;
//        genericApplicationContext.registerBean(containerBeanName, TLQListenerContainer.class, () -> {
//            return this.createRocketMQListenerContainer(containerBeanName, param.getBean(), annotation);
//        }, new BeanDefinitionCustomizer[0]);
//        TLQListenerContainer container = (TLQListenerContainer)genericApplicationContext.getBean(containerBeanName, TLQListenerContainer.class);
//        if (!container.isRunning()) {
//            try {
//                container.start();
//            } catch (Exception var9) {
//                log.error("Started container failed. {}", container, var9);
//                throw new RuntimeException(var9);
//            }
//        }
//
//        log.info("Register the listener to container, listenerBeanName:{}, containerBeanName:{}", beanName, containerBeanName);
//    }
//
//    private TLQListenerContainer createRocketMQListenerContainer(String name, Object bean, TLQMessageListener annotation) {
//        TLQListenzerContainer container = new TLQListenerContainer();
//        container.setTLQMessageListener(annotation);
//        container.setNameServer(this.tlqProperties.getNameSrvUrl());
//        container.setBrokeServer(this.tlqProperties.getBrokeUrl());
//        container.setTopicOrQueue(this.environment.resolvePlaceholders(annotation.topicOrQueue()));
//        container.setSrvname(this.environment.resolvePlaceholders(annotation.srvname()));
//        container.setConsumerGroup(this.environment.resolvePlaceholders(annotation.consumerGroup()));
//        if (TLQListener.class.isAssignableFrom(bean.getClass())) {
//            container.setTlqListener((TLQListener)bean);
//        }
//
//        container.setName(name);
//        return container;
//    }



    public void asyncPullLoop(MsgListenerObj param) {

        TLQPullConsumer consumer = null;
        try {
//            tlqTemplate.registerMessageListener(new MessageListenerImpl() {
//                @Override
//                public ConsumeConcurrentlyStatus getMessage(PullRequestResult pullRequestResult, RequestContext requestContext) {
//                    System.out.println("111111111111111111111111111111111111");
//                    System.out.println(pullRequestResult);
//                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
//                }
//            });


            consumer = new TLQPullConsumer(consumerGroup);
            consumer.setNamesrvAddr(tlqProperties.getNameSrvUrl());
            consumer.subscribe(param.getQueues()[0]);
            consumer.setDomain(tlqProperties.getDomain());
            if (ModeType.QUEUE.name().equals(modeType)) {
                consumer.setModeType(ModeType.QUEUE);
            }

            consumer.setAutoCommit(tlqProperties.getConsumer().isAutoCommit());
            if (tlqProperties.getConsumer().getSubscribeType() == 0) {
                consumer.setSubscribeType(SubscribeType.TLQ_SUB_DURABLE);
            } else {
                consumer.setSubscribeType(SubscribeType.TLQ_SUB_NON_DURABLE);
            }

            consumer.start();
            if(1==1){
                return;
            }
            final MsgListenerObj tempObj = new MsgListenerObj(param.getId(),param.getQueues(),param.getTopic(),param.getBean(),param.getMethod());

            while (true) {
                try {
                    PullResult pullResult = consumer.pullMessage(PullType.PullContinue, 0, PullMsgBatchNums,30000L);
                    String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(new Date());
                    if ((pullResult != null && pullResult.getPullStatus() == PullStatus.FOUND)) {
                        System.out.println(Thread.currentThread().getName()+"【"+ time +"】拉取消息【"+param.getQueues()[0]+"】成功: " + pullResult);
                        List<MessageExt> mesages = pullResult.getMsgFoundList();
                        List<MsgListenerObj> listenerObjList = mqMsgHandler.getListenerObjs();
                        mesages.forEach(item->{
                            TonghtpMessageConverter converter = new TonghtpMessageConverter();
                            Object msgObj = converter.toMessage(item);
                            mqMsgHandler.doMsgListener(tempObj, msgObj, null);
                        });
                    }

                    if ((pullResult != null && pullResult.getPullStatus() == PullStatus.NO_NEW_MSG)) {
                        System.out.println(Thread.currentThread().getName()+"【"+ time +"】拉取【"+param.getQueues()[0]+"】空消息: " + pullResult);
                        try {
                            Thread.sleep(3000L);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception throwable){
                    String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(new Date());
                    System.out.println(Thread.currentThread().getName()+"【"+ time +"】拉取【"+param.getQueues()[0]+"】消息异常: " + throwable);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            //重新拉去
            if(tlqTemplate!=null&&tlqTemplate.getConsumer()!=null){
                try {
                    Thread.sleep(5000);
                    System.out.println("重启");
                    asyncPullLoop(param);
                    if(consumer!=null){
                        consumer.shutdown();
                    }
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }

            }
        }
    }

}