//package com.thp.demo.Demo;
//
//import com.thp.demo.Utils.Result;
//import com.thp.demo.Utils.ResultCode;
//import com.tongtech.client.message.FileMessage;
//import com.tongtech.client.message.Message;
//import com.tongtech.client.producer.*;
//import com.tongtech.tonglinkq.core.TLQTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.io.File;
//import java.nio.charset.StandardCharsets;
//import java.util.ArrayList;
//import java.util.Collection;
//
//
///**
// * @author ：liuzh
// * @date ：Created in 2021/11/26
// * @description:Send message and file test example
// */
//
//@Controller
//public class Producer {
//
//    @Autowired
//    TLQTemplate tlqTemplate;
//
//    @Value("${tlq.producer.SendFilePath}")
//    private String SendFilePath;
//
//    @Value("${tlq.producer.MsgQueueOrTopic}")
//    private String MsgQueueOrTopic;
//
//    @Value("#{${tlq.producer.SendMsgNums}}")
//    private Integer SendMsgNums;
//
//    @Value("#{${tlq.producer.SendMsgBatchNums}}")
//    private Integer SendMsgBatchNums;
//
//
//    Result result = new Result();
//    SendResult sendResult = null;
//
//    @RequestMapping(value = "/synSend")
//    @ResponseBody
//    public Result synSendTest() throws Exception {
//
//        System.out.println("同步发送消息");
//
//        for (int i = 0; i < SendMsgNums; i++) {
//
//            Message message = new Message();
//            message.setTopicOrQueue(MsgQueueOrTopic);
//            message.setBody(("TLQ9_" + i).getBytes());
//
//            try {
//
//                sendResult = tlqTemplate.syncSend(message, 3000);
//
//                if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_OK)) {
//                    System.out.println("成功,结果: " + sendResult);
//                    result.setCode(ResultCode.SUCCESS);
//                    result.setMsg("发送消息成功,详细内容在终端显示");
//                }
//
//                if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_FAILED)) {
//                    System.out.println("失败,结果: " + sendResult);
//                    result.setCode(ResultCode.FAIL);
//                    result.setMsg("发送消息失败,详细内容在终端显示");
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送消息异常,详细内容在终端显示");
//            }
//        }
//
//        return result;
//
//    }
//
//
//    @RequestMapping("/syncSendBatch")
//    @ResponseBody
//    public Result syncSendBatchTest() throws Exception {
//
//        System.out.println("同步发送批量消息");
//
//        Collection messages = new ArrayList();
//
//        for (int i = 0; i < SendMsgBatchNums; i++) {
//            Message message = new Message();
//            message.setBody(("TLQ9_" + i).getBytes());
//            message.setTopicOrQueue(MsgQueueOrTopic);
//            messages.add(message);
//        }
//
//        for (int i = 0; i < SendMsgNums; i++) {
//
//            try {
//
//                SendBatchResult sendBatchResult = tlqTemplate.syncSendBatch(messages, 2000);
//
//                if ((sendBatchResult != null && sendBatchResult.getSendStatus() == SendStatus.SEND_OK)) {
//                    System.out.println("成功,结果: " + sendBatchResult);
//                    result.setCode(ResultCode.SUCCESS);
//                    result.setMsg("发送消息成功,详细内容在终端显示");
//                }
//
//                if ((sendBatchResult != null && sendBatchResult.getSendStatus() == SendStatus.SEND_FAILED)) {
//                    System.out.println("失败,结果: " + sendBatchResult);
//                    result.setCode(ResultCode.FAIL);
//                    result.setMsg("发送消息失败,详细内容在终端显示");
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送消息异常,详细内容在终端显示");
//            }
//
//        }
//        return result;
//    }
//
//    @RequestMapping("/synSendFile")
//    @ResponseBody
//    public Result syncSendFileTest() throws Exception {
//
//        System.out.println("同步发送文件");
//        String NewSendFilePath=new String(SendFilePath.getBytes(StandardCharsets.ISO_8859_1),StandardCharsets.UTF_8);
//        File file = new File(NewSendFilePath);
//        FileMessage fileMessage = new FileMessage();
//        fileMessage.setFile(file);
//        fileMessage.setTopicOrQueue(MsgQueueOrTopic);
//        try {
//
//            SendFileResult sendFileResult = tlqTemplate.syncSendFile(fileMessage, 20000000);
//
//            if ((sendFileResult != null && sendFileResult.getSendStatus() == SendStatus.SEND_OK)) {
//                System.out.println("成功,结果:" + sendFileResult);
//                result.setCode(ResultCode.SUCCESS);
//                result.setMsg("发送文件成功,详细内容在终端显示");
//            }
//
//            if ((sendFileResult != null && sendFileResult.getSendStatus() == SendStatus.FILE_EXIST)) {
//                System.out.println("文件已经存在,结果:" + sendFileResult);
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送文件已经存在,详细内容在终端显示");
//            }
//
//            if ((sendFileResult != null && sendFileResult.getSendStatus() == SendStatus.SEND_FAILED)) {
//                System.out.println("发送文件失败,结果:" + sendFileResult);
//                result.setCode(ResultCode.FAIL);
//                result.setMsg("发送文件失败,详细内容在终端显示");
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            result.setCode(ResultCode.Unkonwn);
//            result.setMsg("发送文件异常,详细内容在终端显示");
//        }
//
//        return result;
//
//    }
//
//
//    @RequestMapping("/asyncSend")
//    @ResponseBody
//    public Result asyncSendTest() {
//
//        System.out.println("异步发送消息");
//
//        for (int i = 0; i < SendMsgNums; i++) {
//
//            Message message = new Message();
//            message.setBody(("TLQ_" + i).getBytes());
//            message.setTopicOrQueue(MsgQueueOrTopic);
//
//            try {
//
//                tlqTemplate.asyncSend(message, new SendCallback() {
//
//                    @Override
//                    public void onSuccess(SendResult sendResult) {
//
//                        if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_OK)) {
//                            System.out.println("成功,结果:" + sendResult);
//                            result.setCode(ResultCode.SUCCESS);
//                            result.setMsg("发送消息成功,详细内容在终端显示");
//                        }
//
//                        if ((sendResult != null && sendResult.getSendStatus() == SendStatus.SEND_FAILED)) {
//                            System.out.println("失败,结果:" + sendResult);
//                            result.setCode(ResultCode.FAIL);
//                            result.setMsg("发送消息失败,详细内容在终端显示");
//                        }
//                    }
//
//                    @Override
//                    public void onException(Throwable throwable) {
//                        System.out.println("异常,结果: " + throwable);
//                    }
//                }, 2000);
//
//                Thread.sleep(100);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送消息异常,详细内容在终端显示");
//            }
//
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/asyncSendBatch")
//    @ResponseBody
//    public Result asyncSendBatchTest() {
//
//        Collection messages = new ArrayList();
//
//        System.out.println("异步发送批量消息");
//
//        for (int i = 0; i < SendMsgBatchNums; i++) {
//
//            Message message = new Message();
//            message.setBody(("TLQ_" + i).getBytes());
//            message.setTopicOrQueue(MsgQueueOrTopic);
//            messages.add(message);
//
//        }
//
//        for (int i = 0; i < SendMsgNums; i++) {
//
//            try {
//
//                tlqTemplate.asyncSendBatch(messages, new SendBatchCallback() {
//
//                    @Override
//                    public void onSuccess(SendBatchResult sendBatchResult) {
//
//                        if ((sendBatchResult != null) && sendBatchResult.getSendStatus() == SendStatus.SEND_OK) {
//                            System.out.println("成功,结果: " + sendBatchResult);
//                            result.setCode(ResultCode.SUCCESS);
//                            result.setMsg("发送消息成功,详细内容在终端显示");
//                        }
//
//                        if ((sendBatchResult != null) && sendBatchResult.getSendStatus() == SendStatus.SEND_FAILED) {
//                            System.out.println("失败,结果: " + sendBatchResult);
//                            result.setCode(ResultCode.FAIL);
//                            result.setMsg("发送消息失败,详细内容在终端显示");
//                        }
//                    }
//
//                    @Override
//                    public void onException(Throwable throwable) {
//                        System.out.println("异常,结果: " + throwable);
//                        result.setCode(ResultCode.Unkonwn);
//                        result.setMsg("发送消息异常,详细内容在终端显示");
//                    }
//
//                }, 3000);
//
//                Thread.sleep(100);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送消息异常,详细内容在终端显示");
//            }
//
//        }
//
//        return result;
//
//    }
//
//    @RequestMapping("/asyncSendFile")
//    @ResponseBody
//    public Result asyncSendFileTest() throws Exception {
//
//        System.out.println("异步发送文件 ");
//
//
//        FileMessage fileMessage = new FileMessage();
//        String NewSendFilePath=new String(SendFilePath.getBytes(StandardCharsets.ISO_8859_1),StandardCharsets.UTF_8);
//        File file = new File(NewSendFilePath);
//        fileMessage.setFile(file);
//        fileMessage.setTopicOrQueue(MsgQueueOrTopic);
//
//        tlqTemplate.asyncSendFile(fileMessage, new SendFileCallback() {
//
//            @Override
//            public void onSuccess(SendFileResult sendFileResult) {
//
//                if (sendFileResult.getSendStatus() == SendStatus.SEND_OK) {
//                    System.out.println("成功,结果: " + sendFileResult);
//                    result.setCode(ResultCode.SUCCESS);
//                    result.setMsg("发送文件成功,详细内容在终端显示");
//                }
//
//                if (sendFileResult.getSendStatus() == SendStatus.SEND_FAILED) {
//                    System.out.println("失败,结果: " + sendFileResult);
//                    result.setCode(ResultCode.FAIL);
//                    result.setMsg("发送文件失败,详细内容在终端显示");
//                }
//
//                if (sendFileResult.getSendStatus() == SendStatus.FILE_EXIST) {
//                    System.out.println("文件已经存在,结果：" + sendFileResult);
//                    result.setCode(ResultCode.Unkonwn);
//                    result.setMsg("发送文件已经存在,详细内容在终端显示");
//                }
//            }
//
//            @Override
//            public void onException(Throwable throwable) {
//
//                System.out.println("异常: " + throwable);
//                result.setCode(ResultCode.Unkonwn);
//                result.setMsg("发送文件异常,详细内容在终端显示");
//
//            }
//        }, 30000);
//
//        return result;
//    }
//
//}
