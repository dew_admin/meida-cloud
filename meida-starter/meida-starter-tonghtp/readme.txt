本文档介绍两种运行方式:直接运行jar和搭建springboot项目运行.
注意:Demo文件中的示例程序已经写入jar中.Demo文件中的jar用于导入springboot项目.


一、运行jar方式
(1)进入主目录RunJar的config文件中，修改配置文件SpringBoot.properties，参考配置application.properties
(2)将配置信息赋给SpringBootTest.jar。 进入主目录RunJar,终端输入命令 java -jar -Dspring.config.location=./config/SpringBoot.properties ./SpringBootTest.jar 
(3)运行示例   参考 3.运行示例






二、搭建SpringBoot环境运行
1.安装以下软件并完成相关配置
 1)Maven(建议使用3.3.9及以上版本)
 2)jdk (使用1.8及以上版本)
 3)idea(2019.02及以上版本)
(2)将相关jar导入Mave本地仓库
1)java客户端jar导入 (默认存放目录  /home/.../HTPClient/java/lib）
  mvn install:install-file -DgroupId=com.tongtech.client  -DartifactId=tongtech-client -Dversion=2.0.0.0  -Dfile=/home/.../Demo/tlq9client.jar -Dpackaging=jar
  mvn deploy:deploy-file -DgroupId=com.tongtech.client -DartifactId=tongtech-client -Dversion=2.0.0.0 -Dpackaging=jar -Dfile=E:\tmp\tlq9client.jar -Durl=https://repo.rdc.aliyun.com/repository/63342-release-0cjGf8/ -DrepositoryId=rdc-releases
  版权声明：本文为CSDN博主「上火了怎么办」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
  原文链接：https://blog.csdn.net/qq_44884300/article/details/89135195
2)springboot项目jar导入 (默认存放目录  /home/.../HTPClient/java/lib）
  mvn install:install-file -DgroupId=com.tongtech.client -DartifactId=springboot -Dversion=2.0.0.0 -Dfile=/home/.../Demo/tonglinkq-spring-boot-starter-4.0-SNAPSHOT.jar -Dpackaging=jar
    mvn deploy:deploy-file -DgroupId=com.tongtech.client -DartifactId=springboot -Dversion=2.0.0.0 -Dpackaging=jar -Dfile=E:\tmp\tonglinkq-spring-boot-starter-4.0-SNAPSHOT.jar -Durl=https://repo.rdc.aliyun.com/repository/63342-release-0cjGf8/ -DrepositoryId=rdc-releases
  
 2.创建springboot项目
(1)创建空SpringBoot项目
(2)在pom.xml配置文件添加jar依赖
<!--添加java客户端jar-->
        <dependency>
            <groupId>com.tongtech.client</groupId>
            <artifactId>tongtech-client</artifactId>
            <version>2.0.0.0</version>
        </dependency>
 <!--添加springboot的jar包-->
        <dependency>
            <groupId>com.tongtech.client</groupId>
            <artifactId>springboot</artifactId>
            <version>2.0.0.0</version>
        </dependency>

(3)配置application.properties
	tlq.domai=domain1
	tlq.name-srv-url=tcp://10.10.82.105:9933
	tlq.consumer.group=my-group
	tlq.consumer.MsgQueueOrTopic=topic1
	tlq.mode-type=TOPIC
	tlq.producer.SendFilePath=/home/hui/video/file8.txt
	tlq.consumer.ReceiveFilePath=/home/hui/video/rev
	tlq.producer.MsgQueueOrTopic=topic1
	tlq.producer.SendMsgNums=50
	tlq.producer.SendMsgBatchNums=2
	tlq.consumer.PullMsgNums=5
	tlq.consumer.PullMsgBatchNums=2
	tlq.manager.topic=topic555
	tlq.manager.domain=domain555
	server.port=8010
说明：
tlq.domai：通信域名称，支持英文字母、阿拉伯数字、特殊字符（下划线）的任意组合，且以英文字母开头，最大长度64字节。
tlq.name-srv-url：管理节点IP和端口号，tcp[udp]://ipv4地址:端口号（必须修改，需和TLQ9_Mgr/config/nameServer.xml中的<IpAddress>及<ListenPort>相同）。
tlq.consumer.group：消费者组名称，支持英文字母、阿拉伯数字、特殊字符（下划线、点）的任意组合，且以英文字母开头，最大长度32字节，目前仅适用发布订阅模式，并且对消费者起作用。
tlq.consumer..MsgQueueOrTopic：消费者拉取消息的主题名称，支持英文字母、阿拉伯数字、特殊字符（下划线、点）的任意组合，且以英文字母开头，最大长度128字节。
tlq.mode-type：应用模式，取值TOPIC和QUEUE，TOPIC：发布订阅模式；QUEUE：点对点模式。
tlq.user-proxy：0：普通工作节点；1：代理工作节点，默认为0。
tlq.producer.retry-times-when-send-failed：消息发送失败的重试次数。
tlq.producer.SendFilePath：发送文件的存放目录。
tlq.consumer.ReceiveFilePath：接收文件的存放目录。
tlq.producer.MsgQueueOrTopic：发送消息的主题或者队列名称，支持英文字母、阿拉伯数字、特殊字符（下划线、点）的任意组合，且以英文字母开头，最大长度128字节。
tlq.producer.SendMsgNums：发送消息数量。
tlq.producer.SendMsgBatchNums：批量发送消息时，每批消息的数量。
tlq.consumer.PullMsgNums：拉取消息数量。
tlq.consumer.PullMsgBatchNums：批量拉取消息时，拉取的每批消息数量。
tlq.manager.topic:新建主题名称
tlq.manager.domain:新建通信域名称
server.port：浏览器运行的端口号。

注意： 收发消息与收发文件不要使用同一个通信域的主题名；
(4)编写Demo	参考TLQ9_Cli/samples/demo_java/SpringBoot/Demo目录中示例.

3.运行示例
以发布订阅模式同步发送消息为例
打开浏览器输入 http://localhost:8010/synSend
	
	
	
	
运行命令
1.同步发送消息           	
http://localhost:8010/synSend
2.同步发送批量消息       	
http://localhost:8010/syncSendBatch
3.同步发送文件           	
http://localhost:8010/synSendFile
4.异步发送消息          	
http://localhost:8010/asyncSend
5.异步发送批量消息		
http://localhost:8010/asyncSendBatch
6.异步发送文件			
http://localhost:8010/asyncSendFile
7.同步拉取消息			
http://localhost:8010/syncPull
8.同步拉取文件			
http://localhost:8010/syncPullFile
9.异步拉取消息			
http://localhost:8010/asyncPull
10.异步拉取文件			
http://localhost:8010/asyncPullFile 
11.新建通信域 	
http://localhost:8010/creatDomain
12.新建主题	
http://localhost:8010/creatTopic
13.查询		
http://localhost:8010/query
14.修改		
http://localhost:8010/modify
15.删除主题	
http://localhost:8010/deleteTopic
16.删除通信域  
http://localhost:8010/deleteDomain
	
