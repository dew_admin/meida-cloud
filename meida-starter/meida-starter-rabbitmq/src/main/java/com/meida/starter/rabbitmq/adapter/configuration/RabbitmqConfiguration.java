package com.meida.starter.rabbitmq.adapter.configuration;

import com.meida.mq.annotation.MsgListenerObj;
import com.meida.mq.handler.MqMsgHandler;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.ConsumerTagStrategy;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


/**
 * <b>功能名：AdapterConfiguration</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-15 jiabing
 */

@Configuration
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "rabbitmq")
@Slf4j
public class RabbitmqConfiguration {


    @Bean("myMessageListenerContainer")
    public SimpleMessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory, MqMsgHandler mqMsgHandler){
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //队列可以是多个，参数是String的数组
//        container.setQueueNames("QueueNames");
        //设置当前并发消费者数(通道数)
        container.setExposeListenerChannel(true);
        container.setConcurrentConsumers(10);
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setMaxConcurrentConsumers(100);
        //是否重回队列
        container.setDefaultRequeueRejected(true);
        container.setConsumerTagStrategy(new ConsumerTagStrategy() {
            @Override
            public String createConsumerTag(String queue) {
                return queue + "_" + UUID.randomUUID().toString();
            }
        });
        container.setErrorHandler((Throwable throwable)->{
            System.out.println("============================Rabbitmq监听异常=============================");
            throwable.printStackTrace();
            log.error("============================Rabbitmq监听异常=============================");
            log.error(throwable.getMessage());
        });

        container.setMessageListener(new ChannelAwareMessageListener(){
            @Override
            //得到了Channel参数，具体使用会在下面的博客详细讲解
            public void onMessage(Message message, Channel channel) throws Exception {
                try{
                    String queue = message.getMessageProperties().getConsumerQueue();
                    List<MsgListenerObj> listenerObjList = mqMsgHandler.getListenerObjs();
                    MsgListenerObj listenerObj = null;
                    for (int i = 0; i < listenerObjList.size(); i++) {
                        MsgListenerObj temp = listenerObjList.get(i);
                        if (Arrays.asList(temp.getQueues()).contains(queue)) {
                            listenerObj = temp;
                            break;
                        }
                    }

                    if (listenerObj != null) {
                        MessageConverter converter = new SimpleMessageConverter();
                        Object msgObj = converter.fromMessage(message);
                        mqMsgHandler.doMsgListener(listenerObj, msgObj, null);
                        try{

                            channel.basicAck( message.getMessageProperties().getDeliveryTag(), false);
                        }catch (Exception ee){
                            ee.printStackTrace();
                            log.error("",ee);
                        }
//                message.getMessageProperties().
//                    System.out.println("====接收到消息=====");
//                    System.out.println(message.getMessageProperties());
//                    System.out.println(new String(message.getBody()));
                    }else{
                        byte[] b = message.getBody();
                        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//                    Set<String> queues = new HashSet<>();
//                    listenerObjList.forEach(item->{
//                        List<String> l = Arrays.asList(item.getQueues());
//                        queues.addAll(l);
//                    });
//                    String[] initQueue = container.getQueueNames();
//                    Set<String> initQueueSet = new HashSet<String>(Arrays.asList(initQueue));
//                    initQueueSet.removeAll(queues);
//                    if(initQueueSet.size()>0){
//                        container.removeQueueNames(initQueueSet.toArray(new String[0]));
//                    }
//                        try{
//                            channel.basicNack( message.getMessageProperties().getDeliveryTag(),false, true);
//                        }catch (Exception ee){
//                            ee.printStackTrace();
//                            log.error("",ee);
//                        }
//
//                        container.removeQueueNames(queue);
                    }
                    System.out.println(String.format("queue=[%s] rabbitmq消息处理完成11111",queue));
                }catch (Exception e){
                    e.printStackTrace();
                    log.error("Rabbitmq监听异常",e);
                }

            }

        });
        return container;
    }
}