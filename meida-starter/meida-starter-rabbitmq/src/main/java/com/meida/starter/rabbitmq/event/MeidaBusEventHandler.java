package com.meida.starter.rabbitmq.event;

/**
 * 业务模块消息处理器接口
 */
public interface MeidaBusEventHandler<T> {
    /**
     * 使用条件
     *
     * @param eventObj
     * @return
     */
    default  boolean support(EventObj<T> eventObj){
        return true;
    };

    void onMessage(EventObj<T> eventObj);
}
