package com.meida.starter.rabbitmq.event;

import cn.hutool.core.util.ObjectUtil;
import com.meida.common.base.utils.FlymeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 监听远程事件,并分发消息到业务模块消息处理器
 *
 * @author zyf
 */
@Component
public class BaseApplicationEvent implements ApplicationListener<MeidaRemoteApplicationEvent> {

    @Autowired(required = false)
    Map<String, MeidaBusEventHandler> meidaBusEventHandlerMap;

    @Override
    public void onApplicationEvent(MeidaRemoteApplicationEvent remoteApplicationEvent) {
        EventObj eventObj = remoteApplicationEvent.getEventObj();
        if (ObjectUtil.isNotEmpty(eventObj)) {
            String handlerName = eventObj.getHandlerName();
            MeidaBusEventHandler meidaBusEventHandler = meidaBusEventHandlerMap.get(handlerName);
            //获取业务模块消息处理器
            if (FlymeUtils.isNotEmpty(meidaBusEventHandler)) {
                meidaBusEventHandler.onMessage(eventObj);
            } else {
                for (MeidaBusEventHandler busEventHandler : meidaBusEventHandlerMap.values()) {
                    if (busEventHandler.support(eventObj)) {
                        busEventHandler.onMessage(eventObj);
                    }
                }
            }
        }
    }

}
