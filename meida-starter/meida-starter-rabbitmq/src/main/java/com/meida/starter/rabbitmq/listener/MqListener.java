package com.meida.starter.rabbitmq.listener;

import com.rabbitmq.client.Channel;

/**
 * @author zyf
 */
public interface MqListener<T> {

    /**
     * 业务处理
     *
     * @param map
     * @param channel
     */
    default void handler(T map, Channel channel) {
    }

}
