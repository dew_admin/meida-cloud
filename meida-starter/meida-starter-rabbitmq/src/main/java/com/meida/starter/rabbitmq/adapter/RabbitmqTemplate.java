package com.meida.starter.rabbitmq.adapter;

import com.meida.mq.adapter.MqTemplate;
import com.meida.mq.exception.MqException;
import com.meida.starter.rabbitmq.client.RabbitMqClient;
import com.meida.starter.rabbitmq.core.DelayExchangeBuilder;
import com.meida.starter.rabbitmq.event.EventObj;
import com.meida.starter.rabbitmq.event.MeidaRemoteApplicationEvent;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.bus.BusProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <b>功能名：RabbitmqTemplate</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-15 jiabing
 */
@Component
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "rabbitmq")
public class RabbitmqTemplate implements MqTemplate {

    @Autowired
    public AmqpTemplate amqpTemplate;

    @Autowired
    private RabbitMqClient rabbitMqClient;

    @Resource
    BusProperties busProperties;
    @Resource
    private ApplicationEventPublisher publisher;

    @Override
    public void convertAndSend(Object var1) throws MqException {
        amqpTemplate.convertAndSend(var1);
    }

    @Override
    public void convertAndSend(String var1, Object var2) throws MqException {
        amqpTemplate.convertAndSend(var1,var2);
    }

    @Override
    public void sendMessage(String queueName, Object params, Integer expiration) {
        amqpTemplate.convertAndSend(DelayExchangeBuilder.DEFAULT_DELAY_EXCHANGE, queueName, params, message -> {
            if (expiration != null && expiration > 0) {
                message.getMessageProperties().setHeader("x-delay", expiration);
            }
            return message;
        });
        //rabbitMqClient.sendMessage(queueName,params,expiration);
    }

    @Override
    public void sendMessage(String queueName, Object params) {
        rabbitMqClient.sendMessage(queueName,params,0);
    }

    @Override
    public Object convertSendAndReceive(String queueName, Object param) {
        return amqpTemplate.convertSendAndReceive(queueName,param);
    }

    @Override
    public void publishEvent(Object object) {
        EventObj eventObj = new EventObj();
        eventObj.setEntity(object);
        publisher.publishEvent(new MeidaRemoteApplicationEvent(eventObj, busProperties.getId()));
    }


}