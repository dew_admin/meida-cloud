package com.meida.starter.rabbitmq.event;

import lombok.Data;

import java.io.Serializable;

/**
 * 远程事件数据对象
 * @author Administrator
 */
@Data
public class EventObj<T> implements Serializable {

    private T entity;
    /**
     * 自定义业务模块消息处理器beanName
     */
    private String handlerName;
}
