package com.meida.starter.rabbitmq.adapter;

import cn.hutool.core.util.ObjectUtil;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.mq.adapter.MqListenerAdapter;
import com.meida.mq.annotation.MsgListenerObj;
import com.meida.mq.handler.MqMsgHandler;
import com.meida.starter.rabbitmq.core.DelayExchangeBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Properties;

/**
 * <b>功能名：RabbitmqListenerAdapter</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-10-14 jiabing
 */
@Component
@Slf4j
@ConditionalOnProperty(prefix = "meida.mq",name="provider",havingValue = "rabbitmq")
public class RabbitmqListenerAdapter implements MqListenerAdapter {

    @Autowired
    private MqMsgHandler mqMsgHandler;


    @Resource(name = "myMessageListenerContainer")
    private SimpleMessageListenerContainer myMessageListenerContainer;

    @Autowired
    private RabbitAdmin rabbitAdmin;

    @Override
    public void initMqListener(Class<?> clazz, String methodName, MsgListenerObj param) {
        mqMsgHandler.addMsgListenerObj(param);
        myMessageListenerContainer.addQueueNames(param.getQueues());
        initQueue(param.getQueues());
    }

    /**
     * 初始化队列
     */
    private void initQueue(String[] queues) {
        DirectExchange directExchange = new DirectExchange(DelayExchangeBuilder.DELAY_EXCHANGE, true, false);

        //创建交换机
        rabbitAdmin.declareExchange(directExchange);
        if (ObjectUtil.isNotEmpty(queues)) {
            for (String queueName : queues) {
                Properties result = rabbitAdmin.getQueueProperties(queueName);
                if (FlymeUtils.isEmpty(result)) {
                    Queue queue = new Queue(queueName, true);
                    rabbitAdmin.declareQueue(queue);
                    Binding binding = BindingBuilder.bind(queue).to(directExchange).with(queueName);
                    rabbitAdmin.declareBinding(binding);
                    log.info("创建队列:" + queueName);
                } else {
                    log.info("已有队列:" + queueName);
                }
            }
        }
    }



//    public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
//        try {
//            //realTodo
//            channel.basicAck(deliveryTag, false);
//        } catch (Exception e) {
//            log.info("接收消息失败,重新放回队列",e.getMessage());
//            try {
//                /**
//                 * deliveryTag:该消息的index
//                 * multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
//                 * requeue：被拒绝的是否重新入队列
//                 */
//                channel.basicNack(deliveryTag, false, true);
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//    }
//
//    private String rabbitmqListenerTemplate(){
//        String temp = " public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {";
//        return temp;
//    }
}