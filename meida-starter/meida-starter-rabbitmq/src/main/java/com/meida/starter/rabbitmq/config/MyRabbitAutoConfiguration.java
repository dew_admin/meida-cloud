package com.meida.starter.rabbitmq.config;


import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

/**
 * 定义RabbitMQ的启动配置类，可以通过配置变量来控制启用、禁用
 * @author zyf
 */
@Configuration
@ConditionalOnProperty("spring.rabbitmq.enable")
public class MyRabbitAutoConfiguration extends RabbitAutoConfiguration {

}

