package com.meida.starter.thirdlogin.controller;


import com.google.common.collect.Maps;
import com.meida.common.constants.AuthConstants;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.starter.thirdlogin.service.GiteeAuthServiceImpl;
import com.meida.starter.thirdlogin.service.QQAuthServiceImpl;
import com.meida.starter.thirdlogin.service.WechatAuthServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author: 三方登录
 * @date: 2018/11/9 15:43
 */
@Api(tags = "三方登录")
@RestController
@AllArgsConstructor
public class ThirdLoginController {

    private final QQAuthServiceImpl qqAuthService;

    private final WechatAuthServiceImpl wechatAuthService;

    private final GiteeAuthServiceImpl giteeAuthService;

    /**
     * 自定义oauth2错误页
     *
     * @param request
     * @return
     */
    @RequestMapping("/oauth/error")
    @ResponseBody
    public Object handleError(HttpServletRequest request) {
        Object error = request.getAttribute("error");
        return error;
    }


    /**
     * 获取第三方登录配置
     *
     * @return
     */
    @ApiOperation(value = "获取第三方登录配置", notes = "获取第三方登录配置")
    @GetMapping("/oauth/login/config")
    @ResponseBody
    public ResultBody getLoginOtherConfig() {
        Map<String, String> map = Maps.newHashMap();
        map.put(AuthConstants.LOGIN_QQ, qqAuthService.getAuthorizationUrl());
        map.put(AuthConstants.LOGIN_WECHAT, wechatAuthService.getAuthorizationUrl());
        map.put(AuthConstants.LOGIN_GITEE, giteeAuthService.getAuthorizationUrl());
        return ResultBody.ok(map);
    }
}
