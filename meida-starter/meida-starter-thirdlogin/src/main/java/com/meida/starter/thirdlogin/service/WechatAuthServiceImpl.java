package com.meida.starter.thirdlogin.service;

import com.alibaba.fastjson.JSONObject;
import com.meida.common.oauth2.OpenOAuth2ClientDetails;
import com.meida.common.oauth2.OpenOAuth2ClientProperties;
import com.meida.common.oauth2.OpenOAuth2Service;
import com.meida.common.security.http.OpenRestTemplate;
import com.meida.common.constants.AuthConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信oauth2认证实现类
 *
 * @author zyf
 */
@Service("wechatAuthService")
@Slf4j
public class WechatAuthServiceImpl implements OpenOAuth2Service {
    @Autowired
    private OpenRestTemplate restTemplate;
    @Autowired
    private OpenOAuth2ClientProperties socialAuthProperties;
    /**
     * 微信 登陆页面的URL
     */
    private final static String AUTHORIZATION_URL = "%s?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect";
    /**
     * 获取token的URL
     */
    private final static String ACCESS_TOKEN_URL = "%s?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    /**
     * 获取用户信息的 URL，oauth_consumer_key 为 apiKey
     */
    private static final String USER_INFO_URL = "%s?access_token=%s&openid=%s&lang=zh_CN";


    @Override
    public String getAuthorizationUrl() {
        String url = String.format(AUTHORIZATION_URL, getClientDetails().getUserAuthorizationUri(), getClientDetails().getClientId(), getClientDetails().getRedirectUri(), getClientDetails().getScope(), System.currentTimeMillis());
        return url;
    }

    @Override
    public JSONObject getAccessToken(String code) {
        JSONObject jsonObject = new JSONObject();
        String url = String.format(ACCESS_TOKEN_URL, getClientDetails().getAccessTokenUri(), getClientDetails().getClientId(), getClientDetails().getClientSecret(), code);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        URI uri = builder.build().encode().toUri();
        String resp = restTemplate.getForObject(uri, String.class);
        if (resp != null && resp.contains("access_token")) {
            jsonObject = JSONObject.parseObject(resp);
            return jsonObject;
        }
        log.error("QQ获得access_token失败，code无效，resp:{}", resp);
        return jsonObject;
    }


    @Override
    public JSONObject getUserInfo(String accessToken, String openId) {
        String url = String.format(USER_INFO_URL, getClientDetails().getUserInfoUri(), accessToken, openId);
        restTemplate.getMessageConverters().set(1,new StringHttpMessageConverter(Charset.forName("UTF-8")));
        String resp = restTemplate.getForObject(url, String.class);
        JSONObject data = JSONObject.parseObject(resp);
        return data;
    }


    @Override
    public String refreshToken(String code) {
        return null;
    }

    /**
     * 获取登录成功地址
     *
     * @return
     */
    @Override
    public String getLoginSuccessUrl() {
        return getClientDetails().getLoginSuccessUri();
    }

    /**
     * 获取客户端配置信息
     *
     * @return
     */
    @Override
    public OpenOAuth2ClientDetails getClientDetails() {
        return socialAuthProperties.getOauth2().get(AuthConstants.LOGIN_WECHAT);
    }

}
