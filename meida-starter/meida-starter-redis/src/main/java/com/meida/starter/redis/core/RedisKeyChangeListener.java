package com.meida.starter.redis.core;

/**
 * 自定义监听
 *
 * @author zyf
 */
public interface RedisKeyChangeListener {
    /**
     * 获取通道名称
     *
     * @return
     */
    default String getTopicName() {
        return "flyme_redis_topic";
    }


    /**
     * 消息监听
     *
     * @param message
     * @param redisKey
     */

    void onSuccess(String message, String redisKey);
}
