package com.meida.starter.redis.listener;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 */
@Slf4j
public class RedisKeyExpireListener extends KeyExpirationEventMessageListener {


    public RedisKeyExpireListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 注意：我们只能获取到失效的 key 而获取不到 value，
        // 所以在设计 key 的时候可以考虑把 value 一并放到 key，然后根据规则取出 value
        String redisKey = new String(message.getBody());
    }
}
