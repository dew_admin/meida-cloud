package com.meida.starter.redis.core;

import com.meida.common.base.constants.FlymeConstants;
import com.meida.common.base.entity.EntityMap;

/**
 * redis消息监听
 *
 * @author zyf
 */
public interface RedisMessageListener<T> {

    /**
     * 获取通道名称
     *
     * @return
     */
    default String getTopicName() {
        return FlymeConstants.COMMON_TOPIC_NAME;
    }

    /**
     * 消息处理
     *
     * @param message
     */
    default T onMessage(EntityMap message) {
        return null;
    }
}
