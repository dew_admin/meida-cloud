package com.meida.starter.shardingjdbc.base;

import org.apache.shardingsphere.sharding.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.RangeShardingValue;

import java.util.Collection;

/**
 * @author zyf
 */
public interface MyStandardShardingAlgorithm {
    /**
     * 根据分片规则值计算分片表
     *
     * @param collection
     * @param preciseShardingValue
     * @return
     */
    String doSharding(Collection<String> collection, PreciseShardingValue<Long> preciseShardingValue);

    /**
     * 用于处理BETWEEN AND分片，如果不配置RangeShardingAlgorithm，SQL中的BETWEEN AND将按照全库路由处理
     *
     * @param collection
     * @param rangeShardingValue
     * @return
     */
    Collection<String> doSharding(Collection<String> collection, RangeShardingValue<Long> rangeShardingValue);
}
