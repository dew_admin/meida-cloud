package com.meida.starter.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 申请售后原因
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sharding_db")
@TableAlias("sd")
@ApiModel(value = "OrderCause对象", description = "退款方式")
public class ShardingDb extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "shardingdbId", type = IdType.ASSIGN_ID)
    private Long shardingdbId;

    @ApiModelProperty(value = "标题")
    private String shardingdbName;

}
