//package com.meida.starter.shardingjdbc.config;
//
//import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
//import com.baomidou.dynamic.datasource.provider.AbstractDataSourceProvider;
//import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.SpringBootConfiguration;
//import org.springframework.boot.autoconfigure.AutoConfigureBefore;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.context.annotation.Primary;
//
//import javax.annotation.Resource;
//import javax.sql.DataSource;
//import java.util.Map;
//
///**
// * 动态数据源配置：
// * 使用@DS注解，切换数据源@DS("sharding")
// */
//@Configuration
//@AutoConfigureBefore({DynamicDataSourceAutoConfiguration.class, SpringBootConfiguration.class})
//public class DataSourceConfiguration2 {
//    /**
//     * 分表数据源名称
//     */
//    public static final String SHARDING_DATA_SOURCE_NAME = "sharding";
//    /**
//     * 动态数据源配置项
//     */
//    @Autowired
//    private DynamicDataSourceProperties dynamicDataSourceProperties;
//    @Resource
//    private DynamicDataSourceProperties properties;
//
//    /**
//     * shardingjdbc有四种数据源，需要根据业务注入不同的数据源
//     *
//     * <p>1. 未使用分片, 脱敏的名称(默认): shardingDataSource;
//     * <p>2. 主从数据源: masterSlaveDataSource;
//     * <p>3. 脱敏数据源：encryptDataSource;
//     * <p>4. 影子数据源：shadowDataSource
//     * <p>
//     * shardingjdbc默认就是shardingDataSource
//     * 如果需要设置其他的可以使用
//     *
//     * @Resource(value="") 设置
//     */
//    @Lazy
//    @Resource
//    DataSource shardingDataSource;
//    DataSource shardingSphereDataSource;
//
//    /**
//     * 将shardingDataSource放到了多数据源（dataSourceMap）中
//     * 注意有个版本的bug，3.1.1版本 不会进入loadDataSources 方法，这样就一直造成数据源注册失败
//     */
//    @Bean
//    public DynamicDataSourceProvider dynamicDataSourceProvider() {
//        Map<String, DataSourceProperty> datasourceMap = dynamicDataSourceProperties.getDatasource();
//        Map<String, DataSourceProperty> datasourceMap = properties.getDatasource();
//        return new AbstractDataSourceProvider() {
//            @Override
//            public Map<String, DataSource> loadDataSources() {
//                Map<String, DataSource> dataSourceMap = createDataSourceMap(datasourceMap);
//                // 将 shardingjdbc 管理的数据源也交给动态数据源管理
//                dataSourceMap.put(SHARDING_DATA_SOURCE_NAME, shardingDataSource);
//                dataSourceMap.put(SHARDING_DATA_SOURCE_NAME, shardingSphereDataSource);
//                return dataSourceMap;
//            }
//        };
//    }
//
//    /**
//     * 将动态数据源设置为首选的
//     * 当spring存在多个数据源时, 自动注入的是首选的对象
//     * 设置为主要的数据源之后，就可以支持shardingjdbc原生的配置方式了
//     *
//     * @return
//     */
//    @Primary
//    @Bean
//    public DataSource dataSource(DynamicDataSourceProvider dynamicDataSourceProvider) {
//        DynamicRoutingDataSource dataSource = new DynamicRoutingDataSource();
//        dataSource.setPrimary(dynamicDataSourceProperties.getPrimary());
//        dataSource.setStrict(dynamicDataSourceProperties.getStrict());
//        dataSource.setStrategy(dynamicDataSourceProperties.getStrategy());
//        dataSource.setPrimary(properties.getPrimary());
//        dataSource.setStrict(properties.getStrict());
//        dataSource.setStrategy(properties.getStrategy());
//        dataSource.setProvider(dynamicDataSourceProvider);
//        dataSource.setP6spy(dynamicDataSourceProperties.getP6spy());
//        dataSource.setSeata(dynamicDataSourceProperties.getSeata());
//        dataSource.setP6spy(properties.getP6spy());
//        dataSource.setSeata(properties.getSeata());
//        return dataSource;
//    }
//}
//
