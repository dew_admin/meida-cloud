<p align="center">
  <a target="_blank" href="https://nacos.io/en-us/"><img src="https://img.shields.io/badge/Nacos-1.0.0-blue.svg" alt="Nacos"></a>
  <a><img src="https://img.shields.io/badge/Spring%20Cloud-%20Greenwich.SR1-brightgreen.svg" alt="SpringCloud"></a>
  <a><img src="https://img.shields.io/badge/Spring%20Boot-2.1.4-brightgreen.svg" alt="SpringBoot"></a>
  <a><img src="https://img.shields.io/badge/Redis-orange.svg"></a>
  <a><img src="https://img.shields.io/badge/RabbitMq-orange.svg"></a>
  <a target="_blank" href="https://www.iviewui.com/docs/guide/install"><img src="https://img.shields.io/badge/iview-3.1.3-brightgreen.svg?style=flat-square" alt="iview"></a>
  <a><img src="https://img.shields.io/badge/vue-2.5.10-brightgreen.svg?style=flat-square" alt="vue"></a>
  <a><img src="https://img.shields.io/npm/l/express.svg" alt="License"></a>
</p>  

## 美达-微服务智能项目开发通用平台

#### 功能特色
搭建基于微服务的快速开发平台。
+ 分布式架构,采用nacos统一配置中心,服务治理
+ 统一API网关、访问鉴权、参数验签、接口调用更安全
+ 深度整合SpringSecurity+Oauth2,更细粒度、灵活的ABAC权限控制.
+ 模块化开发,插拔式的APP快速开放平台
+ 前后端分离方式开发应用
+ 基础平台和具体项目彻底分离机制,有助于公司产品快速沉淀
+ 模块化建表,依据选择的基础模块自动建表
+ 版本化管理,子项目快速升级
+ 自动化API接口文档生成
+ 灵活的扩展机制,通过编写处理器扩展基础接口,满足实际的业务需求
+ 基础接口统一管理,子项目依赖公用,项目底层功能不在重复编写,拿来即用
+ 基础接口多项目碾压测试,消灭BUG于无形中
+ 功能模块可持续性迭代,满足多场景的需求并逐步稳定下来
+ 避免重复劳动,通过组合模块方式快速开发同类型项目
+ 集成代码生成器,1分钟生成增删改查接口及管理界面
+ 零SQL编写,零XML编写,基于Mybatis-plus动态扩展




``` lua
meida-cloud
├── docs
    ├── bin           -- 执行脚本  
    ├── config        -- 公共配置,用于导入到nacos配置中心   
    ├── sql           -- sql文件
    
├── meida-app    -- APP接口服务模块
    ├── meida-app-boot       --APP接口服务启动模块(port = 8301)  
    ├── meida-app-client     --APP接口客户端
    ├── meida-app-provider   --APP接口服务端 
     
├── meida-common  -- 公共模块
    ├── meida-common-core    -- 提供微服务相关依赖包、工具类、全局异常解析等...
    ├── meida-common-starter -- SpringBoot自动扫描
       
├── meida-gateway  --网关模块
    ├── meida-gateway-boot      -- API开放网关启动模块 
    ├── meida-api-gateway-zuul  -- API开放网关-基于Zuul-(port = 8888)    
     
├── meida-platform    --  平台基础服务模块
    ├── meida-base    --  基础服务
      ├── meida-base-boot-- 平台基础服务启动模块
      ├── meida-base-client    -- 基础服务客户端
      ├── meida-base-provider  -- 基础服务服务端(port = 8233)  
    ├── meida-auth    --  鉴权服务
      ├── meida-base-boot      -- 鉴权服务启动模块
      ├── meida-base-client    -- 鉴权服务客户端
      ├── meida-base-provider  -- 鉴权服务服务端(port = 8211)
    ├── meida-bpm    --  工作流服务
      ├── meida-base-boot      -- 工作流服务启动模块
      ├── meida-base-client    -- 工作量服务客户端
      ├── meida-base-provider  -- 工作流服务服务端(port = 8255)
    ├── meida-msg    --  消息服务
      ├── meida-base-boot      -- 消息服务启动模块
      ├── meida-base-client    -- 消息服务客户端
      ├── meida-base-provider  -- 消息服务服务端(port = 8233)
    ├── meida-scheduler    --  任务调度服务
      ├── meida-base-boot      -- 任务服务启动模块
      ├── meida-base-client    -- 任务服务客户端
      ├── meida-base-provider  -- 任务服务服务端(port = 8501)
    ├── meida-generator    --  代码生成器
├── meida-module--  应用服务模块
    ├── meida-file    --  文件服务
      ├── meida-file-client    -- 文件服务客户端
      ├── meida-file-provider  -- 文件服务服务端
    ├── meida-order    --  订单服务
      ├── meida-order-client    -- 订单客户端
      ├── meida-order-provider  -- 订单服务端
    ├── meida-pay    --  支付服务
      ├── meida-pay-client    -- 支付客户端
      ├── meida-pay-provider  -- 支付服务端
    ├── meida-product    --  产品服务
      ├── meida-product-client    -- 产品客户端
      ├── meida-product-provider  -- 产品服务端
    ├── meida-system    --  公共服务
      ├── meida-system-client    -- 公共客户端
      ├── meida-system-provider  -- 公共服务端
    ├── meida-vip       --  会员服务
      ├── meida-vip-client    -- 会员客户端
      ├── meida-vip-provider  -- 会员服务端
```

#### 快速开始
上手难度：★★★

本项目基于springCloud打造的分布式快速开发框架. 需要了解SpringCloud,SpringBoot开发,分布式原理。

1. 准备环境
    + Java1.8
    + 阿里巴巴Nacos服务发现和注册中心 <a href="https://nacos.io/zh-cn/">nacos.io</a>
    + Redis
    + RabbitMq （需安装rabbitmq_delayed_message_exchange插件 <a href="https://www.rabbitmq.com/community-plugins.html" target="_blank">下载地址</a>）
    + Mysql
    + Maven
    + Nodejs
   
2. 导入sql脚本
    + docs/sql/oauth2.sql
    + docs/sql/base.sql
    + docs/sql/gateway.sql
    + docs/sql/quartz.sql && scheduler.sql
    
3. 导入配置中心,Nacos公共配置 
    + 访问 http://localhost:8848/nacos/index.html 
    + 新建配置 
        + 项目目录/docs/config/db.properties >  db.properties
        + 项目目录/docs/config/rabbitmq.properties > rabbitmq.properties
        + 项目目录/docs/config/redis.properties > redis.properties
        + 项目目录/docs/config/common.properties  > common.properties  
     如图:
     ![输入图片说明](https://gitee.com/uploads/images/2019/0425/231436_fce24434_791541.png "nacos.png")
4. 修改主pom.xml  

    初始化
    ``` bush
        maven clean install
    ```
    本地启动,默认不用修改
    ``` xml
        <!--Nacos配置中心地址-->
        <config.server-addr>127.0.0.1:8848</config.server-addr>
        <!--Nacos配置中心命名空间,用于支持多环境.这里必须使用ID，不能使用名称,默认为空-->
        <config.namespace></config.namespace>
        <!--Nacos服务发现地址-->
        <discovery.server-addr>127.0.0.1:8848</discovery.server-addr>
    ```
    
5. 本地启动(顺序启动)
     1. BaseApplication
     2. AuthApplication
     3. AdminApplication
     4. ZuulGatewayApplication(推荐) 或 ApiGatewayApplication 
     访问 http://localhost:8888
     
6. 前端启动
    ```bush
        npm install 
        npm run dev
    ``` 
    访问 http://localhost:8080
    
7. 项目打包部署  

     maven多环境打包,并替换相关变量
   ```bush
     mvn clean install package -P {dev|test|online}
   ```
    项目启动232323233
    ```bush
    ./docs/bin/startup.sh {start|stop|restart|status} meida-base-boot.jar
    ./docs/bin/startup.sh {start|stop|restart|status} meida-app-boot.jar
    ./docs/bin/startup.sh {start|stop|restart|status} meida-gateway-boot.jar
    ```
