package com.meida.app.test;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.app.boot.BaseApiApplication;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.pay.client.entity.PayCashout;
import com.meida.module.pay.provider.service.PayCashoutService;
import com.meida.module.system.provider.service.SysDictDataService;
import com.meida.module.user.client.entity.AppEducational;
import com.meida.module.user.provider.service.AppEducationalService;
import com.meida.module.user.provider.service.AppWorkhistoryService;
import com.meida.msg.client.model.EmailMessage;
import com.meida.msg.client.model.EmailTplMessage;
import com.meida.msg.provider.dispatcher.MessageDispatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BaseApiApplication.class)
@EnableAutoConfiguration
public class UserTest {

    @Resource
    private AppEducationalService educationalService;

    @Resource
    private AppWorkhistoryService workhistoryService;

    @Resource
    private PayCashoutService payCashoutService;

    @Resource
    private SysDictDataService dictDataService;
    @Autowired
    private MessageDispatcher dispatcher;

    @Test
    public void educationListByUserId() {
        List<EntityMap> list = educationalService.listByUserId(null);
        System.out.println(JSONUtil.toJsonStr(list));
    }

    @Test
    public void workListByUserId() {
        List<EntityMap> list = workhistoryService.listByUserId(1L);
        System.out.println(JSONUtil.toJsonStr(list));
    }



    @Test
    public void listDictDataByType() {
        List<EntityMap> list=dictDataService.listByDictType("education");
        System.out.println(JSONUtil.toJsonStr(list));
    }

    /**
     * 提现记录
     */
    @Test
    public void payCashoutPageListByUserId() {
        EntityMap params = new EntityMap();
        params.put("userId",1);
        payCashoutService.pageListByUserId(params, cq -> {
            cq.select(PayCashout.class,"cashoutId","userId","money","cashoutType","status","reason","createTime");
        });
    }


    @Test
    public void updateAndSaveOld() {
        AppEducational educational = new AppEducational();
        educational.setMajor("ceshi");
        educational.setUserId(1L);
        educational.setEducationalId(IdWorker.getId());
        educational.setSchoolId(1L);
        educationalService.updateAndSaveOld(educational);
    }
    @Test
    public void testMail() {
        EmailTplMessage message = new EmailTplMessage();
        message.setTo(new String[]{"574501933@qq.com"});
        message.setSubject("测试");
        message.setContent("测试内容");
        message.setTplCode("email");
        String code = RandomUtil.randomInt(100000, 999999) + "";
        Map params = new HashMap(1);
        params.put("code", code);
        message.setTplParams(params);
        this.dispatcher.dispatch(message);
        try {
            Thread.sleep(50000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

