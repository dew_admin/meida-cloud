//package com.meida.app.test;
//
//import com.meida.generator.vo.GenerateConfig;
//import com.meida.generator.GeneratorCode;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
///**
// * 代码生成
// *
// * @author: zyf
// * @date: 2019/2/19 15:23
// * @description:
// */
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = GeneratorCode.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@EnableAutoConfiguration
//public class GeneratorMain {
//    @Autowired
//    private GeneratorCode generatorCode;
//
//    @Test
//    public void generator() {
//        GenerateConfig config = new GenerateConfig();
//        config.setOverride(true);
//        //模块父路径
//        config.setParentPackage("com.meida.module");
//        //父级模块
//        config.setBaseModule("meida-module");
//        //子模块名称(不存在可不填写)
//        config.setModuleName("meida-system");
//        //表名称
//        config.setTableName("t_jsons");
//        generatorCode.init(config);
//    }
//}
