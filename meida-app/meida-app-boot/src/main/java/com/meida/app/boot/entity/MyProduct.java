package com.meida.app.boot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.module.product.client.entity.ProdProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_product")
@ApiModel(value = "MyProduct对象", description = "MyProduct对象")
public class MyProduct extends ProdProduct {

    @ApiModelProperty(value = "备注")
    private String remark;

}
