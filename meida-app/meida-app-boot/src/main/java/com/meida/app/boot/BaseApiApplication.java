package com.meida.app.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 底层APP接口服务启动入口
 *
 * @author zyf
 */
@EnableFeignClients(basePackages = {"com.meida.**.feign"})
@SpringBootApplication(scanBasePackages = "com.meida")
@RemoteApplicationEventScan(basePackages = "com.meida")
@EnableDiscoveryClient
@ComponentScan(basePackages={"com.meida"})
public class BaseApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(BaseApiApplication.class, args);
    }

}
