package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.CommonAppService;
import com.meida.common.enums.ColleconEnum;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.file.provider.service.SysFileService;
import com.meida.module.product.client.entity.ProdBrand;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.client.entity.ProdProductdetails;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.provider.service.ProdProductdetailsService;
import com.meida.module.product.provider.service.ProdShopService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysColleconService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 产品详细信息处理器
 */
@Component("commpanyGetHandler")
@Log4j2
public class CommpanyGetHandler implements GetInterceptor {



    @Autowired
    private SysFileService sysFileService;

    @Autowired(required = false)
    private CommonAppService commonAppService;
    /**
     * 查询条件扩展
     *
     * @param cq
     * @return
     */
    @Override
    public void prepare(CriteriaQuery cq,EntityMap params) {
      System.out.println("dsfd");
    }



}
