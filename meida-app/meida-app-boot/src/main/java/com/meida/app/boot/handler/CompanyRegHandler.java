package com.meida.app.boot.handler;

import com.meida.common.enums.AuthStatusEnum;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.user.client.entity.AppUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 企业会员注册扩展,注册账户前先保存企业并把企业ID传递给用户
 * @author zyf
 */
@Component("companyRegHandler")
@Log4j2
public class CompanyRegHandler implements SaveInterceptor<AppUser> {
    @Autowired
    private SysCompanyService companyService;

    @Override
    public void prepare(CriteriaSave cs, EntityMap params,AppUser appUser) {
        //保存企业
        String companyName = cs.getParams("companyName");
        String businessLicNo = cs.getParams("businessLicNo");
        ApiAssert.isNotEmpty("企业名称不能为空", companyName);
        Boolean checkCompany = companyService.checkByCompanyName(companyName, businessLicNo);
        ApiAssert.isFalse("企业已注册", checkCompany);
        SysCompany company = new SysCompany();
        company.setCompanyState(AuthStatusEnum.NOAUTH.getCode());
        company.setCompanyName(companyName);
        company.setBusinessLicNo(businessLicNo);
        companyService.save(company);
        appUser.setCompanyId(company.getCompanyId());
    }

}
