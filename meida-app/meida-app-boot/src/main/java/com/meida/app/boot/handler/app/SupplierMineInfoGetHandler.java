package com.meida.app.boot.handler.app;

import com.meida.module.user.client.entity.AppUser;
import com.meida.common.enums.AuthStatusEnum;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.DateUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.provider.service.ProdShopService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.common.enums.ColleconEnum;
import com.meida.module.system.provider.service.SysColleconService;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.vip.client.entity.VipUser;
import com.meida.module.vip.provider.service.VipUserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

/**
 * 供应商个人中心接口扩展
 */
@Component("supplierMineInfoGetHandler")
@Log4j2
public class SupplierMineInfoGetHandler implements GetInterceptor {
    @Autowired
    SysCompanyService companyService;
    @Autowired
    VipUserService vipUserService;
    @Autowired
    SysColleconService colleconService;
    @Autowired
    ProdShopService shopService;

    /**
     * 查询条件扩展
     *
     * @param cq
     * @return
     */
    @Override
    public void prepare(CriteriaQuery cq, EntityMap params) {
        cq.select(AppUser.class, "companyId");
    }


    /**
     * 返回结果集扩展
     *
     * @param cq
     * @return
     */
    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long useId = OpenHelper.getUserId();
        Long companyId = map.getLong("companyId");
        //企业认证状态
        Integer authStatus = AuthStatusEnum.NOAUTH.getCode();
        //会员到期时间,为空表示未开通或者已过期
        String expiryDate = "";
        //产品被收藏量
        Long myProductCount = 0L;
        //店铺被收藏量
        Long myShopCount = 0L;
        //收藏店铺量
        Long shopCount = 0L;
        //收藏产品量
        Long productCount = 0L;
        //收藏需求量
        Long demandCount = 0L;
        //收藏采购量
        Long purchaseCount = 0L;
        //浏览量
        Long browseCount = 0L;
        if (FlymeUtils.isNotEmpty(companyId)) {
            SysCompany company = companyService.getById(companyId);
            authStatus = company.getCompanyState();
            VipUser vipUser = vipUserService.getByCompanyId(companyId);
            Date date = vipUser.getExpiryDate();
            if (FlymeUtils.isNotEmpty(vipUser)) {
                expiryDate = DateUtils.formatDate(date);
            }
            //会员有效期小于当前日期
            Boolean tag = DateUtils.ltToday(date);
            if (tag) {
                //会员过期
                expiryDate = "";
            }
            myProductCount = colleconService.countCountByTargetCompany(companyId, ColleconEnum.SC, ProdProduct.class);
            //查询店铺
            ProdShop prodShop = shopService.findByCompanyId(companyId);
            Optional<ProdShop> shopOptional = Optional.of(prodShop);
            if (shopOptional.isPresent()) {
                myShopCount = colleconService.countCollecon(companyId, ColleconEnum.SC, ProdShop.class);
            }
        }
        productCount = colleconService.countCountByUser(useId, ColleconEnum.SC, ProdProduct.class);
        shopCount = colleconService.countCountByUser(useId, ColleconEnum.SC, ProdShop.class);
        map.put("companyState", authStatus);
        map.put("expiryDate", expiryDate);
        map.put("myProductCount", myProductCount);
        map.put("myShopCount", myShopCount);
    }
}
