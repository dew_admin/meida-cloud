package com.meida.app.boot.handler;


import com.meida.app.boot.entity.MyProduct;
import com.meida.app.boot.mapper.MyProductMapper;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.query.CriteriaSave;

import com.meida.module.product.client.entity.ProdProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 添加产品
 */
@Component("productSaveHandler")
public class ProductSaveHandler implements SaveInterceptor<ProdProduct> {

    @Autowired
    private MyProductMapper myProductMapper;


    /**
     * 插入前置扩展
     */
    @Override
    public void prepare(CriteriaSave cs, EntityMap params, ProdProduct prodProduct) {

    }

    /**
     * 插入后置扩展
     */
    @Override
    public void complete(CriteriaSave cs, EntityMap params, ProdProduct prodProduct) {


    }

    /**
     * 自定义实体类扩展
     */
    @Override
    public Object getEntity(CriteriaSave cs, EntityMap params) {
        MyProduct myProduct = cs.getEntity(MyProduct.class);
        myProduct.setRemark("扩展数据");
        return myProduct;
    }

    /**
     * 获取自定义Mapper
     *
     * @return
     */
    @Override
    public SuperMapper getMapper() {
        return myProductMapper;
    }
}
