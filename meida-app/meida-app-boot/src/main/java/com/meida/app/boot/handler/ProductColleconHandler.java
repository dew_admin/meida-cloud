package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.system.client.entity.SysColleconGroup;
import org.springframework.stereotype.Component;

/**
 * 收藏的商品
 */
@Component("productColleconHandler")
public class ProductColleconHandler implements PageInterceptor {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        String productName = params.get("productName");
        String groupId = params.get("groupId");
        cq.addSelect("product.productName", "product.coverImage");
        cq.like("productName", productName);
        cq.in("sgc.groupId", groupId);
        cq.createJoin(ProdProduct.class).setMainField("targetId");
        cq.createJoin(SysColleconGroup.class).setJoinField("colleconId");
    }


}
