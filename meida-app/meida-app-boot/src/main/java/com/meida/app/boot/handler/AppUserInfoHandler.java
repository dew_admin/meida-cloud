package com.meida.app.boot.handler;


import com.meida.app.provider.handler.UserInfoHandler;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.user.client.entity.AppUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * APP接口扩展
 *
 * @author zyf
 */
@Component
@Log4j2
public class AppUserInfoHandler implements UserInfoHandler {



    @Override
    public AppUser bindMobile(AppUser appUser) {
        log.info("绑定手机号扩展,accountId:" + appUser.getUserId());
        appUser.setUserType("test");
        return appUser;
    }

    @Override
    public void userReg(CriteriaSave cs) {
        log.info("注册用户扩展,accountId:");
    }

}
