package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.system.client.entity.SysColleconGroup;
import org.springframework.stereotype.Component;

/**
 * 收藏的店铺
 */
@Component("shopColleconHandler")
public class ShopColleconHandler implements PageInterceptor {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        String shopName = params.get("shopName");
        String groupId = params.get("groupId");
        cq.addSelect("shop.shopName", "shop.shopLogo");
        cq.like("shopName", shopName);
        cq.in("sgc.groupId", groupId);
        cq.createJoin(ProdShop.class).setMainField("targetId");
        cq.createJoin(SysColleconGroup.class);
    }
}
