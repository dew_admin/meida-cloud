package com.meida.app.boot.mapper;

import com.meida.app.boot.entity.MyProduct;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.product.client.entity.ProdProduct;


/**
 *  产品Mapper扩展接口
 * @author flyme
 * @date 2019-06-17
 */
public interface MyProductMapper extends SuperMapper<MyProduct> {

}
