package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.pay.provider.service.PayConfigService;
import com.meida.module.vip.client.entity.VipType;
import com.meida.module.vip.client.entity.VipUser;
import com.meida.module.vip.provider.service.VipTypeService;
import com.meida.module.vip.provider.service.VipUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 会员类型扩展处理器
 */
@Component("vipTypeListHandler")
public class VipTypeListHandler implements PageInterceptor<VipType> {
    @Autowired
    private VipTypeService vipTypeService;
    @Autowired
    private VipUserService vipUserService;


    @Autowired
    private PayConfigService payConfigService;


    @Override
    public void complete(CriteriaQuery<VipType> cq, List<EntityMap> result, EntityMap extra) {
        //支付方式
        List<EntityMap> payTypes = payConfigService.selectPayTypes();
        extra.put("payTypes", payTypes);
        Long companyId = OpenHelper.getCompanyId();
        //会员有效期
        String expiryDate = "";
        VipUser vipUser = vipUserService.getByCompanyId(companyId);
        if (FlymeUtils.isNotEmpty(vipUser)) {
            expiryDate = vipUser.getExpiryDate().toString();
        }
        extra.put("expiryDate", expiryDate);

    }
}
