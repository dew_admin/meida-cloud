package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.system.client.entity.SysCompany;
import org.springframework.stereotype.Component;

/**
 * 企业黑名单
 */
@Component("companyBlackListHandler")
public class CompanyBlackListHandler implements PageInterceptor {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        String companyName = params.get("companyName");
        cq.select(SysCompany.class, "companyName");
        cq.like("companyName", companyName);
        cq.createJoin(SysCompany.class).setMainField("targetId");
    }
}
