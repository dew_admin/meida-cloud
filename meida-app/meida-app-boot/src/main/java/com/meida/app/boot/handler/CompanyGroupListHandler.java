package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.system.client.entity.SysGroup;
import org.springframework.stereotype.Component;

/**
 * 企业分组
 */
@Component("companyGroupListHandler")
public class CompanyGroupListHandler implements PageInterceptor {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.select(SysGroup.class, "groupName");
        cq.createJoin(SysGroup.class);

    }

}
