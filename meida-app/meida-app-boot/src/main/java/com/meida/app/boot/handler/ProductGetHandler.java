package com.meida.app.boot.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdBrand;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.client.entity.ProdProductdetails;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.provider.service.ProdProductdetailsService;
import com.meida.module.product.provider.service.ProdShopService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.common.enums.ColleconEnum;
import com.meida.module.system.provider.service.SysColleconService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 产品详细信息处理器
 */
@Component("productGetHandler")
@Log4j2
public class ProductGetHandler implements GetInterceptor {

    @Autowired
    private ProdShopService shopService;
    @Autowired
    private SysColleconService colleconService;
    @Autowired
    private ProdProductdetailsService prodProductdetailsService;

    /**
     * 查询条件扩展
     *
     * @param cq
     * @return
     */
    @Override
    public void prepare(CriteriaQuery cq,EntityMap params) {
        cq.select(ProdProduct.class, "productId", "productName", "shopId","productSeries", "productPrice", "deliveryTime", "coverImage", "firerating", "place", "suitLocation", "remark");
        cq.joinSelect(SysCompany.class, "companyId", "companyName","linkMan","linkTel");
        cq.joinSelect(ProdBrand.class, "brandName");
    }


    /**
     * 返回结果集扩展
     *
     * @param cq
     * @return
     */
    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long shopId = map.getLong("shopId");
        Long productId = map.getLong("productId");
        ProdProductdetails prodProductdetails = prodProductdetailsService.getById(productId);
        //店铺收藏标记
        Long shopColleconTag = colleconService.countCollecon(shopId, ColleconEnum.SC, ProdShop.class);
        map.put("shopColleconTag", shopColleconTag);
        //产品收藏标记
        Long productColleconTag = colleconService.countCollecon(productId, ColleconEnum.SC, ProdProduct.class);
        map.put("productColleconTag", productColleconTag);
        map.put("productDesc", prodProductdetails.getProductDesc());
    }
}
