package com.meida.app.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.IndexInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.user.client.entity.AppUser;
import com.meida.module.user.provider.service.AppUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author: zyf
 * @date: 2018/11/9 15:43
 * @description:
 */
@Api(tags = "公共服务")
@RestController
public class AppIndexController extends BaseController<AppUserService, AppUser> {


    /**
     * 首页接口
     */
    @ApiOperation(value = "首页接口", notes = "可通过实现IndexInterceptor方法扩展")
    @GetMapping("/index")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "handlerName", required = true, value = "扩展bean名称", paramType = "form"),
            @ApiImplicitParam(name = "lon", value = "经度", paramType = "form"),
            @ApiImplicitParam(name = "lat", value = "维度", paramType = "form"),
            @ApiImplicitParam(name = "sort", value = "排序方式", paramType = "form"),
            @ApiImplicitParam(name = "order", value = "排序字段", paramType = "form")
    })
    public ResultBody index(@RequestParam(required = false) Map params) {
        EntityMap map = new EntityMap(params);
        EntityMap resultMap = new EntityMap();
        Long userId = OpenHelper.getUserId();
        if (FlymeUtils.isNotEmpty(params)) {
            String handlerName = map.get("handlerName");
            IndexInterceptor handler = SpringContextHolder.getHandler(handlerName, IndexInterceptor.class);
            if (FlymeUtils.isNotEmpty(handler)) {
                resultMap = handler.complete(map, userId);
                resultMap.put("userId", userId);
            }
        }
        return ResultBody.ok(resultMap);
    }
}
