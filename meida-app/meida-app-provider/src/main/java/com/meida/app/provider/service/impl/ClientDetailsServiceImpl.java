package com.meida.app.provider.service.impl;

import com.meida.app.provider.service.feign.IBaseAppRemoteService;
import com.meida.common.base.service.CommonAppService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

/**
 * @author: zyf
 * @date: 2018/11/12 16:26
 * @description:
 */
@Slf4j
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {


    @Autowired(required = false)
    private CommonAppService commonAppService;

    @Autowired
    private IBaseAppRemoteService baseAppRemoteService;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        ClientDetails details = commonAppService.getAppClientInfo(clientId);
        if (details != null && details.getClientId()!=null && details.getAdditionalInformation() != null) {
            String status = details.getAdditionalInformation().getOrDefault("status", "0").toString();
            if (!"1".equals(status)) {
                throw new ClientRegistrationException("客户端已被禁用");
            }
        }
        return details;
    }
}
