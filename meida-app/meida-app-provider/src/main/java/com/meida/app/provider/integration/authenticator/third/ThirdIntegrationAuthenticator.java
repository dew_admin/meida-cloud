package com.meida.app.provider.integration.authenticator.third;


import com.meida.app.provider.integration.authenticator.AbstractIntegrationAuthenticator;
import com.meida.app.provider.integration.model.IntegrationParams;
import com.meida.common.base.constants.CommonConstants;
import com.meida.module.user.client.entity.AppAccount;
import com.meida.module.user.provider.service.AppAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * 第三方登录集成认证
 *
 * @author zyf
 */
@Component
public class ThirdIntegrationAuthenticator extends AbstractIntegrationAuthenticator implements ApplicationEventPublisherAware {


    @Autowired
    private AppAccountService appAccountService;

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public AppAccount authenticate(IntegrationParams integrationParams) {
        //唯一标识openId
        String username = integrationParams.getAuthParameter("username");
        //头像
        String avatar = integrationParams.getAuthParameter("avatar");
        //昵称
        String nickName = integrationParams.getAuthParameter("nickName");
        //第三方登录类型,微信,qq,微博
        String accountType = integrationParams.getAccountType();
        return (AppAccount) appAccountService.register(username, avatar, accountType, nickName);
    }

    @Override
    public void prepare(IntegrationParams integrationParams) {

    }

    @Override
    public boolean support(IntegrationParams integrationAuthentication) {
        return CommonConstants.SMS_AUTH_TYPE.contains(integrationAuthentication.getAccountType());
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
