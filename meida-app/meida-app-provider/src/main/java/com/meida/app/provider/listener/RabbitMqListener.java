package com.meida.app.provider.listener;

import com.meida.app.provider.handler.UserInfoHandler;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.user.client.entity.AppUser;
import com.meida.mq.annotation.MsgListener;
import com.meida.starter.rabbitmq.core.BaseRabbiMqHandler;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * @author zyf
 */
@Component
public class RabbitMqListener extends BaseRabbiMqHandler {


    @Autowired(required = false)
    private UserInfoHandler userInfoHandler;


    /**
     * 用户注册扩展
     *
     * @param cs
     */
    @MsgListener(queues = QueueConstants.QUEUE_USER_REG)
    public void userReg(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        super.onMessage(baseMap, deliveryTag, channel, new MqListener<EntityMap>() {
            @Override
            public void handler(EntityMap map, Channel channel) {
                CriteriaSave cs = new CriteriaSave(baseMap, AppUser.class);
                if (FlymeUtils.isNotEmpty(userInfoHandler)) {
                    userInfoHandler.userReg(cs);
                }
            }
        });
    }

    /**
     * 绑定手机号扩展
     *
     * @param object
     * @return
     */
    @MsgListener(queues = QueueConstants.QUEUE_BIND_MOBILE)
    public AppUser bindMobile(AppUser object, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        super.onMessage(object, deliveryTag, channel, new MqListener<AppUser>() {
            @Override
            public void handler(AppUser appUser, Channel channel) {
                if (FlymeUtils.isNotEmpty(userInfoHandler)) {
                    userInfoHandler.bindMobile(object);
                }

            }
        });
        return object;
    }
}
