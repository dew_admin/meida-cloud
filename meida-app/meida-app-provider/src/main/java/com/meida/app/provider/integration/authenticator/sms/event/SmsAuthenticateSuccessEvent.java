package com.meida.app.provider.integration.authenticator.sms.event;

import org.springframework.context.ApplicationEvent;

/**
 * 短信认证成功事件
 *
 * @author zyf
 */
public class SmsAuthenticateSuccessEvent extends ApplicationEvent {
    public SmsAuthenticateSuccessEvent(Object source) {
        super(source);
    }
}
