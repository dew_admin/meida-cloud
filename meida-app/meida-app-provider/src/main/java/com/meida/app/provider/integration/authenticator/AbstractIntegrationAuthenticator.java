package com.meida.app.provider.integration.authenticator;


import com.meida.app.provider.integration.model.IntegrationParams;
import com.meida.common.utils.StringUtils;
import com.meida.module.user.client.entity.AppAccount;
import jodd.util.StringUtil;

/**
 * 统一登录父类
 **/
public abstract class AbstractIntegrationAuthenticator implements IntegrationAuthenticator {

    @Override
    public abstract AppAccount authenticate(IntegrationParams integrationParams);

    @Override
    public abstract void prepare(IntegrationParams integrationParams);

    @Override
    public abstract boolean support(IntegrationParams integrationParams);

    @Override
    public void complete(IntegrationParams integrationParams) {

    }
}
