package com.meida.app.provider.integration.authenticator.sms.event;

import com.meida.app.provider.integration.model.IntegrationParams;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 短信认证之前的事件，可以监听事件进行用户手机号自动注册
 *
 * @author zyf*/
@Getter
public class SmsAuthenticateBeforeEvent extends ApplicationEvent {

    private String accountName;
    private String password;
    private String userType;
    private String shareCode;
    private String areaCode;

    public SmsAuthenticateBeforeEvent(Object source, String accountName, String password,String shareCode,String areaCode) {
        super(source);
        IntegrationParams integrationParams = (IntegrationParams) source;
        this.accountName = accountName;
        this.password = password;
        this.shareCode=shareCode;
        this.userType = integrationParams.getUserType();
        this.areaCode = areaCode;
    }
}
