package com.meida.app.provider.integration.authenticator.password;


import com.meida.app.provider.integration.authenticator.AbstractIntegrationAuthenticator;
import com.meida.app.provider.integration.model.IntegrationParams;
import com.meida.module.user.client.entity.AppAccount;
import com.meida.common.enums.AccountTypeEnum;
import com.meida.module.user.provider.service.AppAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * 默认登录处理(账户名密码登录)
 *
 * @author zyf
 */
@Component
@Primary
public class UsernamePasswordAuthenticator extends AbstractIntegrationAuthenticator {

    @Autowired
    private AppAccountService appAccountService;

    @Override
    public AppAccount authenticate(IntegrationParams integrationParams) {
        return appAccountService.applogin(integrationParams.getAccountName());
    }

    @Override
    public void prepare(IntegrationParams integrationAuthentication) {

    }

    @Override
    public boolean support(IntegrationParams integrationAuthentication) {
        return AccountTypeEnum.USERNAME.getCode().equals(integrationAuthentication.getAccountType());
    }
}
