package com.meida.app.provider.handler;

import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.user.client.entity.AppUser;

/**
 * @author zyf
 */
public interface UserInfoHandler {

    /**
     * 绑定手机号成功后扩展
     *
     * @param baseUser
     * @return
     */
    AppUser bindMobile(AppUser baseUser);


    /**
     * 用户注册成功后扩展事件
     *
     * @param cs
     */
    void userReg(CriteriaSave cs);

}
