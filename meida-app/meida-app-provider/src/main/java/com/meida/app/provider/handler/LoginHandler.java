package com.meida.app.provider.handler;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.base.module.LoginParams;

import java.util.Map;

/**
 * 登录扩展
 *
 * @author zyf
 */
public interface LoginHandler {

    /**
     * 登录前处理事件
     *
     * @param map
     * @param loginParams
     * @return
     */
    default ResultBody beforLogin(LoginParams loginParams) {
        return ResultBody.ok();
    }


    /**
     * 登录成功后处理事件
     *
     * @param map
     * @param loginParams
     * @return
     */
    ResultBody afterLogin(Map<String, Object> map, LoginParams loginParams);


}
