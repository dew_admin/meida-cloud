package com.meida.app.provider.configuration;

import lombok.AllArgsConstructor;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;


/**
 * 追加白名单路径访问时需要移除请求头过滤器配置
 *
 * @author zyf
 */
@Component("permitAllSecurityConfig")
@AllArgsConstructor
public class PermitAllSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {


    private final Filter permitAuthenticationFilter;

    @Override
    public void configure(HttpSecurity http) {
        http.addFilterBefore(permitAuthenticationFilter, OAuth2AuthenticationProcessingFilter.class);
    }
}