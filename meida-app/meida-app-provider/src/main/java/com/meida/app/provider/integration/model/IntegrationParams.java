package com.meida.app.provider.integration.model;

import lombok.Data;

import java.util.Map;

/**
 * @author zyf
 */
@Data
public class IntegrationParams {

    private String accountType;
    private String accountName;
    private String userType;
    private Map<String, String[]> authParameters;
    private String clientId;

    public String getAuthParameter(String params) {
        String[] values = this.authParameters.get(params);
        if (values != null && values.length > 0) {
            return values[0];
        }
        return null;
    }
}
