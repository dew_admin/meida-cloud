package com.meida.app.provider.listener;

import com.meida.app.provider.integration.authenticator.sms.event.SmsAuthenticateBeforeEvent;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.user.provider.service.AppUserService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zyf
 */
@Component
public class UserInfoListener {
    @Resource
    private AppUserService appUserService;

    /**
     * 手机号验证码登录自动注册监听
     */
    @EventListener
    public void smsLoginEvent(SmsAuthenticateBeforeEvent userInfoEvent) {
        String accountName = userInfoEvent.getAccountName();
        String password = userInfoEvent.getPassword();
        String shareCode = userInfoEvent.getShareCode();
        String areaCode = userInfoEvent.getAreaCode();
        String userType = FlymeUtils.getString(userInfoEvent.getUserType(), "USER");
        appUserService.registerByMobile(accountName, password, userType,shareCode,areaCode);
    }
}
