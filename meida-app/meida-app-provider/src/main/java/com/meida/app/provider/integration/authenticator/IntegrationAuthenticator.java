package com.meida.app.provider.integration.authenticator;

import com.meida.app.provider.integration.model.IntegrationParams;
import com.meida.module.user.client.entity.AppAccount;

/**
 * 统一登录处理接口
 **/
public interface IntegrationAuthenticator {

    /**
     * 处理集成认证
     *
     * @param integrationParams
     * @return
     */
    AppAccount authenticate(IntegrationParams integrationParams);


    /**
     * 进行预处理
     *
     * @param integrationParams
     */
    void prepare(IntegrationParams integrationParams);

    /**
     * 判断是否支持集成认证类型
     *
     * @param integrationParams
     * @return
     */
    boolean support(IntegrationParams integrationParams);

    /**
     * 认证结束后执行
     *
     * @param integrationParams
     */
    void complete(IntegrationParams integrationParams);

}
