package com.meida.app.provider.service.feign;


import com.meida.common.base.constants.CommonConstants;
import com.meida.module.admin.client.api.IBaseAppServiceClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author: zyf
 * @date: 2018/10/24 16:49
 * @description:
 */
@FeignClient(value = CommonConstants.BASE_SERVICE)
public interface IBaseAppRemoteService extends IBaseAppServiceClient {

}
