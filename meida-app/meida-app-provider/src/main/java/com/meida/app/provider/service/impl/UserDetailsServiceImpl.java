package com.meida.app.provider.service.impl;


import com.meida.app.provider.integration.authenticator.IntegrationAuthenticator;
import com.meida.app.provider.integration.model.IntegrationAuthenticationContext;
import com.meida.app.provider.integration.model.IntegrationParams;
import com.meida.common.oauth2.OpenOAuth2ClientProperties;
import com.meida.module.user.client.entity.AppAccount;
import com.meida.common.security.OpenUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * APP用户认证中心
 * @author zyf
 */
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private OpenOAuth2ClientProperties clientProperties;
    private List<IntegrationAuthenticator> authenticators;

    @Autowired(required = false)
    public void setIntegrationAuthenticators(List<IntegrationAuthenticator> authenticators) {
        this.authenticators = authenticators;
    }

    /**
     * 认证中心名称
     */
    @Value("${spring.application.name}")
    private String AUTH_SERVICE_ID;

    @Override
    public UserDetails loadUserByUsername(String username) {
        IntegrationParams integrationAuthentication = Optional.ofNullable(IntegrationAuthenticationContext.get()).orElse(new IntegrationParams());
        integrationAuthentication.setAccountName(username);
        AppAccount account = Optional.ofNullable(this.authenticate(integrationAuthentication)).orElseThrow(() -> new UsernameNotFoundException("系统用户 " + username + " 不存在!"));
        boolean accountNonLocked = true;
        boolean credentialsNonExpired = true;
        boolean enable = true;
        boolean accountNonExpired = true;
        String clientId = clientProperties.getOauth2().get("portal").getClientId();
        return new OpenUser(clientId, account.getDomain(), account.getAccountId(), account.getCompanyId(), account.getUserId(), account.getAccount(), account.getPassword(), accountNonLocked, accountNonExpired, enable, credentialsNonExpired, "", "", account.getAccountType());
    }

    /**
     * 根据登录类型适配具体执行类
     */
    private AppAccount authenticate(IntegrationParams integrationAuthentication) {
        if (this.authenticators != null) {
            for (IntegrationAuthenticator authenticator : authenticators) {
                if (authenticator.support(integrationAuthentication)) {
                    return authenticator.authenticate(integrationAuthentication);
                }
            }
        }
        return null;
    }
}
