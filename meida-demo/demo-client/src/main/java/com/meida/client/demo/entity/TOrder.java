package com.meida.client.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyf
 */




@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("test_order")
@ApiModel(value = "测试订单", description = "测试订单")
public class TOrder extends AbstractEntity {

    @ApiModelProperty(value = "主键")
    @TableId(value = "orderId", type = IdType.ASSIGN_ID)
    private Long orderId;

    @ApiModelProperty(value = "企业ID")
    @TableField(value = "companyId")
    private Long companyId;

    @TableField(value = "userId")
    private Long userId;

    @TableField(value = "orderNo")
    private String orderNo;

    @ApiModelProperty(value = "订单标题")
    @TableField(value = "orderTitle")
    private String orderTitle;

    @ApiModelProperty(value = "订单标题")
    @TableField(value = "orderDate")
    private Date orderDate;
}
