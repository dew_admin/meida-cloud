package com.meida.client.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 档案信息表
 *
 * @author flyme
 * @date 2021-12-08
 */
@Data

@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_info")
@TableAlias("info")
@ApiModel(value="ArcInfo对象", description="档案信息表")
public class ArcInfo extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "arcInfoId", type = IdType.ASSIGN_ID)
    private Long arcInfoId;

    @ApiModelProperty(value = "扩展字段")
    private String expand;

    @ApiModelProperty(value = "父id")
    private Long parentId;

    @ApiModelProperty(value = "外部来源id")
    private Long outSideId;

    @ApiModelProperty(value = "删除时间")
    private String deleteTime;

    @ApiModelProperty(value = "创建批次")
    private String createBatch;

    @ApiModelProperty(value = "调出时间")
    private String adjustTime;

    @ApiModelProperty(value = "查看原文次数")
    private Integer viewFileCount;

    @ApiModelProperty(value = "查看条目次数")
    private Integer viewCount;

    @ApiModelProperty(value = "库房id")
    private Long storeRoomId;

    @ApiModelProperty(value = "索引状态")
    private Integer indexStatus;

    @ApiModelProperty(value = "原文数量")
    private Integer originalCount;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "是否利用")
    private Integer isUse;

    @ApiModelProperty(value = "是否入库")
    private Integer isStore;

    @ApiModelProperty(value = "是否回收站")
    private Integer isRecycle;

    @ApiModelProperty(value = "是否销毁")
    private Integer isDestory;

    @ApiModelProperty(value = "是否编研")
    private Integer isCompilation;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "单位id")
    private Long unitId;

    @ApiModelProperty(value = "顺序号")
    private String seq;

    @ApiModelProperty(value = "子条目数")
    private Integer childCount;

    @ApiModelProperty(value = "原移交单位")
    private String handouverUnit;

    @ApiModelProperty(value = "二级全宗号")
    private String fondsNo;

    @ApiModelProperty(value = "份数")
    private Integer numberNo;

    @ApiModelProperty(value = "归档标识0未归档1待归档2已归档")
    private Integer arcFlag;

    @ApiModelProperty(value = "档号")
    private String arcNo;

    @ApiModelProperty(value = "归档年度")
    private String filingYear;

    @ApiModelProperty(value = "页数")
    private Integer pageNo;

    @ApiModelProperty(value = "保管期限（保管期限Y永久，D30定期30年，D10定期10年）")
    private String retention;

    @ApiModelProperty(value = "文件形成时间")
    private String createdDate;

    @ApiModelProperty(value = "移交时间")
    private String handoverTime;

    @ApiModelProperty(value = "分类号")
    private String arcCtgNo;

    @ApiModelProperty(value = "件号")
    private String pieceNo;

    @ApiModelProperty(value = "题名")
    private String maintitle;

    @ApiModelProperty(value = "密级")
    private String securityClass;

    @ApiModelProperty(value = "存放位置")
    private String folderLocation;

    @ApiModelProperty(value = "归档日期")
    private String pigeonholeDate;

    @ApiModelProperty(value = "部门名称")
    private String department;

    @ApiModelProperty(value = "目录号")
    private String contentNo;

    @ApiModelProperty(value = "批量关联号")
    private String relevanceNo;

    @ApiModelProperty(value = "责任者")
    private String responsibleby;

}
