package com.meida;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.client.demo.entity.ArcInfo;
import com.meida.client.demo.entity.TOrder;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.DateUtils;
import com.meida.sharding.demo.ShardingDemoApplication;
import com.meida.sharding.demo.service.ArcInfoService;
import com.meida.sharding.demo.service.TestOrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingDemoApplication.class)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.meida"})
public class ShardingDemoTest {
    @Autowired
    private TestOrderService testOrderService;

    @Autowired
    private ArcInfoService arcInfoService;

    @Test
    public void insert() {
        for (int i = 0; i < 50; i++) {
            TOrder testOrder = new TOrder();
            testOrder.setOrderId(IdWorker.getId());
            testOrder.setOrderDate(DateUtils.getNowDate());
            testOrder.setUserId(Long.parseLong(i+""));
            testOrder.setCompanyId(Long.parseLong("10000"));
            testOrder.setOrderTitle("测试订单" + i);
            testOrderService.save(testOrder);
        }
    }
    @Test
    public void update(){
        ArcInfo arcInfo=new ArcInfo();
        //arcInfo.setCategoryId(1468888897772232706L);
        arcInfo.setArcNo("3333");
        arcInfo.setArcInfoId(1468981331994746881L);
        UpdateWrapper up=new UpdateWrapper();
        up.eq("categoryId",1468888897772232706L);
        up.eq("arcInfoId",1468981331994746881L);
        arcInfoService.update(arcInfo,up);
    }

    @Test
    public void list() {
        CriteriaQuery cq = new CriteriaQuery(TOrder.class);
        cq.ge("companyId", 20L);
        List<TOrder> list = testOrderService.list(cq);
        System.out.println(list.size());
    }


}

