package com.meida.sharding.demo.service.impl;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.meida.client.demo.entity.TOrder;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.sharding.demo.mapper.TestOrderMapper;
import com.meida.sharding.demo.service.TestOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 退款方式 实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@DS("sharding")
@Transactional(rollbackFor = Exception.class)
public class TestOrderServiceImpl extends BaseServiceImpl<TestOrderMapper, TOrder> implements TestOrderService {


    @Override
    public List<EntityMap> selectOrderList() {
        CriteriaQuery cq = new CriteriaQuery(TOrder.class);
        return selectEntityMap(cq);
    }
}
