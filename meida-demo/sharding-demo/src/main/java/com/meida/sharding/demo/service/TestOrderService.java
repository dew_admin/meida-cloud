package com.meida.sharding.demo.service;

import com.meida.client.demo.entity.TOrder;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;


import java.util.List;

/**
 * 测试订单 接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface TestOrderService extends IBaseService<TOrder> {
    List<EntityMap> selectOrderList();
}
