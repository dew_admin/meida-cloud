package com.meida.sharding.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zyf
 */
@SpringBootApplication(scanBasePackages = "com.meida")
@ComponentScan(basePackages = {"com.meida"})
public class ShardingDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShardingDemoApplication.class, args);
    }

}
