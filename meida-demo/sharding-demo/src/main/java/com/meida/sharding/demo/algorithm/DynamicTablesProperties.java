package com.meida.sharding.demo.algorithm;

import lombok.Data;

@Data
public class DynamicTablesProperties {

    String[] names;

}
