//package com.meida.sharding.demo.algorithm;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
//import org.apache.shardingsphere.driver.jdbc.core.datasource.ShardingSphereDataSource;
//import org.apache.shardingsphere.infra.datanode.DataNode;
//import org.apache.shardingsphere.sharding.rule.ShardingRule;
//import org.apache.shardingsphere.sharding.rule.TableRule;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import javax.sql.DataSource;
//
//@Slf4j
//@Component
//public class ShardingTableRuleActualDataNodesRefreshJob {
//
//    @Autowired
//    private DynamicTablesProperties dynamicTables;
//
//    @Resource(name = "shardingDataSource")
//    private DataSource dataSource;
//
//    public void refreshActualDataNodes() throws NoSuchFieldException, IllegalAccessException {
//        log.info("Job 动态刷新 actualDataNodes START");
//        if (dynamicTables.getNames() == null || dynamicTables.getNames().length == 0) {
//            log.error("【dynamic.table.names】配置为空!");
//            return;
//        }
//        for (int i = 0; i < dynamicTables.getNames().length; i++) {
//            String dynamicTableName = dynamicTables.getNames()[i];
//            TableRule tableRule = null;
//            try {
//                ShardingSphereDataSource shardingDataSource = (ShardingSphereDataSource) dataSource;
//                ShardingRule shardingRule = shardingDataSource.getRuntimeContext().getRule();
//                tableRule = shardingRule.getTableRule(dynamicTableName);
//            } catch (ShardingConfigurationException e) {
//                log.error(String.format("逻辑表：%s 动态分表配置错误！", dynamicTableName));
//            }
//            String dataSourceName = tableRule.getActualDataNodes().get(0).getDataSourceName();
//            String logicTableName = tableRule.getLogicTable();
//            assert tableRule != null;
//            List<DataNode> newDataNodes = getDataNodes(dynamicTableName, dataSourceName, logicTableName);
//            if (newDataNodes.isEmpty()) {
//                throw new UnsupportedOperationException();
//            }
//            createSubTableIfAbsent(logicTableName, newDataNodes);
//            dynamicRefreshDatasource(dataSourceName, tableRule, newDataNodes);
//        }
//        log.info("Job 动态刷新 actualDataNodes END");
//    }
//}
