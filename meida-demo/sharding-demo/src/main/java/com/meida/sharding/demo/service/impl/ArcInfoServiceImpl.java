package com.meida.sharding.demo.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.meida.client.demo.entity.ArcInfo;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;

import com.meida.sharding.demo.mapper.ArcInfoMapper;
import com.meida.sharding.demo.service.ArcInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 档案信息表接口实现类
 *
 * @author flyme
 * @date 2021-12-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
@DS("sharding")
public class ArcInfoServiceImpl extends BaseServiceImpl<ArcInfoMapper, ArcInfo> implements ArcInfoService {




}
