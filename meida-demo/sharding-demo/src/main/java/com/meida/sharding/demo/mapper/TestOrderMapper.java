package com.meida.sharding.demo.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.meida.client.demo.entity.TOrder;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 测试订单 Mapper 接口
 * @author flyme
 */
public interface TestOrderMapper extends SuperMapper<TOrder> {

}
