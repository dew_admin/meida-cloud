package com.meida.sharding.demo.service;

import com.meida.client.demo.entity.ArcInfo;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;


import java.util.Map;

/**
 * 档案信息表 接口
 *
 * @author flyme
 * @date 2021-12-08
 */
public interface ArcInfoService extends IBaseService<ArcInfo> {



}
