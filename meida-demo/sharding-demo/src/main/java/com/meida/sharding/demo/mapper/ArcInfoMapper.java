package com.meida.sharding.demo.mapper;

import com.meida.client.demo.entity.ArcInfo;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 档案信息表 Mapper 接口
 * @author flyme
 * @date 2021-12-08
 */
public interface ArcInfoMapper extends SuperMapper<ArcInfo> {

}
