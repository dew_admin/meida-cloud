package com.meida.module.cms.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 新闻评论
 *
 * @author flyme
 * @date 2019-12-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("cms_comment")
@TableAlias("comment")
@ApiModel(value = "NewsComment对象", description = "新闻评论 ")
public class CmsComment extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "commentId", type = IdType.ASSIGN_ID)
    private Long commentId;

    @ApiModelProperty(value = "新闻Id")
    private Long contentId;

    @ApiModelProperty(value = "评论人")
    @TableField(value = "replyUserId",fill = FieldFill.INSERT)
    private Long replyUserId;

    @ApiModelProperty(value = "被评论人")
    private Long replyToUserId;

    @ApiModelProperty(value = "评论图片")
    private String replyImages;

    @ApiModelProperty(value = "评论内容")
    private String replyContent;

    @ApiModelProperty(value = "评论状态")
    private Integer replyState;

}
