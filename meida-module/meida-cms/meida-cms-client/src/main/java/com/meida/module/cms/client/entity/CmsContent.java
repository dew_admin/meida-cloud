package com.meida.module.cms.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 新闻资讯
 *
 * @author flyme
 * @date 2019-10-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("cms_content")
@TableAlias("content")
@ApiModel(value = "NewsContent对象", description = "新闻资讯")
public class CmsContent extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "contentId", type = IdType.ASSIGN_ID)
    private Long contentId;

    @ApiModelProperty(value = "分类")
    private Long contentTypeId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "来源")
    private String contentSource;

    @ApiModelProperty(value = "来源地址")
    private String sourceUrl;

    @ApiModelProperty(value = "状态")
    private Integer contentState;

    @ApiModelProperty(value = "发布日期")
    private Date sendDate;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "点击量")
    private Integer clickNum;

    @ApiModelProperty(value = "评论量")
    private Integer commentNum;

    @ApiModelProperty(value = "推荐是否")
    private Integer recommend;

    @ApiModelProperty(value = "置顶序号")
    private Integer topOrder;

}
