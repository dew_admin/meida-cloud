/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 14:34:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_content
-- ----------------------------
DROP TABLE IF EXISTS `cms_content`;
CREATE TABLE `cms_content`  (
  `contentId` bigint(20) NOT NULL COMMENT '主键',
  `contentTypeId` bigint(20) NULL DEFAULT NULL COMMENT '分类',
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `subTitle` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `coverImage` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `contentSource` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `sourceUrl` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源地址',
  `contentState` int(11) NULL DEFAULT NULL COMMENT '状态',
  `sendDate` datetime(0) NULL DEFAULT NULL COMMENT '发布日期',
  `topOrder` int(11) NULL DEFAULT NULL COMMENT '置顶序号',
  `author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `clickNum` int(11) NULL DEFAULT NULL COMMENT '点击量',
  `commentNum` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '推荐是否',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`contentId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '新闻资讯' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (406, 400, 'cmsNews', '新闻管理', '新闻管理', '/', 'content/cms/index', 'desktop', '_self', 'PageView', 1, 2, 1, 0, 'meida-base-provider', '2019-12-27 09:41:32', '2020-01-04 17:27:33');
INSERT INTO `base_authority` VALUES (406, 'MENU_cmsNews', 406, NULL, NULL, 1, '2019-12-27 09:41:32', '2020-01-04 17:27:33');


SET FOREIGN_KEY_CHECKS = 1;
