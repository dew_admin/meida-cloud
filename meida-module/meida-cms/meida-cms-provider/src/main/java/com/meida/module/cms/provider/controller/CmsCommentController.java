package com.meida.module.cms.provider.controller;

import com.meida.module.cms.client.entity.CmsComment;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.cms.provider.service.CmsCommentService;


/**
 * 新闻评论 控制器
 *
 * @author flyme
 * @date 2019-12-06
 */
@RestController
@RequestMapping("/cms/comment/")
@Api(tags = "CMS模块-新闻资讯评价")
public class CmsCommentController extends BaseController<CmsCommentService, CmsComment> {

    @ApiOperation(value = "新闻评论 -分页列表", notes = "新闻评论 分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "新闻评论 -列表", notes = "新闻评论 列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "发布评论", notes = "发布评论")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }



    @ApiOperation(value = "新闻评论 -删除", notes = "删除新闻评论 ")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    
    @ApiOperation(value = "新闻评论 -更新状态值", notes = "新闻评论 更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
