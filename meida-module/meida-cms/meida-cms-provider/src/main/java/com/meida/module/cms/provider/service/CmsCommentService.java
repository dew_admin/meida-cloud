package com.meida.module.cms.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.cms.client.entity.CmsComment;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 新闻评论  接口
 *
 * @author flyme
 * @date 2019-12-06
 */
public interface CmsCommentService extends IBaseService<CmsComment> {

    /**
     * 查询评价
     *
     * @param contentId
     * @param num
     * @return
     */
    List<EntityMap> listByContentIdId(Long contentId, Integer num);
}
