package com.meida.module.cms.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.module.cms.client.entity.CmsContent;
import com.meida.module.cms.provider.mapper.CmsContentMapper;
import com.meida.module.cms.provider.service.CmsCommentService;
import com.meida.module.cms.provider.service.CmsContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 新闻资讯   实现类
 *
 * @author flyme
 * @date 2019-10-08
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CmsContentServiceImpl extends BaseServiceImpl<CmsContentMapper, CmsContent> implements CmsContentService {
    @Autowired
    private CmsCommentService commentService;


    @Override
    public ResultBody beforePageList(CriteriaQuery<CmsContent> cq, CmsContent newsContent, EntityMap requestMap) {
        cq.select(CmsContent.class, "*");
        cq.likeByField(CmsContent.class,"title");
        cq.eq(true, "contentState", CommonConstants.ENABLED);
        cq.orderByDesc("content.recommend");
        cq.orderByAsc("content.topOrder");
        cq.orderByDesc("content.createTime");
        return ResultBody.ok();
    }



    @Override
    public ResultBody beforeGet(CriteriaQuery<CmsContent> cq, CmsContent newsContent, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(CmsContent.class, "*");
        cq.addSelect("collecon_tag(1,'CmsContent',contentId," + userId + ") scTag");
        cq.addSelect("collecon_tag(2,'CmsContent',contentId," + userId + ") dzTag");
        return ResultBody.ok();
    }

    @Override
    public void afterGet(CriteriaQuery cq, EntityMap result) {
        Long contentId = result.getLong("contentId");
        List<EntityMap> commentList = commentService.listByContentIdId(contentId, 10);
        updateClickNum(contentId);
        result.put("commentList", commentList);

    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, CmsContent cmsContent, EntityMap extra) {
        cmsContent.setCommentNum(0);
        cmsContent.setContentState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }


    @Override
    public Boolean updateCommentNum(Long contentId, Integer num) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.setSql(true, "commentNum=commentNum+" + num);
        cu.ge("commentNum", 0);
        cu.eq(true, "contentId", contentId);
        return update(cu);
    }
    @Override
    public Boolean updateClickNum(Long contentId) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.setSql(true, "clickNum=clickNum+" + 1);
        cu.eq(true, "contentId", contentId);
        return update(cu);
    }
}
