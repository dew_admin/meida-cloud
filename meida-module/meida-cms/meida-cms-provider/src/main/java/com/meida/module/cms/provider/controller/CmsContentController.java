package com.meida.module.cms.provider.controller;

import com.meida.module.cms.client.entity.CmsContent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.cms.provider.service.CmsContentService;

import java.util.Map;


/**
 * 新闻资讯  控制器
 *
 * @author flyme
 * @date 2019-10-08
 */
@RestController
@RequestMapping("/cms/content/")
@Api(tags = "CMS模块-新闻资讯")
public class CmsContentController extends BaseController<CmsContentService, CmsContent> {

    @ApiOperation(value = "新闻资讯-分页列表", notes = "新闻资讯  分页列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "新闻资讯-列表", notes = "新闻资讯  列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "新闻资讯-添加", notes = "添加新闻资讯  ")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "新闻资讯-更新", notes = "更新新闻资讯  ")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "新闻资讯-删除", notes = "删除新闻资讯  ")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "新闻资讯-详情", notes = "新闻资讯  详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "新闻-更新状态值", notes = "帖子更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "contentState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "contentState") Integer contentState) {
        return bizService.setState(map, "contentState", contentState);
    }
}
