package com.meida.module.cms.provider.mapper;

import com.meida.module.cms.client.entity.CmsContent;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 新闻资讯   Mapper 接口
 * @author flyme
 * @date 2019-10-08
 */
public interface CmsContentMapper extends SuperMapper<CmsContent> {

}
