package com.meida.module.cms.provider.mapper;

import com.meida.module.cms.client.entity.CmsComment;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 新闻评论  Mapper 接口
 * @author flyme
 * @date 2019-12-06
 */
public interface CmsCommentMapper extends SuperMapper<CmsComment> {

}
