package com.meida.module.cms.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.cms.client.entity.CmsComment;
import com.meida.module.cms.client.entity.CmsContent;
import com.meida.module.cms.provider.mapper.CmsCommentMapper;
import com.meida.module.cms.provider.service.CmsCommentService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.cms.provider.service.CmsContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 新闻评论 接口实现类
 *
 * @author flyme
 * @date 2019-12-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CmsCommentServiceImpl extends BaseServiceImpl<CmsCommentMapper, CmsComment> implements CmsCommentService {

    @Autowired
    private CmsContentService contentService;

    @Override
    public List<EntityMap> listByContentIdId(Long contentId, Integer num) {
        Long userId = OpenHelper.getUserId();
        ApiAssert.isNotEmpty("contentId不能为空", contentId);
        CriteriaQuery cq = new CriteriaQuery(CmsComment.class);
        cq.select(CmsComment.class, "commentId", "replyContent", "replyImages","createTime");
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_count(2,'CmsComment',commentId) dzNum");
        cq.addSelect("collecon_tag(2,'CmsComment',commentId," + userId + ") dzTag");
        cq.eq(CmsComment.class, "contentId", contentId);
        cq.limit(num);
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("replyUserId");
        cq.orderByDesc("comment.createTime");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, CmsComment comment, EntityMap extra) {
        CmsContent content = contentService.getById(comment.getContentId());
        ApiAssert.isNotEmpty("新闻已删除", content);
        Long userId = OpenHelper.getUserId();
        comment.setReplyState(CommonConstants.ENABLED);
        comment.setReplyUserId(userId);
        return ResultBody.ok();
    }


    @Override
    public ResultBody afterAdd(CriteriaSave cs, CmsComment comment, EntityMap extra) {
        contentService.updateCommentNum(comment.getContentId(), 1);
        return ResultBody.msg("评论成功");
    }

    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        contentService.updateCommentNum(cd.getIdValue(), -1);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforePageList(CriteriaQuery<CmsComment> cq, CmsComment newsComment, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        Long contentId = newsComment.getContentId();
        ApiAssert.isNotEmpty("contentId不能为空", contentId);
        cq.select(CmsComment.class, "commentId", "replyContent", "replyImages","createTime");
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_count(2,'CmsComment',commentId) dzNum");
        cq.addSelect("collecon_tag(2,'CmsComment',commentId," + userId + ") dzTag");
        cq.eq(CmsComment.class, "contentId", newsComment.getContentId());
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("replyUserId");
        cq.orderByDesc("comment.createTime");
        return ResultBody.ok();
    }
}
