package com.meida.module.cms.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.cms.client.entity.CmsComment;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class CmsCommentInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return CmsComment.class;
    }
}
