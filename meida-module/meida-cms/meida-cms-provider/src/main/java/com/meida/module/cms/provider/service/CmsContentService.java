package com.meida.module.cms.provider.service;

import com.meida.module.cms.client.entity.CmsContent;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 新闻资讯   接口
 *
 * @author flyme
 * @date 2019-10-08
 */
public interface CmsContentService extends IBaseService<CmsContent> {
    Boolean updateCommentNum(Long contentId, Integer num);
    Boolean updateClickNum(Long contentId);
}
