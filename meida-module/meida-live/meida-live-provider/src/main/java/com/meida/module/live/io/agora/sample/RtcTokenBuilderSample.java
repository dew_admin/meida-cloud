package com.meida.module.live.io.agora.sample;

import com.meida.module.live.io.agora.media.RtcTokenBuilder;

public class RtcTokenBuilderSample {
    static String appId = "7368bf629ca34ea48b39a93fd52de92b";
    static String appCertificate = "0dab63ff045d47758eb429745880ea62";
    static String channelName = "7d72365eb983485397e3e3f9d460bdda";
    static String userAccount = "2082341273";
    static int uid = 2082341273;
    static int expirationTimeInSeconds = 3600; 

    public static void main(String[] args) throws Exception {
        RtcTokenBuilder token = new RtcTokenBuilder();
        int timestamp = (int)(System.currentTimeMillis() / 1000 + expirationTimeInSeconds);
        String result = token.buildTokenWithUserAccount(appId, appCertificate,  
        		 channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        
        result = token.buildTokenWithUid(appId, appCertificate,  
       		 channelName, uid, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
    }
}
