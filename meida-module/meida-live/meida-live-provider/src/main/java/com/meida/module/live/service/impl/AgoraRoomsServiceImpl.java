package com.meida.module.live.service.impl;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.HttpUtils;
import com.meida.common.utils.JsonUtils;
import com.meida.module.live.config.AgoraProperties;
import com.meida.module.live.config.RoomsRequestUrl;
import com.meida.module.live.io.agora.utils.RtmTokenBuilderUtil;
import com.meida.module.live.service.AgoraRoomsService;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AgoraRoomsServiceImpl implements AgoraRoomsService {

    @Autowired
    private RtmTokenBuilderUtil rtmTokenBuilderUtil;

    @Autowired
    private AgoraProperties agoraProperties;

    @Override
    public ResultBody setRoomsState(String roomUUid, Integer state) {
        try {
            String requestUrl = RoomsRequestUrl.setRoomsState;
            requestUrl = requestUrl.replace("{appId}", agoraProperties.getAppId());
            requestUrl = requestUrl.replace("{roomUUid}", roomUUid);
            requestUrl = requestUrl.replace("{state}", state.toString());
            System.out.println("请求路径：" + requestUrl);
            Map<String, String> headers = new HashMap<>();
            Map<String, Object> queryParams = new HashMap<>();
            String token = rtmTokenBuilderUtil.getRtmToken("1", 3600 * 24);
            headers.put("x-agora-token", token);
            headers.put("x-agora-uid", "1");
            HttpResponse httpResponse = HttpUtils.doPut(requestUrl, headers, queryParams, "");
            String result = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            System.out.println("请求结果：" + result);
            return ResultBody.ok(JsonUtils.jsonToEntityMap(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultBody.ok();
    }

    public static void main(String[] args) {
        String requestUrl = RoomsRequestUrl.setRoomsState;
        requestUrl = requestUrl.replace("{appId}", "7368bf629ca34ea48b39a93fd52de92b");
        requestUrl = requestUrl.replace("{roomUUid}", "0dab63ff045d47758eb429745880ea62");
        requestUrl = requestUrl.replace("{state}", "1");
        System.out.println(requestUrl);
    }
}
