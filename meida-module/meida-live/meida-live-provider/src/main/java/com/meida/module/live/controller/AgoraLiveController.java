package com.meida.module.live.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.live.io.agora.utils.RtmTokenBuilderUtil;
import com.meida.module.live.service.AgoraRoomsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 声网直播模块控制器
 */
@Slf4j
@Api(tags = "声网直播模块")
@RestController
@RequestMapping("/live/agora/")
public class AgoraLiveController {

    @Autowired
    private RtmTokenBuilderUtil rtmTokenBuilderUtil;

    @Autowired
    private AgoraRoomsService agoraRoomsService;

    @ApiOperation(value = "获取RtmToken", notes = "获取RtmToken")
    @GetMapping(value = "getRtmToken")
    public ResultBody getRtmToken(String account, Integer expiredTsInSeconds) {
        String token = rtmTokenBuilderUtil.getRtmToken(account, expiredTsInSeconds);
        return ResultBody.ok(token);
    }

    @ApiOperation(value = "灵动课堂-设置课堂状态", notes = "灵动课堂-设置课堂状态")
    @PostMapping(value = "setRoomsState")
    public ResultBody setRoomsState(String roomUUid, Integer state) {
        ResultBody resultBody = agoraRoomsService.setRoomsState(roomUUid, state);
        return resultBody;
    }
}
