package com.meida.module.live.handler;

import com.meida.common.base.entity.EntityMap;


/**
 * @author zyf
 */
public interface RoomInitHandler {


    /**
     * 初始化直播间信息
     *
     * @param roomId
     * @param userId
     */
    EntityMap initRoomInfo(String roomId,String userType,Long userId);


}
