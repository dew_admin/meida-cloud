package com.meida.module.live.config;

/**
 * 灵动课堂请求地址配置
 */
public class RoomsRequestUrl {

    /**
     * 接口请求域名
     */
    public static final String serverName = "https://api.agora.io";

    /**
     * 设置课堂状态
     */
    public static final String setRoomsState = serverName + "/edu/apps/{appId}/v2/rooms/{roomUUid}/states/{state}";


}
