package com.meida.module.live.io.agora.sample;

import com.meida.module.live.io.agora.rtm.RtmTokenBuilder;

public class RtmTokenBuilderSample {
    private static String appId = "0dab63ff045d47758eb429745880ea62";
    private static String appCertificate = "5CFd2fd1755d40ecb72977518be15d3b";
    private static String userId = "2882341273";
    private static int expireTimestamp = 0;

    public static void main(String[] args) throws Exception {
    	RtmTokenBuilder token = new RtmTokenBuilder();
        String result = token.buildToken(appId, appCertificate, userId, RtmTokenBuilder.Role.Rtm_User, expireTimestamp);
        System.out.println(result);
    }
}
