package com.meida.module.live.service;

import com.meida.common.mybatis.model.ResultBody;

public interface AgoraRoomsService {

    /**
     * 设置课堂状态
     *
     * @param roomUUid
     * @param state
     * @return
     */
    ResultBody setRoomsState(String roomUUid, Integer state);
}
