package com.meida.module.live.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.live.config.AgoraProperties;
import com.meida.module.live.handler.RoomInitHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 声网直播模块控制器
 */
@Slf4j
@Api(tags = "声网直播模块")
@Controller
@RequestMapping("/common/live/agora/")
public class AgoraHtmlController {


    @Autowired(required = false)
    private RoomInitHandler roomInitHandler;

    @Autowired
    private AgoraProperties agoraProperties;

    @ApiOperation(value = "进入直播界面", notes = "进入直播界面")
    @GetMapping(value = "room")
    public String getRtmToken(ModelMap map, String roomId, String userType, Long userId) {
        String appId = agoraProperties.getAppId();
        if (FlymeUtils.isNotEmpty(roomInitHandler)) {
            EntityMap roomInfo = roomInitHandler.initRoomInfo(roomId, userType, userId);
            map.put("appId", appId);
            map.putAll(roomInfo);
        }
        return "live";
    }

}
