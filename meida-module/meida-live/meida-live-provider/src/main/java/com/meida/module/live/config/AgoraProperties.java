package com.meida.module.live.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "agora")
public class AgoraProperties {
    /**
     * 声网APPID
     */
    private String appId;
    /**
     * 声网APP证书
     */
    private String certificate;

}
