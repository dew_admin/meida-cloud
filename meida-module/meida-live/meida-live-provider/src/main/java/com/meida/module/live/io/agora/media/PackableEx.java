package com.meida.module.live.io.agora.media;

public interface PackableEx extends Packable {
    void unmarshal(ByteBuf in);
}
