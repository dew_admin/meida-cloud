package com.meida.module.live.io.agora.sample;

import com.meida.module.live.io.agora.media.DynamicKey5;

import java.util.Date;
import java.util.Random;

/**
 * Created by Li on 10/1/2016.
 */
public class DynamicKey5Sample {
    static String appID = "7368bf629ca34ea48b39a93fd52de92b";
    static String appCertificate = "0dab63ff045d47758eb429745880ea62";
    static String channel = "7d72365eb983485397e3e3f9d460bdda";
    static int ts = (int)(new Date().getTime()/1000);
    static int r = new Random().nextInt();
    static long uid = 2882341273L;
    static int expiredTs = 0;

    public static void main(String[] args) throws Exception {
        System.out.println(DynamicKey5.generateMediaChannelKey(appID, appCertificate, channel, ts, r, uid, expiredTs));
        System.out.println(DynamicKey5.generateRecordingKey(appID, appCertificate, channel, ts, r, uid, expiredTs));
        System.out.println(DynamicKey5.generateInChannelPermissionKey(appID, appCertificate, channel, ts, r, uid, expiredTs, DynamicKey5.noUpload));
        System.out.println(DynamicKey5.generateInChannelPermissionKey(appID, appCertificate, channel, ts, r, uid, expiredTs, DynamicKey5.audioVideoUpload));
    }
}
