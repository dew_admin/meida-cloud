package com.meida.module.pay.client.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.NoArgsConstructor;

/**
 * 提现请求处理状态
 *
 * @author gp
 * @date 2021/8/9
 */
@NoArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ApiModel("提现请求状态")
public enum PayCashoutStatusEnum {
    /**
     * 提现请求状态-1驳回或失败 0处理中 1提现成功
     */
    fail(-1, "驳回或失败"),
    dealWith(0, "处理中"),
    success(1, "提现成功");

    @EnumValue
    private Integer value;

    private String label;

    PayCashoutStatusEnum(Integer value, String label) {
        this.value = value;
        this.label = label;
    }


    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static PayCashoutStatusEnum getByValue(Integer value) {
        for (PayCashoutStatusEnum payCashoutStatusEnum : values()) {
            if (payCashoutStatusEnum.getValue() == value) {
                return payCashoutStatusEnum;
            }
        }
        return null;
    }
}
