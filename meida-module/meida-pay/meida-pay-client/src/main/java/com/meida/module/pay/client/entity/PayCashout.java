package com.meida.module.pay.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现申请表
 *
 * @author flyme
 * @date 2019-12-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_cashout")
@TableAlias("cashout")
@ApiModel(value = "PayCashout对象", description = "提现申请表")
public class PayCashout extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "cashoutId", type = IdType.ASSIGN_ID)
    private Long cashoutId;

    @ApiModelProperty(value = "申请人Id")
    private Long userId;

    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "申请人店铺Id")
    private Long shopId;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal money;

    @ApiModelProperty(value = "提现手续费")
    private BigDecimal commission;

    @ApiModelProperty(value = "提现手续费比例")
    private BigDecimal ratio;

    @ApiModelProperty(value = "实际到账金额")
    private BigDecimal realMoney;

    @ApiModelProperty(value = "银行卡Id")
    private Long bankCardId;

    @ApiModelProperty(value = "银行名称")
    private String bankName;

    @ApiModelProperty(value = "银行卡号")
    private String cardNo;

    @ApiModelProperty(value = "开户行支行")
    private String bankBranch;

    @ApiModelProperty(value = "aliPayUserId")
    private String aliPayUserId;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "支付宝账号")
    private String aliPayAccount;

    @ApiModelProperty(value = "微信openId")
    private String wxOpenId;

    @ApiModelProperty(value = "提现类型   aliPay支付宝提现 wxPay微信提现")
    private String cashoutType;

    @ApiModelProperty(value = "支付配置Id")
    private Long payId;

    @ApiModelProperty(value = "提现状态 -1驳回或失败 0处理中 1提现成功")
    private Integer status;

    @ApiModelProperty(value = "提现审核状态 0待审核,1审核成功,2审核失败")
    private Integer checkState;

    @ApiModelProperty(value = "提现到账时间")
    private Date arriveTime;

    @ApiModelProperty(value = "提现驳回或者失败原因")
    private String reason;

    @ApiModelProperty(value = "操作人Id")
    private Long operatorId;

}
