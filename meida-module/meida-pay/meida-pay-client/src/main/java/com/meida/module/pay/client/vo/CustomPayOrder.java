package com.meida.module.pay.client.vo;

import com.egzosn.pay.common.bean.PayOrder;

public class CustomPayOrder extends PayOrder {
    private String methedType;
    private Boolean test = false;

    public String getMethedType() {
        return methedType;
    }

    public void setMethedType(String methedType) {
        this.methedType = methedType;
    }

    public Boolean getTest() {
        return test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }


}
