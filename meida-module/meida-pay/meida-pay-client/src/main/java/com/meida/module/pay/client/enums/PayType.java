package com.meida.module.pay.client.enums;

import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.ali.api.AliPayService;
import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.BasePayType;
import com.egzosn.pay.common.bean.TransactionType;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.wx.api.WxPayConfigStorage;
import com.egzosn.pay.wx.api.WxPayService;
import com.egzosn.pay.wx.bean.WxTransactionType;
import com.meida.module.pay.client.entity.PayConfig;


/**
 * 支付类型
 */
public enum PayType implements BasePayType {

    /**
     * 支付宝
     */
    aliPay {
        @Override
        public PayService getPayService(PayConfig payConfig, String handlerName) {
            AliPayConfigStorage configStorage = new AliPayConfigStorage();
            //配置的附加参数的使用
            configStorage.setAttach(payConfig.getPayId());
            configStorage.setPid(payConfig.getPartner());
            configStorage.setAppid(payConfig.getAppId());
            configStorage.setKeyPublic(payConfig.getPublicKey());
            configStorage.setKeyPrivate(payConfig.getPrivateKey());
            configStorage.setNotifyUrl(payConfig.getNotifyUrl() + "/" + handlerName + "/" + payConfig.getPayId());
            configStorage.setReturnUrl(payConfig.getReturnUrl() + "/" + handlerName + "/" + payConfig.getPayId());
            configStorage.setSignType(payConfig.getSignType());
            configStorage.setSeller(payConfig.getSeller());
            configStorage.setPayType(payConfig.getPayType().name());
            configStorage.setMsgType(payConfig.getMsgType());
            configStorage.setInputCharset(payConfig.getInputCharset());
            configStorage.setTest(payConfig.getIsTest());
            //请求连接池配置
            HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
            //最大连接数
            httpConfigStorage.setMaxTotal(20);
            //默认的每个路由的最大连接数
            httpConfigStorage.setDefaultMaxPerRoute(10);
            return new AliPayService(configStorage, httpConfigStorage);
        }

        @Override
        public TransactionType getTransactionType(String transactionType) {
            // com.egzosn.pay.ali.before.bean.AliTransactionType 17年更新的版本,旧版本请自行切换

            // AliTransactionType 17年更新的版本,旧版本请自行切换
            return AliTransactionType.valueOf(transactionType);
        }


    },
    /**
     * 微信支付
     */
    wxPay {
        @Override
        public PayService getPayService(PayConfig payConfig, String handlerName) {
            WxPayConfigStorage wxPayConfigStorage = new WxPayConfigStorage();
            wxPayConfigStorage.setMchId(payConfig.getPartner());
            wxPayConfigStorage.setAppid(payConfig.getAppId());
            //转账公钥，转账时必填
            wxPayConfigStorage.setKeyPublic(payConfig.getPublicKey());
            wxPayConfigStorage.setSecretKey(payConfig.getPrivateKey());
            wxPayConfigStorage.setReturnUrl(payConfig.getReturnUrl() + "/" + handlerName + "/" + payConfig.getPayId());
            wxPayConfigStorage.setNotifyUrl(payConfig.getNotifyUrl() + "/" + handlerName + "/" + payConfig.getPayId());
            wxPayConfigStorage.setSignType(payConfig.getSignType());
            wxPayConfigStorage.setPayType(payConfig.getPayType().name());
            wxPayConfigStorage.setMsgType(payConfig.getMsgType());
            wxPayConfigStorage.setInputCharset(payConfig.getInputCharset());
            wxPayConfigStorage.setTest(payConfig.getIsTest());

            //https证书设置 方式一
        /*    HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
             //TODO 这里也支持输入流的入参。
            //httpConfigStorage.setKeystore(PayType.class.getResourceAsStream("/证书文件"));
            httpConfigStorage.setKeystore("证书信息串");
            httpConfigStorage.setStorePassword("证书密码");
            //设置ssl证书对应的存储方式，这里默认为文件地址
            httpConfigStorage.setCertStoreType(CertStoreType.PATH);
            return  new WxPayService(wxPayConfigStorage, httpConfigStorage);*/
            WxPayService wxPayService = new WxPayService(wxPayConfigStorage);
            //wxPayService.setPayMessageHandler(new WxPayMessageHandler(1));
            return wxPayService;
        }

        /**
         * 根据支付类型获取交易类型
         * @param transactionType 类型值
         * @see WxTransactionType
         * @return
         */
        @Override
        public TransactionType getTransactionType(String transactionType) {

            return WxTransactionType.valueOf(transactionType);
        }
    },balance {
        @Override
        public TransactionType getTransactionType(String transactionType) {
            return WxTransactionType.valueOf(transactionType);
        }

        @Override
        public PayService getPayService(PayConfig payConfig, String handlerName) {
            //这里暂时使用微信的模式，只设置payType
            WxPayConfigStorage wxPayConfigStorage = new WxPayConfigStorage();
            wxPayConfigStorage.setPayType(payConfig.getPayType().name());
            WxPayService payService = new WxPayService(wxPayConfigStorage);
            return payService;
        }
    };

    public abstract PayService getPayService(PayConfig payConfig, String handlerName);


}
