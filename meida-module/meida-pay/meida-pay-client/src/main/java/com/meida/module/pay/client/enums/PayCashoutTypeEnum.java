package com.meida.module.pay.client.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.NoArgsConstructor;

/**
 * 提现类型状态
 *
 * @author gp
 * @date 2021/8/9
 */
@NoArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ApiModel("提现类型状态")
public enum PayCashoutTypeEnum {
    /**
     * 提现类型状态
     */
    aliPay("aliPay", "支付宝"),
    wxPay("wxPay", "微信"),
    bankPay("bankPay", "银行");

    @EnumValue
    private String value;

    private String label;

    PayCashoutTypeEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }


    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static PayCashoutTypeEnum getByValue(String value) {
        for (PayCashoutTypeEnum payCashoutTypeEnum : values()) {
            if (payCashoutTypeEnum.getValue().equals(value)) {
                return payCashoutTypeEnum;
            }
        }
        return null;
    }
}
