package com.meida.module.pay.client.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 充值选项
 *
 * @author flyme
 * @date 2021-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_recharge")
@TableAlias("recharge")
@ApiModel(value="PayRecharge对象", description="充值选项")
public class PayRecharge extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "rechargeId", type = IdType.ASSIGN_ID)
    private Long rechargeId;

    @ApiModelProperty(value = "金额选项")
    private BigDecimal amount;

    @ApiModelProperty(value = "虚拟货币")
    private BigDecimal cybermoney;

    @ApiModelProperty(value = "赠送数量")
    private BigDecimal giftQuantity;

    @ApiModelProperty(value = "ios识别码")
    private String iosIdentifier;

    @ApiModelProperty(value = "状态")
    private Integer state;

    @ApiModelProperty(value = "排序号")
    private Integer sortNo;

}
