package com.meida.module.pay.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * @author zyf
 */

public enum PayTypeEnum {
    /**
     * 支付宝
     */
    aliPay("aliPay", "支付宝"),
    /**
     * 微信
     */
    wxPay("wxPay", "微信"),
    /***
     * 余额
     * */
    balance("balance", "余额");


    @EnumValue
    private String code;
    private String name;

    private PayTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    PayTypeEnum() {

    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }
}
