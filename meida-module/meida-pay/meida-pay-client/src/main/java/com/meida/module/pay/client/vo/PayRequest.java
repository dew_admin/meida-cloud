package com.meida.module.pay.client.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 支付请求参数封装
 *
 * @author zyf
 */
@Data
public class PayRequest implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 支付金额
     */
    private BigDecimal payAmount;
    /**
     * 第三方交易流水号
     */
    private String outTradeNo;

    /**
     * 业务类型 1充值 2消费3返佣,4提现,5退回
     */
    private Integer logType;
    /**
     * 业务ID
     */
    private Long busId;
    /**
     * 标题
     */
    private String title;
    /**
     * 主体
     */
    private String body;
    /**
     * 余额记录状态 余额记录状态默认是1，只有提现的时候有区分 -1提现失败 0审核中 1提现成功
     */
    private Integer status;

    /**
     * 支付密码
     */
    private String payPwd;

    /**
     * 是否使用绑定金
     */
    private Boolean isCheckPayPwd = true;

    /**
     * 是否使用绑定金
     */
    private Boolean useBindAmount;

    /**
     * 绑定金使用模式(1.绑定金,余额混用,2仅使用绑定金)
     */
    private Integer useBindAmountType = 1;

    /**
     * 是否是app扫码支付
     */
    private Object appScanPay;
}
