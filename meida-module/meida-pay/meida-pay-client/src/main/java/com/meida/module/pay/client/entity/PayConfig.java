package com.meida.module.pay.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.egzosn.pay.common.bean.MsgType;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.enums.StateEnum;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.meida.module.pay.client.enums.PayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 支付账户配置
 *
 * @author flyme
 * @date 2019-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_config")
@TableAlias("config")
@ApiModel(value = "PayConfig对象", description = "支付账户配置")
public class PayConfig extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "payId", type = IdType.ASSIGN_ID)
    private Long payId;

    @ApiModelProperty(value = "编码")
    private String payCode;

    @ApiModelProperty(value = "账户名称")
    private String payName;

    @ApiModelProperty(value = "支付合作id,商户id，差不多是支付平台的账号或id")
    private String partner;

    @ApiModelProperty(value = "应用id")
    private String appId;

    @ApiModelProperty(value = "支付平台公钥(签名校验使用)，sign_type只有单一key时public_key与private_key相等，比如sign_type=MD5(友店支付除外)的情况")
    private String publicKey;

    @ApiModelProperty(value = "应用私钥(生成签名)")
    private String privateKey;

    @ApiModelProperty(value = "异步回调地址")
    private String notifyUrl;

    @ApiModelProperty(value = "同步回调地址")
    private String returnUrl;

    @ApiModelProperty(value = "收款账号, 针对支付宝")
    private String seller;

    @ApiModelProperty(value = "签名类型")
    private String signType;

    @ApiModelProperty(value = "枚举值，字符编码 utf-8,gbk等等")
    private String inputCharset;

    @ApiModelProperty(value = "支付类型,aliPay：支付宝，wxPay：微信, youdianPay: 友店微信,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值")
    private PayType payType;

    @ApiModelProperty(value = "消息类型，text,xml,json")
    private MsgType msgType;

    @ApiModelProperty(value = "请求证书地址，请使用绝对路径")
    private String keystorePath;

    @ApiModelProperty(value = "证书对应的密码")
    private String storePassword;

    @ApiModelProperty(value = "是否为测试环境")
    private Boolean isTest;


    @ApiModelProperty(value = "是否为测试环境")
    private Boolean testAmount;

    @ApiModelProperty(value = "图标")
    private String payLogo;

    @ApiModelProperty(value = "是否启用")
    private StateEnum payStatus;

    @ApiModelProperty(value = "微信支付IOS配置")
    private String applinks;

}
