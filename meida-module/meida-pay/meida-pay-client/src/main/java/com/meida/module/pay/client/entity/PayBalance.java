package com.meida.module.pay.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 余额表
 *
 * @author flyme
 * @date 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_balance")
@TableAlias("balance")
@ApiModel(value = "PayBalance对象", description = "余额表")
public class PayBalance extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "balanceId", type = IdType.ASSIGN_ID)
    private Long balanceId;

    @ApiModelProperty(value = "账户余额")
    private BigDecimal amount;

    @ApiModelProperty(value = "账户拥有者Id")
    private Long userId;

    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "绑定金(不可提现)")
    private BigDecimal bindAmount;

    @ApiModelProperty(value = "佣金")
    private BigDecimal commAmount;

    @ApiModelProperty(value = "微信提现账户")
    private String wxOpenId;

    @ApiModelProperty(value = "支付宝提现账户")
    private String aliPayAccount;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "支付密码")
    private String paypwd;

    @ApiModelProperty(value = "版本")
    @Version
    private Long version;

}
