package com.meida.module.pay.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 支付流水信息
 *
 * @author flyme
 * @date 2019-06-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_info")
@TableAlias("payinfo")
@ApiModel(value = "OrdOrder对象", description = "支付流水表")
public class PayInfo extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "支付流水ID")
    @TableId(value = "payInfoId", type = IdType.ASSIGN_ID)
    private Long payInfoId;

    @ApiModelProperty(value = "支付流水目标Id")
    private Long busId;

    @ApiModelProperty(value = "支付流水业务类型")
    private String orderEntity;

    @ApiModelProperty(value = "支付流水标题")
    private String orderTitle;

    @ApiModelProperty(value = "支付流水描述")
    private String orderBody;


    @ApiModelProperty(value = "支付流水封面图")
    private String orderImage;

    @ApiModelProperty(value = "支付流水金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "支付流水状态")
    private String orderStatus;

    @ApiModelProperty(value = "支付状态")
    private Integer payStatus;

    @ApiModelProperty(value = "支付方式")
    private String payType;

    @ApiModelProperty(value = "支付账户")
    private String fromPay;

    @ApiModelProperty(value = "支付终端")
    private String transactionType;

    @ApiModelProperty(value = "支付时间")
    private String payDate;

    @ApiModelProperty(value = "退款日期")
    private String returnDate;

    @ApiModelProperty(value = "用户Id")
    private Long userId;

    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal returnAmount;

    @ApiModelProperty(value = "是否删除（1删除2未删除）")
    private Integer isDel;

    @ApiModelProperty(value = "退款原因")
    private String reason;

    @ApiModelProperty(value = "发票ID")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long invoiceId;

    @ApiModelProperty(value = "第三方订单号")
    private String outTradeNo;

    @ApiModelProperty(value = "联系人姓名")
    private String linkName;

    @ApiModelProperty(value = "关闭日期")
    private String closeDate;

    private Integer applyType;

    @ApiModelProperty(value = "联系电话")
    private String linkPhone;

    @ApiModelProperty(value = "企业")
    private Long companyId;

    @ApiModelProperty(value = "附加支付参数")
    private String attach;

}
