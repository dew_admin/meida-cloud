package com.meida.module.pay.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 余额记录表
 *
 * @author flyme
 * @date 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pay_balance_log")
@TableAlias("pbl")
@ApiModel(value="PayBalanceLog对象", description="余额记录表")
public class PayBalanceLog extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "balanceLogId", type = IdType.ASSIGN_ID)
    private Long balanceLogId;

    @ApiModelProperty(value = "余额ID")
    private Long balanceId;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "用户类型   USER 用户端余额     WORKER师傅端余额")
    private String userType;

    @ApiModelProperty(value = "第三方流水号")
    private String tradeNo;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "描述")
    private String body;

    @ApiModelProperty(value = "来源用户ID")
    private Long sourceUserId;

    @ApiModelProperty(value = "流水金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "操作类型  1收到工费 2充值 3洗车 4提现")
    private Integer logType;

    @ApiModelProperty(value = "状态默认为1  提现的时候：-1提现失败  0审核中 1提现成功")
    private Integer status;

    @ApiModelProperty(value = "业务Id")
    private Long busId;

}
