package com.meida.module.pay.client.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 创建订单参数模型
 */
@Data
public class OrderInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 所属业务模块
     */
    private String handlerName;
    /**
     * 购买用户
     */
    private String userId;
    /**
     * 支付对象ID
     */
    private String targetId;
    /**
     * 订单金额
     */
    private BigDecimal amount;
    /**
     * 联系人姓名
     */
    private String linkName;
    /**
     * 联系人电话
     */
    private String linkPhone;
    /**
     * 公司
     */
    private String company;
    /**
     * 创建类型（1安卓2ios）
     */
    private Integer applyType;
    /**
     * 订单名称
     */
    private String body;

}
