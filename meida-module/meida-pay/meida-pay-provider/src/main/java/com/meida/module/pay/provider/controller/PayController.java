package com.meida.module.pay.provider.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.egzosn.pay.common.api.PayConfigStorage;
import com.egzosn.pay.common.bean.PayOutMessage;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.constants.SettingConstant;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.QRCodeUtils;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.provider.response.PayResponse;
import com.meida.module.pay.provider.service.PayConfigService;
import com.meida.module.pay.provider.service.PayInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;


/**
 * 订单支付
 *
 * @author flyme
 * @date 2019-06-22
 */
@RestController
@Api(tags = "支付模块-支付管理")
public class PayController extends BaseController<PayConfigService, PayConfig> {

    @Resource
    private PayConfigService service;

    @Resource
    private PayInfoService payInfoService;


    /**
     * 获取APP支付预订单信息
     *
     * @return 支付预订单信息
     * @desc 接收参数:payId:支付方式Id,outTradeNo:订单号
     */
    @ApiOperation(value = "获取APP端支付参数", notes = "获取APP支付参数")
    @PostMapping(value = "/pay/app")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payId", value = "支付方式ID", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "支付handlerName", paramType = "form"),
            @ApiImplicitParam(name = "outTradeNo", value = "订单号", paramType = "form"),
            @ApiImplicitParam(name = "payPwd", value = "支付密码(余额支付时使用)", paramType = "form")
    })
    public ResultBody getPayInfo(@RequestParam(required = false) Map params) {
        return payInfoService.getPayInfo(params, "APP");
    }


    /**
     * 获取二维码支付图片
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payId", value = "支付方式ID", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "支付handlerName", paramType = "form"),
            @ApiImplicitParam(name = "outTradeNo", value = "订单号", paramType = "form"),
            @ApiImplicitParam(name = "addition", value = "附加信息", paramType = "form")
    })
    @ApiOperation(value = "二维码支付下单", notes = "获取二维码")
    @GetMapping(value = "/pay/qrPay")
    public ResultBody toWxQrPay(@RequestParam(required = false) Map params) {
        return payInfoService.getPayInfo(params, "NATIVE");
    }

    /**
     * 获取app支付二维码
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payId", value = "支付方式ID", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "支付handlerName", paramType = "form"),
            @ApiImplicitParam(name = "outTradeNo", value = "订单号", paramType = "form"),
            @ApiImplicitParam(name = "addition", value = "附加信息", paramType = "form")
    })
    @ApiOperation(value = "获取app支付二维码", notes = "获取app支付二维码")
    @GetMapping(value = "/pay/getPayQrCode")
    public ResultBody getPayQrCode(@RequestParam(required = false) Map params) {
        String payUrl = "pay/qrPay";
        EntityMap map = new EntityMap(params);
        //是否使用支付密码
        Integer usePwd = map.get("usePwd", 0);
        if (usePwd.equals(1)) {
            params.put("appScanPay", 1);
        }
        //定义二维码参数
        String qrcode = QRCodeUtils.createQRCodeByBase64(JSONObject.toJSONString(params));
        EntityMap result = new EntityMap();
        result.put("qrcode", qrcode);
        result.put("payUrl", payUrl);
        return ResultBody.ok(result);
    }

    /**
     * 支付回调地址
     */
    @PostMapping(value = "/common/payBack/{handlerName}/{payId}")
    public String payBack(HttpServletRequest request, @PathVariable Integer payId, @PathVariable String handlerName) throws IOException {
        System.out.println("支付回调入口" + payId);
        PayResponse payResponse = service.getPayResponse(new Long(payId), handlerName);

        System.out.println("支付回调入对象" + payResponse);
        PayConfigStorage storage = payResponse.getStorage();
        Map<String, Object> params = payResponse.getService().getParameter2Map(request.getParameterMap(), request.getInputStream());
        System.out.println("支付回调params" + params);
        if (null == params) {
            System.out.println("params为空");
            return payResponse.getService().getPayOutMessage("fail", "失败").toMessage();
        }
        //校验
        if (payResponse.getService().verify(params)) {
            PayOutMessage outMessage = payResponse.getRouter().route(params, storage);
            return outMessage.toMessage();
        } else {
            System.out.println("验签失败");
        }
        return payResponse.getService().getPayOutMessage("fail", "失败").toMessage();
    }
}
