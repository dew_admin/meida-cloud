package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayCashout;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 提现申请表 接口
 *
 * @author flyme
 * @date 2019-12-20
 */
public interface PayCashoutService extends IBaseService<PayCashout> {

    /**
     * 提现记录
     *
     * @param map
     * @param cqc
     * @return
     */
    ResultBody pageListByUserId(Map map, CriteriaQueryCallBack cqc);

    /**
     * 添加提现申请记录
     *
     * @param cashout
     * @param balanceLogType
     * @param title
     * @param body
     * @param payPwd
     * @return
     */
    ResultBody cashoutAdd(PayCashout cashout, Integer balanceLogType, String title, String body, String payPwd);

    /**
     * 统计相应提现状态
     *
     * @param status
     * @return
     */
    Long countByCashoutStatus(Integer status);

    /**
     * 合计相应提现状态
     *
     * @param status
     * @return
     */
    BigDecimal sumCashoutStatus(Long userId, Integer status);

    /**
     * 合计相应提现状态
     *
     * @param status
     * @return
     */
    BigDecimal sumCashoutStatus(Integer status);
}
