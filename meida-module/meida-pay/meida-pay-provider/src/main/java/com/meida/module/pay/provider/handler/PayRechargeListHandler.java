package com.meida.module.pay.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.pay.client.entity.PayBalance;
import com.meida.module.pay.client.entity.PayRecharge;
import com.meida.module.pay.provider.service.PayBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 充值配置list扩展
 *
 * @author zyf
 */
@Component
public class PayRechargeListHandler implements PageInterceptor<PayRecharge> {

    @Autowired
    private PayBalanceService payBalanceService;

    @Override
    public void prepare(CriteriaQuery<PayRecharge> cq, PageParams pageParams, EntityMap params) {
        cq.clear();
        cq.select(PayRecharge.class, "*");
        cq.eq(PayRecharge.class, "state", 1);
        cq.orderByAsc("sortNo");
    }

    @Override
    public void complete(CriteriaQuery<PayRecharge> cq, List<EntityMap> result, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        PayBalance payBalance = payBalanceService.addUserBalance(userId);
        //账户余额
        extra.put("balanceAmount", payBalance.getBindAmount());
    }
}
