package com.meida.module.pay.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.pay.client.entity.PayInfo;
import com.meida.module.pay.provider.service.PayInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 支付信息管理
 *
 * @author flyme
 * @date 2019-06-23
 */
@RestController
@Api(tags = "支付模块-支付管理")
@RequestMapping("/pay/")
public class PayInfoController extends BaseController<PayInfoService, PayInfo> {


    @ApiOperation(value = "支付信息-列表", notes = "订单列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "创建订单", notes = "创建订单")
    @PostMapping(value = "create")
    public ResultBody create(@RequestParam(required = false) Map params) {
        return bizService.createPayInfo(params);
    }

    @ApiOperation(value = "订单-删除", notes = "删除订单")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "查询订单支付结果", notes = "查询订单是否支付")
    @GetMapping(value = "checkPayState")
    public ResultBody checkPayState(@RequestParam(required = false) Map params) {
        return bizService.checkPayState(params);
    }

    @ApiOperation(value = "支付信息-详情", notes = "支付信息详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
