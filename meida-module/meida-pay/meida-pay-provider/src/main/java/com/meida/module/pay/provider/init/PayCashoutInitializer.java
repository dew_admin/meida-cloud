package com.meida.module.pay.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.pay.client.entity.PayCashout;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class PayCashoutInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return PayCashout.class;
    }
}
