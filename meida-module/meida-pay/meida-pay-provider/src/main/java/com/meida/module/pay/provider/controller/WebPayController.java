package com.meida.module.pay.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.JsonUtils;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.client.entity.PayInfo;
import com.meida.module.pay.provider.service.PayConfigService;
import com.meida.module.pay.provider.service.PayInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 订单支付
 *
 * @author flyme
 * @date 2019-06-22
 */
@Controller
@Api(tags = "支付模块-支付管理")
public class WebPayController extends BaseController<PayConfigService, PayConfig> {
    @Autowired
    private PayInfoService orderService;

    @Resource
    private PayInfoService payInfoService;


    /**
     * 跳到支付页面 针对实时支付,即时付款
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payId", value = "支付方式ID", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "支付handlerName", paramType = "form"),
            @ApiImplicitParam(name = "outTradeNo", value = "订单号", paramType = "form")
    })
    @ApiOperation(value = "支付宝网页支付", notes = "支付宝网页支付")
    @GetMapping(value = "/pay/toPage")
    public void toPay(HttpServletResponse response, Map params) {
        ResultBody<String> resultBody = payInfoService.getPayInfo(params, "PAGE");
        String payInfo = resultBody.getData();
        JsonUtils.writeToWeb(payInfo, response);
    }

    /**
     * 网站同步回调
     */
    @GetMapping(value = "payback")
    public String payback(String outTradeNo) {
        PayInfo order = orderService.findByOutTradeNo(outTradeNo);
        Long orderId = order.getPayInfoId();
        return "redirect:http://localhsot.com/#/success?orderId=" + orderId;
    }

}
