package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayRecharge;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 充值选项 接口
 *
 * @author flyme
 * @date 2021-08-07
 */
public interface PayRechargeService extends IBaseService<PayRecharge> {

    /**
     * 苹果内购充值
     *
     * @param receipt_data
     * @return
     */
    ResultBody appleRechargeSuccess(String receipt_data);
}
