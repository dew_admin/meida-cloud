package com.meida.module.pay.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.pay.client.entity.PayCashout;
import com.meida.module.pay.provider.service.PayCashoutService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.Map;


/**
 * 提现申请表控制器
 *
 * @author flyme
 * @date 2019-12-20
 */
@Api(tags = "后台接口")
@RestController
@RequestMapping("/pay/cashout/")
public class PayCashoutController extends BaseController<PayCashoutService, PayCashout> {

    @ApiOperation(value = "提现申请表-分页列表", notes = "提现申请表分页列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map map) {
        return bizService.pageList(map);
    }

    @ApiOperation(value = "提现申请表-列表", notes = "提现申请表列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "提现申请表-添加", notes = "添加提现申请表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "提现申请表-更新", notes = "更新提现申请表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "提现申请表-删除", notes = "删除提现申请表")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "提现申请表-详情", notes = "提现申请表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "提现申请表-更新状态值", notes = "提现申请表更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
