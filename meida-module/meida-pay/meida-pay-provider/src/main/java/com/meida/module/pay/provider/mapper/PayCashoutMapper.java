package com.meida.module.pay.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.pay.client.entity.PayCashout;


/**
 * 提现申请表 Mapper 接口
 * @author flyme
 * @date 2019-12-20
 */
public interface PayCashoutMapper extends SuperMapper<PayCashout> {

}
