package com.meida.module.pay.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.pay.client.entity.PayBalanceLog;
import com.meida.module.pay.provider.mapper.PayBalanceLogMapper;
import com.meida.module.pay.provider.service.PayBalanceLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 余额记录表 实现类
 *
 * @author flyme
 * @date 2019-12-16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PayBalanceLogServiceImpl extends BaseServiceImpl<PayBalanceLogMapper, PayBalanceLog> implements PayBalanceLogService {


    @Override
    public ResultBody listByUserIdAndUserType(Map map) {
        CriteriaQuery<PayBalanceLog> cq = new CriteriaQuery(map, PayBalanceLog.class);
        EntityMap requestMap = cq.getRequestMap();
        cq.select("pbl.*");
        //用户类型
        cq.eq("pbl.userType", requestMap.get("userType"));
        //用户Id
        cq.eq("pbl.userId", requestMap.get("userId"));
        //余额记录类型
        cq.eq("pbl.logType", requestMap.get("logType"));
        //筛选状态为1的，余额记录默认是1，只有提现的时候有区分 -1提现失败 0审核中 1提现成功
        cq.eq("status", 1);
        cq.orderByDesc("pbl.createTime");
        ResultBody resultBody = basePageList(cq);
        return resultBody;
    }

    @Override
    public PayBalanceLog findByBusIdAndType(Long busId, Integer balanceLogType) {
        QueryWrapper<PayBalanceLog> q = new QueryWrapper<>();
        PayBalanceLog one = getOne(q.eq("busId", busId).eq("logType", balanceLogType));
        return one;
    }


    @Override
    public ResultBody beforePageList(CriteriaQuery<PayBalanceLog> cq, PayBalanceLog sysBalanceLog, EntityMap requestMap) {
        cq.eq(PayBalanceLog.class, "logType");
        cq.eq(PayBalanceLog.class, "balanceId");
        cq.ge(PayBalanceLog.class, "createTime", requestMap.get("beginTime"));
        cq.le(PayBalanceLog.class, "createTime", requestMap.get("endTime"));
        cq.orderByDesc("pbl.createTime");
        return ResultBody.ok();
    }

    @Override
    public BigDecimal totalAmountByLogType(Long userId, Integer logType) {
        CriteriaQuery<PayBalanceLog> cq = new CriteriaQuery(PayBalanceLog.class);
        cq.select("ifnull(sum(amount),0) amount");
        cq.eq(true, "userId", userId);
        cq.eq(true, "logType", logType);
        return getObj(cq, e -> new BigDecimal(e.toString()));
    }

    /**
     * 添加收益日志
     *
     * @param balanceId
     * @param amount
     * @param body
     * @param title
     * @param logType
     * @param sourceUserId
     * @return
     */
    @Override
    public Boolean addBalanceLog(Long balanceId, Long userId, BigDecimal amount, String body, String title, Integer logType, Long sourceUserId) {
        PayBalanceLog balanceLog = new PayBalanceLog();
        balanceLog.setAmount(amount);
        balanceLog.setBalanceId(balanceId);
        balanceLog.setUserId(userId);
        balanceLog.setSourceUserId(sourceUserId);
        balanceLog.setBody(body);
        balanceLog.setTitle(title);
        balanceLog.setLogType(logType);
        return save(balanceLog);
    }

    @Override
    public BigDecimal totalAmountByLogTypeAndDate(Long userId, Integer logType, String date) {
        CriteriaQuery<PayBalanceLog> cq = new CriteriaQuery(PayBalanceLog.class);
        cq.select("COALESCE(SUM(amount),0) amount");
        cq.eq(true, "userId", userId);
        cq.eq(true, "logType", logType);
        cq.eq(true,"DATE_FORMAT(createTime, '%Y-%m-%d')", date);
        return getObj(cq, e -> new BigDecimal(e.toString()));
    }
}
