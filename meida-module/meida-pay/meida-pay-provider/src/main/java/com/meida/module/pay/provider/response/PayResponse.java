package com.meida.module.pay.provider.response;

import com.egzosn.pay.common.api.PayConfigStorage;
import com.egzosn.pay.common.api.PayMessageRouter;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.MsgType;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.client.enums.PayType;
import com.meida.module.pay.provider.handler.PaySuccessHandler;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 支付响应对象
 *
 * @author zyf
 */
@Data
@NoArgsConstructor
public class PayResponse {

    /**
     * 支付配置
     */
    private PayConfigStorage storage;
    /**
     * 支付接口
     */
    private PayService service;
    /**
     * 支付路由
     */
    private PayMessageRouter router;
    /**
     * 是否启用代理
     */
    private Boolean proxy = true;

    /**
     * 支付参数
     */
    private Object payInfo;

    private PayConfig payConfig;

    /**
     * 初始化支付配置
     */
    public void init(PayConfig payConfig, String handlerName) {
        //根据不同的账户类型 初始化支付配置
        this.service = payConfig.getPayType().getPayService(payConfig, handlerName);
        this.storage = service.getPayConfigStorage();
        this.payConfig = payConfig;
        //这里设置http请求配置
        /*if (proxy) {
            service.setRequestTemplateConfigStorage(getHttpConfigStorage(payConfig));
        }*/
        String keystorePath = payConfig.getKeystorePath();
        if (FlymeUtils.isNotEmpty(keystorePath)) {
            service.setRequestTemplateConfigStorage(getHttpConfigStorage(payConfig));
        }
        buildRouter(payConfig.getPayId(), handlerName);
    }

    /**
     * 获取http配置，如果配置为null则为默认配置，无代理,无证书的请求方式。
     * 此处非必需
     *
     * @param payConfig 账户信息
     * @return 请求配置
     */
    public HttpConfigStorage getHttpConfigStorage(PayConfig payConfig) {
        HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
        /* 网路代理配置 根据需求进行设置*/
        //http代理地址
        // httpConfigStorage.setHttpProxyHost("192.168.1.69");
        //代理端口
        //httpConfigStorage.setHttpProxyPort(3308);
        //代理用户名
        // httpConfigStorage.setAuthUsername("user");
        //代理密码
        // httpConfigStorage.setAuthPassword("password");
        //设置ssl证书路径 https证书设置 方式二
        httpConfigStorage.setKeystore(payConfig.getKeystorePath());
        //设置ssl证书对应的密码
        httpConfigStorage.setStorePassword(payConfig.getStorePassword());
        return httpConfigStorage;
    }


    /**
     * 配置路由
     *
     * @param payId 指定账户id，用户多微信支付多支付宝支付
     */
    private void buildRouter(Long payId, String handlerName) {
        router = new PayMessageRouter(this.service);
        PaySuccessHandler payBeforeHandler = SpringContextHolder.getHandler(handlerName, PaySuccessHandler.class);
        router
                .rule()
                //消息类型
                .msgType(MsgType.text.name())
                //支付账户事件类型
                .payType(PayType.aliPay.name())
                //拦截器
                .interceptor(payBeforeHandler)
                //处理器
                .handler(payBeforeHandler)
                .end()
                .rule()
                .msgType(MsgType.xml.name())
                .payType(PayType.wxPay.name())
                .handler(payBeforeHandler)
                .interceptor(payBeforeHandler)
                .end();
    }

}
