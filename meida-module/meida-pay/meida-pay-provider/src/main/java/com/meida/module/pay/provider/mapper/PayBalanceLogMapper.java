package com.meida.module.pay.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.pay.client.entity.PayBalanceLog;


/**
 * 余额记录表 Mapper 接口
 * @author flyme
 * @date 2019-12-16
 */
public interface PayBalanceLogMapper extends SuperMapper<PayBalanceLog> {

}
