package com.meida.module.pay.provider.handler;

import com.meida.module.pay.client.entity.PayRecharge;


/**
 * @author zyf
 */
public interface PayRechargeHandler {


    /**
     * 余额充值
     *
     * @param userId
     * @param payRecharge
     */
    Boolean balanceRecharge(Long userId, PayRecharge payRecharge);


}
