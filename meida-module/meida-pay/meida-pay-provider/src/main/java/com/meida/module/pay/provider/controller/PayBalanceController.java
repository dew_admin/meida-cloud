package com.meida.module.pay.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.pay.client.entity.PayBalance;
import com.meida.module.pay.provider.service.PayBalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.util.Map;


/**
 * 余额表控制器
 *
 * @author flyme
 * @date 2019-12-16
 */
@Api(tags = "后台接口")
@RestController
@RequestMapping("/pay/balance/")
public class PayBalanceController extends BaseController<PayBalanceService, PayBalance> {

    @ApiOperation(value = "余额表-分页列表", notes = "余额表分页列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map map) {
        return bizService.pageList(map);
    }

    @ApiOperation(value = "余额表-列表", notes = "余额表列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "余额表-添加", notes = "添加余额表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "余额表-更新", notes = "更新余额表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    @ApiOperation(value = "余额表-删除", notes = "删除余额表")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "余额表-详情", notes = "余额表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "余额表-更新状态值", notes = "余额表更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }


    @ApiOperation(value = "设置支付密码", notes = "设置支付密码")
    @PostMapping(value = "setPayPwd")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payPwd", required = true, value = "支付密码", paramType = "form"),
            @ApiImplicitParam(name = "smsCode", required = true, value = "验证码(修改时使用)", paramType = "form")
    })
    public ResultBody setPayPwd(@RequestParam(value = "payPwd") String payPwd, @RequestParam(value = "smsCode", required = false) String smsCode) {
        return bizService.setPayPwd(payPwd, smsCode);
    }

    @ApiOperation(value = "绑定提现账户", notes = "绑定提现账户")
    @PostMapping(value = "bindCashAccount")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bindCashType", required = true, value = "提现账户类型(1微信,2支付宝)", paramType = "form"),
            @ApiImplicitParam(name = "cashAccount", required = true, value = "提现账户", paramType = "form")
    })
    public ResultBody bindCashAccount(@RequestParam(value = "bindCashType") Integer bindCashType, @RequestParam(value = "cashAccount", required = false) String cashAccount) {
        return bizService.bindCashAccount(bindCashType, cashAccount);
    }

    @ApiOperation(value = "获取支付宝用户信息", notes = "获取支付宝用户信息")
    @PostMapping(value = "getAliUserInfoByAuthCode")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authCode", required = true, value = "app获取到的auth_code", paramType = "form"),
            @ApiImplicitParam(name = "payId", required = true, value = "payId", paramType = "form")
    })
    public ResultBody getAliUserInfoByAuthCode(@RequestParam(value = "authCode") String authCode, @RequestParam(value = "payId", required = false) Long payId) {
        return bizService.getAliUserInfoByAuthCode(authCode, payId);
    }

    /**
     * 导出余额表统计
     *
     * @param params
     * @param request
     * @param response
     */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "余额表统计", "余额表统计表");
    }
}
