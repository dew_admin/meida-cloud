package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayBalance;
import com.meida.module.pay.client.vo.PayRequest;

import java.math.BigDecimal;

/**
 * 余额表 接口
 *
 * @author flyme
 * @date 2019-12-16
 */
public interface PayBalanceService extends IBaseService<PayBalance> {

    /**
     * 根据用户类型和userId获取余额
     *
     * @param userId
     * @param userType
     * @return
     */
    PayBalance getByUserIdAndUserType(Long userId, String userType);

    /**
     * 添加余额和余额记录
     *
     * @param userId     用户Id
     * @param userType   用户类型
     * @param amount     金额
     * @param outTradeNo 第三方交易流水号
     * @param type       业务类型
     * @param busId      业务Id  包括 订单Id,提现申请Id
     * @param title      标题，描述
     * @param body
     * @return
     */
    boolean addBalance(Long userId, String userType, BigDecimal amount, String outTradeNo, Integer type, Long busId, String title, String body);

    /**
     * 减少余额和添加余额记录
     *
     * @param payRequest
     * @return
     */
    boolean subtractBalance(PayRequest payRequest);

    /**
     * 创建用户账户
     *
     * @param userId
     * @return
     */
    PayBalance addUserBalance(Long userId);

    /**
     * 设置支付密码
     *
     * @return
     */
    ResultBody setPayPwd(String paypwd, String smsCode);

    /**
     * 查询用户账户
     *
     * @param userId
     * @return
     */
    PayBalance getByUserId(Long userId);


    /**
     * 设置账户余额
     *
     * @param userId
     * @return
     */
    Boolean addBindAmount(Long userId, Long sourceUserId, BigDecimal amount);

    /**
     * 设置账户绑定金余额
     *
     * @param userId
     * @return
     */
    Boolean addBindAmount(Long userId, Long sourceUserId, BigDecimal amount, Integer logType, String title, String body);

    /**
     * 绑定提现账号
     *
     * @param bindCashType
     * @param cashAccount
     * @return
     */
    ResultBody bindCashAccount(Integer bindCashType, String cashAccount);

    /**
     * 支付宝
     * 得到 auth_code 后换取 access_token 和 userId
     *
     * @return
     */
    ResultBody getAliUserInfoByAuthCode(String authCode,Long payId);
}
