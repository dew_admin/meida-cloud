package com.meida.module.pay.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.pay.client.entity.PayRecharge;
import com.meida.module.pay.provider.service.PayRechargeService;


/**
 * 充值选项控制器
 *
 * @author flyme
 * @date 2021-08-07
 */
@RestController
@RequestMapping("/pay/recharge/")
public class PayRechargeController extends BaseController<PayRechargeService, PayRecharge> {

    @ApiOperation(value = "充值选项-分页列表", notes = "充值选项分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "充值选项-列表", notes = "充值选项列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "充值选项-添加", notes = "添加充值选项")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "充值选项-更新", notes = "更新充值选项")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "充值选项-删除", notes = "删除充值选项")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "充值选项-详情", notes = "充值选项详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "更新充值选项状态", notes = "更新充值选项状态")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }

    @ApiOperation(value = "苹果内购充值", notes = "苹果内购充值")
    @PostMapping(value = "appleRecharge/success")
    public ResultBody appleRecharge(@RequestParam(value = "receipt_data") String receipt_data) {
        return bizService.appleRechargeSuccess(receipt_data);
    }
}
