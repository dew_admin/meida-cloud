package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayInfo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 支付信息接口
 *
 * @author flyme
 * @date 2019-06-23
 */
public interface PayInfoService extends IBaseService<PayInfo> {
    /**
     * 创建支付信息
     *
     * @param params
     * @return
     */
    ResultBody createPayInfo(Map params);

    /**
     * 获取支付信息
     *
     * @param params
     * @param tradeType 交易类型(可选值 APP,PAGE,NATIVE)
     * @return
     */
    ResultBody getPayInfo(Map params, String tradeType);

    /**
     * 根据订单号查询订单
     *
     * @param outTradeNo
     * @return
     */
    PayInfo findByOutTradeNo(String outTradeNo);

    /**
     * 查询未支付订单
     *
     * @param cls
     * @param companyId
     * @return
     */
    PayInfo findNoPayOrder(Class cls, Long companyId);

    /**
     * 更新发票ID
     *
     * @param orderId
     * @param invoiceId
     * @return
     */
    Boolean updateInvoiceId(Long orderId, Long invoiceId);


    /**
     * 检查订单是否支付
     *
     * @param params
     * @return
     */
    ResultBody checkPayState(Map params);

    /**
     * 清空订单发票ID
     *
     * @param invoiceIds
     * @return
     */
    Boolean clearInvoiceId(Long[] invoiceIds);

    /**
     * 订单回调校验
     *
     * @param outTradeNo
     * @return
     */
    Boolean validate(String outTradeNo);

    /**
     * 获取创建订单时参数
     *
     * @param outTradeNo
     * @return
     */
    EntityMap getOrderParams(String outTradeNo);

    /**
     * 订单支付成功处理
     *
     * @param context
     * @return
     */
    Boolean success(Map<String, Object> context, Integer orderStatus);


    /**
     * 合计相应支付状态金额
     *
     * @param payStatus
     * @return
     */
    BigDecimal sumPayStatus(Long userId, Integer payStatus);
}
