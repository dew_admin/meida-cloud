package com.meida.module.pay.provider.mapper;

import com.meida.module.pay.client.entity.PayBalance;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 余额表 Mapper 接口
 * @author flyme
 * @date 2020-03-02
 */
public interface PayBalanceMapper extends SuperMapper<PayBalance> {

}
