package com.meida.module.pay.provider.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.pay.client.entity.PayRecharge;
import com.meida.module.pay.provider.handler.PayRechargeHandler;
import com.meida.module.pay.provider.mapper.PayRechargeMapper;
import com.meida.module.pay.provider.service.PayRechargeService;
import com.meida.module.pay.provider.utils.IosVerifyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 充值选项接口实现类
 *
 * @author flyme
 * @date 2021-08-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PayRechargeServiceImpl extends BaseServiceImpl<PayRechargeMapper, PayRecharge> implements PayRechargeService {

    @Autowired(required = false)
    private PayRechargeHandler payRechargeHandler;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, PayRecharge recharge, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<PayRecharge> cq, PayRecharge recharge, EntityMap requestMap) {
        cq.orderByAsc("recharge.sortNo");
        return ResultBody.ok();
    }

    @Override
    public ResultBody appleRechargeSuccess(String receipt_data) {
        //首先调用正式环境查看验证结果;
        //type:1->正式环境,发送平台验证
        String verifyResult = IosVerifyUtil.buyAppVerify(receipt_data, 1);
        if (verifyResult == null) {
            //如果验证结果不存在;提示前端;订单信息不存在;
            return ResultBody.failed("验证失败");
        }
        JSONObject job = JSONObject.parseObject(verifyResult);
        String states = job.getString("status");
        //states为21007表示,收据信息是沙盒测试用;需要请求沙盒测试地址对凭证进行校验
        if ("21007".equals(states)) {
            //如果正式环境校验不成功,则请求沙盒测试地址对凭证进行校验
            //type:0->沙盒测试,发送平台验证
            verifyResult = IosVerifyUtil.buyAppVerify(receipt_data, 0);
            job = JSONObject.parseObject(verifyResult);
            states = job.getString("status");
        }
        if (states.equals("0")) {
            // 前端所提供的收据是有效的,验证成功
            String r_receipt = job.getString("receipt");
            JSONObject returnJson = JSONObject.parseObject(r_receipt);
            String in_app = returnJson.getString("in_app");
            JSONArray jsonArray = JSONArray.parseArray(in_app);
            JSONObject in_appJson = jsonArray.getJSONObject(jsonArray.size() - 1);
            System.out.println("IOS支付验证结果3:" + in_appJson);
            //todo 业务代码
            String iosIdentifier = in_appJson.getString("product_id");
            Long userId = OpenHelper.getUserId();
            PayRecharge payRecharge = getByProperty("iosIdentifier", iosIdentifier);
            if (FlymeUtils.isNotEmpty(payRechargeHandler)) {
                payRechargeHandler.balanceRecharge(userId, payRecharge);
            } else {
                return ResultBody.failed("充值失败");
            }
            return ResultBody.ok().setMsg("支付成功");
            //支付成功
        } else {
            return ResultBody.failed("支付失败");
        }
    }
}
