package com.meida.module.pay.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.pay.client.entity.PayBalanceLog;
import org.springframework.stereotype.Component;

/**
 * 用户收支明细扩展
 *
 * @author zyf
 */
@Component("userBalanceLogListHandler")
public class UserBalanceLogListHandler implements PageInterceptor<PayBalanceLog> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("sourceUserId");
        cq.eq(PayBalanceLog.class, "userId", userId);
    }


}
