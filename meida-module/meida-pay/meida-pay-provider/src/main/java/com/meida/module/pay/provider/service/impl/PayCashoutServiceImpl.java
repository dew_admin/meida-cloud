package com.meida.module.pay.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.ApiAssert;
import com.meida.module.pay.client.entity.PayCashout;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.client.vo.PayRequest;
import com.meida.module.pay.provider.mapper.PayCashoutMapper;
import com.meida.module.pay.provider.service.PayBalanceService;
import com.meida.module.pay.provider.service.PayCashoutService;
import com.meida.module.pay.provider.service.PayConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 提现申请表 实现类
 *
 * @author flyme
 * @date 2019-12-20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PayCashoutServiceImpl extends BaseServiceImpl<PayCashoutMapper, PayCashout> implements PayCashoutService {

    @Resource
    private PayBalanceService balanceService;
    @Resource
    private PayConfigService payConfigService;

    @Override
    public ResultBody pageListByUserId(Map params, CriteriaQueryCallBack cqc) {
        CriteriaQuery<PayCashout> cq = new CriteriaQuery(params, PayCashout.class);
        EntityMap paramsMap = cq.getRequestMap();
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        } else {
            cq.select("cashout.*");
        }
        cq.eq("cashout.userId", paramsMap.get("userId"));
        cq.eq("cashout.userType", paramsMap.get("userType"));
        cq.eq("cashout.cashoutType", paramsMap.get("cashoutType"));
        cq.eq("cashout.status", paramsMap.get("status"));
        cq.orderByDesc("cashout.createTime");
        return basePageList(cq);
    }

    @Override
    public ResultBody cashoutAdd(PayCashout cashout, Integer balanceLogType, String title, String body, String payPwd) {
        PayConfig payConfig = payConfigService.getById(cashout.getPayId());
        cashout.setCashoutType(payConfig.getPayType().name());
        boolean insert = cashout.insert();
        if (insert) {
            PayRequest payRequest = new PayRequest();
            payRequest.setUserId(cashout.getUserId());
            payRequest.setUserType(cashout.getUserType());
            payRequest.setPayAmount(cashout.getMoney());
            payRequest.setOutTradeNo(null);
            payRequest.setLogType(balanceLogType);
            payRequest.setBusId(cashout.getCashoutId());
            payRequest.setTitle(title);
            payRequest.setBody(body);
            payRequest.setStatus(0);
            payRequest.setPayPwd(payPwd);
            payRequest.setUseBindAmount(false);
            boolean b = balanceService.subtractBalance(payRequest);
            if (!b) {
                ApiAssert.failure("系统繁忙，请稍后再试！");
            }
        }
        return ResultBody.result("提交", insert);
    }

    @Override
    public Long countByCashoutStatus(Integer status) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq("status", status);
        return count(qw);
    }

    @Override
    public BigDecimal sumCashoutStatus(Long userId, Integer status) {
        CriteriaQuery cq = new CriteriaQuery(PayCashout.class);
        cq.addSelect("COALESCE(sum(money),0) amount");
        cq.eq("status", status);
        cq.eq("userId", userId);
        EntityMap one = findOne(cq);
        BigDecimal amount = one.getBigDecimal("amount");
        return amount;
    }

    @Override
    public BigDecimal sumCashoutStatus(Integer status) {
        CriteriaQuery cq = new CriteriaQuery(PayCashout.class);
        cq.addSelect("COALESCE(sum(money),0) amount");
        cq.eq("status", status);
        EntityMap one = findOne(cq);
        BigDecimal amount = one.getBigDecimal("amount");
        return amount;
    }
}
