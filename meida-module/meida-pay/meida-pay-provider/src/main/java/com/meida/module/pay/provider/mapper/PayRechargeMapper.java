package com.meida.module.pay.provider.mapper;

import com.meida.module.pay.client.entity.PayRecharge;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 充值选项 Mapper 接口
 * @author flyme
 * @date 2021-08-07
 */
public interface PayRechargeMapper extends SuperMapper<PayRecharge> {

}
