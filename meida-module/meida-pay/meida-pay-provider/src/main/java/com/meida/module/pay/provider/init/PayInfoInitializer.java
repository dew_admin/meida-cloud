package com.meida.module.pay.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.pay.client.entity.PayInfo;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class PayInfoInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return PayInfo.class;
    }
}
