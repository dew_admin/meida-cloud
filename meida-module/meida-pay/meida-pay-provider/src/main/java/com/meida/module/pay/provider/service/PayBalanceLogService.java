package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayBalanceLog;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 余额记录表 接口
 *
 * @author flyme
 * @date 2019-12-16
 */
public interface PayBalanceLogService extends IBaseService<PayBalanceLog> {

    /**
     * 根据userId和userType获取余额记录
     * @param map
     * @return
     */
    ResultBody listByUserIdAndUserType(Map map);

    /**
     * 根据业务Id和余额记录类型查找
     * @param busId
     * @param balanceLogType
     * @return
     */
    PayBalanceLog findByBusIdAndType(Long busId, Integer balanceLogType);

    /**
     * 根据收支类型统计账户收益
     *
     * @param userId
     * @param logType
     * @return
     */
    BigDecimal totalAmountByLogType(Long userId, Integer logType);

    /**
     * 添加收益来源
     */
    Boolean addBalanceLog(Long balanceId,Long userId, BigDecimal amount, String body, String title, Integer logType, Long sourceUserId);


    /**
     * 根据收支类型统计账户收益
     *
     * @param userId
     * @param logType
     * @return
     */
    BigDecimal totalAmountByLogTypeAndDate(Long userId, Integer logType,String date);
}
