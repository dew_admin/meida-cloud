package com.meida.module.pay.provider.mapper;

import com.meida.module.pay.client.entity.PayConfig;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 支付账户配置 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-22
 */
public interface PayConfigMapper extends SuperMapper<PayConfig> {

}
