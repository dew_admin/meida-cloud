package com.meida.module.pay.provider.controller;


import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.provider.service.PayConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.Map;


/**
 * 支付账户配置
 *
 * @author flyme
 * @date 2019-06-22
 */
@RestController
@RequestMapping("/payconfig/")
@Api(tags = "支付模块-支付配置")
public class PayConfigController extends BaseController<PayConfigService, PayConfig> {


    @ApiOperation(value = "支付方式", notes = "支付方式 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "支付方式-列表", notes = "支付方式列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "获取Applink", notes = "获取Applink")
    @GetMapping(value = "getAppLinks/{payId}")
    public String getApplinks(@PathVariable Long payId) {
        PayConfig payConfig = bizService.getById(payId);
        return payConfig.getApplinks();
    }


    @ApiOperation(value = "支付方式-更新", notes = "更新支付方式")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "支付方式-启用状态", notes = "支付方式-启用状态")
    @PostMapping(value = "setPayStatus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "payStatus", required = true, value = "启用状态", paramType = "query")
    })
    public ResultBody setPayStatus(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "payStatus") Integer payStatus) {
        return bizService.setPayStatus(ids, payStatus);
    }

    @ApiOperation(value = "支付方式-修改测试状态", notes = "支付方式-修改测试状态")
    @PostMapping(value = "setIsTest")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "isTest", required = true, value = "测试状态", paramType = "query")
    })
    public ResultBody setIsTest(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "isTest") Integer isTest) {
        return bizService.setIsTest(ids, isTest);
    }
}
