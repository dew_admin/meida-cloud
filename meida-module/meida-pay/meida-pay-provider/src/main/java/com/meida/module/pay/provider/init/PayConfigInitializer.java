package com.meida.module.pay.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.pay.client.entity.PayConfig;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class PayConfigInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return PayConfig.class;
    }
}
