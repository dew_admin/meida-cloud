package com.meida.module.pay.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.pay.client.entity.PayInfo;


/**
 * 支付信息表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-23
 */
public interface PayInfoMapper extends SuperMapper<PayInfo> {

}
