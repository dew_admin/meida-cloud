package com.meida.module.pay.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.pay.client.entity.PayConfig;
import com.meida.module.pay.provider.response.PayResponse;

import java.util.List;

/**
 * 支付账户配置 接口
 *
 * @author flyme
 * @date 2019-06-22
 */
public interface PayConfigService extends IBaseService<PayConfig> {
    /**
     * 获取支付响应对象
     *
     * @param payId
     * @param handlerName
     * @return
     */
    PayResponse getPayResponse(Long payId,String handlerName);


    /**
     * 返回支付类型
     *
     * @return
     */
    List<EntityMap> selectPayTypes();



    /**
     * 设置启用状态
     *
     * @param ids
     * @param payStatus
     * @return
     */
    ResultBody setPayStatus(Long[] ids, Integer payStatus);

    /**
     * 设置测试状态
     *
     * @param ids
     * @param isTest
     * @return
     */
    ResultBody setIsTest(Long[] ids, Integer isTest);

    /**
     * 执行提现操作  微信转账到零钱或者到支付宝余额（本操作适用于后台审核提现，不适用于自动提现）
     * @param cashoutId 提现申请Id
     * @param balanceLogType 余额记录类型
     * @param handlerName 处理器
     * @return
     */
    ResultBody  doCashout(Long cashoutId,Integer balanceLogType,String handlerName);
}
