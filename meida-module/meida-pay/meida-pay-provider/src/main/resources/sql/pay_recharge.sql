/*
 Navicat Premium Data Transfer

 Source Server         : 8.131.232.142
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 8.131.232.142:3306
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 07/08/2021 15:54:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_recharge
-- ----------------------------
DROP TABLE IF EXISTS `pay_recharge`;
CREATE TABLE `pay_recharge`  (
  `rechargeId` int(11) NOT NULL COMMENT '主键',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额选项',
  `cybermoney` decimal(10, 2) NULL DEFAULT NULL COMMENT '虚拟货币',
  `giftQuantity` decimal(10, 2) NULL DEFAULT NULL COMMENT '赠送数量',
  `iosIdentifier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ios识别码',
  `state` int(1) NULL DEFAULT NULL COMMENT '状态',
  `sortNo` int(1) NULL DEFAULT NULL COMMENT '排序号',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`rechargeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '充值选项' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
