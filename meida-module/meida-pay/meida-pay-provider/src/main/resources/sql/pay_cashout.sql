/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 07/08/2021 20:15:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_cashout
-- ----------------------------
DROP TABLE IF EXISTS `pay_cashout`;
CREATE TABLE `pay_cashout`  (
  `cashoutId` bigint(20) NOT NULL COMMENT '主键',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '申请人Id',
  `userType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户类型',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '申请人店铺Id',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '提现金额',
  `commission` decimal(10, 2) NULL DEFAULT NULL COMMENT '提现手续费',
  `ratio` decimal(10, 4) NULL DEFAULT NULL COMMENT '提现手续费比例',
  `realMoney` decimal(10, 2) NULL DEFAULT NULL COMMENT '实际到账金额',
  `bankCardId` bigint(20) NULL DEFAULT NULL COMMENT '银行卡Id',
  `bankName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行名称',
  `aliPayUserId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'aliPayUserId',
  `cardNo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `bankBranch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '开户行支行',
  `realName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `aliPayAccount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '支付宝账号',
  `wxOpenId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信openId',
  `cashoutType` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提现类型  aliPay支付宝提现 wxPay微信提现',
  `payId` bigint(20) NULL DEFAULT NULL COMMENT '支付配置Id',
  `status` int(1) NULL DEFAULT 0 COMMENT '提现状态 -1驳回或失败 0处理中 1提现成功',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提现驳回或者失败原因',
  `operatorId` bigint(20) NULL DEFAULT NULL COMMENT '操作人Id',
  `updateTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`cashoutId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '提现申请表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
