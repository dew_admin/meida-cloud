/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 07/08/2021 20:11:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_balance_log
-- ----------------------------
DROP TABLE IF EXISTS `pay_balance_log`;
CREATE TABLE `pay_balance_log`  (
  `balanceLogId` bigint(20) NOT NULL COMMENT '主键',
  `balanceId` bigint(20) NULL DEFAULT NULL COMMENT '余额ID',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `tradeNo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方流水号',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `body` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `busId` bigint(20) NULL DEFAULT NULL COMMENT '业务ID',
  `amount` decimal(32, 2) NULL DEFAULT NULL COMMENT '流水金额',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  `sourceUserId` bigint(20) NULL DEFAULT NULL COMMENT '来源用户ID',
  `userType` int(1) NULL DEFAULT NULL COMMENT '账户类型',
  `logType` int(11) NULL DEFAULT NULL COMMENT '操作类型 1充值 2消费3=返佣',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `updateTime` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`balanceLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pay_balance_log
-- ----------------------------
INSERT INTO `pay_balance_log` VALUES (1215279850609688578, 1215279850177675265, 1214824862389334017, NULL, '收益', '邀请奖励', NULL, 10.00, NULL, 1213277468715585538, NULL, 1, '2020-01-09 22:31:09', NULL);

SET FOREIGN_KEY_CHECKS = 1;
