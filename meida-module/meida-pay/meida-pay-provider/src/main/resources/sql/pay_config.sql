/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 10:51:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_config
-- ----------------------------
DROP TABLE IF EXISTS `pay_config`;
CREATE TABLE `pay_config`  (
  `payId` bigint(20) NOT NULL COMMENT '主键',
  `payName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户名称',
  `payCode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `partner` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付合作id,商户id，差不多是支付平台的账号或id',
  `appId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用id',
  `publicKey` varchar(1204) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付平台公钥(签名校验使用)，sign_type只有单一key时public_key与private_key相等，比如sign_type=MD5(友店支付除外)的情况',
  `privateKey` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用私钥(生成签名)',
  `notifyUrl` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异步回调地址',
  `returnUrl` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步回调地址',
  `seller` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款账号, 针对支付宝',
  `signType` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签名类型',
  `inputCharset` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举值，字符编码 utf-8,gbk等等',
  `payType` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付类型,aliPay：支付宝，wxPay：微信, youdianPay: 友店微信,此处开发者自定义对应com.egzosn.pay.demo.entity.PayType枚举值',
  `msgType` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型，text,xml,json',
  `keystorePath` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求证书地址，请使用绝对路径',
  `storePassword` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书对应的密码',
  `isTest` int(1) NULL DEFAULT 0 COMMENT '是否为测试环境',
  `payStatus` int(1) NULL DEFAULT NULL COMMENT '是否启用',
  `applinks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payLogo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'logo',
  `updateTime` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改日期',
  `createTime` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`payId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付账户配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pay_config
-- ----------------------------
INSERT INTO `pay_config` VALUES (1, '支付宝', 'AliPay', '2088631939728787', '2019112569412515', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnNYDODxeCZ3Qyr898p4YSjrGnkGyezefWfYaOxKdEB5vLoS4painz8OFjr0B2pscpundrSuIYe3uQwVhn+lEJxS53lwK+6qcq29mDtxoPLUam2GpjF0UCqF0WDvP7nWrppsyN4jW1saNE13TpTw3rq/MkM11U47TfbiOy8X4LWV/pX4nq8zxdAkrqVOnrbi0zoVp9s0RX6eVCfqtu9nqT5wIl1HSSvVsIvH0T2voxkIcTJ2R7D1//46329wwOTWkbyOCUZ6qbBVIs1mdNoMYI3L38WdNDJL2UAtAPTyPS//sEoFT+Dt4axCF6BX4wOMCfK9HQhBTTfpo5oM7z72qoQIDAQAB', 'MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDbGtqiRK7EgHaoXmnOsjXRAdCHZb0AsC++1IzBZMugL5t8BYiY4GrUX9zup/jPJloY+SIG+FRSU0ovRKeBK1n84ggdUpd4VaeDYuXjnN2cb5xpZcKqyPQfZJpJsp2/eD/Lfnnk1xrPnIn4Cu4wROZGQjvJKWhjAFluhva7NSCfGnER11W3h5E+6VkKI3lVWFKGnOYeFCE2q299qWCurrXeti1qJzu28mKMdMoqHKxvyykEEVRqGkUX44j1fykIMMra2z3n4la4iSz8UEUQKTVK/ZIVd0PknConqu8gIFFG5aGiETjSFI4RdcApdi+ZeWrwh2RMC6aG4LSmjr8MLjBnAgMBAAECggEAUhU1YyUn9CXMTjdQnEbXMn3jiVXsAJ4AxRULxkfkoRY0wbmw+tTYMMfZFO+IntJXXpO207WkoVLir+Kgfhunkr17HMEcTdCI+QfL6AGUZE7L4hBlZWXzTu95fbKUtm5uN+Yh9EFBohWa+nnd0ed9/URC8ZG5sZ0n4+hb9rZoijHEJb2PfnXaTkyw3htmRgcJV0pmLPyzDRXIGtZzYynwLrCvM7SvmS0CCoOGUJW2FlNpinRbMS0FGum2VheWZM/k62fBAIt5/3brK32qKLFT3+Wff1kpkzFpKYxXMaukpSEPc52L0cFpmKuzGPXdxlNfzr2P9A2bYmIDv3YtvySMgQKBgQD1/KZto0+5KK4WRGvYeIOP32+9pXQTTwB0b/3RYxwrblymoW/JEz4wPOHKS+/Fv9Ih9nBrY/1YdZm7k18IT5ptC+64L9DACP2J2v/T/idGVbmRm1fROoiGjay8NSseyuOSb+8r93u6e/gkrcOfN+g/WscrjIvfhOlhogL+jhll/wKBgQDkBhM9XtGRzuwb3zKemHWB23BAACTvBdr5NjmM2/3G/c/qAHsFY20tT5l/SGdjhGojr88QK9BzbtVs9TUaAII+07aXJkveeaZiys3sCna/+5bsJ8kanciaj9enTVuQkXg12f6wL/WMfimnHNhH2ZbHTF+Ou1qUirXHncJAOn3FmQKBgDmfTX0xXGOLW/xrWrgCww7Y0FwkfeRoBTtBYLvD871YxOtjtcPsdgzmth2lXC1+mcLK79q4OYiXQ8Pk6cYQn6Qbv3f1+3iAoqPAYV1Pk/Ot8SJiuVeR1pluZqBP9CHqdRTAk85A0wMEN0SgT8rkVctBgJZ6/ekGQYVVZk4rC1gRAoGAMp0SmjW4yOeoSwuA/ekTdC6UPPCLQi8Hive5UIXoT8o441UV+X03V65qgPE+Wb2jnaZBjPJcw7KfVx8amVnxjyaJSgWrI4fXIeioSrOL5lNfDa5LGkhZpc017iHiqDVR/IoV2V/b3kKG1lw1ucUXjcdrEpCNq5kafmOLIZyaHEkCgYARRVsgM0hAnF4QFtCcjaxEi7BziVeAbgoI4mdTdQfYu0br3rCMtWPmfwcDqkqhouPOzlxZscHxY5nIiMqVao9GkXVNBymGJb80NXD5xipnvrJ8IYh1dpJQnnEvEo2t9v1Xb/1v9XC2aSW2f1SZPOr+Dfd1UwzS0ps2eQSPJ9ySjg==', 'https://api.hswlm.cn/api/common/payBack', 'https://api.hswlm.cn/api/common/payBack', '2088631939728787', 'RSA2', 'utf-8', 'aliPay', 'text', NULL, NULL, 0, 1, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/1c6019df242a4ba4b760ed32ea4ed51c.png', '2020-01-05 15:21:59', NULL);
INSERT INTO `pay_config` VALUES (2, '公众号', 'WxPay', '1512689611', 'wxabe6c35370058d8e', NULL, 'tYlnOzraXdOxnErrS4na7wmgHDADp1CO', 'https://api.hswlm.cn/api/common/payBack', 'https://api.hswlm.cn/api/common/payBack', NULL, 'MD5', 'utf-8', 'wxPay', 'xml', '', '', 0, 0, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/3666c7f5fae9401297c67f9e553b8173.png', '2020-01-05 15:22:31', NULL);
INSERT INTO `pay_config` VALUES (3, '微信', 'WxPay', '1565284011', 'wxa0517853ca1e0fcb', '', 'wins77317wins77317wins77317wins7', 'https://api.hswlm.cn/api/common/payBack', 'https://api.hswlm.cn/api/common/payBack', NULL, 'MD5', 'utf-8', 'wxPay', 'xml', '', '', 0, 1, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/f70e81f1278e42109d33a46f17e0dca9.png', '2020-01-05 15:22:16', NULL);
INSERT INTO `pay_config` VALUES (4, '余额', 'Balance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MD5', 'utf-8', 'balance', 'xml', NULL, NULL, 0, 1, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b9b27276ebd44daf95833417562e4c51.png', '2020-02-19 09:00:09', NULL);

INSERT INTO `base_menu` VALUES (106, 1, 'pay', '支付配置', '支付配置', '/', 'system/payconfig/index', 'alipay', '_self', 'PageView', 1, 9, 1, 0, 'meida-base-provider', '2018-12-27 15:26:54', '2020-01-05 15:19:01');
INSERT INTO `base_authority` VALUES (106, 'MENU_pay', 106, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-05 15:19:01');


SET FOREIGN_KEY_CHECKS = 1;
