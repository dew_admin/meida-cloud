/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 10:51:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_balance
-- ----------------------------
DROP TABLE IF EXISTS `pay_balance`;
CREATE TABLE `pay_balance`  (
  `balanceId` bigint(20) NOT NULL COMMENT '主键',
  `amount` decimal(32, 2) NULL DEFAULT 0.00 COMMENT '账户余额',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '账户拥有者Id',
  `bindAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '绑定金(不可提现)',
  `userType` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户类型   user 用户端余额     worker师傅端余额',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `paypwd` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付密码',
  `version` bigint(32) NULL DEFAULT 1 COMMENT '版本',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`balanceId`) USING BTREE,
  UNIQUE INDEX `uid_utp_index`(`userId`, `userType`) USING BTREE COMMENT '保证一个用户类型有一个余额'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额表' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (552, 550, 'balance', '会员余额', '会员余额', '/', 'finance/balance/index', 'bars', '_self', 'PageView', 1, 99, 1, 0, 'meida-base-provider', '2020-03-02 14:21:11', '2020-03-02 14:21:11');
INSERT INTO `base_authority` VALUES (552, 'MENU_balance', 552, NULL, NULL, 1, '2020-03-02 14:21:11', '2020-03-02 14:21:11');

SET FOREIGN_KEY_CHECKS = 1;
