package com.meida.module.admin.provider.mapper;


import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseUserAccountLogsMapper extends SuperMapper<BaseUserAccountLogs> {
}
