package com.meida.module.admin.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;


/**
 * @author zyf
 */

public interface BaseUserMapper extends SuperMapper<BaseUser> {

    @Select("select count(1) from sys_dept where deptId =  #{deptId}")
    long selectByDeptId(@Param("deptId") Long deptId);

    @Select("select deptCode from sys_dept where deptId =  #{deptId}")
    String selectDeptCodeByDeptId(@Param("deptId") Long deptId);

    @Select("select companyId from sys_dept where deptId =  #{deptId}")
    String selectCompanyIdByDeptId(@Param("deptId") Long deptId);

    @Select("select deptId from sys_dept where deptCode = #{deptCode} and companyId= #{companyId} ")
    Long selectByDeptCode(@Param("deptCode") String deptCode, @Param("companyId") Long companyId);

    @Select("select companyId from sys_company where companyNo =  #{companyNo}")
    Long selectByCompanyNo(@Param("companyNo") String companyNo);

    @Select("select * from sys_dept where deptCode =  #{deptCode} and companyId= #{companyId} ")
    Map selectDeptByDeptCode(@Param("deptCode") String deptCode, @Param("companyId") Long companyId);
}
