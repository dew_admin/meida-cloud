package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayIpLimit;
import com.meida.common.mybatis.base.mapper.SuperMapper;



public interface GatewayIpLimitMapper extends SuperMapper<GatewayIpLimit> {
}
