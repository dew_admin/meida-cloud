package com.meida.module.admin.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.client.entity.BaseRoleUser;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseRoleUserMapper extends SuperMapper<BaseRoleUser> {
    /**
     * 查询系统用户角色
     *
     * @param userId
     * @return
     */
    List<BaseRole> selectRoleUserList(@Param("userId") Long userId);


    /**
     * 根据校色
     *
     * @param roleCode
     * @param companyId
     * @return
     */
    List<Long> selectUserIdByRoleCode(@Param("roleCode") String roleCode,@Param("companyId") Long companyId);


    /**
     * 查询系统用户角色code
     *
     * @param userId
     * @return
     */
    List<String> selectRoleCodesByUserId(@Param("userId") Long userId);

    /**
     * 查询用户角色名称
     * @param userId
     * @return
     */
    List<String> selectRoleNamesByUserId(@Param("userId") Long userId);

    /**
     * 查询用户角色ID列表
     * @param userId
     * @return
     */
    List<Long> selectRoleUserIdList(@Param("userId") Long userId);
}
