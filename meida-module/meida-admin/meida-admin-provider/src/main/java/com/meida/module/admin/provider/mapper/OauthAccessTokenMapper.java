package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthAccessToken;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2访问令牌 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthAccessTokenMapper extends SuperMapper<OauthAccessToken> {

}
