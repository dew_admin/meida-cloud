package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayRateLimit;
import com.meida.common.mybatis.base.mapper.SuperMapper;


public interface GatewayRateLimitMapper extends SuperMapper<GatewayRateLimit> {
}
