package com.meida.module.admin.provider.service;


import com.meida.common.base.service.CommonAppService;
import com.meida.module.admin.client.entity.BaseApp;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.security.OpenClientDetails;

/**
 * 应用信息管理
 *
 * @author zyf
 */
public interface BaseAppService extends IBaseService<BaseApp>, CommonAppService {

    /**
     * 查询应用列表
     *
     * @param pageParams
     * @return
     */
    ResultBody findListPage(PageParams pageParams);

    /**
     * 获取app信息
     *
     * @param appId
     * @return
     */
    BaseApp getAppInfo(String appId);




    /**
     * 更新应用开发新型
     *
     * @param client
     */
    void updateAppClientInfo(OpenClientDetails client);

    /**
     * 添加应用
     *
     * @param app 应用
     * @return 应用信息
     */
    BaseApp addAppInfo(BaseApp app);

    /**
     * 修改应用
     *
     * @param app 应用
     * @return 应用信息
     */
    BaseApp updateInfo(BaseApp app);


    /**
     * 重置秘钥
     *
     * @param appId
     * @return
     */
    String restSecret(String appId);

    /**
     * 删除应用
     *
     * @param appId
     * @return
     */
    void removeApp(String appId);
}
