package com.meida.module.admin.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.admin.client.entity.BaseAccount;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class BaseAccountLogInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return BaseUserAccountLogs.class;
    }
}
