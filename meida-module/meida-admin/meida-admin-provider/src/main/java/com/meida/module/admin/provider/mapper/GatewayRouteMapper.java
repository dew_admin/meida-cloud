package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayRoute;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * @author zyf
 */
public interface GatewayRouteMapper extends SuperMapper<GatewayRoute> {
}
