package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthClientToken;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2客户端令牌 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthClientTokenMapper extends SuperMapper<OauthClientToken> {

}
