package com.meida.module.admin.provider.service.impl;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.admin.client.entity.BaseUserImportRecord;
import com.meida.module.admin.provider.mapper.BaseUserImportRecordMapper;
import com.meida.module.admin.provider.service.BaseUserImportRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户导入接口实现类
 *
 * @author flyme
 * @date 2023-06-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseUserImportRecordServiceImpl extends BaseServiceImpl<BaseUserImportRecordMapper, BaseUserImportRecord> implements BaseUserImportRecordService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, BaseUserImportRecord buir, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<BaseUserImportRecord> cq, BaseUserImportRecord buir, EntityMap requestMap) {
        cq.orderByDesc("buir.createTime");
        return ResultBody.ok();
    }
}
