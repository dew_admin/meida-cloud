package com.meida.module.admin.provider.service.impl;

import com.meida.common.exception.OpenAlertException;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenClientDetails;
import com.meida.common.utils.BeanConvertUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.RandomValueUtils;
import com.meida.common.constants.AuthConstants;
import com.meida.module.admin.client.constants.BaseConstants;
import com.meida.module.admin.client.entity.BaseApp;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.mapper.BaseAppMapper;
import com.meida.module.admin.provider.service.BaseAppService;
import com.meida.module.admin.provider.service.BaseAuthorityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author: zyf
 * @date: 2018/11/12 16:26
 * @description:
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseAppServiceImpl extends BaseServiceImpl<BaseAppMapper, BaseApp> implements BaseAppService {

    @Resource
    private BaseAppMapper baseAppMapper;
    @Resource
    private BaseAuthorityService baseAuthorityService;
    @Resource
    private JdbcClientDetailsService jdbcClientDetailsService;

    /**
     * 查询应用列表
     *
     * @param pageParams
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody findListPage(PageParams pageParams) {
        BaseApp query = pageParams.mapToObject(BaseApp.class);
        CriteriaQuery<BaseApp> cq = new CriteriaQuery(pageParams);
        cq.lambda()
                .eq(FlymeUtils.isNotEmpty(query.getUserId()), BaseApp::getUserId, query.getUserId())
                .eq(FlymeUtils.isNotEmpty(query.getAppType()), BaseApp::getAppType, query.getAppType())
                .eq(FlymeUtils.isNotEmpty(query.getAppId()), BaseApp::getAppId, query.getAppId())
                .likeRight(FlymeUtils.isNotEmpty(query.getAppName()), BaseApp::getAppName, query.getAppName())
                .likeRight(FlymeUtils.isNotEmpty(query.getAppNameEn()), BaseApp::getAppNameEn, query.getAppNameEn());
        cq.select("app.*,baseuser.userName");
        //关联User表
        cq.createJoin(BaseUser.class);
        cq.orderByDesc("createTime");
        return basePageList(cq);
    }

    /**
     * 获取app详情
     *
     * @param appId
     * @return
     */
//    @Cacheable(value = "apps", key = "#appId")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BaseApp getAppInfo(String appId) {
        return baseAppMapper.selectById(appId);
    }

    /**
     * 获取app和应用信息
     *
     * @param clientId
     * @return
     */
    @Override
//    @Cacheable(value = "apps", key = "'client:'+#clientId")
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public OpenClientDetails getAppClientInfo(String clientId) {
        BaseClientDetails baseClientDetails;
        try {
            baseClientDetails = (BaseClientDetails) jdbcClientDetailsService.loadClientByClientId(clientId);
        } catch (Exception e) {
            return null;
        }
        OpenClientDetails openClient = new OpenClientDetails();
        Set<String> autoApproveScopes= baseClientDetails.getAutoApproveScopes();
        if (FlymeUtils.isEmpty(autoApproveScopes)) {
            baseClientDetails.setAutoApproveScopes(new HashSet<>());
        }
        BeanUtils.copyProperties(baseClientDetails, openClient);
        String appId = baseClientDetails.getAdditionalInformation().get("appId").toString();
        openClient.setAuthorities(baseAuthorityService.findAuthorityByApp(appId));
        return openClient;
    }

    /**
     * 更新应用开发新型
     *
     * @param client
     */
    @CacheEvict(value = {"apps"}, key = "'client:'+#client.clientId")
    @Override
    public void updateAppClientInfo(OpenClientDetails client) {
        BaseApp app = getAppInfo(client.getClientId());
        Map info = BeanConvertUtils.objectToMap(app);
        client.setAdditionalInformation(info);
        jdbcClientDetailsService.updateClientDetails(client);
    }


    /**
     * 添加应用
     *
     * @param app
     * @return 应用信息
     */
    @CachePut(value = "apps", key = "#app.appId")
    @Override
    public BaseApp addAppInfo(BaseApp app) {
        String clientId = String.valueOf(System.currentTimeMillis());
        String clientSecret = RandomValueUtils.uuid();
        app.setAppId(clientId);
        app.setSecretKey(clientSecret);
        if (app.getIsPersist() == null) {
            app.setIsPersist(0);
        }
        baseAppMapper.insert(app);
        Map info = BeanConvertUtils.objectToMap(app);
        // 功能授权
        BaseClientDetails client = new BaseClientDetails();
        client.setClientId(app.getAppId());
        client.setClientSecret(app.getSecretKey());
        client.setAuthorizedGrantTypes(Arrays.asList("authorization_code", "client_credentials", "implicit", "refresh_token"));
        client.setAdditionalInformation(info);
        client.setAccessTokenValiditySeconds(AuthConstants.ACCESS_TOKEN_VALIDITY_SECONDS);
        client.setRefreshTokenValiditySeconds(AuthConstants.REFRESH_TOKEN_VALIDITY_SECONDS);
        jdbcClientDetailsService.addClientDetails(client);
        return app;
    }

    /**
     * 修改应用
     *
     * @param app 应用
     * @return 应用信息
     */
    @Caching(evict = {
            @CacheEvict(value = {"apps"}, key = "#app.appId"),
            @CacheEvict(value = {"apps"}, key = "'client:'+#app.appId")
    })
    @Override
    public BaseApp updateInfo(BaseApp app) {
        baseAppMapper.updateById(app);
        // 修改客户端附加信息
        BaseApp appInfo = getAppInfo(app.getAppId());
        Map info = BeanConvertUtils.objectToMap(appInfo);
        BaseClientDetails client = (BaseClientDetails) jdbcClientDetailsService.loadClientByClientId(appInfo.getApiKey());
        client.setAdditionalInformation(info);
        jdbcClientDetailsService.updateClientDetails(client);
        return app;
    }

    /**
     * 重置秘钥
     *
     * @param appId
     * @return
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = {"apps"}, key = "#appId"),
            @CacheEvict(value = {"apps"}, key = "'client:'+#appId")
    })
    public String restSecret(String appId) {
        BaseApp appInfo = getAppInfo(appId);
        if (appInfo == null) {
            throw new OpenAlertException(appId + "应用不存在!");
        }
        if (appInfo.getIsPersist().equals(BaseConstants.ENABLED)) {
            throw new OpenAlertException(String.format("保留数据,不允许修改"));
        }
        // 生成新的密钥
        String secretKey = RandomValueUtils.randomAlphanumeric(32);
        appInfo.setSecretKey(secretKey);
        baseAppMapper.updateById(appInfo);
        jdbcClientDetailsService.updateClientSecret(appInfo.getApiKey(), secretKey);
        return secretKey;
    }

    /**
     * 删除应用
     *
     * @param appId
     * @return
     */
    @Caching(evict = {
            @CacheEvict(value = {"apps"}, key = "#appId"),
            @CacheEvict(value = {"apps"}, key = "'client:'+#appId")
    })
    @Override
    public void removeApp(String appId) {
        BaseApp appInfo = getAppInfo(appId);
        if (appInfo == null) {
            throw new OpenAlertException(appId + "应用不存在!");
        }
        if (appInfo.getIsPersist().equals(BaseConstants.ENABLED)) {
            throw new OpenAlertException(String.format("保留数据,不允许删除"));
        }
        // 移除应用权限
        baseAuthorityService.removeAuthorityApp(appId);
        baseAppMapper.deleteById(appInfo.getAppId());
        jdbcClientDetailsService.removeClientDetails(appInfo.getApiKey());
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String apiKey = String.valueOf(RandomValueUtils.randomAlphanumeric(24));
        String secretKey = String.valueOf(RandomValueUtils.randomAlphanumeric(32));
        System.out.println("apiKey=" + apiKey);
        System.out.println("secretKey=" + secretKey);
        System.out.println("encodeSecretKey=" + encoder.encode(secretKey));
    }


}
