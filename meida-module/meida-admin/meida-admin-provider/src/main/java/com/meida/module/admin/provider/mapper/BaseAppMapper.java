package com.meida.module.admin.provider.mapper;


import com.meida.module.admin.client.entity.BaseApp;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseAppMapper extends SuperMapper<BaseApp> {

}
