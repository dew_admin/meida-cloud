package com.meida.module.admin.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;

/**
 * 登录日志
 *
 * @author zyf
 */
public interface BaseUserAccountLogsService extends IBaseService<BaseUserAccountLogs> {


}
