package com.meida.module.admin.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseUserCompany;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseUserCompanyMapper extends SuperMapper<BaseUserCompany> {
    /**
     * 查询企业用户ID
     *
     * @param companyId
     * @return
     */
    List<Long> selectUserIdByCompanyId(@Param("companyId") Long companyId);

    Integer countByCompanyIdAndStatus(@Param("companyId") Long companyId,@Param("status") Integer status);
}
