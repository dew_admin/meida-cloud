package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthRefreshToken;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2刷新令牌 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthRefreshTokenMapper extends SuperMapper<OauthRefreshToken> {

}
