package com.meida.module.admin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.PageParams;
import com.meida.module.admin.client.entity.BaseMenu;

import java.util.List;

/**
 * 菜单资源管理
 *
 * @author zyf
 */
public interface BaseMenuService extends IBaseService<BaseMenu> {
    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<BaseMenu> findListPage(PageParams pageParams);

    /**
     * 查询列表
     *
     * @return
     */
    List<BaseMenu> findAllList();

    /**
     * 根据主键获取菜单
     *
     * @param menuId
     * @return
     */
    BaseMenu getMenu(Long menuId);

    /**
     * 检查菜单编码是否存在
     *
     * @param menuCode
     * @return
     */
    Boolean isExist(String menuCode);


    /**
     * 根据菜单名称获取菜单
     *
     * @param menuName
     * @return
     */
    BaseMenu getMenu(String menuName);


}
