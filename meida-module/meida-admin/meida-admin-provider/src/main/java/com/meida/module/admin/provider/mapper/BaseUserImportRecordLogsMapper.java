package com.meida.module.admin.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseUserImportRecordLogs;

/**
 * 用户导入日志记录 Mapper 接口
 *
 * @author flyme
 * @date 2023-06-29
 */
public interface BaseUserImportRecordLogsMapper extends SuperMapper<BaseUserImportRecordLogs> {

}
