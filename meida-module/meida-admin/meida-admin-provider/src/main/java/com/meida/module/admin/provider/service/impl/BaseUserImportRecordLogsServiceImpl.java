package com.meida.module.admin.provider.service.impl;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.admin.client.entity.BaseUserImportRecordLogs;
import com.meida.module.admin.provider.mapper.BaseUserImportRecordLogsMapper;
import com.meida.module.admin.provider.service.BaseUserImportRecordLogsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户导入日志记录接口实现类
 *
 * @author flyme
 * @date 2023-06-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseUserImportRecordLogsServiceImpl extends BaseServiceImpl<BaseUserImportRecordLogsMapper, BaseUserImportRecordLogs> implements BaseUserImportRecordLogsService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, BaseUserImportRecordLogs buirl, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<BaseUserImportRecordLogs> cq, BaseUserImportRecordLogs buirl, EntityMap requestMap) {
        cq.orderByDesc("buirl.createTime");

        return ResultBody.ok();
    }
}
