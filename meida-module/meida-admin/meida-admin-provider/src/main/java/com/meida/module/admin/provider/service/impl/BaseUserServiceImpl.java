package com.meida.module.admin.provider.service.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.handler.AdminUserInfoHandler;
import com.meida.common.base.handler.UserDeptAssignHandler;
import com.meida.common.base.service.BaseAdminUserService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.enums.StateEnum;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.oauth2.OpenOAuth2ClientProperties;
import com.meida.common.security.OpenAuthority;
import com.meida.common.security.OpenHelper;
import com.meida.common.security.SecurityConstants;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.ExcelUtils;
import com.meida.common.utils.event.FlymeEventClient;
import com.meida.module.admin.client.constants.BaseConstants;
import com.meida.module.admin.client.entity.*;
import com.meida.module.admin.client.model.UserInfo;
import com.meida.module.admin.provider.mapper.BaseRoleUserMapper;
import com.meida.module.admin.provider.mapper.BaseUserCompanyMapper;
import com.meida.module.admin.provider.mapper.BaseUserDeptMapper;
import com.meida.module.admin.provider.mapper.BaseUserMapper;
import com.meida.module.admin.provider.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author: zyf
 * @date: 2018/10/24 16:33
 * @description:
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseUserServiceImpl extends BaseServiceImpl<BaseUserMapper, BaseUser> implements BaseUserService, BaseAdminUserService {

    @Resource
    private BaseUserMapper baseUserMapper;
    @Resource
    private BaseRoleService roleService;

    @Resource
    private BaseRoleUserMapper roleUserMapper;
    @Resource
    private AutowireCapableBeanFactory spring;
    @Resource
    private BaseAuthorityService baseAuthorityService;

    @Resource
    private BaseRoleService baseRoleService;

    @Resource
    private BaseUserCompanyMapper baseUserCompanyMapper;

    @Resource
    private BaseUserDeptMapper baseUserDeptMapper;

    @Resource
    private BaseAccountService baseAccountService;

    @Resource
    private BaseRoleUserMapper baseRoleUserMapper;

    @Autowired(required = false)
    private UserDeptAssignHandler userDeptAssignHandler;

    @Resource
    private FlymeEventClient flymeEventClient;

    @Autowired
    private OpenOAuth2ClientProperties clientProperties;

    @Autowired
    private RedisTokenStore redisTokenStore;

    @Autowired(required = false)
    private AdminUserInfoHandler adminUserInfoHandler;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    BaseUserImportRecordService baseUserImportRecordService;

    @Autowired
    BaseUserImportRecordLogsService baseUserImportRecordLogsService;


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, BaseUser baseUser, EntityMap extra) {
        String userName = baseUser.getUserName();
        if (getUserByUsername(userName) != null) {
            return ResultBody.failed("用户名:" + userName + "已注册");
        }
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        if (CommonConstants.INT_0.equals(superAdmin)) {
            Long organizationId = OpenHelper.getOrganizationId();
            if (FlymeUtils.isNotEmpty(organizationId)) {
                baseUser.setOrganizationId(organizationId);
            }
        }
        baseUser.setStatus(CommonConstants.ENABLED);
        return super.beforeAdd(cs, baseUser, extra);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<BaseUser> cq, BaseUser baseUser, EntityMap requestMap) {
        PageParams pageParams = cq.getPageParams();
        BaseUser query = cq.getEntity(BaseUser.class);
        if (FlymeUtils.isNotEmpty(query)) {
            cq.lambda()
                    .eq(FlymeUtils.isNotEmpty(query.getUserId()), BaseUser::getUserId, query.getUserId())
                    .eq(FlymeUtils.isNotEmpty(query.getUserType()), BaseUser::getUserType, query.getUserType())
                    .like(FlymeUtils.isNotEmpty(query.getUserName()), BaseUser::getUserName, query.getUserName())
                    .like(FlymeUtils.isNotEmpty(query.getNickName()), BaseUser::getNickName, query.getNickName())
                    .like(FlymeUtils.isNotEmpty(query.getMobile()), BaseUser::getMobile, query.getMobile())
                    .like(FlymeUtils.isNotEmpty(query.getCertNo()), BaseUser::getCertNo, query.getCertNo())
                    .like(FlymeUtils.isNotEmpty(query.getCustomDeptName()), BaseUser::getCustomDeptName, query.getCustomDeptName())
                    .like(FlymeUtils.isNotEmpty(query.getUserNo()), BaseUser::getUserNo, query.getUserNo())
                    .ge(FlymeUtils.isNotEmpty(pageParams.getBeginDate()), BaseUser::getCreateTime, pageParams.getBeginDate())
                    .le(FlymeUtils.isNotEmpty(pageParams.getEndDate()), BaseUser::getCreateTime, pageParams.getEndDate())
                    .eq(FlymeUtils.isNotEmpty(query.getStatus()), BaseUser::getStatus, query.getStatus());
            if (FlymeUtils.isNotEmpty(query.getStr())) {
                cq.and(i -> i.like("userName", query.getStr()).or().like("nickName", query.getStr()));

            }
        }
        cq.ne("userName", CommonConstants.ROOT);
        cq.orderByDesc("createTime");
        return super.beforePageList(cq, baseUser, requestMap);
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<BaseUser> cq, BaseUser baseUser, EntityMap requestMap) {
        cq.eq("deptId", requestMap.getLong("deptId"));
        cq.eq("companyId", requestMap.getLong("companyId"));
        cq.like("userName", requestMap.get("userName"));
        cq.like("nickName", requestMap.get("nickName"));
        cq.eq("userType", requestMap.get("userType"));
        return super.beforeListEntityMap(cq, baseUser, requestMap);
    }

    /**
     * 查询列表
     *
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseUser> findAllList() {
        List<BaseUser> list = baseUserMapper.selectList(new QueryWrapper<>());
        return list;
    }

    /**
     * 查询列表
     *
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseUser> findByUserType(Integer userType) {
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        cq.eq("userType", userType);
        List<BaseUser> list = baseUserMapper.selectList(cq);
        return list;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseUser> listByUserIds(List ids) {
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        cq.in("userId", ids);
        List<BaseUser> list = baseUserMapper.selectList(cq);
        return list;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseUser> findByUserIds(String ids) {
        String[]arrayIds=ids.split(",");
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        cq.in("userId", arrayIds);
        List<BaseUser> list = baseUserMapper.selectList(cq);
        //创建id到索引的映射
        Map<String, Integer> userIdMap = new HashMap<>();
        for (int i = 0; i < arrayIds.length; i++) {
            userIdMap.put(arrayIds[i], i);
        }
        list.sort((p1, p2) -> {
            Integer index1 = userIdMap.getOrDefault(p1.getUserId().toString(), Integer.MIN_VALUE);
            Integer index2 = userIdMap.getOrDefault(p2.getUserId().toString(), Integer.MIN_VALUE);
            return index1.compareTo(index2);
        });
        return list;
    }

    /**
     * 依据系统用户Id查询系统用户信息
     *
     * @param userId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BaseUser getUserById(Long userId) {
        return baseUserMapper.selectById(userId);
    }


    /**
     * 根据用户ID获取用户信息和权限
     *
     * @param userId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public UserInfo getUserWithAuthoritiesById(Long userId) {
        // 用户权限列表
        List<OpenAuthority> authorities = Lists.newArrayList();
        // 用户角色列表
        List<Map> roles = Lists.newArrayList();
        List<BaseRole> rolesList = roleService.getUserRoles(userId);
        Integer superAdmin = 0;
        if (rolesList != null) {
            for (BaseRole role : rolesList) {
                String roleCode = role.getRoleCode();
                Map roleMap = Maps.newHashMap();
                roleMap.put("roleId", role.getRoleId());
                roleMap.put("roleCode", role.getRoleCode());
                roleMap.put("roleName", role.getRoleName());
                // 用户角色详情
                roles.add(roleMap);
                // 加入角色标识
                OpenAuthority authority = new OpenAuthority(role.getRoleId().toString(), SecurityConstants.AUTHORITY_PREFIX_ROLE + role.getRoleCode(), null, "role");
                authorities.add(authority);
                if (roleCode.equals("superadmin") || roleCode.equals("admin")) {
                    superAdmin = 1;
                }
            }
        }

        //查询系统用户资料
        BaseUser baseUser = getUserById(userId);

        // 加入用户权限
        List<OpenAuthority> userGrantedAuthority = baseAuthorityService.findAuthorityByUser(userId, CommonConstants.ROOT.equals(baseUser.getUserName()));
        if (userGrantedAuthority != null && userGrantedAuthority.size() > 0) {
            authorities.addAll(userGrantedAuthority);
        }
        UserInfo userProfile = new UserInfo();
        BeanUtils.copyProperties(baseUser, userProfile);
        userProfile.setSuperAdmin(superAdmin);
        //设置用户资料,权限信息
        userProfile.setAuthorities(authorities);
        userProfile.setRoles(roles);
        return userProfile;
    }


    /**
     * 依据登录名查询系统用户信息
     *
     * @param username
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BaseUser getUserByUsername(String username) {
        QueryWrapper<BaseUser> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(BaseUser::getUserName, username);
        BaseUser saved = baseUserMapper.selectOne(queryWrapper);
        return saved;
    }

    /**
     * 设置用户状态
     *
     * @param userId
     * @return
     */
    @Override
    public ResultBody setStatus(Long userId) {
        ResultBody resultBody = new ResultBody();
        EntityMap user = findById(userId);
        Boolean allowUpdate = true;
        if (FlymeUtils.isNotEmpty(adminUserInfoHandler)) {
            resultBody = adminUserInfoHandler.beforeUpdateState(user, userId);
        }
        if (resultBody.isOk()) {
            Integer stateEnum = user.getInt("status");
            CriteriaUpdate cu = new CriteriaUpdate();
            cu.eq("userId", userId);
            if (stateEnum.equals(StateEnum.ENABLE.getCode())) {
                cu.set(true, "status", StateEnum.DISABLE.getCode());
                resultBody.setMsg(StateEnum.DISABLE.getName() + "成功").data(false);
            } else {
                cu.set(true, "status", StateEnum.ENABLE.getCode());
                resultBody.setMsg(StateEnum.ENABLE.getName() + "成功").data(true);
            }
            update(cu);
        }
        return resultBody;

    }

    /**
     * 设置在线状态
     *
     * @param userId
     * @return
     */
    @Override
    public ResultBody setOnLine(Long userId, Integer onLine) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.eq("userId", userId);
        cu.set(true, "onLineState", onLine);
        update(cu);
        return ResultBody.ok("操作成功");

    }

    /**
     * 设置在线状态
     *
     * @return
     */
    @Override
    public Long countOnLine() {
        CriteriaQuery cu = new CriteriaQuery(BaseUser.class);
        cu.eq("onLineState", CommonConstants.ENABLED);
        return count(cu);
    }

    @Override
    public ResultBody deleteUser(String accountName) {
        BaseAccount baseAccount = baseAccountService.getAccount(accountName);
        if (FlymeUtils.isNotEmpty(baseAccount)) {
            Long userId = baseAccount.getUserId();
            if (FlymeUtils.isNotEmpty(userId)) {
                baseRoleService.removeUserRoles(userId);
                baseAccountService.removeAccount(userId);
                removeById(userId);
            }
        }
        return ResultBody.ok();
    }

    /**
     * 移除企业所有组员
     *
     * @param userId
     * @return
     */
    @Override
    public void removeUserCompnay(Long userId) {
        QueryWrapper<BaseUserCompany> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(BaseUserCompany::getUserId, userId);
        baseUserCompanyMapper.delete(queryWrapper);
    }


    @Override
    public ResultBody removeToken(String accountName) {
        Collection<OAuth2AccessToken> accessTokens = redisTokenStore.findTokensByClientIdAndUserName(clientProperties.getOauth2().get("admin").getClientId(), accountName);
        if (FlymeUtils.isNotEmpty(accessTokens)) {
            for (OAuth2AccessToken accessToken : accessTokens) {
                redisTokenStore.removeAccessToken(accessToken);
            }
        }
        return ResultBody.ok();
    }

    /**
     * 解除锁定
     *
     * @param userId
     * @return
     */
    @Override
    public ResultBody unLock(Long userId) {
        BaseUser user = getUserById(userId);
        if (FlymeUtils.isNotEmpty(user)) {
            List<BaseAccount> baseAccounts = baseAccountService.selectByUserId(userId);
            if (FlymeUtils.isNotEmpty(baseAccounts)) {
                for (BaseAccount baseAccount : baseAccounts) {
                    String errorKey = CommonConstants.ACCOUNT_LOCK_ERROR_KEY + baseAccount.getAccount();
                    String errorTime = CommonConstants.ACCOUNT_LOCK_ERROR_TIME + baseAccount.getAccount();
                    String errorCountKey = CommonConstants.ACCOUNT_LOCK_ERROR_COUNT + baseAccount.getAccount();
                    Boolean hasKey = redisUtils.hasKey(errorKey);
                    if (hasKey) {
                        redisUtils.del(errorKey);
                    }
                    if (redisUtils.hasKey(errorTime)) {
                        redisUtils.del(errorTime);
                    }
                    if (redisUtils.hasKey(errorCountKey)) {
                        redisUtils.del(errorCountKey);
                    }

                }
            }
        }
        return ResultBody.ok();
    }

    /**
     * 移除部门所有组员
     *
     * @param userId
     * @return
     */
    private void removeUserDept(Long userId) {
        QueryWrapper<BaseUserDept> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(BaseUserDept::getUserId, userId);
        baseUserDeptMapper.delete(queryWrapper);
    }

    /**
     * 用户分配企业
     *
     * @param userId
     * @param companyIds
     */
    @Override
    public void saveUserCompanys(Long userId, String... companyIds) {
        ApiAssert.isNotEmpty("用户ID不能为空", userId);
        // 先清空,在添加
        removeUserCompnay(userId);
        if (FlymeUtils.isNotEmpty(companyIds)) {
            for (String companyId : companyIds) {
                BaseUserCompany userCompany = new BaseUserCompany();
                userCompany.setUserId(userId);
                userCompany.setCompanyId(Long.parseLong(companyId));
                baseUserCompanyMapper.insert(userCompany);
            }

        }
    }

    /**
     * 用户分配部门
     *
     * @param userId
     * @param deptIds
     */
    @Override
    public void saveUserDepts(Long userId, String... deptIds) {
        ApiAssert.isNotEmpty("用户ID不能为空", userId);
        // 先清空,在添加
        removeUserDept(userId);
        if (FlymeUtils.isNotEmpty(deptIds)) {
            for (String deptId : deptIds) {
                BaseUserDept userDept = new BaseUserDept();
                userDept.setUserId(userId);
                userDept.setDeptId(Long.parseLong(deptId));
                baseUserDeptMapper.insert(userDept);
            }

        }
    }

    /**
     * 获取分配部门数据
     *
     * @param userId
     * @param organizationId
     * @return
     */
    @Override
    public ResultBody getAuthDeptList(Long userId, Long organizationId) {
        EntityMap map = new EntityMap();
        if (FlymeUtils.isNotEmpty(userDeptAssignHandler)) {
            map = userDeptAssignHandler.getAuthDeptList(userId, organizationId);
            List<Long> deptIds = baseUserDeptMapper.selectUserDeptIds(userId);
            map.put("deptIds", deptIds);
        }
        return ResultBody.ok(map);
    }

    @Override
    public void saveUserRole(Long userId, String roleCode) {
        BaseRole baseRole = baseRoleService.getRoleByRoleCode(roleCode);
        ApiAssert.isNotEmpty(roleCode + "角色不存在", baseRole);
        if (FlymeUtils.isNotEmpty(baseRole)) {
            BaseRoleUser roleUser = new BaseRoleUser();
            roleUser.setUserId(userId);
            roleUser.setRoleId(baseRole.getRoleId());
            baseRoleUserMapper.insert(roleUser);
        }

    }


    @Override
    public Boolean saveCompany(Long userId, String companyIds) {
        Boolean tag = false;
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq(true, "userId", userId);
        baseUserCompanyMapper.delete(cd);
        if (FlymeUtils.isNotEmpty(companyIds)) {
            String[] array = companyIds.split(",");
            for (String s : array) {
                BaseUserCompany baseUserCompany = new BaseUserCompany();
                baseUserCompany.setUserId(userId);
                baseUserCompany.setCompanyId(Long.parseLong(s));
                baseUserCompanyMapper.insert(baseUserCompany);
                tag = true;
            }
        }
        return tag;
    }

    @Override
    public Boolean saveDept(Long userId, String deptIds) {
        Boolean tag = false;
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq(true, "userId", userId);
        baseUserDeptMapper.delete(cd);
        if (FlymeUtils.isNotEmpty(deptIds)) {
            String[] array = deptIds.split(",");
            for (String s : array) {
                BaseUserDept baseUserDept = new BaseUserDept();
                baseUserDept.setUserId(userId);
                baseUserDept.setDeptId(Long.parseLong(s));
                baseUserDeptMapper.insert(baseUserDept);
                tag = true;
            }
        }
        return tag;
    }

    @Override
    public Boolean saveDeptAndCompany(Long userId, String deptIds, String companyIds) {
        saveDept(userId, deptIds);
        saveCompany(userId, companyIds);
        return true;
    }

    @Override
    public List<Long> selectCompanyIdsByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(BaseUserCompany.class);
        cq.select(BaseUserCompany.class, "companyId");
        cq.eq("userId", userId);
        return listObjs(e -> Long.parseLong(e.toString()));
    }

    @Override
    public List<Long> selectDeptIdsByUserId(Long userId, Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(BaseUserDept.class);
        cq.select(BaseUserDept.class, "deptId");
        cq.eq("userId", userId);
        cq.eq("companyId", companyId);
        return listObjs(e -> Long.parseLong(e.toString()));
    }

    @Override
    public ResultBody beforeDelete(CriteriaDelete<BaseUser> cd) {
        Long userId = OpenHelper.getUserId();
        Long[] userIds = cd.getIds();
        if (FlymeUtils.contains(userIds, userId)) {
            return ResultBody.failed("禁止删除当前用户");
        }
        for (Long id : userIds) {
            BaseUser user = getUserById(id);
            //发送删除minio账户通知
            EntityMap map = new EntityMap();
            map.put("userId", user.getUserId());
            map.put("userName", user.getUserName());
            map.put("optType", "delete");
            flymeEventClient.publishEvent("minioAddUserListener", map);
        }
        return super.beforeDelete(cd);
    }

    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        if (FlymeUtils.isNotEmpty(ids)) {
            baseRoleService.removeUserRoles(ids);
            baseAccountService.removeAccount(ids);
        }
        return ResultBody.ok();
    }

    @Override
    public Boolean deleteByCompanyId(Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        cq.eq(true, "companyId", companyId);
        List<BaseUser> userList = baseUserMapper.selectList(cq);
        if (FlymeUtils.isNotEmpty(userList)) {
            for (BaseUser baseUser : userList) {
                baseRoleService.removeUserRoles(baseUser.getUserId());
                baseAccountService.removeAccount(baseUser.getUserId());
                removeById(baseUser.getUserId());
            }
        }
        return true;
    }

    @Override
    public List<Long> selectUserIdByRoleCode(String roleCode, Long companyId) {
        return roleUserMapper.selectUserIdByRoleCode(roleCode, companyId);
    }

    @Override
    public Long countByCompanyIdAndDeptId(Long companyId, Long deptId) {
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(true, "companyId", companyId);
        }
        if (FlymeUtils.isNotEmpty(deptId)) {
            cq.eq(true, "deptId", deptId);
        }
        return count(cq);
    }

    @Override
    public Long countByCompanyId(Long companyId) {
        return countByCompanyIdAndDeptId(companyId, null);
    }

    @Override
    public Integer countByCompanyIdAndStatus(Long companyId, Integer status) {
        return baseUserCompanyMapper.countByCompanyIdAndStatus(companyId, status);
    }

    @Override
    public Long countByDeptId(Long deptId) {
        return countByCompanyIdAndDeptId(null, deptId);
    }

    @Override
    public EntityMap getAdminUserById(Long userId, String userType) {
        CriteriaQuery cq = new CriteriaQuery(BaseUser.class);
        cq.eq(true, "userId", userId);
        cq.eq("userType", userType);
        return findOne(cq);
    }

    @Override
    public BaseUserImportRecord importFile(String deptId, MultipartFile file) {

        try {


            List<Map> read = ExcelUtils.importExcel(file, 0, 1, Map.class);


            List<BaseUser> userList = new ArrayList<>();
            List<BaseUser> errUserList = new ArrayList<>();
            List<BaseAccount> accountList = new ArrayList<>();
            List<BaseUserImportRecordLogs> recordLogsList = new ArrayList<>();

            BaseUserImportRecord importRecord = BaseUserImportRecord.builder().importId(IdWorker.getId()).deptId(deptId).importNum(read.size()).successNum(userList.size()).failNum(errUserList.size()).deleted(CommonConstants.DEL_NO).fileName(file.getOriginalFilename()).build();

            for (int i = 0; i < read.size(); i++) {
                StringBuffer errMsg = new StringBuffer();
                Map objects = read.get(i);
                if (ObjectUtil.isNull(objects.get("姓名"))) {
                    errMsg.append("第" + (i + 1) + "行姓名为空");
                }
                if (ObjectUtil.isNull(objects.get("账号"))) {
                    errMsg.append("第" + (i + 1) + "行账号为空");
                }
                if (ObjectUtil.isNull(objects.get("密码"))) {
                    errMsg.append("第" + (i + 1) + "行密码为空");
                }
                if (ObjectUtil.isNull(objects.get("机构全宗号"))) {
                    errMsg.append("第" + (i + 1) + "行机构全宗号为空");
                }
                BaseUser user = new BaseUser();
                user.setUserId(IdWorker.getId());
                user.setStatus(CommonConstants.ENABLED);
                user.setUserType(CommonConstants.ENABLED_STR);
                if (ObjectUtil.isNotNull(objects.get("姓名"))) {
                    user.setNickName(objects.get("姓名").toString());
                }
                if (ObjectUtil.isNotNull(objects.get("账号"))) {
                    user.setUserName(objects.get("账号").toString());
                }
                if ((objects.get("账号") + "").length() < 6) {
                    errMsg.append("账号:" + user.getUserName() + "密码长度不足6位");
                }
                //加密
                if (ObjectUtil.isNotNull(objects.get("密码"))) {
                    String encodePassword = passwordEncoder.encode(objects.get("密码").toString());
                    user.setPassword(encodePassword);
                }
                if (ObjectUtil.isNotNull(objects.get("机构全宗号"))) {
                    user.setCompanyId(baseUserMapper.selectByCompanyNo(objects.get("机构全宗号").toString()));
                }
                if (ObjectUtil.isNotNull(objects.get("部门机构代码"))) {
                    user.setDeptId(StrUtil.isNotEmpty(deptId) ? Long.valueOf(deptId) : baseUserMapper.selectByDeptCode(objects.get("部门机构代码").toString(), user.getCompanyId()));
                }
                if (ObjectUtil.isNotNull(objects.get("备注"))) {
                    user.setUserDesc(objects.get("备注").toString());
                }
                user.setOrganizationId(OpenHelper.getOrganizationId());
                if (getUserByUsername(user.getUserName()) != null) {
                    errMsg.append("账号:" + user.getUserName() + "已注册");
                }
                if (StrUtil.isEmpty(deptId)) {
                   /* Map map = baseUserMapper.selectDeptByDeptCode(objects.get(4).toString(), user.getCompanyId());
                   if(CollUtil.isEmpty( map)){
                       errMsg.append("部门机构代码:" +objects.get(4).toString() + "不存在");
                   }*/
                    if (ObjectUtil.isNotNull(objects.get("部门机构代码"))) {
                        Long l = baseUserMapper.selectByDeptCode(objects.get("部门机构代码").toString(), user.getCompanyId());
                        if (l == null || l < 1) {
                            errMsg.append("部门机构代码:" + objects.get("部门机构代码").toString() + "不存在");
                        }
                    } else {
                        errMsg.append("部门机构代码为空");
                    }

                }

                if (StrUtil.isEmpty(deptId) && baseUserMapper.selectByDeptId(user.getDeptId()) == 0) {
                    //errMsg.append("部门机构代码:" + user.getDeptId() + "不存在");
                }
                if (StrUtil.isNotEmpty(deptId)) {
                    /*String s = baseUserMapper.selectDeptCodeByDeptId(Long.valueOf(deptId));
                    if (!(objects.get("部门机构代码") + "").equals(s)) {

                        errMsg.append("部门机构代码:" + objects.get("部门机构代码") + "与选择的" + s + "不匹配");
                    }*/
                    String id = baseUserMapper.selectCompanyIdByDeptId(Long.valueOf(deptId));
                    if (!id.equals(baseUserMapper.selectByCompanyNo(objects.get("机构全宗号").toString()))) {
                        errMsg.append("机构全宗号:" + objects.get("机构全宗号") + "与选择的单位不匹配");
                    }

                }
                if (StrUtil.isEmpty(errMsg.toString())) {
                    userList.add(user);
                    BaseAccount baseAccount = new BaseAccount(user.getUserId(), user.getUserName(), user.getPassword(), BaseConstants.USER_ACCOUNT_TYPE_USERNAME, BaseConstants.ACCOUNT_DOMAIN_ADMIN, null);
                    baseAccount.setStatus(CommonConstants.ENABLED);
                    accountList.add(baseAccount);
                } else {
                    user.setErrMsg(errMsg.toString());
                    errUserList.add(user);
                }
                recordLogsList.add(BaseUserImportRecordLogs.builder().importId(importRecord.getImportId()).userId(user.getUserId()).errMsg(errMsg.toString()
                ).nickName(user.getNickName()).userName(user.getUserName()).password(ObjectUtil.isNotNull(objects.get("密码")) ? objects.get("密码").toString() : "").companyNo(objects.get("机构全宗号") + "").companyId(user.getCompanyId()).deptCode(objects.get("部门机构代码") + "").deptId(user.getDeptId()).userDesc(user.getUserDesc()).fileName(file.getOriginalFilename()).deleted(CommonConstants.DEL_NO).build());
            }

            saveBatch(userList);
            baseAccountService.saveBatch(accountList);

            for (BaseUser baseUser : userList) {
                this.saveCompany(baseUser.getUserId(), baseUser.getCompanyId().toString());
                this.saveDept(baseUser.getUserId(), baseUser.getDeptId().toString());
                EntityMap map = new EntityMap();
                map.put("userName", FlymeUtils.isNotEmpty(baseUser.getAccount()) ? baseUser.getAccount() : baseUser.getUserName());
                map.put("password", baseUser.getPassword());
                map.put("optType", "add");
            }
            importRecord.setFailNum(errUserList.size());
            importRecord.setSuccessNum(userList.size());
            baseUserImportRecordService.save(importRecord);
            baseUserImportRecordLogsService.saveBatch(recordLogsList);

            return importRecord;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new BaseUserImportRecord();
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<BaseUser> find(Map params) {
        QueryWrapper<BaseUser> cq = new QueryWrapper();
        cq.like("userName", MapUtil.getStr(params, "str")).or().like("nickName", MapUtil.getStr(params, "str"));
        List<BaseUser> list = baseUserMapper.selectList(cq);
        return list;
    }

    @Override
    public ResultBody batchRemove(String userIdss) {

        List<String> split = StrUtil.split(userIdss, ',');
        Long[] userIds = new Long[split.size()];
        for (int i = 0; i < split.size(); i++) {
            userIds[i] = Long.parseLong(split.get(i));
        }
        if (FlymeUtils.contains(userIds, OpenHelper.getUserId())) {
            return ResultBody.failed("禁止删除当前用户");
        }
        try {
            for (Long id : userIds) {
                BaseUser user = getUserById(id);
                //发送删除minio账户通知
                EntityMap map = new EntityMap();
                map.put("userId", user.getUserId());
                map.put("userName", user.getUserName());
                map.put("optType", "delete");
                flymeEventClient.publishEvent("minioAddUserListener", map);
                removeById(id);

            }
        } catch (Exception e) {
        }


        if (FlymeUtils.isNotEmpty(userIds)) {
            baseRoleService.removeUserRoles(userIds);
            baseAccountService.removeAccount(userIds);
        }

        return ResultBody.ok();
    }


    @Override
    public void batchSaveDeptAndCompany(String userIds, String deptIds, String companyIds) {
        for (String s : StrUtil.split(userIds, ',')) {
            Long userId = Long.valueOf(s);
            saveDept(userId, deptIds);
            saveCompany(userId, companyIds);
        }
    }

    @Override
    public List<BaseUser> getUserByCompany(Long companyId) {
        return baseUserMapper.selectList(new QueryWrapper<BaseUser>().eq("companyId", companyId));
    }

    @Override
    public List<BaseUser> getUserByDept(Long deptId) {
        return baseUserMapper.selectList(new QueryWrapper<BaseUser>().eq("deptId", deptId));
    }
}
