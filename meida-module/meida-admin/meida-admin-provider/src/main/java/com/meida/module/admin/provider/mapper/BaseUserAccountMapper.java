package com.meida.module.admin.provider.mapper;


import com.meida.module.admin.client.entity.BaseAccount;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseUserAccountMapper extends SuperMapper<BaseAccount> {
}
