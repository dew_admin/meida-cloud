package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayRateLimitApi;
import com.meida.module.admin.client.model.RateLimitApi;
import com.meida.common.mybatis.base.mapper.SuperMapper;


import java.util.List;

/**
 * @author zyf
 */
public interface GatewayRateLimitApisMapper extends SuperMapper<GatewayRateLimitApi> {

    List<RateLimitApi> selectRateLimitApi();

}
