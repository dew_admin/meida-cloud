package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayAccessLogs;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface GatewayLogsMapper extends SuperMapper<GatewayAccessLogs> {
}
