package com.meida.module.admin.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meida.module.admin.client.entity.BaseAuthorityApp;
import com.meida.common.security.OpenAuthority;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseAuthorityAppMapper extends BaseMapper<BaseAuthorityApp> {

    /**
     * 获取应用已授权权限
     *
     * @param appId
     * @return
     */
    List<OpenAuthority> selectAuthorityByApp(@Param("appId") String appId);
}
