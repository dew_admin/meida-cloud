package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.BaseAuthorityAction;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseAuthorityActionMapper extends SuperMapper<BaseAuthorityAction> {

}
