package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.model.AuthorityMenu;
import com.meida.module.admin.client.entity.BaseAuthorityUser;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.security.OpenAuthority;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseAuthorityUserMapper extends SuperMapper<BaseAuthorityUser> {

    /**
     * 获取用户已授权权限
     *
     * @param userId
     * @return
     */
    List<OpenAuthority> selectAuthorityByUser(@Param("userId") Long userId);

    /**
     * 获取用户已授权权限完整信息
     *
     * @param userId
     * @return
     */
    List<AuthorityMenu> selectAuthorityMenuByUser(@Param("userId") Long userId,@Param("menuGroup") Integer menuGroup);
}
