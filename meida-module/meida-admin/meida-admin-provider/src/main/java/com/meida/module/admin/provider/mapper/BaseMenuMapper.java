package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.BaseMenu;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseMenuMapper extends SuperMapper<BaseMenu> {
}
