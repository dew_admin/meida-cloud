package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthApprovals;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2已授权客户端 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthApprovalsMapper extends SuperMapper<OauthApprovals> {

}
