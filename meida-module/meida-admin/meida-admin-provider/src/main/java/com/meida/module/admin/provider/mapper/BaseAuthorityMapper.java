package com.meida.module.admin.provider.mapper;


import com.meida.module.admin.client.model.AuthorityAction;
import com.meida.module.admin.client.model.AuthorityApi;
import com.meida.module.admin.client.model.AuthorityMenu;
import com.meida.common.base.module.AuthorityResource;
import com.meida.module.admin.client.entity.BaseAuthority;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.security.OpenAuthority;


import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */

public interface BaseAuthorityMapper extends SuperMapper<BaseAuthority> {

    /**
     * 查询所有资源授权列表
     * @return
     */
    List<AuthorityResource> selectAllAuthorityResource();

    /**
     * 查询已授权权限列表
     *
     * @param map
     * @return
     */
    List<OpenAuthority> selectAuthorityAll(Map map);


    /**
     * 获取菜单权限
     *
     * @param map
     * @return
     */
    List<AuthorityMenu> selectAuthorityMenu(Map map);

    /**
     * 获取操作权限
     *
     * @param map
     * @return
     */
    List<AuthorityAction> selectAuthorityAction(Map map);

    /**
     * 获取API权限
     *
     * @param map
     * @return
     */
    List<AuthorityApi> selectAuthorityApi(Map map);


}
