package com.meida.module.admin.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.admin.client.entity.BaseApi;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class BaseApiInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return BaseApi.class;
    }
}
