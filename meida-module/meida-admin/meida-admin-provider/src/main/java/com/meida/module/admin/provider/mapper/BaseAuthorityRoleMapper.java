package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.model.AuthorityMenu;
import com.meida.module.admin.client.entity.BaseAuthorityRole;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.common.security.OpenAuthority;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseAuthorityRoleMapper extends SuperMapper<BaseAuthorityRole> {

    /**
     * 获取角色已授权权限
     *
     * @param roleId
     * @return
     */
    List<OpenAuthority> selectAuthorityByRole(@Param("roleId") Long roleId);

    /**
     * 获取角色菜单权限
     *
     * @param roleId
     * @return
     */
    List<AuthorityMenu> selectAuthorityMenuByRole(@Param("roleId") Long roleId,@Param("menuGroup") Integer menuGroup);
}
