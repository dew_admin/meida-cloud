package com.meida.module.admin.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.admin.client.entity.GatewayRateLimit;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class GateWayRateLimitInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return GatewayRateLimit.class;
    }
}
