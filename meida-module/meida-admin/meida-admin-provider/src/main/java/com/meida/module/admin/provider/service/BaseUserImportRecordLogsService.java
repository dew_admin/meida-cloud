package com.meida.module.admin.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.admin.client.entity.BaseUserImportRecordLogs;

/**
 * 用户导入日志记录 接口
 *
 * @author flyme
 * @date 2023-06-29
 */
public interface BaseUserImportRecordLogsService extends IBaseService<BaseUserImportRecordLogs> {

}
