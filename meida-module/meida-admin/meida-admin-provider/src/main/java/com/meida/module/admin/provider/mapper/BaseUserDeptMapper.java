package com.meida.module.admin.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseUserDept;


import java.util.List;

/**
 * @author zyf
 */

public interface BaseUserDeptMapper extends SuperMapper<BaseUserDept> {
    /**
     * 查询用户已分配部门
     * @param userId
     * @return
     */
    List<Long> selectUserDeptIds(Long userId);
}
