package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.BaseRole;
import com.meida.common.mybatis.base.mapper.SuperMapper;


import java.util.List;
import java.util.Map;

/**
 * @author zyf
 */

public interface BaseRoleMapper extends SuperMapper<BaseRole> {

    List<BaseRole> selectRoleList(Map params);

    List<BaseRole> getRolesByGroupQueryImpl(Map params);
}
