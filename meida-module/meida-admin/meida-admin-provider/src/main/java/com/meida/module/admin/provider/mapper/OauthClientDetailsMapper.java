package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthClientDetails;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2客户端信息 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthClientDetailsMapper extends SuperMapper<OauthClientDetails> {

}
