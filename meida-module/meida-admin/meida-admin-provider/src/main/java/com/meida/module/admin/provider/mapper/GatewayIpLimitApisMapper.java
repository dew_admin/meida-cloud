package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.GatewayIpLimitApi;
import com.meida.module.admin.client.model.IpLimitApi;
import com.meida.common.mybatis.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author zyf
 */

public interface GatewayIpLimitApisMapper extends SuperMapper<GatewayIpLimitApi> {

    List<IpLimitApi> selectIpLimitApi(@Param("policyType") int policyType);
}
