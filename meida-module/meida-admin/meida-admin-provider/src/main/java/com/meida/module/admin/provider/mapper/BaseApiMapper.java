package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.BaseApi;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseApiMapper extends SuperMapper<BaseApi> {
}
