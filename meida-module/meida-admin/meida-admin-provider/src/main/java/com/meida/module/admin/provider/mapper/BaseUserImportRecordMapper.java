package com.meida.module.admin.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.admin.client.entity.BaseUserImportRecord;

/**
 * 用户导入 Mapper 接口
 *
 * @author flyme
 * @date 2023-06-29
 */
public interface BaseUserImportRecordMapper extends SuperMapper<BaseUserImportRecord> {

}
