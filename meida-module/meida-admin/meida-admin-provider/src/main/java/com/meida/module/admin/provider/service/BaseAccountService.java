package com.meida.module.admin.provider.service;

import com.meida.common.base.service.BaseAdminAccountService;
import com.meida.common.base.service.BaseThirdLoginService;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.entity.BaseAccount;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import com.meida.module.admin.client.model.UserAccount;

import java.util.List;

/**
 * 系统用户登录账号管理
 * 支持多账号登陆
 *
 * @author zyf
 */
public interface BaseAccountService extends IBaseService<BaseAccount> , BaseAdminAccountService, BaseThirdLoginService {



    /**
     * 注册账号
     *
     * @param userId
     * @param account
     * @param password
     * @param accountType
     * @param status
     * @param domain
     * @param registerIp
     * @return
     */
    BaseAccount register(Long userId, String account, String password, String accountType, Integer status, String domain, String registerIp);

    /**
     * 绑定账户
     */
     ResultBody bindAccount(Long userId, String accountName, String accountType, String nickName, String avatar);


    /**
     * 注册账号
     *
     * @param accountName
     * @return
     */
    BaseAccount registerByMobile(String accountName, String password);



    /**
     * 绑定系统用户名账户
     *
     * @param userId
     * @param username
     * @param password
     * @return
     */
    BaseAccount registerUsernameAccount(Long userId, String username, String password);

    /**
     * 绑定email账号
     *
     * @param email
     * @param userId
     * @param password
     * @return
     */
    BaseAccount registerEmailAccount(Long userId, String email, String password);


    /**
     * 绑定手机账号
     *
     * @param userId
     * @param password
     * @param mobile
     * @return
     */
    BaseAccount registerMobileAccount(Long userId, String mobile, String password);

    /**
     * 支持密码、手机号、email登陆
     * 其他方式没有规则，无法自动识别。需要单独开发

     * @return
     */
    UserAccount login(String username);




    /**
     * 重置用户密码
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    void resetPassword(Long userId, String oldPassword, String newPassword);

    /**
     * 重置用户密码
     *
     * @param userId
     * @param password
     */
    void resetPassword(Long userId, String password);

    /**
     * 根据验证码重置用户密码
     *
     * @param accountId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    void resetPwdByOldPwd(Long accountId, String oldPassword, String newPassword);

    /**
     * 添加登录日志
     *
     * @param log
     */
    void addLoginLog(BaseUserAccountLogs log);
    /**
     * 校验账户是否存在
     *
     * @param accountName
     */
    Boolean checkByAccountName(String accountName);

    /**
     * 检查账号是否存在
     *
     * @param userId
     * @param account
     * @param accountType
     * @return
     */
    Boolean isExist(Long userId, String account, String accountType);

    /**
     * 检查账号是否存在
     *
     * @param account
     * @param accountType
     * @param domain
     * @return
     */
    Boolean isExist(String account, String accountType, String domain);

    /**
     * 检查账号是否存在
     *
     * @param account
     * @param accountType
     * @return
     */
    Boolean isExist(String account, String accountType);

    /**
     * 检查账户类型是否存在
     *
     * @param userId
     * @param accountType
     * @return
     */
    Boolean isExist(Long userId,String accountType);

    /**
     * 解绑email账号
     *
     * @param email
     * @param userId
     * @return
     */
    void removeEmailAccount(Long userId, String email);

    /**
     * 解绑手机账号
     *
     * @param userId
     * @param mobile
     * @return
     */
    void removeMobileAccount(Long userId, String mobile);

    /**
     * 删除账户
     * @param userIds
     */
    void removeAccount(Long[] userIds);

    /**
     * 删除账户
     * @param userId
     */
    void removeAccount(Long userId);

    /**
     * 查询账户
     * @param accountName
     * @return
     */
    BaseAccount getAccount(String accountName);

    /**
     * 查询账户
     * @param userId
     * @return
     */
    List<BaseAccount> selectByUserId(Long userId);


    void batchResetPassword(String userIds, String password);
}
