package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.BaseAction;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */

public interface BaseActionMapper extends SuperMapper<BaseAction> {

}
