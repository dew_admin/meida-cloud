package com.meida.module.admin.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.admin.client.entity.BaseAuthorityRole;
import com.meida.module.admin.client.entity.BaseAuthorityUser;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class BaseAuthorityUserInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return BaseAuthorityUser.class;
    }
}
