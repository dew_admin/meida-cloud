package com.meida.module.admin.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.admin.client.entity.BaseUserAccountLogs;
import com.meida.module.admin.provider.mapper.BaseUserAccountLogsMapper;
import com.meida.module.admin.provider.service.BaseUserAccountLogsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: zyf
 * @date: 2018/10/24 16:33
 * @description:
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseUserAccountLogServiceImpl extends BaseServiceImpl<BaseUserAccountLogsMapper, BaseUserAccountLogs> implements BaseUserAccountLogsService {

}
