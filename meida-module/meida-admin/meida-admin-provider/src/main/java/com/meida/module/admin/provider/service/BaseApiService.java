package com.meida.module.admin.provider.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.module.admin.client.entity.BaseApi;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 接口资源管理
 *
 * @author zyf
 */
public interface BaseApiService extends IBaseService<BaseApi> {


    /**
     * 查询列表
     *
     * @return
     */
    List<BaseApi> findAllList(String serviceId);

    /**
     * 根据主键获取接口
     *
     * @param apiId
     * @return
     */
    BaseApi getApi(Long apiId);


    /**
     * 检查接口编码是否存在
     *
     * @param apiCode
     * @return
     */
    Boolean isExist(String apiCode);

    /**
     * 添加接口
     *
     * @param api
     * @return
     */
    void addApi(BaseApi api);

    /**
     * 修改接口
     *
     * @param api
     * @return
     */
    void updateApi(BaseApi api);

    /**
     * 查询接口
     *
     * @param apiCode
     * @return
     */
    BaseApi getApi(String apiCode);

    /**
     * 移除接口
     *
     * @param apiId
     * @return
     */
    void removeApi(Long apiId);


    /**
     * 获取数量
     *
     * @param queryWrapper
     * @return
     */
    Long getCount(QueryWrapper<BaseApi> queryWrapper);

    /**
     * 根据serviceId、path查询
     * @param serviceId
     * @param path
     * @return
     */
    List<BaseApi>  findByServiceIdAndPath(String serviceId,String path);

}
