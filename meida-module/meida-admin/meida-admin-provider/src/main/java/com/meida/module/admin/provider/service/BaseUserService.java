package com.meida.module.admin.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.client.entity.BaseUserImportRecord;
import com.meida.module.admin.client.model.UserInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 系统用户资料管理
 *
 * @author: zyf
 * @date: 2018/10/24 16:38
 * @description:
 */
public interface BaseUserService extends IBaseService<BaseUser> {


    /**
     * 查询列表
     *
     * @return
     */
    List<BaseUser> findAllList();

    /**
     * 查询列表
     *
     * @return
     */
    List<BaseUser> findByUserType(Integer userType);

    List<BaseUser> listByUserIds(List ids);

    List<BaseUser> findByUserIds(String ids);


    /**
     * 根据用户ID获取用户信息
     *
     * @param userId
     * @return
     */
    BaseUser getUserById(Long userId);


    /**
     * 根据用户ID获取用户信息和权限
     *
     * @param userId
     * @return
     */
    UserInfo getUserWithAuthoritiesById(Long userId);


    /**
     * 依据登录名查询系统用户信息
     *
     * @param username
     * @return
     */
    BaseUser getUserByUsername(String username);

    /**
     * 设置用户状态
     *
     * @param userId
     * @return
     */
    ResultBody setStatus(Long userId);


    /**
     * 设置在线状态
     *
     * @param userId
     * @param onLine
     * @return
     */
     ResultBody setOnLine(Long userId,Integer onLine);

    /**
     * 统计在线数量
     * @return
     */
     Long countOnLine();

    /**
     * 删除用户企业
     *
     * @param userId
     */
    void removeUserCompnay(Long userId);


    /**
     * 移除用户token
     *
     * @param accountName
     * @return
     */
    ResultBody removeToken(String accountName);



    /**
     * 解除用户锁定
     *
     * @param userId
     * @return
     */
    ResultBody unLock(Long userId);

    /**
     * 删除用户
     *
     * @param accountName
     * @return
     */
    ResultBody deleteUser(String accountName);

    /**
     * 用户分配企业
     *
     * @param userId
     * @param companyIds
     */
    void saveUserCompanys(Long userId, String... companyIds);

    /**
     * 用户分配部门
     *
     * @param userId
     * @param deptIds
     */
    void saveUserDepts(Long userId, String... deptIds);

    /**
     * 获取部门分配列表(穿梭框)数据
     *
     * @param userId
     * @param organizationId
     * @return
     */
    ResultBody getAuthDeptList(Long userId, Long organizationId);


    /**
     * 设置用户角色
     *
     * @param userId
     * @param roleCode
     */
    void saveUserRole(Long userId, String roleCode);


    /**
     * 保存用户机构
     *
     * @param userId
     * @param companyIds
     * @return
     */
    Boolean saveCompany(Long userId,String companyIds);

    /**
     * 保存用户部门
     *
     * @param userId
     * @param deptIds
     * @return
     */
    Boolean saveDept(Long userId,String deptIds);

    /**
     * 保存用户部门 和机构
     *
     * @param userId
     * @param deptIds
     * @param companyIds
     * @return
     */
    Boolean saveDeptAndCompany(Long userId,String deptIds,String companyIds);

    /**
     * 查询用户已分配的企业IDS
     * @param userId
     * @return
     */
    List<Long> selectCompanyIdsByUserId(Long userId);

    /**
     * 查询用户已分配的部门IDS
     *
     * @param userId
     * @return
     */
    List<Long> selectDeptIdsByUserId(Long userId, Long companyId);

    /**
     * description: 批量导入账号
     * date: 2023年-06月-27日 17:54
     * author: ldd
     *
     * @param deptId
     * @param file
     * @return void
     */
    BaseUserImportRecord importFile(String deptId, MultipartFile file);

    //用户查找
    List<BaseUser> find(Map params);

    //批量删除用户
    ResultBody batchRemove(String userIds);

    //批量授权部门
    void batchSaveDeptAndCompany(String userIds, String deptIds, String companyIds);

    //获取全宗下人员
    List<BaseUser> getUserByCompany(Long companyId);

    //获取部门下人员
    List<BaseUser> getUserByDept(Long deptId);
}
