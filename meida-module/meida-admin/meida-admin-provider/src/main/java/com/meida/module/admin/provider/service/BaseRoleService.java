package com.meida.module.admin.provider.service;

import com.meida.common.base.service.BaseSysRoleService;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.entity.BaseRole;
import com.meida.module.admin.client.entity.BaseRoleUser;

import java.util.List;
import java.util.Map;

/**
 * 角色管理
 *
 * @author zyf
 */
public interface BaseRoleService extends IBaseService<BaseRole>, BaseSysRoleService {


    /**
     * 查询列表
     *
     * @return
     */
    List<BaseRole> findAllList();


    /**
     * 查询企业下角色
     *
     * @return
     */
    List<BaseRole> findByCompanyId(Long companyId);


    /**
     * 根据角色类型查询
     *
     * @param roleType
     * @return
     */
    List<BaseRole> selectByType(String roleType);

    /**
     * 不包含指定角色
     * @param roleType
     * @return
     */
    List<BaseRole> listByNotInType(String roleType);

    /**
     * 获取角色信息
     *
     * @param roleId
     * @return
     */
    BaseRole getRole(Long roleId);


    /**
     * 更新角色
     *
     * @param role 角色
     * @return
     */
    BaseRole updateRole(BaseRole role);

    /**
     * 删除角色
     *
     * @param params 角色ID
     * @return
     */
    void removeRole(Map params);

    /**
     * 设置状态
     *
     * @param userId
     * @return
     */
    ResultBody setStatus(Long roleId);

    /**
     * 检测角色编码是否存在
     *
     * @param roleCode
     * @return
     */
    Boolean isExist(String roleCode);

    /**
     * 用户添加角色
     *
     * @param userId
     * @param roles
     * @return
     */
    void saveUserRoles(Long userId, String... roles);


    /**
     * 查询角色
     *
     * @param roleCode
     * @return
     */
    BaseRole getRoleByRoleCode(String roleCode);


    /**
     * 角色添加成员
     *
     * @param roleId
     * @param userIds
     */
    void saveRoleUsers(Long roleId, String... userIds);

    /**
     * 查询角色成员
     *
     * @return
     */
    List<BaseRoleUser> findRoleUsers(Long roleId);

    /**
     * 获取角色所有授权组员数量
     *
     * @param roleId
     * @return
     */
    Long getCountByRole(Long roleId);

    /**
     * 获取组员角色数量
     *
     * @param userId
     * @return
     */
    Long getCountByUser(Long userId);

    /**
     * 移除角色所有组员
     *
     * @param roleId
     * @return
     */
    void removeRoleUsers(Long roleId);

    /**
     * 移除组员的所有角色
     *
     * @param userId
     * @return
     */
    void removeUserRoles(Long userId);

    /**
     * 移除组员的所有角色
     *
     * @param userId
     * @return
     */
    void removeUserRoles(Long[] userIds);

    /**
     * 检测是否存在
     *
     * @param userId
     * @param roleId
     * @return
     */
    Boolean isExist(Long userId, Long roleId);

    /**
     * 获取用户角色列表
     *
     * @param userId
     * @return
     */
    List<BaseRole> getUserRoles(Long userId);


    /**
     * 获取用户角色编码
     *
     * @param userId
     * @return
     */
    List<String> selectRoleCodesByUserId(Long userId);


    /**
     * 获取用户角色名称
     */
    List<String> selectRoleNamesByUserId(Long userId);

    /**
     * 根据Flowable GroupQueryImpl查询岗位列表
     *
     * @param params
     * @return
     */
    List<BaseRole> getRoleByFlowableGroupQueryImpl(Map params);

    /**
     * 查询用户分配角色列表
     *
     * @param userId
     * @param organizationId
     * @param roleType
     * @return
     */
    ResultBody getAuthRoleList(Long userId, Long organizationId,Integer roleType);

    //批量分配用户角色
    void updateUserRoles(String userIds, String[] strings);

    List<String> selectUserIdByRoleCode(String[] roleCode);
}
