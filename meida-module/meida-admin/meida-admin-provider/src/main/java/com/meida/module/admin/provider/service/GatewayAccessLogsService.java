package com.meida.module.admin.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meida.module.admin.client.entity.GatewayAccessLogs;
import com.meida.common.mybatis.model.PageParams;

/**
 * 网关访问日志
 * @author zyf
 */
public interface GatewayAccessLogsService {
    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<GatewayAccessLogs> findListPage(PageParams pageParams);
}
