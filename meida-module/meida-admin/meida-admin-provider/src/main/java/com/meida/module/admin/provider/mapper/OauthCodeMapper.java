package com.meida.module.admin.provider.mapper;

import com.meida.module.admin.client.entity.OauthCode;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * oauth2授权码 Mapper 接口
 * @author flyme
 * @date 2020-06-04
 */
public interface OauthCodeMapper extends SuperMapper<OauthCode> {

}
