/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 16:50:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`  (
  `roleId` bigint(20) NOT NULL COMMENT '角色ID',
  `roleCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编码',
  `roleName` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色名称',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `roleType` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色类型',
  `roleDesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色描述',
  `createTime` datetime(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  `isPersist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  PRIMARY KEY (`roleId`) USING BTREE,
  UNIQUE INDEX `role_code`(`roleCode`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统角色-基础信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES (1, 'superadmin', '系统管理员', 1, '0', '系统管理员', '2018-07-29 21:14:54', '2019-12-22 15:09:15', 1);
INSERT INTO `base_role` VALUES (2, 'admin', '平台管理员', 1, '0', '运营人员', '2018-07-29 21:14:54', '2020-01-14 11:46:58', 1);


SET FOREIGN_KEY_CHECKS = 1;
