/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:26:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_authority
-- ----------------------------
DROP TABLE IF EXISTS `base_authority`;
CREATE TABLE `base_authority`  (
  `authorityId` bigint(20) NOT NULL,
  `authority` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限标识',
  `menuId` bigint(20) NULL DEFAULT NULL COMMENT '菜单资源ID',
  `apiId` bigint(20) NULL DEFAULT NULL COMMENT 'API资源ID',
  `actionId` bigint(20) NULL DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`authorityId`) USING BTREE,
  INDEX `menu_id`(`menuId`) USING BTREE,
  INDEX `api_id`(`apiId`) USING BTREE,
  INDEX `action_id`(`actionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-菜单权限、操作权限、API权限' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_authority
-- ----------------------------
INSERT INTO `base_authority` VALUES (1, 'MENU_system', 1, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-04 17:59:25');
INSERT INTO `base_authority` VALUES (3, 'MENU_systemMenu', 3, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-04 17:29:17');
INSERT INTO `base_authority` VALUES (6, 'MENU_systemApi', 6, NULL, NULL, 1, '2019-07-30 15:43:15', '2019-07-30 15:43:15');
INSERT INTO `base_authority` VALUES (8, 'MENU_systemRole', 8, NULL, NULL, 1, '2019-07-30 15:43:15', '2019-07-30 15:43:15');
INSERT INTO `base_authority` VALUES (10, 'MENU_systemUser', 10, NULL, NULL, 1, '2019-07-30 15:43:15', '2019-11-10 15:51:08');
INSERT INTO `base_authority` VALUES (13, 'MENU_gateway', 13, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-04 17:59:18');
INSERT INTO `base_authority` VALUES (20, 'MENU_systemDict', 20, NULL, NULL, 1, NULL, NULL);
INSERT INTO `base_authority` VALUES (105, 'MENU_config', 105, NULL, NULL, 1, '2019-07-30 15:43:15', '2019-07-30 15:43:15');

INSERT INTO `base_authority` VALUES (199, 'MENU_generate', 199, NULL, NULL, 1, '2020-05-20 18:03:28', NULL);


INSERT INTO `base_authority` VALUES (99, 'API_all', NULL, 1, NULL, 1, '2019-07-30 15:43:15', '2019-07-30 15:43:15');
INSERT INTO `base_authority` VALUES (100, 'API_actuator', NULL, 2, NULL, 1, '2019-07-30 15:43:15', '2019-07-30 15:43:15');

SET FOREIGN_KEY_CHECKS = 1;
