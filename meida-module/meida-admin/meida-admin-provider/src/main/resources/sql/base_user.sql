/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 06/08/2021 14:45:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `userId` bigint(20) NOT NULL COMMENT '用户ID',
  `userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登陆账号',
  `nickName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像',
  `userNo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户编号',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `userType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'platform' COMMENT '用户类型:platform-平台 isp-服务提供商 dev-自研开发者',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `organizationId` bigint(20) NULL DEFAULT NULL COMMENT '机构ID',
  `registerIp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册IP',
  `status` int(11) NULL DEFAULT 1 COMMENT '状态:0-禁用 1-启用 2-锁定',
  `userDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '描述',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`userId`) USING BTREE,
  UNIQUE INDEX `user_name`(`userName`) USING BTREE,
  INDEX `user_id`(`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户-基础信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES (521677655146233856, 'superadmin', '超级管理员', '','001', '515608851@qq.com', '18518226890', 'super', 1, NULL, NULL, '2018-12-10 13:20:45', 1, '2222222222', '2018-12-10 13:20:45', '2019-12-22 16:39:45');
INSERT INTO `base_user` VALUES (557063237640650752, 'admin', '平台管理员', '','002', '156924558@qq.com', '13000000006', 'normal', 1, NULL, NULL, '2019-03-18 04:50:25', 1, '', '2019-03-18 04:50:25', '2019-12-22 16:42:37');

SET FOREIGN_KEY_CHECKS = 1;
