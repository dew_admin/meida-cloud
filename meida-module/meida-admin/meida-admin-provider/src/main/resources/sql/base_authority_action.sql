/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:34:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_authority_action
-- ----------------------------
DROP TABLE IF EXISTS `base_authority_action`;
CREATE TABLE `base_authority_action`  (
  `actionAuthorityId` bigint(20) NOT NULL,
  `actionId` bigint(20) NULL DEFAULT NULL COMMENT '操作ID',
  `authorityId` bigint(20) NULL DEFAULT NULL COMMENT 'API',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`actionAuthorityId`) USING BTREE,
  INDEX `action_id`(`actionId`) USING BTREE,
  INDEX `authority_id`(`authorityId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-功能操作关联表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
