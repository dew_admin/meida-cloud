/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:35:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_authority_role
-- ----------------------------
DROP TABLE IF EXISTS `base_authority_role`;
CREATE TABLE `base_authority_role`  (
  `authorityRoleId` bigint(20) NULL DEFAULT NULL COMMENT '主键',
  `authorityId` bigint(20) NULL DEFAULT NULL COMMENT '权限ID',
  `roleId` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `expireTime` datetime(0) NULL DEFAULT NULL COMMENT '过期时间:null表示长期',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  INDEX `authority_id`(`authorityId`) USING BTREE,
  INDEX `role_id`(`roleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-角色关联' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_authority_role
-- ----------------------------


SET FOREIGN_KEY_CHECKS = 1;
