/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 16:45:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gateway_ip_limit_api
-- ----------------------------
DROP TABLE IF EXISTS `gateway_ip_limit_api`;
CREATE TABLE `gateway_ip_limit_api`  (
  `policyId` bigint(20) NOT NULL COMMENT '策略ID',
  `apiId` bigint(20) NOT NULL COMMENT '接口资源ID',
  INDEX `policy_id`(`policyId`) USING BTREE,
  INDEX `api_id`(`apiId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '开放网关-IP访问控制-API接口' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
