/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 04/06/2020 15:35:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `client_secret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorities` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'oauth2客户端信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('1552294656514', '$2a$10$UAzdXTnT9DAyfzSNInoX4.bt/8V0zdn23m7uQiwsyorHLucf4ftfO', '', 'userProfile', 'authorization_code,client_credentials,password', 'http://localhost:8888/oauth/admin/callback', '', -1, 2592000, '{\"appIcon\":\"\",\"website\":\"http://www.baidu.com\",\"appName\":\"运营后台\",\"appType\":\"pc\",\"appDesc\":\"运营后台\",\"appId\":\"1552294656514\",\"appNameEn\":\"Admin\",\"updateTime\":1553011144275,\"userType\":\"platform\",\"userId\":521677655146233856,\"appOs\":\"\",\"status\":1}', '');
INSERT INTO `oauth_client_details` VALUES ('7gBZcbsC7kLIWCdELIl8nxcs', '$2a$10$4di0sSQdr9yk4uTKWtZqzedsxI8sWXoR67x.G.Qmy4K2L7ZaFQt6W', '', 'userProfile', 'authorization_code,client_credentials,password', 'http://localhost:8888/login,http://localhost:8888/webjars/springfox-swagger-ui/o2c.html', '', -1, 2592000, '{\"appIcon\":\"\",\"website\":\"http://www.baidu.com\",\"appName\":\"资源服务器\",\"appType\":\"server\",\"appDesc\":\"资源服务器\",\"appId\":\"1552274783265\",\"appNameEn\":\"ResourceServer\",\"updateTime\":1553011137731,\"userType\":\"platform\",\"userId\":521677655146233856,\"appOs\":\"\",\"status\":1}', '');

SET FOREIGN_KEY_CHECKS = 1;
