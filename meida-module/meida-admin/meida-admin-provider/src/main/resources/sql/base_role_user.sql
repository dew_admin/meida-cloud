/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:35:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_role_user
-- ----------------------------
DROP TABLE IF EXISTS `base_role_user`;
CREATE TABLE `base_role_user`  (
  `userRoleId` bigint(20) NULL DEFAULT NULL COMMENT '主键',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `roleId` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  INDEX `fk_user`(`userId`) USING BTREE,
  INDEX `fk_role`(`roleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统角色-用户关联' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_role_user
-- ----------------------------
INSERT INTO `base_role_user` VALUES (1, 1, 1);
INSERT INTO `base_role_user` VALUES (2,2, 2);

SET FOREIGN_KEY_CHECKS = 1;
