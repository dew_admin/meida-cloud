/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:17:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu`  (
  `menuId` bigint(20) NOT NULL COMMENT '菜单Id',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父级菜单',
  `menuCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单编码',
  `menuName` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `menuDesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `scheme` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '路径前缀',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '菜单标题',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '_self' COMMENT '打开方式:_self窗口内,_blank新窗口',
  `menuLayout` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '布局页面',
  `hiddenHeaderContent` tinyint(1) NULL DEFAULT NULL COMMENT '是否隐藏页面顶部内容',
  `priority` bigint(20) NOT NULL COMMENT '优先级 越小越靠前',
  `status` tinyint(3) NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `isPersist` tinyint(3) NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  `serviceId` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '服务名',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`menuId`) USING BTREE,
  UNIQUE INDEX `menu_code`(`menuCode`) USING BTREE,
  INDEX `service_id`(`serviceId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-菜单信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES (1, 0, 'system', '系统管理', '系统管理', '/', '', 'setting', '_self', 'PageView', 1, 10, 1, 0, 'meida-base-provider', '2018-07-29 21:20:10', '2020-01-04 17:59:25');
INSERT INTO `base_menu` VALUES (3, 1, 'systemMenu', '功能菜单', '功能菜单资源', '/', 'system/menus/index', 'appstore', '_self', 'PageView', 1, 3, 1, 0, 'meida-base-provider', '2018-07-29 21:20:13', '2020-01-04 17:29:17');
INSERT INTO `base_menu` VALUES (8, 1, 'systemRole', '角色管理', '角色信息管理', '/', 'system/role/index', 'solution', '_self', 'PageView', 1, 8, 1, 0, 'meida-base-provider', '2018-12-27 15:26:54', '2019-07-11 18:03:10');
INSERT INTO `base_menu` VALUES (10, 1, 'systemUser', '系统用户', '系统用户', '/', 'system/user/index', 'code', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2019-11-10 15:51:08');
INSERT INTO `base_menu` VALUES (20, 1, 'systemDict', '数据字典', '数据字典', '/', 'system/dict/index', 'hdd', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2019-07-11 18:03:55');
INSERT INTO `base_menu` VALUES (105, 1, 'config', '系统配置', '系统配置', '/', 'system/config/index', 'setting', '_self', 'PageView', 1, 8, 1, 0, 'meida-base-provider', '2018-12-27 15:26:54', '2019-07-11 18:03:10');

INSERT INTO `base_menu` VALUES (199, 1, 'generate', '代码生成', '', 'http://', 'localhost:8888/base/generate/tables', 'bars', '_blank', 'PageView', NULL, 0, 1, 0, 'meida-base-provider', '2020-05-20 18:03:28', NULL);
INSERT INTO `base_menu` VALUES (13, 0, 'gateway', 'API网关', 'API网关', '/', '', 'tool', '_self', 'PageView', 1, 9, 1, 0, 'meida-base-provider', '2019-02-25 00:15:09', '2020-01-04 17:59:18');
INSERT INTO `base_menu` VALUES (6, 13, 'systemApi', 'API列表', 'API接口资源', '/', 'system/api/index', 'md-document', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2018-07-29 21:20:13', '2019-03-13 21:48:12');

SET FOREIGN_KEY_CHECKS = 1;
