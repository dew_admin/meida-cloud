/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 16:33:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_action
-- ----------------------------
DROP TABLE IF EXISTS `base_action`;
CREATE TABLE `base_action`  (
  `actionId` bigint(20) NOT NULL COMMENT '资源ID',
  `actionCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源编码',
  `actionName` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '资源名称',
  `actionDesc` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源描述',
  `menuId` bigint(20) NULL DEFAULT NULL COMMENT '资源父节点',
  `priority` int(10) NOT NULL DEFAULT 0 COMMENT '优先级 越小越靠前',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `createTime` datetime(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  `isPersist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  `serviceId` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务名称',
  PRIMARY KEY (`actionId`) USING BTREE,
  UNIQUE INDEX `action_code`(`actionCode`) USING BTREE,
  UNIQUE INDEX `action_id`(`actionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-功能操作' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
