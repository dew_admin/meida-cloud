/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 14:58:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_api
-- ----------------------------
DROP TABLE IF EXISTS `base_api`;
CREATE TABLE `base_api`  (
  `apiId` bigint(20) NOT NULL COMMENT '接口ID',
  `apiCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '接口编码',
  `apiName` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '接口名称',
  `apiCategory` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'default' COMMENT '接口分类:default-默认分类',
  `apiDesc` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '资源描述',
  `requestMethod` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求方式',
  `contentType` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '响应类型',
  `serviceId` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务ID',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求路径',
  `priority` bigint(20) NOT NULL COMMENT '优先级',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `createTime` datetime(0) NOT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `isPersist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '保留数据0-否 1-是 不允许删除',
  `isAuth` tinyint(3) NOT NULL DEFAULT 1 COMMENT '是否需要认证: 0-无认证 1-身份认证 默认:1',
  `isOpen` tinyint(3) NULL DEFAULT NULL COMMENT '是否公开: 0-内部的 1-公开的',
  `className` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '类名',
  `actionName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '按钮名称',
  `menuName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '菜单名称',
  `methodName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '方法名',
  `requestParamCfg` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求参数配置',
  `responseParamCfg` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '响应数据配置',
  `requestParam` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求参数',
  `responseParam` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '响应参数',
  PRIMARY KEY (`apiId`) USING BTREE,
  UNIQUE INDEX `api_id`(`apiId`) USING BTREE,
  UNIQUE INDEX `api_code`(`apiCode`) USING BTREE,
  INDEX `service_id`(`serviceId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统资源-API接口' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of base_api
-- ----------------------------
INSERT INTO `base_api` VALUES (1, 'all', '全部', 'default', '所有请求', 'get,post', NULL, 'meida-api-gateway-zuul', '/**', 0, 1, '2019-03-07 21:52:17', '2019-03-14 21:41:28', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_api` VALUES (2, 'actuator', '监控端点', 'default', '监控端点', 'post', NULL, 'meida-api-gateway-zuul', '/actuator/**', 0, 1, '2019-03-07 21:52:17', '2019-03-14 21:41:28', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
