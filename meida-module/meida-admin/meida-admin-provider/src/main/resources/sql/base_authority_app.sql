/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud2

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 15:34:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_authority_app
-- ----------------------------
DROP TABLE IF EXISTS `base_authority_app`;
CREATE TABLE `base_authority_app`  (
  `authorityAppId` bigint(20) NOT NULL,
  `authorityId` bigint(20) NOT NULL COMMENT '权限ID',
  `appId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用ID',
  `expireTime` datetime(0) NULL DEFAULT NULL COMMENT '过期时间:null表示长期',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`authorityAppId`) USING BTREE,
  INDEX `authority_id`(`authorityId`) USING BTREE,
  INDEX `app_id`(`appId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限-应用关联' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
