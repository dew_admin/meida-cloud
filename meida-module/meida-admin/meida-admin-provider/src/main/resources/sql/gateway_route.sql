/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 16:45:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gateway_route
-- ----------------------------
DROP TABLE IF EXISTS `gateway_route`;
CREATE TABLE `gateway_route`  (
  `routeId` bigint(20) NOT NULL COMMENT '路由ID',
  `routeTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `routeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路由名称',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `serviceId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务ID',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '完整地址',
  `routeDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由描述',
  `stripPrefix` tinyint(3) NOT NULL DEFAULT 1 COMMENT '忽略前缀',
  `retryable` tinyint(3) NOT NULL DEFAULT 0 COMMENT '0-不重试 1-重试',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态:0-无效 1-有效',
  `isPersist` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否为保留数据:0-否 1-是',
  `showApi` tinyint(3) NULL DEFAULT NULL COMMENT '是否在接口文档中展示',
  PRIMARY KEY (`routeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '开放网关-路由' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gateway_route
-- ----------------------------
INSERT INTO `gateway_route` VALUES (556587504019439616, '后台接口服务', 'meida-base-provider', '/base/**', 'meida-base-provider', '', NULL, 0, 0, 1, 1, 0);
INSERT INTO `gateway_route` VALUES (556595914240688125, 'APP接口服务', 'meida-app-provider', '/api/**', 'meida-app-provider', '', NULL, 0, 0, 1, 1, 1);
INSERT INTO `gateway_route` VALUES (556595914240688128, '消息服务', 'meida-msg-provider', '/msg/**', 'meida-msg-provider', '', NULL, 0, 0, 0, 1, 0);
INSERT INTO `gateway_route` VALUES (556595914240688139, '任务调度服务', 'meida-scheduler-provider', '/scheduler/**', 'meida-scheduler-provider', '', NULL, 0, 0, 0, 1, 0);
INSERT INTO `gateway_route` VALUES (556595914240688145, '工作流服务', 'meida-bpm-provider', '/bpm/**', 'meida-bpm-provider', '', NULL, 0, 0, 0, 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
