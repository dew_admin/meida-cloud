package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统权限-角色关联
 * @author zyf
 */
@TableName("base_authority_role")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BaseAuthorityRole extends AbstractEntity implements Serializable {

    @TableId(value = "authorityRoleId", type = IdType.ASSIGN_ID)
    private Long authorityRoleId;
    /**
     * 权限ID
     */
    private Long authorityId;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 过期时间:null表示长期
     */
    private Date expireTime;

    private static final long serialVersionUID = 1L;


}
