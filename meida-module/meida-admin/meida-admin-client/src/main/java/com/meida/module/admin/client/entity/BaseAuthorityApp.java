package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统权限-应用关联
 * @author zyf
 */
@TableName("base_authority_app")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BaseAuthorityApp extends AbstractEntity implements Serializable{

    @TableId(value = "authorityAppId", type = IdType.ASSIGN_ID)
    private Long authorityAppId;
    /**
     * 权限ID
     */
    private Long authorityId;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 过期时间:null表示长期
     */
    private Date expireTime;

    private static final long serialVersionUID = 1L;

}
