package com.meida.module.admin.client.model;


import com.meida.module.admin.client.entity.BaseApi;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zyf
 */
@Data
public class RateLimitApi extends BaseApi implements Serializable {
    private static final long serialVersionUID = 1212925216631391016L;
    private Long itemId;
    private Long policyId;
    private String policyName;
    private Long limitQuota;
    private String intervalUnit;
    private String url;


}