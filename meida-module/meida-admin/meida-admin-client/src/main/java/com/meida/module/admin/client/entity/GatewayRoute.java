package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 网关动态路由
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */
@Data
@TableName("gateway_route")
public class GatewayRoute implements Serializable {
    private static final long serialVersionUID = -2952097064941740301L;

    /**
     * 路由ID
     */
    @TableId(value = "routeId",type = IdType.ASSIGN_ID)
    private Long routeId;

    /**
     * 路由名称
     */
    private String routeName;

    /**
     * 路由描述
     */
    private  String routeTitle;

    /**
     * 路径
     */
    private String path;

    /**
     * 服务ID
     */
    private String serviceId;

    /**
     * 完整地址
     */
    private String url;

    /**
     * 忽略前缀
     */
    private Integer stripPrefix;

    /**
     * 0-不重试 1-重试
     */
    private Integer retryable;

    /**
     * 状态:0-无效 1-有效
     */
    private Integer status;

    /**
     * 保留数据0-否 1-是 不允许删除
     */
    private Integer isPersist;
    /**
     * 是否在接口文档中展示0-否 1-是
     */
    private Integer showApi;


    /**
     * 路由说明
     */
    private String routeDesc;
    
}
