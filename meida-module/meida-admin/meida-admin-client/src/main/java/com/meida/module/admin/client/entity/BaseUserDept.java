package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户-部门关联
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */
@TableName("base_user_dept")
@Data
@TableAlias("bud")
public class BaseUserDept implements Serializable {
    private static final long serialVersionUID = -667816444278087761L;

    @TableId(value = "userDeptId", type = IdType.ASSIGN_ID)
    private Long userDeptId;
    /**
     * 系统用户ID
     */
    private Long userId;

    /**
     * 部门ID
     */
    private Long deptId;


}
