package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 系统用户-基础信息
 *
 * @author zyf
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("base_user")
@TableAlias("baseuser")
public class BaseUser extends AbstractEntity {
    private static final long serialVersionUID = -735161640894047414L;
    /**
     * 系统用户ID
     */
    @TableId(value = "userId", type = IdType.ASSIGN_ID)
    private Long userId;

    /**
     * 登陆名
     */
    private String userName;

    /**
     * 用户类型:platform-平台 isp-服务提供商 dev-自研开发者
     */
    private String userType;


    /**
     * 用户编号
     */
    private String userNo;

    /**
     * 证件号码
     */
    private String certNo;

    /**
     * 证件类型
     */
    private String certType;

    /**
     * 企业ID
     */
    private Long companyId;

    /**
     * 部门ID
     */
    private Long deptId;

    /**
     * 自定义部门名称
     */
    private String customDeptName;
    /**
     * 机构ID
     */
    private Long organizationId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 描述
     */
    private String userDesc;


    /**
     * 注册IP
     */
    private String registerIp;


    /**
     * 状态:0-禁用 1-启用 2-锁定
     */
    private Integer status;

    /**
     * 状态:0-下线 1-在线
     */
    private Integer onLineState;

    /**
     * 密码
     */
    @JsonIgnore
    @TableField(exist = false)
    private String password;

    /**
     * 账户
     */
    @JsonIgnore
    @TableField(exist = false)
    private String account;


    /**
     * 注册角色
     */
    @JsonIgnore
    @TableField(exist = false)
    private String roleCode;


    /**
     * 手机号同步注册账户
     */
    @JsonIgnore
    @TableField(exist = false)
    private String mobileReg;

    /**
     * 邮箱同步注册账户
     */
    @JsonIgnore
    @TableField(exist = false)
    private String emailReg;

    @TableField(exist = false)
    private String errMsg;

    @TableField(exist = false)
    private String str;

}
