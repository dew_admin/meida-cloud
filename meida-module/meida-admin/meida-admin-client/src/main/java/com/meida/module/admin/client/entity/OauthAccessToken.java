package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Blob;

/**
 * oauth2访问令牌
 *
 * @author flyme
 * @date 2020-06-04
 */
@Data
@Accessors(chain = true)
@TableName("oauth_access_token")
@TableAlias("oat")
@ApiModel(value="OauthAccessToken对象", description="oauth2访问令牌")
public class OauthAccessToken {
private static final long serialVersionUID=1L;

    private String tokenId;

    private Blob token;

    @TableId(value = "authentication_id", type = IdType.ASSIGN_ID)
    private String authenticationId;

    private String userName;

    private String clientId;

    private Blob authentication;

    private String refreshToken;

}
