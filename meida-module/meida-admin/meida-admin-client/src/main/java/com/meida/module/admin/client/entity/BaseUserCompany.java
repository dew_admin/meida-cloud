package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * 企业-企业与用户关联
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */
@TableName("base_user_company")
@Data
@TableAlias("buc")
public class BaseUserCompany implements Serializable {
    private static final long serialVersionUID = -667816444278087761L;

    @TableId(value = "userCompanyId", type = IdType.ASSIGN_ID)
    private Long userCompanyId;
    /**
     * 系统用户ID
     */
    private Long userId;

    /**
     * 角色ID
     */
    private Long companyId;


}
