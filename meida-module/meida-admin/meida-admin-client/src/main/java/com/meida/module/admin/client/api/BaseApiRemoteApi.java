package com.meida.module.admin.client.api;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.entity.BaseApi;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author
 */
public interface BaseApiRemoteApi {

    /**
     * 根据service_path获取api配置信息
     *
     * @param
     * @return
     */
    @PostMapping("/api/infoByServiceIdAndPath")
    ResultBody<BaseApi> getApiInfo(@RequestParam(value = "serviceId") String serviceId,
                                   @RequestParam(value = "path") String path);
}


