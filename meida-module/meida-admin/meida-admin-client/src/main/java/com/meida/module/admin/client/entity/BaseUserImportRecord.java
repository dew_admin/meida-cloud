package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 用户导入
 *
 * @author flyme
 * @date 2023-06-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("base_user_import_record")
@TableAlias("buir")
@ApiModel(value = "BaseUserImportRecord对象", description = "用户导入")
public class BaseUserImportRecord extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    @TableId(value = "importId", type = IdType.ASSIGN_ID)
    private Long importId;

    @ApiModelProperty(value = "文件名")
    private String fileName;

    @ApiModelProperty(value = "所属机构")
    private String deptId;
    private Long companyId;

    @ApiModelProperty(value = "导入数量")
    private Integer importNum;

    @ApiModelProperty(value = "成功数量")
    private Integer successNum;

    @ApiModelProperty(value = "失败数量")
    private Integer failNum;

}
