package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * oauth2客户端令牌
 *
 * @author flyme
 * @date 2020-06-04
 */
@Data
@Accessors(chain = true)
@TableName("oauth_client_token")
@TableAlias("oct")
@ApiModel(value="OauthClientToken对象", description="oauth2客户端令牌")
public class OauthClientToken {

private static final long serialVersionUID=1L;

    private String tokenId;

    private Blob token;

    @TableId(value = "authentication_id", type = IdType.ASSIGN_ID)
    private String authenticationId;

    private String userName;

    private String clientId;

}
