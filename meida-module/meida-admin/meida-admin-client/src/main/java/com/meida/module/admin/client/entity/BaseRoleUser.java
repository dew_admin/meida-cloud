package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;

import java.io.Serializable;

/**
 * 系统角色-角色与用户关联
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */
@TableName("base_role_user")
@TableAlias("ru")
public class BaseRoleUser implements Serializable {
    private static final long serialVersionUID = -667816444278087761L;

    @TableId(value = "userRoleId", type = IdType.ASSIGN_ID)
    private Long userRoleId;
    /**
     * 系统用户ID
     */
    private Long userId;

    /**
     * 角色ID
     */
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取角色ID
     *
     * @return role_id - 角色ID
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 设置角色ID
     *
     * @param roleId 角色ID
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
