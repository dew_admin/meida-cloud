package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 用户导入日志记录
 *
 * @author flyme
 * @date 2023-06-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("base_user_import_record_logs")
@TableAlias("buirl")
@ApiModel(value = "BaseUserImportRecordLogs对象", description = "用户导入日志记录")
public class BaseUserImportRecordLogs extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    private Long importId;

    @ApiModelProperty(value = "id")
    @TableId(value = "logsId", type = IdType.ASSIGN_ID)
    private Long logsId;

    @ApiModelProperty(value = "文件名")
    private String fileName;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 昵称
     */
    private String nickName;
    /**
     * 登陆名
     */
    private String userName;
    /**
     * 描述
     */
    private String userDesc;
    private String password;
    private String companyNo;
    private String deptCode;
    /**
     * 企业ID
     */
    private Long companyId;

    /**
     * 部门ID
     */
    private Long deptId;


    @ApiModelProperty(value = "错误描述")
    private String errMsg;

}
