package com.meida.module.admin.client.api;


import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.model.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zyf
 */
@Component
@FeignClient(value = CommonConstants.BASE_SERVICE)
public interface BaseUserRemoteApi {
    /**
     * 获取用户详细信息
     *
     * @param userId
     * @return
     */
    @PostMapping("/user/info")
    ResultBody<UserInfo> getUserInfo(@RequestParam(value = "userId") Long userId);


}
