package com.meida.module.admin.client.model;

import com.google.common.collect.Lists;
import com.meida.common.security.OpenAuthority;
import com.meida.module.admin.client.entity.BaseUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * @author: zyf
 * @date: 2018/11/12 11:35
 * @description:
 */
@Data
public class UserInfo extends BaseUser implements Serializable {
    private static final long serialVersionUID = 6717800085953996702L;
    /**
     * 用户角色
     */
    private Collection<Map> roles = Lists.newArrayList();

    /**
     * 用户权限
     */
    private Collection<OpenAuthority> authorities = Lists.newArrayList();

    /**
     * 第三方账号
     */
    private String thirdParty;

    /**
     * 是否是系统管理员
     */
    private Integer superAdmin;
    
}
