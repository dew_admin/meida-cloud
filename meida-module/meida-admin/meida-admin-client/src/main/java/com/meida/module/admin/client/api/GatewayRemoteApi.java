package com.meida.module.admin.client.api;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.entity.GatewayRoute;
import com.meida.module.admin.client.model.IpLimitApi;
import com.meida.module.admin.client.model.RateLimitApi;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author zyf
 */
public interface GatewayRemoteApi {

    /**
     * 获取接口黑名单列表
     *
     * @return
     */
    @GetMapping("/gateway/api/blackList")
    ResultBody<List<IpLimitApi>> getApiBlackList() ;

    /**
     * 获取接口白名单列表
     * @return
     */
    @GetMapping("/gateway/api/whiteList")
    ResultBody<List<IpLimitApi> > getApiWhiteList();

    /**
     * 获取限流列表
     * @return
     */
    @GetMapping("/gateway/api/rateLimit")
    ResultBody<List<RateLimitApi> > getApiRateLimitList();

    /**
     * 获取路由列表
     * @return
     */
    @GetMapping("/gateway/api/route")
    ResultBody<List<GatewayRoute> > getApiRouteList();
}
