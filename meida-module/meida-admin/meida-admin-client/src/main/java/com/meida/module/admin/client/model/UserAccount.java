package com.meida.module.admin.client.model;

import com.meida.module.admin.client.entity.BaseAccount;
import lombok.Data;

import java.io.Serializable;

/**
 * 自定义账户信息
 */
@Data
public class UserAccount extends BaseAccount implements Serializable {
    private static final long serialVersionUID = 6717800085953996702L;

    /**
     * 系统用户资料
     */
    private UserInfo userProfile;


}
