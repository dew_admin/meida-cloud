package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * oauth2刷新令牌
 *
 * @author flyme
 * @date 2020-06-04
 */
@Data
@Accessors(chain = true)
@TableName("oauth_refresh_token")
@TableAlias("ort")
@ApiModel(value="OauthRefreshToken对象", description="oauth2刷新令牌")
public class OauthRefreshToken {

private static final long serialVersionUID=1L;

    private String tokenId;

    private Blob token;

    private Blob authentication;

}
