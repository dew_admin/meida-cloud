package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统资源-菜单信息
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */


@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("base_menu")
@TableAlias("menu")
@ApiModel(value = "BaseMenu菜单对象", description = "菜单表")
public class BaseMenu extends AbstractEntity {
    /**
     * 菜单Id
     */
    @TableId(value = "menuId", type = IdType.ASSIGN_ID)
    private Long menuId;

    /**
     * 菜单编码
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 图标
     */
    private String icon;

    /**
     * 父级菜单
     */
    private Long parentId;

    /**
     * 路径前缀:/,http://,https://
     */
    private String scheme;

    /**
     * 请求路径
     */
    private String path;

    /**
     * 打开方式:_self窗口内,_blank新窗口
     */
    private String target;

    /**
     * 优先级 越小越靠前
     */
    private Integer priority;

    /**
     * 菜单分组
     */
    private Integer menuGroup;

    /**
     * 描述
     */
    private String menuDesc;

    /**
     * 布局页面
     */
    private String menuLayout;

    /**
     * 是否隐藏页面顶部内容
     */
    private Boolean hiddenHeaderContent;

    /**
     * 状态:0-无效 1-有效
     */
    private Integer status;


    /**
     * 保留数据0-否 1-是 不允许删除
     */
    private Integer isPersist;

    /**
     * 服务ID
     */
    private String serviceId;



}
