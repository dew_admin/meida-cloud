package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * oauth2已授权客户端
 *
 * @author flyme
 * @date 2020-06-04
 */
@Data
@Accessors(chain = true)
@TableName("oauth_approvals")
@TableAlias("approvals")
@ApiModel(value="OauthApprovals对象", description="oauth2已授权客户端")
public class OauthApprovals {

private static final long serialVersionUID=1L;

    private String userId;

    private String clientId;

    private String scope;

    private String status;

    private Date expiresAt;

    private Date lastModifiedAt;

}
