package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统权限-功能操作关联表
 *
 * @author: zyf
 * @date: 2018/10/24 16:21
 * @description:
 */
@TableName("base_authority_action")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BaseAuthorityAction extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 1471599074044557390L;

    @TableId(value = "actionAuthorityId", type = IdType.ASSIGN_ID)
    private Long actionAuthorityId;
    /**
     * 操作资源ID
     */
    private Long actionId;

    /**
     * 权限ID
     */
    private Long authorityId;

}
