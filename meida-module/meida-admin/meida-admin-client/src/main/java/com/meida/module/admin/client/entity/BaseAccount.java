package com.meida.module.admin.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统用户-登录账号
 * @author zyf
 */
@Data
@TableName("base_account")
@TableAlias("account")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class BaseAccount extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -4484479600033295192L;

    @TableId(value = "accountId", type = IdType.ASSIGN_ID)
    private Long accountId;

    /**
     * 系统用户Id
     */
    private Long userId;

    /**
     * 标识：手机号、邮箱、 系统用户名、或第三方应用的唯一标识
     */
    private String account;

    /**
     * 密码凭证：站内的保存密码、站外的不保存或保存token）
     */
    private String password;

    /**
     * 登录类型:password-密码、mobile-手机号、email-邮箱、weixin-微信、weibo-微博、qq-等等
     */
    private String accountType;

    /**
     * 第三方登录昵称
     */
    private String nickName;
    /**
     * 第三方登录头像
     */
    private String avatar;

    /**
     * 账户域
     */
    private String domain;

    /**
     * 状态:0-禁用 1-启用 2-锁定
     */
    private Integer status;

    private String registerIp;


    public BaseAccount(String account, String password, String accountType, String nickName, String avatar) {
        this.account = account;
        this.password = password;
        this.accountType = accountType;
        this.avatar = avatar;
        this.nickName = nickName;
    }

    public BaseAccount(Long userId, String account, String password, String accountType, String nickName, String avatar) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.accountType = accountType;
        this.avatar = avatar;
        this.nickName = nickName;
    }

    public BaseAccount(Long userId, String account, String password, String accountType) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.accountType = accountType;
    }

}
