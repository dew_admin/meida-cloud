package com.meida.module.admin.client.api;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.model.AuthorityMenu;
import com.meida.common.base.module.AuthorityResource;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 权限控制API接口
 *
 * @author zyf
 */
public interface BaseAuthorityRemoteApi {
    /**
     * 获取所有访问权限列表
     * @return
     */
    @GetMapping("/authority/access")
    ResultBody<List<AuthorityResource>> findAuthorityResource();

    /**
     * 获取菜单权限列表
     *
     * @return
     */
    @GetMapping("/authority/menu")
    ResultBody<List<AuthorityMenu>> findAuthorityMenu();
}
