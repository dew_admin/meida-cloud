package com.meida.module.admin.client.api;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.admin.client.model.UserAccount;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zyf
 */
public interface BaseUserAccountRemoteApi {
    /**
     * 登录
     *
     * @param username 登录名
     * @return
     */
    @PostMapping("/account/localLogin")
    ResultBody<UserAccount> localLogin(@RequestParam(value = "username") String username);


    /**
     * 注册第三方登录账号
     *
     * @param username
     * @param avatar
     * @param accountType
     * @param nickName
     * @return
     */
    @PostMapping("/account/register/thirdParty")
    ResultBody<UserAccount> registerThirdPartyAccount(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "avatar") String avatar,
            @RequestParam(value = "accountType") String accountType,
            @RequestParam(value = "nickName") String nickName
    );

    /**
     * 注册手机号账号
     *
     * @param accountName
     * @return
     */
    @PostMapping("/account/register/mobile")
    ResultBody<UserAccount> registerByMobile(
            @RequestParam(value = "accountName") String accountName,
            @RequestParam(value = "password") String password
    );

    /**
     * 修改密码
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @PostMapping("/account/reset/password")
    ResultBody resetPassword(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "oldPassword") String oldPassword,
            @RequestParam(value = "newPassword") String newPassword
    );

}
