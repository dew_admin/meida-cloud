package com.meida.module.weixin.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

public interface TemplateMessageService {

    ResultBody  sendTemplateMessage(String appid, WxMpTemplateMessage templateMessage);
}
