package com.meida.module.weixin.provider.service.impl;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.weixin.provider.service.TemplateMessageService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateMessageServiceImpl implements TemplateMessageService {
    @Autowired
    private WxMpService wxService;

    @Override
    public ResultBody sendTemplateMessage(String appid, WxMpTemplateMessage templateMessage) {
        try {
            wxService.switchoverTo(appid).getTemplateMsgService().sendTemplateMsg(templateMessage);
        } catch (WxErrorException e) {
            return ResultBody.failed("微信模板消息发送失败:错误信息" + e.getMessage());
        }
        return ResultBody.ok("微信模板消息发送成功");
    }
}
