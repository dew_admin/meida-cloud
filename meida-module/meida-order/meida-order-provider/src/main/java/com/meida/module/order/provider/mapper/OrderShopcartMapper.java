package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderShopcart;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 购物车 Mapper 接口
 * @author flyme
 * @date 2019-12-01
 */
public interface OrderShopcartMapper extends SuperMapper<OrderShopcart> {

}
