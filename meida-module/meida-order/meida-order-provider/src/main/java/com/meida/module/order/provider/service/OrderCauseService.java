package com.meida.module.order.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.order.client.entity.OrderCause;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 退款方式 接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderCauseService extends IBaseService<OrderCause> {
     List<EntityMap> selectCaustList();
}
