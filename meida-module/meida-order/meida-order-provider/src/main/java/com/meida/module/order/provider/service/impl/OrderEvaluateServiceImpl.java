package com.meida.module.order.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.client.entity.OrderEvaluate;
import com.meida.module.order.provider.mapper.OrderEvaluateMapper;
import com.meida.module.order.provider.service.OrderDetailsService;
import com.meida.module.order.provider.service.OrderEvaluateService;
import com.meida.module.order.provider.service.OrderInfoService;
import com.meida.module.order.provider.service.OrderLogisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单评价表  实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrderEvaluateServiceImpl extends BaseServiceImpl<OrderEvaluateMapper, OrderEvaluate> implements OrderEvaluateService {

    @Autowired
    private OrderDetailsService orderDetailsService;
    @Autowired
    private OrderLogisticsService logisticsService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<EntityMap> listByCount(Long productId, int count) {
        CriteriaQuery cq = new CriteriaQuery(OrderEvaluate.class);
        List<EntityMap> commentList = new ArrayList<>();
        if (FlymeUtils.isNotEmpty(productId)) {
            cq.select(OrderEvaluate.class, "orderEvaluateId", "evaluateContent", "evaluateImages", "evaluateScore", "createTime");
            cq.select("user.nickName", "user.userName", "user.avatar");
            cq.eq("evaluate.evaluateState", CommonConstants.ENABLED);
            cq.eq("evaluate.productId", productId);
            cq.orderByDesc("evaluate.createTime");
            cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("evaluateUserId").setJoinField("userId");
            if (count > 0) {
                cq.last("limit " + count);
            }
            commentList = selectEntityMap(cq);
        }
        return commentList;
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public EntityMap findByOrderDetailsId(Long orderDetailsId) {
        CriteriaQuery cq = new CriteriaQuery(OrderEvaluate.class);
        cq.eq(true, "orderDetailsId", orderDetailsId);
        return findOne(cq);
    }

    /**
     * 商品评价列表前置操作
     *
     * @param cq
     * @param prodComment
     * @param requestMap
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<OrderEvaluate> cq, OrderEvaluate prodComment, EntityMap requestMap) {
        Long productId = requestMap.getLong("productId");
        cq.select(OrderEvaluate.class, "orderEvaluateId", "evaluateContent", "evaluateImages", "evaluateScore","describeScore", "createTime");
        cq.select("user.nickName", "user.userName", "user.avatar");
        cq.eq("evaluate.evaluateState", CommonConstants.ENABLED);
        cq.eq("evaluate.productId", productId);
        cq.orderByDesc("evaluate.createTime");
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("evaluateUserId");
        return ResultBody.ok();
    }




    /**
     * 评价保存前置操作
     *
     * @param cs
     * @param orderEvaluate
     * @return
     */
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, OrderEvaluate orderEvaluate, EntityMap extra) {
        Long orderDetailsId = orderEvaluate.getOrderDetailsId();
        ApiAssert.isNotEmpty("订单ID不能为空", orderDetailsId);
        ApiAssert.isNotEmpty("评价内容不能为空", orderEvaluate.getEvaluateContent());
        OrderDetails orderDetails = orderDetailsService.getById(orderDetailsId);
        ApiAssert.isNotEmpty("评价订单不存在", orderDetails);
        ApiAssert.isTrue("订单已评价", orderDetails.getEvaluatedState().equals(CommonConstants.DISABLED));
        orderEvaluate.setOrderId(orderDetails.getOrderId());
        orderEvaluate.setProductId(orderDetails.getProductId());
        orderEvaluate.setShopId(orderDetails.getShopId());
        orderEvaluate.setEvaluateUserId(OpenHelper.getUserId());
        //默认未删除
        orderEvaluate.setEvaluateState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }

    /**
     * 评价保存后置续操作
     *
     * @param cs
     * @param orderEvaluate
     * @return
     */
    @Override
    public ResultBody afterAdd(CriteriaSave cs, OrderEvaluate orderEvaluate, EntityMap extra) {
        //更新订单明细评价状态
        orderDetailsService.setEvaluatedState(orderEvaluate.getOrderDetailsId(), CommonConstants.ENABLED);
        //获取物流评分,已评价过的不在传递该参数
        Integer logisticsScore = cs.getInt("logisticsScore", 0);
        //获取商家评分,已评价过的不在传递该参数
        Integer shopScore = cs.getInt("shopScore", 0);
        if (FlymeUtils.isNotEmpty(logisticsScore)) {
            //物流评分
            logisticsService.setLogisticsScore(orderEvaluate.getOrderId(), logisticsScore);
        }
        if (FlymeUtils.isNotEmpty(shopScore)) {
            //店铺评分
            orderInfoService.setShopScore(orderEvaluate.getOrderId(), shopScore);
        }
        return ResultBody.msg("评价成功");
    }
}
