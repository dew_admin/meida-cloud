package com.meida.module.order.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrderShopcart;
import com.meida.module.order.provider.service.OrderShopcartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 购物车控制器
 *
 * @author flyme
 * @date 2019-12-01
 */
@RestController
@RequestMapping("/shopcart/")
@Api(tags = "订单模块-购物车管理")
public class OrderShopcartController extends BaseController<OrderShopcartService, OrderShopcart> {


    @ApiOperation(value = "我的购物车列表", notes = "购物车列表")
    @GetMapping(value = "list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "handlerName", value = "扩展handler", paramType = "form")
    })
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "购物车加减", notes = "购物车加减")
    @PostMapping(value = "save")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "skuId", value = "商品规格ID", paramType = "form"),
            @ApiImplicitParam(name = "num", value = "购买数量", paramType = "form")
    })
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.addShopCart(params);
    }


    @ApiOperation(value = "删除所选购物车商品", notes = "删除所选购物车商品")
    @PostMapping(value = "delete")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "商品规格Id,多个逗号隔开", paramType = "form")
    })
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "清空购物车", notes = "清空购物车")
    @PostMapping(value = "clearAll")
    public ResultBody clearAll() {
        Long userId = OpenHelper.getUserId();
        return bizService.clearAll(userId);
    }


    @ApiOperation(value = "选择购物车单个商品", notes = "选择购物车单个商品")
    @PostMapping(value = "checkByShopCardId")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shopCardId", value = "购物车记录ID", paramType = "form")
    })
    public ResultBody checkByShopCardId(@RequestParam(value = "shopCardId") Long shopCardId) {
        Long userId = OpenHelper.getUserId();
        return bizService.checkByShopCardId(userId, shopCardId);
    }

    @ApiOperation(value = "全选购物车内单个店铺所有商品", notes = "全选购物车内单个店铺所有商品")
    @PostMapping(value = "checkByShopId")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shopId", value = "店铺Id", paramType = "form")
    })
    public ResultBody checkByShopId(@RequestParam(value = "shopId") Long shopId, @RequestParam(value = "selected") Integer selected) {
        Long userId = OpenHelper.getUserId();
        return bizService.checkByShopId(userId, shopId, selected);
    }

    @ApiOperation(value = "全/反选购物车", notes = "全反选购物车")
    @PostMapping(value = "checkAll")
    public ResultBody checkAll(@RequestParam(value = "selected") Integer selected) {
        Long userId = OpenHelper.getUserId();
        return bizService.checkAll(userId, selected);
    }


    @ApiOperation(value = "查询购物车商品数量", notes = "查询购物车商品数量")
    @PostMapping(value = "shopCardTotal")
    public ResultBody shopCardTotal() {
        Long userId = OpenHelper.getUserId();
        EntityMap result = bizService.totalShopCard(userId, null);
        return ResultBody.ok(result);
    }

    @ApiOperation(value = "查询购物车订单结算时优惠券列表", notes = "查询购物车订单结算时优惠券列表")
    @GetMapping(value = "couponList")
    public ResultBody couponList() {
        Long userId = OpenHelper.getUserId();
        EntityMap couponList = bizService.selectCouponList(userId);
        return ResultBody.ok(couponList);
    }





    @ApiOperation(value = "更新购车商品所选规格", notes = "更新购车商品所选规格")
    @PostMapping(value = "updateSku")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shopCardId", value = "shopCardId", paramType = "form"),
            @ApiImplicitParam(name = "skuId", value = "skuId", paramType = "form"),
            @ApiImplicitParam(name = "num", value = "num", paramType = "form")
    })
    public ResultBody updateSku(@RequestParam(value = "shopCardId") Long shopCardId, @RequestParam(value = "skuId") Long skuId, @RequestParam(value = "num") Integer num) {
        return bizService.updateSku(shopCardId, skuId, num);
    }


}
