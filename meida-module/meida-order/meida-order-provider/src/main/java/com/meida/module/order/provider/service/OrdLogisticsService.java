package com.meida.module.order.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.order.client.entity.OrdLogisticsCompany;

/**
 * 物流公司 接口
 *
 * @author flyme
 * @date 2020-01-01
 */
public interface OrdLogisticsService extends IBaseService<OrdLogisticsCompany> {

}
