package com.meida.module.order.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.order.client.entity.OrdInvoice;
import org.springframework.stereotype.Component;

/**
 * 门店发票列表
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("shopInvoiceListHandler")
public class ShopInvoiceListHandler implements PageInterceptor {
    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(OrdInvoice.class, "companyId", companyId);
    }

}
