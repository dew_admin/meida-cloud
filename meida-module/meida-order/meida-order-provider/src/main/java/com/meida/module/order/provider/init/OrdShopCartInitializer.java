package com.meida.module.order.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.order.client.entity.OrderShopcart;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class OrdShopCartInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return OrderShopcart.class;
    }
}
