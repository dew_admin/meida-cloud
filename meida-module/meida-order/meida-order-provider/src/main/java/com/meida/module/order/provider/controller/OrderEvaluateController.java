package com.meida.module.order.provider.controller;

import com.meida.common.annotation.RlockRepeat;
import com.meida.common.lock.LockConstant;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrderEvaluate;
import com.meida.module.order.provider.service.OrderEvaluateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.Map;


/**
 * 订单评价表 控制器
 *
 * @author flyme
 * @date 2019-11-28
 */
@RestController
@RequestMapping("/order/evaluate/")
@Api(tags = "订单模块-订单评价")
public class OrderEvaluateController extends BaseController<OrderEvaluateService, OrderEvaluate> {

    @ApiOperation(value = "订单评价-分页列表", notes = "订单评价分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "订单评价-列表", notes = "订单评价")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "订单评价-添加", notes = "添加订单评价")
    @PostMapping(value = "save")
    //@RLock(prefix = "evaluate_", paramName = {"orderDetailsId"})
    @RlockRepeat(prefix = "register", lockConstant = LockConstant.SUBMIT)
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "订单评价 -更新", notes = "更新订单评价")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "订单评价-删除", notes = "删除订单评价")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "订单评价-详情", notes = "订单评价详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "订单评价 -更新状态值", notes = "订单评价更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }


}
