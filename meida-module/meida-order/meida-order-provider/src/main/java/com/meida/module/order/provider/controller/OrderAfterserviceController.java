package com.meida.module.order.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrderAfterservice;
import com.meida.module.order.provider.service.OrderAfterserviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 售后服务控制器
 *
 * @author flyme
 * @date 2019-11-28
 */
@RestController
@RequestMapping("/order/afterservice/")
@Api(tags = "订单模块-商品售后")
public class OrderAfterserviceController extends BaseController<OrderAfterserviceService, OrderAfterservice> {

    @ApiOperation(value = "售后服务-分页列表", notes = "售后服务分页列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "售后服务-列表", notes = "售后服务列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }


    @ApiOperation(value = "申请售后提交", notes = "提交售后服务")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "售后服务-更新", notes = "更新售后服务")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "售后服务-删除", notes = "删除售后服务")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "售后服务-详情", notes = "售后服务详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "根据订单ID查询售后", notes = "根据订单ID查询售后")
    @GetMapping(value = "getByOrderId")
    public ResultBody getByOrderId(@RequestParam(value = "orderId") Long orderId) {
        EntityMap map = bizService.findByOrderId(orderId);
        return ResultBody.ok(map);
    }

    @ApiOperation(value = "售后服务-更新状态值", notes = "售后服务更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
