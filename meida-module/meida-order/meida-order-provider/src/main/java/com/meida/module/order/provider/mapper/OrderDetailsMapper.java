package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderDetails;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 订单明细表  Mapper 接口
 *
 * @author flyme
 * @date 2019-11-27
 */
public interface OrderDetailsMapper extends SuperMapper<OrderDetails> {

    /**
     * 根据子订单ID更新普通商品销量
     *
     * @param orderId
     * @return
     */
    Boolean updateSaleCountByOrderId(Long orderId);

    /**
     * 根据父订单ID更新普通商品销量
     *
     * @param orderId
     * @return
     */
    Boolean updateSaleCountByParentOrderId(Long orderId);


    /**
     * 根据子订单ID更新营销商品销量
     *
     * @param orderId
     * @return
     */
    Boolean updateMarketingSaleCountByOrderId(Long orderId);

    /**
     * 根据父订单ID更新营销商品销量
     *
     * @param orderId
     * @return
     */
    Boolean updateMarketingSaleCountByParentOrderId(Long orderId);

}
