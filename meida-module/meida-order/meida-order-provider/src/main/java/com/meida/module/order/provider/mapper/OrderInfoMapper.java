package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderInfo;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 订单表 Mapper 接口
 * @author flyme
 * @date 2019-11-27
 */
public interface OrderInfoMapper extends SuperMapper<OrderInfo> {

}
