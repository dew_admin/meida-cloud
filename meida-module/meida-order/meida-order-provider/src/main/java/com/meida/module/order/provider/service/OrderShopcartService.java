package com.meida.module.order.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.client.entity.OrderShopcart;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;
import java.util.Map;

/**
 * 购物车 接口
 *
 * @author flyme
 * @date 2019-12-01
 */
public interface OrderShopcartService extends IBaseService<OrderShopcart> {
    /**
     * 添加购物车
     *
     * @param params
     * @return
     */
    ResultBody addShopCart(Map<String, Object> params);

    /**
     * 查询购车车明细
     *
     * @param useId
     * @param skuId
     * @return
     */
    OrderShopcart findByUserIdAndSkuId(Long useId, Long skuId);

    /**
     * 清空购物车
     *
     * @param userId
     * @return
     */
    ResultBody clearAll(Long userId);

    /**
     * 全选购物车
     *
     * @param userId
     * @param selected
     * @return
     */
    ResultBody checkAll(Long userId, Integer selected);


    /**
     * 全选购物车内单个店铺所有商品
     *
     * @param shopId
     * @param userId
     * @return
     */
    ResultBody checkByShopId(Long userId, Long shopId, Integer selected);

    /**
     * 选择购物车单个商品
     *
     * @param userId     用户ID
     * @param shopCardId 购物车ID
     * @return
     */
    ResultBody checkByShopCardId(Long userId, Long shopCardId);


    /**
     * 统计购物车数量
     *
     * @param userId
     * @param selected
     * @return
     */
    EntityMap totalShopCard(Long userId, Integer selected);


    /**
     * 更新商品规格
     *
     * @param shopCardId 购物车ID
     * @param skuId      规格ID
     * @param num        购买数量
     * @return
     */
    ResultBody updateSku(Long shopCardId, Long skuId, Integer num);

    /**
     * 查询个人购物车已选商品
     *
     * @param userId 用户ID
     * @return
     */
    List<EntityMap> listByUserId(Long userId);

    /**
     * 查询个人购物车已选商品(按店铺分组)
     *
     * @param userId        用户ID
     * @param couponUserIds 优惠券Id,逗号隔开
     * @return
     */
    List<EntityMap> listByUserIdAndGroupShop(Long userId, String couponUserIds);

    /**
     * 查询订单结算时优惠券列表
     *
     * @param userId 用户ID
     * @return
     */
    EntityMap selectCouponList(Long userId);

    /**
     * 删除用户购物车中已生成订单的商品
     * @param orderDetailsList
     * @return
     */
    Boolean clearByOrderDetails(List<OrderDetails> orderDetailsList, Long userId);
}
