package com.meida.module.order.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.order.client.entity.OrdInvoice;
import com.meida.module.order.client.entity.OrdInvoiceReceiver;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.provider.mapper.OrdInvoiceMapper;
import com.meida.module.order.provider.service.OrdInvoiceReceiverService;
import com.meida.module.order.provider.service.OrdInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 发票 实现类
 *
 * @author flyme
 * @date 2019-06-26
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrdInvoiceServiceImpl extends BaseServiceImpl<OrdInvoiceMapper, OrdInvoice> implements OrdInvoiceService {

    @Autowired
    private OrdInvoiceReceiverService invoiceReceiverService;

    //@Autowired
    //private PayInfoService orderService;


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<OrdInvoice> cq, OrdInvoice ordInvoice, EntityMap requestMap) {
        cq.select(OrdInvoice.class, "*");
        cq.select(OrderInfo.class, "orderNo");
        cq.orderByAsc("invoice.invoiceState");
        cq.orderByDesc("invoice.createTime");
        cq.createJoin(OrderInfo.class);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<OrdInvoice> cq, OrdInvoice ordInvoice, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(OrdInvoice.class, "invoiceId", "invoiceTitle", "invoiceAmount", "invoiceType", "address");
        cq.select(OrdInvoiceReceiver.class, "bankName", "bankNo", "creditCode", "telephone");
        cq.select(OrderInfo.class, "orderNo");
        cq.likeByField(OrdInvoice.class, "invoiceTitle");
        cq.eq(OrdInvoice.class, "userId", userId);
        cq.createJoin(OrdInvoiceReceiver.class);
        cq.createJoin(OrderInfo.class);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, OrdInvoice ordInvoice, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        Long orderId = cs.getLongParams("orderId");
        // PayInfo order = orderService.getById(orderId);
        // Long invoiceId = order.getInvoiceId();
        // ApiAssert.isEmpty("发票已申请,不可重复申请", invoiceId);
        Long invoiceReceiverId = cs.getLongParams("invoiceReceiverId");
        if (FlymeUtils.isNotEmpty(invoiceReceiverId)) {
            OrdInvoiceReceiver invoiceReceiver = invoiceReceiverService.getById(invoiceReceiverId);
            cs.put("consignee", invoiceReceiver.getUserName());
            cs.put("address", invoiceReceiver.getAddress());
            cs.put("invoiceReceiverId", invoiceReceiverId);
        }
        cs.put("userId", userId);
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, OrdInvoice ordInvoice, EntityMap extra) {
        //更新订单开票ID
        //orderService.updateInvoiceId(orderId, invoice.getInvoiceId());
        return ResultBody.ok(ordInvoice);
    }


    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        //orderService.clearInvoiceId(ids);
        return super.afterDelete(cd, ids);
    }

    @Override
    public ResultBody addinvoice(OrdInvoice ordinvoice) {
        boolean a = save(ordinvoice);
        ResultBody resultBody = new ResultBody();
        if (a) {
            resultBody.put("ordinvoice", ordinvoice);
        }
        return resultBody;
    }

    public Boolean addInvoide(OrderInfo orderInfo, CriteriaSave cs) {
        String invoiceTitle = cs.getParams("invoiceTitle");
        Boolean tag = false;
        if (FlymeUtils.isNotEmpty(invoiceTitle)) {
            Integer invoiceType = cs.getInt("invoiceType", 1);
            String creditCode = cs.getParams("creditCode");
            String invoiceBody = cs.getParams("invoiceBody");
            Integer electron = cs.getInt("electron", 1);

            OrdInvoice ordInvoice = new OrdInvoice();
            ordInvoice.setInvoiceTitle(invoiceTitle);
            ordInvoice.setInvoiceType(invoiceType);
            ordInvoice.setCompanyId(orderInfo.getCompanyId());
            ordInvoice.setCreditCode(creditCode);
            ordInvoice.setInvoiceBody(invoiceBody);
            ordInvoice.setElectron(electron);
            ordInvoice.setOrderId(orderInfo.getOrderId());
            ordInvoice.setUserId(orderInfo.getUserId());
            ordInvoice.setInvoiceAmount(orderInfo.getPayAmount());
            tag = save(ordInvoice);
        }
        return tag;
    }


    public Boolean addInvoide(OrderInfo orderInfo, Map params) {
        CriteriaSave cs = new CriteriaSave(params, OrdInvoice.class);
        return addInvoide(orderInfo, cs);
    }

}
