package com.meida.module.order.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrdInvoice;
import com.meida.module.order.provider.service.OrdInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 发票控制器
 *
 * @author flyme
 * @date 2019-06-26
 */
@RestController
@RequestMapping("/invoice/")
@Api(tags = "订单模块-发票管理")
public class OrdInvoiceController extends BaseController<OrdInvoiceService, OrdInvoice> {

    @ApiOperation(value = "发票-列表", notes = "发票列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "发票-添加", notes = "添加发票")
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }


    @ApiOperation(value = "发票-更新", notes = "更新发票")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "发票-删除", notes = "删除发票")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "发票-详情", notes = "发票详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


}
