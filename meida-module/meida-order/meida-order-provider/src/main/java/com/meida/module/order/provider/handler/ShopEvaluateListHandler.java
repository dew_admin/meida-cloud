package com.meida.module.order.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.client.entity.OrderEvaluate;
import com.meida.module.order.client.entity.OrderInfo;
import org.springframework.stereotype.Component;

/**
 * 店铺所有商品评价列表
 *
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("shopEvaluateListHandler")
public class ShopEvaluateListHandler implements PageInterceptor {
    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.select(OrderInfo.class, "orderNo");
        cq.select(OrderDetails.class, "productTitle");
        cq.eq(OrderEvaluate.class, "shopId", companyId);
        cq.createJoin(OrderInfo.class);
        cq.createJoin(OrderDetails.class);
    }

}
