package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderCause;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 退款方式 Mapper 接口
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderCauseMapper extends SuperMapper<OrderCause> {

}
