package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrdInvoiceReceiver;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 发票主体 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-20
 */
public interface OrdInvoiceReceiverMapper extends SuperMapper<OrdInvoiceReceiver> {

}
