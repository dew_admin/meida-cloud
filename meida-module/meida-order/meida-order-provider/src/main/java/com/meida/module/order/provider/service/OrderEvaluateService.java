package com.meida.module.order.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.order.client.entity.OrderEvaluate;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 订单评价表  接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderEvaluateService extends IBaseService<OrderEvaluate> {
    /**
     * 查询指定条数商品评论,count<=0时查询全部
     */
    List<EntityMap> listByCount(Long productId, int count);

    /**
     * 查询订单明细评价
     *
     * @param orderDetailsId
     * @return
     */
    EntityMap findByOrderDetailsId(Long orderDetailsId);

}
