package com.meida.module.order.provider.listener;

import com.meida.common.constants.QueueConstants;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.provider.service.OrderDetailsService;
import com.meida.mq.annotation.MsgListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.lang.model.type.IntersectionType;

/**
 * @author zyf
 */
@Component
public class OrderRabbitMqListener {

    @Autowired
    private OrderDetailsService orderDetailsService;

    /**
     * 检测是否有商品
     *
     * @return
     */
    @MsgListener(queues = QueueConstants.QUEUE_ORDER_CHECK)
    public Long process(Long[] ids) {
        Long n = 0L;
        CriteriaQuery cq = new CriteriaQuery(OrderDetails.class);
        cq.in("productId", ids);
        try {
            n = orderDetailsService.count(cq);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }

}
