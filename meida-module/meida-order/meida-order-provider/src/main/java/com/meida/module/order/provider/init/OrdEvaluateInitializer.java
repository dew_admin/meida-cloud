package com.meida.module.order.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.order.client.entity.OrderEvaluate;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class OrdEvaluateInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return OrderEvaluate.class;
    }
}
