package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrdInvoice;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 发票 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-26
 */
public interface OrdInvoiceMapper extends SuperMapper<OrdInvoice> {

}
