package com.meida.module.order.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.order.client.entity.OrderDetails;

import java.util.List;
import java.util.Map;

/**
 * 订单明细表  接口
 *
 * @author flyme
 * @date 2019-11-27
 */
public interface OrderDetailsService extends IBaseService<OrderDetails> {
    /**
     * 查询列表订单明细(管理商家名称)
     *
     * @param orderId
     * @param parentOrder
     * @return
     */
    List<EntityMap> listByOrderIdForList(Long orderId, Integer parentOrder);


    /**
     * 查询订单明细
     *
     * @param orderDetailsId
     * @return
     */
    EntityMap findByOrderDetailsId(Long orderDetailsId);

    /**
     * 查询订单明细包含的店铺数量
     *
     * @param orderId
     * @return
     */
    Integer coutShopIdCount(Long orderId);


    /**
     * 检查营销商品是否购买
     *
     * @param marketingId
     * @param userId
     * @return
     */
    Long checkMarketingProduct(Long marketingId,Long userId);

    /**
     * 查询列表订单明细(按店铺分组)
     *
     * @param orderId
     * @param parentOrder
     * @return
     */
    List<EntityMap> listByOrderIdForDetails(Long orderId, Integer parentOrder);

    /**
     * 查询订单明细评价信息
     *
     * @param params
     * @return
     */
    ResultBody getCommentInfo(Map params);

    /**
     * 更新评价状态
     *
     * @param orderDetailsId
     * @param evaluatedState
     */
    void setEvaluatedState(Long orderDetailsId, Integer evaluatedState);

    /**
     * 更新售后状态
     *
     * @param orderDetailsId
     * @param afterSaleState
     */
    void setAfterSaleState(Long orderDetailsId, Integer afterSaleState);


}
