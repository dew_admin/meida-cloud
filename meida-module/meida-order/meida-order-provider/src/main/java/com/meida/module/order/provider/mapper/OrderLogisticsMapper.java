package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderLogistics;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 物流信息 Mapper 接口
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderLogisticsMapper extends SuperMapper<OrderLogistics> {

}
