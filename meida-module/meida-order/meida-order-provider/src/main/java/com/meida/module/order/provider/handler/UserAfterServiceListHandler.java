package com.meida.module.order.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.order.client.entity.OrderAfterservice;
import org.springframework.stereotype.Component;

/**
 * 个人售后服务列表
 *
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("userAfterServiceListHandler")
public class UserAfterServiceListHandler implements PageInterceptor {
    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        cq.eq(OrderAfterservice.class, "userId", userId);
    }

}
