package com.meida.module.order.provider.service;

import com.meida.module.order.client.entity.OrdInvoiceReceiver;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 发票主体 接口
 *
 * @author flyme
 * @date 2019-07-20
 */
public interface OrdInvoiceReceiverService extends IBaseService<OrdInvoiceReceiver> {

}
