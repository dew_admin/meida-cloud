package com.meida.module.order.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.order.client.entity.OrdLogisticsCompany;
import com.meida.module.order.provider.mapper.OrdLogisticsMapper;
import com.meida.module.order.provider.service.OrdLogisticsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 物流公司接口实现类
 *
 * @author flyme
 * @date 2020-01-01
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrdLogisticsServiceImpl extends BaseServiceImpl<OrdLogisticsMapper, OrdLogisticsCompany> implements OrdLogisticsService {


}
