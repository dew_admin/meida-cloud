package com.meida.module.order.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.order.client.entity.OrderCause;
import com.meida.module.order.provider.mapper.OrderCauseMapper;
import com.meida.module.order.provider.service.OrderCauseService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 退款方式 实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrderCauseServiceImpl extends BaseServiceImpl<OrderCauseMapper, OrderCause> implements OrderCauseService {

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<EntityMap> selectCaustList() {
        CriteriaQuery cq = new CriteriaQuery(OrderCause.class);
        cq.eq(OrderCause.class, "state", CommonConstants.ENABLED);
        return selectEntityMap(cq);
    }
}
