package com.meida.module.order.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.order.client.entity.OrderInfo;

import java.util.Map;

/**
 * 订单接口
 *
 * @author flyme
 * @date 2019-11-27
 */
public interface OrderInfoService extends IBaseService<OrderInfo> {

    /**
     * 购物车中普通商品订单提交前确认(计算优惠金额,运费,设置收货地址,发票等)
     *
     * @param params        请求参数
     * @param couponUserIds 优惠券Id,多个逗号隔开
     * @return
     */
    ResultBody confirm(Map params, String couponUserIds);


    /**
     * 商品直接购买前确认(计算优惠金额,运费,设置收货地址,发票等)
     *
     * @param params        请求参数
     * @param couponUserIds 优惠券ID,多个逗号隔开
     * @param buyNum        购买数量
     * @param marketingId   营销商品ID
     * @param skuId         商品规格Id
     * @return
     */
    ResultBody confirm(Map params, Long skuId, Long marketingId, Integer buyNum, String couponUserIds);


    /**
     * 商品直接购买前校验是否可以购买
     *
     * @param params      请求参数
     * @param marketingId 营销商品ID
     * @return
     */
    ResultBody check(Map params, Long marketingId);


    /**
     * 取消订单
     *
     * @param map
     * @return
     */
    ResultBody closeOrder(Map map);

    /**
     * 删除订单(逻辑删除)
     *
     * @param map
     * @return
     */
    ResultBody deleteOrder(Map map);

    /**
     * 确认收货
     *
     * @param params 请求参数
     * @return
     */
    ResultBody receiveOrder(Map params);

    /**
     * 确认发货
     *
     * @param orderId
     * @return
     */
    ResultBody setLogisticsState(Long orderId);

    /**
     * 店铺评分
     *
     * @param orderId   订单Id
     * @param shopScore 分数
     */
    void setShopScore(Long orderId, Integer shopScore);

    /**
     * 根据订单号查询订单
     *
     * @param orderNo
     * @return
     */
    OrderInfo findByOrderNo(String orderNo);

    /**
     * 支付成功修改订单状态
     *
     * @param outTradeNo
     * @param orderState
     */
    Boolean paySuccess(String outTradeNo, Map<String, Object> context, Integer orderState);


    /**
     * 商品提交订单
     *
     * @param params        请求参数
     * @param skuId         商品规格Id
     * @param marketingId   营销商品Id
     * @param addressId     收货地址
     * @param invoiceId     发票
     * @param leaveMsg      留言
     * @param couponUserIds 优惠券Id,多个逗号隔开
     * @return
     */
    ResultBody orderSub(Map params, Long addressId, Long invoiceId, String couponUserIds, Long skuId, Long marketingId, Integer buyNum, String leaveMsg);


    /**
     * 商品直接购买结算时优惠券列表
     *
     * @param skuId
     * @param buyNum
     * @param userId
     * @return
     */
    EntityMap couponList(Long skuId, Integer buyNum, Long userId);

    /**
     * 统计各个状态店铺订单数量
     *
     * @param shopId
     * @return
     */
    EntityMap countOrderStateByShopId(Long shopId);


    /**
     * 统计各个状态用户订单数量
     *
     * @param userId
     * @return
     */
    EntityMap countOrderStateByUserId(Long userId);


    /**
     * 按门店统计销售额
     *
     * @param params 请求参数
     * @return
     */
    ResultBody totalSale(Map params);

    /**
     * 单个门店销售额明细
     *
     * @param params 请求参数
     * @return
     */
    ResultBody totalSaleByShop(Map params);

    /**
     * 统计销售情况
     *
     * @return
     */
    Map<String, Object> totalSales(Long comapnyId) ;


    /**
     * 按年月周日统计销售额
     * @param params
     * @return
     */
    ResultBody totalSaleByType(Map params);

    /**
     * 订单数据汇总
     * @param params
     * @return
     */
    ResultBody getTotalData(Map params);


    /**
     * 按商品分类统计销售情况
     * @param params
     * @return
     */
    ResultBody getSaleByCateGory(Map params);
}
