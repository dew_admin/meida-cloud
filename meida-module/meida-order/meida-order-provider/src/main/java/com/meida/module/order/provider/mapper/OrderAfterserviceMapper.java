package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderAfterservice;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 售后服务 Mapper 接口
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderAfterserviceMapper extends SuperMapper<OrderAfterservice> {

}
