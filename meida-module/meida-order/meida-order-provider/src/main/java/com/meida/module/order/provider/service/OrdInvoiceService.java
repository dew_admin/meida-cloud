package com.meida.module.order.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.order.client.entity.OrdInvoice;
import com.meida.module.order.client.entity.OrderInfo;

import java.util.Map;

/**
 * 发票 接口
 *
 * @author flyme
 * @date 2019-06-26
 */
public interface OrdInvoiceService extends IBaseService<OrdInvoice> {

    /**
     * @param ordinvoice
     * @return
     */
    ResultBody addinvoice(OrdInvoice ordinvoice);

    /**
     * 添加订单发票
     *
     * @param orderInfo
     * @param cs
     * @return
     */
    Boolean addInvoide(OrderInfo orderInfo, CriteriaSave cs);

    /**
     * 添加订单发票
     *
     * @param orderInfo
     * @param cs
     * @return
     */
    Boolean addInvoide(OrderInfo orderInfo, Map params);

}
