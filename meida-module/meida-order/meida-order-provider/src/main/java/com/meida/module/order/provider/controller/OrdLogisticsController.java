package com.meida.module.order.provider.controller;

import com.meida.module.order.client.entity.OrdLogisticsCompany;
import com.meida.module.order.provider.service.OrdLogisticsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;



/**
 * 物流公司控制器
 *
 * @author flyme
 * @date 2020-01-01
 */
@RestController
@RequestMapping("/logistics/")
public class OrdLogisticsController extends BaseController<OrdLogisticsService, OrdLogisticsCompany>  {

    @ApiOperation(value = "物流公司-分页列表", notes = "物流公司分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "物流公司-列表", notes = "物流公司列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "物流公司-添加", notes = "添加物流公司")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "物流公司-更新", notes = "更新物流公司")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "物流公司-删除", notes = "删除物流公司")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "物流公司-详情", notes = "物流公司详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "物流公司-更新状态值", notes = "物流公司更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
            return bizService.setState(map, "state", state);
    }
}
