package com.meida.module.order.provider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrdInvoiceReceiver;
import com.meida.module.order.provider.service.OrdInvoiceReceiverService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 发票常用收件人控制器
 *
 * @author flyme
 * @date 2019-07-20
 */
@RestController
@Api(tags = "订单模块-发票常用收件人")
@RequestMapping("/invoice/receiver/")
public class OrdInvoiceReceiverController extends BaseController<OrdInvoiceReceiverService, OrdInvoiceReceiver> {

    @ApiOperation(value = "发票常用收件人-分页列表", notes = "发票常用收件人分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "发票常用收件人-列表", notes = "发票常用收件人列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "发票常用收件人-添加", notes = "添加发票常用收件人")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "发票常用收件人-更新", notes = "更新发票常用收件人")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "发票常用收件人-删除", notes = "删除发票常用收件人")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "发票常用收件人-详情", notes = "发票常用收件人详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
