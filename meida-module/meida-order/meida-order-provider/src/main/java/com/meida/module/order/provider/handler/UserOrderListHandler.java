package com.meida.module.order.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.constants.SettingConstant;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.provider.service.OrderDetailsService;
import com.meida.module.system.client.entity.SysCompanyInfo;
import com.meida.module.system.provider.service.SysCompanyInfoService;
import com.meida.module.system.provider.service.SysCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户订单列表
 *
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("userOrderListHandler")
public class UserOrderListHandler implements PageInterceptor<OrderInfo> {
    @Autowired
    private OrderDetailsService orderDetailsService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private SysCompanyService companyService;


    @Autowired
    private SysCompanyInfoService companyInfoService;

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        //未删除
        cq.eq(OrderInfo.class, "deleted", CommonConstants.DEL_NO);
        cq.eq(OrderInfo.class, "userId", userId);
    }

    @Override
    public void complete(CriteriaQuery<OrderInfo> cq, List<EntityMap> result, EntityMap extra) {
        for (EntityMap orderInfo : result) {
            Long orderId = orderInfo.getLong("orderId");
            Long companyId = orderInfo.getLong("companyId");
            Integer parentOrder = orderInfo.getInt("parentOrder");
            if (FlymeUtils.isNotEmpty(companyId)) {
                SysCompanyInfo companyInfo = companyInfoService.getById(companyId);
                orderInfo.put("managerId", companyInfo.getManagerId());
            }
            //查询订单明细
            List<EntityMap> orderDetailsList = orderDetailsService.listByOrderIdForList(orderId, parentOrder);

            orderInfo.put("orderDetailsList", orderDetailsList);
            if (FlymeUtils.isNotEmpty(orderDetailsList)) {
                Integer shopCount = orderDetailsService.coutShopIdCount(orderId);
                Integer evaluatedCount = 0;
                for (EntityMap entityMap : orderDetailsList) {
                    evaluatedCount += entityMap.getInt("evaluatedState", 0);
                }
                orderInfo.put("evaluatedCount", evaluatedCount);
                if (FlymeUtils.isNotEmpty(shopCount) && shopCount > 1) {
                    EntityMap map = redisUtils.getConfigMap("PLATFORM_CONFIG");
                    String platformName = map.get(SettingConstant.PLATFORM_NAME, "平台订单");
                    orderInfo.put("shopName", platformName);
                } else {
                    //取第一个订单明细的店铺名称
                    EntityMap orderDetails = orderDetailsList.get(0);
                    String shopName = orderDetails.get("shopName");
                    orderInfo.put("shopName", shopName);
                }
            }
        }
    }
}
