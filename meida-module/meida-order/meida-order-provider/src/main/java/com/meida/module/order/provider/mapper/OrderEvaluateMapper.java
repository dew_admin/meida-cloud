package com.meida.module.order.provider.mapper;

import com.meida.module.order.client.entity.OrderEvaluate;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 订单评价表  Mapper 接口
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderEvaluateMapper extends SuperMapper<OrderEvaluate> {

}
