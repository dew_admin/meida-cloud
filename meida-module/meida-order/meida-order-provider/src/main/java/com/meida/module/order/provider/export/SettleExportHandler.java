package com.meida.module.order.provider.export;

import com.meida.common.mybatis.interceptor.ExportInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.provider.service.OrderInfoService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 销售统计导出
 *
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("settleExportHandler")
public class SettleExportHandler implements ExportInterceptor<OrderInfo> {
    @Resource
    private OrderInfoService orderInfoService;

    @Override
    public ResultBody initData(Map params) {
        return orderInfoService.totalSale(params);
    }
}
