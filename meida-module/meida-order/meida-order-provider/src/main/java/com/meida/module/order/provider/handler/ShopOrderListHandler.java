package com.meida.module.order.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.client.entity.OrderLogistics;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("shopOrderListHandler")
public class ShopOrderListHandler implements PageInterceptor {
    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        //TODO 暂时用企业ID,后续要分门店
        cq.select(OrderLogistics.class, "receiveMan", "receiveTel", "receiveAddress");
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(OrderInfo.class, "companyId", companyId);
        cq.eq(OrderInfo.class, "deleted", CommonConstants.DEL_NO);
        cq.createJoin(OrderLogistics.class).setMainField("orderId").setJoinField("orderId");
    }

}
