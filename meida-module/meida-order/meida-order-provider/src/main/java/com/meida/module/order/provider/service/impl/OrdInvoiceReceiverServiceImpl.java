package com.meida.module.order.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.order.client.entity.OrdInvoiceReceiver;
import com.meida.module.order.provider.mapper.OrdInvoiceReceiverMapper;
import com.meida.module.order.provider.service.OrdInvoiceReceiverService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 发票主体 实现类
 *
 * @author flyme
 * @date 2019-07-20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrdInvoiceReceiverServiceImpl extends BaseServiceImpl<OrdInvoiceReceiverMapper, OrdInvoiceReceiver> implements OrdInvoiceReceiverService {

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforeListEntityMap(CriteriaQuery<OrdInvoiceReceiver> cq, OrdInvoiceReceiver ordInvoiceReceiver, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.eq(OrdInvoiceReceiver.class, "userId", userId);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs,OrdInvoiceReceiver ordInvoiceReceiver,EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        cs.put("userId", userId);
        return ResultBody.ok();
    }
}
