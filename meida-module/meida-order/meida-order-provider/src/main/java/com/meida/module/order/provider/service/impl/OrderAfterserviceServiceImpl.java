package com.meida.module.order.provider.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.order.client.entity.OrderAfterservice;
import com.meida.module.order.client.entity.OrderCause;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.provider.mapper.OrderAfterserviceMapper;
import com.meida.module.order.provider.service.OrderAfterserviceService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.order.provider.service.OrderCauseService;
import com.meida.module.order.provider.service.OrderDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 售后服务 实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrderAfterserviceServiceImpl extends BaseServiceImpl<OrderAfterserviceMapper, OrderAfterservice> implements OrderAfterserviceService {

    @Autowired
    private OrderCauseService causeService;
    @Autowired
    private OrderDetailsService detailsService;


    @Override
    public ResultBody beforePageList(CriteriaQuery<OrderAfterservice> cq, OrderAfterservice orderAfterservice, EntityMap requestMap) {
        cq.select(OrderAfterservice.class, "*");
        cq.select(OrderInfo.class, "orderNo");
        cq.select(OrderDetails.class, "productTitle", "productImages");
        cq.createJoin(OrderInfo.class);
        cq.createJoin(OrderDetails.class);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, OrderAfterservice orderAfterservice, EntityMap extra) {
        Long orderDetailsId = orderAfterservice.getOrderDetailsId();
        Long userId = OpenHelper.getUserId();
        ApiAssert.isNotEmpty("申请退货ID不能为空", orderDetailsId);
        OrderAfterservice check = findByOrderDetailsId(orderDetailsId);
        ApiAssert.isEmpty("请勿重复申请", check);
        OrderDetails orderDetails = detailsService.getById(orderDetailsId);
        orderAfterservice.setServiceState(0);
        orderAfterservice.setUserId(userId);
        orderAfterservice.setOrderId(orderDetails.getOrderId());
        orderAfterservice.setServiceNo(IdWorker.getIdStr());
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, OrderAfterservice orderAfterservice, EntityMap extra) {
        Long orderDetailsId = orderAfterservice.getOrderDetailsId();
        detailsService.setAfterSaleState(orderDetailsId, CommonConstants.ENABLED);
        return super.afterAdd(cs, orderAfterservice, extra);
    }

    @Override
    public ResultBody beforeGet(CriteriaQuery<OrderAfterservice> cq, OrderAfterservice orderAfterservice, EntityMap requestMap) {
        Long orderDetailsId = orderAfterservice.getOrderDetailsId();
        ApiAssert.isNotEmpty("申请退货ID不能为空", orderDetailsId);
        cq.eq(OrderAfterservice.class, "orderDetailsId", orderDetailsId);
        return ResultBody.ok();
    }

    @Override
    public void afterGet(CriteriaQuery cq, EntityMap result) {

        Long orderDetailsId = result.getLong("orderDetailsId");
        EntityMap orderDetails = detailsService.findByOrderDetailsId(orderDetailsId);
        Long causeId = result.getLong("causeId");
        OrderCause orderCause = causeService.getById(causeId);
        result.put("orderDetails", orderDetails);
        result.put("returnCauseTitle", orderCause.getReturnCauseTitle());

    }


    public OrderAfterservice findByOrderDetailsId(Long orderDetailsId) {
        CriteriaQuery cq = new CriteriaQuery(OrderAfterservice.class);
        cq.eq(true, "orderDetailsId", orderDetailsId);
        return getOne(cq);
    }

    @Override
    public EntityMap findByOrderId(Long orderId) {
        CriteriaQuery cq = new CriteriaQuery(OrderAfterservice.class);
        cq.eq(true, "orderId", orderId);
        return findOne(cq);
    }

    @Override
    public OrderAfterservice findEntityByOrderId(Long orderId) {
        CriteriaQuery cq = new CriteriaQuery(OrderAfterservice.class);
        cq.eq(true, "orderId", orderId);
        return getOne(cq);
    }

    @Override
    public Long countByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(OrderAfterservice.class);
        cq.eq(true, "userId", userId);
        return count(cq);
    }
}
