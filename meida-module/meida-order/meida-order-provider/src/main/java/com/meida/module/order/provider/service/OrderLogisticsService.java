package com.meida.module.order.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.order.client.entity.OrderLogistics;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 物流信息 接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderLogisticsService extends IBaseService<OrderLogistics> {

    /**
     * 查询订单物流信息
     *
     * @param orderId
     * @return
     */
    EntityMap findByOrderId(Long orderId);

    /**
     * 物流评分
     *
     * @param orderId
     * @param logisticsScore
     */
    void setLogisticsScore(Long orderId, Integer logisticsScore);

    /**
     * 保存订单收货信息
     *
     * @param addressId
     * @param orderId
     * @return
     */
    Boolean add(Long addressId, Long orderId);


    /**
     * 删除订单收货信息
     *
     * @param orderId
     * @return
     */
    Boolean delByOrderId(Long orderId);

}
