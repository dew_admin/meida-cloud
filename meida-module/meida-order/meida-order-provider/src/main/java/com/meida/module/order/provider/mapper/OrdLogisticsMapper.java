package com.meida.module.order.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.order.client.entity.OrdLogisticsCompany;

/**
 * 物流公司 Mapper 接口
 * @author flyme
 * @date 2020-01-01
 */
public interface OrdLogisticsMapper extends SuperMapper<OrdLogisticsCompany> {

}
