package com.meida.module.order.provider.controller;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrderInfo;
import com.meida.module.order.provider.service.OrderInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 订单控制器
 *
 * @author flyme
 * @date 2019-11-27
 */
@RestController
@RequestMapping("/order/")
@Api(tags = "订单模块-订单管理")
public class OrderInfoController extends BaseController<OrderInfoService, OrderInfo> {

    @ApiOperation(value = "用户订单列表", notes = "用户订单分页列表")
    @GetMapping(value = "listByUser")
    public ResultBody listByUser(@RequestParam(required = false) Map params) {
        //设置默认处理器
        String handlerName = params.getOrDefault(CommonConstants.HANDLER_NAME, "userOrderListHandler").toString();
        params.put("handlerName", handlerName);
        return bizService.pageList(params);
    }

    @ApiOperation(value = "商家订单列表", notes = "商家订单分页列表")
    @GetMapping(value = "listByShop")
    public ResultBody listByShop(@RequestParam(required = false) Map params) {
        //设置默认处理器
        String handlerName = params.getOrDefault(CommonConstants.HANDLER_NAME, "shopOrderListHandler").toString();
        params.put("handlerName", handlerName);
        return bizService.pageList(params);
    }

    @ApiOperation(value = "平台订单列表", notes = "平台订单分页列表")
    @GetMapping(value = "listAll")
    public ResultBody listAll(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "购物车商品结算确认", notes = "购物车商品结算确认(计算优惠金额,运费,设置收货地址,发票等)")
    @PostMapping(value = "confirm")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "couponUserIds", value = "优惠券ID", paramType = "form")
    })
    public ResultBody confirm(@RequestParam(required = false) Map params, String couponUserIds) {
        return bizService.confirm(params, couponUserIds);
    }


    @ApiOperation(value = "购物车结算提交订单", notes = "确认无误后提交创建订单,提交后删除购物车")
    @PostMapping(value = "shopCardOrderSub")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "addressId", value = "收货地址ID", paramType = "form"),
            @ApiImplicitParam(name = "invoiceId", value = "发票Id", paramType = "form"),
            @ApiImplicitParam(name = "leaveMsg", value = "买家留言", paramType = "form"),
            @ApiImplicitParam(name = "couponUserIds", value = "优惠券ID", paramType = "form")
    })
    public ResultBody shopCardOrderSub(@RequestParam(required = false) Map params, Long addressId, Long invoiceId, String couponUserIds) {
        params.put("addressId", addressId);
        params.put("invoiceId", invoiceId);
        params.put("couponUserIds", couponUserIds);
        return bizService.add(params);
    }

    @ApiOperation(value = "订单更新", notes = "更新订单")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "删除订单", notes = "删除订单,可通过handler扩展")
    @PostMapping(value = "delete")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", paramType = "form")
    })
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.deleteOrder(params);
    }


    @ApiOperation(value = "订单详情", notes = "订单详情,可通过handler扩展")
    @GetMapping(value = "get")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", paramType = "form")
    })
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "取消订单", notes = "取消订单,可通过handler扩展")
    @PostMapping(value = "cancel")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", paramType = "form")
    })
    public ResultBody cancel(@RequestParam(required = false) Map params) {
        return bizService.closeOrder(params);
    }

    @ApiOperation(value = "确认收货", notes = "确认收货,可通过handler扩展")
    @PostMapping(value = "received")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "订单ID", paramType = "form")
    })
    public ResultBody received(@RequestParam(required = false) Map params) {
        return bizService.receiveOrder(params);
    }


    @ApiOperation(value = "商品直接购买校验", notes = "商品直接购买校验")
    @PostMapping(value = "orderCheck")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "marketingId", value = "营销商品ID", paramType = "form")
    })
    public ResultBody orderCheck(@RequestParam(required = false) Map params, Long skuId, Long marketingId) {
        return bizService.check(params, marketingId);
    }


    @ApiOperation(value = "商品直接购买结算确认", notes = "营销商品结算确认(计算优惠金额,运费,设置收货地址,发票等)")
    @PostMapping(value = "orderConfirm")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "skuId", value = "规格ID", paramType = "form"),
            @ApiImplicitParam(name = "marketingId", value = "营销商品ID", paramType = "form"),
            @ApiImplicitParam(name = "couponUserIds", value = "优惠券ID", paramType = "form"),
            @ApiImplicitParam(name = "buyNum", value = "购买数量", paramType = "form")
    })
    public ResultBody orderConfirm(@RequestParam(required = false) Map params, Long skuId, Long marketingId, String couponUserIds, Integer buyNum) {
        return bizService.confirm(params, skuId, marketingId, buyNum, couponUserIds);
    }


    @ApiOperation(value = "商品直接购买提交订单", notes = "商品确认无误后提交创建订单")
    @PostMapping(value = "orderSub")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "addressId", value = "收货地址ID", paramType = "form"),
            @ApiImplicitParam(name = "skuId", value = "规格Id", paramType = "form"),
            @ApiImplicitParam(name = "marketingId", value = "营销商品Id", paramType = "form"),
            @ApiImplicitParam(name = "buyNum", value = "购买数量", paramType = "form"),
            @ApiImplicitParam(name = "leaveMsg", value = "买家留言", paramType = "form"),
            @ApiImplicitParam(name = "couponUserIds", value = "优惠券ID", paramType = "form")
    })
    public ResultBody orderSub(@RequestParam(required = false) Map params, Long addressId, Long invoiceId, Long skuId, Long marketingId, String couponUserIds, Integer buyNum, String leaveMsg) {
        return bizService.orderSub(params, addressId, invoiceId, couponUserIds, skuId, marketingId, buyNum, leaveMsg);
    }


    @ApiOperation(value = "商品直接购买结算时优惠券列表", notes = "商品直接购买结算时优惠券列表")
    @GetMapping(value = "couponList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "skuId", value = "规格Id", paramType = "form"),
            @ApiImplicitParam(name = "buyNum", value = "购买数量", paramType = "form")
    })
    public ResultBody couponList(@RequestParam(required = false) Map params, Long skuId, Integer buyNum) {
        Long userId = OpenHelper.getUserId();
        EntityMap list = bizService.couponList(skuId, buyNum, userId);
        return ResultBody.ok(list);
    }


    @ApiOperation(value = "各个状态订单数量统计", notes = "各个状态订单数量统计")
    @GetMapping(value = "countByOrderState")
    public ResultBody countByOrderState() {
        Long companyId = OpenHelper.getCompanyId();
        EntityMap map = bizService.countOrderStateByShopId(companyId);
        return ResultBody.ok(map);
    }


    /**
     * 单个门店销售额明细
     *
     * @param params
     * @return
     */
    @GetMapping(value = "totalSaleByShop")
    public ResultBody totalSaleByShop(@RequestParam(required = false) Map params) {
        return bizService.totalSaleByShop(params);
    }

    /**
     * 按门店统计销售额
     *
     * @param params
     * @return
     */
    @GetMapping(value = "totalSale")
    public ResultBody totalSale(@RequestParam(required = false) Map params) {
        return bizService.totalSale(params);
    }


    /**
     * 导出销售统计
     *
     * @param params
     * @param request
     * @param response
     */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "销售统计", "销售统计表", "settleExportHandler");
    }

    /**
     * 按年月周日类型统计销售额和订单量
     *
     * @param params
     * @return
     */
    @GetMapping(value = "totalSaleByType")
    public ResultBody totalSaleByType(@RequestParam(required = false) Map params) {
        return bizService.totalSaleByType(params);
    }

    /**
     * 订单数据汇总
     *
     * @param params
     * @return
     */
    @GetMapping(value = "getTotalData")
    public ResultBody getTotalData(@RequestParam(required = false) Map params) {
        return bizService.getTotalData(params);
    }

    /**
     * 按商品分类汇总销售情况
     *
     * @param params
     * @return
     */
    @GetMapping(value = "getSaleByCateGory")
    public ResultBody getSaleByCateGory(@RequestParam(required = false) Map params) {
        return bizService.getSaleByCateGory(params);
    }
}
