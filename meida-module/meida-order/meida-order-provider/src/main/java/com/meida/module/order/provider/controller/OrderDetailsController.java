package com.meida.module.order.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.order.client.entity.OrderDetails;
import com.meida.module.order.provider.service.OrderDetailsService;


/**
 * 订单明细表 控制器
 *
 * @author flyme
 * @date 2019-11-27
 */
@RestController
@RequestMapping("/order/details/")
@Api(tags = "订单模块-订单明细")
public class OrderDetailsController extends BaseController<OrderDetailsService, OrderDetails> {


    @ApiOperation(value = "订单明细表 -列表", notes = "订单明细表 列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "订单明细表 -添加", notes = "添加订单明细表 ")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "订单明细表 -更新", notes = "更新订单明细表 ")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "订单明细表 -删除", notes = "删除订单明细表 ")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "订单明细表 -详情", notes = "订单明细表 详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "查看订单明细评价信息", notes = "查看订单明细评价信息")
    @GetMapping(value = "commentInfo")
    public ResultBody commentInfo(@RequestParam(required = false) Map params) {
        return bizService.getCommentInfo(params);
    }

}
