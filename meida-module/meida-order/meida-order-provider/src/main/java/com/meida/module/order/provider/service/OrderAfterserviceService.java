package com.meida.module.order.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.order.client.entity.OrderAfterservice;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 售后服务 接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface OrderAfterserviceService extends IBaseService<OrderAfterservice> {

    /**
     * 根据订单id查询
     *
     * @param orderId
     * @return
     */
    EntityMap findByOrderId(Long orderId);

    /**
     * 根据订单id查询
     *
     * @param orderId
     * @return
     */
    OrderAfterservice findEntityByOrderId(Long orderId);

    Long countByUserId(Long userId);

}
