package com.meida.module.order.provider.service.impl;

import cn.hutool.http.HttpUtil;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.JsonUtils;
import com.meida.module.order.client.entity.OrderLogistics;
import com.meida.module.order.provider.mapper.OrderLogisticsMapper;
import com.meida.module.order.provider.service.OrderInfoService;
import com.meida.module.order.provider.service.OrderLogisticsService;
import com.meida.module.system.client.entity.SysAddress;
import com.meida.module.order.client.entity.OrdLogisticsCompany;
import com.meida.module.system.provider.service.SysAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 物流信息 实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrderLogisticsServiceImpl extends BaseServiceImpl<OrderLogisticsMapper, OrderLogistics> implements OrderLogisticsService {

    @Autowired
    private SysAddressService addressService;
    @Autowired
    private OrderInfoService orderInfoService;

    @Override
    public EntityMap findByOrderId(Long orderId) {
        CriteriaQuery cq = new CriteriaQuery(OrderLogistics.class);
        cq.select(OrderLogistics.class, "*");
        cq.select(OrdLogisticsCompany.class, "logisticsName");
        cq.createJoin(OrdLogisticsCompany.class);
        cq.eq(true, "orderId", orderId);
        return findOne(cq);
    }

    @Override
    public void setLogisticsScore(Long orderId, Integer logisticsScore) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set(true, "logisticsScore", logisticsScore);
        cu.eq(true, "logisticsScore", 0);
        cu.eq(true, "orderId", orderId);
        update(cu);
    }

    /**
     * 添加物流信息
     *
     * @param addressId
     * @param orderId
     * @return
     */
    @Override
    public Boolean add(Long addressId, Long orderId) {
        SysAddress address = addressService.getById(addressId);
        Boolean tag = false;
        if (FlymeUtils.isNotEmpty(address)) {
            OrderLogistics orderLogistics = new OrderLogistics();
            orderLogistics.setOrderId(orderId);
            orderLogistics.setLogisticsScore(CommonConstants.INT_0);
            orderLogistics.setLogisticsState(CommonConstants.DISABLED);
            orderLogistics.setReceiveAddress(address.getAddressInfo());
            orderLogistics.setReceiveMan(address.getUserName());
            orderLogistics.setAreaName(address.getAreaName());
            orderLogistics.setReceiveTel(address.getMobile());
            tag = save(orderLogistics);
        }
        return tag;
    }

    @Override
    public Boolean delByOrderId(Long orderId) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq(true, "orderId", orderId);
        return remove(cd);
    }

    @Override
    public ResultBody beforeGet(CriteriaQuery<OrderLogistics> cq, OrderLogistics orderLogistics, EntityMap requestMap) {
        Long orderId = orderLogistics.getOrderId();
        ApiAssert.isNotEmpty("订单ID不能为空", orderId);
        cq.eq(true, "orderId", orderId);
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, OrderLogistics orderLogistics,EntityMap extra) {
        Long logisticsId = orderLogistics.getLogisticsId();
        String logisticsNo = orderLogistics.getLogisticsNo();
        if (FlymeUtils.isNotEmpty(logisticsNo)) {
            ApiAssert.isNotEmpty("物流公司不能为空", logisticsId);
        }
        if (FlymeUtils.isNotEmpty(logisticsId)) {
            ApiAssert.isNotEmpty("物流单号不能为空", logisticsNo);
            //修改订单为已发货
            orderInfoService.setLogisticsState(orderLogistics.getOrderId());
        }
        return ResultBody.ok();
    }

    public static void main(String[] args) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("resultv2", 1);
        paramMap.put("text", "773006970956741");
        String result = HttpUtil.get("https://www.kuaidi100.com/autonumber/autoComNum", paramMap);

        Map<String, Object> map = JsonUtils.jsonToMap(result, true);
        Object auto = map.get("auto");
        if (FlymeUtils.isNotEmpty(auto)) {
            List<Map> list = JsonUtils.json2list(auto.toString(), Map.class);
            if (FlymeUtils.isNotEmpty(list)) {
                Object comCode = list.get(0).get("comCode");

                String result1 = HttpUtil.get("https://www.kuaidi100.com/query?type=yunda&postid=4300623877954&temp=0.30106187173860754&phone=", paramMap);
                System.out.println(result1);

            }
        }
        System.out.println(map.get("auto"));
    }
}
