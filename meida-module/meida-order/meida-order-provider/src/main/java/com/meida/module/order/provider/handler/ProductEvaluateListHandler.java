package com.meida.module.order.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.order.client.entity.OrderEvaluate;
import org.springframework.stereotype.Component;

/**
 * 单个商品评价列表
 *
 * @author flyme
 * @date 2019/11/28 14:50
 */
@Component("productEvaluateListHandler")
public class ProductEvaluateListHandler implements PageInterceptor {
    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long productId = params.getLong("productId");
        cq.eq(OrderEvaluate.class, "productId", productId);
    }

}
