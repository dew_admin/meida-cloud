/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:32:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_cause
-- ----------------------------
DROP TABLE IF EXISTS `order_cause`;
CREATE TABLE `order_cause`  (
  `returnCauseId` bigint(20) NOT NULL COMMENT '主键',
  `returnCauseTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`returnCauseId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '退款方式' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
