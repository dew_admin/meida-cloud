/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:33:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_shopcart
-- ----------------------------
DROP TABLE IF EXISTS `order_shopcart`;
CREATE TABLE `order_shopcart`  (
  `shopCardId` bigint(20) NOT NULL COMMENT '主键',
  `productId` bigint(20) NULL DEFAULT NULL COMMENT '商品ID',
  `skuId` bigint(20) NULL DEFAULT NULL COMMENT '规格ID',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `buyNum` int(11) NULL DEFAULT NULL COMMENT '购买数量',
  `selected` int(1) NULL DEFAULT NULL COMMENT '选中状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`shopCardId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '购物车' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
