/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:33:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `order_evaluate`;
CREATE TABLE `order_evaluate`  (
  `orderEvaluateId` bigint(20) NOT NULL COMMENT '主键',
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单ID',
  `orderDetailsId` bigint(20) NULL DEFAULT NULL COMMENT '订单明细ID',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `productId` bigint(20) NULL DEFAULT NULL COMMENT '产品ID',
  `evaluateUserId` bigint(20) NULL DEFAULT NULL COMMENT '评价人',
  `describeScore` int(11) NULL DEFAULT NULL COMMENT '描述评分',
  `evaluateContent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `evaluateImages` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评价图片',
  `evaluateScore` int(11) NULL DEFAULT NULL COMMENT '服务评分',
  `evaluateState` int(11) NULL DEFAULT NULL COMMENT '是否删除 0：已删除，1未删除',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`orderEvaluateId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单评价表 ' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
