/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:32:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details`  (
  `orderDetailsId` bigint(20) NOT NULL COMMENT '主键',
  `parentOrderId` bigint(20) NULL DEFAULT NULL COMMENT '父订单ID',
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单Id',
  `productId` bigint(32) NULL DEFAULT NULL COMMENT '商品ID',
  `marketingType` int(255) NULL DEFAULT NULL COMMENT '营销类型',
  `marketingId` bigint(20) NULL DEFAULT NULL COMMENT '营销商品ID',
  `skuId` bigint(32) NULL DEFAULT NULL COMMENT '规格ID',
  `shopId` bigint(32) NULL DEFAULT NULL COMMENT '店铺Id',
  `companyId` bigint(32) NULL DEFAULT NULL COMMENT '企业Id',
  `productTitle` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品标题',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '买家ID',
  `productImages` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `skuTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所选规格',
  `categoryId1` bigint(20) NULL DEFAULT NULL COMMENT '商品一级分类',
  `categoryId2` bigint(20) NULL DEFAULT NULL COMMENT '商品二级分类',
  `categoryId3` bigint(20) NULL DEFAULT NULL COMMENT '商品三级分类',
  `productPrice` decimal(11, 2) NULL DEFAULT NULL COMMENT '商品价格',
  `productNum` int(11) NULL DEFAULT NULL COMMENT '商品数量',
  `productAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品价格',
  `evaluatedState` int(11) NULL DEFAULT NULL COMMENT '评价状态',
  `afterSaleState` int(11) NULL DEFAULT NULL COMMENT '售后状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`orderDetailsId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单明细表 ' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
