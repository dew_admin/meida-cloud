/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:32:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_afterservice
-- ----------------------------
DROP TABLE IF EXISTS `order_afterservice`;
CREATE TABLE `order_afterservice`  (
  `afterServiceId` bigint(20) NOT NULL COMMENT '主键',
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单ID',
  `orderDetailsId` bigint(20) NULL DEFAULT NULL COMMENT '订单明细ID',
  `serviceType` int(11) NULL DEFAULT NULL COMMENT '1:维修2:退货3:换货',
  `causeId` bigint(20) NULL DEFAULT NULL COMMENT '申请原因',
  `serviceNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务单号',
  `serviceDesc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '申请描述',
  `serviceImages` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `returnAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '退款金额',
  `returnType` int(11) NULL DEFAULT NULL COMMENT '退款方式',
  `returnAddress` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退回地址',
  `linkMan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `linkTel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `serviceState` int(255) NULL DEFAULT NULL COMMENT '审批状态',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`afterServiceId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '售后服务' ROW_FORMAT = Dynamic;


INSERT INTO `base_menu` VALUES (452, 450, 'afterservice', '售后服务', '', '/', 'order/afterservice/index', 'insurance', '_self', 'PageView', 1, 2, 1, 0, 'meida-base-provider', '2020-01-02 12:37:20', '2020-01-04 17:33:48');
INSERT INTO `base_authority` VALUES (452, 'MENU_afterservice', 452, NULL, NULL, 1, '2020-01-02 12:37:20', '2020-01-04 17:33:48');

SET FOREIGN_KEY_CHECKS = 1;
