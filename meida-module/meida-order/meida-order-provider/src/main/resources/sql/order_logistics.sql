/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:33:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_logistics
-- ----------------------------
DROP TABLE IF EXISTS `order_logistics`;
CREATE TABLE `order_logistics`  (
  `orderLogisticsId` bigint(20) NOT NULL COMMENT '主键',
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单Id',
  `logisticsId` bigint(20) NULL DEFAULT NULL COMMENT '物流公司ID',
  `logisticsNo` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `logisticsScore` int(11) NULL DEFAULT NULL COMMENT '物流评分',
  `logisticsState` int(11) NULL DEFAULT NULL COMMENT '物流状态',
  `receiveAddress` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `receiveMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `receiveTel` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收件人电话',
  `sellerMsg` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '卖家留言',
  `sellerMan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '卖家姓名',
  `sellerTel` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '卖家联系方式',
  `areaName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `sendAddress` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发货地址',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`orderLogisticsId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物流信息' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (454, 450, 'logistics', '物流公司', '', '/', 'system/logistics/index', 'rocket', '_self', 'PageView', 1, 4, 1, 0, 'meida-base-provider', '2020-01-08 23:33:58', '2020-01-09 11:28:35');
INSERT INTO `base_authority` VALUES (454, 'MENU_logistics', 454, NULL, NULL, 1, '2020-01-08 23:33:58', '2020-01-09 11:28:35');

SET FOREIGN_KEY_CHECKS = 1;
