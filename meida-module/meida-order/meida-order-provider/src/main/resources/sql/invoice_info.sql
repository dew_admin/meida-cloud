/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:36:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for invoice_info
-- ----------------------------
DROP TABLE IF EXISTS `invoice_info`;
CREATE TABLE `invoice_info`  (
  `invoiceId` bigint(20) NOT NULL COMMENT '主键',
  `invoiceTitle` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发票抬头',
  `creditCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业税号',
  `invoiceType` int(11) NULL DEFAULT NULL COMMENT '发票类型 1：个人发票2：企业发票',
  `invoiceBody` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发票内容',
  `invoiceAmount` decimal(32, 2) NULL DEFAULT NULL COMMENT '开票金额',
  `electron` int(11) NULL DEFAULT NULL COMMENT '电子发票',
  `consignee` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业地址',
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单id',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '所属企业',
  `invoiceReceiverId` bigint(20) NULL DEFAULT NULL COMMENT '发票主体ID',
  `invoiceKind` int(11) NULL DEFAULT NULL COMMENT '发票种类 （1普通发票2专用发票）',
  `invoiceNo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方发票请求流水号',
  `telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `expressCompany` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `expressNo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递单号',
  `invoiceState` int(11) NULL DEFAULT NULL COMMENT '开票状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`invoiceId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '发票' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (453, 450, 'invoice', '发票管理', '', '/', 'order/invoice/index', 'audit', '_self', 'PageView', 1, 3, 1, 0, 'meida-base-provider', '2020-01-02 15:30:11', '2020-01-05 15:53:52');
INSERT INTO `base_authority` VALUES (453, 'MENU_invoice', 453, NULL, NULL, 1, '2020-01-02 15:30:11', '2020-01-05 15:53:52');

SET FOREIGN_KEY_CHECKS = 1;
