/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:39:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ord_logistics_company
-- ----------------------------
DROP TABLE IF EXISTS `ord_logistics_company`;
CREATE TABLE `ord_logistics_company`  (
  `logisticsId` bigint(20) NOT NULL COMMENT '主键',
  `logisticsName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流公司名称',
  `logisticsNo` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流编码',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`logisticsId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物流公司' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ord_logistics_company
-- ----------------------------
INSERT INTO `ord_logistics_company` VALUES (1, '顺丰', 'SF', NULL, '2020-01-09 00:11:48');
INSERT INTO `ord_logistics_company` VALUES (1214943059889303553, '圆通', 'YT', '2020-01-09 00:12:52', NULL);
INSERT INTO `ord_logistics_company` VALUES (1215135448975757314, '天天快递', 'tiantian', '2020-01-09 12:57:21', NULL);
INSERT INTO `ord_logistics_company` VALUES (1215273676332568578, '申通', 'ST', '2020-01-09 22:06:37', NULL);
INSERT INTO `ord_logistics_company` VALUES (1215273746037706753, '韵达', 'yunda', '2020-01-09 22:06:54', NULL);

SET FOREIGN_KEY_CHECKS = 1;
