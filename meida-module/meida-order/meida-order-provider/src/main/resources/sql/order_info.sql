/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 11:33:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`  (
  `orderId` bigint(20) NOT NULL COMMENT '主键',
  `orderTitle` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单标题',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `orderNo` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父订单ID',
  `parentOrder` int(11) NULL DEFAULT NULL COMMENT '是否是母单',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `orderType` int(11) NULL DEFAULT NULL COMMENT '订单类型',
  `productCount` int(255) NULL DEFAULT NULL COMMENT '商品件数',
  `userCouponId` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优惠券ID',
  `freightAmount` decimal(20, 2) NULL DEFAULT NULL COMMENT '运费',
  `totalAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单金额',
  `couponAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠金额',
  `discountAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣金额',
  `payAmount` decimal(10, 2) NULL DEFAULT NULL COMMENT '应支付金额',
  `showState` int(255) NULL DEFAULT NULL COMMENT '订单是否展示1:展示 2:不展示',
  `payInfoId` bigint(20) NULL DEFAULT NULL COMMENT '支付信息ID',
  `selfState` int(11) NULL DEFAULT NULL COMMENT '是否自营',
  `shopScore` int(11) NULL DEFAULT NULL COMMENT '商家评分',
  `orderState` int(11) NULL DEFAULT NULL COMMENT '订单状态',
  `payTime` datetime(0) NULL DEFAULT NULL COMMENT '付款日期',
  `receiveAddressId` bigint(20) NULL DEFAULT NULL COMMENT '买家收货地址ID',
  `payState` int(11) NULL DEFAULT NULL COMMENT '支付状态',
  `payType` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付方式',
  `deleted` int(255) NULL DEFAULT NULL COMMENT '删除状态0:已删除,1:未删除',
  `leaveMsg` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '买家留言',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`orderId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (450, 0, 'order', '订单管理', '', '/', '/', 'profile', '_self', 'PageView', 1, 4, 1, 0, 'meida-base-provider', '2019-12-31 19:02:54', '2020-01-04 17:58:30');
INSERT INTO `base_menu` VALUES (451, 450, 'menu_order', '订单管理', '', '/', 'order/product/index', 'appstore', '_self', 'PageView', 1, 1, 1, 0, 'meida-base-provider', '2019-12-31 19:05:08', '2020-01-04 17:33:43');

INSERT INTO `base_menu` VALUES (550, 0, 'finance', '财务管理', '', '/', '', 'bars', '_self', 'PageView', 1, 5, 1, 0, 'meida-base-provider', '2020-02-16 18:24:39', '2020-02-16 18:25:12');
INSERT INTO `base_menu` VALUES (551, 550, 'saleTotal', '销售统计', '', '/', 'finance/settle/index', 'bars', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2020-02-16 18:26:35', NULL);

INSERT INTO `base_authority` VALUES (550, 'MENU_finance', 550, NULL, NULL, 1, '2020-02-16 18:24:39', '2020-02-16 18:25:12');
INSERT INTO `base_authority` VALUES (551, 'MENU_saleTotal', 551, NULL, NULL, 1, '2020-02-16 18:26:35', NULL);


INSERT INTO `base_authority` VALUES (450, 'MENU_order', 450, NULL, NULL, 1, '2019-12-31 19:02:54', '2020-01-04 17:58:30');
INSERT INTO `base_authority` VALUES (451, 'MENU_menu_order', 451, NULL, NULL, 1, '2019-12-31 19:05:08', '2020-01-04 17:33:43');





SET FOREIGN_KEY_CHECKS = 1;
