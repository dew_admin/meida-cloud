package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

import java.math.BigDecimal;

/**
 * 订单明细表
 *
 * @author flyme
 * @date 2019-11-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_details")
@TableAlias("details")
@ApiModel(value = "OrderDetails对象", description = "订单明细表 ")
public class OrderDetails extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "orderDetailsId", type = IdType.ASSIGN_ID)
    private Long orderDetailsId;

    @ApiModelProperty(value = "父订单ID")
    private Long parentOrderId;

    @ApiModelProperty(value = "订单Id")
    private Long orderId;

    @ApiModelProperty(value = "商品ID")
    private Long productId;

    @ApiModelProperty(value = "商品一级分类")
    private Long categoryId1;

    @ApiModelProperty(value = "商品二级分类")
    private Long categoryId2;

    @ApiModelProperty(value = "商品三级分类")
    private Long categoryId3;

    @ApiModelProperty(value = "营销商品ID")
    private Long marketingId;

    @ApiModelProperty(value = "营销商品类型")
    private Integer marketingType;

    @ApiModelProperty(value = "规格ID")
    private Long skuId;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "店铺Id")
    private Long shopId;

    @ApiModelProperty(value = "企业Id")
    private Long companyId;

    @ApiModelProperty(value = "商品标题")
    private String productTitle;




    @ApiModelProperty(value = "商品图片")
    private String productImages;


    @ApiModelProperty(value = "所选规格")
    private String skuTitle;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "商品数量")
    private Integer productNum;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal productAmount;

    @ApiModelProperty(value = "评价状态")
    private Integer evaluatedState;

    @ApiModelProperty(value = "售后状态")
    private Integer afterSaleState;

}
