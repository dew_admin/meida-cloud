package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 物流信息
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_logistics")
@TableAlias("logistics")
@ApiModel(value="OrderLogistics对象", description="物流信息")
public class OrderLogistics extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "orderLogisticsId", type = IdType.ASSIGN_ID)
    private Long orderLogisticsId;

    @ApiModelProperty(value = "订单Id")
    private Long orderId;

    @ApiModelProperty(value = "物流公司ID")
    private Long logisticsId;

    @ApiModelProperty(value = "物流单号")
    private String logisticsNo;

    @ApiModelProperty(value = "物流评分")
    private Integer logisticsScore;

    @ApiModelProperty(value = "物流状态")
    private Integer logisticsState;

    @ApiModelProperty(value = "收货地址")
    private String receiveAddress;

    @ApiModelProperty(value = "收货地址")
    private String areaName;

    @ApiModelProperty(value = "收件人")
    private String receiveMan;

    @ApiModelProperty(value = "收件人电话")
    private String receiveTel;

    @ApiModelProperty(value = "卖家留言")
    private String sellerMsg;

    @ApiModelProperty(value = "卖家姓名")
    private String sellerMan;

    @ApiModelProperty(value = "卖家联系方式")
    private String sellerTel;

    @ApiModelProperty(value = "发货地址")
    private String sendAddress;

}
