package com.meida.module.order.client.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author flyme
 * @date 2020/1/8 15:31
 */
@Data
public class LeaveMsg implements Serializable {
    private String leaveMsg;
    private Long shopId;
}
