package com.meida.module.order.client.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 订单表
 *
 * @author flyme
 * @date 2019-11-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_info")
@TableAlias("orderinfo")
@ApiModel(value = "OrderInfo对象", description = "订单表")
public class OrderInfo extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "orderId", type = IdType.ASSIGN_ID)
    private Long orderId;

    @ApiModelProperty(value = "订单标题")
    private String orderTitle;

    @ApiModelProperty(value = "店铺ID")
    private Long shopId;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "父订单ID")
    private Long parentId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "用户ID")
    private Long userId;


    @ApiModelProperty(value = "订单类型:1,普通订单2营销订单")
    private Integer orderType;

    @ApiModelProperty(value = "优惠券ID")
    private String userCouponId;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "运费")
    private BigDecimal freightAmount;


    @ApiModelProperty(value = "商品件数")
    private Integer productCount;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal couponAmount;

    @ApiModelProperty(value = "折扣金额")
    private BigDecimal discountAmount;

    @ApiModelProperty(value = "应支付金额")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "支付信息ID")
    private Long payInfoId;

    @ApiModelProperty(value = "是否自营")
    private Integer selfState;

    @ApiModelProperty(value = "订单是否展示1:展示 0:不展示")
    private Integer showState;

    @ApiModelProperty(value = "订单是否是母单1:是 0:不是")
    private Integer parentOrder;

    @ApiModelProperty(value = "商家评分")
    private Integer shopScore;

    @ApiModelProperty(value = "支付方式")
    private Integer payType;

    @ApiModelProperty(value = "卖家留言")
    private String leaveMsg;

    @ApiModelProperty(value = "卖家收货地址")
    private Long receiveAddressId;

    @ApiModelProperty(value = "订单状态")
    private Integer orderState;

    @ApiModelProperty(value = "付款日期")
    private Date payTime;

    @ApiModelProperty(value = "支付状态")
    private Integer payState;



    @ApiModelProperty(value = "删除状态")
    private Integer deleted;

}
