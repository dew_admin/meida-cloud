package com.meida.module.order.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * @author zyf
 */

public enum ShopCarTypeEnum {
    /**
     * 增加购物车商品
     */
    CAR_ADD("add", "增加购物车商品"),
    /**
     * 减少购物车商品
     */
    CAR_SUB("sub", "减少购物车商品"),
    /***
     * 价格
     * */
    CAR_SET("set", "设置购物车商品数量");


    @EnumValue
    private String code;
    private String name;

    private ShopCarTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    ShopCarTypeEnum() {

    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }
}
