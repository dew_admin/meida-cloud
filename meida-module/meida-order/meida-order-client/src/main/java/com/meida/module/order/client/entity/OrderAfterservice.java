package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 售后服务
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_afterservice")
@TableAlias("afterservice")
@ApiModel(value="OrderAfterservice对象", description="售后服务")
public class OrderAfterservice extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "afterServiceId", type = IdType.ASSIGN_ID)
    private Long afterServiceId;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;


    @ApiModelProperty(value = "订单明细ID")
    private Long orderDetailsId;

    @ApiModelProperty(value = "1:维修2:退货3:换货")
    private Integer serviceType;

    @ApiModelProperty(value = "申请原因")
    private Long causeId;

    @ApiModelProperty(value = "服务单号")
    private String serviceNo;

    @ApiModelProperty(value = "申请描述")
    private String serviceDesc;

    @ApiModelProperty(value = "图片")
    private String serviceImages;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal returnAmount;

    @ApiModelProperty(value = "退款方式")
    private Integer returnType;

    @ApiModelProperty(value = "退回地址")
    private String returnAddress;

    @ApiModelProperty(value = "联系人")
    private String linkMan;

    @ApiModelProperty(value = "联系方式")
    private String linkTel;

    @ApiModelProperty(value = "服务审批状态")
    private Integer serviceState;

    @ApiModelProperty(value = "申请人")
    private Long userId;

}
