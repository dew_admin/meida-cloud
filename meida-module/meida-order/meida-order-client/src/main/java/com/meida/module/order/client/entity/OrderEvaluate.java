package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 订单评价表 
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_evaluate")
@TableAlias("evaluate")
@ApiModel(value="OrderEvaluate对象", description="订单评价表 ")
public class OrderEvaluate extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "orderEvaluateId", type = IdType.ASSIGN_ID)
    private Long orderEvaluateId;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "商品ID")
    private Long productId;

    @ApiModelProperty(value = "订单明细ID")
    private Long orderDetailsId;


    @ApiModelProperty(value = "店铺ID")
    private Long shopId;

    @ApiModelProperty(value = "评价人")
    private Long evaluateUserId;

    @ApiModelProperty(value = "评价内容")
    private String evaluateContent;

    @ApiModelProperty(value = "评价图片")
    private String evaluateImages;

    @ApiModelProperty(value = "描述评分")
    private Integer describeScore;


    @ApiModelProperty(value = "商品评分")
    private Integer evaluateScore;

    @ApiModelProperty(value = "是否删除 0：已删除，1未删除")
    private Integer evaluateState;

}
