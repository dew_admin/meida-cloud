package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 发票主体
 *
 * @author flyme
 * @date 2019-07-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("invoice_receiver")
@TableAlias("oir")
@ApiModel(value = "OrdInvoiceReceiver对象", description = "发票收件人列表")
public class OrdInvoiceReceiver extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "invoiceReceiverId", type = IdType.ASSIGN_ID)
    private Long invoiceReceiverId;

    @ApiModelProperty(value = "收件人")
    private String userName;

    @ApiModelProperty(value = "收件地址")
    private String address;

    @ApiModelProperty(value = "联系方式")
    private String telephone;

    @ApiModelProperty(value = "开户行")
    private String bankName;

    @ApiModelProperty(value = "类型")
    private Integer invoiceType;

    @ApiModelProperty(value = "卡号")
    private String bankNo;

    @ApiModelProperty(value = "企业税号")
    private String creditCode;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "企业")
    private Long companyId;

    @ApiModelProperty(value = "企业固话")
    private String enterpriseTel;

    @ApiModelProperty(value = "企业地址")
    private String enterpriseAddress;

}
