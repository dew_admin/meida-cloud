package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 申请售后原因
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("order_cause")
@TableAlias("cause")
@ApiModel(value="OrderCause对象", description="退款方式")
public class OrderCause extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "returnCauseId", type = IdType.ASSIGN_ID)
    private Long returnCauseId;

    @ApiModelProperty(value = "标题")
    private String returnCauseTitle;

    @ApiModelProperty(value = "状态")
    private Integer state;

}
