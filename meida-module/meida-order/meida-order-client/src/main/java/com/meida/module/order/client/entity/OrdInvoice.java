package com.meida.module.order.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 发票
 *
 * @author flyme
 * @date 2019-06-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("invoice_info")
@TableAlias("invoice")
@ApiModel(value = "OrdInvoice对象", description = "发票")
public class OrdInvoice extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "invoiceId", type = IdType.ASSIGN_ID)
    private Long invoiceId;

    @ApiModelProperty(value = "发票抬头")
    private String invoiceTitle;

    @ApiModelProperty(value = "发票类型 1：个人发票2：企业发票")
    private Integer invoiceType;

    @ApiModelProperty(value = "企业税号")
    private String creditCode;

    @ApiModelProperty(value = "发票内容")
    private String invoiceBody;

    @ApiModelProperty(value = "开票金额")
    private BigDecimal invoiceAmount;

    @ApiModelProperty(value = "发票类型 0纸质，1：电子")
    private Integer electron;


    @ApiModelProperty(value = "邮寄地址")
    private String address;

    @ApiModelProperty(value = "收件人")
    private String consignee;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "发票主体")
    private Long invoiceReceiverId;

    @ApiModelProperty(value = "所属企业")
    private Long companyId;


    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "发票种类 （1普通发票2专用发票）")
    private Integer invoiceKind;

    @ApiModelProperty(value = "第三方发票请求流水号")
    private String invoiceNo;

    @ApiModelProperty(value = "快递公司")
    private String expressCompany;

    @ApiModelProperty(value = "快递单号")
    private String expressNo;

    @ApiModelProperty(value = "开票状态 0：未开 1：已开")
    private Integer invoiceState;

    @ApiModelProperty(value = "备注")
    private String remark;

}
