/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:04:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dictdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictdata`;
CREATE TABLE `sys_dictdata`  (
  `dictDataId` bigint(20) NOT NULL COMMENT '主键',
  `dicDataTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典值key',
  `dicDataValue` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典值',
  `dictId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属字典',
  `dictDataType` int(255) NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  `allowDel` int(11) NULL DEFAULT NULL,
  `sortOrder` decimal(10, 2) NULL DEFAULT NULL COMMENT '排序',
  `createTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updateTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dictDataId`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
