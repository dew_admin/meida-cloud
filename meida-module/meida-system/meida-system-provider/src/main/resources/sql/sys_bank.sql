/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 06/08/2021 14:47:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_bank
-- ----------------------------
DROP TABLE IF EXISTS `sys_bank`;
CREATE TABLE `sys_bank`  (
  `bankId` bigint(20) NOT NULL COMMENT '主键',
  `bankName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开户行',
  `bankNo` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账户',
  `bankOwner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开户人',
  `organizationId` bigint(20) NULL DEFAULT NULL COMMENT '机构ID',
  `bankAddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开户地址',
  `bankState` int(1) NULL DEFAULT NULL COMMENT '状态',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`bankId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '银行信息' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
