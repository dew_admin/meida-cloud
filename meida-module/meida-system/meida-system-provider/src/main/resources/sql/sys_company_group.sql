/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:03:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_company_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_company_group`;
CREATE TABLE `sys_company_group`  (
  `gcId` bigint(20) NULL DEFAULT NULL COMMENT '主键',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '目标企业ID',
  `companyName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业名称',
  `groupId` bigint(20) NULL DEFAULT NULL COMMENT '分组ID',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '所属用户ID',
  `ownerCompanyId` bigint(20) NULL DEFAULT NULL COMMENT '所属企业ID',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '企业分组中间表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
