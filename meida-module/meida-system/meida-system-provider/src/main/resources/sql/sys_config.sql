/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 14:36:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `configId` bigint(20) NOT NULL COMMENT '主键',
  `configKey` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '键',
  `configVal` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `configType` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置类型',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改日期',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`configId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'OSS_USED', 'ALI_OSS', 'oss', '2020-03-30 15:13:16', '2020-02-13 10:57:27', 'OSS存储方式');
INSERT INTO `sys_config` VALUES (2, 'LOCAL_OSS', '{\"changed\":true,\"endpoint\":\"admin.hswlm.cn/api/common/file/view\",\"filePath\":\"c:/upload\",\"http\":\"http://\",\"serviceName\":\"LOCAL_OSS\"}', 'oss', '2020-03-30 15:00:16', '2020-02-13 10:57:27', '本地存储配置');
INSERT INTO `sys_config` VALUES (3, 'ALI_OSS', '{\"accessKey\":\"LTAI4Fhq1AUAd3jPEfSaX8S4\",\"bucket\":\"haveplayed\",\"changed\":true,\"endpoint\":\"oss-cn-shenzhen.aliyuncs.com\",\"filePath\":\"\",\"http\":\"http://\",\"secretKey\":\"3Y106z4GQgP47ssH97KfB7O28xGhip\",\"serviceName\":\"ALI_OSS\"}', 'oss', '2020-03-30 15:13:16', '2020-02-13 10:57:27', '阿里云存储配置');
INSERT INTO `sys_config` VALUES (7, 'SMS_CONFIG', '{\"accessKey\":\"LTAI4Fhq1AUAd3jPEfSaX8S4\",\"accessSecret\":\"3Y106z4GQgP47ssH97KfB7O28xGhip\",\"enable\":true,\"encryption\":true,\"signName\":\"玩了么商城\",\"smsCodeHander\":\"aliSmsHandler\",\"smsTplCode\":\"SMS_174814144\"}', 'sms', '2020-03-06 11:24:01', '2020-02-13 10:57:27', '短信配置');
INSERT INTO `sys_config` VALUES (8, 'PLATFORM_CONFIG', '{\"platformName\":\"玩了么商城\"}', 'platform', '2020-03-06 11:24:06', '2020-02-13 10:57:27', '平台配置');
INSERT INTO `sys_config` VALUES (9, 'IM_CONFIG', '{\"appKey\":\"sfci50a7sxohi\",\"appSecret\":\"xOgOAzuMXeN6\",\"serviceName\":\"IM_RONGCLOUD\"}', 'im', '2020-03-06 11:24:06', '2020-02-13 10:57:27', '即时通讯配置');
INSERT INTO `sys_config` VALUES (10, 'PUSH_CONFIG', '[{\"keyName\":\"name1\",\"appKey\":\"c4298a30010f3086e7d098bb\",\"secretKey\":\"a7d01a6c5fb838dcaf604024\",\"ambient\":false,\"serviceName\":\"PUSH_JG\"}]', 'push', '2020-04-04 11:18:00', '2020-02-13 10:57:27', '极光推送');
INSERT INTO `sys_config` VALUES (11, 'CATEGORY_LEVEL', '3', 'category', '2020-03-06 11:24:06', '2020-02-13 10:57:27', '商品分类最大级别');
INSERT INTO `sys_config` VALUES (12, 'GLOBAL_CONFIG', '{\"enableProductParams\":false,\"maxCategoryLevel\":3,\"recommendAmount\":15,\"recommendEnable\":true}', 'global', '2020-04-05 13:16:50', '2020-02-13 10:57:27', '全局个性配置');

SET FOREIGN_KEY_CHECKS = 1;
