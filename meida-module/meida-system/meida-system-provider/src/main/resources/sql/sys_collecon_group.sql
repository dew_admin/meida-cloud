/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 10:03:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_collecon_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_collecon_group`;
CREATE TABLE `sys_collecon_group`  (
  `sgcId` bigint(20) NOT NULL COMMENT '主键',
  `colleconId` bigint(20) NULL DEFAULT NULL COMMENT '收藏ID',
  `groupId` bigint(20) NULL DEFAULT NULL COMMENT '分组ID',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sgcId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '收藏分组中间表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
