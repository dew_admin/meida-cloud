/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:01:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_collecon
-- ----------------------------
DROP TABLE IF EXISTS `sys_collecon`;
CREATE TABLE `sys_collecon`  (
  `colleconId` bigint(20) NOT NULL COMMENT '主键',
  `targetId` bigint(20) NULL DEFAULT NULL COMMENT '收藏目标Id',
  `groupId` bigint(20) NULL DEFAULT NULL COMMENT '分组ID',
  `targetEntity` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '目标实体',
  `optType` int(11) NULL DEFAULT NULL COMMENT '操作类型 1收藏2点赞3关注',
  `targetCompanyId` bigint(20) NULL DEFAULT NULL COMMENT '收藏目标所属企业ID',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `updateTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`colleconId`) USING BTREE,
  INDEX `idx_targetId`(`targetId`, `targetEntity`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '收藏点赞关注表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
