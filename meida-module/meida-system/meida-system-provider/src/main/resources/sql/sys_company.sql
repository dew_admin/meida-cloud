/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 06/08/2021 14:50:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_company`;
CREATE TABLE `sys_company`  (
  `companyId` bigint(20) NOT NULL COMMENT '主键',
  `companyName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业名称',
  `companyLogo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业logo',
  `linkMan` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `linkTel` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `businessScope` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主营范围',
  `companyAddress` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `companyType` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业类型',
  `businessLic` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '营业执照',
  `organizationId` bigint(20) NULL DEFAULT NULL COMMENT '机构ID',
  `businessLicNo` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '营业执照号',
  `companyDesc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业简介',
  `legalPerson` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业法人',
  `backIdCard` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '法人身份证反面',
  `frontIdCard` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '法人身份证正面',
  `cityId` bigint(20) NULL DEFAULT NULL COMMENT '市',
  `proId` bigint(20) NULL DEFAULT NULL COMMENT '省',
  `areaId` bigint(20) NULL DEFAULT NULL COMMENT '区域ID',
  `areaName` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所在地',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `managerId` bigint(20) NULL DEFAULT NULL COMMENT '管理员账户',
  `auditContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核意见',
  `companyState` int(11) NULL DEFAULT NULL COMMENT '认证状态',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '是否推荐',
  `referrerUserId` bigint(20) NULL DEFAULT NULL COMMENT '邀请人ID',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`companyId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '企业表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_company
-- ----------------------------
INSERT INTO `sys_company` VALUES (1, '深圳何氏户外用品有限公司', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/dbed3819a0f848a0b94ba242192616fa.png', '何先生', '13728682642', '', '前海深港合作区前湾一路1号A栋201室', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 4403, 440, 440305, '广东省深圳市南山区', NULL, NULL, '已审核', 2, NULL, NULL, NULL, '2020-01-10 17:36:52');
INSERT INTO `sys_company` VALUES (1225024779286646785, '何氏户外', '', '何振洲', '18682387185', '', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/37950fa10c79484998589d66c7ed1602.png', NULL, NULL, NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/5c45279339414a5ca5996568b95e8c0d.png', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/5ad75b7077ce41328240534029bba3c1.png', NULL, NULL, NULL, '', NULL, 1215593746472501251, '审核成功', 2, NULL, NULL, '2020-02-05 19:54:01', '2020-03-14 15:32:27');
INSERT INTO `sys_company` VALUES (1240560964004134913, '宏图钓鱼场', '', '欧阳', '15818758958', '', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/97bec8614a94455abdc056243883de9c.png', NULL, NULL, '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/65612b28c99044fcaa818351cdc7fed8.png', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/8d19806524f04ae7887f8fe1ec83176d.png', 4419, 440, NULL, '广东省东莞市', NULL, 1239223227871363073, '请上传，企业营业执照！！！', 10, NULL, NULL, '2020-03-19 16:49:16', '2020-03-23 00:46:11');
INSERT INTO `sys_company` VALUES (1244267936528437250, '雪乐山', '', '刘先生', '1868888889', '', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/9438289af0dc46bcba18fe8d90acbf2a.png', NULL, NULL, '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/2d67c28b82a249738ec6f4c0bc55190d.png', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c09c3ccb4a3d4c6c8a6ad155b63bd769.png', NULL, NULL, NULL, '', NULL, 1243520332689543169, '已审核', 2, NULL, NULL, '2020-03-29 22:19:28', '2020-04-01 23:06:52');
INSERT INTO `sys_company` VALUES (1244456594975985666, '爱尚竞技馆', '', '赵子香', '18838125259', '', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/0782358eab594d269e9bf0a66e9000f0.jpg', NULL, NULL, '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/a07bc1886ee641cdabe0ca1faadf3986.jpg', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c9b6c20ec72743219f61783b55503a39.jpg', 4102, 410, 410202, '河南省开封市龙亭区', NULL, 1231796156136030210, '已审核', 2, 0, NULL, '2020-03-30 10:49:07', '2020-04-01 14:05:56');
INSERT INTO `sys_company` VALUES (1245188173499166721, '上品户外', '', '赵子香', '15937125258', '', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/6c7abc938e2441a797c5c2e63d27c86b.jpg', NULL, NULL, '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7ac6a19005f34ca48be429e9e9bff452.jpg', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/d313fd38efb844949f9a1f6c9a7782b9.jpg', 4101, 410, NULL, '', NULL, 1245181613096005634, '已审核', 2, NULL, NULL, '2020-04-01 11:16:09', '2020-04-02 21:24:09');
INSERT INTO `sys_company` VALUES (1245701999009095681, '深圳市美达信息技术有限公司', NULL, '陈先生', '18001250778', NULL, NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/574a387920004083b8153dbfe7401e5c.png', NULL, NULL, NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7db59b6faabb4bca833facdcc3081fa1.png', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/61f32fb97aff4da0bf14673d26cbf41d.png', NULL, NULL, NULL, '', NULL, 1215568881594114050, '已审核', 2, NULL, NULL, '2020-04-02 21:17:55', '2020-04-02 21:19:18');
INSERT INTO `sys_company` VALUES (1245920330341310466, '火鼎贸易有限公司', '', '颜女士', '13554843127', '户外装备', '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c00677f309cd47c99080875a1c1da507.jpg', NULL, NULL, '', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/2b4d6c99b5244bd5962ebf0fa89bb7b6.jpg', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/a2dfdeebb98b48068bb66c6d221a1dad.jpg', 4403, 440, NULL, '', NULL, 1225794651847299074, '已审核', 2, 1, NULL, '2020-04-03 11:45:29', '2020-04-03 12:10:55');

SET FOREIGN_KEY_CHECKS = 1;
