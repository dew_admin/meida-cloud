package com.meida.module.system.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.constants.SettingConstant;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.JsonUtils;
import com.meida.module.system.client.entity.SysConfig;
import com.meida.module.system.provider.mapper.BaseConfigMapper;
import com.meida.module.system.provider.service.BaseConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统配置表 服务实现类
 *
 * @author flyme
 * @date 2019-06-10
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class BaseConfigServiceImpl extends BaseServiceImpl<BaseConfigMapper, SysConfig> implements BaseConfigService {

    @Override
    public void initBaseConfig() {
        String configKey = "CONFIG";
        Map<Object, Object> configMaps = new HashMap<>(0);
        List<SysConfig> configs = list();
        if (ObjectUtils.isNotEmpty(configs)) {
            redisUtils.delByProjectName(configKey);
            configs.forEach(config -> {
                String key = config.getConfigKey();
                configMaps.put(key, config.getConfigVal());

            });
            redisUtils.setProjectMap(configKey, configMaps);
            log.info("Redis初始化完成...");
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<SysConfig> listByType(String configType) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(true, "configType", configType);
        return list(qw);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public SysConfig getByKey(String configKey) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(true, "configKey", configKey);
        return getOne(qw);
    }

    /**
     * 保存对象存储配置
     *
     * @param model
     * @return
     */
    @Override
    public ResultBody saveOss(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        String serviceName = cs.getParams("serviceName");
        String filePath = cs.getParams("filePath");
        String http = cs.getParams("http", "http://");
        String bucket = cs.getParams("bucket");
        String secretKey = cs.getParams("secretKey");
        String accessKey = cs.getParams("accessKey");

        SysConfig used = getByKey("OSS_USED");
        if (FlymeUtils.isEmpty(used)) {
            used = new SysConfig();
            used.setConfigKey("OSS_USED");
            used.setConfigType("oss");
            used.setRemark("OSS存储方式");
        }
        used.setConfigVal(serviceName);
        saveOrUpdate(used);


        SysConfig oss = getByKey(serviceName);
        if (FlymeUtils.isEmpty(oss)) {
            oss = new SysConfig();
            oss.setConfigKey(serviceName);
            oss.setConfigType("oss");
        }
        EntityMap map = new EntityMap();
        map.put("filePath", filePath);
        map.put("http", http);
        map.put("changed", true);
        map.put("serviceName", serviceName);
        if (serviceName.equals(SettingConstant.LOCAL_OSS)) {
            String endpoint2 = cs.getParams("endpoint2");
            String domain = cs.getParams("domain");
            oss.setRemark("本地存储配置");
            map.put("endpoint", endpoint2);
            map.put("domain", domain);
        }else{
            String endpoint = cs.getParams("endpoint");
            oss.setRemark("云存储配置");
            map.put("bucket", bucket);
            map.put("secretKey", secretKey);
            map.put("accessKey", accessKey);
            map.put("endpoint", endpoint);
        }
        oss.setConfigVal(JsonUtils.beanToJson(map));
        saveOrUpdate(oss);
        //刷新缓存
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }

    @Override
    public ResultBody saveSms(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        Boolean enable = cs.getBoolean("enable");
        Boolean encryption = cs.getBoolean("encryption");
        String accessKey = cs.getParams("accessKey");
        String accessSecret = cs.getParams("accessSecret");
        String signName = cs.getParams("signName");
        String smsTplCode = cs.getParams("smsTplCode");
        String smsCodeHander = cs.getParams("smsCodeHander");
        EntityMap map = new EntityMap();
        map.put("accessKey", accessKey);
        map.put("accessSecret", accessSecret);
        map.put("enable", enable);
        map.put("encryption", encryption);
        map.put("smsTplCode", smsTplCode);
        map.put("signName", signName);
        map.put("smsCodeHander", smsCodeHander);
        SysConfig sms = getByKey("SMS_CONFIG");
        if (FlymeUtils.isEmpty(sms)) {
            sms = new SysConfig();
            sms.setConfigKey("SMS_CONFIG");
            sms.setConfigType("sms");
        }
        sms.setConfigVal(JsonUtils.beanToJson(map));
        saveOrUpdate(sms);
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }

    @Override
    public ResultBody savePush(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        String pushConfig = cs.getParams("pushConfig");
        SysConfig push = getByKey("PUSH_CONFIG");
        if (FlymeUtils.isEmpty(push)) {
            push = new SysConfig();
            push.setConfigKey("PUSH_CONFIG");
            push.setConfigType("push");
            push.setRemark("推送配置");
        }
        push.setConfigVal(pushConfig);
        saveOrUpdate(push);
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }


    @Override
    public ResultBody saveIm(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        String serviceName = cs.getParams("serviceName");
        String appKey = cs.getParams("appKey");
        String appSecret = cs.getParams("appSecret");
        EntityMap map = new EntityMap();
        map.put("serviceName", serviceName);
        map.put("appKey", appKey);
        map.put("appSecret", appSecret);
        SysConfig config = getByKey("IM_CONFIG");
        if (FlymeUtils.isEmpty(config)) {
            config = new SysConfig();
            config.setConfigKey("IM_CONFIG");
            config.setConfigType("im");
            config.setRemark("即时通讯配置");
        }
        config.setConfigVal(JsonUtils.beanToJson(map));
        saveOrUpdate(config);
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }

    @Override
    public ResultBody globalConfigSave(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        Boolean recommendEnable = cs.getBoolean("recommendEnable");
        Boolean enableProductParams = cs.getBoolean("enableProductParams");
        Integer maxCategoryLevel = cs.getInt("maxCategoryLevel", 3);
        Integer recommendAmount = cs.getInt("recommendAmount", 3);
        EntityMap map = new EntityMap();
        map.put("recommendEnable", recommendEnable);
        map.put("maxCategoryLevel", maxCategoryLevel);
        map.put("recommendAmount", recommendAmount);
        map.put("enableProductParams", enableProductParams);
        SysConfig config = getByKey("GLOBAL_CONFIG");
        if (FlymeUtils.isEmpty(config)) {
            config = new SysConfig();
            config.setConfigKey("GLOBAL_CONFIG");
            config.setConfigType("global");
        }
        config.setConfigVal(JsonUtils.beanToJson(map));
        saveOrUpdate(config);
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }

    @Override
    public ResultBody configSave(Map params) {
        CriteriaSave cs = new CriteriaSave(params, entityClass);
        String configKey = cs.getParams("configKey");
        if (FlymeUtils.isEmpty(configKey)) {
            return ResultBody.failed("configKey必传");
        }
        SysConfig config = getByKey(configKey);
        SysConfig newConfig = cs.getEntity(SysConfig.class);
        if (FlymeUtils.isNotEmpty(config)) {
            newConfig.setConfigId(config.getConfigId());
        }
        saveOrUpdate(newConfig);
        initBaseConfig();
        return ResultBody.ok("设置成功", true);
    }
}
