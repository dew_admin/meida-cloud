package com.meida.module.system.provider.controller;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.system.client.entity.SysConfig;
import com.meida.module.system.provider.service.BaseConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * 系统配置
 *
 * @author flyme
 * @date 2019-06-10
 */
@RestController
@RequestMapping("/config/")
@Api(tags = "基础模块-系统配置")
@ApiIgnore
public class BaseConfigController extends BaseController<BaseConfigService, SysConfig> {

    @RequestMapping(value = "oss", method = RequestMethod.GET)
    @ApiOperation(value = "查看OSS文件存储配置")
    public ResultBody oss() {
        List<SysConfig> list = bizService.listByType("oss");
        EntityMap map = new EntityMap();
        if (FlymeUtils.isNotEmpty(list)) {
            for (SysConfig config : list) {
                map.put(config.getConfigKey(), config.getConfigVal());
            }
        }
        return ResultBody.ok(map);
    }


    @RequestMapping(value = "get", method = RequestMethod.GET)
    @ApiOperation(value = "查看配置")
    public ResultBody get(@RequestParam(required = false) Map map) {
        CriteriaQuery<SysConfig> cq = new CriteriaQuery(map, SysConfig.class);
        SysConfig params = cq.getEntity(SysConfig.class);
        List<SysConfig> list = bizService.listByType(params.getConfigType());
        EntityMap result = new EntityMap();
        if (FlymeUtils.isNotEmpty(list)) {
            for (SysConfig config : list) {
                result.put(config.getConfigKey(), config.getConfigVal());
            }
        }
        return ResultBody.ok(result);
    }

    @ApiOperation(value = "OSS配置保存", notes = "oss配置保存")
    @PostMapping(value = "ossSave")
    @ApiIgnore
    public ResultBody ossSave(@RequestParam(required = false) Map params) {
        return bizService.saveOss(params);
    }

    @ApiOperation(value = "SMS配置保存", notes = "sms配置保存")
    @PostMapping(value = "smsSave")
    @ApiIgnore
    public ResultBody smsSave(@RequestParam(required = false) Map params) {
        return bizService.saveSms(params);
    }

    @ApiOperation(value = "即时通讯配置保存", notes = "即时通讯配置保存")
    @PostMapping(value = "saveIm")
    @ApiIgnore
    public ResultBody saveIm(@RequestParam(required = false) Map params) {
        return bizService.saveIm(params);
    }

    @ApiOperation(value = "推送配置保存", notes = "推送配置保存")
    @PostMapping(value = "pushSave")
    @ApiIgnore
    public ResultBody pushSave(@RequestParam(required = false) Map params) {
        return bizService.savePush(params);
    }

    @ApiOperation(value = "全局配置保存", notes = "全局配置保存")
    @PostMapping(value = "globalConfigSave")
    @ApiIgnore
    public ResultBody globalConfigSave(@RequestParam(required = false) Map params) {
        return bizService.globalConfigSave(params);
    }

    @ApiOperation(value = "配置保存", notes = "配置保存")
    @PostMapping(value = "configSave")
    @ApiIgnore
    public ResultBody configSave(@RequestParam(required = false) Map params) {
        return bizService.configSave(params);
    }


}
