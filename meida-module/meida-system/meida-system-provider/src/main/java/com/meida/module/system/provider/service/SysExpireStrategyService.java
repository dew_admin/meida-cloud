package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.system.client.entity.SysExpireStrategy;

import java.util.Date;

/**
 * 接口
 *
 * @author flyme
 * @date 2021-11-11
 */
public interface SysExpireStrategyService extends IBaseService<SysExpireStrategy> {

    /**
     * 设置过期key到redis
     */
    void setExpireKeyToRedis();

    /***
     * 保存到数据库
     * @param redisKey
     * @param targetId   目标Id
     * @param targetType 目标类型
     * @param expireTime 过期日期
     * @return
     */
    SysExpireStrategy saveAndToRedis(String redisKey, Long targetId, String targetType, Date expireTime);

    /***
     * 根据key键删除
     * @param redisKey
     * @return
     */
    Boolean deleteByRedisKey(String redisKey);

}
