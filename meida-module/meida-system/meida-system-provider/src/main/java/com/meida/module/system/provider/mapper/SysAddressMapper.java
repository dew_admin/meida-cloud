package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysAddress;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 个人地址 Mapper 接口
 * @author flyme
 * @date 2019-11-28
 */
public interface SysAddressMapper extends SuperMapper<SysAddress> {

}
