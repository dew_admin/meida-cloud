package com.meida.module.system.provider.controller;

import com.meida.common.mybatis.model.*;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysGroup;
import com.meida.module.system.provider.service.SysGroupService;
import springfox.documentation.annotations.ApiIgnore;

import javax.swing.*;
import java.util.Map;


/**
 * 分组控制器
 *
 * @author flyme
 * @date 2019-06-27
 */
@RestController
@RequestMapping("/group/")
@Api(tags = "基础模块-分组管理")
public class SysGroupController extends BaseController<SysGroupService, SysGroup> {

    @ApiIgnore
    @ApiOperation(value = "分组分页列表", notes = "分组分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "分组全部列表", notes = "分组全部列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiIgnore
    @ApiOperation(value = "添加分组", notes = "添加分组")
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiIgnore
    @ApiOperation(value = "更新分组", notes = "更新分组")
    @PostMapping(value = "update")
    public ResultBody edit(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiIgnore
    @ApiOperation(value = "删除分组", notes = "删除分组")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "企业创建的分组列表", notes = "企业创建分组列表 ")
    @GetMapping(value = "/listByComapny")
    public ResultBody listByComapny(@RequestParam(required = false) Map params) {
        return bizService.listByComapny(params);
    }

    @ApiOperation(value = "分组-详情", notes = "分组详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
