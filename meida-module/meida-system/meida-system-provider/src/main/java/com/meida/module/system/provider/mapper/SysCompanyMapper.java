package com.meida.module.system.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysCompany;



import java.util.List;

/**
 * 企业表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-02
 */
public interface SysCompanyMapper extends SuperMapper<SysCompany> {

    /**
     * 查询用户账套
     * @param account
     * @return
     */
    List<SysCompany> selectByAccount(String account);

    /**
     * 查询用户已分配企业ID
     * @param userId
     * @return
     */
    List<Long> selectUserCompanyIds(Long userId);
}
