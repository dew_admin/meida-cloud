package com.meida.module.system.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.system.client.entity.SysBank;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.mapper.SysBankMapper;
import com.meida.module.system.provider.service.SysBankService;
import com.meida.module.system.provider.service.SysCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 银行信息接口实现类
 *
 * @author flyme
 * @date 2020-08-19
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysBankServiceImpl extends BaseServiceImpl<SysBankMapper, SysBank> implements SysBankService {

    @Autowired
    private SysCompanyService companyService;


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysBank bank, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        SysCompany currentCompany = companyService.getById(companyId);
        bank.setOrganizationId(currentCompany.getOrganizationId());
        bank.setBankState(CommonConstants.ENABLED);
        bank.setCompanyId(companyId);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysBank> cq, SysBank sysBank, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        SysCompany currentCompany = companyService.getById(companyId);
        if (FlymeUtils.isNotEmpty(currentCompany)) {
            cq.eq(SysBank.class, "organizationId", currentCompany.getOrganizationId());
            cq.eq(SysBank.class, "companyId", companyId);
        }
        cq.eq(SysBank.class,"bankState",CommonConstants.ENABLED);
        return super.beforeListEntityMap(cq, sysBank, requestMap);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysBank> cq, SysBank bank, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        SysCompany currentCompany = companyService.getById(companyId);
        if (FlymeUtils.isNotEmpty(currentCompany)) {
            cq.eq(SysBank.class, "organizationId", currentCompany.getOrganizationId());
            cq.eq(SysBank.class, "companyId", companyId);
        }
        cq.orderByDesc("bank.createTime");
        return ResultBody.ok();
    }
}
