package com.meida.module.system.provider.service.impl;


import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.system.client.entity.SysDict;
import com.meida.module.system.client.entity.SysDictData;
import com.meida.module.system.provider.mapper.SysDictDataMapper;
import com.meida.module.system.provider.service.SysDictDataService;
import com.meida.module.system.provider.service.SysDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典数据接口实现
 *
 * @author flyme
 */
@Slf4j
@Service
public class SysDictDataServiceImpl extends BaseServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {

    @Resource
    private SysDictService dictService;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> listByDictId(Long dictId) {
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(SysDictData.class);
        cq.select("dictDataId", "dicDataTitle", "dicDataValue");
        cq.eq("dictId", dictId).eq("state", CommonConstants.ENABLED).orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> listByDictType(String dictType) {
        SysDict dict = dictService.findByType(dictType);
        ApiAssert.isNotEmpty("dictType不存在", dict);
        return listByDictId(dict.getDictId());
    }

    @Override
    public void deleteByDictId(Long dictId) {
        CriteriaQuery cq = new CriteriaQuery(SysDictData.class);
        cq.eq("dictId", dictId);
        remove(cq);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectByDictId(Long dictId) {
        CriteriaQuery cq = new CriteriaQuery(SysDictData.class);
        cq.select("dicDataTitle as name,dicDataValue as code,dictDataId as id").eq("dictId", dictId);
        cq.eq("state", CommonConstants.ENABLED_STR).orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public EntityMap selectByTypes(String[] dictTypes) {
        EntityMap map = new EntityMap();
        if (FlymeUtils.isNotEmpty(dictTypes)) {
            for (String type : dictTypes) {
                SysDict sysDict = dictService.findByType(type);
                if (FlymeUtils.isNotEmpty(sysDict)) {
                    List<EntityMap> maps = selectByDictId(sysDict.getDictId());
                    map.put(type, maps);
                }
            }
        }
        return map;
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysDictData dictData, EntityMap extra) {
        String dicDataValue = cs.getParams("dicDataValue");
        SysDict dict = dictService.getById(dictData.getDictId());
        if (dict == null) {
            return ResultBody.failed("字典类型id不存在");
        }
        if (FlymeUtils.isEmpty(dicDataValue)) {
            dicDataValue = IdWorker.getIdStr();
            dictData.setDicDataValue(dicDataValue);
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, SysDictData dictData, EntityMap extra) {
        dictService.initDictRedis();
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, SysDictData dictData, EntityMap extra) {
        dictService.initDictRedis();
        return ResultBody.ok();
    }


    @Override
    public ResultBody beforePageList(CriteriaQuery<SysDictData> cq, SysDictData dictData, EntityMap requestMap) {
        cq.select(SysDictData.class, "*");
        cq.eq(entityClass, "dictId");
        return ResultBody.ok();
    }


    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        dictService.initDictRedis();
        return ResultBody.ok();
    }

    /**
     * 状态更新完成后刷新redis
     *
     * @param cq
     * @return
     */
    @Override
    public ResultBody afterSetState(CriteriaUpdate cq) {
        dictService.initDictRedis();
        return ResultBody.ok();
    }

}
