package com.meida.module.system.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysDict;

/**
 * @author: flyme
 * @date: 2018/3/7 15:26
 * @desc: 字典项管理Mapper 接口
 */
public interface SysDictMapper extends SuperMapper<SysDict> {


}
