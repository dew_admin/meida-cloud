package com.meida.module.system.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.system.client.entity.SysCompany;

import java.util.List;
import java.util.Map;

/**
 * 企业表 服务类
 *
 * @author flyme
 * @date 2019-06-02
 */
public interface SysCompanyService extends IBaseService<SysCompany> {


    /**
     * 检测企业是否注册
     *
     * @param companyName
     */
    Boolean checkByCompanyName(Long organizationId, String companyName, String businessLicNo);

    /**
     * 检测企业是否注册
     *
     * @param companyName
     */
    Boolean checkByCompanyName(String companyName, String businessLicNo);


    /**
     * 查询最新推荐企业
     */
    List<EntityMap> listRecommend(CriteriaQuery cq, int count, boolean collecon, String companyType);

    /**
     * 企业认证审核
     *
     * @param map
     * @return
     */
    ResultBody setAuditState(Map map);

    /**
     * 重置认证状态
     *
     * @param companyId
     * @return
     */
    ResultBody resetAuditState(Long companyId);

    /**
     * 设置推荐状态
     *
     * @param ids
     * @param recommend
     * @return
     */
    ResultBody setRecommend(Long[] ids, Integer recommend);

    /**
     * 获取某账户账套
     *
     * @param accountName
     * @return
     */
    ResultBody getByAccount(String accountName);

    /**
     * 获取企业分配列表(穿梭框)数据
     *
     * @param userId
     * @param organizationId
     * @return
     */
    ResultBody getAuthCompanyList(Long userId, Long organizationId);

    /**
     * 树形结构数据
     *
     * @return
     */
    List<EntityMap> treeList(Map params);

    /**
     * 查询用户已分配的机构
     *
     * @param userId
     * @return
     */
    List<EntityMap> selectCompanyByUserId(Long userId);

    /**
     * 排序
     *
     * @param companyId
     * @param type
     * @return
     */
    ResultBody sort(Long companyId, Integer type);

    /**
     * description: 校验字段是否重复
     * date: 2023年-05月-31日 15:34
     * author: ldd
     *
     * @param businessLicNo
     * @param companyNo
     * @return java.lang.Boolean
     */
    Boolean checkByCompanyColumn(Long companyId, String businessLicNo, String companyNo);

}
