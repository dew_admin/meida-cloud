package com.meida.module.system.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.constant.SystemConstant;
import com.meida.module.system.client.entity.SysArea;
import com.meida.module.system.provider.service.SysAreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 区域管理控制器
 *
 * @author flyme
 * @date 2019-06-11
 */
@RestController
@Api(tags = "基础模块-区域管理")
public class SysAreaController extends BaseController<SysAreaService, SysArea> {

    /**
     * 获取省份
     */
    @ApiOperation(value = "获取省份")
    @GetMapping(value = "/area/getPro")
    public ResultBody getPro() {
        return bizService.selectByLevel(SystemConstant.AREA_PRO);
    }

    /**
     * 获取城市
     */
    @ApiOperation(value = "获取城市")
    @GetMapping(value = "/area/getCity")
    @ApiImplicitParam(value = "省ID", name = "areaId", paramType = "form")
    public ResultBody getCity(@RequestParam(value = "areaId", required = false) Long areaId) {
        return bizService.selectByLevel(SystemConstant.AREA_CITY, areaId);
    }

    /**
     * 获取区域
     */
    @ApiOperation(value = "获取区域")
    @GetMapping(value = "/area/getArea")
    @ApiImplicitParam(value = "市ID", name = "areaId", paramType = "form")
    public ResultBody getArea(@RequestParam(value = "areaId", required = false) Long areaId) {
        return bizService.selectByLevel(SystemConstant.AREA_QU, areaId);
    }


    @ApiOperation(value = "获取省市区3级数据", notes = "获取省市区 ")
    @GetMapping(value = "area/all")
    public ResultBody all() {
        return bizService.getAllArea();
    }


    /**
     * 城市索引列表
     *
     * @return
     */
    @ApiOperation(value = "城市索引列表", notes = "城市索引列表")
    @GetMapping("indexList")
    @ApiImplicitParam(value = "是否转换json格式", name = "conver", paramType = "form")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "是否转换json格式", name = "conver", paramType = "form"),
            @ApiImplicitParam(value = "是否包含热门城市", name = "hasHot", paramType = "form")
    })
    public ResultBody getMobile(@RequestParam(value = "conver", required = false, defaultValue = "false") Integer dataType, @RequestParam(value = "hasHot", required = false, defaultValue = "true") Boolean hasHot) {
        return bizService.selectIndexCity(dataType, hasHot);
    }

}
