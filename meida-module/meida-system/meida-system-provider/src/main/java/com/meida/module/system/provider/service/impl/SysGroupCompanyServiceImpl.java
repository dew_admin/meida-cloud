package com.meida.module.system.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.system.client.entity.SysCompanyGroup;
import com.meida.module.system.client.entity.SysGroup;
import com.meida.module.system.provider.mapper.SysGroupCompanyMapper;
import com.meida.module.system.provider.service.SysGroupCompanyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 企业分组中间表 实现类
 *
 * @author flyme
 * @date 2019-08-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysGroupCompanyServiceImpl extends BaseServiceImpl<SysGroupCompanyMapper, SysCompanyGroup> implements SysGroupCompanyService {



    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysCompanyGroup> cq, SysCompanyGroup sysCompanyGroup, EntityMap requestMap) {

        Long userId = OpenHelper.getUserId();
        Long companyId = OpenHelper.getCompanyId();
        cq.select(SysCompanyGroup.class, "*");
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.eq(SysGroup.class, "userId", userId);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(SysCompanyGroup.class, "ownerCompanyId", companyId);
        }
        return super.beforeListEntityMap(cq, sysCompanyGroup, requestMap);
    }

    @Override
    public ResultBody beforePageList(CriteriaQuery<SysCompanyGroup> cq, SysCompanyGroup sysCompanyGroup, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        cq.select(SysCompanyGroup.class, "*");
        cq.select(SysGroup.class, "groupName", "groupId");
        cq.eq(SysGroup.class, "groupId");
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.eq(SysGroup.class, "userId", userId);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(SysCompanyGroup.class, "ownerCompanyId", companyId);
        }
        cq.createJoin(SysGroup.class);
        return super.beforePageList(cq, sysCompanyGroup, requestMap);
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysCompanyGroup sysCompanyGroup, EntityMap extra) {
        List<Long> groupIds = cs.getArrayLong("groupIds");
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        if (FlymeUtils.isNotEmpty(userId)) {
            cs.put("userId", userId);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            cs.put("ownerCompanyId", companyId);
        }
        if (FlymeUtils.isNotEmpty(groupIds)) {
            for (Long groupId : groupIds) {
                cs.put("groupId", groupId);
            }
        }
        return super.beforeAdd(cs, sysCompanyGroup, extra);
    }



}
