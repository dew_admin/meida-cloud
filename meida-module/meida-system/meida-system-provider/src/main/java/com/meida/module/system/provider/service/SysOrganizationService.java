package com.meida.module.system.provider.service;

import com.meida.module.system.client.entity.SysOrganization;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 机构(集团) 接口
 *
 * @author flyme
 * @date 2020-07-24
 */
public interface SysOrganizationService extends IBaseService<SysOrganization> {

}
