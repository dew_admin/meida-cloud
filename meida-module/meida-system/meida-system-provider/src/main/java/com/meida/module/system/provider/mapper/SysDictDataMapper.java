package com.meida.module.system.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysDictData;

/**
 * @author: flyme
 * @date: 2018/3/7 15:26
 * @desc: 字典值管理Mapper 接口
 */
public interface SysDictDataMapper extends SuperMapper<SysDictData> {


}
