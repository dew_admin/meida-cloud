package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.system.client.entity.SysAddress;

import java.util.List;

/**
 * 个人地址 接口
 *
 * @author flyme
 * @date 2019-11-28
 */
public interface SysAddressService extends IBaseService<SysAddress> {

    /**
     * 查询个人收货地址
     *
     * @param userId
     * @return
     */
    List<EntityMap> listByUserId(Long userId);

    /**
     * 查询用户默认收货地址
     * @param userId
     * @return
     */
    SysAddress findDefaultAddress(Long userId);

    /**
     * 设置默认地址
     * @param addressId
     * @return
     */
    Boolean setDefaultAddress(Long addressId);

}
