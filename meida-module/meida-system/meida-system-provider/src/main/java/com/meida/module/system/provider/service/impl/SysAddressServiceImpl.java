package com.meida.module.system.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.system.client.entity.SysAddress;
import com.meida.module.system.provider.mapper.SysAddressMapper;
import com.meida.module.system.provider.service.SysAddressService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.system.provider.service.SysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 收货地址 实现类
 *
 * @author flyme
 * @date 2019-11-28
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysAddressServiceImpl extends BaseServiceImpl<SysAddressMapper, SysAddress> implements SysAddressService {


    @Autowired
    private SysAreaService areaService;

    /**
     * 查询个人收货地址
     *
     * @param userId
     * @return
     */
    @Override
    public List<EntityMap> listByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(SysAddress.class);
        cq.eq(true, "userId", userId);
        cq.orderByDesc("defaultAddress");
        cq.eq("deleted", CommonConstants.DEL_NO);
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysAddress> cq, SysAddress sysAddress, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.eq(true, "userId", userId);
        cq.eq("deleted", CommonConstants.DEL_NO);
        cq.orderByDesc("defaultAddress");
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysAddress sysAddress, EntityMap extra) {
        sysAddress.setUserId(OpenHelper.getUserId());
        sysAddress.setDeleted(CommonConstants.DEL_NO);
        if (FlymeUtils.isNotEmpty(sysAddress)) {
            String areaName = areaService.getAreaName(sysAddress.getProId(), sysAddress.getCityId(), sysAddress.getAreaId());
            sysAddress.setAreaName(areaName);
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, SysAddress sysAddress, EntityMap extra) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set(true, "defaultAddress", CommonConstants.DISABLED);
        cu.ne(true, "addressId", sysAddress.getAddressId());
        setDefaultAddress(sysAddress.getAddressId());
        return ResultBody.ok("添加成功");
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<SysAddress> cu, SysAddress sysAddress,EntityMap extra) {
        if (FlymeUtils.isNotEmpty(sysAddress)) {
            String areaName = areaService.getAreaName(sysAddress.getProId(), sysAddress.getCityId(), sysAddress.getAreaId());
            sysAddress.setAreaName(areaName);
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, SysAddress sysAddress,EntityMap extra) {
        setDefaultAddress(sysAddress.getAddressId());
        return ResultBody.ok("更新成功");
    }

    @Override
    public SysAddress findDefaultAddress(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(SysAddress.class);
        cq.eq("defaultAddress", CommonConstants.ENABLED);
        cq.eq(true, "userId", userId);
        return getOne(cq);
    }

    @Override
    public Boolean setDefaultAddress(Long addressId) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set(true, "defaultAddress", CommonConstants.DISABLED);
        cu.ne(true, "addressId", addressId);
        return update(cu);

    }
}
