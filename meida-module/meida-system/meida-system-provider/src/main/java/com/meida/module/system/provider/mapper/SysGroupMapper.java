package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysGroup;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 分组 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface SysGroupMapper extends SuperMapper<SysGroup> {

}
