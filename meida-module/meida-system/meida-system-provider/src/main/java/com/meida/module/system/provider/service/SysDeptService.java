package com.meida.module.system.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.system.client.entity.SysDept;

import java.util.List;

/**
 * 部门 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface SysDeptService extends IBaseService<SysDept> {
    /**
     * 查询tree需要的数据结构
     *
     * @return
     */
    List<EntityMap> deptTreeList(Long parentId,Long companyId);

    List<EntityMap> selectTreeList(Long companyId);

    /**
     * 根据companyId从统计数量
     * @param companyId
     * @return
     */
    Long countByCompanyId(Long companyId);

    /**
     * 根据parentId从统计数量
     * @param parentId
     * @return
     */
    Long countByParentId(Long parentId);

    /**
     * 查询用户已分配的部门
     * @param userId
     * @return
     */
    List<EntityMap> selectDeptByUserId(Long userId);

    List<Long> selectDeptByUserId(Long userId, Long companyId);

    /**
     * 查询用户已分配的部门
     * @param userId
     * @return
     */
    List<EntityMap> treeByUserId(Long userId, Long parentId, Long companyId);

    /**
     * description: 内存加载部门数据·
     * date: 2024年-01月-25日 14:23
     * author: ldd
     *
     * @param
     * @return void
     */
    void loadData2Redis();
    /**
     * 排序
     *
     * @param deptId
     * @param type
     * @return
     */
    ResultBody sort(Long deptId, Integer type);

    /**
     * description: 校验字段是否重复
     * date: 2023年-05月-31日 15:34
     * author: ldd
     *
     * @param deptCode
     * @return java.lang.Boolean
     */
    Boolean checkByCompanyColumn(Long companyId, Long deptId, String deptCode);

    List<SysDept> selectByCompanyId(Long companyId);

    //查询用户未分配的部门
    List<EntityMap> treeOtherByUserId(Long userId, Long parentId, Long companyId);
}
