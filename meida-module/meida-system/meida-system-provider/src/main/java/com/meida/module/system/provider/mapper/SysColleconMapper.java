package com.meida.module.system.provider.mapper;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.system.client.entity.SysCollecon;
import com.meida.common.mybatis.base.mapper.SuperMapper;


import java.util.List;

/**
 * 收藏点赞关注表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-13
 */
public interface SysColleconMapper extends SuperMapper<SysCollecon> {
    /**
     * 查询企业热门收藏产品排行
     *
     * @param map
     * @return
     */
    List<EntityMap> selectColleconProductByCompanyId(EntityMap map);
}
