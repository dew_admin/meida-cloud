package com.meida.module.system.provider.service;

import com.meida.module.system.client.entity.SysColleconGroup;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 收藏分组中间表 接口
 *
 * @author flyme
 * @date 2019-06-16
 */
public interface SysGroupColleconService extends IBaseService<SysColleconGroup> {
    Boolean deleteByColleconIds(Long[] colleconIds);

    Boolean setColleconGroup(Long[] colleconIds, Long[] groupIds);

    List<Long> selectGroupId(Long colleconId);
}
