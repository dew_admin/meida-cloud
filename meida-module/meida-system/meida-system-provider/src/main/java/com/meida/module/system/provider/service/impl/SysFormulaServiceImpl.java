package com.meida.module.system.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysFormula;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.mapper.SysFormulaMapper;
import com.meida.module.system.provider.service.SysFormulaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 公式接口实现类
 *
 * @author flyme
 * @date 2020-08-08
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysFormulaServiceImpl extends BaseServiceImpl<SysFormulaMapper, SysFormula> implements SysFormulaService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysFormula formula, EntityMap extra) {
        formula.setStatus(CommonConstants.ENABLED);
        formula.setCompanyId(OpenHelper.getCompanyId());
        formula.setOrganizationId(OpenHelper.getOrganizationId());
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysFormula> cq, SysFormula formula, EntityMap requestMap) {
        cq.select(SysFormula.class, "*");
        cq.addSelect(SysCompany.class, "companyName");
        cq.addSelect(SysOrganization.class, "organizationName");
        cq.like(SysFormula.class, "formulaTitle");
        cq.eq(SysFormula.class, "formulaType");
        cq.eq(SysFormula.class, "companyId", OpenHelper.getCompanyId());
        cq.eq(SysFormula.class, "organizationId");

        cq.createJoin(SysCompany.class);
        cq.createJoin(SysOrganization.class);
        cq.orderByDesc("formula.createTime");
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysFormula> cq, SysFormula sysFormula, EntityMap requestMap) {
        cq.eq(SysFormula.class, "companyId", OpenHelper.getCompanyId());
        cq.eq(SysFormula.class, "formulaType");
        cq.eq(SysFormula.class, "organizationId");
        return super.beforeListEntityMap(cq, sysFormula, requestMap);
    }

}
