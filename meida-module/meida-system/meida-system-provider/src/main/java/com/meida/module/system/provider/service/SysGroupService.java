package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.system.client.entity.SysGroup;

import java.util.List;
import java.util.Map;

/**
 * 分组 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface SysGroupService extends IBaseService<SysGroup> {
    /**
     * 企业创建的分组列表
     *
     * @param params
     * @return
     */
    ResultBody listByComapny(Map params);


    List<EntityMap> selectByGroupId(List<Long> groupIds);

    /**
     * 根据groupCode查询分组
     *
     * @param groupCode
     * @return
     */
    SysGroup findByGroupCode(String groupCode);
}
