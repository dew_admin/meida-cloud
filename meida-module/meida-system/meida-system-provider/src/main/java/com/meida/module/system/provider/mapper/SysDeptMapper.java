package com.meida.module.system.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysDept;


/**
 * 部门 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface SysDeptMapper extends SuperMapper<SysDept> {

}
