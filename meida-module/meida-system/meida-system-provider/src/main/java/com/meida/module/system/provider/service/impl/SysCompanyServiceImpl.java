package com.meida.module.system.provider.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.*;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.common.enums.AuthStatusEnum;
import com.meida.common.enums.ColleconEnum;
import com.meida.common.enums.YesNoEnum;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.security.OpenUser;
import com.meida.common.utils.ApiAssert;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.mapper.SysCompanyMapper;
import com.meida.module.system.provider.service.SysAreaService;
import com.meida.module.system.provider.service.SysCompanyInfoService;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 企业表 服务实现类
 *
 * @author flyme
 * @date 2019-06-02
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysCompanyServiceImpl extends BaseServiceImpl<SysCompanyMapper, SysCompany> implements SysCompanyService {
    @Resource
    private SysAreaService areaService;
    @Resource
    private BaseColleconService colleconService;

    @Autowired(required = false)
    private BaseAdminAccountService baseAdminAccountService;

    @Autowired(required = false)
    private BaseSysRoleService sysRoleService;

    @Autowired(required = false)
    private BaseProductService baseProductService;

    @Autowired(required = false)
    private BaseShopService baseShopService;

    @Autowired(required = false)
    private BaseAdminUserService baseAdminUserService;

    @Autowired(required = false)
    private BaseAppUserService baseAppUserService;

    @Resource
    private SysCompanyMapper companyMapper;
    @Autowired
    private SysCompanyService companyService;

    @Autowired
    private SysCompanyInfoService companyInfoService;

    @Autowired
    private SysDeptService deptService;


    @Override
    public void afterGet(CriteriaQuery cq, EntityMap result) {
        Long companyId = result.getLong("companyId");
        long colleconTag = colleconService.countCollecon(companyId, ColleconEnum.SC, SysCompany.class);
        result.put("colleconTag", colleconTag);
        String accountName = result.get("linkTel");
        EntityMap account = baseAdminAccountService.getAccount(accountName, "USERNAME");
        if (FlymeUtils.isNotEmpty(account)) {
            Long userId = account.getLong("userId");
            Long roleId = sysRoleService.getRoleId(userId);
            result.put("roleId", roleId);
        }
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysCompany company, EntityMap extra) {
        company.setCompanyState(AuthStatusEnum.AUTHING.getCode());
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        Long organizationId = company.getOrganizationId();
        if (CommonConstants.INT_0.equals(superAdmin)) {
            SysCompany currentCompany = companyService.getById(OpenHelper.getCompanyId());
            if (FlymeUtils.isNotEmpty(currentCompany)) {
                company.setOrganizationId(currentCompany.getOrganizationId());
            }
        }
        Boolean check = checkByCompanyName(organizationId, company.getCompanyName(), company.getBusinessLicNo());
        ApiAssert.isFalse("名称已存在", check);

        String result = checkByCompany(0L, company.getBusinessLicNo(), company.getCompanyNo());
        if (StrUtil.isNotEmpty(result)) {
            return ResultBody.failed(result + "已存在");
        }
        Long parentId = FlymeUtils.isEmpty(company.getParentId()) ? 0L : company.getParentId();
        SysCompany parent = getById(parentId);
        Long count = 0L;
        CriteriaQuery cq = new CriteriaQuery(SysCompany.class);
        if (parentId.equals(0L)) {
            count = count(cq.eq("parentId", cs.getParams("parentId")));
        } else {
            count = count(cq.eq("parentId", parent.getCompanyId()));
        }
        Integer sortOrder = FlymeUtils.getInteger(count.intValue(), 0) + 1;
        company.setParentId(parentId);
        company.setOrderNo(sortOrder);
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, SysCompany company, EntityMap extra) {
        String shopName = cs.getParams("shopName");
        if (FlymeUtils.isNotEmpty(shopName)) {
            //如果有店铺信息则发送店铺注册通知
            try {
                OpenUser openUser = OpenHelper.getUser();
                Long companyId = company.getCompanyId();
                openUser.setCompanyId(companyId);
                cs.put("companyId", company.getCompanyId());
                mqTemplate.convertAndSend(QueueConstants.QUEUE_SHOP_REG, cs);
                mqTemplate.convertAndSend(QueueConstants.QUEUE_BIND_COMPANY, openUser);
            } catch (Exception e) {
                removeById(company.getCompanyId());
            }
        }
        //当需要企业详情时传递hasCompanyInfo=true
        Boolean hasCompanyInfo = cs.getBoolean("hasCompanyInfo");
        if (hasCompanyInfo) {
            EntityMap params = cs.getRequestMap();
            companyInfoService.saveCompanyInfo(company, params);
        }
        return ResultBody.ok("添加成功", company);
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<SysCompany> cu, SysCompany company, EntityMap extra) {
        String check = checkByCompany(company.getCompanyId(), company.getBusinessLicNo(), company.getCompanyNo());
        return StrUtil.isEmpty(check) ? ResultBody.ok() : ResultBody.failed(check + "已存在");
    }

    @Override
    public List<EntityMap> treeList(Map params) {
        EntityMap map = new EntityMap(params);
        Long companyId = map.getLong("companyId");
        CriteriaQuery cq = new CriteriaQuery(SysCompany.class);
        cq.select(SysCompany.class, "companyId as id", "companyName as label", "parentId");
        cq.eq("companyId", companyId);
        cq.orderByAsc("orderNo");
        List<EntityMap> companyList = selectEntityMap(cq);
        return companyList;
    }

    /**
     * 企业审核
     *
     * @param map
     * @return
     */
    @Override
    public ResultBody setAuditState(Map map) {
        CriteriaUpdate<SysCompany> cu = new CriteriaUpdate(map, entityClass);
        SysCompany company = cu.getEntity(SysCompany.class);
        Long companyId = company.getCompanyId();
        ApiAssert.isNotEmpty("companyId不能为空", companyId);
        Integer companyState = company.getCompanyState();
        cu.set("companyState", companyState);
        ResultBody resultBody = baseUpdate(cu);
        if (resultBody.isOk()) {
            return ResultBody.ok("审核成功", companyState);
        } else {
            return ResultBody.failed("审核失败");
        }
    }

    @Override
    public ResultBody resetAuditState(Long companyId) {
        ApiAssert.isNotEmpty("主键不能为空", companyId);
        UpdateWrapper product = new UpdateWrapper();
        product.set("companyState", AuthStatusEnum.NOAUTH.getCode());
        product.in("companyId", companyId);
        boolean result = update(product);
        return ResultBody.ok("", 0);

    }

    @Override
    public ResultBody getByAccount(String accountName) {
        List companyList = companyMapper.selectByAccount(accountName);
        return ResultBody.ok(companyList);
    }

    @Override
    public ResultBody getAuthCompanyList(Long userId, Long organizationId) {
        CriteriaQuery cq = new CriteriaQuery(SysCompany.class);
        cq.select(SysCompany.class, "companyId", "companyName");
        cq.eq(SysCompany.class, "companyState", 2);
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        //非超级管理员查询自己的企业
        if (CommonConstants.INT_0.equals(superAdmin)) {
            cq.eq(SysCompany.class, "organizationId", organizationId);
        }
        List<EntityMap> list = selectEntityMap(cq);
        List<Long> companyIds = companyMapper.selectUserCompanyIds(userId);
        EntityMap map = new EntityMap();
        map.put("companyList", list);
        map.put("companyIds", companyIds);
        return ResultBody.ok(map);
    }

    @Override
    public Boolean checkByCompanyName(String companyName, String businessLicNo) {
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(SysCompany::getCompanyName, companyName).or().eq(FlymeUtils.isNotEmpty(businessLicNo), SysCompany::getBusinessLicNo, businessLicNo);
        long count = count(queryWrapper);
        return count > 0 ? true : false;
    }

    @Override
    public Boolean checkByCompanyName(Long organizationId, String companyName, String businessLicNo) {
        CriteriaQuery<SysCompany> queryWrapper = new CriteriaQuery(SysCompany.class);
        queryWrapper.eq(true, "organizationId", organizationId).and(e -> e.eq("companyName", companyName).or().eq(FlymeUtils.isNotEmpty(businessLicNo), "businessLicNo", businessLicNo));
        long count = count(queryWrapper);
        return count > 0 ? true : false;
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<EntityMap> listRecommend(CriteriaQuery cq, int count, boolean collecon, String companyType) {
        cq.select(SysCompany.class, "companyId", "companyName", "companyLogo");
        if (collecon) {
            setCollecon(cq);
        }
        cq.eq("companyState", AuthStatusEnum.AUTHSUCCESS.getCode());
        cq.eq("company.recommend", YesNoEnum.YES);
        cq.eq("companyType", companyType);
        cq.orderByDesc("company.createTime");
        if (count > 0) {
            cq.last("limit " + count);
        }
        return selectEntityMap(cq);
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysCompany> cq, SysCompany company, EntityMap requestMap) {
        cq.select(SysCompany.class, "*");
        cq.select(SysOrganization.class, "organizationName");
        setCollecon(cq);
        cq.likeByField(SysCompany.class, "companyName");
        cq.eq(SysCompany.class, "companyType");
        Integer superAdmin = OpenHelper.getUser().getSuperAdmin();
        if (CommonConstants.INT_1.equals(superAdmin)) {
            //如果是超级管理员
            cq.eq(SysCompany.class, "organizationId");
        } else {
            Long companyId = OpenHelper.getCompanyId();
            SysCompany currentCompany = companyService.getById(companyId);
            if (FlymeUtils.isNotEmpty(currentCompany)) {
                cq.eq(SysCompany.class, "organizationId", currentCompany.getOrganizationId());
            }

        }

        cq.eq(SysCompany.class, "companyState");
        cq.createJoin(SysOrganization.class);
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysCompany> cq, SysCompany company, EntityMap requestMap) {
        cq.eq(SysCompany.class, "organizationId");
        return super.beforeListEntityMap(cq, company, requestMap);
    }


    @Override
    public ResultBody beforeDelete(CriteriaDelete<SysCompany> cd) {
        Long id = cd.getIdValue();
        if (FlymeUtils.isNotEmpty(id)) {
            Long childCount = deptService.countByCompanyId(id);
            if (childCount > 0) {
                return ResultBody.failed("删除失败:已存在关联部门");
            }
        }
        return super.beforeDelete(cd);
    }


    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        if (FlymeUtils.isNotEmpty(baseShopService) && FlymeUtils.isNotEmpty(ids)) {
            //删除企业店铺
            baseShopService.deletByCompanyIds(ids);
        }
        if (FlymeUtils.isNotEmpty(baseAdminUserService)) {
            baseAdminUserService.deleteByCompanyId(cd.getIdValue());
        }
        if (FlymeUtils.isNotEmpty(baseAppUserService)) {
            baseAppUserService.deleteByCompanyId(cd.getIdValue());
        }
        companyInfoService.removeById(cd.getIdValue());
        return super.afterDelete(cd, ids);
    }

    @Override
    public List<EntityMap> selectCompanyByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(SysCompany.class);
        cq.select(SysCompany.class, "companyId", "companyName","parentId");
        cq.eq(true, "buc.userId", userId);
        cq.orderByAsc("company.orderNo");
        cq.createJoin("com.meida.module.admin.client.entity.BaseUserCompany").setMainField("companyId").setJoinField("companyId");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody sort(Long companyId, Integer type) {
        ApiAssert.isNotEmpty("companyId不能为空", companyId);
        ApiAssert.isNotEmpty("移动类型不能为空", type);
        //排序为从小到大排   type为1为上移  2为下移
        if (type != 1 && type != 2) {
            ApiAssert.failure("移动类型只能为上移或下移");
        }
        SysCompany cate = this.getById(companyId);
        CriteriaQuery<SysCompany> query = new CriteriaQuery<SysCompany>(SysCompany.class);
        //查询同级别的
        query.lambda().eq(SysCompany::getParentId, cate.getParentId());
        query.orderByAsc("orderNo");
        List<SysCompany> list = this.list(query);
        int sourceIndex = -1;
        int targetIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            SysCompany obj = list.get(i);
            if (obj.getCompanyId().equals(companyId)) {
                if (type == 1) {//上移
                    if (i == 0) {
                        return ResultBody.failed("无法上移");
                    } else {
                        sourceIndex = i;
                        targetIndex = i - 1;
                    }
                } else if (type == 2) {//下移
                    if (i == list.size() - 1) {
                        return ResultBody.failed("无法下移");
                    } else {
                        sourceIndex = i;
                        targetIndex = i + 1;
                    }
                }
            }
        }
        if (sourceIndex < 0 || targetIndex < 0) {
            return ResultBody.failed("移动错误");
        }
        Long targetId = list.get(sourceIndex).getCompanyId();
        list.get(sourceIndex).setCompanyId(list.get(targetIndex).getCompanyId());
        list.get(targetIndex).setCompanyId(targetId);
        List<SysCompany> updateObj = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            SysCompany obj = new SysCompany();
            obj.setCompanyId(list.get(i).getCompanyId());
            obj.setOrderNo(i);
            updateObj.add(obj);
        }
        this.saveOrUpdateBatch(updateObj);
        return ResultBody.ok();
    }


    private void setCollecon(CriteriaQuery<SysCompany> cq) {
        //是否添加收藏标记
        Long userId = OpenHelper.getUserId();
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.addSelect("(select count(targetId) from sys_collecon sc where sc.targetId=company.companyId and targetEntity='SysCompany' and userId=" + userId + ") colleconTag");
        }
    }

    @Override
    public Boolean checkByCompanyColumn(Long companyId, String businessLicNo, String companyNo) {
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().ne(companyId > 0, SysCompany::getCompanyId, companyId).and(e -> e.eq(SysCompany::getCompanyNo, companyNo).or().eq(SysCompany::getBusinessLicNo, businessLicNo));
        long count = count(queryWrapper);
        return count > 0 ? true : false;
    }

    public String checkByCompany(Long companyId, String businessLicNo, String companyNo) {
        String result = "";
        QueryWrapper<SysCompany> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().ne(companyId > 0, SysCompany::getCompanyId, companyId);
        if (StrUtil.isNotEmpty(businessLicNo)) {
            queryWrapper.lambda().eq(SysCompany::getBusinessLicNo, businessLicNo);
            long count = count(queryWrapper);
            result += count > 0 ? "机构代码" : "";
        }
        if (StrUtil.isNotEmpty(companyNo)) {
            queryWrapper.clear();
            queryWrapper.lambda().ne(companyId > 0, SysCompany::getCompanyId, companyId);
            queryWrapper.lambda().eq(SysCompany::getCompanyNo, companyNo);
            long count = count(queryWrapper);
            result += count > 0 ? "全宗号" : "";
        }
        return result;
    }
}
