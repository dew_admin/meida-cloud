package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.system.client.entity.SysArea;

import java.util.List;

/**
 * 区域表 服务类
 *
 * @author flyme
 * @date 2019-06-11
 */
public interface SysAreaService extends IBaseService<SysArea> {

    /**
     * 根据级别查询
     *
     * @param level
     * @return
     */
    ResultBody selectByLevel(Integer level);

    /**
     * 根据级别查询
     *
     * @param level
     * @return
     */
    ResultBody selectByLevel(Integer level, Long areaId);

    /**
     * 获取热门城市
     *
     * @return
     */
    List<EntityMap> selectHotList();

    /**
     * 初始化区域
     *
     * @return
     */
    void initArea();

    /**
     * 获取区域
     *
     * @return
     */
    ResultBody getAllArea();

    /**
     * 获取城市索引列表
     *
     * @param dataType
     * @return
     */
    ResultBody selectIndexCity(Integer dataType, Boolean hasHot);

    /**
     * 查询区域名称
     *
     * @param proId
     * @param cityId
     * @param areaId
     * @return
     */
    String getAreaName(Long proId, Long cityId, Long areaId);

    /**
     * 查询默认城市
     * @return
     */
    SysArea getDefaultSysArea();
}
