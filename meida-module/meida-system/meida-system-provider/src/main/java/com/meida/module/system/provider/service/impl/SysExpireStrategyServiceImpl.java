package com.meida.module.system.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.DateUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.module.system.client.entity.SysExpireStrategy;
import com.meida.module.system.provider.mapper.SysExpireStrategyMapper;
import com.meida.module.system.provider.service.SysExpireStrategyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 接口实现类
 *
 * @author flyme
 * @date 2021-11-11
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class SysExpireStrategyServiceImpl extends BaseServiceImpl<SysExpireStrategyMapper, SysExpireStrategy> implements SysExpireStrategyService {
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysExpireStrategy ses, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysExpireStrategy> cq, SysExpireStrategy ses, EntityMap requestMap) {
        cq.orderByDesc("expireStrategy.createTime");
        return ResultBody.ok();
    }

    private List<String> queryExpireRedisKeyList() {
        String dateTime = DateUtils.getDateTime();
        QueryWrapper qw = new QueryWrapper();
        qw.select("redisKey");
        qw.le("expireTime", dateTime);
        return listObjs(qw, i -> i.toString());
    }

    @Override
    public void setExpireKeyToRedis() {
        log.info("过期事件重新放入redis中");
        List<String> keyList = queryExpireRedisKeyList();
        keyList.forEach(redisKey -> {
            redisUtils.set(redisKey, redisKey, 60 * 2);
        });
    }

    @Override
    public SysExpireStrategy saveAndToRedis(String redisKey, Long targetId, String targetType, Date expireTime) {
        SysExpireStrategy sysExpireStrategy = new SysExpireStrategy();
        sysExpireStrategy.setRedisKey(redisKey);
        sysExpireStrategy.setTargetId(targetId);
        sysExpireStrategy.setTargetType(targetType);
        sysExpireStrategy.setExpireTime(expireTime);
        boolean save = save(sysExpireStrategy);
        if (save) {
            Date nowDateTime = DateUtils.getNowDateTime();
            long time1 = expireTime.getTime();
            long time2 = nowDateTime.getTime();
            if (time1 > time2) {
                redisUtils.set(redisKey, targetId, (time1-time2) / 1000);
            }
        }
        return sysExpireStrategy;
    }

    @Override
    public Boolean deleteByRedisKey(String redisKey) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq("redisKey", redisKey);
        return remove(cd);
    }

}
