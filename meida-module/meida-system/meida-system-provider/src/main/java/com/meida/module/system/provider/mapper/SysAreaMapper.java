package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysArea;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 区域表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-11
 */
public interface SysAreaMapper extends SuperMapper<SysArea> {

}
