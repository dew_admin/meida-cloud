package com.meida.module.system.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysBlacklist;
import com.meida.module.system.provider.service.SysBlacklistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 黑名单控制器
 *
 * @author flyme
 * @date 2019-07-04
 */
@RestController
@RequestMapping("/system/")
@Api(tags = "基础模块-黑名单管理")
public class SysBlacklistController extends BaseController<SysBlacklistService, SysBlacklist> {

    @ApiOperation(value = "黑名单-分页列表", notes = "黑名单分页列表 ")
    @GetMapping(value = "blacklist/pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "黑名单-列表", notes = "黑名单列表 ")
    @GetMapping(value = "blacklist/list}")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "黑名单-添加", notes = "添加黑名单")
    @PostMapping(value = "blacklist/add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "黑名单-更新", notes = "更新黑名单")
    @PostMapping(value = "blacklist/update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "黑名单-删除", notes = "删除黑名单")
    @PostMapping(value = "blacklist/remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "黑名单-详情", notes = "黑名单详情")
    @GetMapping(value = "blacklist/get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
