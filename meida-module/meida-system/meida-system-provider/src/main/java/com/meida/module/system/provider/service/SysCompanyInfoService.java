package com.meida.module.system.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysCompanyInfo;

/**
 * 企业详情表 服务类
 *
 * @author flyme
 * @date 2021-12-15
 */
public interface SysCompanyInfoService extends IBaseService<SysCompanyInfo> {
    /**
     * 保存企业详情
     */
    Boolean saveCompanyInfo(SysCompany company, EntityMap params);
}
