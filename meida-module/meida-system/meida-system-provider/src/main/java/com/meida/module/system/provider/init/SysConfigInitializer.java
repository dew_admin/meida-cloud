package com.meida.module.system.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.system.client.entity.SysConfig;
import com.meida.module.system.client.entity.SysDict;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class SysConfigInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return SysConfig.class;
    }
}
