package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysBank;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 银行信息 Mapper 接口
 * @author flyme
 * @date 2020-08-19
 */
public interface SysBankMapper extends SuperMapper<SysBank> {

}
