package com.meida.module.system.provider.service;

import com.meida.module.system.client.entity.SysCompanyGroup;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 企业分组中间表 接口
 *
 * @author flyme
 * @date 2019-08-05
 */
public interface SysGroupCompanyService extends IBaseService<SysCompanyGroup> {

}
