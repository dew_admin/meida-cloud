package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysCompanyGroup;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 企业分组中间表 Mapper 接口
 *
 * @author flyme
 * @date 2019-08-05
 */
public interface SysGroupCompanyMapper extends SuperMapper<SysCompanyGroup> {

}
