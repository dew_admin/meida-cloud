package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysColleconGroup;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 收藏分组中间表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-16
 */
public interface SysGroupColleconMapper extends SuperMapper<SysColleconGroup> {

}
