package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysExpireStrategy;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 *  Mapper 接口
 * @author flyme
 * @date 2021-11-11
 */
public interface SysExpireStrategyMapper extends SuperMapper<SysExpireStrategy> {

}
