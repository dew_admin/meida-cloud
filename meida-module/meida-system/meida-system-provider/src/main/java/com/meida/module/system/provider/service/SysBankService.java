package com.meida.module.system.provider.service;

import com.meida.module.system.client.entity.SysBank;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 银行信息 接口
 *
 * @author flyme
 * @date 2020-08-19
 */
public interface SysBankService extends IBaseService<SysBank> {

}
