package com.meida.module.system.provider.controller;

import com.meida.common.annotation.LoginRequired;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 企业管理接口
 *
 * @author zyf
 */
@Api(tags = "基础模块-企业管理")
@RestController
@RequestMapping("/company/")
public class SysCompanyController extends BaseController<SysCompanyService, SysCompany> {


    @ApiOperation(value = "企业-分页列表", notes = "企业分页列表 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "企业-列表", notes = "企业列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "企业-树形列表", notes = "树形列表")
    @GetMapping(value = "tree")
    public ResultBody tree(@RequestParam(required = false) Map params) {
        List<EntityMap> list = bizService.treeList(params);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "查询用户已分配的企业")
    @GetMapping(value = "listByUserId")
    public ResultBody listByUserId(@RequestParam(required = false) Long userId) {
        List<EntityMap> list = bizService.selectCompanyByUserId(userId);
        return ResultBody.ok(list);
    }

    /**
     * 查询企业
     */
    @ApiOperation(value = "企业-详细信息", notes = "查询企业详细信息")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "企业-添加", notes = "添加企业")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    /**
     * 更新企业
     */
    @ApiOperation(value = "企业-更新", notes = "只更新非空字段")
    @PostMapping(value = "edit")
    public ResultBody edit(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    @ApiOperation(value = "企业-删除", notes = "企业删除")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "企业认证审核", notes = "企业认证审核")
    @PostMapping(value = "setAuditState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "auditState", required = true, value = "审核状态", paramType = "form"),
            @ApiImplicitParam(name = "auditContent", required = true, value = "审核内容", paramType = "form"),
            @ApiImplicitParam(name = "companyAccount", value = "企业账户,需编写handler,实现UpdateInterceptor", paramType = "form"),
            @ApiImplicitParam(name = "roleCode", value = "角色编码", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "handlerName", paramType = "form")
    })
    public ResultBody setAuditState(@RequestParam(required = false) Map params) {
        return bizService.setAuditState(params);
    }

    @ApiOperation(value = "重置认证审核", notes = "重置企业认证")
    @PostMapping(value = "resetAuditState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "handlerName", paramType = "form")
    })
    public ResultBody resetAuditState(@RequestParam(value = "companyId") Long companyId) {
        return bizService.resetAuditState(companyId);
    }

    @ApiOperation(value = "设置推荐状态", notes = "设置推荐状态")
    @PostMapping(value = "setRecommend")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "recommend", required = true, value = "推荐状态", paramType = "query")
    })
    public ResultBody setRecommend(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "recommend") Integer recommend) {
        return bizService.setRecommend(ids, recommend);
    }


    @ApiOperation(value = "获取用户账套")
    @GetMapping(value = "getByAccount")
    @LoginRequired(required = false)
    public ResultBody getByAccount(@RequestParam(value = "accountName") String accountName) {
        return ResultBody.ok(bizService.getByAccount(accountName));
    }

    @ApiOperation(value = "获取分配企业列表")
    @GetMapping(value = "getAuthCompanyList")
    public ResultBody getAuthCompanyList(@RequestParam(value = "userId") Long userId, @RequestParam(value = "organizationId") Long organizationId) {
        return bizService.getAuthCompanyList(userId, organizationId);
    }

    @ApiOperation(value = "企业-排序", notes = "企业-排序")
    @PostMapping(value = "sort")
    public ResultBody upOrDown(@RequestParam(value = "companyId") Long companyId, Integer type) {
        return bizService.sort(companyId, type);
    }


}
