package com.meida.module.system.provider.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.handler.BaseCheckAuthHandler;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.system.client.constant.SystemConstant;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.mapper.SysDeptMapper;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门 实现类
 *
 * @author flyme
 * @date 2019-07-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Autowired(required = false)
    private BaseCheckAuthHandler baseCheckAuthHandler;

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysDept> cq, SysDept dept, EntityMap requestMap) {
        cq.select(SysDept.class, "*");
        cq.eq(SysDept.class,"companyId");
        return super.beforeListEntityMap(cq, dept, requestMap);
    }

    @Override
    public ResultBody beforePageList(CriteriaQuery<SysDept> cq, SysDept dept, EntityMap requestMap) {
        cq.select(SysDept.class, "*");
        return super.beforePageList(cq, dept, requestMap);
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysDept dept, EntityMap extra) {
        Long companyId = FlymeUtils.isNotEmpty(dept.getCompanyId()) ? dept.getCompanyId() : OpenHelper.getCompanyId();
        dept.setCompanyId(companyId);
        dept.setIsLeaf(1);
        if (FlymeUtils.isNotEmpty(baseCheckAuthHandler)) {
            Long count = countByCompanyId(companyId);
            Boolean tag = baseCheckAuthHandler.checkDeptCount(count.intValue());
            if (!tag) {
                return ResultBody.failed("添加失败:已达最大授权数量");
            }
        }
        Long parentId = FlymeUtils.isEmpty(dept.getParentId()) ? 0L : dept.getParentId();
        SysDept parent = getById(parentId);
        Long count = 0L;
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        if (parentId.equals(0L)) {
            count = count(cq.eq("parentId", cs.getParams("parentId")));
        } else {
            count = count(cq.eq("parentId", parent.getDeptId()));
        }
        Integer sortOrder = FlymeUtils.getInteger(count.intValue(), 0) + 1;
        dept.setParentId(parentId);
        dept.setOrderNo(sortOrder);
        Boolean column = checkByCompanyColumn(dept.getCompanyId(), 0L, dept.getDeptCode());
        ApiAssert.isFalse("部门代码已存在", column);

        return ResultBody.ok();
    }

    @Override
    public ResultBody afterAdd(CriteriaSave cs, SysDept dept, EntityMap extra) {
        Long parentId = dept.getParentId();
        if (FlymeUtils.isNotEmpty(parentId) && !parentId.equals(0L)) {
            SysDept parentDept = getById(parentId);
            parentDept.setIsLeaf(0);
            parentDept.updateById();
        }
        loadData2Redis();
        return super.afterAdd(cs, dept, extra);
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<SysDept> cu, SysDept dept, EntityMap extra) {
        Boolean column = checkByCompanyColumn(dept.getCompanyId(), dept.getDeptId(), dept.getDeptCode());
        ApiAssert.isFalse("部门代码已存在", column);
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, SysDept sysDept, EntityMap extra) {
        loadData2Redis();
        return super.afterEdit(cu, sysDept, extra);
    }

    @Override
    public List<EntityMap> deptTreeList(Long parentId, Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "deptId as id", "deptName as label", "parentId", "isLeaf", "disabled", "icon");
        cq.eq("parentId", parentId);
        cq.eq("companyId", companyId);
        cq.orderByAsc("orderNo");
        List<EntityMap> deptList = selectEntityMap(cq);
        return deptList;
    }

    @Override
    public List<EntityMap> selectTreeList(Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "deptId as id", "deptName as label", "parentId", "isLeaf levels", "icon");
        cq.eq(true,"companyId",companyId);
        cq.orderByAsc("orderNo");
        List<EntityMap> allList = selectEntityMap(cq);
        List<EntityMap> result = buildTree(allList, 0L);
        return result;
    }

    /**
     * 递归构建
     *
     * @param list
     * @param parentId
     * @return
     */
    private static List<EntityMap> buildTree(List<EntityMap> list, Long parentId) {
        List<EntityMap> result = new ArrayList();
        list.forEach(dept -> {
            Long id = dept.getLong("id");
            Long pid = dept.getLong("parentId");
            Integer isLeaf = dept.getInt("level");
            dept.put("leaf",false);
            if(FlymeUtils.isNotEmpty(isLeaf)&&isLeaf.equals(1)){
                dept.put("leaf",true);
            }
            if (parentId.equals(pid)) {
                List child = buildTree(list, id);
                dept.put("children", child);
                result.add(dept);
            }
        });
        return result;
    }


    @Override
    public ResultBody beforeDelete(CriteriaDelete<SysDept> cd) {
        Long id = cd.getIdValue();
        if (FlymeUtils.isNotEmpty(id)) {
            Long childCount = countByParentId(id);
            if (childCount > 0) {
                return ResultBody.failed("删除失败:已存在下级部门，请先删除下级部门");
            }
            SysDept sysDept = getById(id);
            if (sysDept.getArcInfoCount() > 0) {
                return ResultBody.failed("删除失败:该部门下有档案数据，请先删除档案数据");
            }

        }

        return super.beforeDelete(cd);
    }

    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        loadData2Redis();
        return super.afterDelete(cd, ids);
    }

    @Override
    public Long countByParentId(Long deptId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.eq(true, "parentId", deptId);
        return count(cq);
    }

    @Override
    public Long countByCompanyId(Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.eq(true, "companyId", companyId);
        return count(cq);
    }


    @Override
    public List<EntityMap> selectDeptByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "*");
        cq.eq(true, "bud.userId", userId);
        cq.createJoin("com.meida.module.admin.client.entity.BaseUserDept").setMainField("deptId").setJoinField("deptId");
        return selectEntityMap(cq);
    }

    @Override
    public List<Long> selectDeptByUserId(Long userId, Long companyId) {
        List<Long> result = new ArrayList<>();
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "deptId");
        cq.eq(true, "bud.userId", userId);
        cq.eq(true, "dept.companyId", companyId);
        cq.createJoin("com.meida.module.admin.client.entity.BaseUserDept").setMainField("deptId").setJoinField("deptId");
        List<EntityMap> list = selectEntityMap(cq);
        if (FlymeUtils.isNotEmpty(list)) {
            for (EntityMap entityMap : list) {
                Long deptId = entityMap.getLong("deptId");
                result.add(deptId);
            }
        }
        return result;
    }

    @Override
    public List<EntityMap> treeByUserId(Long userId, Long parentId, Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "deptId as id", "deptName as label", "parentId", "isLeaf", "disabled", "icon", "deptNo", "deptCode");
        cq.eq("parentId", parentId);
        cq.eq("companyId", companyId);
        cq.eq(true, "bud.userId", userId);
        cq.createJoin("com.meida.module.admin.client.entity.BaseUserDept").setMainField("deptId").setJoinField("deptId");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody sort(Long deptId, Integer type) {
        ApiAssert.isNotEmpty("deptId不能为空", deptId);
        ApiAssert.isNotEmpty("移动类型不能为空", type);
        //排序为从小到大排   type为1为上移  2为下移
        if (type != 1 && type != 2) {
            ApiAssert.failure("移动类型只能为上移或下移");
        }
        SysDept cate = this.getById(deptId);
        CriteriaQuery<SysDept> query = new CriteriaQuery<>(SysDept.class);
        //查询同级别的
        query.lambda().eq(SysDept::getParentId, cate.getParentId());
        query.orderByAsc("orderNo");
        List<SysDept> list = this.list(query);
        int sourceIndex = -1;
        int targetIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            SysDept obj = list.get(i);
            if (obj.getDeptId().equals(deptId)) {
                if (type == 1) {//上移
                    if (i == 0) {
                        return ResultBody.failed("无法上移");
                    } else {
                        sourceIndex = i;
                        targetIndex = i - 1;
                    }
                } else if (type == 2) {//下移
                    if (i == list.size() - 1) {
                        return ResultBody.failed("无法下移");
                    } else {
                        sourceIndex = i;
                        targetIndex = i + 1;
                    }
                }
            }
        }
        if (sourceIndex < 0 || targetIndex < 0) {
            return ResultBody.failed("移动错误");
        }
        Long targetId = list.get(sourceIndex).getDeptId();
        list.get(sourceIndex).setDeptId(list.get(targetIndex).getDeptId());
        list.get(targetIndex).setDeptId(targetId);
        List<SysDept> updateObj = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            SysDept obj = new SysDept();
            obj.setDeptId(list.get(i).getDeptId());
            obj.setOrderNo(i);
            updateObj.add(obj);
        }
        this.saveOrUpdateBatch(updateObj);
        return ResultBody.ok();
    }

    @Override
    public Boolean checkByCompanyColumn(Long companyId, Long deptId, String deptCode) {
        QueryWrapper<SysDept> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().ne(deptId > 0, SysDept::getDeptId, deptId).eq(SysDept::getCompanyId, companyId).eq(SysDept::getDeptCode, deptCode);
        long count = count(queryWrapper);
        return count > 0 ? true : false;

    }

    @Override
    public synchronized void loadData2Redis() {
        if (redisUtils.hasKey(SystemConstant.SYS_DEPT_PREFIX)) {
            redisUtils.del(SystemConstant.SYS_DEPT_PREFIX);
        }
        List<SysDept> list = this.list();
        redisUtils.setList(SystemConstant.SYS_DEPT_PREFIX, list);
    }

    @Override
    public List<SysDept> selectByCompanyId(Long companyId) {
        return list(new QueryWrapper<SysDept>().eq("companyId", companyId));
    }

    @Override
    public List<EntityMap> treeOtherByUserId(Long userId, Long parentId, Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(SysDept.class);
        cq.select(SysDept.class, "deptId as id", "deptName as label", "parentId", "isLeaf", "disabled", "icon", "deptNo", "deptCode");
        cq.eq("parentId", parentId);
        cq.eq("companyId", companyId);
        cq.last("and deptId not in  (select deptId from base_user_dept where userId=" + userId + " )");
        return selectEntityMap(cq);
    }
}
