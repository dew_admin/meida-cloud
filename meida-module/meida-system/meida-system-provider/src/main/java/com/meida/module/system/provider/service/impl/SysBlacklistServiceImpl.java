package com.meida.module.system.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.system.client.entity.SysBlacklist;
import com.meida.module.system.provider.mapper.SysBlacklistMapper;
import com.meida.module.system.provider.service.SysBlacklistService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 黑名单 实现类
 *
 * @author flyme
 * @date 2019-07-04
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysBlacklistServiceImpl extends BaseServiceImpl<SysBlacklistMapper, SysBlacklist> implements SysBlacklistService {



    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforeListEntityMap(CriteriaQuery<SysBlacklist> cq, SysBlacklist sysBlacklist, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(SysBlacklist.class, "blacklistId", "targetId");
        cq.eq(SysBlacklist.class, "ownerId", userId);
        return super.beforeListEntityMap(cq, sysBlacklist, requestMap);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysBlacklist> cq, SysBlacklist sysBlacklist, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(SysBlacklist.class, "blacklistId", "targetId", "targetName", "createTime");
        cq.eq(SysBlacklist.class, "ownerId", userId);
        return super.beforePageList(cq, sysBlacklist, requestMap);
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysBlacklist blacklist,EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        blacklist.setOwnerId( userId);
        return ResultBody.ok();
    }
}
