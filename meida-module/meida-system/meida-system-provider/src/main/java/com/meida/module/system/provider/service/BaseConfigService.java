package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.system.client.entity.SysConfig;

import java.util.List;
import java.util.Map;

/**
 * 系统配置表 服务类
 *
 * @author flyme
 * @date 2019-06-10
 */
public interface BaseConfigService extends IBaseService<SysConfig> {

    /**
     * 初始化系统配置到redis
     */
    void initBaseConfig();

    /**
     * 根据类型查询
     *
     * @param configType
     * @return
     */
    List<SysConfig> listByType(String configType);

    /**
     * 保存文件存储配置
     *
     * @param params
     * @return
     */
    ResultBody saveOss(Map params);

    /**
     * 保存短信配置
     *
     * @param params
     * @return
     */
    ResultBody saveSms(Map params);

    /**
     * 保存推送配置
     *
     * @param params
     * @return
     */
    ResultBody savePush(Map params);

    /**
     * 保存即时通讯
     *
     * @param params
     * @return
     */
    ResultBody saveIm(Map params);

    /**
     * 保存能配置
     *
     * @param params
     * @return
     */
    ResultBody globalConfigSave(Map params);

    /**
     * 保存配置
     *
     * @param params
     * @return
     */
    ResultBody configSave(Map params);
}
