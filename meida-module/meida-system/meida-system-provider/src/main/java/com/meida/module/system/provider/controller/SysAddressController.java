package com.meida.module.system.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysAddress;
import com.meida.module.system.provider.service.SysAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 收货地址控制器
 *
 * @author flyme
 * @date 2019-11-28
 */
@RestController
@RequestMapping("/system/address/")
@Api(tags = "基础模块-收货地址管理")
public class SysAddressController extends BaseController<SysAddressService, SysAddress> {


    @ApiOperation(value = "收货地址-列表", notes = "收货地址列表 <a href=\"#\" target='_blank'>参数说明</a>")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "收货地址-添加", notes = "添加收货地址")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "收货地址-更新", notes = "更新收货地址")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "收货地址-删除", notes = "删除收货地址")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "收货地址-详情", notes = "收货地址详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


}
