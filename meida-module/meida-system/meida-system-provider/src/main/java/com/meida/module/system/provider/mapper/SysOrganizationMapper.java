package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysOrganization;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 机构(集团) Mapper 接口
 * @author flyme
 * @date 2020-07-24
 */
public interface SysOrganizationMapper extends SuperMapper<SysOrganization> {

}
