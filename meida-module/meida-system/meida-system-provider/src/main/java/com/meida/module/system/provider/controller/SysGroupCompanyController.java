package com.meida.module.system.provider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysCompanyGroup;
import com.meida.module.system.provider.service.SysGroupCompanyService;

import javax.swing.*;
import java.util.Map;


/**
 * 企业自定义分组表控制器
 *
 * @author flyme
 * @date 2019-08-05
 */
@RestController
@RequestMapping("/company/sgc")
@Api(tags = "基础模块-企业分组")
public class SysGroupCompanyController extends BaseController<SysGroupCompanyService, SysCompanyGroup> {

    @ApiOperation(value = "企业自定义分组表-分页列表", notes = "企业分组中间表分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "企业自定义分组表-列表", notes = "企业自定义分组表列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "企业自定义分组表-添加", notes = "添加企业自定义分组表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "企业自定义分组表-更新", notes = "更新企业自定义分组表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "企业自定义分组表-删除", notes = "删除企业自定义分组表")
    @PostMapping(value = "remove")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "企业自定义分组表-详情", notes = "企业自定义分组表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
