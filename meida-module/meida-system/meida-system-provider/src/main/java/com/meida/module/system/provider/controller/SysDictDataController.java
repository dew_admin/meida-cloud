package com.meida.module.system.provider.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.meida.common.annotation.LoginRequired;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysDict;
import com.meida.module.system.client.entity.SysDictData;
import com.meida.module.system.provider.service.SysDictDataService;
import com.meida.module.system.provider.service.SysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;


/**
 * @author zyf
 */
@Slf4j
@RestController
@Api(tags = "基础模块-字典管理")
@RequestMapping("/dictdata/")
public class SysDictDataController extends BaseController<SysDictDataService, SysDictData> {

    @Autowired
    private SysDictService dictService;


    @GetMapping(value = "/listByDictType/{dictType}")
    @ApiOperation(value = "通过字典类型获取字典值", notes = "字典类型为字段名称")
    public ResultBody<EntityMap> listByDictType(@PathVariable String dictType) {
        List<EntityMap> list = bizService.listByDictType(dictType);
        return ResultBody.ok(list);
    }

    @GetMapping(value = "getByTypes")
    @ApiImplicitParam(name = "dictTypes", required = true, value = "字典类型,可传多个,逗号隔开", paramType = "form")
    @ApiOperation(value = "通过字典类型获取字典值", notes = "字典类型为字段名称")
    @LoginRequired(required = false)
    public ResultBody getByTypes(@RequestParam String[] dictTypes) {
        EntityMap map = bizService.selectByTypes(dictTypes);
        return ResultBody.ok(map);
    }


    @ApiOperation(value = "字典数据-分页列表", notes = "分页列表 ")
    @GetMapping(value = "page")
    @ApiIgnore
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "字典数据-列表", notes = "列表 ")
    @GetMapping(value = "list")
    @ApiIgnore
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }


    @ApiOperation(value = "字典数据-删除", notes = "删除")
    @PostMapping(value = "remove")
    @ApiIgnore
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "字典值-添加", notes = "添加字典值")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "字典值-更新", notes = "更新字典值")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    @DeleteMapping(value = "/delByIds/{ids}")
    @ApiOperation(value = "批量通过id删除")
    @ApiIgnore
    public ResultBody<Object> delByIds(@PathVariable Long[] ids) {
        bizService.removeByIds(CollectionUtil.toList(ids));
        dictService.initDictRedis();
        return ResultBody.ok("批量通过id删除数据成功");
    }

    @ApiOperation(value = "字典值-启用禁用状态设置", notes = "设置启用禁用状态")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map params, @RequestParam(value = "state") Integer state) {
        return bizService.setState(params, "state", state);
    }


}
