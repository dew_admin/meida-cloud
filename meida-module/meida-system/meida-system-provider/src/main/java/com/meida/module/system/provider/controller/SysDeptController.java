package com.meida.module.system.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;


/**
 * 部门控制器
 *
 * @author flyme
 * @date 2019-07-05
 */
@RestController
@RequestMapping("/system/dept/")
@Api(tags = "公共服务")
public class SysDeptController extends BaseController<SysDeptService, SysDept> {

    @ApiOperation(value = "部门-分页列表", notes = "部门分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "部门-列表", notes = "部门列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "部门-树形列表", notes = "树形列表")
    @GetMapping(value = "tree")
    public ResultBody tree(@RequestParam(required = false) Long parentId, @RequestParam(required = false) Long companyId) {
        List<EntityMap> list = bizService.deptTreeList(parentId, companyId);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "查询用户已分配的部门")
    @GetMapping(value = "treeByUserId")
    public ResultBody listByUserId(@RequestParam Long userId,@RequestParam(required = false) Long parentId, @RequestParam(required = false) Long companyId) {
        List<EntityMap> list = bizService.treeByUserId(userId,parentId,companyId);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "查询用户未分配的部门")
    @GetMapping(value = "treeOtherByUserId")
    public ResultBody treeOtherByUserId(@RequestParam Long userId, @RequestParam(required = false) Long parentId, @RequestParam(required = false) Long companyId) {
        List<EntityMap> list = bizService.treeOtherByUserId(userId, parentId, companyId);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "查询用户已分配的部门")
    @GetMapping(value = "listByUserId")
    public ResultBody listByUserId(@RequestParam Long userId) {
        List<EntityMap> list = bizService.selectDeptByUserId(userId);
        return ResultBody.ok(list);
    }


    @ApiOperation(value = "查询部门")
    @GetMapping(value = "selectByCompanyId")
    public ResultBody selectByCompanyId(@RequestParam Long companyId) {
        List<EntityMap> list = bizService.selectTreeList(companyId);
        return ResultBody.ok(list);
    }



    @ApiOperation(value = "部门-添加", notes = "添加部门")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "部门-更新", notes = "更新部门")
    @PostMapping(value = "edit")
    @ApiIgnore
    public ResultBody edit(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "部门-删除", notes = "删除部门")
    @PostMapping(value = "remove")
    @ApiIgnore
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "部门-详情", notes = "部门详情")
    @GetMapping(value = "get")
    @ApiIgnore
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "部门-排序", notes = "部门-排序")
    @PostMapping(value = "sort")
    public ResultBody sort(@RequestParam(value = "deptId") Long deptId, Integer type) {
        return bizService.sort(deptId, type);
    }

}
