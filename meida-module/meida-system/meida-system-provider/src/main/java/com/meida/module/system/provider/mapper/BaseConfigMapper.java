package com.meida.module.system.provider.mapper;


import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysConfig;


/**
 * 系统配置表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-10
 */
public interface BaseConfigMapper extends SuperMapper<SysConfig> {

}
