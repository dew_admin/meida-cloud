package com.meida.module.system.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.system.client.entity.SysColleconGroup;
import com.meida.module.system.provider.mapper.SysGroupColleconMapper;
import com.meida.module.system.provider.service.SysGroupColleconService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 收藏分组中间表 实现类
 *
 * @author flyme
 * @date 2019-06-16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysGroupColleconServiceImpl extends BaseServiceImpl<SysGroupColleconMapper, SysColleconGroup> implements SysGroupColleconService {
    @Override
    public Boolean deleteByColleconIds(Long[] colleconIds) {
        UpdateWrapper<SysColleconGroup> up = new UpdateWrapper<>();
        return remove(up.in("colleconId", colleconIds));
    }

    @Override
    public Boolean setColleconGroup(Long[] colleconIds, Long[] groupIds) {
        if (ObjectUtils.isNotNull(colleconIds, groupIds)) {
            List<SysColleconGroup> colleconGroups = new ArrayList<>();
            for (Long groupId : groupIds) {
                for (Long colleconId : colleconIds) {
                    SysColleconGroup colleconGroup = new SysColleconGroup();
                    colleconGroup.setColleconId(colleconId);
                    colleconGroup.setGroupId(groupId);
                    colleconGroups.add(colleconGroup);
                }
            }
            return saveBatch(colleconGroups);
        } else {
            return false;
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Long> selectGroupId(Long colleconId) {
        CriteriaQuery qw = new CriteriaQuery(entityClass);
        qw.select("groupId");
        qw.eq(true, "colleconId", colleconId);
        List<Long> list = selectLongs(qw);
        return list;
    }


}
