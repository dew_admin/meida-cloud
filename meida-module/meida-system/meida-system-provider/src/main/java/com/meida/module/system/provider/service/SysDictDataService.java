package com.meida.module.system.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.system.client.entity.SysDictData;

import java.util.List;

/**
 * 字典数据接口
 *
 * @author flyme
 */
public interface SysDictDataService extends IBaseService<SysDictData> {


    /**
     * 通过dictId获取启用字典 已排序
     */
    List<EntityMap> listByDictId(Long dictId);

    /**
     * 通过dictType获取启用字典 已排序
     */
    List<EntityMap> listByDictType(String dictType);

    /**
     * 通过dictId删除
     */
    void deleteByDictId(Long dictId);

    /**
     * 根据上级id查询
     */
    List<EntityMap> selectByDictId(Long id);

    /**
     * 根据dictTypes查询
     */
    EntityMap selectByTypes(String[] dictTypes);



}