package com.meida.module.system.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.service.SysOrganizationService;


/**
 * 机构(集团)控制器
 *
 * @author flyme
 * @date 2020-07-24
 */
@RestController
@RequestMapping("/system/organization/")
public class SysOrganizationController extends BaseController<SysOrganizationService, SysOrganization>  {

    @ApiOperation(value = "机构(集团)-分页列表", notes = "机构(集团)分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "机构(集团)-列表", notes = "机构(集团)列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "机构(集团)-添加", notes = "添加机构(集团)")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "机构(集团)-更新", notes = "更新机构(集团)")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "机构(集团)-删除", notes = "删除机构(集团)")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "机构(集团)-详情", notes = "机构(集团)详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
