package com.meida.module.system.provider.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.annotation.LoginRequired;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysDict;
import com.meida.module.system.provider.service.SysDictDataService;
import com.meida.module.system.provider.service.SysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;


/**
 * @author flyme
 */
@Slf4j
@Api(tags = "基础模块-字典分类管理")
@RestController
@RequestMapping("/dict")
public class SysDictController extends BaseController<SysDictService, SysDict> {


    @Autowired
    private SysDictDataService dictDataService;
    @Autowired
    private SysDictService dictService;


    @ApiOperation(value = "获取全部数据")
    @GetMapping(value = "/list")
    public ResultBody list() {
        QueryWrapper qw = new QueryWrapper<Map<String, Object>>();
        qw.select("dictId", "dicTitle", "dicType", "sortOrder", "parentId");
        List list = bizService.listMaps(qw);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "字典分类-添加", notes = "添加字典分类")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }


    @ApiOperation(value = "字典分类-更新", notes = "更新字典分类")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "通过id删除")
    @DeleteMapping(value = "/delByIds/{id}")
    @ApiIgnore
    public ResultBody<Object> delAllByIds(@PathVariable Long id) {
        bizService.removeById(id);
        dictDataService.deleteByDictId(id);
        dictService.initDictRedis();
        return ResultBody.ok("删除成功");
    }


    @ApiOperation(value = "字典分类-删除", notes = "删除字典分类")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "搜索字典")
    @GetMapping(value = "/search")
    @ApiIgnore
    public ResultBody<List<SysDict>> search(@RequestParam String key) {
        List<SysDict> list = bizService.search(key);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "获取字典")
    @GetMapping(value = "/initDict")
    @LoginRequired(required = false)
    public ResultBody initDict() {
        return ResultBody.ok(bizService.initDict());
    }

}
