package com.meida.module.system.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysFormula;
import com.meida.module.system.provider.service.SysFormulaService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 公式控制器
 *
 * @author flyme
 * @date 2020-08-08
 */
@RestController
@RequestMapping("/system/formula/")
public class SysFormulaController extends BaseController<SysFormulaService, SysFormula> {

    @ApiOperation(value = "公式-分页列表", notes = "公式分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "公式-列表", notes = "公式列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "公式-添加", notes = "添加公式")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "公式-更新", notes = "更新公式")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "公式-删除", notes = "删除公式")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "公式-详情", notes = "公式详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
    @ApiOperation(value = "更新公式", notes = "更新公式")
    @PostMapping(value = "setStatus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "status", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setStatus(@RequestParam(required = false) Map map, @RequestParam(value = "status") Integer status) {
            return bizService.setState(map, "status", status);
            }

}
