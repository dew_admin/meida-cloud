package com.meida.module.system.provider.service;

import com.meida.module.system.client.entity.SysBlacklist;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 黑名单 接口
 *
 * @author flyme
 * @date 2019-07-04
 */
public interface SysBlacklistService extends IBaseService<SysBlacklist> {

}
