package com.meida.module.system.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysCompanyInfo;
import com.meida.module.system.provider.mapper.SysCompanyInfoMapper;
import com.meida.module.system.provider.service.SysAreaService;
import com.meida.module.system.provider.service.SysCompanyInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 企业详情表 服务实现类
 *
 * @author flyme
 * @date 2019-06-02
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysCompanyInfoServiceImpl extends BaseServiceImpl<SysCompanyInfoMapper, SysCompanyInfo> implements SysCompanyInfoService {

    @Resource
    private SysAreaService areaService;

    @Override
    public Boolean saveCompanyInfo(SysCompany company, EntityMap params) {
        SysCompanyInfo companyInfo = new SysCompanyInfo();
        Long proId = params.getLong("proId");
        Long cityId = params.getLong("cityId");
        Long areaId = params.getLong("areaId");
        String areaName = areaService.getAreaName(proId, cityId, areaId);
        companyInfo.setAreaName(areaName);
        return saveOrUpdate(companyInfo);
    }
}
