package com.meida.module.system.provider.mapper;

import com.meida.module.system.client.entity.SysBlacklist;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 黑名单 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-04
 */
public interface SysBlacklistMapper extends SuperMapper<SysBlacklist> {

}
