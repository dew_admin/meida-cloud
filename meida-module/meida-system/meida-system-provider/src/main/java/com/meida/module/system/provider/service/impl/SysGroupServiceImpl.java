package com.meida.module.system.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.system.client.entity.SysGroup;
import com.meida.module.system.provider.mapper.SysGroupMapper;
import com.meida.module.system.provider.service.SysGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 分组 实现类
 *
 * @author flyme
 * @date 2019-06-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysGroupServiceImpl extends BaseServiceImpl<SysGroupMapper, SysGroup> implements SysGroupService {



    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysGroup> cq, SysGroup sysGroup, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        cq.select(SysGroup.class, "groupId", "groupName");
        cq.eq(SysGroup.class, "targetEntity");
        cq.eq(SysGroup.class, "groupName");
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(SysGroup.class, "companyId", companyId);
        }
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.eq(SysGroup.class, "userId", userId);
        }
        return super.beforeListEntityMap(cq, sysGroup, requestMap);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody listByComapny(Map params) {
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(params, SysGroup.class);
        cq.select(SysGroup.class, "groupId", "groupName");
        cq.eq(SysGroup.class, "targetEntity");
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.eq(SysGroup.class, "userId", userId);
        }
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(SysGroup.class, "companyId", companyId);
        }
        return baseList(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectByGroupId(List<Long> groupIds) {
        CriteriaQuery cq = new CriteriaQuery(entityClass);
        cq.select(SysGroup.class, "groupId", "groupName");
        cq.in(true, "groupId", groupIds);
        return selectEntityMap(cq);
    }

    @Override
    public SysGroup findByGroupCode(String groupCode) {
        CriteriaQuery<SysGroup> cq = new CriteriaQuery(SysGroup.class);
        cq.eq(true, "groupCode", groupCode);
        return getOne(cq);
    }

    @Override
    public ResultBody beforePageList(CriteriaQuery<SysGroup> cq, SysGroup sysGroup, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        cq.select(SysGroup.class, "*");
        cq.eq(SysGroup.class, "groupName");
        cq.eq(SysGroup.class, "targetEntity");
        if (FlymeUtils.isNotEmpty(companyId)) {
            cq.eq(SysGroup.class, "companyId", companyId);
        }
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.eq(SysGroup.class, "userId", userId);
        }
        return super.beforePageList(cq, sysGroup, requestMap);
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysGroup sysGroup, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        Long userId = OpenHelper.getUserId();
        sysGroup.setCompanyId(companyId);
        sysGroup.setUserId(userId);
        return ResultBody.ok();
    }
}
