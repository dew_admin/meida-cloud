package com.meida.module.system.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.system.client.entity.SysOrganization;
import com.meida.module.system.provider.mapper.SysOrganizationMapper;
import com.meida.module.system.provider.service.SysOrganizationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 机构(集团)接口实现类
 *
 * @author flyme
 * @date 2020-07-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysOrganizationServiceImpl extends BaseServiceImpl<SysOrganizationMapper, SysOrganization> implements SysOrganizationService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysOrganization organization, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysOrganization> cq, SysOrganization organization, EntityMap requestMap) {
      cq.eq(SysOrganization.class,"organizationName");
      cq.orderByDesc("organization.createTime");
      return ResultBody.ok();
    }


}
