package com.meida.module.system.provider.listener;

import com.meida.module.system.provider.service.BaseConfigService;
import com.meida.module.system.provider.service.SysAreaService;
import com.meida.module.system.provider.service.SysDictService;
import com.meida.module.system.provider.service.SysExpireStrategyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author: flyme
 * @date: 2019/2/12 16:24
 * @desc: 初始化系统配置
 */
@Slf4j
@Component("systemInitListener")
@Order(1)
@AllArgsConstructor
public class SystemInitListener  {

    private final SysDictService dictService;

    private final SysAreaService areaService;

    private final BaseConfigService baseConfigService;

    private final SysExpireStrategyService sysExpireStrategyService;


    @PostConstruct
    @Order(1)
    public void init() {
        log.info("********************系统服务已启动,准备初始化配置***********************");
        dictService.initDictRedis();
        areaService.initArea();
        baseConfigService.initBaseConfig();
        sysExpireStrategyService.setExpireKeyToRedis();
        log.info("********************数据初始化完成************************************");
    }

}
