package com.meida.module.system.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.system.client.entity.SysFormula;

/**
 * 公式 接口
 *
 * @author flyme
 * @date 2020-08-08
 */
public interface SysFormulaService extends IBaseService<SysFormula> {

}
