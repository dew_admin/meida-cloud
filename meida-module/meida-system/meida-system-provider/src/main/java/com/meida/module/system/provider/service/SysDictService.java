package com.meida.module.system.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.system.client.entity.SysDict;

import java.util.List;
import java.util.Map;

/**
 * 字典接口
 *
 * @author flyme
 */
public interface SysDictService extends IBaseService<SysDict> {

    /**
     * 排序获取全部
     *
     * @return
     */
    List<SysDict> selectAll();

    /**
     * 通过type获取
     *
     * @param type
     * @return
     */
    SysDict findByType(String type);

    /**
     * 模糊搜索
     *
     * @param key
     * @return
     */
    List<SysDict> search(String key);

    /**
     * 初始化字典到redis
     */
    void initDictRedis();


    /**
     * 根据code查询
     */
    List<EntityMap> selectByCode(String dicType);

    /**
     * 从缓存读取dict
     * @return
     */
    Map<Object, Object> initDict();
}