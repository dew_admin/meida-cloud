package com.meida.module.system.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysCompanyInfo;

/**
 * 企业详情表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-02
 */
public interface SysCompanyInfoMapper extends SuperMapper<SysCompanyInfo> {

}
