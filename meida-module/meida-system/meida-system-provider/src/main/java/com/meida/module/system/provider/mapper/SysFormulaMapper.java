package com.meida.module.system.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.system.client.entity.SysFormula;


/**
 * 公式 Mapper 接口
 * @author flyme
 * @date 2020-08-08
 */
public interface SysFormulaMapper extends SuperMapper<SysFormula> {

}
