package com.meida.module.system.provider.service;

import com.meida.common.base.service.BaseColleconService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.system.client.entity.SysCollecon;
import com.meida.common.enums.ColleconEnum;

import java.util.List;

/**
 * 收藏点赞关注表 服务类
 *
 * @author flyme
 * @date 2019-06-13
 */
public interface SysColleconService extends IBaseService<SysCollecon>, BaseColleconService {
    /**
     * 查找用户收藏关注记录
     */
    SysCollecon findCollecon(Long targetId, Long userId, ColleconEnum colleconEnum, String entityName);

    /**
     * 用户收藏关注
     */
    ResultBody collecon(Long targetId, Long targetUserId, ColleconEnum colleconEnum, String entityName, Long[] groupIds);

    /**
     * 批量设置分组
     */
    Boolean setGroup(Long[] colleconIds, Long[] groupIds);


    /**
     * 统计某个对象被收藏/点赞/关注记录
     */
    long countAllCollecon(Long targetId, ColleconEnum colleconEnum, Class cls);

    /**
     * 统计企业被收藏数量
     */
    long countCountByTargetCompany(Long targetCompanyId, ColleconEnum colleconEnum, Class cls);

    /**
     * 统计某用户收藏/点赞/关注数量
     */
    long countCountByUser(Long userId, ColleconEnum colleconEnum, Class cls);

    /**
     * 查询企业热门收藏产品排行
     *
     * @param companyId
     * @param limit
     * @return
     */
    List<EntityMap> selectColleconProductByCompanyId(Long companyId, int limit);

    /**
     * 统计近一个月的关注数量
     *
     * @return
     */
    EntityMap totalByTargetEntity();

    /**
     * 统计某用户被收藏/点赞/关注数量
     */
    long countCountByTargetUser(Long targetUserId, ColleconEnum colleconEnum, String targetEntity);

}
