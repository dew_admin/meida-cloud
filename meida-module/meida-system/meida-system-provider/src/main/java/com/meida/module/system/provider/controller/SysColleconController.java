package com.meida.module.system.provider.controller;


import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.system.client.entity.SysCollecon;
import com.meida.common.enums.ColleconEnum;
import com.meida.module.system.provider.service.SysColleconService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 收藏点赞关注表控制器
 *
 * @author flyme
 * @date 2019-06-13
 */
@RestController
@Api(tags = "基础模块-通用收藏关注点赞")
@RequestMapping("/collecon/")
public class SysColleconController extends BaseController<SysColleconService, SysCollecon> {


    @ApiOperation(value = "收藏关注列表", notes = "收藏关注列表,handlerName默认collecon <a href=\"javascript:openParamInfo('meida-base-provider','/collecon/list');\">参数说明</a>")
    @GetMapping(value = "page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "handlerName", required = true, value = "handlerName", paramType = "form"),
            @ApiImplicitParam(name = "entityName", required = true, value = "目标实体类型", paramType = "form")
    })
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "添加/取消收藏关注", notes = "添加/取消收藏关注")
    @PostMapping(value = "add")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "targetId", required = true, value = "目标ID", paramType = "form"),
            @ApiImplicitParam(name = "optType", required = true, value = "操作类型", paramType = "query"),
            @ApiImplicitParam(name = "entityName", required = true, value = "目标实体类型", paramType = "form")
    })
    public ResultBody collecon(@RequestParam(value = "targetId") Long targetId,@RequestParam(value = "targetUserId",required = false) Long targetUserId, @RequestParam ColleconEnum optType, @RequestParam String entityName, @RequestParam(value = "groupIds", required = false) Long[] groupIds) {
        return bizService.collecon(targetId, targetUserId,optType, entityName, groupIds);
    }


    @ApiOperation(value = "删除收藏关注", notes = "可批量删除")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "设置收藏关注分组", notes = "可批量设置")
    @PostMapping(value = "setGroup")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "colleconIds", required = true, value = "对象ID", paramType = "form"),
            @ApiImplicitParam(name = "groupIds", required = true, value = "分组ID", paramType = "form")
    })
    public ResultBody batchDel(@RequestParam(value = "colleconIds") Long[] colleconIds, @RequestParam(value = "groupIds") Long[] groupIds) {
        bizService.setGroup(colleconIds, groupIds);
        return ResultBody.msg("设置成功");
    }


    @ApiOperation(value = "企业被收藏热门产品", notes = "企业被收藏热门产品")
    @GetMapping(value = "product")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", required = true, value = "企业ID", paramType = "form"),
            @ApiImplicitParam(name = "limit", required = true, value = "查询条数", paramType = "form")
    })
    public ResultBody product(@RequestParam(value = "companyId") Long companyId, @RequestParam(value = "limit", defaultValue = "5") Integer limit) {
        List<EntityMap> result = bizService.selectColleconProductByCompanyId(companyId, limit);
        return ResultBody.ok(result);
    }



    @ApiOperation(value = "店铺收藏关注近一月统计", notes = "店铺收藏关注近一月统计")
    @GetMapping(value = "totalByTargetEntity")
    public ResultBody totalByTargetEntity() {
        EntityMap result = bizService.totalByTargetEntity();
        return ResultBody.ok(result);
    }
}
