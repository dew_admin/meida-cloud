package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 收藏分组表
 *
 * @author flyme
 * @date 2019-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_collecon_group")
@ApiModel(value = "SysColleconGroup对象", description = "收藏分组中间表")
@TableAlias("sgc")
public class SysColleconGroup extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sgcId", type = IdType.ASSIGN_ID)
    private Long sgcId;

    @ApiModelProperty(value = "收藏ID")
    private Long colleconId;

    @ApiModelProperty(value = "分组ID")
    private Long groupId;

}
