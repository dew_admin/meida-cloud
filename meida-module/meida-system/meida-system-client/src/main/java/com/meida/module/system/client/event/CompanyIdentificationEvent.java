package com.meida.module.system.client.event;

import com.meida.module.system.client.entity.SysCompany;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 企业认证事件
 */
@Getter
public class CompanyIdentificationEvent extends ApplicationEvent {


    private SysCompany company;

    /**
     * 重写构造函数
     */
    public CompanyIdentificationEvent(Object source, SysCompany company) {
        super(source);
        this.company = company;
    }
}
