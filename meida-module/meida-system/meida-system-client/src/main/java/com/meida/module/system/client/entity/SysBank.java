package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 银行信息
 *
 * @author flyme
 * @date 2020-08-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_bank")
@TableAlias("bank")
@ApiModel(value="SysBank对象", description="银行信息")
public class SysBank extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "bankId", type = IdType.ASSIGN_ID)
    private Long bankId;

    @ApiModelProperty(value = "开户行")
    private String bankName;

    @ApiModelProperty(value = "账户")
    private String bankNo;

    @ApiModelProperty(value = "开户人")
    private String bankOwner;

    @ApiModelProperty(value = "开户地址")
    private String bankAddress;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "机构")
    private Long organizationId;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "银行状态")
    private Integer bankState;

}
