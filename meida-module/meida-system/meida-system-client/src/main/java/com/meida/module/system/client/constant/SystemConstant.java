package com.meida.module.system.client.constant;

public interface SystemConstant {

    /**
     * 省
     */
    Integer AREA_PRO = 1;

    /**
     * 市
     */
    Integer AREA_CITY = 2;

    /**
     * 区
     */
    Integer AREA_QU = 3;

    /**
     * 未认证
     */
    Integer COMPANY_STATES_NOAUTH = 0;
    /**
     * 已认证
     */
    Integer COMPANY_STATES_AUTH = 1;


    //部门缓存前缀
    String SYS_DEPT_PREFIX = "arc_dept";
}
