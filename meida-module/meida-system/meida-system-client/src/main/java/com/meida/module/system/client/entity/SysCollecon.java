package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 收藏点赞关注表
 *
 * @author flyme
 * @date 2019-06-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "SysCollecon对象", description = "收藏点赞关注表")
@TableName("sys_collecon")
@TableAlias("collecon")
public class SysCollecon extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "colleconId", type = IdType.ASSIGN_ID)
    private Long colleconId;

    @ApiModelProperty(value = "收藏目标Id")
    private Long targetId;

    @ApiModelProperty(value = "目标企业ID")
    private Long targetCompanyId;

    @ApiModelProperty(value = "目标用户ID")
    private Long targetUserId;

    @ApiModelProperty(value = "目标实体类型")
    private String targetEntity;

    @ApiModelProperty(value = "操作类型 1收藏2点赞3关注")
    private Integer optType;

    @ApiModelProperty(value = "收藏用户ID")
    private Long userId;

    @ApiModelProperty(value = "分组ID")
    private Long groupId;

    @ApiModelProperty(value = "是否是取消操作")
    @TableField(exist = false)
    private Boolean isCancel;

}
