package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 企业表
 *
 * @author flyme
 * @date 2019-06-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_company")
@NoArgsConstructor
@TableAlias("company")
public class SysCompany extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "companyId", type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long companyId;

    @ApiModelProperty(value = "企业名称")
    private String companyName;

    @ApiModelProperty(value = "企业Logo")
    private String companyLogo;

    @ApiModelProperty(value = "企业编码")
    private String companyNo;

    @ApiModelProperty(value = "联系人")
    private String linkMan;

    @ApiModelProperty(value = "机构ID")
    private Long organizationId;

    @ApiModelProperty(value = "租户ID")
    private Long tenantId;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "联系电话")
    private String linkTel;

    @ApiModelProperty(value = "地址")
    private String companyAddress;

    @ApiModelProperty(value = "企业类型")
    private String companyType;
    @ApiModelProperty(value = "营业执照号")
    private String businessLicNo;

    @ApiModelProperty(value = "排序值")
    private Integer orderNo;

    @ApiModelProperty(value = "企业简介")
    private String companyDesc;

    @ApiModelProperty(hidden = true, value = "企业状态")
    private Integer companyState;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty(value = "档案数量")
    private int arcInfoCount;

}
