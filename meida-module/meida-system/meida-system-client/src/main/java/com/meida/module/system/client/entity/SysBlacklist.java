package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 黑名单
 *
 * @author flyme
 * @date 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_blacklist")
@TableAlias("blacklist")
@ApiModel(value = "SysBlacklist对象", description = "黑名单")
public class SysBlacklist extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "blacklistId", type = IdType.ASSIGN_ID)
    private Long blacklistId;

    @ApiModelProperty(value = "对象名称")
    private String targetEntity;

    @ApiModelProperty(value = "自定义名称")
    private String targetName;

    @ApiModelProperty(value = "黑名单对象ID")
    private Long targetId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "创建者ID")
    private Long ownerId;

}
