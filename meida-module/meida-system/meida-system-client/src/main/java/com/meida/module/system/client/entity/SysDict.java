package com.meida.module.system.client.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.NotDelete;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author: flyme
 * @date: 2018/3/7 15:26
 * @desc: 字典表
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@TableName("sys_dict")
@TableAlias("dict")
@ApiModel(value = "字典")
public class SysDict extends AbstractEntity {


    @TableId(value = "dictId", type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long dictId;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "字典名称")
    private String dicTitle;

    @ApiModelProperty(value = "字典类型")
    private String dicType;

    /**
     * 排序值
     */
    private BigDecimal sortOrder;

    /**
     * 是否允许删除
     */
    @NotDelete
    private Integer allowDel;

    /**
     * 备注
     */
    private String remark;
}