package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统部门
 *
 * @author flyme
 * @date 2019-07-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_dept")
@TableAlias("dept")
@ApiModel(value = "系统部门", description = "部门")
public class SysDept extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "deptId", type = IdType.ASSIGN_ID)
    private Long deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "部门代码")
    private String deptCode;

    @ApiModelProperty(value = "部门编码")
    private String deptNo;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "排序")
    private Integer sortOrder;

    @ApiModelProperty(value = "部分负责人")
    private String headerId;

    @ApiModelProperty(value = "是否是父节点")
    private Integer isParent;

    @ApiModelProperty(value = "是否是子节点")
    private Integer isLeaf;

    @ApiModelProperty(value = "部门状态")
    private Integer deptState;

    @ApiModelProperty(value = "是否禁用")
    private Integer disabled;

    @ApiModelProperty(value = "排序值")
    private Integer orderNo;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "档案数量")
    private int arcInfoCount;

}
