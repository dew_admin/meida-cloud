package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 区域表
 *
 * @author flyme
 * @date 2019-06-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_area")
@ApiModel(value = "SysArea对象", description = "区域表")
@TableAlias("area")
public class SysArea extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "areaId", type = IdType.ASSIGN_ID)
    private Long areaId;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "区域编码")
    private String areaCode;

    @ApiModelProperty(value = "编码")
    private String adCode;

    @ApiModelProperty(value = "坐标")
    private String location;

    @ApiModelProperty(value = "城市编码")

    private String cityCode;

    @ApiModelProperty(value = "上级区域#FK")

    private Long parentId;

    @ApiModelProperty(value = "级别")

    private Integer areaLevel;

    @ApiModelProperty(value = "是否热门")
    private Integer hot;

    @ApiModelProperty(value = "全拼")
    private String fullLetter;

    @ApiModelProperty(value = "是否启用  1启用")
    private Integer enabled;

    @ApiModelProperty(value = "是否默认  0非默认,1默认")
    private Integer defaultArea;

    @ApiModelProperty(value = "首字母")
    private String firstLetter;


}
