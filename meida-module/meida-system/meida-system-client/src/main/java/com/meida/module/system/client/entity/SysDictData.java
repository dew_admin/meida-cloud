package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author flyme
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@TableName("sys_dictdata")
@TableAlias("dictdata")
@ApiModel(value = "字典数据")
public class SysDictData extends AbstractEntity {

    @TableId(value = "dictDataId", type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long dictDataId;

    @ApiModelProperty(value = "所属字典")
    private String dictId;

    @ApiModelProperty(value = "数据名称")
    private String dicDataTitle;

    @ApiModelProperty(value = "数据值")
    private String dicDataValue;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

    @ApiModelProperty(value = "是否启用 1启用 0禁用")
    private Integer state;


    @ApiModelProperty(value = "是否启用 1文本 2数组")
    private Integer dictDataType;

    @ApiModelProperty(value = "是否允许删除")
    private Integer allowDel;

    @ApiModelProperty(value = "备注")
    private String remark;


}
