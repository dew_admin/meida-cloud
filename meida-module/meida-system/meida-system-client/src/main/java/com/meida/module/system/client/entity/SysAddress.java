package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 收货地址
 *
 * @author flyme
 * @date 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_address")
@TableAlias("address")
@ApiModel(value = "收货地址对象", description = "收货地址")
public class SysAddress extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "addressId", type = IdType.ASSIGN_ID)
    private Long addressId;

    @ApiModelProperty(value = "用户Id")
    private Long userId;

    @ApiModelProperty(value = "收货人")
    private String userName;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "联系方式")
    private String telephone;

    @ApiModelProperty(value = "地址所在地区")
    private String areaName;

    @ApiModelProperty(value = "省")
    private Long proId;

    @ApiModelProperty(value = "市")
    private Long cityId;

    @ApiModelProperty(value = "区")
    private Long areaId;

    @ApiModelProperty(value = "详细地址")
    private String addressInfo;

    @ApiModelProperty(value = "默认地址")
    private Integer defaultAddress;

    @ApiModelProperty(value = "删除标记")
    private Integer deleted;

    @ApiModelProperty(value = "备注")
    private String remark;

}
