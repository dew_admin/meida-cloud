package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.enums.YesNoEnum;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 企业详细信息
 *
 * @author flyme
 * @date 2019-06-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_company_info")
@NoArgsConstructor
@TableAlias("ci")
public class SysCompanyInfo extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "companyInfoId", type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long companyInfoId;

    @ApiModelProperty(value = "管理员账户")
    private Long managerId;

    @ApiModelProperty(value = "主营范围")
    private String businessScope;


    @ApiModelProperty(value = "地址")
    private String companyAddress;

    @ApiModelProperty(value = "营业执照")
    private String businessLic;

    @ApiModelProperty(value = "企业法人")
    private String legalPerson;

    @ApiModelProperty(value = "法人身份证正面")
    private String frontIdCard;
    @ApiModelProperty(value = "法人身份证反面")
    private String backIdCard;

    @ApiModelProperty(value = "营业执照号")
    private String businessLicNo;

    @ApiModelProperty("区")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long areaId;

    @ApiModelProperty("市")
    private Long cityId;

    @ApiModelProperty("省")
    private Long proId;

    @ApiModelProperty("省市区名称")
    private String areaName;

    @ApiModelProperty(value = "审核意见")
    private String auditContent;

    @ApiModelProperty("推荐状态")
    private YesNoEnum recommend;

    @ApiModelProperty(hidden = true, value = "推荐人")
    private Long referrerUserId;

    @ApiModelProperty("备注")
    private String remark;


}
