package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统配置表
 *
 * @author flyme
 * @date 2019-06-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_config2")
@ApiModel(value = "SysConfig对象", description = "系统配置表")
public class SysConfig extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "configId", type = IdType.ASSIGN_ID)
    private Long configId;

    @ApiModelProperty(value = "键")

    private String configKey;

    @ApiModelProperty(value = "值")

    private String configVal;

    @ApiModelProperty(value = "配置类型")

    private String configType;

    @ApiModelProperty(value = "说明")
    private String remark;


}
