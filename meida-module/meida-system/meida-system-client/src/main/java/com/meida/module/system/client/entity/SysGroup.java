package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 分组
 *
 * @author flyme
 * @date 2019-06-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_group1")
@TableAlias("group1")
@ApiModel(value = "SysGroup对象", description = "分组")
public class SysGroup extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分组ID")
    @TableId(value = "groupId", type = IdType.ASSIGN_ID)
    private Long groupId;

    @ApiModelProperty(value = "分组名称")
    private String groupName;

    @ApiModelProperty(value = "实体类型")
    private String targetEntity;

    @ApiModelProperty(value = "分组编码")
    private String groupCode;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

}
