package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 机构(集团)
 *
 * @author flyme
 * @date 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_organization")
@TableAlias("organization")
@ApiModel(value="SysOrganization对象", description="机构(集团)")
public class SysOrganization extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "organizationId", type = IdType.ASSIGN_ID)
    private Long organizationId;

    @ApiModelProperty(value = "机构名称")
    private String organizationName;

    @ApiModelProperty(value = "机构代码")
    private String organizationCode;

    @ApiModelProperty(value = "公司电话")
    private String organTel;

    @ApiModelProperty(value = "负责人")
    private String proMan;

    @ApiModelProperty(value = "联系人")
    private String linkMan;


    @ApiModelProperty(value = "地址")
    private String address;

}
