package com.meida.module.system.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 
 *
 * @author flyme
 * @date 2021-11-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_expire_strategy")
@TableAlias("expireStrategy")
@ApiModel(value="SysExpireStrategy对象", description="")
public class SysExpireStrategy extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = " expireStrategyId", type = IdType.ASSIGN_ID)
    private Long  expireStrategyId;

    @ApiModelProperty(value = "过期键")
    private String redisKey;

    @ApiModelProperty(value = "目标Id")
    private Long targetId;

    @ApiModelProperty(value = "目标类型")
    private String targetType;

    @ApiModelProperty(value = "过期时间")
    private Date expireTime;

}
