package com.meida.module.push.provider.util;

import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.devices.Device;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zyf
 */
@Slf4j
public class IosPushUtils {
    private static PushNotificationManager pushManager;

    static {
        String certificatePath = "C:\\ios.p12";
        //导出证书时设置的密码
        String msgCertificatePassword = "123";
        try {
            pushManager.initializeConnection(new AppleNotificationServerBasicImpl(certificatePath, msgCertificatePassword, false));
        } catch (CommunicationException e) {
            e.printStackTrace();
        } catch (KeystoreException e) {
            e.printStackTrace();
        }
    }

    public static void pushMsg(String deviceToken) throws Exception {

        // 图标小红圈的数值
        int badge = 13;
        // 铃音
        String sound = "default";
        String message = "志辉大傻子5";

        List<String> tokens = new ArrayList<>();
        tokens.add(deviceToken);


        boolean sendCount = true;

        PushNotificationPayload payload = new PushNotificationPayload();
        // 消息内容
        payload.addAlert(message);
        payload.addBadge(badge);
        payload.addCustomDictionary("uid", "11777548498555535383");
        //payload.addCustomDictionary("type", 12);
        //payload.addCustomDictionary("title", "haahi");
        // 铃音
        //payload.addSound("default.caf");

        // true：表示的是产品测试推送服务 false：表示的是产品发布推送服务
        List<PushedNotification> notifications = new ArrayList<>();
        // 开始推送消息
        if (sendCount) {
            Device device = new BasicDevice();
            device.setToken(deviceToken);
            PushedNotification notification = pushManager.sendNotification(device, payload, false);
            notifications.add(notification);
        } else {
            List<Device> devices = new ArrayList<>();
            for (String token : tokens) {
                devices.add(new BasicDevice(token));
            }
            notifications = pushManager.sendNotifications(payload, devices);
        }

        List<PushedNotification> failedNotification = PushedNotification.findFailedNotifications(notifications);
        List<PushedNotification> successfulNotification = PushedNotification.findSuccessfulNotifications(notifications);
        int failed = failedNotification.size();
        int successful = successfulNotification.size();
        log.info("zsl==========成功数：" + successful);
        log.info("zsl==========失败数：" + failed);
        pushManager.stopConnection();
        log.info("zsl==========消息推送完毕");
    }


    public static void main(String[] args) throws Exception {
        //手机设备token号
        String deviceToken = "6ad35e29732ba2cf7acc832b4b3a58b8f31e0b0a4f3a2d5deba92e592d1a4f8c";
        pushMsg(deviceToken);

    }
}