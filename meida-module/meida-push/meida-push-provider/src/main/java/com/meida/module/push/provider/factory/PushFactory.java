package com.meida.module.push.provider.factory;

import com.meida.common.base.client.BasePushClient;
import com.meida.common.base.service.BasePushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @Author:yexg
 * @Date:2019-12-13 16:34
 * @Description: 推送工厂类
 **/
@Component
public class PushFactory<K> {

    @Autowired(required = false)
    private BasePushClient<K> basePushClient;

    /******获取推送实例***/
    public BasePushService createBasePush(String key) {
        return basePushClient.getMap().get(key);
    }
}
