package com.meida.module.push.provider.configuration;

import cn.jpush.api.JPushClient;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.common.base.client.BasePushClient;
import com.meida.common.base.service.BasePushService;
import com.meida.module.push.provider.service.JPushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * 默认配置类
 *
 * @author zyf
 */
@Slf4j
@Configuration
public class PushConfiguration {

    @Autowired(required = false)
    private Map<String, BasePushService> basePushServiceMap;

    @Bean
    @ConditionalOnMissingBean(BasePushClient.class)
    public BasePushClient jPushClient(RedisUtils redisUtils) {
        BasePushClient basePushClient = new BasePushClient();
        List<EntityMap> maps = redisUtils.getConfigListMap("PUSH_CONFIG");
        if (FlymeUtils.isEmpty(maps)) {
            return basePushClient;
        }
        EntityMap map = maps.get(0);
        String serviceName = map.get("serviceName");
        if (FlymeUtils.isEmpty(serviceName)) {
            return basePushClient;
        }
        if (FlymeUtils.isNotEmpty(basePushServiceMap)) {
            BasePushService basePushService = basePushServiceMap.get(serviceName);
            if (basePushService == null) {
                return basePushClient;
            }
            if (basePushService instanceof JPushService) {
                return initJpush(maps, basePushService);
            }
        }
        return basePushClient;
    }

    /****初始化极光推送**/
    private BasePushClient initJpush(List<EntityMap> maps, BasePushService<JPushClient> basePushService) {
        BasePushClient<JPushClient> basePushClient = new BasePushClient<>();
        for (EntityMap s : maps) {
            //不同客户端标识
            String keyName = s.get("keyName");
            String appKey = s.get("appKey");
            String secretKey = s.get("secretKey");
            boolean ambient = s.get("ambient", false);
            if (FlymeUtils.anyOneIsNull(appKey, secretKey, keyName)) {
                continue;
            }
            /*** 初始化默认推送***/
            if (basePushClient.getMap().isEmpty()) {
                JPushClient jPush = new JPushClient(secretKey, appKey);
                basePushService.init(jPush, ambient);
                basePushClient.getMap().put(keyName, basePushService);
                continue;
            }
            /***初始化其他推送**/
            try {
                JPushClient jPush = new JPushClient(secretKey, appKey);
                BasePushService<JPushClient> jPushService = new JPushService<>();
                jPushService.init(jPush, ambient);
                basePushClient.getMap().put(keyName, jPushService);
                log.info("JPush初始化成功###############################");
            } catch (Exception e) {
                log.error("JPush初始化失败!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }
        return basePushClient;
    }
}
