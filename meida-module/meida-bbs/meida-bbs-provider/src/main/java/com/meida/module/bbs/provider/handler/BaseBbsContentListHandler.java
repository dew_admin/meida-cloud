package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.CommonFileService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.constants.FileTypeConstant;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.DateUtils;
import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.module.bbs.provider.service.BbsTypeService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 帖子列表
 *
 * @author zyf
 */
@Component("baseBbsContentListHandler")
public class BaseBbsContentListHandler implements PageInterceptor<BbsContent> {

    @Resource
    private BbsTypeService typeService;

    @Resource
    private CommonFileService commonFileService;

    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery<BbsContent> cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        String keyword = cq.getParams("keyword");
        //收藏标记
        cq.addSelect("collecon_tag(1,'BbsContent',contentId," + userId + ") scTag");
        //点赞标记
        cq.addSelect("collecon_tag(2,'BbsContent',contentId," + userId + ") dzTag");
        //关注标记
        cq.addSelect("collecon_tag(3,'AppUser',content.userId," + userId + ") gzTag");
        //点赞数
        cq.addSelect("collecon_count(2,'BbsContent',contentId) dzNum");
        //收藏数
        cq.addSelect("collecon_count(1,'BbsContent',contentId) scNum");
        cq.eq(BbsContent.class, "contentState", CommonConstants.ENABLED);
        if (FlymeUtils.isNotEmpty(keyword)) {
            cq.and(e -> e.like("content.title", keyword).or().like("content.tags", keyword).or().like("content.content",keyword));
        }
    }

    @Override
    public void complete(CriteriaQuery<BbsContent> cq, List<EntityMap> result, EntityMap extra) {
        for (EntityMap entityMap : result) {
            initResult(entityMap);
        }
    }

    /**
     * 扩展返回对象
     *
     * @param entityMap
     */
    public void initResult(EntityMap entityMap) {
        //标签ID(可多个)
        String typeId = entityMap.get("typeId");
        Long contentId = entityMap.getLong("contentId");
        Date createTime = entityMap.get("createTime");
        //格式化显示日期
        String showTime = DateUtils.formatFromTodayCn(createTime);
        entityMap.put("showTime", showTime);
        List<EntityMap> typeNames = new ArrayList<>();
        if (FlymeUtils.isNotEmpty(typeId)) {
            //查询标签
            typeNames = typeService.listTypeTypeId(typeId);
        }
        entityMap.put("typeNames", typeNames);
        //查询图片
        List<EntityMap> images = commonFileService.selectFileListByFileGroup(contentId, BbsContent.class.getSimpleName(), FileTypeConstant.FILETYPE_IMAGE);
        entityMap.put("images", images);
    }
}
