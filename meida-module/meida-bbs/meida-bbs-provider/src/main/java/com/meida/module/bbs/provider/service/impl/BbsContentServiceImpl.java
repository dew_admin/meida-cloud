package com.meida.module.bbs.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ConvertUtils;
import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.module.bbs.provider.mapper.BbsContentMapper;
import com.meida.module.bbs.provider.service.BbsContentService;
import com.meida.module.bbs.provider.service.BbsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 帖子接口实现类
 *
 * @author flyme
 * @date 2019-12-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BbsContentServiceImpl extends BaseServiceImpl<BbsContentMapper, BbsContent> implements BbsContentService {

    @Autowired
    private BbsTypeService typeService;

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<BbsContent> cq, BbsContent bbsContent, EntityMap requestMap) {
        String typeId = cq.getParams("typeId");
        cq.select(BbsContent.class, "contentId", "contentType", "typeId", "title", "coverImage", "content", "address", "topOrder", "commentNum", "tags", "contentGroup", "moduleId", "videoUrl", "createTime");
        cq.likeByField(BbsContent.class, "title");
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        //内容类型ID
        cq.eq(BbsContent.class, "contentType");
        if (FlymeUtils.isNotEmpty(typeId)) {
            String[] typeIds = typeId.split(",");
            if (typeIds.length > 1) {
                cq.regexp("typeId", typeId);
            } else {
                cq.regexp("typeId", typeId);
            }
        }
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("userId");
        cq.orderByDesc("content.recommend");
        cq.orderByAsc("content.topOrder");
        cq.orderByDesc("content.createTime");
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforeGet(CriteriaQuery<BbsContent> cq, BbsContent content, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(BbsContent.class, "*");
        cq.addSelect("user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_tag(1,'BbsContent',contentId," + userId + ") scTag");
        cq.addSelect("collecon_tag(2,'BbsContent',contentId," + userId + ") dzTag");
        cq.addSelect("collecon_tag(3,'AppUser',user.userId," + userId + ") gzTag");
        cq.addSelect("collecon_count(2,'BbsContent',contentId) dzNum");
        cq.addSelect("collecon_count(1,'BbsContent',contentId) scNum");
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("userId");
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, BbsContent content, EntityMap extra) {
        Integer contentState = content.getContentState();
        if (FlymeUtils.isEmpty(contentState)) {
            content.setContentState(CommonConstants.ENABLED);
        }
        String typeId = content.getTypeId();
        if (FlymeUtils.isNotEmpty(typeId)) {
            List<String> typeNames = typeService.selectTagNames(typeId);
            if (FlymeUtils.isNotEmpty(typeNames)) {
                content.setTags(ConvertUtils.listToString(typeNames));
            }
        }
        content.setCommentNum(0);
        content.setRecommend(CommonConstants.DISABLED);
        return ResultBody.msg("发布成功");
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<BbsContent> cu, BbsContent bbsContent, EntityMap extra) {
        String typeId = bbsContent.getTypeId();
        if (FlymeUtils.isNotEmpty(typeId)) {
            List<String> typeNames = typeService.selectTagNames(typeId);
            if (FlymeUtils.isNotEmpty(typeNames)) {
                bbsContent.setTags(ConvertUtils.listToString(typeNames));
            }
        }
        return super.beforeEdit(cu, bbsContent, extra);
    }

    @Override
    public Boolean updateCommentNum(Long contentId, Integer num) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.setSql(true, "commentNum=commentNum+" + num);
        cu.ge("commentNum", 0);
        cu.eq(true, "contentId", contentId);
        return update(cu);
    }
}
