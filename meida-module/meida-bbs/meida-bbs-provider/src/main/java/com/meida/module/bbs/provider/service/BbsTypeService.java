package com.meida.module.bbs.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.bbs.client.entity.BbsType;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 帖子分类  接口
 *
 * @author flyme
 * @date 2019-12-06
 */
public interface BbsTypeService extends IBaseService<BbsType> {
    /**
     * 根据分组类型查询
     * @param groupId
     * @return
     */
    List<EntityMap> listByGroupId(Long groupId);

    /**
     * 根据分类ID查询(typeId可为多个逗号隔开)
     * @param typeId
     * @return
     */
    List<EntityMap> listTypeTypeId(String typeId);

    /**
     * 查询分类名称
     * @param typeIds
     * @return
     */
    List<String> selectTagNames(String typeIds);
}
