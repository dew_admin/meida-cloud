package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.bbs.client.entity.BbsContent;
import org.springframework.stereotype.Component;

/**
 * 用户草稿帖子列表
 *
 * @author zyf
 */
@Component("userBbsContentListHandler")
public class UserBbsContentListHandler extends BaseBbsContentListHandler {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        String keyword = cq.getParams("keyword");
        Integer contentState = cq.getInt("contentState", -1);
        //收藏标记
        cq.addSelect("collecon_tag(1,'BbsContent',contentId," + userId + ") scTag");
        //点赞标记
        cq.addSelect("collecon_tag(2,'BbsContent',contentId," + userId + ") dzTag");
        //关注标记
        cq.addSelect("collecon_tag(3,'AppUser',content.userId," + userId + ") gzTag");
        //点赞数
        cq.addSelect("collecon_count(2,'BbsContent',contentId) dzNum");
        //收藏数
        cq.addSelect("collecon_count(1,'BbsContent',contentId) scNum");
        cq.eq(BbsContent.class, "userId", userId);
        cq.eq(BbsContent.class, "contentState", contentState);
        cq.like(BbsContent.class, "title", keyword);
    }
}
