package com.meida.module.bbs.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.bbs.client.entity.BbsComment;

import java.util.List;
import java.util.Map;

/**
 * 帖子回复 接口
 *
 * @author flyme
 * @date 2019-12-06
 */
public interface BbsCommentService extends IBaseService<BbsComment> {
    /**
     * 查询评价(微信形式,显示部分层级)
     *
     * @param contentId
     * @param showType
     * @param size
     * @return
     */
    List<EntityMap> listByContentIdAndSize(Long contentId, Integer showType,  Integer size);

    /**
     * 查询子评论
     *
     * @param parentComments
     * @param showType
     * @return
     */
    List<EntityMap> getChildComment(List<EntityMap> parentComments,Integer showType);

    /**
     * 统计回复数量
     * @param contentId
     * @return
     */
    Long countByContentId(Long contentId);

    /**
     * 用户评论跟用户回复的分页数据
     * @return
     */
    ResultBody getReplyPageList(Map params);

    /**
     * 更新点赞数量
     * @param commentId
     * @param num
     * @return
     */
    Boolean updateDzNum(Long commentId, Integer num);

}
