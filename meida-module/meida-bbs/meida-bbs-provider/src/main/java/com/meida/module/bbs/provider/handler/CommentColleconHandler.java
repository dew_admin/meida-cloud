package com.meida.module.bbs.provider.handler;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.bbs.client.entity.BbsComment;
import com.meida.module.bbs.provider.service.BbsCommentService;
import com.meida.module.system.client.entity.SysCollecon;
import com.meida.starter.rabbitmq.event.EventObj;
import com.meida.starter.rabbitmq.event.MeidaBusEventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 更新点赞数量
 *
 * @author zyf
 */
@Component
public class CommentColleconHandler implements MeidaBusEventHandler<SysCollecon> {

    @Autowired
    private BbsCommentService commentService;

    @Override
    public boolean support(EventObj<SysCollecon> eventObj) {
        SysCollecon collecon = eventObj.getEntity();
        if (FlymeUtils.isNotEmpty(collecon)) {
            String targetEntity = collecon.getTargetEntity();
            if (targetEntity.equals(BbsComment.class.getSimpleName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onMessage(EventObj<SysCollecon> eventObj) {
        SysCollecon collecon = eventObj.getEntity();
        if (FlymeUtils.isNotEmpty(collecon)) {
            Long targetId = collecon.getTargetId();
            Boolean isCancel = collecon.getIsCancel();
            Integer num = isCancel ? -1 : 1;
            commentService.updateDzNum(targetId, num);
        }
    }
}
