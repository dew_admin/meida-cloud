package com.meida.module.bbs.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.bbs.client.entity.BbsComment;
import com.meida.module.bbs.provider.service.BbsCommentService;


/**
 * 帖子回复控制器
 *
 * @author flyme
 * @date 2019-12-06
 */
@RestController
@RequestMapping("/bbs/comment/")
@Api(tags = "论坛模块-帖子回复")
public class BbsCommentController extends BaseController<BbsCommentService, BbsComment> {

    @ApiOperation(value = "帖子回复-分页列表", notes = "帖子回复分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "帖子回复-评论列表", notes = "帖子回复分页列表")
    @GetMapping(value = "getReplyPageList")
    public ResultBody getReplyPageList(@RequestParam(required = false) Map params) {
        return bizService.getReplyPageList(params);
    }

    @ApiOperation(value = "帖子回复-列表", notes = "帖子回复列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "帖子回复-添加", notes = "添加帖子回复")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }


    @ApiOperation(value = "帖子回复-删除", notes = "删除帖子回复")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "帖子回复-更新状态值", notes = "帖子回复更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
