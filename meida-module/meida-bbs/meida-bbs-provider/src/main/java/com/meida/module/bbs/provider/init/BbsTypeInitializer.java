package com.meida.module.bbs.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.bbs.client.entity.BbsType;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class BbsTypeInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return BbsType.class;
    }
}
