package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.bbs.client.entity.BbsType;
import org.springframework.stereotype.Component;

/**
 * 帖子类型/话题列表
 *
 * @author zyf
 */
@Component
public class BbsTypeListHandler implements PageInterceptor<BbsType> {

    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.clear();
        cq.select(BbsType.class, "typeId", "typeName", "sortOrder", "sortOrder", "groupId", "typeState", "retention", "updateTime", "createTime");
        cq.like(BbsType.class, "typeName");
        cq.eq(BbsType.class, "groupId");
        cq.eq(BbsType.class, "typeState");
        cq.eq(BbsType.class, "retention");
    }

}
