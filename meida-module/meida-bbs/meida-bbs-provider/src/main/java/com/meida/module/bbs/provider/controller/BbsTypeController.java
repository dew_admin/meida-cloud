package com.meida.module.bbs.provider.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.bbs.client.entity.BbsType;
import com.meida.module.bbs.provider.service.BbsTypeService;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 帖子分类 控制器
 *
 * @author flyme
 * @date 2019-12-06
 */
@RestController
@RequestMapping("/bbs/type/")
@Api(tags = "论坛模块-帖子分类")
@ApiIgnore
public class BbsTypeController extends BaseController<BbsTypeService, BbsType> {

    @ApiOperation(value = "帖子分类 -分页列表", notes = "帖子分类 分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "帖子分类 -列表", notes = "帖子分类 列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "帖子分类列表", notes = "根据groupId查询")
    @GetMapping(value = "listByGroupId")
    public ResultBody listByGroupId(Long groupId) {
        List list = bizService.listByGroupId(groupId);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "帖子分类 -添加", notes = "添加帖子分类 ")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "帖子分类 -更新", notes = "更新帖子分类 ")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "帖子分类 -删除", notes = "删除帖子分类 ")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "帖子分类-详情", notes = "帖子分类详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "帖子分类 -更新状态值", notes = "帖子分类 更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "typeState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "typeState") Integer state) {
        return bizService.setState(map, "typeState", state);
    }
}
