package com.meida.module.bbs.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.module.bbs.client.entity.BbsType;
import com.meida.module.bbs.provider.mapper.BbsTypeMapper;
import com.meida.module.bbs.provider.service.BbsTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 帖子分类 接口实现类
 *
 * @author flyme
 * @date 2019-12-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BbsTypeServiceImpl extends BaseServiceImpl<BbsTypeMapper, BbsType> implements BbsTypeService {

    @Override
    public ResultBody baseAdd(CriteriaSave cs, BbsType obj) {
        obj.setGroupId(CommonConstants.INT_1);
        return super.baseAdd(cs, obj);
    }

    @Override
    public List<EntityMap> listByGroupId(Long groupId) {
        CriteriaQuery<BbsType> cq = new CriteriaQuery(BbsType.class);
        cq.eq("groupId", groupId);
        cq.eq("typeState", CommonConstants.ENABLED);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> listTypeTypeId(String typeId) {
        CriteriaQuery<BbsType> cq = new CriteriaQuery(BbsType.class);
        ApiAssert.isNotEmpty("typeId不能为空", typeId);
        cq.select(BbsType.class, "typeId", "typeName");
        String[] typeIds = typeId.split(",");
        cq.lambda().eq(BbsType::getTypeState, CommonConstants.ENABLED).orderByAsc(BbsType::getSortOrder);
        if (typeIds.length > 1) {
            cq.regexp("typeId", typeId);
        } else {
            cq.eq("typeId", typeId);
        }
        return selectEntityMap(cq);
    }
    @Override
    public List<String> selectTagNames(String typeIds) {
        List<String> tagNames = new ArrayList<>();
        if (FlymeUtils.isNotEmpty(typeIds)) {
            CriteriaQuery qw = new CriteriaQuery(BbsType.class);
            qw.select("typeName");
            qw.in(true, "typeId", typeIds.split(","));
            tagNames = listObjs(qw,e -> e.toString());
        }
        return tagNames;
    }

}
