package com.meida.module.bbs.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.bbs.client.entity.BbsContent;

/**
 * 帖子 接口
 *
 * @author flyme
 * @date 2019-12-06
 */
public interface BbsContentService extends IBaseService<BbsContent> {
    /**
     * 更新评论数量
     *
     * @param contentId
     * @param num
     * @return
     */
    Boolean updateCommentNum(Long contentId, Integer num);


}
