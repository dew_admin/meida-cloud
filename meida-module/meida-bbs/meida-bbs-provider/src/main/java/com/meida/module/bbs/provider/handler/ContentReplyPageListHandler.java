package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.bbs.client.entity.BbsComment;
import com.meida.module.bbs.provider.service.BbsCommentService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 帖子评论列表
 *
 * @author zyf
 */
@Component
public class ContentReplyPageListHandler implements PageInterceptor<BbsComment> {

    @Resource
    private BbsCommentService commentService;

    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        List<String> coloum=new ArrayList<>();
        coloum.add("comment.dzNum");
        coloum.add("comment.createTime");
        cq.orderByDesc(true,coloum);
    }

    @Override
    public void complete(CriteriaQuery<BbsComment> cq, List<EntityMap> result, EntityMap extra) {
        //默认层级模式(1:微信模式,2:微博模式)
        Integer showType = cq.getInt("showType", 2);
        if (showType.equals(CommonConstants.INT_2)) {
            //查询
            commentService.getChildComment(result, showType);
        }
    }
}
