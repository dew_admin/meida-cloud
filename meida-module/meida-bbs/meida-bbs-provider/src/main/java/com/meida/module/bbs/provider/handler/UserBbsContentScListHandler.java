package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.enums.ColleconEnum;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.module.system.client.entity.SysCollecon;
import org.springframework.stereotype.Component;

/**
 * 用户收藏帖子列表
 *
 * @author zyf
 */
@Component("userBbsContentScListHandler")
public class UserBbsContentScListHandler implements PageInterceptor<BbsContent> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long otherUserId = params.getLong("userId");
        Long userId = OpenHelper.getUserId();
        cq.addSelect("collecon_tag(2,'BbsContent',contentId," + userId + ") dzTag");
        //点赞数
        cq.addSelect("collecon_count(2,'BbsContent',contentId) dzNum");
        //收藏数
        cq.addSelect("collecon_count(1,'BbsContent',contentId) scNum");
        if (FlymeUtils.isNotEmpty(otherUserId)) {
            cq.eq(SysCollecon.class, "userId", otherUserId);
        } else {
            cq.eq(SysCollecon.class, "userId", userId);
        }
        cq.eq(SysCollecon.class, "optType", ColleconEnum.SC.getCode());
        cq.eq(SysCollecon.class, "targetEntity", "BbsContent");
        cq.createJoin(SysCollecon.class).setJoinField("targetId").setMainField("contentId");
    }
}
