package com.meida.module.bbs.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.bbs.client.entity.BbsComment;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class BbsCommentInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return BbsComment.class;
    }
}
