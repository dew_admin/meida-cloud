package com.meida.module.bbs.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.CommonFileService;
import com.meida.common.constants.FileTypeConstant;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.DateUtils;
import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.module.bbs.provider.service.BbsCommentService;
import com.meida.module.bbs.provider.service.BbsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 帖子详情扩展
 *
 * @author zyf
 */
@Component("bbsContentGetHandler")
public class BbsContentGetHandler implements GetInterceptor {

    @Autowired
    private BbsCommentService commentService;
    @Resource
    private BbsTypeService typeService;


    @Autowired
    private CommonFileService commonFileService;

    @Override
    public void prepare(CriteriaQuery cq, EntityMap params) {

    }

    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long contentId = map.getLong("contentId");
        String typeId = map.get("typeId");
        Date createTime = map.get("createTime");
        //默认层级模式
        Integer showType = cq.getInt("showType", 2);
        List<EntityMap> commentList = commentService.listByContentIdAndSize(contentId, showType, 10);
        Long replyCount = commentService.countByContentId(contentId);
        map.put("replyCount", replyCount);
        map.put("commentList", commentList);
        //查询图片
        List<EntityMap> images = commonFileService.selectFileListByFileGroup(contentId, BbsContent.class.getSimpleName(), FileTypeConstant.FILETYPE_IMAGE);
        map.put("images", images);

        //查询标签
        List<EntityMap> typeNames = typeService.listTypeTypeId(typeId);
        map.put("typeNames", typeNames);
        //格式化显示日期
        String showTime = DateUtils.formatFromTodayCn(createTime);
        map.put("showTime", showTime);
    }
}
