package com.meida.module.bbs.provider.mapper;

import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 帖子 Mapper 接口
 * @author flyme
 * @date 2019-12-06
 */
public interface BbsContentMapper extends SuperMapper<BbsContent> {

}
