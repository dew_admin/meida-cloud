package com.meida.module.bbs.provider.handler;

import cn.hutool.extra.qrcode.QrCodeUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.bbs.client.entity.BbsContent;
import com.meida.module.system.client.entity.SysCollecon;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户关注的作者帖子列表
 *
 * @author zyf
 */
@Component("userFollowBbsContentListHandler")
public class UserFollowBbsContentListHandler extends BaseBbsContentListHandler {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId=OpenHelper.getUserId();
        cq.eq(true,"collecon.userId",userId);
        //类型为关注
        cq.eq(true,"collecon.optType",3);
        cq.addSelect(BbsContent.class,"content");
        //只查询我关注的作者发布的内容
        cq.createJoin(SysCollecon.class).setMainField("userId").setJoinField("targetId");
        super.prepare(cq, pageParams, params);
    }

    @Override
    public void complete(CriteriaQuery<BbsContent> cq, List<EntityMap> result, EntityMap extra) {
        for (EntityMap entityMap : result) {
            initResult(entityMap);
        }
    }
}
