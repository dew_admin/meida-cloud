/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 14:42:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bbs_content
-- ----------------------------
DROP TABLE IF EXISTS `bbs_content`;
CREATE TABLE `bbs_content`  (
  `contentId` bigint(20) NOT NULL COMMENT '主键',
  `contentTypeId` bigint(20) NULL DEFAULT NULL COMMENT '分类',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `coverImage` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `contentState` int(11) NULL DEFAULT NULL COMMENT '状态',
  `topOrder` int(11) NULL DEFAULT NULL COMMENT '置顶序号',
  `commentNum` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '推荐是否',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '发布人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`contentId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '帖子' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (407, 400, 'bbsContent', '帖子管理', '帖子管理', '/', 'content/bbs/index', 'solution', '_self', 'PageView', 1, 3, 1, 0, 'meida-base-provider', '2019-12-27 14:42:07', '2020-01-04 17:28:31');
INSERT INTO `base_authority` VALUES (407, 'MENU_bbsContent', 407, NULL, NULL, 1, '2019-12-27 14:42:07', '2020-01-04 17:28:31');


SET FOREIGN_KEY_CHECKS = 1;
