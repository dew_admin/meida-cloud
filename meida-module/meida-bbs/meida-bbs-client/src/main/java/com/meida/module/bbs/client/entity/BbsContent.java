package com.meida.module.bbs.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 帖子
 *
 * @author flyme
 * @date 2019-12-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bbs_content")
@TableAlias("content")
@ApiModel(value="BbsContent对象", description="帖子")
public class BbsContent extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "contentId", type = IdType.ASSIGN_ID)
    private Long contentId;

    @ApiModelProperty(value = "内容类型(1,图文,2:视频)")
    private Integer contentType;

    @ApiModelProperty(value = "分类ID")
    private String typeId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "视频地址")
    private String videoUrl;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "发布坐标地址")
    private String address;

    @ApiModelProperty(value = "状态")
    private Integer contentState;

    @ApiModelProperty(value = "置顶序号")
    private Integer topOrder;

    @ApiModelProperty(value = "分组")
    private Integer contentGroup;

    @ApiModelProperty(value = "评论数量")
    private Integer commentNum;

    @ApiModelProperty(value = "推荐是否")
    private Integer recommend;

    @ApiModelProperty(value = "标签")
    private String tags;

    @ApiModelProperty(value = "发布人")
    @TableField(value = "userId",fill = FieldFill.INSERT)
    private Long userId;

    @ApiModelProperty(value = "关联模块ID")
    private Long moduleId;

}
