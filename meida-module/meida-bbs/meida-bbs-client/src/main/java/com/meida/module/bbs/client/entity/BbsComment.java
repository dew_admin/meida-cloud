package com.meida.module.bbs.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 帖子回复
 *
 * @author flyme
 * @date 2019-12-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bbs_comment")
@TableAlias("comment")
@ApiModel(value="BbsComment对象", description="帖子回复")
public class BbsComment extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "commentId", type = IdType.ASSIGN_ID)
    private Long commentId;

    @ApiModelProperty(value = "内容Id")
    private Long contentId;

    @ApiModelProperty(value = "上级评论ID")
    private Long replyCommentId;

    @ApiModelProperty(value = "评论人")
    private Long replyUserId;

    @ApiModelProperty(value = "被评论人")
    private Long replyToUserId;

    @ApiModelProperty(value = "评论图片")
    private String replyImages;

    @ApiModelProperty(value = "评论内容")
    private String replyContent;

    @ApiModelProperty(value = "评论状态")
    private Integer replyState;

    @ApiModelProperty(value = "点赞数量")
    private Integer dzNum;

}
