package com.meida.module.file.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 文件类型
 *
 * @author flyme
 * @date 2021-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_file_type")
@TableAlias("sft")
@ApiModel(value="SysFileType对象", description="文件类型")
public class SysFileType extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "fileTypeId", type = IdType.ASSIGN_ID)
    private Long fileTypeId;

    @ApiModelProperty(value = "类型名称")
    private String typeName;

    @ApiModelProperty(value = "编码类型")
    private String contentType;

    @ApiModelProperty(value = "是否允许转换PDF")
    private Integer allowConvert;

    @ApiModelProperty(value = "是否允许OCR识别")
    private Integer allowOcr;

    @ApiModelProperty(value = "允许上传的文件最大大小")
    private Integer allowMaxSize;

    @ApiModelProperty(value = "允许上传图片的分辨率")
    private String allowFbl;

    @ApiModelProperty(value = "文件图标")
    private String fileIcon;

    @ApiModelProperty(value = "是否允许上传")
    private Integer state;

    @ApiModelProperty(value = "是否使用")
    private Integer useState;

}
