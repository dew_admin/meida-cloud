package com.meida.module.file.client.vo;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传文件对象
 * @author zyf
 */
@Data
public class FileRequest {
    /**
     * 文件对象
     */
    private MultipartFile multipartFile;
    /**
     * 文件ID
     */
    private Long uid;
}
