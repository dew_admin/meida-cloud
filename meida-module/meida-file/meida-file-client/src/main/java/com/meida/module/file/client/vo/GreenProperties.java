package com.meida.module.file.client.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GreenProperties {

    @ApiModelProperty(value = "阿里云accessKeyId")
    private String accessKeyId;

    @ApiModelProperty(value = "阿里云accessKeySecret")
    private String accessKeySecret;

    @ApiModelProperty(value = "用阿里绿网服务的regionId")
    private String regionId;
}
