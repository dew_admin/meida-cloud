package com.meida.module.file.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.meida.module.file.client.vo.SerializableStream;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件表
 *
 * @author flyme
 * @date 2019-06-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_file")
@TableAlias("file")
@ApiModel(value = "SysFile对象", description = "文件表")
public class SysFile extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "fileId", type = IdType.ASSIGN_ID)
    private Long fileId;

    @ApiModelProperty(value = "文件名")

    private String fileName;

    @ApiModelProperty(value = "文件原始名称")

    private String originalName;

    @ApiModelProperty(value = "文件大小")

    private Long fileSize;

    @ApiModelProperty(value = "文件key")

    private String fileKey;

    @ApiModelProperty(value = "文件扩展名")
    private String fileExt;

    @ApiModelProperty(value = "文件分组(1:图片,2:视频,3:文件)")
    private Integer fileGroup;

    @ApiModelProperty(value = "存储方式")
    private Integer storeType;

    @ApiModelProperty(value = "文件类型")

    private String fileType;

    @ApiModelProperty(value = "文件预览地址")
    private String fileUrl;

    @ApiModelProperty(value = "本地存储路径")
    private String localPath;

    @ApiModelProperty(value = "缩略图本地存储路径")
    private String smallPath;

    @ApiModelProperty(value = "缩略图OSS路径")
    private String smallOssPath;


    @ApiModelProperty(value = "云存储路径")

    private String ossPath;

    @ApiModelProperty(value = "业务对象ID")

    private Long busId;

    @ApiModelProperty(value = "业务类型")
    private String busType;

    @ApiModelProperty(value = "创建用户")
    private Long createUser;

    @ApiModelProperty(value = "下载次数")
    private Integer downloadCount;

    private String sha256;

    @TableField(exist = false)
    private Boolean hasSmallOssPath;

    @TableField(exist = false)
    private Integer width;

    @TableField(exist = false)
    private Integer height;

    @TableField(exist = false)
    private Integer heightDpi;
    @TableField(exist = false)
    private Integer widthDpi;
    @TableField(exist = false)
    private SerializableStream serializableStream;
    @TableField(exist = false)
    private String base64Img;




}
