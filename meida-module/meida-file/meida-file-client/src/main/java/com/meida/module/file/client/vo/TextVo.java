package com.meida.module.file.client.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TextVo {

    @ApiModelProperty(value = "业务Id")
    private String dataId;

    @ApiModelProperty(value = "文本内容")
    private String content;
}
