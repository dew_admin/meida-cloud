package com.meida.module.file.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * minio上传日志
 *
 * @author flyme
 * @date 2022-01-23
 */
@Data
@Accessors(chain = true)
@TableName("minio_log")
@TableAlias("ml")
@ApiModel(value = "MinioLog对象", description = "minio记录")
public class MinioLog {
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "文件路径")
    @TableField(value = "key_name")
    private String keyName;

    @ApiModelProperty(value = "日志内容")
    private String value;

    @ApiModelProperty(value = "用户")
    private String userName;

    @ApiModelProperty(value = "状态")
    private Integer status;
}
