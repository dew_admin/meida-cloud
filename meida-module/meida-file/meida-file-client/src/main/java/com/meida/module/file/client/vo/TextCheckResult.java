package com.meida.module.file.client.vo;

import java.util.List;

public class TextCheckResult {

    private String msg;
    private int code;
    private List<Data> data;
    private String requestId;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public List<Data> getData() {
        return data;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public static class Data {

        private String msg;
        private int code;
        private String dataId;
        private List<Results> results;
        private String content;
        private String taskId;

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public void setDataId(String dataId) {
            this.dataId = dataId;
        }

        public String getDataId() {
            return dataId;
        }

        public void setResults(List<Results> results) {
            this.results = results;
        }

        public List<Results> getResults() {
            return results;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getTaskId() {
            return taskId;
        }
    }

    public static class Results {

        private double rate;
        private String suggestion;
        private String label;
        private String scene;

        public void setRate(double rate) {
            this.rate = rate;
        }

        public double getRate() {
            return rate;
        }

        public void setSuggestion(String suggestion) {
            this.suggestion = suggestion;
        }

        public String getSuggestion() {
            return suggestion;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public void setScene(String scene) {
            this.scene = scene;
        }

        public String getScene() {
            return scene;
        }
    }
}
