package com.meida.module.file.client.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 上传文件请求对象
 *
 * @author zyf
 */
@Data
public class UploadRequest implements Serializable {
    /**
     * 桶名称
     */
    private String bucketName;

    /**
     * 总块数
     */
    private int totalChunks;

    /**
     * 当前块数，从1开始
     */
    private int chunkNumber;

    /**
     * 上传id，每个文件唯一
     */
    private String identifier;


}
