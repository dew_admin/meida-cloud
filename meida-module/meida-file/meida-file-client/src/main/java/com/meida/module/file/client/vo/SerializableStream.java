package com.meida.module.file.client.vo;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;

/**
 * 实现InputStream序列化
 *
 * @author zyf
 */
@Slf4j
public class SerializableStream implements Serializable {
    private final static int LENGTH = 1024;
    private transient InputStream inputStream;
    private String fileName;


    public SerializableStream(MultipartFile multipartFile) {
        try {
            this.inputStream = multipartFile.getInputStream();
            //this.fileName = multipartFile.getOriginalFilename();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InputStream getInputStream() {
        return inputStream;
    }

//    public MultipartFile getMultipartFile() {
//        return getMultipartFile(inputStream, fileName);
//    }

//    public MultipartFile getMultipartFile(InputStream inputStream, String fileName) {
//        FileItem fileItem = createFileItem(inputStream, fileName);
//        return new CommonsMultipartFile(fileItem);
//    }

    /**
     * FileItem类对象创建
     *
     * @param inputStream inputStream
     * @param fileName    fileName
     * @return FileItem
     */
    public FileItem createFileItem(InputStream inputStream, String fileName) {
        FileItemFactory factory = new DiskFileItemFactory(16, null);
        String textFieldName = "file";
        FileItem item = factory.createItem(textFieldName, MediaType.MULTIPART_FORM_DATA_VALUE, true, fileName);
        int bytesRead = 0;
        byte[] buffer = new byte[8192];
        OutputStream os = null;
        //使用输出流输出输入流的字节
        try {
            os = item.getOutputStream();
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            inputStream.close();
        } catch (IOException e) {
            log.error("Stream copy exception", e);
            throw new IllegalArgumentException("文件上传失败");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    log.error("Stream close exception", e);

                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("Stream close exception", e);
                }
            }
        }

        return item;
    }


    private void writeObject(ObjectOutputStream oos) throws Exception {
        oos.defaultWriteObject();

        byte[] buff = new byte[LENGTH];
        int tmp;
        while ((tmp = inputStream.read(buff, 0, LENGTH)) != -1) {
            oos.write(buff, 0, tmp);
        }
    }

    private void readObject(ObjectInputStream ois) throws Exception {
        ois.defaultReadObject();

        byte[] buf = new byte[LENGTH];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int tmp;
        while ((tmp = ois.read(buf, 0, LENGTH)) != -1) {
            bos.write(buf, 0, tmp);
        }

        inputStream = new ByteArrayInputStream(bos.toByteArray());
    }
}