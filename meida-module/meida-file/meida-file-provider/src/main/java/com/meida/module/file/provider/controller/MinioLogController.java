package com.meida.module.file.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.file.client.entity.MinioLog;
import com.meida.module.file.provider.service.MinioLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * minioLog文件上传控制器
 *
 * @author flyme
 * @date 2019-06-03
 */
@Slf4j
@Api(tags = "文件模块-文件管理")
@RequestMapping("/file/minio/log/")
@RestController
public class MinioLogController extends BaseController<MinioLogService, MinioLog> {


    @ApiOperation(value = "minio上传日志", notes = "minio上传日志")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "minio上传日志-删除", notes = "删除minio上传日志")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam String userName, @RequestParam Integer status) {
        Boolean tag = bizService.deleteByUserName(userName, status);
        return ResultBody.ok(tag);
    }
}
