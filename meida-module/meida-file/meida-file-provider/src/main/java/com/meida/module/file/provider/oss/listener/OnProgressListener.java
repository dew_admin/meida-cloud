package com.meida.module.file.provider.oss.listener;

/**
 * 监听文件流上传进度
 *
 * @author zyf
 */
public interface OnProgressListener {
    /**
     * 错误回调
     *
     * @param errorMsg
     */
    default void onError(String errorMsg) {

    }

    /**
     * 进度回调
     *
     * @param process
     */
    void onProgress(int process);

    /**
     * 完成回调
     */
    default void onCompleted() {

    }
}
