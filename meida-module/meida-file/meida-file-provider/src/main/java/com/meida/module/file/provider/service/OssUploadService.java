package com.meida.module.file.provider.service;


import com.meida.common.base.entity.EntityMap;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.client.vo.OssSetting;
import com.rabbitmq.client.Channel;

import java.io.InputStream;

/**
 * oss上传接口
 *
 * @author zyf
 */
public interface OssUploadService {

    /**
     * 分隔符
     */
    String SEPARATOR = "/";

    /**
     * 分片文件大小
     */
    long MAX_PART_SIZE = 5 * 1024 * 1024;

    /**
     * 获取上传配置
     *
     * @return
     */
    OssSetting getOssSetting();

    /**
     * 获取OssPath父路径
     *
     * @return
     */
    String getBaseOssPath();

    /**
     * 获取OssPath路径
     *
     * @param fileBasePath
     * @param fileName
     * @return
     */
    String getOssPath(String fileBasePath, String fileName);


    /**
     * 获取硬盘路径
     *
     * @param fileBasePath
     * @param fileName
     * @return
     */
    String getLocalPath(String fileBasePath, String fileName);

    /**
     * 上传文件
     *
     * @param localPath
     * @param fileBasePath
     * @param fileName     真实文件名
     * @param sysFile      文件流
     * @param fKey
     * @param uid          前端文件ID
     * @param userId
     * @return
     */
    String upload(String localPath, String fileName, SysFile sysFile, Long uid, String fileBasePath, String fKey, Long userId,EntityMap params);

}
