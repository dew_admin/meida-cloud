package com.meida.module.file.provider.listener;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.event.FlymeSyncEvent;
import com.meida.common.base.listener.FlymeListener;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.file.provider.oss.client.MioClient;
import com.meida.starter.redis.core.RedisMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 同步minio账户
 *
 * @author zyf
 */
@Component
public class MinioAddUserListener implements FlymeListener {


    @Autowired(required = false)
    private MioClient mioClient;

    @Override
    public Object onMessage(EntityMap message) {
        if (FlymeUtils.isNotEmpty(mioClient)) {
            String userName = message.get("userName");
            String password = message.get("password");
            String optType = message.get("optType");
            if (optType.equals("add")) {
                mioClient.addUser(userName, password);
            } else {
                mioClient.deleteUser(userName);
            }

        }
        return null;
    }

}
