package com.meida.module.file.provider.oss.client;


import cn.hutool.json.JSONUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.WebSocketMsgService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.common.constants.SettingConstant;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.FileTypeUtils;
import com.meida.common.utils.HashMapUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.client.vo.OssSetting;
import com.meida.module.file.client.vo.SerializableStream;
import com.meida.module.file.provider.enums.StoreTypeEnum;
import com.meida.module.file.provider.oss.listener.OnProgressListener;
import com.meida.module.file.provider.oss.progress.ProgressInputStream;
import com.meida.module.file.provider.service.OssUploadService;
import com.meida.module.file.provider.service.SysFileService;
import com.meida.mq.adapter.MqTemplate;
import io.minio.*;
import io.minio.admin.MinioAdminClient;
import io.minio.admin.UserInfo;
import io.minio.http.Method;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * minio文件上传
 *
 * @author zyf
 */
@Component("MIO_OSS")
@Slf4j
@Order(100)
@DependsOn({"systemInitListener"})
public class MioClient implements OssUploadService {


    private RedisUtils redisUtils;

    private MinioClient minioClient;

    private MinioAdminClient minioAdminClient;

    @Autowired(required = false)
    private WebSocketMsgService webSocketMsgService;

    @Autowired
    private SysFileService fileService;

    @Autowired(required = false)
    private MqTemplate mqTemplate;

    //存储桶名称
    private String bucketName;
    //配置桶只读的是个比较长的json字符，放在配置文件中.
    private static final String MINIO_CONFIG = "config/minio-policy.json";

    @Override
    public OssSetting getOssSetting() {
        String v = redisUtils.getConfig(StoreTypeEnum.MIO_OSS.name());
        if (FlymeUtils.isNotEmpty(v)) {
            return JSONUtil.toBean(v, OssSetting.class);
        }
        return null;
    }

    @PostConstruct
    private void init() {
        OssSetting os = getOssSetting();
        System.out.println("【OssSetting】"+JSONUtil.toJsonStr(os));
        log.info("OssSetting {}",JSONUtil.toJsonStr(os));
        if (FlymeUtils.isNotEmpty(os)) {
            String used = redisUtils.getConfig(SettingConstant.OSS_USED);
            if (used.equals(StoreTypeEnum.MIO_OSS.name())) {
                String endpoint = os.getHttp() + os.getEndpoint();
                minioClient = MinioClient.builder().endpoint(endpoint).credentials(os.getAccessKey(), os.getSecretKey()).build();
                minioAdminClient = MinioAdminClient.builder().endpoint(endpoint).credentials(os.getAccessKey(), os.getSecretKey()).build();
                this.bucketName = os.getBucket();
                makeBucket(os);
            }
        }
    }


    public String getBasePath() {
        OssSetting ossSetting = getOssSetting();
        String ossBasePath = ossSetting.getHttp() + ossSetting.getEndpoint() + "/";
        return ossBasePath;
    }

    @Override
    public String getBaseOssPath() {
        OssSetting ossSetting = getOssSetting();
        String ossBasePath = getBasePath() + ossSetting.getBucket() + "/";
        return ossBasePath;
    }

    @Override
    public String getOssPath(String fileBasePath, String fileName) {
        return getBaseOssPath() + fileBasePath + fileName;
    }

    @Override
    public String getLocalPath(String fileBasePath, String fileKey) {
        return getOssSetting().getFilePath() + "/" + getOssSetting().getBucket() + "/" + fileBasePath + fileKey;
    }


    @Override
    public String upload(String localPath, String fileName, SysFile sysFile, Long uid, String fileBasePath, String fileKey, Long userId, EntityMap params) {
        String objectID = fileBasePath + fileKey;
        String fileExt = sysFile.getFileExt();
        try {
            SerializableStream serializableStream = sysFile.getSerializableStream();
            if (FlymeUtils.isEmpty(serializableStream)) {
                ApiAssert.failure("文件对象不能为空");
                return null;
            }
            InputStream inputStream = serializableStream.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(inputStream);
            ProgressInputStream fis =
                    new ProgressInputStream(bis, inputStream.available(), 0L, new OnProgressListener() {
                        @Override
                        public void onProgress(int process) {
                            if (FlymeUtils.isNotEmpty(webSocketMsgService) && FlymeUtils.isNotEmpty(uid)) {
                                Map<String, Object> processMap = new HashMap<>();
                                processMap.put(uid.toString(), process);
                                webSocketMsgService.sendMessage(userId, 1, processMap);
                            }
                        }

                        @Override
                        public void onCompleted() {
                            Boolean allowConvert = fileService.checkAllowConvert(fileExt);
                            if (allowConvert) {
                                params.put("localPath", localPath);
                                params.put("fileId", sysFile.getFileId());
                                params.put("ossPath", sysFile.getOssPath());
                                params.put("allowConvert", fileService.allowConvert(fileExt));
                                //上传完成后推送转换PDF事件
                                mqTemplate.sendMessage(QueueConstants.QUEUE_UPLOAD_CONVERT, params, 5000);
                            }

                        }
                    });
            Map<String, String> map = new HashMap<>(5);
            if (FlymeUtils.isNotEmpty(userId)) {
                map.put("userId", userId.toString());
            }
            minioClient.putObject(PutObjectArgs.builder().bucket(bucketName)
                    .contentType(FileTypeUtils.getMediaTypeValue(FilenameUtils.getExtension(objectID)))
                    .object(objectID)
                    .userMetadata(map)
                    .stream(fis, fis.available(), ObjectWriteArgs.MIN_MULTIPART_SIZE).build());
            fis.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            ApiAssert.failure("文书上传失败");
        }

        return null;
    }

    /**
     * 创建bucketName
     *
     * @param ossSetting
     */
    public void makeBucket(OssSetting ossSetting) {
        try {
            if (minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
                return;
            }
            log.warn("【{}】的桶不存在，将会创建一个。", ossSetting.getBucket());
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());

            // 设置桶为只读权限
            String policy = FlymeUtils.streamToString(new ClassPathResource(MINIO_CONFIG).getInputStream());
            policy = policy.replace("bucket-name", bucketName);
            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder()
                    .bucket(bucketName)
                    .config(policy)
                    .build());
            log.info("【{}】的桶已经创建成功", bucketName);
        } catch (Exception ex) {
            ex.printStackTrace();
            ApiAssert.failure("初始化创建 MinioClient 出错，请检查");
        }
    }

    /**
     * 修改标签.
     *
     * @param objectName objectName
     * @param tag        tag
     */
    public void updateTags(String objectName, String tag) {
        try {
            Map<String, String> tags = minioClient.getObjectTags(GetObjectTagsArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .build()).get();
            if (null == tags) {
                tags = HashMapUtils.newFixQuarterSize();
            } else {
                tags = new HashMap<>(tags);
            }
            tags.put("info", tag);
            minioClient.setObjectTags(SetObjectTagsArgs.builder().bucket(bucketName)
                    .object(objectName)
                    .tags(tags)
                    .build());
        } catch (Exception ex) {
            ApiAssert.failure("修改标签失败");
        }
    }

    /**
     * 上传文件
     *
     * @param filePath
     * @param objectID
     */
    public void uploadByPath(String filePath, String objectID) {
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            PutObjectArgs putObjectArgs = PutObjectArgs.builder().bucket(bucketName).object(objectID).stream(fileInputStream, fileInputStream.available(), ObjectWriteArgs.MIN_MULTIPART_SIZE).build();
            minioClient.putObject(putObjectArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除存储bucket
     *
     * @param bucketName 存储bucket名称
     * @return Boolean
     */
    public Boolean removeBucket(String bucketName) {
        try {
            minioClient.removeBucket(RemoveBucketArgs.builder()
                    .bucket(bucketName)
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 文件上传
     *
     * @param file 文件
     * @return Boolean
     */
    public Boolean upload(MultipartFile file) {
        try {
            PutObjectArgs objectArgs = PutObjectArgs.builder().bucket(bucketName).object(file.getOriginalFilename())
                    .stream(file.getInputStream(), file.getSize(), -1).contentType(file.getContentType()).build();
            //文件名称相同会覆盖
            minioClient.putObject(objectArgs);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 文件下载
     *
     * @param fileName 文件名称
     * @param res      response
     * @return Boolean
     */
    public void download(String fileName, HttpServletResponse res) {
        GetObjectArgs objectArgs = GetObjectArgs.builder().bucket(bucketName)
                .object(fileName).build();
        try (GetObjectResponse response = minioClient.getObject(objectArgs)) {
            byte[] buf = new byte[1024];
            int len;
            try (FastByteArrayOutputStream os = new FastByteArrayOutputStream()) {
                while ((len = response.read(buf)) != -1) {
                    os.write(buf, 0, len);
                }
                os.flush();
                byte[] bytes = os.toByteArray();
                res.setCharacterEncoding("utf-8");
                //设置强制下载不打开
                res.setContentType("application/force-download");
                res.addHeader("Content-Disposition", "attachment;fileName=" + fileName);
                try (ServletOutputStream stream = res.getOutputStream()) {
                    stream.write(bytes);
                    stream.flush();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 批量删除文件对象
     *
     * @param objects 对象名称集合
     */
    public Iterable<Result<DeleteError>> removeObjects(List<String> objects) {
        List<DeleteObject> dos = objects.stream().map(e -> new DeleteObject(e)).collect(Collectors.toList());
        Iterable<Result<DeleteError>> results = minioClient.removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(dos).build());
        for (Result<DeleteError> result : results) {
            try {
                DeleteError error = result.get();
                log.error("Error in deleting object " + error.objectName() + "; " + error.message());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return results;
    }

    /**
     * 批量删除文件对象
     *
     * @param object 对象名称集合
     */
    public Iterable<Result<DeleteError>> removeObject(String object) {
        List<DeleteObject> dos = new ArrayList<>();
        DeleteObject deleteObject = new DeleteObject(object);
        dos.add(deleteObject);
        Iterable<Result<DeleteError>> results = minioClient.removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(dos).build());
        for (Result<DeleteError> result : results) {
            try {
                DeleteError error = result.get();
                log.error("Error in deleting object " + error.objectName() + "; " + error.message());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return results;
    }

    /**
     * 查看文件对象
     *
     * @return 存储bucket内文件对象信息
     */
    public List<Item> listObjects() {
        Iterable<Result<Item>> results = minioClient.listObjects(
                ListObjectsArgs.builder().bucket(bucketName).build());
        List<Item> items = new ArrayList<>();
        try {
            for (Result<Item> result : results) {
                items.add(result.get());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return items;
    }

    /**
     * 查看文件对象
     *
     * @return 存储bucket内文件对象信息
     */
    public List<Item> listObjects(String path, Boolean recursive) {
        Iterable<Result<Item>> results = minioClient.listObjects(
                ListObjectsArgs.builder().bucket(bucketName).prefix(path).recursive(recursive).build());
        List<Item> items = new ArrayList<>();
        try {
            for (Result<Item> result : results) {
                items.add(result.get());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return items;
    }

    /**
     * 预览图片
     *
     * @param fileName
     * @return
     */
    public String preview(String fileName) {
        // 查看文件地址
        new GetPresignedObjectUrlArgs();
        GetPresignedObjectUrlArgs build = GetPresignedObjectUrlArgs.builder().bucket(bucketName).object(fileName).method(Method.GET).build();
        try {
            String url = minioClient.getPresignedObjectUrl(build);
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 复制文件
     *
     * @param sourceBucket 源文件桶
     * @param sourceObject 源文件名
     * @param bucket       目标桶
     * @param object       目标文件
     * @return void
     **/
    public void copyObject(String sourceBucket, String sourceObject, String bucket, String object) {
        try {
            CopySource source = CopySource.builder().bucket(sourceBucket).object(sourceObject).build();
            minioClient.copyObject(CopyObjectArgs.builder().bucket(bucket).object(object).source(source).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加minio账户
     *
     * @param userName
     * @param password
     */
    public void addUser(String userName, String password) {
        try {
            List<String> memberOf = new ArrayList<>();
            memberOf.add("upload");
            minioAdminClient.addUser(userName, UserInfo.Status.ENABLED, password, "readwrite", memberOf);
            minioAdminClient.setPolicy(userName, false, "readwrite");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除minio账户
     *
     * @param userName
     */
    public void deleteUser(String userName) {
        try {
            minioAdminClient.deleteUser(userName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public MioClient(RedisUtils redisUtils) {
        this.redisUtils = redisUtils;
    }
}
