package com.meida.module.file.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.file.client.entity.MinioLog;
import com.meida.module.file.client.entity.SysFile;

import java.util.List;

/**
 * minioLog 服务类
 *
 * @author flyme
 * @date 2019-06-03
 */
public interface MinioLogService extends IBaseService<MinioLog> {
    /**
     * 查询用户上传日志
     *
     * @param userName
     * @param status
     * @return
     */
    List<MinioLog> selectByUserName(String userName, Integer status);

    /**
     * 更新上传日志
     * @param minioLogId
     * @return
     */
    boolean updateStatus(Long minioLogId);

    /**
     * 删除上传日志
     * @param userName
     * @param status
     * @return
     */
    Boolean deleteByUserName(String userName, Integer status);
}
