package com.meida.module.file.provider.service.impl;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.common.collect.Lists;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.configuration.OpenCommonProperties;
import com.meida.common.constants.FileTypeConstant;
import com.meida.common.constants.QueueConstants;
import com.meida.common.constants.SettingConstant;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.thread.IThreadPoolExecutorService;
import com.meida.common.thread.ThreadHandler;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.ConvertUtils;
import com.meida.common.utils.HashUtil;
import com.meida.common.utils.JsonUtils;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.client.vo.FileRequest;
import com.meida.module.file.client.vo.SerializableStream;
import com.meida.module.file.provider.enums.StoreTypeEnum;
import com.meida.module.file.provider.handler.FileUploadHandler;
import com.meida.module.file.provider.mapper.SysFileMapper;
import com.meida.module.file.provider.oss.FileUploadUtil;
import com.meida.module.file.provider.oss.client.MioClient;
import com.meida.module.file.provider.service.OssUploadService;
import com.meida.module.file.provider.service.SysFileService;
import com.meida.starter.fileview.conver.MediaUtils;
import com.meida.starter.fileview.conver.OfficeUtils;
import com.meida.starter.fileview.result.ConvertResult;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 文件表 服务实现类
 *
 * @author flyme
 * @date 2019-06-03
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SysFileServiceImpl extends BaseServiceImpl<SysFileMapper, SysFile> implements SysFileService {
    @Autowired
    private OpenCommonProperties openCommonProperties;
    @Autowired(required = false)
    private Map<String, OssUploadService> ossUploadServiceMap;
    @Autowired(required = false)
    private Map<String, FileUploadHandler> fileUploadHandlerMap;

    @Autowired
    private MioClient mioClient;

    @Autowired
    private IThreadPoolExecutorService executorService;

    @Value("${meida.uploadFolder:''}")
    String uploadFolder;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody getFileList(Long busId, String busType) {
        CriteriaQuery<SysFile> cq = new CriteriaQuery<SysFile>(SysFile.class);
        cq.select("file.*");
        cq.select("CONCAT('" + openCommonProperties.getFileServerAddr() + "/1/',file.fileId) url");
        cq.select("CONCAT('" + openCommonProperties.getFileServerAddr() + "/2/',file.fileId) smallUrl");
        cq.eq("busId", busId);
        cq.eq("busType", busType);
        return baseList(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectFileList(Long busId, Class cls) {
        return selectFileListByFileType(busId, cls, null);
    }

    @Override
    public List<EntityMap> selectFileListByFileType(Long busId, Class cls, String fileType) {
        return selectFileListByFileType(busId, cls.getSimpleName(), fileType);
    }

    @Override
    public List<EntityMap> selectFileListByFileType(Long busId, String clsName, String fileType) {
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(SysFile.class);
        String used = redisUtils.getConfig(SettingConstant.OSS_USED);
        StoreTypeEnum storeTypeEnum = StoreTypeEnum.getByName(used);
        if (used.equals(storeTypeEnum.LOCAL_OSS)) {
            cq.select(SysFile.class, "fileUrl url");
        } else {
            cq.select(SysFile.class, "ossPath url");
        }
        cq.select(SysFile.class, "fileId", "fileId uid", "fileSize", "fileName name", "smallOssPath url", "ossPath");
        cq.eq("busId", busId);
        if (FlymeUtils.isNotEmpty(fileType)) {
            cq.like("fileType", fileType);
        }
        cq.eq("busType", clsName);
        ResultBody<List<EntityMap>> resultBody = baseList(cq);
        if (resultBody.isOk()) {
            return resultBody.getData();
        }
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectDeleteFileList(Long[] fileIds, Long busId, String busType) {
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(SysFile.class);
        if (FlymeUtils.isNotEmpty(fileIds)) {
            cq.select(SysFile.class, "fileId", "fileId uid", "localPath", "fileUrl", "ossPath", "smallOssPath url");
            cq.in(true, "fileId", fileIds);
            ResultBody<List<EntityMap>> resultBody = baseList(cq);
            return resultBody.getData();
        } else {
            return null;
        }

    }

    @Override
    public boolean setBusIdAndBusType(Long busId, String busType, String fileIds) {
        String[] split = fileIds.split(",");
        ArrayList<Long> longIds = new ArrayList<>();
        for (String fileId : split) {
            Long id = Long.valueOf(fileId);
            longIds.add(id);
        }
        CriteriaQuery<SysFile> q = new CriteriaQuery<SysFile>(SysFile.class);
        List<SysFile> files = list(q.in("fileId", longIds));
        for (SysFile file : files) {
            file.setBusId(busId).setBusType(busType);
        }
        return updateBatchById(files);
    }

    @Override
    public List<SysFile> selectByBusId(Long busId) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        cq.eq(true, "busId", busId);
        return list(cq);
    }

    @Override
    public boolean deleteByBusId(Long busId) {
        List<SysFile> fileList = selectByBusId(busId);
        if (FlymeUtils.isNotEmpty(fileList)) {
            for (SysFile sysFile : fileList) {
                cn.hutool.core.io.FileUtil.del(sysFile.getLocalPath());
                removeById(sysFile.getFileId());
            }
        }
        return true;
    }

    @Override
    public EntityMap upload(HttpServletRequest request, MultipartFile file, Long busId, String busType, String handlerName) {
        Long userId = OpenHelper.getUserId();
        EntityMap params = new EntityMap();
        params.put("busType", busType);
        params.put("userId", userId);
        params.put("busId", busId);
        return uploadFile(request, file, null, busId, busType, null, userId, false, handlerName, params);
    }


    private EntityMap uploadFile(HttpServletRequest request, MultipartFile file, Long uid, Long busId, String busType, String base64Img, Long userId, Boolean batchUpload, String handlerName, EntityMap params) {
        if (FlymeUtils.isEmpty(file)) {
            ApiAssert.failure("上传文件不能为空");
        }
        //存储方式
        String used = redisUtils.getConfig(SettingConstant.OSS_USED);
        ApiAssert.isNotEmpty("未配置文件上传方式", used);
        StoreTypeEnum storeTypeEnum = StoreTypeEnum.getByName(used);
        ApiAssert.isNotEmpty(used + "文件上传方式不存在", storeTypeEnum);
        //文件上传服务接口
        OssUploadService ossUploadService = ossUploadServiceMap.get(used);
        ApiAssert.isNotEmpty("ossUploadService获取异常", ossUploadService);
        FileUploadHandler fileUploadHandler = null;
        if (FlymeUtils.isNotEmpty(handlerName)) {
            fileUploadHandler = fileUploadHandlerMap.get(handlerName);
            if (FlymeUtils.isNotEmpty(fileUploadHandler)) {
                Boolean check = fileUploadHandler.validation(request, busId, params);
                if (!check) {
                    ApiAssert.failure("校验失败");
                }
            }
        }
        Object object = applicationContext.getEnvironment().getProperty(CommonConstants.CLOUD_SERVER_KEY);
        String fileBasePath = "";
        if (FlymeUtils.isEmpty(object)) {
            //添加固定目录 //TODO 待优化
            fileBasePath += "DA-YW/";
        }
        fileBasePath += FileUploadUtil.datePath() + "/";
        //真实文件名
        String originalFilename = file.getOriginalFilename();
        //重命名文件
        String fileKey = FlymeUtils.renameFile(originalFilename);
        InputStream inputStream = FileUploadUtil.getInputStream(file);
        SysFile sysFile = new SysFile();
        //base64格式文件
        sysFile.setBase64Img(base64Img);
        //计算文件MD5
        sysFile.setSha256(HashUtil.sha256(inputStream));
        sysFile.setSerializableStream(new SerializableStream(file));
        sysFile.setStoreType(storeTypeEnum.getValue());
        String fileUrl = "";
        sysFile.setFileId(IdWorker.getId());
        sysFile.setFileName(originalFilename);
        sysFile.setFileSize(file.getSize());
        sysFile.setBusId(busId);
        sysFile.setBusType(busType);
        sysFile.setFileType(file.getContentType());
        sysFile.setFileKey(fileBasePath + fileKey);
        sysFile.setHasSmallOssPath(false);
        String ossSmallPath = "";
        String fileExt = FileUploadUtil.getExtend(originalFilename, "");
        sysFile.setFileExt(fileExt);
        //获取图片文件属性
        FileUploadUtil.getImgProperty(file, originalFilename, sysFile);
        //获取文件类定义
        EntityMap fileTypeMap = getFileType(fileExt);
        if (FlymeUtils.isNotEmpty(fileTypeMap)) {
            Integer fileGroup = fileTypeMap.getInt("typeGroup");
            sysFile.setFileGroup(fileGroup);
        }
        //提前获取OSS路径
        sysFile.setOssPath(ossUploadService.getOssPath(fileBasePath, fileKey));
        sysFile.setLocalPath(ossUploadService.getLocalPath(fileBasePath, fileKey));
        //异步上传到OSS
        uploadToOss(request, ossUploadService, sysFile, used, fileKey, uid, fileBasePath, userId, batchUpload, params);
        //是否保存到系统默认文件表
        Boolean saveTag = true;
        if (FlymeUtils.isNotEmpty(fileUploadHandler)) {
            //调用自定义保存逻辑
            saveTag = fileUploadHandler.customSaveFile(request, sysFile);
        }
        if (saveTag) {
            this.save(sysFile);
        }
        //文档转换(可选)
        converFile(sysFile, fileUploadHandler);
        EntityMap response = new EntityMap();
        //返回前端的数据
        response.put("url", sysFile.getSmallOssPath());
        response.put("ossPath", sysFile.getOssPath());
        response.put("uid", sysFile.getFileId());
        response.put("status", "done");
        response.put("name", sysFile.getFileName());
        return response;
    }

    @Override
    public EntityMap upload(HttpServletRequest request, MultipartFile[] files, Long busId, String busType, String handlerName) {
        return upload(request, files, null, busId, busType, handlerName);
    }

    @Override
    public EntityMap upload(HttpServletRequest request, MultipartFile[] files, Long[] uid, Long busId, String busType, String handlerName) {
        FileUploadHandler fileUploadHandler = null;
        Long userId = OpenHelper.getUserId();
        EntityMap params = new EntityMap();
        params.put("busType", busType);
        params.put("userId", userId);
        params.put("busId", busId);
        if (FlymeUtils.isNotEmpty(handlerName)) {
            fileUploadHandler = fileUploadHandlerMap.get(handlerName);
            if (FlymeUtils.isNotEmpty(fileUploadHandler)) {
                Boolean check = fileUploadHandler.validation(request, busId, params);
                if (!check) {
                    ApiAssert.failure("校验失败");
                }
            }
        }
        EntityMap result = new EntityMap();
        List<String> fileIds = new ArrayList<>();

        //用户上传文件进度key
        String mapKey = "upload:process:" + userId;
        Map uploadMap = new ConcurrentHashMap<>();
        //存入redis
        redisUtils.set(mapKey, uploadMap);
        if (FlymeUtils.isNotEmpty(files)) {
            List<FileRequest> fileRequestList = new ArrayList<>();
            int i = 0;
            for (MultipartFile file : files) {
                FileRequest fileRequest = new FileRequest();
                fileRequest.setMultipartFile(file);
                if (FlymeUtils.isNotEmpty(uid)) {
                    fileRequest.setUid(uid[i]);
                }
                fileRequestList.add(fileRequest);
                i++;
            }
            List<EntityMap> list = executorService.handlerList(fileRequestList, 5, new ThreadHandler<FileRequest>() {
                @Override
                public EntityMap handlerObj(String threadName, Integer threadNo, FileRequest fileRequest) {
                    MultipartFile file = fileRequest.getMultipartFile();
                    Long fileId = fileRequest.getUid();
                    EntityMap map = uploadFile(request, file, fileId, busId, busType, null, userId, true, handlerName, params);
                    fileIds.add(map.get("uid").toString());
                    return map;
                }
            });
            result.put("fileList", JsonUtils.beanToJson(list));
            result.put("fileIds", String.join(",", fileIds));
        }
        return result;
    }

    @Override
    public List<EntityMap> selectFileList(List<Long> fileIds, CriteriaQueryCallBack callBack) {
        if (FlymeUtils.isEmpty(fileIds)) {
            return Lists.newArrayList();
        }
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        if (FlymeUtils.isNotEmpty(callBack)) {
            callBack.init(cq);
        }
        cq.in("fileId", fileIds);
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> selectFileListLikeFileType(Long busId, String clsName, String fileType) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        cq.select(SysFile.class, "fileId", "fileId uid", "fileName name", "fileSize", "fileUrl", "fileUrl", "smallOssPath url", "ossPath");
        cq.eq(true, "busId", busId);
        cq.eq(true, "busType", clsName);
        if (FlymeUtils.isNotEmpty(fileType)) {
            cq.like("fileType", fileType);
        }
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> selectFileListNotLikeFileType(Long busId, String clsName, String fileType) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        cq.select(SysFile.class, "fileId", "fileId uid", "fileName name", "fileSize", "fileUrl", "smallOssPath url", "ossPath");
        cq.eq(true, "busId", busId);
        cq.eq(true, "busType", clsName);
        if (FlymeUtils.isNotEmpty(fileType)) {
            cq.notLike("fileType", fileType);
        }
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> selectFileList(Long busId, String clsName, CriteriaQueryCallBack callBack) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        if (FlymeUtils.isNotEmpty(callBack)) {
            callBack.init(cq);
        }
        cq.eq(true, "busId", busId);
        cq.eq(true, "busType", clsName);
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> selectFileListByFileGroup(Long busId, String clsName, Integer fileGroup) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        cq.select(SysFile.class, "fileId", "fileId uid", "fileName name", "fileSize", "fileUrl", "localPath", "ossPath", "smallOssPath url");
        cq.eq("busId", busId);
        cq.eq("busType", clsName);
        if (FlymeUtils.isNotEmpty(fileGroup)) {
            cq.eq("fileGroup", fileGroup);
        }
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> selectFileListByFileGroup(Long busId, String clsName, Integer fileGroup, CriteriaQueryCallBack callBack) {
        CriteriaQuery cq = new CriteriaQuery(SysFile.class);
        if (FlymeUtils.isNotEmpty(callBack)) {
            callBack.init(cq);
        }
        cq.eq(true, "busId", busId);
        cq.eq(true, "busType", clsName);
        cq.eq(true, "fileGroup", fileGroup);
        return selectEntityMap(cq);
    }

    @Override
    public String getCoverImg(String fileIds) {
        String coverImg = "";
        List<Long> fileIdList = ConvertUtils.stringToLongList(fileIds);
        List<EntityMap> fileList = selectFileList(fileIdList, q -> {
            q.select("fileId", "smallOssPath");
            q.like("fileType", "image");
            q.orderByAsc("createTime");
            q.limit(1);
        });
        if (FlymeUtils.isNotEmpty(fileList)) {
            EntityMap fileMap = fileList.get(0);
            coverImg = fileMap.get("smallOssPath");
        }
        return coverImg;
    }

    /**
     * 上传文件到OSS
     */
    private void uploadToOss(HttpServletRequest request, OssUploadService ossUploadService, SysFile sysFile, String used, String fileKey, Long uid, String fileBasePath, Long userId, Boolean batchUpload, EntityMap params) {

        String localPath = sysFile.getLocalPath();
        String fileName = sysFile.getFileName();
        if (batchUpload) {
            //批量上传已经是多线程,不在走队列
            if (FlymeUtils.isNotEmpty(ossUploadService)) {
                //上传OSS
                ossUploadService.upload(localPath, fileName, sysFile, uid, fileBasePath, fileKey, userId, params);
            }
        } else {
            EntityMap ossMap = new EntityMap();
            ossMap.put("sysFile", sysFile);
            ossMap.put("used", used);
            ossMap.put("userId", userId);
            //前端文件id
            ossMap.put("uid", uid);
            ossMap.put("fKey", fileKey);
            ossMap.put("fileBasePath", fileBasePath);
            //是否异步上传(默认异步)
            String sync = request.getParameter("sync");
            if (FlymeUtils.isNotEmpty(sync) && sync.equals("0")) {
                ossUploadService.upload(sysFile.getLocalPath(), fileName, sysFile, null, fileBasePath, fileKey, userId, ossMap);
            } else {
                //异步上传文件到OSS
                mqTemplate.convertAndSend(QueueConstants.QUEUE_UPLOADOSS, ossMap);
            }
        }

    }

    /**
     * 文字识别
     */
    private void syncOcr(SysFile f, String localPath, FileUploadHandler fileUploadHandler) {
        if (FlymeUtils.isNotEmpty(fileUploadHandler)) {
            //转换前置事件
            Boolean ocrBefore = fileUploadHandler.ocrBefore(f);
            if (ocrBefore) {
                String fileType = f.getFileType();
                //允许ocr识别的文件类型才执行
                if (!checkAllowOcr(fileType).equals(CommonConstants.INT_0)) {
                    EntityMap ossMap = new EntityMap();
                    ossMap.put("fileId", f.getFileId());
                    ossMap.put("filePath", localPath);
                    ossMap.put("fileExt", f.getFileExt());
                    //异步识别文字
                    mqTemplate.convertAndSend(QueueConstants.QUEUE_OCR, ossMap);
                }
            }
        }
    }

    /**
     * 文档转换
     */
    @Override
    public void converFile(SysFile sysFile, FileUploadHandler fileUploadHandler) {
        String fileExt = sysFile.getFileExt();
        if (FlymeUtils.isNotEmpty(fileUploadHandler)) {
            String fileType = sysFile.getFileExt();
            //是否开启转换
            Boolean openConvert = fileUploadHandler.openConvert();
            if (!openConvert) {
                return;
            }
            //前置条件是否通过
            Boolean converBefore = fileUploadHandler.convertBefore(sysFile);
            if (!converBefore) {
                return;
            }
            //是否允许转换
            Boolean allowConvert = checkAllowConvert(fileType);
            if (!allowConvert) {
                return;
            }
            //异步执行文件转换
            executorService.execute(() -> {
                //String filePath = sysFile.getLocalPath();
                //File file = new File(filePath);
                String filePath = uploadFolder + sysFile.getFileName();
                File file = getNetUrlHttp(sysFile.getOssPath(), filePath);
                try {
                    if (file.exists()) {
                        ConvertResult result = new ConvertResult(null, false);

                        File targetDir = new File(file.getParentFile(), "pdf");
                        if (!targetDir.exists()) {
                            targetDir.mkdirs();
                        }
                        if (allowConvert(fileExt).equals(CommonConstants.INT_1)) {
                            result = OfficeUtils.convertPdf(file, targetDir, openCommonProperties.getOfficePath());
                        } /*if (result.isSuccess()) {
                            fileUploadHandler.convertSuccess(sysFile.getFileId(), result.getTargetFile());
                            //转换后进行文字识别
                            if (checkAllowOcr(fileType).equals(CommonConstants.INT_2)) {
                                syncOcr(sysFile, result.getTargetFile(), fileUploadHandler);
                            }
                        } else {
                            log.error("文档转换失败:" + sysFile.getFileName());
                        }*/
                        //视频转换
                        if (allowConvert(fileExt).equals(CommonConstants.INT_2)) {
                            //视频转换
                            result = MediaUtils.convertMedia(file, targetDir, openCommonProperties.getFfmpegPath(), CommonConstants.INT_2);

                            //MediaUtils.convertMedia(file, targetDir, openCommonProperties.getFfmpegPath(), CommonConstants.INT_2);
                        }
                        //音频转换
                        if (allowConvert(fileExt).equals(CommonConstants.INT_3)) {
                            result = MediaUtils.convertMedia(file, targetDir, openCommonProperties.getFfmpegPath(), CommonConstants.INT_3);
                        }
                        if (result.isSuccess()) {
                            log.info("转换回调,文件路径:" + result.getTargetFile());
                            FileUtil.del(file);
                            updateFilePath(sysFile, result.getTargetFile());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        }
        //文档内容识别(可选)
        if (checkAllowOcr(fileExt).equals(CommonConstants.INT_1)) {
            syncOcr(sysFile, sysFile.getLocalPath(), fileUploadHandler);
        }
    }

    private void updateFilePath(SysFile params, String pdfPath) {

        try {
            String ossPath = getOssPath(params.getOssPath());
            Long categoryId = params.getBusId();
            Long fileId = params.getFileId();
            if (FlymeUtils.allNotNull(categoryId, fileId)) {
                File file = new File(pdfPath);
                FileInputStream input = new FileInputStream(file);
                //重命名文件
                String fileKey = FileUploadUtil.datePath() + "/" + FlymeUtils.renameFile(file.getName());
                String contentType = MediaTypeFactory.getMediaType(file.getName()).orElse(MediaType.APPLICATION_OCTET_STREAM).toString();
                MultipartFile targetFiles = new MockMultipartFile(file.getName(), fileKey, contentType, IOUtils.toByteArray(input));
                mioClient.upload(targetFiles);
                FileUtil.del(pdfPath);

                String selSql = "select tableName from arc_category where categoryId=?";
                log.info("查询表名" + selSql);
                Map<String, Object> category = jdbcTemplate.queryForMap(selSql, categoryId);
                if (FlymeUtils.isNotEmpty(category)) {
                    EntityMap map = new EntityMap(category);
                    String tableName = map.get("tableName");
                    if (FlymeUtils.isNotEmpty(tableName)) {
                        String update = "update arc_original" + tableName;
                        String updateSql = update + " set filePath=? where arcOriginalId=?";
                        log.info("更新PDF路径" + updateSql);
                        jdbcTemplate.update(updateSql, ossPath + fileKey, fileId);
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getOssPath(String path) {
        int s = path.lastIndexOf("archive");
        String a = path.substring(0, s + 8);
        return a;
    }

    private File getNetUrlHttp(String path, String localPath) {
        //对本地文件命名，path是http的完整路径，主要得到资源的名字
        String newUrl = path;
        newUrl = newUrl.split("[?]")[0];
        String[] bb = newUrl.split("/");
        //得到最后一个分隔符后的名字
        String fileName = bb[bb.length - 1];
        File file = null;

        URL urlfile;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            //判断文件的父级目录是否存在，不存在则创建
            file = new File(localPath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                //创建文件
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //下载
            urlfile = new URL(newUrl);
            inputStream = urlfile.openStream();
            outputStream = new FileOutputStream(file);

            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != outputStream) {
                    outputStream.close();
                }
                if (null != inputStream) {
                    inputStream.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /**
     * 查询上传文件配置
     *
     * @param fileType
     * @return
     */
    private EntityMap getFileType(String fileType) {
        EntityMap fileTypeMap = (EntityMap) redisUtils.get(FileTypeConstant.RedisKey);
        if (FlymeUtils.isNotEmpty(fileTypeMap)) {
            return fileTypeMap.get(fileType);
        }
        return null;
    }

    /**
     * 检测是否允许转换PDF
     *
     * @param fileType
     * @return
     */
    @Override
    public Boolean checkAllowConvert(String fileType) {
        Boolean result = false;
        EntityMap map = getFileType(fileType);
        if (FlymeUtils.isNotEmpty(map)) {
            int allowConvert = map.getInt("allowConvert", 0);
            result = allowConvert > CommonConstants.MIN_PAGE;
        }
        return result;
    }

    @Override
    public Integer allowConvert(String fileType) {
        EntityMap map = getFileType(fileType);
        return map.getInt("allowConvert", 0);
    }


    /**
     * 检测是否允许ocr识别
     *
     * @param fileType
     * @return 1 直接识别，2等待转PDF后识别
     */
    private Integer checkAllowOcr(String fileType) {
        Integer result = 0;
        EntityMap map = getFileType(fileType);
        if (FlymeUtils.isNotEmpty(map)) {
            result = map.getInt("allowOcr", 0);
        }
        return result;
    }

    /**
     * 通过队列上传
     */
    @Override
    public void uploadByMq(EntityMap map, Channel channel) {
        try {
            String used = map.get("used", StoreTypeEnum.ALI_OSS.name());
            OssUploadService ossUploadService = ossUploadServiceMap.get(used);
            SysFile sysFile = map.get("sysFile");
            String filePath = sysFile.getLocalPath();
            Long userId = map.getLong("userId");
            String fileName = sysFile.getFileName();
            String fileBasePath = map.get("fileBasePath");
            String fKey = map.get("fKey");
            if (FlymeUtils.isNotEmpty(sysFile)) {
                //上传OSS
                ossUploadService.upload(filePath, fileName, sysFile, null, fileBasePath, fKey, userId, map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
