package com.meida.module.file.provider.controller;

import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.file.client.entity.SysFileType;
import com.meida.module.file.provider.service.SysFileTypeService;


/**
 * 文件类型控制器
 *
 * @author flyme
 * @date 2021-12-29
 */
@RestController
@RequestMapping("/file/sft/")
public class SysFileTypeController extends BaseController<SysFileTypeService, SysFileType> {

    @ApiOperation(value = "文件类型-分页列表", notes = "文件类型分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "文件类型-列表", notes = "文件类型列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "文件类型-列表", notes = "文件类型列表")
    @GetMapping(value = "listByUsed")
    public ResultBody list() {
        List<EntityMap> list= bizService.listByUsed();
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "获取可上传文件类型", notes = "获取可上传文件类型")
    @GetMapping(value = "getAccept")
    public ResultBody getAccept(@RequestParam(required = false) String group) {
        List<EntityMap> accept = bizService.selectAccept(group);
        return ResultBody.ok(accept);
    }

    @ApiOperation(value = "文件类型-添加", notes = "添加文件类型")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "文件类型-更新", notes = "更新文件类型")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "文件类型-删除", notes = "删除文件类型")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "文件类型-详情", notes = "文件类型详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    /**
     * 更新文件上传状态
     */
    @ApiOperation(value = "更新文件上传状态", notes = "更新文件上传状态")
    @PostMapping(value = "setStatus")
    @ApiImplicitParam(name = "fileTypeId", required = true, value = "主键", paramType = "form")
    public ResultBody setStatus(@RequestParam(value = "fileTypeId") Long fileTypeId) {
        return bizService.setStatus(fileTypeId);
    }
}
