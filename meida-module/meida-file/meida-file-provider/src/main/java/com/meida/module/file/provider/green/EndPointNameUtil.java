package com.meida.module.file.provider.green;

public class EndPointNameUtil {

    protected static String getDomain(String regionId) {
        if ("cn-shanghai".equals(regionId)) {
            return "green.cn-shanghai.aliyuncs.com";
        }

        if ("cn-beijing".equals(regionId)) {
            return "green.cn-beijing.aliyuncs.com";
        }

        if ("ap-southeast-1".equals(regionId)) {
            return "green.ap-southeast-1.aliyuncs.com";
        }

        if ("us-west-1".equals(regionId)) {
            return "green.us-west-1.aliyuncs.com";
        }

        return "green.cn-shanghai.aliyuncs.com";
    }
}
