package com.meida.module.file.provider.handler;

import com.meida.common.base.entity.EntityMap;

import java.util.List;

public interface FileRemoveHandler {
    /**
     * 扩展文件删除
     *
     * @param result fileId集合
     * @return
     */
    default void complete(List<EntityMap> result) {

    }
}
