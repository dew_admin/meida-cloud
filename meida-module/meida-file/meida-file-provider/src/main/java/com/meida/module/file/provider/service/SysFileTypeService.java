package com.meida.module.file.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.file.client.entity.SysFileType;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 文件类型 接口
 *
 * @author flyme
 * @date 2021-12-29
 */
public interface SysFileTypeService extends IBaseService<SysFileType> {

    /**
     * 查询可上传的文件类型
     *
     * @param group 类型分组1:图片2:文件3:音频,4:视频(多个逗号分隔)
     * @return
     */
    List<EntityMap> selectAccept(String group);

    /**
     * 查询已启用的文件类型
     * @return
     */
    List<EntityMap> listByUsed();

    /**
     * 设置状态
     *
     * @param fileTypeId
     * @return
     */
    ResultBody setStatus(Long fileTypeId);
}
