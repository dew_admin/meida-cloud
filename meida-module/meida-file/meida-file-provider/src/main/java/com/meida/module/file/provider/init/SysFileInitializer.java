package com.meida.module.file.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.file.client.entity.SysFile;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class SysFileInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return SysFile.class;
    }
}
