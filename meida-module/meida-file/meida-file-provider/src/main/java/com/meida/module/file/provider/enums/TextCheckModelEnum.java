package com.meida.module.file.provider.enums;

public enum TextCheckModelEnum {
    keyword("keyword"),
    antispam("antispam");

    private final String checkModel;

    public String getCheckModel() {
        return checkModel;
    }

    TextCheckModelEnum(String checkModel) {
        this.checkModel = checkModel;
    }
}
