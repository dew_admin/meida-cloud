package com.meida.module.file.provider.controller;


import cn.hutool.core.util.StrUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.BaseAdminUserService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.event.FlymeEventClient;
import com.meida.module.file.client.entity.MinioLog;
import com.meida.module.file.provider.service.MinioLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * minioLog文件上传控制器
 *
 * @author flyme
 * @date 2019-06-03
 */
@Slf4j
@Api(tags = "文件管理-mino账户修改")
@RequestMapping("/minio/")
@RestController
public class MinioAdminController extends BaseController<MinioLogService, MinioLog> {

    @Autowired(required = false)
    private BaseAdminUserService baseAdminUserService;

    @Autowired(required = false)
    private FlymeEventClient flymeEventClient;

    @ApiOperation(value = "修改minio账户", notes = "修改minio账户")
    @PostMapping(value = "user/update")
    public ResultBody remove(@RequestParam(required = false) Long userId, @RequestParam String password) {
        if (FlymeUtils.isNotEmpty(baseAdminUserService)) {
            if (FlymeUtils.isEmpty(userId)) {
                userId = OpenHelper.getUserId();
            }
            EntityMap baseUser = baseAdminUserService.getAdminUserById(userId,"1");
            String account = baseUser.get("userName");
            EntityMap map = new EntityMap();
            map.put("userName", account);
            map.put("password", password);
            map.put("userId", userId);
            map.put("optType", "delete");
            flymeEventClient.publishEvent("minioAddUserListener", map);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.put("optType", "add");
            flymeEventClient.publishEvent("minioAddUserListener", map);
        }
        return ResultBody.ok();
    }

    @ApiOperation(value = "批量修改minio账户", notes = "批量修改minio账户")
    @PostMapping(value = "user/batchUpdate")
    public ResultBody batchUpdate(@RequestParam(required = false) String userIds, @RequestParam String password) {
        if (FlymeUtils.isNotEmpty(baseAdminUserService)) {
            for (String s : StrUtil.split(userIds, ',')) {


                EntityMap baseUser = baseAdminUserService.getAdminUserById(Long.valueOf(s), "1");
                String account = baseUser.get("userName");
                EntityMap map = new EntityMap();
                map.put("userName", account);
                map.put("password", password);
                map.put("userId", s);
                map.put("optType", "delete");
                flymeEventClient.publishEvent("minioAddUserListener", map);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                map.put("optType", "add");
                flymeEventClient.publishEvent("minioAddUserListener", map);
            }

        }
        return ResultBody.ok();
    }
}
