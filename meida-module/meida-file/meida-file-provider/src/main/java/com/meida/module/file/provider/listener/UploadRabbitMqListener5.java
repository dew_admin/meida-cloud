package com.meida.module.file.provider.listener;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.QueueConstants;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.provider.enums.StoreTypeEnum;
import com.meida.module.file.provider.service.OssUploadService;
import com.meida.module.file.provider.service.SysFileService;
import com.meida.mq.annotation.MsgListener;
import com.meida.starter.rabbitmq.config.RabbitComponent;
import com.meida.starter.rabbitmq.core.BaseRabbiMqHandler;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import lombok.AllArgsConstructor;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

import java.io.InputStream;
import java.util.Map;

/**
 * 上传文件到OSS
 *
 * @author zyf
 */
//@RabbitComponent(value = "uploadRabbitMqListener5")
//@AllArgsConstructor
public class UploadRabbitMqListener5 extends BaseRabbiMqHandler<EntityMap> {

    private SysFileService sysFileService;

    @MsgListener(queues = QueueConstants.QUEUE_UPLOADOSS)
    public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        super.onMessage(baseMap, deliveryTag, channel, new MqListener<EntityMap>() {
            @Override
            public void handler(EntityMap map, Channel channel) {
                sysFileService.uploadByMq(map, channel);
            }
        });
    }

}
