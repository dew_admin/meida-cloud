package com.meida.module.file.provider.mapper;

import com.meida.module.file.client.entity.SysFileType;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 文件类型 Mapper 接口
 * @author flyme
 * @date 2021-12-29
 */
public interface SysFileTypeMapper extends SuperMapper<SysFileType> {

}
