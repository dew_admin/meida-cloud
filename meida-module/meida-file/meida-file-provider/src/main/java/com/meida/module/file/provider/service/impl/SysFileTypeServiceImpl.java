package com.meida.module.file.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.FileTypeConstant;
import com.meida.common.enums.StateEnum;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.module.file.client.entity.SysFileType;
import com.meida.module.file.provider.mapper.SysFileTypeMapper;
import com.meida.module.file.provider.service.SysFileTypeService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 文件类型接口实现类
 *
 * @author flyme
 * @date 2021-12-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Order(2)
public class SysFileTypeServiceImpl extends BaseServiceImpl<SysFileTypeMapper, SysFileType> implements SysFileTypeService {



    @PostConstruct
    public void init() {
        this.initFileType();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysFileType sft, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysFileType> cq, SysFileType sft, EntityMap requestMap) {
        cq.orderByDesc("sft.createTime");
        return ResultBody.ok();
    }

    @Override
    @Cacheable(cacheNames = "accept")
    public List<EntityMap> selectAccept(String group) {
        CriteriaQuery cq = new CriteriaQuery(SysFileType.class);
        cq.select("typeName", "allowMaxSize");
        if (FlymeUtils.isNotEmpty(group)) {
            String[] groupArray = group.split(",");
            cq.in("fileGroup", groupArray);
        }
        cq.eq("state", CommonConstants.ENABLED);
        List<EntityMap> fileTypeList = selectEntityMap(cq);
        return fileTypeList;
    }

    @Override
    public List<EntityMap> listByUsed() {
        CriteriaQuery cq = new CriteriaQuery(SysFileType.class);
        cq.eq(true,"useState", CommonConstants.ENABLED);
        List<EntityMap> fileTypeList = selectEntityMap(cq);
        return fileTypeList;
    }

    /**
     * 初始化上传文件配置
     *
     * @return
     */
    public EntityMap initFileType() {
        CriteriaQuery cq = new CriteriaQuery(SysFileType.class);
        cq.select("*");
        cq.eq("state", CommonConstants.ENABLED);
        List<EntityMap> list = selectEntityMap(cq);
        /*EntityMap fileTypeMap = (EntityMap) redisUtils.get(FileTypeConstant.RedisKey);
        if (FlymeUtils.isEmpty(fileTypeMap)) {
            fileTypeMap = new EntityMap();
            if (FlymeUtils.isNotEmpty(list)) {
                for (EntityMap entityMap : list) {
                    String typeName = entityMap.get("typeName");
                    fileTypeMap.put(typeName, entityMap);
                }
                redisUtils.set(FileTypeConstant.RedisKey, fileTypeMap);
            }
        }*/
        EntityMap fileTypeMap = new EntityMap();
        if (FlymeUtils.isNotEmpty(list)) {
            for (EntityMap entityMap : list) {
                String typeName = entityMap.get("typeName");
                if(FlymeUtils.isNotEmpty(typeName)) {
                    fileTypeMap.put(typeName, entityMap);
                }
            }
            redisUtils.set(FileTypeConstant.RedisKey, fileTypeMap);
        }

        return fileTypeMap;
    }

    @Override
    @CacheEvict(value = {"accept"}, allEntries = true)
    public ResultBody setStatus(Long fileTypeId) {
        ResultBody resultBody = new ResultBody();
        SysFileType fileType = getById(fileTypeId);
        Integer stateEnum = fileType.getState();
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.eq("fileTypeId", fileTypeId);
        if (stateEnum.equals(StateEnum.ENABLE.getCode())) {
            cu.set(true, "state", StateEnum.DISABLE.getCode());
            resultBody.setMsg(StateEnum.DISABLE.getName() + "成功").data(0);
        } else {
            cu.set(true, "state", StateEnum.ENABLE);
            resultBody.setMsg(StateEnum.ENABLE.getName() + "成功").data(1);
        }
        update(cu);
        initFileType();
        return resultBody;

    }
}
