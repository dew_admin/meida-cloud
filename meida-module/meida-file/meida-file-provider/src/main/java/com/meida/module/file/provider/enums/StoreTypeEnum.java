package com.meida.module.file.provider.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.NoArgsConstructor;

/**
 * 文件存储方式
 *
 * @author gp
 * @date 2021/8/21
 */
@NoArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ApiModel("文件类型")
public enum StoreTypeEnum {
    /**
     * 存储方式
     */
    LOCAL_OSS(0, "本地"),
    ALI_OSS(1, "阿里云"),
    AWS_OSS(2, "亚马逊"),
    MIO_OSS(3, "MINIO");

    @EnumValue
    private Integer value;

    private String label;

    StoreTypeEnum(Integer value, String label) {
        this.value = value;
        this.label = label;
    }


    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public static StoreTypeEnum getByValue(Integer value) {
        for (StoreTypeEnum fileGroupEnum : values()) {
            if (fileGroupEnum.getValue() == value) {
                return fileGroupEnum;
            }
        }
        return null;
    }

    public static StoreTypeEnum getByName(String name) {
        for (StoreTypeEnum storeTypeEnum : values()) {
            if (storeTypeEnum.name().equals(name)) {
                return storeTypeEnum;
            }
        }
        return null;
    }
}
