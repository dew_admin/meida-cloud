package com.meida.module.file.provider.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.NoArgsConstructor;

/**
 * 文件类型
 *
 * @author gp
 * @date 2021/8/21
 */
@NoArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@ApiModel("文件类型")
public enum FileGroupEnum {
    /**
     * 文件类型
     */
    Image(1, "图片"),
    Video(2, "视频"),
    Other(3, "文件");

    @EnumValue
    private Integer value;

    private String label;

    FileGroupEnum(Integer value, String label) {
        this.value = value;
        this.label = label;
    }


    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }


    public static FileGroupEnum getByValue(Integer value) {
        for (FileGroupEnum fileGroupEnum : values()) {
            if (fileGroupEnum.getValue() == value) {
                return fileGroupEnum;
            }
        }
        return null;
    }
}
