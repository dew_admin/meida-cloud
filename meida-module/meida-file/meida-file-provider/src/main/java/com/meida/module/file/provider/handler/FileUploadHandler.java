package com.meida.module.file.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.file.client.entity.SysFile;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.parser.Entity;

/**
 * 文件上传扩展
 *
 * @author zyf
 */
public interface FileUploadHandler {

    /**
     * 上传前检测
     *
     * @param request
     * @param busId
     * @param params 上传参数
     * @return
     */
    default Boolean validation(HttpServletRequest request, Long busId, EntityMap params) {
        return true;
    }

    /**
     * 自定义文件存储
     *
     * @param sysFile 默认保存文件对象
     * @return
     */
    default Boolean customSaveFile(HttpServletRequest request, SysFile sysFile) {
        return false;
    }


    /**
     * 开启文档转换
     *
     * @return
     */
    default Boolean openConvert() {
        return true;
    }

    /**
     * 转换前置事件
     *
     * @param f
     * @return
     */
    default Boolean convertBefore(SysFile f) {
        return true;
    }

    /**
     * ocr识别前置事件
     *
     * @param f
     * @return
     */
    default Boolean ocrBefore(SysFile f) {
        return true;
    }

    /**
     * 转换成功事件
     *
     * @param fileId
     * @param pdfPath
     */
    default void convertSuccess(Long fileId, String pdfPath) {

    }
}
