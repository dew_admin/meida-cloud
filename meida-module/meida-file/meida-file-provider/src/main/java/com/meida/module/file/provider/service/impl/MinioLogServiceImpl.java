package com.meida.module.file.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.module.file.client.entity.MinioLog;
import com.meida.module.file.provider.mapper.MinioLogMapper;
import com.meida.module.file.provider.service.MinioLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * minioLog 服务实现类
 *
 * @author flyme
 * @date 2019-06-03
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class MinioLogServiceImpl extends BaseServiceImpl<MinioLogMapper, MinioLog> implements MinioLogService {
    @Override
    public ResultBody beforePageList(CriteriaQuery<MinioLog> cq, MinioLog minioLog, EntityMap requestMap) {
        if (FlymeUtils.isNotEmpty(minioLog)) {
            deleteByPath();
            String userName = minioLog.getUserName();
            cq.and(e->e.eq(true,"userName",userName).or().like(true,"value","\""+userName+"\"")).notLike(true,"key_name","download/");
            cq.orderByDesc("status");
        }
        return ResultBody.ok();
    }

    @Override
    public List<MinioLog> selectByUserName(String userName, Integer status) {
        CriteriaQuery<MinioLog> cq = new CriteriaQuery(MinioLog.class);
        cq.and(e->e.eq(true,"userName",userName).or().like(true,"value","\""+userName+"\""));
        cq.eq("status", status);
        return list(cq);
    }

    @Override
    public Boolean deleteByUserName(String userName, Integer status) {
        CriteriaDelete<MinioLog> cd = new CriteriaDelete();
        cd.and(e->e.eq(true,"userName",userName).or().like(true,"value","\""+userName+"\""));
        cd.eq("status", status);
        return remove(cd);
    }

    public Boolean deleteByPath() {
        CriteriaDelete<MinioLog> cd = new CriteriaDelete();
        cd.and(e->e.like(true,"key_name","download/"));
        return remove(cd);
    }

    @Override
    public boolean updateStatus(Long minioLogId) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set("status", CommonConstants.ENABLED);
        cu.eq("id", minioLogId);
        return update(cu);
    }
}
