package com.meida.module.file.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.file.client.entity.MinioLog;


/**
 * minioLog Mapper 接口
 *
 * @author flyme
 * @date 2019-06-03
 */
public interface MinioLogMapper extends SuperMapper<MinioLog> {

}
