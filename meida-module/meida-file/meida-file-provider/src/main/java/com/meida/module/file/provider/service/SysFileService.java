package com.meida.module.file.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.service.CommonFileService;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.provider.handler.FileUploadHandler;
import com.rabbitmq.client.Channel;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 文件表 服务类
 *
 * @author flyme
 * @date 2019-06-03
 */
public interface SysFileService extends IBaseService<SysFile>, CommonFileService {
    /**
     * 获取文件
     *
     * @param busId
     * @param busType
     * @return
     */
    ResultBody getFileList(Long busId, String busType);


    /**
     * 查询删除文件列表
     *
     * @param fileIds
     * @param busId
     * @param busType
     * @return
     */
    List<EntityMap> selectDeleteFileList(Long[] fileIds, Long busId, String busType);

    /**
     * 为files设置busId和busType
     *
     * @param busId
     * @param busType
     * @param fileIds
     * @return
     */
    boolean setBusIdAndBusType(Long busId, String busType, String fileIds);


    /**
     * 查询文件
     *
     * @param busId
     * @return
     */
    List<SysFile> selectByBusId(Long busId);


    /**
     * 删除文件
     *
     * @param busId
     * @return
     */
    boolean deleteByBusId(Long busId);


    /**
     * 上传文件
     *
     * @param file
     * @param busId
     * @param busType
     * @return
     */
    EntityMap upload(HttpServletRequest request, MultipartFile file, Long busId, String busType, String handlerName);


    /**
     * 批量上传
     *
     * @param file
     * @param busId
     * @param busType
     * @return
     */
    EntityMap upload(HttpServletRequest request, MultipartFile[] file, Long busId, String busType, String handlerName);

    /**
     * 批量上传
     *
     * @param files
     * @param busId
     * @param uid
     * @param busType
     * @param handlerName
     * @return
     */
    EntityMap upload(HttpServletRequest request, MultipartFile[] files, Long[] uid, Long busId, String busType, String handlerName);

    /**
     * 通过队列上传
     */
    void uploadByMq(EntityMap map, Channel channel);

    /**
     * 文档转换
     *
     * @param sysFile
     * @param fileUploadHandler
     */
    void converFile(SysFile sysFile, FileUploadHandler fileUploadHandler);

    /**
     * 检测是否允许转换PDF
     *
     * @param fileType
     * @return
     */
    Boolean checkAllowConvert(String fileType);

    /**
     * 转换类型
     *
     * @param fileType
     * @return
     */
    Integer allowConvert(String fileType);

}
