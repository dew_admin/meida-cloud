/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : localhost:3300
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 07/08/2021 20:17:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mkg_coupon
-- ----------------------------
DROP TABLE IF EXISTS `mkg_coupon`;
CREATE TABLE `mkg_coupon`  (
  `couponId` bigint(20) NOT NULL COMMENT '主键',
  `couponTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖券说明',
  `expiryBeginTime` date NULL DEFAULT NULL COMMENT '有效开始日期',
  `expiryEndTime` date NULL DEFAULT NULL COMMENT '有效截止日期',
  `couponType` int(11) NULL DEFAULT NULL COMMENT '优惠形式 1折扣 2满减',
  `withAmount` decimal(11, 0) NULL DEFAULT NULL COMMENT '使用门槛 -1不限制 ,大于0 满n元可用',
  `couponAmount` decimal(20, 0) NULL DEFAULT NULL COMMENT '优惠券面值',
  `useConfig` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '可使用范围配置',
  `categoryId3` bigint(20) NULL DEFAULT NULL COMMENT '商品三级分类',
  `categoryId2` bigint(20) NULL DEFAULT NULL COMMENT '商品二级分类',
  `categoryId1` bigint(20) NULL DEFAULT NULL COMMENT '商品一级分类',
  `productId` bigint(20) NULL DEFAULT NULL COMMENT '商品ID 0=通用 1=拍照提问 2=预约辅导 3=学业无忧',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '所属店铺',
  `receiveNum` int(11) NULL DEFAULT NULL COMMENT '每人限领张数',
  `provideNum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发放数量',
  `surplusNum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '剩余数量',
  `state` int(5) NULL DEFAULT NULL COMMENT '状态 0禁用 1启用',
  `couponDesc` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优惠券说明',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`couponId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mkg_coupon
-- ----------------------------
INSERT INTO `mkg_coupon` VALUES (1215205813869498370, '满50元减10元', '2020-01-09', '2020-01-11', 2, 50, 10, NULL, NULL, NULL, NULL, NULL, 1, 1, '5', '4', 1, NULL, '2020-01-09 17:36:58', '2020-01-10 20:36:18');
INSERT INTO `mkg_coupon` VALUES (1215205932157259777, '满100元减30元', '2020-01-10', '2020-01-19', 2, 100, 30, NULL, NULL, NULL, NULL, NULL, 1, 1, '5', '4', 1, NULL, '2020-01-09 17:37:26', '2020-02-12 20:59:13');
INSERT INTO `mkg_coupon` VALUES (1215206063640301570, '满200元减50元', '2020-01-09', '2020-01-24', 2, 200, 50, NULL, NULL, NULL, NULL, NULL, 1, 1, '10', '9', 1, NULL, '2020-01-09 17:37:57', '2020-02-12 20:59:14');
INSERT INTO `mkg_coupon` VALUES (1215206223640416257, '满1000元减200元', '2020-01-10', '2020-01-31', 2, 1000, 200, NULL, NULL, NULL, NULL, NULL, 1, 1, '11', '11', 1, NULL, '2020-01-09 17:38:35', '2020-02-12 20:59:16');
INSERT INTO `mkg_coupon` VALUES (1215206444046897154, '满50元减20元', '2020-01-09', '2020-01-31', 2, 50, 20, NULL, NULL, NULL, NULL, NULL, 1213348479121182722, 1, '5', '5', 1, NULL, '2020-01-09 17:39:28', '2020-02-12 20:59:17');
INSERT INTO `mkg_coupon` VALUES (1215206574837878786, '满100元减30元', '2020-01-10', '2020-01-31', 2, 100, 30, NULL, NULL, NULL, NULL, NULL, 1213348479121182722, 1, '0', '0', 1, NULL, '2020-01-09 17:39:59', '2020-02-12 20:59:18');
INSERT INTO `mkg_coupon` VALUES (1217286793409200130, '满300减50', '2020-01-15', '2020-01-18', 2, 300, 50, NULL, NULL, NULL, NULL, NULL, 1, 1, '80', '79', 1, NULL, '2020-01-15 11:26:02', '2020-02-12 20:59:19');

SET FOREIGN_KEY_CHECKS = 1;
