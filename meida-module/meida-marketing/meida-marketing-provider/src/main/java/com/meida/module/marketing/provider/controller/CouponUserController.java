package com.meida.module.marketing.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.marketing.client.entity.MkgCouponUser;
import com.meida.module.marketing.provider.service.CouponUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 用户优惠券控制器
 *
 * @author flyme
 * @date 2019-10-22
 */
@RestController
@RequestMapping("/couponuser/")
@Api(tags = "营销模块-用户优惠券")
public class CouponUserController extends BaseController<CouponUserService, MkgCouponUser> {

    @ApiOperation(value = "用户优惠券-分页列表", notes = "用户优惠券分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "用户优惠券-列表", notes = "用户优惠券列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "用户优惠券-添加", notes = "用户优惠券添加")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "用户优惠券-更新", notes = "用户优惠券更新")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "用户优惠券-删除", notes = "用户优惠券删除")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "用户优惠券-删除", notes = "用户优惠券删除")
    @PostMapping(value = "deleteByState")
    public ResultBody deleteByState(Long couponUserId, Integer useState) {
        Boolean n = bizService.deleteByState(couponUserId, useState);
        return ResultBody.result("删除", n);
    }


    @ApiOperation(value = "用户优惠券-详情", notes = "用户优惠券详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
