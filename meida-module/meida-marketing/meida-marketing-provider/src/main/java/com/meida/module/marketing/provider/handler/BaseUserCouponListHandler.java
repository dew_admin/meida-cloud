package com.meida.module.marketing.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.DateUtils;
import com.meida.module.marketing.client.entity.MkgCoupon;
import com.meida.module.marketing.client.entity.MkgCouponUser;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * 用户优惠券列表
 *
 * @author zyf
 */
@Component
public class BaseUserCouponListHandler implements PageInterceptor<MkgCouponUser> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery<MkgCouponUser> cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        Integer useState = params.getInt("useState", 0);
        Integer productId = params.getInt("productId", null);
        BigDecimal amount = params.getBigDecimal("amount");
        cq.clear();
        cq.select(MkgCoupon.class, "couponTitle", "couponType", "withAmount", "couponAmount", "limitAmount", "productId");
        cq.select(MkgCouponUser.class, "couponUserId", "useState", "userExpireBeginDate", "userExpireEndDate", "useTime");
        cq.eq(MkgCouponUser.class, "userId", userId);
        //未删除
        cq.eq(MkgCouponUser.class,"isDelete", 0);
        String nowDate = DateUtils.formatDate();
        //可用
        if (useState.equals(0)) {
            cq.eq("useState", 0);
            cq.le("userExpireBeginDate", nowDate);
            cq.ge("userExpireEndDate", nowDate);
            if (FlymeUtils.isNotEmpty(productId)) {
                cq.in("productId", productId,0);
            }
            if (FlymeUtils.isNotEmpty(amount) && FlymeUtils.gtzero(amount)) {
                //订单金额符合优惠券使用条件
                cq.le(MkgCoupon.class, "withAmount", amount);
            }
        }
        //不可用(未过期不能用)
        if (useState.equals(3)) {
            //前提是未使用未过期
            cq.eq("useState", 0);
            cq.ge("userExpireEndDate", nowDate);
            //不可用的情况(时间未到,不符合产品类型，不符合使用金额)
            cq.and(e->e.gt("userExpireBeginDate",nowDate).or().notIn(FlymeUtils.isNotEmpty(productId),"productId",productId,0).or().gt(FlymeUtils.gtzero(amount), "withAmount", amount));

        }
        //已使用
        if (useState.equals(1)) {
            cq.eq("useState", 1);
        }
        //已过期
        if (useState.equals(2)) {
            cq.eq("useState", 0);
            //当前时间大于过期时间
            cq.lt("userExpireEndDate", nowDate);
        }
    }

    @Override
    public void complete(CriteriaQuery<MkgCouponUser> cq, List<EntityMap> result, EntityMap extra) {

    }

}
