package com.meida.module.marketing.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.marketing.client.entity.MkgCoupon;
import com.meida.module.marketing.provider.service.CouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 优惠券控制器
 *
 * @author flyme
 * @date 2019-10-22
 */
@RestController
@RequestMapping("/coupon/")
@Api(tags = "营销模块-优惠券管理")
public class CouponController extends BaseController<CouponService, MkgCoupon> {

    @ApiOperation(value = "优惠券-分页列表", notes = "分页列表")
    @GetMapping(value = "page")
    @ApiIgnore
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "优惠券-列表", notes = "优惠券列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "优惠券-添加", notes = "优惠券添加")
    @PostMapping(value = "save")
    @ApiIgnore
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "优惠券-更新", notes = "优惠券更新")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "优惠券-删除", notes = "优惠券删除")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "优惠券-详情", notes = "优惠券详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "更新优惠券状态", notes = "更新优惠券状态")
    @PostMapping(value = "setCouponState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
