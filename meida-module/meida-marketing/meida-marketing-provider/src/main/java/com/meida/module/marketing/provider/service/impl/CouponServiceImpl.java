package com.meida.module.marketing.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.marketing.client.entity.MkgCoupon;
import com.meida.module.marketing.provider.mapper.CouponMapper;
import com.meida.module.marketing.provider.service.CouponService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 优惠券Service实现类
 *
 * @author flyme
 * @date 2019-10-22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CouponServiceImpl extends BaseServiceImpl<CouponMapper, MkgCoupon> implements CouponService {


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<MkgCoupon> cq, MkgCoupon mkgCoupon, EntityMap requestMap) {
        if (FlymeUtils.isNotEmpty(mkgCoupon)) {
            cq.like("couponTitle", mkgCoupon.getCouponTitle());
            cq.eq("state", mkgCoupon.getState());
            cq.ge(MkgCoupon.class, "couponAmount", requestMap.get("couponAmountMin"));
            cq.le(MkgCoupon.class, "couponAmount", requestMap.get("couponAmountMax"));
            cq.ge("expiryBeginTime", mkgCoupon.getExpiryBeginTime());
            cq.le("expiryEndTime", mkgCoupon.getExpiryEndTime());
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, MkgCoupon mkgCoupon, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        mkgCoupon.setShopId(companyId);
        mkgCoupon.setSurplusNum(mkgCoupon.getProvideNum());
        return ResultBody.ok();
    }

    @Override
    public Boolean updateSurplusNum(Long couponId, Integer surplusNum) {
        MkgCoupon mkgCoupon = getById(couponId);
        mkgCoupon.setSurplusNum(mkgCoupon.getSurplusNum() - surplusNum);
        return updateById(mkgCoupon);
    }
}
