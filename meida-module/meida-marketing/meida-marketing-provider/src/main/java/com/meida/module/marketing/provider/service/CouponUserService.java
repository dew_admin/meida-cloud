package com.meida.module.marketing.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.marketing.client.entity.MkgCoupon;
import com.meida.module.marketing.client.entity.MkgCouponUser;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 接口
 *
 * @author flyme
 * @date 2019-10-22
 */
public interface CouponUserService extends IBaseService<MkgCouponUser> {

    /**
     * 查询优惠券
     *
     * @param couponId
     * @return
     */
    EntityMap getCoupon(Long couponId);

    /**
     * 查询优惠券
     *
     * @param couponUserId
     * @return
     */
    EntityMap getCouponById(Long couponUserId, CriteriaQueryCallBack cqc);

    /**
     * 查询购物车中优惠券列表
     *
     * @param userId
     * @param list
     * @return
     */
    EntityMap listByShopCard(Long userId, List<EntityMap> list);

    /**
     * 查询商品直接购买结算时优惠券列表
     *
     * @param userId
     * @param shopId
     * @param productAmount
     * @return
     */
    EntityMap listByShopIdAndAmount(Long shopId, BigDecimal productAmount, Long userId);

    /**
     * 统计个人未使用优惠券数量
     *
     * @param userId
     * @return
     */
    long countByUserId(Long userId);

    /**
     * 查询个人优惠券
     *
     * @param userId
     * @param useState
     * @return
     */
    List<EntityMap> listByUserId(Long userId, Integer useState);


    /**
     * 计算优惠金额
     */
    EntityMap calculateCouponAmount(String couponUserIds);

    /**
     * 计算优惠金额
     */
    EntityMap calculateCouponAmount(String couponUserIds, Long shopId);

    ResultBody CouponcouPageList(Map params);

    /**
     * 查询优惠券领取数量
     *
     * @param userId
     * @param couponId
     */
    Long countByCouponAndUserId(Long userId, Long couponId);

    /**
     * 查询所选店铺店铺优惠券
     *
     * @param couponUserIds
     * @param shopId
     * @return
     */
    List<Long> selectCouponUserIds(String[] couponUserIds, Long shopId);

    /**
     * 修改优惠券使用状态
     *
     * @param couponUserIds
     * @param useState
     * @return
     */
    Boolean updateCouponState(String couponUserIds, Integer useState);

    /**
     * 修改优惠券使用状态
     *
     * @param orderId
     * @param couponUserIds
     * @param useState
     * @return
     */
    Boolean updateCouponState(Long orderId, String couponUserIds, Integer useState);

    /**
     * 统计可用的优惠券数量
     *
     * @param userId
     * @param productId
     * @param amount
     * @return
     */
    Integer countEnabledCoupon(Long userId, Integer productId, BigDecimal amount);

    /**
     * 统计不可用优惠券数量
     *
     * @param userId
     * @param productId
     * @param amount
     * @param callBack
     * @return
     */
    Integer countDisabledCoupon(Long userId, Integer productId, BigDecimal amount, CriteriaQueryCallBack callBack);

    /**
     * 查询不可用优惠券数量
     *
     * @param userId
     * @param productId
     * @param amount    订单金额
     * @return
     */
    Integer countDisabledCoupon(Long userId, Integer productId, BigDecimal amount);

    /**
     * 查询过期优惠券数量
     *
     * @param userId
     * @return
     */
    Integer countExpireCoupon(Long userId);

    /**
     * 计算折扣金额
     *
     * @param couponUserId 用户优惠券Id
     * @param orderAmount  订单金额
     * @return
     */
    BigDecimal calculateDiscountAmountByCouponUserId(Long couponUserId, BigDecimal orderAmount);

    /**
     * 删除优惠券
     *
     * @param couponUserId
     * @param useState
     * @return
     */
    Boolean deleteByState(Long couponUserId, Integer useState);
}
