package com.meida.module.marketing.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.marketing.client.entity.MkgCoupon;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 后台优惠券列表
 *
 * @author zyf
 */
@Component("backstageCouponListHandler")
public class BackstageCouponListHandler implements PageInterceptor<MkgCoupon> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(MkgCoupon.class, "shopId", companyId);
    }

    @Override
    public void complete(CriteriaQuery<MkgCoupon> cq, List<EntityMap> result, EntityMap extra) {

    }

}
