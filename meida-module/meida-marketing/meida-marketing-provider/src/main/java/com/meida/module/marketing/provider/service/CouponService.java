package com.meida.module.marketing.provider.service;


import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.marketing.client.entity.MkgCoupon;

/**
 * 接口
 *
 * @author flyme
 * @date 2019-10-22
 */
public interface CouponService extends IBaseService<MkgCoupon> {

    /**
     * 更新优惠券数量
     *
     * @param couponId
     * @param surplusNum
     * @return
     */
    Boolean updateSurplusNum(Long couponId, Integer surplusNum);





}
