package com.meida.module.marketing.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.marketing.client.entity.MkgCouponUser;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class MkgCouponUserInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return MkgCouponUser.class;
    }
}
