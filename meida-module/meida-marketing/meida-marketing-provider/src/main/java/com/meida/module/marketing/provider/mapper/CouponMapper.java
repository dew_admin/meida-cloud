package com.meida.module.marketing.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.marketing.client.entity.MkgCoupon;


/**
 *  Mapper 接口
 * @author flyme
 * @date 2019-10-22
 */
public interface CouponMapper extends SuperMapper<MkgCoupon> {

}
