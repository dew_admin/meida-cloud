package com.meida.module.marketing.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.marketing.client.entity.MkgCoupon;
import com.meida.module.marketing.client.entity.MkgCouponUser;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户优惠券列表(含有产品分类逻辑)
 *
 * @author zyf
 */
@Component
public class UserCouponListByProductHandler extends BaseUserCouponListHandler {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery<MkgCouponUser> cq, PageParams pageParams, EntityMap params) {
        super.prepare(cq, pageParams, params);
    }

    @Override
    public void complete(CriteriaQuery<MkgCouponUser> cq, List<EntityMap> result, EntityMap extra) {

    }

}
