package com.meida.module.marketing.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券
 *
 * @author flyme
 * @date 2019-10-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mkg_coupon")
@TableAlias("coupon")
@ApiModel(value = "优惠券", description = "优惠券")

public class MkgCoupon extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "couponId", type = IdType.ASSIGN_ID)
    private Long couponId;

    @ApiModelProperty(value = "优惠券标题")
    private String couponTitle;

    @ApiModelProperty(value = "优惠券面值")
    private BigDecimal couponAmount;

    @ApiModelProperty(value = "有效开始时间")
    private Date expiryBeginTime;

    @ApiModelProperty(value = "有效截止日期")
    private Date expiryEndTime;

    @ApiModelProperty(value = "使用门槛 -1不限制 ,大于0 满n元可用")
    private BigDecimal withAmount;

    @ApiModelProperty(value = "限制优惠金额(0不限制,大于0限制)")
    private BigDecimal limitAmount;

    @ApiModelProperty(value = "每人限领张数")
    private Integer receiveNum;

    @ApiModelProperty(value = "可使用范围配置,json存储")
    private String useConfig;

    @ApiModelProperty(value = "可使用商品")
    private Long productId;

    @ApiModelProperty(value = "可使用商品一级分类")
    private Long categoryId1;

    @ApiModelProperty(value = "可使用商品二级分类")
    private Long categoryId2;

    @ApiModelProperty(value = "可使用商品三级分类")
    private Long categoryId3;


    @ApiModelProperty(value = "所属店铺")
    private Long shopId;


    @ApiModelProperty(value = "状态 0禁用 1启用")
    private Integer state;

    @ApiModelProperty(value = "优惠形式 1折扣 2满减")
    private Integer couponType;

    @ApiModelProperty(value = "发放数量")
    private Integer provideNum;

    @ApiModelProperty(value = "剩余数量")
    private Integer surplusNum;

    @ApiModelProperty(value = "有效天数")
    private Integer validDays;

    @ApiModelProperty(value = "使用说明")
    private String couponDesc;


}
