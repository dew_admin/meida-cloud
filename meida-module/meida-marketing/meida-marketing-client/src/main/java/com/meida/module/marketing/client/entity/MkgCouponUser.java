package com.meida.module.marketing.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author flyme
 * @date 2019-10-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mkg_coupon_user")
@TableAlias("couponuser")
@ApiModel(value = "BusVoucherdetail对象", description = "")
public class MkgCouponUser extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "couponUserId", type = IdType.ASSIGN_ID)
    private Long couponUserId;

    @ApiModelProperty(value = "优惠券id")
    private Long couponId;

    @ApiModelProperty(value = "优惠券领取人id")
    private Long userId;

    @ApiModelProperty(value = "店铺ID")
    private Long shopId;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "用户优惠券有效开始日期")
    private Date userExpireBeginDate;

    @ApiModelProperty(value = "用户优惠券有效截止日期")
    private Date userExpireEndDate;

    @ApiModelProperty(value = "0 未使用 1已使用,2已过期")
    private Integer useState;

    @ApiModelProperty(value = "是否可以删除")
    private Integer isDelete;

    @ApiModelProperty(value = "使用时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String useTime;

}
