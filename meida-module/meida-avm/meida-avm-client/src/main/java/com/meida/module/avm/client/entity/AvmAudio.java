package com.meida.module.avm.client.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 音频
 *
 * @author flyme
 * @date 2020-02-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("avm_audio")
@TableAlias("audio")
@ApiModel(value="AvmAudio对象", description="音频")
public class AvmAudio extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "audioId", type = IdType.ASSIGN_ID)
    private Long audioId;

    @ApiModelProperty(value = "音频标题")
    private String audioTitle;

    @ApiModelProperty(value = "音频本地地址")
    private String audioUrl;

    @ApiModelProperty(value = "OSS地址")
    private String ossUrl;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "评论数量")
    private Integer commentNum;

    @ApiModelProperty(value = "音频状态")
    private Integer audioState;

    @ApiModelProperty(value = "音频类型")
    private String audioType;

    @ApiModelProperty(value = "音频格式")
    private String audioFormat;

    @ApiModelProperty(value = "音频大小")
    private BigDecimal audioSize;

    @ApiModelProperty(value = "发布人")
    private Long userId;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

    @ApiModelProperty(value = "总时长")
    private Integer duration;

    @ApiModelProperty(value = "艺术家")
    private String artist;

}
