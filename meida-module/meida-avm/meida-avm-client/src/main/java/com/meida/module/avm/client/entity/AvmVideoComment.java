package com.meida.module.avm.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 视频回复
 *
 * @author flyme
 * @date 2019-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("avm_video_comment")
@TableAlias("avc")
@ApiModel(value="AvmVideoComment对象", description="视频回复")
public class AvmVideoComment extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "commentId", type = IdType.ASSIGN_ID)
    private Long commentId;

    @ApiModelProperty(value = "视频Id")
    private Long videoId;

    @ApiModelProperty(value = "评论人")
    private Long replyUserId;

    @ApiModelProperty(value = "被评论人")
    private Long replyToUserId;

    @ApiModelProperty(value = "评论图片")
    private String replyImages;

    @ApiModelProperty(value = "评论内容")
    private String replyContent;

    @ApiModelProperty(value = "评论状态")
    private Integer replyState;

}
