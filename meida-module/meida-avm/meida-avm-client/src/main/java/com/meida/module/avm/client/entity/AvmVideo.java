package com.meida.module.avm.client.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 小视频
 *
 * @author flyme
 * @date 2019-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("avm_video")
@TableAlias("video")
@ApiModel(value="AvmVideo对象", description="小视频")
public class AvmVideo extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "videoId", type = IdType.ASSIGN_ID)
    private Long videoId;

    @ApiModelProperty(value = "视频标题")
    private String videoTitle;

    @ApiModelProperty(value = "视频本地地址")
    private String videoUrl;

    @ApiModelProperty(value = "OSS地址")
    private String ossUrl;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "视频类型")
    private String videoType;

    @ApiModelProperty(value = "视频状态")
    private Integer videoState;

    @ApiModelProperty(value = "视频格式")
    private String videoFormat;

    @ApiModelProperty(value = "视频大小")
    private BigDecimal videoSize;

    @ApiModelProperty(value = "发布人")
    private Long userId;

    @ApiModelProperty(value = "评论数量")
    private Integer commentNum;

    @ApiModelProperty(value = "发布地址")
    private String sendAddress;


    @ApiModelProperty(value = "视频标签")
    private String videoTag;


}
