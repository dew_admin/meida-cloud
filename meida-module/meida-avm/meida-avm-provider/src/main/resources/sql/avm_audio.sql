/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 14:47:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for avm_audio
-- ----------------------------
DROP TABLE IF EXISTS `avm_audio`;
CREATE TABLE `avm_audio`  (
  `audioId` bigint(20) NOT NULL COMMENT '主键',
  `audioTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频标题',
  `audioUrl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频本地地址',
  `ossUrl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'OSS地址',
  `coverImage` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `commentNum` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `audioState` int(11) NULL DEFAULT NULL COMMENT '音频状态',
  `audioType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频类型',
  `audioFormat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '音频格式',
  `audioSize` decimal(10, 2) NULL DEFAULT NULL COMMENT '音频大小',
  `duration` int(11) NULL DEFAULT NULL COMMENT '总时长',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `artist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '艺术家',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '发布人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`audioId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '音频' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (409, 400, 'audio', '音频管理', '', '/', 'content/avm/audio/index', 'customer-service', '_self', 'PageView', 1, 5, 1, 0, 'meida-base-provider', '2020-02-12 19:34:56', NULL);
INSERT INTO `base_authority` VALUES (409, 'MENU_audio', 409, NULL, NULL, 1, '2020-02-12 19:34:57', NULL);


SET FOREIGN_KEY_CHECKS = 1;
