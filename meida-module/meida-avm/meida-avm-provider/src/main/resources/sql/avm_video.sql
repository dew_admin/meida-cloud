/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 14:47:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for avm_video
-- ----------------------------
DROP TABLE IF EXISTS `avm_video`;
CREATE TABLE `avm_video`  (
  `videoId` bigint(20) NOT NULL COMMENT '主键',
  `videoTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频标题',
  `videoUrl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频本地地址',
  `ossUrl` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'OSS地址',
  `coverImage` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `commentNum` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `videoState` int(11) NULL DEFAULT NULL COMMENT '视频状态',
  `videoTag` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频TAG',
  `videoType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频类型',
  `videoFormat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频格式',
  `videoSize` decimal(10, 2) NULL DEFAULT NULL COMMENT '视频大小',
  `sendAddress` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发布地址',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '发布人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`videoId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '小视频' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (408, 400, 'avmVideo', '小视频管理', '小视频管理', '/', 'content/avm/video/index', 'video-camera', '_self', 'PageView', 1, 4, 1, 0, 'meida-base-provider', '2019-12-29 09:59:00', '2020-01-04 17:27:44');
INSERT INTO `base_authority` VALUES (408, 'MENU_avmVideo', 408, NULL, NULL, 1, '2019-12-29 09:59:00', '2020-01-04 17:27:44');


SET FOREIGN_KEY_CHECKS = 1;
