package com.meida.module.avm.provider.service;

import com.meida.module.avm.client.entity.AvmVideo;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 小视频 接口
 *
 * @author flyme
 * @date 2019-12-07
 */
public interface AvmVideoService extends IBaseService<AvmVideo> {
    /**
     * 更新评论数量
     *
     * @param contentId
     * @param num
     * @return
     */
    Boolean updateCommentNum(Long contentId, Integer num);
}
