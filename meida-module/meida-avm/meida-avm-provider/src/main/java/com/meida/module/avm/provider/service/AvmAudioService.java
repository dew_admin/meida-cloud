package com.meida.module.avm.provider.service;

import com.meida.module.avm.client.entity.AvmAudio;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 音频 接口
 *
 * @author flyme
 * @date 2020-02-12
 */
public interface AvmAudioService extends IBaseService<AvmAudio> {

}
