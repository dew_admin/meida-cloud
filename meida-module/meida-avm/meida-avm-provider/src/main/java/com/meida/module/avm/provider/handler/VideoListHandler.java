package com.meida.module.avm.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.avm.client.entity.AvmVideo;
import com.meida.module.avm.provider.service.AvmVideoService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 视频列表
 *
 * @author zyf
 */
@Component("videoListHandler")
public class VideoListHandler implements PageInterceptor<AvmVideo> {


    @Resource
    private AvmVideoService avmVideoService;

    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        String operate = params.get("operate");
        cq.addSelect("collecon_tag(2,'AvmVideo',videoId," + userId + ") dzTag");
        cq.addSelect("collecon_tag(1,'AvmVideo',videoId," + userId + ") scTag");
        cq.eq(true, "videoState", CommonConstants.ENABLED);
        if (FlymeUtils.isNotEmpty(operate)) {
            Long currentId = params.getLong("currentId");
            AvmVideo avmVideo = avmVideoService.getById(currentId);
            if (FlymeUtils.isNotEmpty(avmVideo)) {
                Date createTime = avmVideo.getCreateTime();
                if (operate.equals("up")) {
                    cq.le(AvmVideo.class, "createTime", createTime);
                } else {
                    cq.ge(AvmVideo.class, "createTime", createTime);

                }
                cq.ne("video.videoId", currentId);
                //cq.limit(6);
            }
        }

    }


}
