package com.meida.module.avm.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.avm.client.entity.AvmAudio;
import com.meida.module.avm.provider.mapper.AvmAudioMapper;
import com.meida.module.avm.provider.service.AvmAudioService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 音频接口实现类
 *
 * @author flyme
 * @date 2020-02-12
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AvmAudioServiceImpl extends BaseServiceImpl<AvmAudioMapper, AvmAudio> implements AvmAudioService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AvmAudio audio, EntityMap extra) {
        audio.setSortOrder(99);
        audio.setAudioState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AvmAudio> cq, AvmAudio audio, EntityMap requestMap) {
      cq.orderByAsc("audio.sortOrder");
      return ResultBody.ok();
    }
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<EntityMap> afterPageList(CriteriaQuery<AvmAudio> cq, List<EntityMap> datas, ResultBody resultBody) {
        for (EntityMap data : datas) {
            data.put("name",data.get("activityTitle"));
            data.put("title",data.get("activityTitle"));
            data.put("path",data.get("audioUrl"));
        }
        return super.afterPageList(cq,datas,resultBody);
    }
}
