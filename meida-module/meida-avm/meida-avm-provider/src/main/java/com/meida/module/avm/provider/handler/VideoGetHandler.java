package com.meida.module.avm.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.avm.provider.service.AvmVideoCommentService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 视频详情扩展
 *
 * @author zyf
 */
@Component("videoGetHandler")
public class VideoGetHandler implements GetInterceptor {

    @Resource
    private AvmVideoCommentService commentService;

    @Override
    public void prepare(CriteriaQuery cq, EntityMap params) {

    }

    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long videoId = map.getLong("videoId");
        List<EntityMap> commentList = commentService.listByVideoId(videoId, 10);
        map.put("commentList", commentList);
    }
}
