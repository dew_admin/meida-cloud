package com.meida.module.avm.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.avm.client.entity.AvmAudio;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class AvmAudioInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return AvmAudio.class;
    }

}
