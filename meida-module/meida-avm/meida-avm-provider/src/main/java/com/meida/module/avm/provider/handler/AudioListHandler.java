package com.meida.module.avm.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.avm.client.entity.AvmAudio;
import org.springframework.stereotype.Component;

/**
 * 音频列表
 *
 * @author zyf
 */
@Component("audioListHandler")
public class AudioListHandler implements PageInterceptor<AvmAudio> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.eq(AvmAudio.class, "audioState", CommonConstants.ENABLED);
    }


}
