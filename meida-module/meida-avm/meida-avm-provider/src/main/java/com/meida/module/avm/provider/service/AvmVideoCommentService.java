package com.meida.module.avm.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.avm.client.entity.AvmVideoComment;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 视频回复 接口
 *
 * @author flyme
 * @date 2019-12-07
 */
public interface AvmVideoCommentService extends IBaseService<AvmVideoComment> {

    List<EntityMap> listByVideoId(Long videoId, Integer num);
}
