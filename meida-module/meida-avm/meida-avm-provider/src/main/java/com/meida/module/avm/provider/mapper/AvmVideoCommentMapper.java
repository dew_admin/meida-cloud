package com.meida.module.avm.provider.mapper;

import com.meida.module.avm.client.entity.AvmVideoComment;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 视频回复 Mapper 接口
 * @author flyme
 * @date 2019-12-07
 */
public interface AvmVideoCommentMapper extends SuperMapper<AvmVideoComment> {

}
