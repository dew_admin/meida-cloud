package com.meida.module.avm.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.avm.client.entity.AvmVideoComment;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class AvmVideoCommentInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return AvmVideoComment.class;
    }

}
