package com.meida.module.avm.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.module.avm.client.entity.AvmVideo;
import com.meida.module.avm.provider.mapper.AvmVideoMapper;
import com.meida.module.avm.provider.service.AvmVideoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 小视频接口实现类
 *
 * @author flyme
 * @date 2019-12-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AvmVideoServiceImpl extends BaseServiceImpl<AvmVideoMapper, AvmVideo> implements AvmVideoService {


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AvmVideo> cq, AvmVideo video, EntityMap requestMap) {
        cq.select(AvmVideo.class, "videoId", "videoTitle", "ossUrl", "coverImage", "videoUrl", "videoType", "commentNum", "videoTag", "sendAddress", "videoState", "createTime");
        cq.addSelect("collecon_count(2,'AvmVideo',videoId) dzNum");
        cq.likeByField(AvmVideo.class, "videoTitle");
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("userId");
        cq.orderByDesc("video.createTime");
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforeGet(CriteriaQuery<AvmVideo> cq, AvmVideo content, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        cq.select(AvmVideo.class, "*");
        cq.select("user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_tag(1,'AvmVideo',videoId," + userId + ") scTag");
        cq.addSelect("collecon_tag(2,'AvmVideo',videoId," + userId + ") dzTag");
        cq.addSelect("collecon_count(2,'AvmVideo',videoId) dzNum");
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("userId");
        return ResultBody.ok();
    }


    /**
     * 插入前置处理
     * @param cs
     * @param video
     * @param extra
     * @return
     */
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AvmVideo video, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        video.setCommentNum(0);
        video.setUserId(userId);
        video.setVideoState(CommonConstants.ENABLED);
        return ResultBody.msg("发布成功");
    }

    @Override
    public Boolean updateCommentNum(Long videoId, Integer num) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.setSql(true, "commentNum=commentNum+" + num);
        cu.ge("commentNum", 0);
        cu.eq(true, "videoId", videoId);
        return update(cu);
    }
}
