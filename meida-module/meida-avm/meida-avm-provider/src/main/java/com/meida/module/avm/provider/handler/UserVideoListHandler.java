package com.meida.module.avm.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.avm.client.entity.AvmVideo;
import org.springframework.stereotype.Component;

/**
 * 用户视频列表
 *
 * @author zyf
 */
@Component("userVideoListHandler")
public class UserVideoListHandler implements PageInterceptor<AvmVideo> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        cq.eq(AvmVideo.class, "userId", userId);
    }



}
