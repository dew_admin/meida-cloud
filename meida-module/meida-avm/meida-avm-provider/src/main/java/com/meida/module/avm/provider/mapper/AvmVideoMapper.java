package com.meida.module.avm.provider.mapper;

import com.meida.module.avm.client.entity.AvmVideo;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 小视频 Mapper 接口
 * @author flyme
 * @date 2019-12-07
 */
public interface AvmVideoMapper extends SuperMapper<AvmVideo> {

}
