package com.meida.module.avm.provider.mapper;

import com.meida.module.avm.client.entity.AvmAudio;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 音频 Mapper 接口
 * @author flyme
 * @date 2020-02-12
 */
public interface AvmAudioMapper extends SuperMapper<AvmAudio> {

}
