package com.meida.module.avm.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.avm.client.entity.AvmVideo;
import com.meida.module.avm.client.entity.AvmVideoComment;
import com.meida.module.avm.provider.mapper.AvmVideoCommentMapper;
import com.meida.module.avm.provider.service.AvmVideoCommentService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.avm.provider.service.AvmVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 视频回复接口实现类
 *
 * @author flyme
 * @date 2019-12-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AvmVideoCommentServiceImpl extends BaseServiceImpl<AvmVideoCommentMapper, AvmVideoComment> implements AvmVideoCommentService {

    @Autowired
    private AvmVideoService videoService;

    @Override
    public List<EntityMap> listByVideoId(Long videoId, Integer num) {
        Long userId = OpenHelper.getUserId();
        ApiAssert.isNotEmpty("videoId不能为空", videoId);
        CriteriaQuery cq = new CriteriaQuery(AvmVideoComment.class);
        cq.select(AvmVideoComment.class, "commentId", "replyContent", "replyImages", "createTime");
        cq.select("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_count(2,'AvmVideoComment',commentId) dzNum");
        cq.addSelect("collecon_tag(2,'AvmVideoComment',commentId," + userId + ") dzTag");
        cq.eq(AvmVideoComment.class, "videoId", videoId);
        cq.limit(num);
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("replyUserId");
        cq.orderByDesc("avc.createTime");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AvmVideoComment comment, EntityMap extra) {
        AvmVideo video = videoService.getById(comment.getVideoId());
        ApiAssert.isNotEmpty("视频已删除", video);
        Long userId = OpenHelper.getUserId();
        comment.setReplyState(CommonConstants.ENABLED);
        comment.setReplyUserId(userId);
        return ResultBody.ok();
    }


    @Override
    public ResultBody afterAdd(CriteriaSave cs, AvmVideoComment comment, EntityMap extra) {
        videoService.updateCommentNum(comment.getVideoId(), 1);
        return ResultBody.ok("评论成功");
    }

    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        videoService.updateCommentNum(cd.getIdValue(), -1);
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AvmVideoComment> cq, AvmVideoComment videoComment, EntityMap requestMap) {
        Long userId = OpenHelper.getUserId();
        Long videoId = videoComment.getVideoId();
        ApiAssert.isNotEmpty("videoId不能为空", videoId);
        cq.select(AvmVideoComment.class, "commentId", "replyContent", "replyImages", "createTime");
        cq.addSelect("user.userId", "user.nickName", "user.userName", "user.avatar");
        cq.addSelect("collecon_count(2,'AvmVideoComment',commentId) dzNum");
        cq.addSelect("collecon_tag(2,'AvmVideoComment',commentId," + userId + ") dzTag");
        cq.eq(AvmVideoComment.class, "videoId", videoComment.getVideoId());
        cq.createJoin("com.meida.module.user.client.entity.AppUser").setMainField("replyUserId");
        cq.orderByDesc("avc.createTime");
        return ResultBody.ok();
    }

}
