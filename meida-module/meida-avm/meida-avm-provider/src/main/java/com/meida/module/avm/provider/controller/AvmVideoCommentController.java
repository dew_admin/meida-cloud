package com.meida.module.avm.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.avm.client.entity.AvmVideoComment;
import com.meida.module.avm.provider.service.AvmVideoCommentService;


/**
 * 视频回复控制器
 *
 * @author flyme
 * @date 2019-12-07
 */
@RestController
@RequestMapping("/avm/avc/")
@Api(tags = "视频模块-小视频")
public class AvmVideoCommentController extends BaseController<AvmVideoCommentService, AvmVideoComment>  {

    @ApiOperation(value = "视频回复-分页列表", notes = "视频回复分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "视频回复-列表", notes = "视频回复列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "视频回复-添加", notes = "添加视频回复")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }



    @ApiOperation(value = "视频回复-删除", notes = "删除视频回复")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }




    @ApiOperation(value = "视频回复-更新状态值", notes = "视频回复更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
            return bizService.setState(map, "state", state);
    }
}
