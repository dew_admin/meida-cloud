/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 16:27:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for inte_activity
-- ----------------------------
DROP TABLE IF EXISTS `inte_activity`;
CREATE TABLE `inte_activity`  (
  `activityId` bigint(20) NOT NULL COMMENT '主键',
  `typeId` bigint(20) NULL DEFAULT NULL COMMENT '分类ID',
  `activityTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `coverImage` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `activityContent` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `activityMobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动电话',
  `proId` bigint(20) NULL DEFAULT NULL COMMENT '省ID',
  `cityId` bigint(20) NULL DEFAULT NULL COMMENT '城市ID',
  `areaId` bigint(20) NULL DEFAULT NULL COMMENT '区ID',
  `areaName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `beginTime` datetime(0) NULL DEFAULT NULL COMMENT '开始日期',
  `endTime` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  `viewCount` int(255) NULL DEFAULT NULL COMMENT '查看次数',
  `activityTag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `activityAddress` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `lat` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '纬度',
  `lon` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '经度',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '是否推荐',
  `activityState` int(11) NULL DEFAULT NULL COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`activityId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '周边活动' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (410, 400, 'inteactivity', '周边活动', '', '/', 'activity/inteactivity/index', 'car', '_self', 'PageView', 1, 0, 1, 0, 'meida-base-provider', '2019-12-31 16:29:40', '2020-01-04 17:30:57');
INSERT INTO `base_authority` VALUES (410, 'MENU_inteactivity', 410, NULL, NULL, 1, '2019-12-31 16:29:40', '2020-01-04 17:30:57');


SET FOREIGN_KEY_CHECKS = 1;
