package com.meida.module.activity.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.activity.client.entity.InteActivity;

import java.util.List;

/**
 * 周边活动 接口
 *
 * @author flyme
 * @date 2019-10-06
 */
public interface InteActivityService extends IBaseService<InteActivity> {


    /**
     * 查询周边活动
     *
     * @return
     */
    List<EntityMap> listNearby(EntityMap params, int count, boolean collecon);
}
