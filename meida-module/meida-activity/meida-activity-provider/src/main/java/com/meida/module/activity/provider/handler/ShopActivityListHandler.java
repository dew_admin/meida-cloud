package com.meida.module.activity.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.activity.client.entity.InteActivity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 优惠活动列表
 *
 * @author zyf
 */
@Component("shopActivityListHandler")
public class ShopActivityListHandler implements PageInterceptor<InteActivity> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(InteActivity.class, "shopId", companyId);
    }

    @Override
    public void complete(CriteriaQuery<InteActivity> cq, List<EntityMap> result, EntityMap extra) {

    }

}
