package com.meida.module.activity.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.activity.client.api.InteActivityRemoteApi;
import com.meida.module.activity.client.entity.InteActivity;
import com.meida.module.activity.provider.service.InteActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 周边活动控制器
 *
 * @author flyme
 * @date 2019-10-06
 */
@RestController
@RequestMapping("/interact/activity/")
@Api(tags = "周边活动", value = "周边活动")
public class InteActivityController extends BaseController<InteActivityService, InteActivity> implements InteActivityRemoteApi {

    @ApiOperation(value = "周边活动分页列表", notes = "周边活动分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "周边活动-列表", notes = "周边活动列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "周边活动-添加", notes = "添加周边活动")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "周边活动-更新", notes = "更新周边活动")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "周边活动-删除", notes = "删除周边活动")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "周边活动-详情", notes = "周边活动详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "推荐设置", notes = "推荐设置")
    @PostMapping(value = "setRecommend")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "recommend", required = true, value = "推荐状态", paramType = "form")
    })
    public ResultBody setRecommend(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "recommend") Integer recommend) {
        return bizService.setRecommend(ids, recommend);
    }


    @ApiOperation(value = "更新发布状态", notes = "更新发布状态")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "activityState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "activityState") Integer activityState) {
        return bizService.setState(map, "activityState", activityState);
    }

}
