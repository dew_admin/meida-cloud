package com.meida.module.activity.provider.mapper;

import com.meida.module.activity.client.entity.InteActivity;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 周边活动 Mapper 接口
 * @author flyme
 * @date 2019-10-06
 */
public interface InteActivityMapper extends SuperMapper<InteActivity> {

}
