package com.meida.module.activity.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.activity.client.entity.InteActivity;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class InteActivityInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return InteActivity.class;
    }

}
