package com.meida.module.activity.provider.handler;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.activity.client.entity.InteActivity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 周边活动列表
 *
 * @author zyf
 */
@Component("appActivityListHandler")
public class AppActivityListHandler implements PageInterceptor<InteActivity> {


    @Override
    public ResultBody validate(CriteriaQuery cq, EntityMap params) {
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.eq(InteActivity.class, "activityState", CommonConstants.ENABLED);
    }

    @Override
    public void complete(CriteriaQuery<InteActivity> cq, List<EntityMap> result, EntityMap extra) {

    }

}
