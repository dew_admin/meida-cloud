package com.meida.module.activity.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 周边活动
 *
 * @author flyme
 * @date 2019-10-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("inte_activity")
@TableAlias("activity")
@ApiModel(value="InteActivity对象", description="周边活动")
public class InteActivity extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "activityId", type = IdType.ASSIGN_ID)
    private Long activityId;

    @ApiModelProperty(value = "分类ID")
    private Long typeId;

    @ApiModelProperty(value = "标题")
    private String activityTitle;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "内容")
    private String activityContent;

    @ApiModelProperty(value = "省ID")
    private Long proId;

    @ApiModelProperty(value = "城市ID")
    private Long cityId;

    @ApiModelProperty(value = "区ID")
    private Long areaId;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "开始日期")
    private Date beginTime;

    @ApiModelProperty(value = "结束日期")
    private Date endTime;

    @ApiModelProperty(value = "查看次数")
    private Integer viewCount;

    @ApiModelProperty(value = "活动电话")
    private String activityMobile;

    @ApiModelProperty(value = "标签")
    private String activityTag;

    @ApiModelProperty(value = "详细地址")
    private String activityAddress;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "经度")
    private String lon;

    @ApiModelProperty(value = "是否推荐")
    private Integer recommend;

    @ApiModelProperty(value = "状态")
    private Integer activityState;

    @ApiModelProperty(value = "门店ID")
    private Long shopId;

}
