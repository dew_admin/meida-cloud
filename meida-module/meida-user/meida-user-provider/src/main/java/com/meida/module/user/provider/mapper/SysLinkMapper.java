package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysLink;


/**
 * 友情链接 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-11
 */
public interface SysLinkMapper extends SuperMapper<SysLink> {

}
