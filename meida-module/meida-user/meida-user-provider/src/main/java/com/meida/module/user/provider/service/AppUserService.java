package com.meida.module.user.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenUser;
import com.meida.module.user.client.entity.AppUser;

import java.util.List;
import java.util.Map;

/**
 * 系统用户资料管理
 *
 * @author: zyf
 * @date: 2018/10/24 16:38
 * @description:
 */
public interface AppUserService extends IBaseService<AppUser> {


    /**
     * 根据用户ID获取用户信息
     *
     * @param userId
     * @return
     */
    AppUser getUserById(Long userId);


    /**
     * 根据shareCode查询用户
     *
     * @param shareCode
     * @return
     */
    AppUser getUserByShareCode(String shareCode);


    /**
     * 获取当前用户详细信息
     *
     * @param params
     * @return
     */
    ResultBody getMineInfo(Map params);


    /**
     * 根据userIds查询用户
     *
     * @param userIds
     * @return
     */
    List<EntityMap> selectUsers(String userIds);


    /**
     * 登录初始化
     *
     * @param map
     * @return
     */
    ResultBody userInit(Map map);

    /**
     * 依据登录名查询系统用户信息
     *
     * @param username
     * @return
     */
    AppUser getUserByUsername(String username);


    /**
     * 注册手机账户
     *
     * @param accountName
     * @param password
     * @param userType
     * @param shareCode
     */
    void registerByMobile(String accountName, String password, String userType, String shareCode, String areaCode);

    /**
     * 依据手机号查询用户
     *
     * @param mobile
     * @return
     */
    AppUser getUserByMobile(String mobile);

    /**
     * 更新当前用户
     *
     * @param params
     * @return
     */
    ResultBody updateMineInfo(Map params);


    /**
     * 绑定手机号
     *
     * @param mobile
     * @param areaCode
     * @param smsCode
     * @param password
     * @param shareCode
     * @param userType
     * @return
     */
    ResultBody<AppUser> bingMobile(String mobile, String areaCode, String smsCode, String password, String shareCode, String userType);

    /**
     * 换绑手机号
     *
     * @param userId
     * @param mobile
     * @param smsCode
     * @return
     */
    ResultBody<Boolean> changeMobile(Long userId, String mobile, String smsCode);

    /**
     * 设置状态
     *
     * @param userId
     * @return
     */
    ResultBody setStatus(Long userId);


    /**
     * 用户绑定企业
     *
     * @param openUser
     */
    void bindCompany(OpenUser openUser);

    /**
     * 清空用户企业ID
     *
     * @param companyId
     */
    void unBindCompany(Long companyId);

    /**
     * 保存用户
     */
    AppUser save(String userName, String mobile, Long companyId);

    /**
     * 添加用户
     *
     * @return
     */
    ResultBody saveUser(Map model);

    /**
     * 统计被邀请用户
     *
     * @param userId
     * @return
     */
    long countByInviterId(Long userId);

    /**
     * 统计每月会员新增数量
     */
    ResultBody totalUserGroupMonth(Map params);

    /**
     * 生成UserNo
     *
     * @return
     */
    String createUserNo();

    /**
     * 统计某日注册用户数量
     *
     * @param date
     * @return
     */
    long countByDate(String date);

    /**
     * 查询所有设置系统推送通知的用户
     *
     * @return
     */
    List<String> queryAllSetSystemMsgNotice();

}
