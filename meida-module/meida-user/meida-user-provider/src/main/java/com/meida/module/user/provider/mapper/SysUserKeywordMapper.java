package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysUserKeyword;

/**
 * 关键词 Mapper 接口
 * @author flyme
 * @date 2020-05-20
 */
public interface SysUserKeywordMapper extends SuperMapper<SysUserKeyword> {

}
