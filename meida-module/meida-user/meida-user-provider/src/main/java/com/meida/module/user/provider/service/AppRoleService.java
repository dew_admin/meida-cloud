package com.meida.module.user.provider.service;

import com.meida.module.user.client.entity.AppRole;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 用户角色 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface AppRoleService extends IBaseService<AppRole> {
    /**
     * 根据code查询角色
     *
     * @param roleCode
     * @return
     */
    AppRole getByRoleCode(String roleCode);

}
