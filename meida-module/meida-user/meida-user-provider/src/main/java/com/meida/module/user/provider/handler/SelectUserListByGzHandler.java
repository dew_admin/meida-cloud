package com.meida.module.user.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.enums.ColleconEnum;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.system.client.entity.SysCollecon;
import com.meida.module.user.client.entity.AppUser;
import org.springframework.stereotype.Component;

/**
 * 查询用户已关注的用户列表（用户关注其他用户）
 *
 * @author zyf
 */
@Component
public class SelectUserListByGzHandler implements PageInterceptor<AppUser> {
    @Override
    public void prepare(CriteriaQuery<AppUser> cq, PageParams pageParams, EntityMap params) {
        Long otherUserId = params.getLong("userId");
        Long userId = OpenHelper.getUserId();
        cq.clear();
        cq.select(AppUser.class, "userId", "nickName", "avatar", "userType","userNo", "shareCode", "tags", "motto");
        //关注标记
        cq.addSelect("collecon_tag(3,'AppUser',collecon.targetId," + userId + ") gzTag");
        cq.eq(SysCollecon.class, "optType", ColleconEnum.GZ.getCode());
        cq.eq(SysCollecon.class, "targetEntity", "AppUser");
        if (FlymeUtils.isNotEmpty(otherUserId)) {
            cq.eq(SysCollecon.class, "userId", otherUserId);
        } else {
            cq.eq(SysCollecon.class, "userId", userId);
        }
        String keyword = params.get("keyword");
        if (FlymeUtils.isNotEmpty(keyword)) {
            cq.and(e->e.like(true,"tags",keyword).or().like(true,"nickName",keyword));
        }
        cq.orderByDesc("collecon.createTime");
        cq.createJoin(SysCollecon.class).setJoinField("targetId").setMainField("userId");
    }


}
