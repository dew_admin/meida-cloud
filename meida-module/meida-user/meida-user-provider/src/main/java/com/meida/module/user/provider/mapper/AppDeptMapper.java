package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppDept;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 部门 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface AppDeptMapper extends SuperMapper<AppDept> {

}
