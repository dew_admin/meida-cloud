package com.meida.module.user.provider.service;

import com.meida.module.user.client.entity.AppMenu;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 系统资源-菜单信息 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface AppMenuService extends IBaseService<AppMenu> {

}
