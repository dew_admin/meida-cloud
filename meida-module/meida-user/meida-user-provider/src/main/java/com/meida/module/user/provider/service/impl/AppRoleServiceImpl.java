package com.meida.module.user.provider.service.impl;


import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.user.client.entity.AppRole;
import com.meida.module.user.provider.mapper.AppRoleMapper;
import com.meida.module.user.provider.service.AppRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户角色 实现类
 *
 * @author flyme
 * @date 2019-06-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppRoleServiceImpl extends BaseServiceImpl<AppRoleMapper, AppRole> implements AppRoleService {


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AppRole> cq, AppRole appRole, EntityMap requestMap) {
        cq.select(AppRole.class, "roleId", "roleName", "roleCode");
        cq.eq(AppRole.class, "groupName");
        return super.beforePageList(cq, appRole, requestMap);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public AppRole getByRoleCode(String roleCode) {
        CriteriaQuery<AppRole> cq = new CriteriaQuery(AppRole.class);
        cq.eq(true, "roleCode", roleCode);
        return getOne(cq);
    }


}
