package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysUserKeyword;
import com.meida.module.user.provider.service.SysUserKeywordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * 关键词控制器
 *
 * @author flyme
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/suk/")
public class SysUserKeywordController extends BaseController<SysUserKeywordService, SysUserKeyword>  {

    @ApiOperation(value = "关键词-分页列表", notes = "关键词分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "关键词-列表", notes = "关键词列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "关键词-添加", notes = "添加关键词")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "关键词-更新", notes = "更新关键词")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "关键词-删除", notes = "删除关键词")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "关键词-详情", notes = "关键词详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


}
