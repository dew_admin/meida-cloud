package com.meida.module.user.provider.controller;

import com.meida.common.annotation.RlockRepeat;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.lock.LockConstant;
import com.meida.common.mybatis.model.*;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.dto.ThirdBindParams;
import com.meida.module.user.client.entity.AppUser;
import com.meida.module.user.provider.service.AppAccountService;
import com.meida.module.user.provider.service.AppUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * APP用户模块接口
 *
 * @author zyf
 */
@Api(tags = "个人中心")
@RestController
@RequestMapping("/front/user/")
public class AppUserController extends BaseController<AppUserService, AppUser> {


    @Resource
    private AppAccountService appAccountService;


    @ApiOperation(value = "用户-分页列表", notes = "APP用户分页列表 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    /**
     * 获取当前用户详细信息
     *
     * @return
     */
    @ApiOperation(value = "我的-用户中心", notes = "可通过编写自定义handler方法扩展")
    @GetMapping("mineInfo")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "handlerName", value = "扩展handlerName名称", paramType = "form", defaultValue = "userMineInfoHandler")
    })
    public ResultBody getMineInfo(@RequestParam(required = false) Map params) {
        return bizService.getMineInfo(params);
    }


    /**
     * 查询用户昵称头像
     *
     * @return
     */
    @ApiOperation(value = "查询用户昵称头像", notes = "用户ids")
    @GetMapping("selectUsers")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userIds", value = "用户ids", paramType = "form")
    })
    public ResultBody selectUsers(@RequestParam String userIds) {
        List<EntityMap> users = bizService.selectUsers(userIds);
        return ResultBody.ok(users);
    }


    /**
     * 获取其他用户详细信息
     *
     * @return
     */
    @ApiOperation(value = "获取其他用户详情", notes = "可通过编写自定义handler方法扩展")
    @GetMapping("get")
    public ResultBody getAppUserInfo(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    /**
     * 邀请用户列表
     *
     * @return
     */
    @ApiOperation(value = "邀请用户列表", notes = "可通过编写自定义handler方法扩展")
    @GetMapping("list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        ResultBody resultBody = bizService.listEntityMap(params);
        List<EntityMap> list = (List<EntityMap>) resultBody.getData();
        EntityMap extra = resultBody.getExtra();
        EntityMap data = new EntityMap();
        data.put("list", list);
        data.put("extra", extra);
        resultBody.data(data);
        return resultBody;
    }


    /**
     * 綁定第三方账户
     */
    @ApiOperation(value = "第三方账户绑定&解绑", notes = "该接口用于设置中账户管理,绑定和解绑都调用该接口")
    @PostMapping(value = "thirdBind")
    @RlockRepeat(prefix = "register", lockConstant = LockConstant.SUBMIT)
    public ResultBody thirdBind(ThirdBindParams thirdBindParams) {
        Long userId = OpenHelper.getUser().getUserId();
        return appAccountService.thirdBind(userId, thirdBindParams);
    }


    /**
     * 绑定手机号,并调用初始化登录方法
     */
    @ApiOperation(value = "绑定手机号", notes = "绑定后调用初始化登录方法")
    @PostMapping("bindMobile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", required = true, value = "手机号", paramType = "form", defaultValue = "18739941307"),
            @ApiImplicitParam(name = "areaCode", required = false, value = "国际区号", paramType = "form", defaultValue = "+86"),
            @ApiImplicitParam(name = "smsCode", required = true, value = "验证码", paramType = "form"),
            @ApiImplicitParam(name = "shareCode", required = false, value = "邀请码", paramType = "form"),
            @ApiImplicitParam(name = "userType", required = true, value = "用户类型,当手机号未注册时传递", paramType = "form"),
            @ApiImplicitParam(name = "password", value = "密码,当手机号未注册时可设置密码,默认123456", paramType = "form")
    })
    @RlockRepeat(prefix = "register", lockConstant = LockConstant.SUBMIT)
    public ResultBody<AppUser> bindMobile(
            @RequestParam(value = "mobile") String mobile,
            @RequestParam(value = "areaCode", required = false) String areaCode,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "smsCode") String smsCode,
            @RequestParam(value = "shareCode") String shareCode,
            @RequestParam(value = "userType", required = false) String userType
    ) {
        return bizService.bingMobile(mobile,areaCode, smsCode, password, shareCode, userType);
    }

    /**
     * 更新当前用户基本资料
     */
    @ApiOperation(value = "当前用户-更新", notes = "可更新单个字段")
    @PostMapping(value = "updateMine")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "nickName", value = "昵称", paramType = "form"),
            @ApiImplicitParam(name = "avatar", value = "头像", paramType = "form"),
            @ApiImplicitParam(name = "sex", value = "性别", paramType = "form"),
            @ApiImplicitParam(name = "birthday", value = "生日", paramType = "form"),
            @ApiImplicitParam(name = "proId", value = "省ID", paramType = "form"),
            @ApiImplicitParam(name = "cityId", value = "市ID", paramType = "form"),
            @ApiImplicitParam(name = "areaId", value = "区ID", paramType = "form"),
            @ApiImplicitParam(name = "motto", value = "个性签名", paramType = "form")
    })
    public ResultBody updateMine(@RequestParam(required = false) Map params) {
        return bizService.updateMineInfo(params);
    }


    /**
     * 更新用户基本资料
     */
    @ApiOperation(value = "用户-更新", notes = "可更新单个字段")
    @PostMapping(value = "update")
    public ResultBody updateUserInfo(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    /**
     * 修改密码
     */
    @ApiOperation(value = "用户-根据旧密码修改密码", notes = "根据旧密码修改密码")
    @PostMapping("resetPwdByOldPwd")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPassword", required = true, value = "旧密码", paramType = "form"),
            @ApiImplicitParam(name = "newPassword", required = true, value = "新密码", paramType = "form")
    })
    public ResultBody resetPwdByOldPwd(@RequestParam(value = "oldPassword") String oldPassword,
                                       @RequestParam(value = "newPassword") String newPassword
    ) {
        Long accountId = OpenHelper.getAccountId();
        appAccountService.resetPwdByOldPwd(accountId, oldPassword, newPassword);
        return ResultBody.msg("修改成功");
    }

    /**
     * 重置用户密码
     */
    @ApiOperation(value = "用户-重置用户密码", notes = "重置用户密码")
    @PostMapping("resetPwd")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", required = true, value = "用户Id", paramType = "form"),
            @ApiImplicitParam(name = "password", value = "新密码", paramType = "form")
    })
    public ResultBody resetPwd(@RequestParam(value = "userId") Long userId,
                               @RequestParam(value = "password", required = false) String password
    ) {
        appAccountService.resetPwd(userId, password);
        return ResultBody.msg("密码重置成功");
    }

    /**
     * 换绑手机号
     */
    @ApiOperation(value = "换绑手机号", notes = "换绑手机号")
    @PostMapping("changeMobile")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", required = true, value = "新手机号", paramType = "form", defaultValue = "17703715373"),
            @ApiImplicitParam(name = "smsCode", required = true, value = "验证码", paramType = "form")
    })
    public ResultBody<Boolean> changeMobile(@RequestParam(value = "mobile") String mobile,
                                            @RequestParam(value = "smsCode") String smsCode
    ) {
        Long userId = OpenHelper.getUserId();
        return bizService.changeMobile(userId, mobile, smsCode);
    }

    /**
     * 更新用户状态
     */
    @ApiOperation(value = "用户-更新状态", notes = "更新用户状态")
    @PostMapping(value = "setStatus")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody setStatus(@RequestParam(value = "userId") Long userId) {
        return bizService.setStatus(userId);
    }

    /**
     * 删除用户
     *
     * @param param
     * @return
     */
    @ApiOperation(value = "用户-删除", notes = "删除用户")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map param) {
        return bizService.delete(param);
    }


    /**
     * 添加用户
     *
     * @param param
     * @return
     */
    @PostMapping(value = "add")
    @ApiOperation(value = "用户-添加", notes = "添加用户")
    public ResultBody add(@RequestParam(required = false) Map param) {
        return bizService.saveUser(param);
    }

    @PostMapping(value = "export")
    @ApiOperation(value = "用户-导出", notes = "用户-导出")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) Map params) {
        String handlerName = (String) params.get("handlerName");
        bizService.export(null, params, request, response, "平台用户", "平台用户表", handlerName);
    }

}
