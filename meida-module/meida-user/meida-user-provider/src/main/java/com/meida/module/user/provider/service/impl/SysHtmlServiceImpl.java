package com.meida.module.user.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.user.provider.mapper.SysHtmlMapper;
import com.meida.module.user.client.entity.SysHtml;
import com.meida.module.user.provider.service.SysHtmlService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 服务协议 实现类
 *
 * @author flyme
 * @date 2019-07-13
 */
@Service
public class SysHtmlServiceImpl extends BaseServiceImpl<SysHtmlMapper, SysHtml> implements SysHtmlService {


    @Override
    public SysHtml findByCode(String htmlCode) {
        QueryWrapper queryWrapper = new QueryWrapper<SysHtml>();
        queryWrapper.eq("htmlCode", htmlCode);
        return getOne(queryWrapper);
    }

    @Override
    public SysHtml findByCodeOrId(String htmlCode) {
        QueryWrapper<SysHtml> queryWrapper = new QueryWrapper<SysHtml>();
        queryWrapper.eq("htmlCode", htmlCode);
        queryWrapper.or().eq("htmlId", htmlCode);
        return getOne(queryWrapper);
    }

    @Override
    public List<EntityMap> listByHtmlType(Integer htmlType, Integer htmlGroup) {
        return listByHtmlType(htmlType, htmlGroup, null);
    }

    @Override
    public List<EntityMap> listByHtmlType(Integer htmlType, Integer htmlGroup, CriteriaQueryCallBack cqc) {
        CriteriaQuery<SysHtml> cq = new CriteriaQuery(SysHtml.class);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        }
        cq.eq(SysHtml.class, "htmlStatus", CommonConstants.ENABLED);
        cq.eq(SysHtml.class, "htmlType", htmlType);
        cq.eq(SysHtml.class, "htmlGroup", htmlGroup);
        return selectEntityMap(cq);
    }

    @Override
    public EntityMap findByHtmlCode(String htmlCode, Integer htmlGroup, CriteriaQueryCallBack cqc) {
        CriteriaQuery<SysHtml> cq = new CriteriaQuery(SysHtml.class);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        } else {
            cq.select(SysHtml.class, "htmlId", "htmlTitle", "htmlContent", "htmlGroup", "jumpType", "htmlImage");
        }
        cq.eq(SysHtml.class, "htmlType", CommonConstants.INT_1);
        if (FlymeUtils.isNotEmpty(htmlGroup)) {
            cq.eq(SysHtml.class, "htmlGroup", htmlGroup);
        }
        cq.eq(SysHtml.class, "htmlCode", htmlCode);
        cq.eq(SysHtml.class, "htmlStatus", CommonConstants.ENABLED);
        return findOne(cq);
    }

    @Override
    public EntityMap findByHtmlCode(String htmlCode) {
        return findByHtmlCode(htmlCode, null, null);
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<SysHtml> cq, SysHtml sysHtml, EntityMap requestMap) {
        cq.select(SysHtml.class, "htmlId", "parentId", "htmlType", "sortOrder", "htmlGroup", "htmlTitle", "htmlContent");
        cq.eq("parentId", 0);
        cq.eq("htmlGroup", sysHtml.getHtmlGroup());
        return super.beforeListEntityMap(cq, sysHtml, requestMap);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysHtml html, EntityMap extra) {
        String htmlTitle = html.getHtmlTitle();
        ApiAssert.isNotEmpty("分类名称不能为空", htmlTitle);
        SysHtml prodCategory = getHtml(htmlTitle);
        ApiAssert.isEmpty("分类已存在", prodCategory);

        Long parentId = FlymeUtils.isEmpty(html.getParentId()) ? 0L : html.getParentId();
        SysHtml parent = getById(parentId);
        long count;
        CriteriaQuery cq = new CriteriaQuery(SysHtml.class);
        if (parentId.equals(0L)) {
            count = count(cq.eq("parentId", cs.getParams("parentId")));
        } else {
            count = count(cq.eq("parentId", parent.getHtmlId()));
        }
        Long sortOrder = FlymeUtils.getLong(count, 0L) + 1;

        html.setParentId(parentId);
        html.setHtmlTitle("分类名称" + sortOrder);
        html.setSortOrder(sortOrder.intValue());
        html.setHtmlStatus(CommonConstants.ENABLED);
        return ResultBody.ok();
    }

    public SysHtml getHtml(String htmlTitle) {
        CriteriaQuery<SysHtml> cq = new CriteriaQuery(SysHtml.class);
        cq.eq(true, "htmlTitle", htmlTitle);
        return getOne(cq);
    }


    @Override
    public ResultBody beforeGet(CriteriaQuery<SysHtml> cq, SysHtml sysHtml, EntityMap requestMap) {
        cq.select(SysHtml.class, "*");
        cq.lambda().or(e -> e.eq(true, SysHtml::getHtmlTitle, sysHtml.getHtmlTitle()).or().eq(true, SysHtml::getHtmlCode, sysHtml.getHtmlCode()));
        return super.beforeGet(cq, sysHtml, requestMap);
    }
}
