package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.user.client.entity.SysKeyword;

/**
 * 关键词 接口
 *
 * @author flyme
 * @date 2020-03-22
 */
public interface SysKeywordService extends IBaseService<SysKeyword> {

}
