package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppMenu;
import com.meida.module.user.provider.service.AppMenuService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 菜单信息控制器
 *
 * @author flyme
 * @date 2019-06-27
 */
@RestController
@RequestMapping("/front/menu")
@Api(tags = "公共服务")
@ApiIgnore
public class AppMenuController extends BaseController<AppMenuService, AppMenu> {

    /**
     * 系统资源-菜单信息分页列表
     *
     * @param pageModel
     * @return
     */
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    /**
     * 系统资源-菜单信息列表
     *
     * @param model
     * @return
     */
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    /**
     * 添加系统资源-菜单信息
     *
     * @param model
     * @return
     */
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    /**
     * 更新系统资源-菜单信息
     *
     * @param model
     * @return
     */
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    /**
     * 删除系统资源-菜单信息
     *
     * @param model
     * @return
     */
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }
}
