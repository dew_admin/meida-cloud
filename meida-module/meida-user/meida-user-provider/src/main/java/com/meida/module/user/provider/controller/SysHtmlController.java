package com.meida.module.user.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysHtml;
import com.meida.module.user.provider.service.SysHtmlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;


/**
 * 服务协议控制器
 *
 * @author flyme
 * @date 2019-07-13
 */
@RestController
@RequestMapping("/html/")
@Api(tags = "公共服务")
public class SysHtmlController extends BaseController<SysHtmlService, SysHtml> {

    @ApiOperation(value = "服务协议-分页列表", notes = "服务协议分页列表 ")
    @GetMapping(value = "pageList")
    @ApiIgnore
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "服务协议-列表", notes = "服务协议列表 ")
    @GetMapping(value = "list")
    @ApiIgnore
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "根据类型查询", notes = "根据类型查询")
    @GetMapping(value = "listByHtmlType")
    public ResultBody listByHtmlType(Integer htmlType,Integer htmlGroup) {
        List<EntityMap> list = bizService.listByHtmlType(htmlType,htmlGroup);
        return ResultBody.ok(list);
    }


    @ApiOperation(value = "根据htmlCode查询协议", notes = "根据htmlCode查询协议")
    @GetMapping(value = "findByCode")
    public ResultBody findByCode(String htmlCode) {
        EntityMap map = bizService.findByHtmlCode(htmlCode);
        return ResultBody.ok(map);
    }

    @ApiOperation(value = "服务协议-添加", notes = "添加服务协议")
    @PostMapping(value = "save")
    @ApiIgnore
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "服务协议-更新", notes = "更新服务协议")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "服务协议-删除", notes = "删除服务协议")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "服务协议-详情", notes = "服务协议详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
