package com.meida.module.user.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.module.user.client.entity.AppEducational;
import com.meida.module.user.client.entity.AppWorkhistory;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 工作经历 接口
 *
 * @author flyme
 * @date 2021-08-07
 */
public interface AppWorkhistoryService extends IBaseService<AppWorkhistory> {
    /**
     * 查询用户工作经历
     * @param userId 用户Id
     * @return
     */
    List<EntityMap> listByUserId(Long userId);

    /**
     * 查询用户工作经历
     * @param userId 用户Id
     * @return
     */
    List<EntityMap> listByUserIdAndNoAuth(Long userId);

    /**
     * 查询用户工作经历
     * @param userId 用户ID
     * @param cqc 查询扩展
     * @return
     */
    List<EntityMap> listByUserId(Long userId, CriteriaQueryCallBack cqc);

    /**
     * 编辑工作经历(保留原记录)
     * @param workhistory
     * @return
     */
    boolean updateAndSaveOld(AppWorkhistory workhistory);

    /**
     * 审核并且禁用原记录
     * @param workhistoryId
     * @param state
     * @return
     */
    boolean approveAndDisableParent(Long workhistoryId, Integer state);
}
