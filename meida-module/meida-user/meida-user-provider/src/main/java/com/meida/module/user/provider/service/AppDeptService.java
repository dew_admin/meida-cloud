package com.meida.module.user.provider.service;

import com.meida.module.user.client.entity.AppDept;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 部门 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface AppDeptService extends IBaseService<AppDept> {

}
