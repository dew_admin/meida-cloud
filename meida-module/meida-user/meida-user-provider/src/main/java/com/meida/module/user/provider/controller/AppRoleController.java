package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppRole;
import com.meida.module.user.provider.service.AppRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 用户角色控制器
 *
 * @author flyme
 * @date 2019-06-27
 */
@RestController
@RequestMapping("/front/role")
@Api(tags = "公共服务")
public class AppRoleController extends BaseController<AppRoleService, AppRole> {

    /**
     * 用户角色分页列表
     * @param pageModel
     * @return
     */
    @ApiIgnore
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map param) {
        return bizService.pageList(param);
    }

    /**
     * 用户角色列表
     * @param model
     * @return
     */
    @GetMapping(value = "list")
    @ApiOperation(value = "用户角色列表", notes = "用户角色列表 ")
    public ResultBody list(@RequestParam(required = false) Map param) {
        return bizService.listEntityMap(param);
    }

    /**
     * 添加用户角色
     * @param model
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map param) {
        return bizService.add(param);
    }

    /**
     * 更新用户角色
     * @param model
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map param) {
        return bizService.edit(param);
    }

    /**
     * 删除用户角色
     * @param model
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map param) {
        return bizService.delete(param);
    }
}
