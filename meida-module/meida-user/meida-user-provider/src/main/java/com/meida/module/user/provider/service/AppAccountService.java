package com.meida.module.user.provider.service;

import com.meida.common.base.service.BaseThirdLoginService;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.user.client.dto.ThirdBindParams;
import com.meida.module.user.client.entity.AppAccount;

/**
 * 系统用户登录账号管理
 * 支持多账号登陆
 *
 * @author zyf
 */
public interface AppAccountService extends IBaseService<AppAccount>, BaseThirdLoginService {


    /**
     * 绑定账户
     */
    ResultBody thirdBind(Long userId, ThirdBindParams thirdBindParams);


    /**
     * 绑定系统用户名账户
     *
     * @param userId
     * @param accontName
     * @param password
     * @return
     */
    AppAccount registerUsernameAccount(Long userId, String accontName, String password, Boolean check);

    /**
     * 绑定email账号
     *
     * @param email
     * @param userId
     * @param password
     * @return
     */
    AppAccount registerEmailAccount(Long userId, String email, String password);


    /**
     * 绑定手机账号
     *
     * @param userId
     * @param password
     * @param mobile
     * @param areaCode
     * @return
     */
    AppAccount registerMobileAccount(Long userId, String mobile, String areaCode, String password, Boolean check);


    /**
     * 支持密码、手机号、QQ、微信等第三方登录
     * 其他方式没有规则，无法自动识别。需要单独开发
     *
     * @param account 登陆账号
     * @return
     */
    AppAccount applogin(String account);

    AppAccount getUserAccount(String account, String accountType);

    /**
     * 重置用户密码
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    void resetPassword(Long userId, String oldPassword, String newPassword);

    /**
     * 根据验证码重置用户密码
     *
     * @param userId
     * @param mobile
     * @param smsCode
     * @param newPassword
     * @return
     */
    void resetPwdBySmsCode(String mobile, String smsCode, String newPassword);

    /**
     * 根据验证码重置用户密码
     *
     * @param useId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    void resetPwdByOldPwd(Long useId, String oldPassword, String newPassword);

    /**
     * 重置用户密码
     *
     * @param userId
     * @param pasword
     */
    void resetPwd(Long userId, String pasword);


    /**
     * 校验账户是否存在
     *
     * @param accountName
     */
    Boolean checkByAccountName(String accountName);

    /**
     * 校验账户是否绑定手机号
     *
     * @param accountName
     */
    Boolean accountIsBind(String accountName);

    /**
     * 检查账号是否存在
     *
     * @param userId
     * @param account
     * @param accountType
     * @return
     */
    Boolean isExist(Long userId, String account, String accountType);

    /**
     * 检查账号是否存在
     *
     * @param account
     * @param accountType
     * @return
     */
    Boolean isExist(String account, String accountType);

    /**
     * 检查账户类型是否存在
     *
     * @param userId
     * @param accountType
     * @return
     */
    Boolean isExist(Long userId, String accountType);

    /**
     * 解绑email账号
     *
     * @param email
     * @param userId
     * @return
     */
    void removeEmailAccount(Long userId, String email);

    /**
     * 解绑手机账号
     *
     * @param userId
     * @param mobile
     * @return
     */
    void removeMobileAccount(Long userId, String mobile);


    /**
     * 查询手机号账户
     *
     * @param userId
     * @return
     */
    AppAccount getMobileAccount(Long userId);


    /**
     * 查询账户
     *
     * @param userId
     * @return
     */
    AppAccount getAccount(Long userId, String accountType);

    /**
     * 查询账户
     *
     * @param userId
     * @return
     */
    EntityMap getAccountByAccountTypeAndUserId(Long userId, String accountType);


    /**
     * 查询手机号账户
     *
     * @param mobile
     * @return
     */
    AppAccount getMobileAccount(String mobile);

    /**
     * 删除账户
     *
     * @param userIds
     */
    void removeAccount(Long[] userIds);

    /**
     * 删除账户
     *
     * @param userId
     */
    void removeAccount(Long userId);


}
