package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysHtml;


/**
 * 服务协议 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-13
 */
public interface SysHtmlMapper extends SuperMapper<SysHtml> {

}
