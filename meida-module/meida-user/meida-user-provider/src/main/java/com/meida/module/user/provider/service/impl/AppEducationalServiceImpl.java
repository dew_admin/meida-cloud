package com.meida.module.user.provider.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.system.client.entity.SysDictData;
import com.meida.module.user.client.entity.AppEducational;
import com.meida.module.user.client.entity.AppSchool;
import com.meida.module.user.provider.mapper.AppEducationalMapper;
import com.meida.module.user.provider.service.AppEducationalService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 教育经历接口实现类
 *
 * @author flyme
 * @date 2021-08-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppEducationalServiceImpl extends BaseServiceImpl<AppEducationalMapper, AppEducational> implements AppEducationalService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AppEducational educational, EntityMap extra) {
        educational.setState(CommonConstants.DISABLED);
        educational.setShowState(CommonConstants.ENABLED);
        educational.setUserId(OpenHelper.getUserId());
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AppEducational> cq, AppEducational educational, EntityMap requestMap) {
        cq.orderByDesc("educational.createTime");
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> listByUserId(Long userId) {
        return listByUserId(userId, null);
    }

    @Override
    public List<EntityMap> listByUserIdAndNoAuth(Long userId) {
        CriteriaQuery<AppEducational> cq = new CriteriaQuery(AppEducational.class);
        cq.select(AppEducational.class,"*");
        cq.select(SysDictData.class, "dicDataTitle");
        cq.select(AppSchool.class, "schoolName", "schoolLogo");
        cq.createJoin(AppSchool.class).setMainField("schoolId").setJoinField("schoolId");
        cq.createJoin(SysDictData.class).setMainField("educationId").setJoinField("dictDataId");
        cq.eq(AppEducational.class, "userId", userId);
        cq.eq(AppEducational.class, "showState", CommonConstants.ENABLED);

        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> listByUserId(Long userId, CriteriaQueryCallBack cqc) {
        CriteriaQuery<AppEducational> cq = new CriteriaQuery(AppEducational.class);
        cq.eq(AppEducational.class, "state", CommonConstants.ENABLED);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        }
        cq.eq(AppEducational.class, "userId", userId);
        return selectEntityMap(cq);
    }

    @Override
    public boolean updateAndSaveOld(AppEducational educational) {
        Long educationalId = educational.getEducationalId();
        if (FlymeUtils.isNotEmpty(educationalId)) {
            Long parentId = educational.getEducationalId();
            //查询现有对象
            AppEducational appEducational = getById(educationalId);
            Integer authState = appEducational.getState();
            if (authState.equals(CommonConstants.INT_1)) {
                educational.setState(CommonConstants.INT_0);
                educational.setShowState(CommonConstants.INT_1);
                educational.setParentId(parentId);
                educational.setEducationalId(IdWorker.getId());
                educational.setUserId(appEducational.getUserId());
                appEducational.setShowState(0);
                //隐藏原对象
                appEducational.updateById();
                return educational.insert();
            } else {
                educational.updateById();
            }
        } else {
            educational.setState(CommonConstants.DISABLED);
            educational.setShowState(CommonConstants.INT_1);
            educational.setUserId(OpenHelper.getUserId());
            return educational.insert();
        }
        return true;
    }

    @Override
    public boolean approveAndDisableParent(Long educationalId, Integer state) {
        AppEducational educational = getById(educationalId);
        Long parentId = educational.getParentId();
        if (state.equals(CommonConstants.ENABLED)) {
            ApiAssert.isNotEmpty("对象不存在", educational);
            educational.setState(CommonConstants.ENABLED);
            educational.setShowState(CommonConstants.ENABLED);
            educational.setParentId(null);
            boolean tag = educational.updateById();
            if (FlymeUtils.isNotEmpty(parentId)) {
                CriteriaDelete cd = new CriteriaDelete();
                cd.eq(true, "educationalId", parentId);
                remove(cd);
            }
        } else {
            if (FlymeUtils.isNotEmpty(parentId)) {
                CriteriaDelete cd = new CriteriaDelete();
                cd.eq(true, "educationalId", educationalId);
                remove(cd);
                CriteriaUpdate cu = new CriteriaUpdate();
                cu.set("state", CommonConstants.INT_1);
                cu.set("showState", CommonConstants.INT_1);
                cu.eq(true, "educationalId", parentId);
                update(cu);
            }
        }
        return true;
    }
}
