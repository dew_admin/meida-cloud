package com.meida.module.user.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.module.user.client.entity.SysHtml;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * 服务协议 接口
 *
 * @author flyme
 * @date 2019-07-13
 */
public interface SysHtmlService extends IBaseService<SysHtml> {

    SysHtml findByCode(String htmlCode);

    SysHtml findByCodeOrId(String htmlCode);

    /**
     * 根据类型查询
     * @param htmlType
     * @param htmlGroup
     * @return
     */
    List<EntityMap> listByHtmlType(Integer htmlType,Integer htmlGroup);

    List<EntityMap> listByHtmlType(Integer htmlType,Integer htmlGroup, CriteriaQueryCallBack cqc);


    /**
     * 查询服务协议
     * @param htmlCode
     * @param htmlGroup
     * @param cqc
     * @return
     */
    EntityMap findByHtmlCode(String htmlCode,Integer htmlGroup,CriteriaQueryCallBack cqc);

    /**
     * 查询服务协议
     * @param htmlCode
     * @return
     */
    EntityMap findByHtmlCode(String htmlCode);
}
