package com.meida.module.user.provider.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.user.client.entity.AppWorkhistory;
import com.meida.module.user.provider.mapper.AppWorkhistoryMapper;
import com.meida.module.user.provider.service.AppWorkhistoryService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 工作经历接口实现类
 *
 * @author flyme
 * @date 2021-08-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppWorkhistoryServiceImpl extends BaseServiceImpl<AppWorkhistoryMapper, AppWorkhistory> implements AppWorkhistoryService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AppWorkhistory workhistory, EntityMap extra) {
        workhistory.setState(CommonConstants.DISABLED);
        workhistory.setUserId(OpenHelper.getUserId());
        workhistory.setShowState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }
    @Override
    public List<EntityMap> listByUserIdAndNoAuth(Long userId) {
        CriteriaQuery<AppWorkhistory> cq = new CriteriaQuery(AppWorkhistory.class);
        cq.select(AppWorkhistory.class,"*");
        cq.eq(AppWorkhistory.class, "userId", userId);
        cq.eq(AppWorkhistory.class, "showState", CommonConstants.ENABLED);
        return selectEntityMap(cq);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AppWorkhistory> cq, AppWorkhistory workhistory, EntityMap requestMap) {
        cq.eq(AppWorkhistory.class, "companyName");
        cq.orderByDesc("workhistory.createTime");
        return ResultBody.ok();
    }


    @Override
    public List<EntityMap> listByUserId(Long userId) {
        return listByUserId(userId, null);
    }

    @Override
    public List<EntityMap> listByUserId(Long userId, CriteriaQueryCallBack cqc) {
        CriteriaQuery<AppWorkhistory> cq = new CriteriaQuery(AppWorkhistory.class);
        cq.eq(AppWorkhistory.class, "state", CommonConstants.ENABLED);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        }
        cq.eq(AppWorkhistory.class, "userId", userId);
        return selectEntityMap(cq);
    }

    @Override
    public boolean updateAndSaveOld(AppWorkhistory workhistory) {
        Long workhistoryId = workhistory.getWorkhistoryId();
        if (FlymeUtils.isNotEmpty(workhistoryId)) {
            Long parentId = workhistory.getWorkhistoryId();
            //查询现有对象
            AppWorkhistory appWorkhistory = getById(workhistoryId);
            Integer authState = appWorkhistory.getState();
            if (authState.equals(CommonConstants.INT_1)) {
                workhistory.setState(CommonConstants.INT_0);
                workhistory.setShowState(CommonConstants.INT_1);
                workhistory.setParentId(parentId);
                workhistory.setWorkhistoryId(IdWorker.getId());
                workhistory.setUserId(appWorkhistory.getUserId());
                appWorkhistory.setShowState(0);
                //隐藏原对象
                appWorkhistory.updateById();
                return workhistory.insert();
            } else {
                workhistory.updateById();
            }
        } else {
            workhistory.setState(CommonConstants.DISABLED);
            workhistory.setShowState(CommonConstants.INT_1);
            workhistory.setUserId(OpenHelper.getUserId());
            return workhistory.insert();
        }
        return true;
    }


    @Override
    public boolean approveAndDisableParent(Long workhistoryId, Integer state) {
        AppWorkhistory workhistory = getById(workhistoryId);
        Long parentId = workhistory.getParentId();
        if (state.equals(CommonConstants.ENABLED)) {
            ApiAssert.isNotEmpty("对象不存在", workhistory);
            workhistory.setState(CommonConstants.ENABLED);
            workhistory.setShowState(CommonConstants.ENABLED);
            workhistory.setParentId(null);
            boolean tag = workhistory.updateById();
            if (FlymeUtils.isNotEmpty(parentId)) {
                CriteriaDelete cd = new CriteriaDelete();
                cd.eq(true, "workhistoryId", parentId);
                remove(cd);
            }
        } else {
            if (FlymeUtils.isNotEmpty(parentId)) {
                CriteriaDelete cd = new CriteriaDelete();
                cd.eq(true, "workhistoryId", workhistoryId);
                remove(cd);
                CriteriaUpdate cu = new CriteriaUpdate();
                cu.set("state", CommonConstants.INT_1);
                cu.set("showState", CommonConstants.INT_1);
                cu.eq(true, "workhistoryId", parentId);
                update(cu);
            }
        }
        return true;
    }
}
