package com.meida.module.user.provider.listener;

import com.meida.common.constants.QueueConstants;
import com.meida.common.security.OpenUser;
import com.meida.module.user.provider.service.AppUserService;
import com.meida.mq.annotation.MsgListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 用户监听器
 *
 * @author zyf
 */
@Component
public class AppUserRabbitMqListener {

    @Resource
    private AppUserService userService;

    /**
     * 更新用户企业ID
     *
     * @return
     */
    @MsgListener(queues = QueueConstants.QUEUE_BIND_COMPANY)
    public void process(OpenUser openUser) {
        userService.bindCompany(openUser);
    }

    /**
     * 清空用户企业ID
     *
     * @return
     */
    @MsgListener(queues = QueueConstants.QUEUE_UNBIND_COMPANY)
    public void unbind(Long companyId) {
        userService.unBindCompany(companyId);
    }


}
