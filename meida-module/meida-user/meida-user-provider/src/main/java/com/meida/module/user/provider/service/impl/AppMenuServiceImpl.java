package com.meida.module.user.provider.service.impl;


import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.user.client.entity.AppMenu;
import com.meida.module.user.provider.mapper.AppMenuMapper;
import com.meida.module.user.provider.service.AppMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 系统资源-菜单信息 实现类
 *
 * @author flyme
 * @date 2019-06-27
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppMenuServiceImpl extends BaseServiceImpl<AppMenuMapper, AppMenu> implements AppMenuService {




    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AppMenu> cq, AppMenu appMenu, EntityMap requestMap) {
        cq.select(AppMenu.class, "parentId", "menuId");
        return super.beforePageList(cq, appMenu, requestMap);
    }
}
