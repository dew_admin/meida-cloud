package com.meida.module.user.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysSlider;
import com.meida.module.user.provider.service.SysSliderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 轮播图控制器
 *
 * @author flyme
 * @date 2019-07-05
 */
@RestController
@RequestMapping("/slider/")
@Api(tags = "基础模块-轮播图管理")
public class SysSliderController extends BaseController<SysSliderService, SysSlider> {

    @ApiOperation(value = "轮播图-分页列表", notes = "轮播图分页列表 ")
    @GetMapping(value = "page")
    @ApiIgnore
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "轮播图-列表", notes = "轮播图列表 ")
    @GetMapping(value = "list")
    @ApiIgnore
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "轮播图-添加", notes = "添加轮播图")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "轮播图-更新", notes = "更新轮播图")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "轮播图-删除", notes = "删除轮播图")
    @PostMapping(value = "remove")
    @ApiIgnore
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "轮播图-详情", notes = "轮播图详情")
    @PostMapping(value = "get")
    @ApiIgnore
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "轮播图-发布状态", notes = "轮播图-发布状态")
    @PostMapping(value = "setSendStatus")
    @ApiIgnore
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "sendStatus", required = true, value = "发布状态", paramType = "query")
    })
    public ResultBody setSendStatus(@RequestParam(required = false) Map map, @RequestParam(value = "sendStatus") Integer sendStatus) {
        return bizService.setState(map, "sendStatus", sendStatus);
    }


    @ApiOperation(value = "根据类型查询轮播图", notes = "根据类型查询轮播图")
    @PostMapping(value = "getOneBySliderType")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sliderType", required = true, value = "轮播图类型", paramType = "form")
    })
    public ResultBody getOneBySliderType(@RequestParam(value = "sliderType") Integer sliderType) {
        EntityMap map = bizService.getOneBySliderType(sliderType);
        return ResultBody.ok(map);
    }

}
