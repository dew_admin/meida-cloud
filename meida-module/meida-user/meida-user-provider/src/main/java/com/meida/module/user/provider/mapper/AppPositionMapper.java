package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppPosition;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 职位 Mapper 接口
 *
 * @author flyme
 * @date 2019-08-04
 */
public interface AppPositionMapper extends SuperMapper<AppPosition> {

}
