package com.meida.module.user.provider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppPosition;
import com.meida.module.user.provider.service.AppPositionService;

import javax.swing.*;
import java.util.Map;


/**
 * 职位控制器
 *
 * @author flyme
 * @date 2019-08-04
 */
@RestController
@Api(tags = "APP用户管理")
@RequestMapping("/front/position/")
public class AppPositionController extends BaseController<AppPositionService, AppPosition> {

    @ApiOperation(value = "职位-分页列表", notes = "职位分页列表 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "职位-列表", notes = "职位列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "职位-添加", notes = "添加职位")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "职位-更新", notes = "更新职位")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "职位-删除", notes = "删除职位")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


}
