package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysFeedback;


/**
 * 反馈意见 Mapper 接口
 *
 * @author flyme
 * @date 2019-09-08
 */
public interface SysFeedbackMapper extends SuperMapper<SysFeedback> {

}
