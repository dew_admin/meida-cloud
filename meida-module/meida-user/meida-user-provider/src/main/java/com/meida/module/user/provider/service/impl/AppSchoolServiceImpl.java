package com.meida.module.user.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.module.user.client.entity.AppSchool;
import com.meida.module.user.provider.mapper.AppSchoolMapper;
import com.meida.module.user.provider.service.AppSchoolService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 学校接口实现类
 *
 * @author flyme
 * @date 2021-08-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppSchoolServiceImpl extends BaseServiceImpl<AppSchoolMapper, AppSchool> implements AppSchoolService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AppSchool school, EntityMap extra) {
        school.setState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<AppSchool> cq, AppSchool school, EntityMap requestMap) {
        cq.orderByDesc("school.createTime");
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> schoolList(CriteriaQueryCallBack cqc) {
        CriteriaQuery<AppSchool> cq = new CriteriaQuery(AppSchool.class);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        }
        cq.lambda().eq(AppSchool::getState, CommonConstants.ENABLED);
        return selectEntityMap(cq);
    }
}
