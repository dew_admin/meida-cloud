package com.meida.module.user.provider.controller;

import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.user.client.entity.SysHtml;
import com.meida.module.user.provider.service.SysHtmlService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: zyf
 * @date: 2018/11/9 15:43
 * @description: 基础服务模块接口, 不需要登录鉴权的接口
 */
@Api(tags = "基础模块-公共接口")
@Controller
@RequestMapping("/common")
public class SysCommonController {

    @Autowired
    private SysHtmlService htmlService;

    /**
     * 获取服务协议
     */
    @GetMapping(value = "/html/get")
    public String getHtml(ModelMap map, String code, String title) {
        ApiAssert.isNotEmpty("参数不能为空", code);
        SysHtml html = htmlService.findByCodeOrId(code);
        if (FlymeUtils.isEmpty(html)) {
            html = new SysHtml();
            html.setHtmlCode(code);
            html.setIsParent(0);
            html.setParentId(1L);
            html.setHtmlStatus(1);
            html.setHtmlContent(title);
            html.setHtmlTitle(title);
            htmlService.save(html);
        }
        map.addAttribute("html", html);
        return "html";
    }


    /**
     * 分享APP下载页面
     */
    @RequestMapping(value = "/download")
    public String download(ModelMap map, String type, String courseId) {
        return "download";
    }


 
}
