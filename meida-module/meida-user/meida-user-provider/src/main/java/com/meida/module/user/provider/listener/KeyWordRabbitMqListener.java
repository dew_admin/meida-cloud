package com.meida.module.user.provider.listener;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.user.provider.service.SysUserKeywordService;
import com.meida.mq.annotation.MsgListener;
import com.meida.starter.rabbitmq.config.RabbitComponent;
import com.meida.starter.rabbitmq.core.BaseRabbiMqHandler;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

import javax.annotation.Resource;

/**
 * 搜索关机键记录
 *
 * @author zyf
 */
@RabbitComponent(value = "keyWordListener")
public class KeyWordRabbitMqListener extends BaseRabbiMqHandler<EntityMap> {

    @Resource
    private SysUserKeywordService userKeywordService;

    @MsgListener(queues = "keyword")
    public void onMessage(EntityMap baseMap, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        super.onMessage(baseMap, deliveryTag, channel, new MqListener<EntityMap>() {
            @Override
            public void handler(EntityMap map, Channel channel) {
                String keyword = map.get("keyword");
                if (FlymeUtils.isNotEmpty(keyword)) {
                    userKeywordService.insert(keyword);
                }
            }
        });
    }
}
