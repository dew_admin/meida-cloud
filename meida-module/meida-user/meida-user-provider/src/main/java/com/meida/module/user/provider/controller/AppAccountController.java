package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppAccount;
import com.meida.module.user.provider.service.AppAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * APP账户模块接口
 *
 * @author zyf
 */
@Api(tags = "APP用户管理")
@RestController
@RequestMapping("/front/account/")
public class AppAccountController extends BaseController<AppAccountService, AppAccount> {


    @ApiOperation(value = "账户-分页列表", notes = "APP账户分页列表 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    /**
     * 更新其他账户基本资料
     */
    @ApiOperation(value = "账户-更新", notes = "可更新单个字段")
    @PostMapping(value = "update")
    public ResultBody updateUserInfo(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    /**
     * 重置账户密码
     */
    @ApiOperation(value = "账户-重置账户密码", notes = "重置账户密码")
    @PostMapping("resetPwd")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", required = true, value = "账户Id", paramType = "form"),
            @ApiImplicitParam(name = "password", value = "新密码", paramType = "form")
    })
    public ResultBody resetPwd(@RequestParam(value = "userId") Long userId,
                               @RequestParam(value = "password", required = false) String password
    ) {
        bizService.resetPwd(userId, password);
        return ResultBody.msg("密码重置成功");
    }


    /**
     * 更新账户状态
     */
    @ApiOperation(value = "账户-更新状态", notes = "更新账户状态")
    @PostMapping(value = "setStatus")
    @ApiImplicitParam(name = "userId", required = true, value = "主键", paramType = "form")
    public ResultBody setStatus(@RequestParam(value = "userId") Long userId) {
        return null;
    }

    /**
     * 删除账户
     *
     * @param model
     * @return
     */
    @ApiOperation(value = "账户-删除", notes = "删除账户")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    /**
     * 添加账户
     *
     * @param model
     * @return
     */
    @PostMapping(value = "add")
    @ApiOperation(value = "账户-添加", notes = "添加账户")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }
}
