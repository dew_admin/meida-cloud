package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppDept;
import com.meida.module.user.provider.service.AppDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 部门控制器
 *
 * @author flyme
 * @date 2019-07-05
 */
@RestController
@RequestMapping("/front/dept")
@Api(tags = "公共服务")
public class AppDeptController extends BaseController<AppDeptService, AppDept> {

    @ApiOperation(value = "部门-分页列表", notes = "部门分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "部门-列表", notes = "部门列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "部门-添加", notes = "添加部门")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "部门-更新", notes = "更新部门")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "部门-删除", notes = "删除部门")
    @PostMapping(value = "remove")
    @ApiIgnore
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "部门-详情", notes = "部门详情")
    @GetMapping(value = "get")
    @ApiIgnore
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
