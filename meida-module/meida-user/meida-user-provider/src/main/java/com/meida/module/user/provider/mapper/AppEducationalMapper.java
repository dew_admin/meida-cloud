package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppEducational;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 教育经历 Mapper 接口
 * @author flyme
 * @date 2021-08-07
 */
public interface AppEducationalMapper extends SuperMapper<AppEducational> {

}
