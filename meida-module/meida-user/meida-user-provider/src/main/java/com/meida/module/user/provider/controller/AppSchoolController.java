package com.meida.module.user.provider.controller;

import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppSchool;
import com.meida.module.user.provider.service.AppSchoolService;


/**
 * 学校控制器
 *
 * @author flyme
 * @date 2021-08-07
 */
@RestController
@RequestMapping("/school/")
public class AppSchoolController extends BaseController<AppSchoolService, AppSchool> {

    @ApiOperation(value = "学校-分页列表", notes = "学校分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "学校-列表", notes = "学校列表")
    @GetMapping(value = "list")
    public ResultBody list() {
        List<EntityMap> list = bizService.schoolList(null);
        return ResultBody.ok(list);
    }

    @ApiOperation(value = "学校-添加", notes = "添加学校")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "学校-更新", notes = "更新学校")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "学校-删除", notes = "删除学校")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "学校-详情", notes = "学校详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "更新学校状态", notes = "更新学校状态")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setReasonSort(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "state", state);
    }
}
