package com.meida.module.user.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.user.client.entity.AppDept;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class AppDeptInitializer extends DbInitializer{

    @Override
    public Class<?> getEntityClass() {
        return AppDept.class;
    }
}
