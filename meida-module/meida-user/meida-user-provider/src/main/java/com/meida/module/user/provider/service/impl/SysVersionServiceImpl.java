package com.meida.module.user.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.user.provider.mapper.SysVersionMapper;
import com.meida.module.user.client.entity.SysVersion;
import com.meida.module.user.provider.service.SysVersionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 版本控制 实现类
 *
 * @author flyme
 * @date 2019-10-19
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysVersionServiceImpl extends BaseServiceImpl<SysVersionMapper, SysVersion> implements SysVersionService {

    @Override
    public ResultBody getByVeCode(String veCode) {
        CriteriaQuery cq = new CriteriaQuery(SysVersion.class);
        cq.select(SysVersion.class, "*");
        cq.eq(SysVersion.class, "verCode", veCode);
        cq.orderByDesc("verNo");
        cq.last("limit 0,1");
        return baseGet(cq);
    }

}
