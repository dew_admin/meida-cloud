package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysSlider;


/**
 * 轮播图 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface SysSliderMapper extends SuperMapper<SysSlider> {

}
