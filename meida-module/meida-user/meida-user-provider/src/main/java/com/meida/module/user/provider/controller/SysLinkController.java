package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysLink;
import com.meida.module.user.provider.service.SysLinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 友情链接控制器
 *
 * @author flyme
 * @date 2019-07-11
 */
@RestController
@RequestMapping("/link/")
@Api(tags = "基础模块-友情链接")
public class SysLinkController extends BaseController<SysLinkService, SysLink> {

    @ApiOperation(value = "友情链接-分页列表", notes = "友情链接分页列表 ")
    @GetMapping(value = "page")
    @ApiIgnore
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "友情链接-列表", notes = "友情链接列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "友情链接-添加", notes = "添加友情链接")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "友情链接-更新", notes = "更新友情链接")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "友情链接-删除", notes = "删除友情链接")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "友情链接-详情", notes = "友情链接详情")
    @PostMapping(value = "get")
    @ApiIgnore
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "友情链接-发布状态", notes = "轮播图-发布状态")
    @PostMapping(value = "setSendStatus")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "sendStatus", required = true, value = "发布状态", paramType = "query")
    })
    public ResultBody setOnLineState(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "sendStatus") Integer sendStatus) {
        return bizService.setSendStatus(ids, sendStatus);
    }
}
