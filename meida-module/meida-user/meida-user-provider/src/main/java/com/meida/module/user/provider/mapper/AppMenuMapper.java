package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppMenu;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 系统资源-菜单信息 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface AppMenuMapper extends SuperMapper<AppMenu> {

}
