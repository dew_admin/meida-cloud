package com.meida.module.user.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.user.client.entity.SysSlider;
import com.meida.module.user.provider.mapper.SysSliderMapper;
import com.meida.module.user.provider.service.SysSliderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 轮播图 实现类
 *
 * @author flyme
 * @date 2019-07-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysSliderServiceImpl extends BaseServiceImpl<SysSliderMapper, SysSlider> implements SysSliderService {

    @Override
    public List<EntityMap> listBySliderType(int sliderType) {
        CriteriaQuery cq = new CriteriaQuery(SysSlider.class);
        cq.select("sliderId", "sliderTitle", "sliderImg", "jumpType", "content");
        cq.eq("sliderType", sliderType);
        cq.eq("sendStatus", CommonConstants.ENABLED);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }

    @Override
    public List<EntityMap> listBySliderType(int sliderType, CriteriaQueryCallBack cqc) {
        CriteriaQuery cq = new CriteriaQuery(SysSlider.class);
        if (FlymeUtils.isNotEmpty(cqc)) {
            cqc.init(cq);
        }
        return listBySliderType(sliderType);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<EntityMap> listBySliderType(int sliderType, int count) {
        CriteriaQuery cq = new CriteriaQuery(SysSlider.class);
        cq.select("sliderId", "sliderTitle", "sliderImg", "jumpType", "content");
        cq.eq("sliderType", sliderType);
        cq.eq("sendStatus", CommonConstants.ENABLED);
        cq.orderByAsc("sortOrder");
        cq.limit(count);
        return selectEntityMap(cq);
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysSlider> cq, SysSlider sysSlider, EntityMap requestMap) {
        cq.likeByField(SysSlider.class, "sliderTitle");
        cq.eq(SysSlider.class, "sliderType");
        cq.orderByAsc("sortOrder");
        return ResultBody.ok();
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public EntityMap getOneBySliderType(Integer sliderType) {
        CriteriaQuery cq = new CriteriaQuery(SysSlider.class);
        cq.select("*");
        cq.eq(SysSlider.class, "sliderType", sliderType);
        cq.orderByDesc("slider.createTime");
        cq.limit(1);
        return findOne(cq);
    }

    @Override
    public List<EntityMap> listBySliderTypeAndModuleId(int sliderType, int moduleId) {
        CriteriaQuery cq = new CriteriaQuery(SysSlider.class);
        cq.select("sliderId", "sliderTitle", "sliderImg", "jumpType", "content");
        cq.eq("sliderType", sliderType);
        cq.eq("sendStatus", CommonConstants.ENABLED);
        cq.eq("moduleId", moduleId);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }
}
