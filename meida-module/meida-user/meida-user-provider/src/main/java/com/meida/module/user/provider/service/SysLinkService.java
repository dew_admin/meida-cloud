package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.user.client.entity.SysLink;

import java.util.List;

/**
 * 友情链接 接口
 *
 * @author flyme
 * @date 2019-07-11
 */
public interface SysLinkService extends IBaseService<SysLink> {
    /**
     * 查询友情链接
     *
     * @param count
     * @return
     */
    List<EntityMap> selectByCount(int count);

    /**
     * 更新发布状态
     *
     * @param ids
     * @param sendStatus
     * @return
     */
    ResultBody setSendStatus(Long[] ids, Integer sendStatus);
}
