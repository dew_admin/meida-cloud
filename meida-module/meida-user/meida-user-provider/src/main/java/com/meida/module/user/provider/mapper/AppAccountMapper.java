package com.meida.module.user.provider.mapper;


import com.meida.module.user.client.entity.AppAccount;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */
public interface AppAccountMapper extends SuperMapper<AppAccount> {

}
