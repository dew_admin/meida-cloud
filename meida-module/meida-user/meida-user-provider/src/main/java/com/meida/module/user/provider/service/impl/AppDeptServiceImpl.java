package com.meida.module.user.provider.service.impl;


import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.user.client.entity.AppDept;
import com.meida.module.user.provider.mapper.AppDeptMapper;
import com.meida.module.user.provider.service.AppDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 部门 实现类
 *
 * @author flyme
 * @date 2019-07-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppDeptServiceImpl extends BaseServiceImpl<AppDeptMapper, AppDept> implements AppDeptService {


    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<AppDept> cq, AppDept appDept, EntityMap requestMap) {
        cq.select(AppDept.class, "*");
        return super.beforeListEntityMap(cq, appDept, requestMap);
    }

    @Override
    public ResultBody beforePageList(CriteriaQuery<AppDept> cq, AppDept appDept, EntityMap requestMap) {
        cq.select(AppDept.class, "*");
        return super.beforePageList(cq, appDept, requestMap);
    }



    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AppDept appDept,EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        appDept.setCompanyId( companyId);
        return ResultBody.ok();
    }
}
