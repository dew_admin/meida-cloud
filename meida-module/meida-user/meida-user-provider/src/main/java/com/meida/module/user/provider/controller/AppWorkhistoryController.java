package com.meida.module.user.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.ApiAssert;
import com.meida.module.user.client.entity.AppWorkhistory;
import com.meida.module.user.provider.service.AppWorkhistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 工作经历控制器
 *
 * @author flyme
 * @date 2021-08-07
 */
@RestController
@RequestMapping("/workhistory/")
public class AppWorkhistoryController extends BaseController<AppWorkhistoryService, AppWorkhistory> {

    @ApiOperation(value = "工作经历-分页列表", notes = "工作经历分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "工作经历-列表", notes = "工作经历列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "工作经历-添加", notes = "添加工作经历")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "工作经历-更新", notes = "更新工作经历")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "工作经历-删除", notes = "删除工作经历")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "工作经历-详情", notes = "工作经历详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "工作经历-编辑并保留原记录", notes = "编辑工作经历(保留原记录)")
    @PostMapping(value = "updateAndSaveOld")
    public ResultBody updateAndSaveOld(AppWorkhistory workhistory) {
        boolean n = bizService.updateAndSaveOld(workhistory);
        return ResultBody.result("更新", n);
    }




    @ApiOperation(value = "工作经历-审核并禁用原记录", notes = "审核并禁用原记录")
    @PostMapping(value = "authSub")
    public ResultBody authSub(Long workhistoryId,Integer state) {
        boolean n = bizService.approveAndDisableParent(workhistoryId,state);
        return ResultBody.result("审核", n);
    }

    @ApiOperation(value = "用户工作经历-列表", notes = "用户工作经历")
    @GetMapping(value = "listByUserId")
    public ResultBody listByUserId(@RequestParam(required = false) Map params) {
        EntityMap map=new EntityMap(params);
        Long userId = map.getLong("userId");
        ApiAssert.isNotEmpty("userId不能为空", userId);
        List<EntityMap> list = bizService.listByUserIdAndNoAuth(userId);
        return ResultBody.ok(list);
    }

}
