package com.meida.module.user.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.user.client.entity.SysVersion;

/**
 * 版本控制 Mapper 接口
 * @author flyme
 * @date 2019-10-19
 */
public interface SysVersionMapper extends SuperMapper<SysVersion> {

}
