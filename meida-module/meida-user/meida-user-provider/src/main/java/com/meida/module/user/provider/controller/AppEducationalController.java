package com.meida.module.user.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.utils.ApiAssert;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.AppEducational;
import com.meida.module.user.provider.service.AppEducationalService;


/**
 * 教育经历控制器
 *
 * @author flyme
 * @date 2021-08-07
 */
@RestController
@RequestMapping("/educational/")
public class AppEducationalController extends BaseController<AppEducationalService, AppEducational> {

    @ApiOperation(value = "教育经历-分页列表", notes = "教育经历分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "教育经历-列表", notes = "教育经历列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "教育经历-添加", notes = "添加教育经历")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "教育经历-更新", notes = "更新教育经历")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "教育经历-删除", notes = "删除教育经历")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "教育经历-详情", notes = "教育经历详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "教育经历-编辑并保留原记录", notes = "编辑教育经历(保留原记录)")
    @PostMapping(value = "updateAndSaveOld")
    public ResultBody updateAndSaveOld(AppEducational educational) {
        boolean n = bizService.updateAndSaveOld(educational);
        return ResultBody.result("更新", n);
    }


    @ApiOperation(value = "教育经历-审核并禁用原记录", notes = "审核并禁用原记录")
    @PostMapping(value = "authSub")
    public ResultBody authSub(Long educationalId,Integer state) {
        boolean n = bizService.approveAndDisableParent(educationalId,state);
        return ResultBody.result("审核", n);
    }

    @ApiOperation(value = "用户教育经历-列表", notes = "用户教育经历")
    @GetMapping(value = "listByUserId")
    public ResultBody listByUserId(@RequestParam(required = false) Map params) {
        EntityMap map=new EntityMap(params);
        Long userId = map.getLong("userId");
        ApiAssert.isNotEmpty("userId不能为空", userId);
        List<EntityMap> list = bizService.listByUserIdAndNoAuth(userId);
        return ResultBody.ok(list);
    }
}
