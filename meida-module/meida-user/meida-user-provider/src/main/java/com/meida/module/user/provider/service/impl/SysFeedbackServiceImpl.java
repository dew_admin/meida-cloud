package com.meida.module.user.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.user.client.entity.SysFeedback;
import com.meida.module.user.provider.mapper.SysFeedbackMapper;
import com.meida.module.user.provider.service.SysFeedbackService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 反馈意见 实现类
 *
 * @author flyme
 * @date 2019-09-08
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysFeedbackServiceImpl extends BaseServiceImpl<SysFeedbackMapper, SysFeedback> implements SysFeedbackService {

    @Override
    public ResultBody beforePageList(CriteriaQuery<SysFeedback> cq, SysFeedback sysFeedback, EntityMap requestMap) {
        SysFeedback query = cq.getEntity(SysFeedback.class);
        cq.lambda()
                .like(FlymeUtils.isNotEmpty(query.getFeedContent()), SysFeedback::getFeedContent, query.getFeedContent())
                .like(FlymeUtils.isNotEmpty(query.getMobile()), SysFeedback::getMobile, query.getMobile())
                .eq(FlymeUtils.isNotEmpty(query.getFeedState()), SysFeedback::getFeedState, query.getFeedState());
        cq.eq("user.userNo", requestMap.get("userNo"));
        cq.eq("user.userType", requestMap.get("userType"));
        cq.select("user.nickName", "user.userName", "user.avatar", "user.userNo", "user.userType");
        cq.createJoin("com.meida.module.user.client.entity.AppUser");
        cq.orderByAsc("feedState");
        cq.orderByDesc("createTime");
        return ResultBody.ok();
    }


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysFeedback sysFeedback, EntityMap extra) {
        Long userId = OpenHelper.getUserId();
        sysFeedback.setUserId(userId);
        sysFeedback.setFeedState(CommonConstants.DISABLED);
        return ResultBody.ok();
    }

}
