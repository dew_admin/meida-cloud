package com.meida.module.user.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.enums.ColleconEnum;
import com.meida.common.mybatis.interceptor.GetInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.system.provider.service.SysColleconService;
import com.meida.module.user.client.entity.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 个人中心
 *
 * @author zyf
 */
@Component
public class BaseUserInfoHandler implements GetInterceptor {

    @Autowired
    private SysColleconService colleconService;

    @Override
    public void prepare(CriteriaQuery cq, EntityMap params) {
        cq.clear();
        Long otherUserId = params.getLong("userId");
        //如果没与传递userId则查询当前登录用户
        Long userId = OpenHelper.getUserId();
        params.put("userId", userId);
        cq.select(AppUser.class, "userId", "sex", "nickName", "avatar", "personProfile", "userNo", "shareCode", "tags", "motto", "userType", "frontCoverImage");
        //是否关注改用户
        cq.addSelect("collecon_tag(3,'AppUser'," + otherUserId + "," + userId + ") gzTag");
        cq.addSelect("collecon_count(1,'AppUser',userId) dzNum");
        cq.addSelect("collecon_count(2,'AppUser',userId) scNum");
        cq.addSelect("collecon_count(3,'AppUser',userId) fsNum");
        if (FlymeUtils.isNotEmpty(otherUserId)) {
            cq.eq(true, "user.userId", otherUserId);
        } else {
            cq.eq(true, "user.userId", userId);
        }


    }

    @Override
    public void complete(CriteriaQuery cq, EntityMap map) {
        Long userId = map.getLong("userId");
        //我关注的用户数量
        Long gzNum = colleconService.countCountByUser(userId, ColleconEnum.GZ, AppUser.class);
        map.put("gzNum", gzNum);
    }
}
