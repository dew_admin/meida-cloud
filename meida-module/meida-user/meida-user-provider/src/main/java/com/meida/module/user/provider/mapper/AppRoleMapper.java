package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppRole;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 用户角色 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-27
 */
public interface AppRoleMapper extends SuperMapper<AppRole> {

}
