package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppUser;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * @author zyf
 */
public interface AppUserMapper extends SuperMapper<AppUser> {

}
