package com.meida.module.user.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.module.user.client.entity.AppSchool;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 学校 接口
 *
 * @author flyme
 * @date 2021-08-07
 */
public interface AppSchoolService extends IBaseService<AppSchool> {
    /**
     * 根据状态查询学校列表
     *
     * @param cqc
     * @return
     */
    List<EntityMap> schoolList(CriteriaQueryCallBack cqc);

}
