package com.meida.module.user.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.ApiAssert;

import com.meida.module.user.client.entity.SysLink;
import com.meida.module.user.provider.mapper.SysLinkMapper;
import com.meida.module.user.provider.service.SysLinkService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 友情链接 实现类
 *
 * @author flyme
 * @date 2019-07-11
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysLinkServiceImpl extends BaseServiceImpl<SysLinkMapper, SysLink> implements SysLinkService {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectByCount(int count) {
        CriteriaQuery cq = new CriteriaQuery(SysLink.class);
        cq.eq("sendStatus", CommonConstants.ENABLED);
        cq.last("limit " + count);
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody setSendStatus(Long[] ids, Integer sendStatus) {
        ApiAssert.isNotEmpty("主键不能为空", ids);
        UpdateWrapper product = new UpdateWrapper();
        product.set("sendStatus", sendStatus);
        product.in("linkId", ids);
        boolean result = update(product);
        if (result) {
            return ResultBody.ok("操作成功", sendStatus);
        } else {
            return ResultBody.failed("操作失败");
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysLink> cq, SysLink sysLink, EntityMap requestMap) {
        cq.likeByField(SysLink.class, "linkName");
        return ResultBody.ok();
    }
}
