package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.user.client.entity.SysVersion;

/**
 * 版本控制 接口
 *
 * @author flyme
 * @date 2019-10-19
 */
public interface SysVersionService extends IBaseService<SysVersion> {

    /**
     * 查询版本
     *
     * @param verCode
     * @return
     */
    ResultBody getByVeCode(String verCode);

}
