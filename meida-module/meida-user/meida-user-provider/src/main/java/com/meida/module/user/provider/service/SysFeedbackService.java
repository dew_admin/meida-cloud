package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.user.client.entity.SysFeedback;

/**
 * 反馈意见 接口
 *
 * @author flyme
 * @date 2019-09-08
 */
public interface SysFeedbackService extends IBaseService<SysFeedback> {

}
