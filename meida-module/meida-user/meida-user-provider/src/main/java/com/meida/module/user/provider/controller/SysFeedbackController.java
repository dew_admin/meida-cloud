package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysFeedback;
import com.meida.module.user.provider.service.SysFeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 反馈意见控制器
 *
 * @author flyme
 * @date 2019-09-08
 */
@Api(tags = "意见反馈", value = "意见反馈")
@RestController
@RequestMapping("/feedback/")
public class SysFeedbackController extends BaseController<SysFeedbackService, SysFeedback> {


    @ApiOperation(value = "反馈意见-分页列表", notes = "反馈意见分页列表")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "反馈意见-列表", notes = "反馈意见列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "反馈意见-添加", notes = "添加反馈意见")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "反馈意见-更新", notes = "更新反馈意见")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }


    @ApiOperation(value = "反馈意见-删除", notes = "删除反馈意见")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "反馈意见-详情", notes = "反馈意见详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "反馈意见状态设置", notes = "反馈意见状态设置")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "feedState", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map params, @RequestParam(value = "feedState") Integer feedState) {
        return bizService.setState(params, "feedState", feedState);
    }
}
