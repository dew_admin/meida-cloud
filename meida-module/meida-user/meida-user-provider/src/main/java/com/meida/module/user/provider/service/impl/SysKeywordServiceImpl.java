package com.meida.module.user.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.user.provider.mapper.SysKeywordMapper;
import com.meida.module.user.client.entity.SysKeyword;
import com.meida.module.user.provider.service.SysKeywordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 关键词接口实现类
 *
 * @author flyme
 * @date 2020-03-22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysKeywordServiceImpl extends BaseServiceImpl<SysKeywordMapper, SysKeyword> implements SysKeywordService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, SysKeyword keyword, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<SysKeyword> cq, SysKeyword keyword, EntityMap requestMap) {
      cq.eq(SysKeyword.class,"groupType");
      cq.orderByDesc("keyword.createTime");
      return ResultBody.ok();
    }
}
