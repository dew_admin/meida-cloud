package com.meida.module.user.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.user.client.entity.SysVersion;
import com.meida.module.user.provider.service.SysVersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * APP版本升级
 *
 * @author flyme
 * @date 2019-10-19
 */
@RestController
@RequestMapping("/version/")
@Api(tags = "公共服务")
public class SysVersionController extends BaseController<SysVersionService, SysVersion> {

    @ApiOperation(value = "版本升级-分页列表", notes = "版本升级分页列表 ")
    @GetMapping(value = "page")
    @ApiIgnore
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "版本升级-添加", notes = "添加版本升级")
    @PostMapping(value = "save")
    @ApiIgnore
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "版本升级-更新", notes = "更新版本升级")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "版本升级-删除", notes = "删除版本升级")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "版本升级接口", notes = "版本升级接口")
    @GetMapping(value = "getByCode")
    public ResultBody get(@RequestParam(value = "verCode") String verCode) {
        return bizService.getByVeCode(verCode);
    }

    @ApiOperation(value = "详情接口", notes = "详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "强制更新状态设置", notes = "强制更新状态设置")
    @PostMapping(value = "setForceUpdate")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "forceUpdate", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setForceUpdate(@RequestParam(required = false) Map params, @RequestParam(value = "forceUpdate") Integer forceUpdate) {
        return bizService.setState(params, "forceUpdate", forceUpdate);
    }
}
