package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.user.client.entity.SysUserKeyword;

/**
 * 关键词 接口
 *
 * @author flyme
 * @date 2020-05-20
 */
public interface SysUserKeywordService extends IBaseService<SysUserKeyword> {
    ResultBody insert(String keyWord);
}
