package com.meida.module.user.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.user.client.entity.AppPosition;
import com.meida.module.user.provider.mapper.AppPositionMapper;
import com.meida.module.user.provider.service.AppPositionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 职位 实现类
 *
 * @author flyme
 * @date 2019-08-04
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AppPositionServiceImpl extends BaseServiceImpl<AppPositionMapper, AppPosition> implements AppPositionService {




    @Override
    public ResultBody beforeAdd(CriteriaSave cs, AppPosition appDept, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        cs.put("companyId", companyId);
        return ResultBody.ok();
    }


}
