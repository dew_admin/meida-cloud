package com.meida.module.user.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.user.client.entity.AppUser;
import org.springframework.stereotype.Component;

/**
 * 根据用户标签查询
 *
 * @author zyf
 */
@Component
public class SelectUserListByTagsHandler implements PageInterceptor<AppUser> {
    @Override
    public void prepare(CriteriaQuery<AppUser> cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        String keyword = params.get("keyword");
        if (FlymeUtils.isNotEmpty(keyword)) {
            cq.clear();
            cq.select(AppUser.class, "userId", "nickName", "avatar","userType", "userNo", "shareCode", "tags", "motto");
            //点赞标记
            cq.addSelect("collecon_tag(3,'AppUser',userId," + userId + ") gzTag");
            if (FlymeUtils.isNotEmpty(keyword)) {
                cq.and(e->e.like(true,"tags",keyword).or().like(true,"nickName",keyword));
            }
        }
    }

}
