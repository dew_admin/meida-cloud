package com.meida.module.user.provider.handler;

import com.meida.common.base.entity.EntityMap;

public interface BindMobileAfterHandler {

    /**
     * 绑定手机号之后返回扩展数据
     *
     * @param userId
     * @return
     */
    EntityMap bindSuccessAfter(Long userId);
}
