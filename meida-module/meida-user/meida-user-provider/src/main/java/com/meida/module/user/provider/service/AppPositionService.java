package com.meida.module.user.provider.service;

import com.meida.module.user.client.entity.AppPosition;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 职位 接口
 *
 * @author flyme
 * @date 2019-08-04
 */
public interface AppPositionService extends IBaseService<AppPosition> {

}
