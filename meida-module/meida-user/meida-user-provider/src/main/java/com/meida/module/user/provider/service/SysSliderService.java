package com.meida.module.user.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.callback.CriteriaQueryCallBack;
import com.meida.module.user.client.entity.SysSlider;

import java.util.List;

/**
 * 轮播图 接口
 *
 * @author flyme
 * @date 2019-07-05
 */
public interface SysSliderService extends IBaseService<SysSlider> {


    /**
     * 根据类型获取轮播图
     *
     * @param sliderType
     * @return
     */
    List<EntityMap> listBySliderType(int sliderType);

    /**
     * 根据类型获取轮播图
     *
     * @param sliderType
     * @return
     */
    List<EntityMap> listBySliderType(int sliderType, CriteriaQueryCallBack cqc);

    /**
     * 根据类型获取指定数量多张轮播图
     *
     * @param sliderType
     * @param count
     * @return
     */
    List<EntityMap> listBySliderType(int sliderType, int count);

    /**
     * 根据类型获取一张轮播图
     *
     * @param sliderType
     * @return
     */
    EntityMap getOneBySliderType(Integer sliderType);

    /**
     * 查找轮播图
     *
     * @param sliderType
     * @param moduleId
     * @return
     */
    List<EntityMap> listBySliderTypeAndModuleId(int sliderType, int moduleId);

}
