package com.meida.module.user.provider.mapper;

import com.meida.module.user.client.entity.AppSchool;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 学校 Mapper 接口
 * @author flyme
 * @date 2021-08-07
 */
public interface AppSchoolMapper extends SuperMapper<AppSchool> {

}
