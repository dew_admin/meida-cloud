/*
 Navicat Premium Data Transfer

 Source Server         : 8.131.232.142
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 8.131.232.142:3306
 Source Schema         : student_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 08/08/2021 11:27:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_educational
-- ----------------------------
DROP TABLE IF EXISTS `app_educational`;
CREATE TABLE `app_educational`  (
  `educationalId` bigint(20) NOT NULL COMMENT '主键',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父ID(审核前保留原教育经历,审核后可删除)',
  `schoolId` bigint(20) NULL DEFAULT NULL COMMENT '学校ID',
  `educationId` bigint(20) NULL DEFAULT NULL COMMENT '学历ID',
  `major` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '专业',
  `state` int(1) NULL DEFAULT NULL COMMENT '状态',
  `educationDate` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '教育时间',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`educationalId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教育经历' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
