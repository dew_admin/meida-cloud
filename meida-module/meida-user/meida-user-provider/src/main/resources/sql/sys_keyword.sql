/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:19:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_keyword
-- ----------------------------
DROP TABLE IF EXISTS `sys_keyword`;
CREATE TABLE `sys_keyword`  (
  `keywordId` bigint(20) NOT NULL,
  `content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `updateTime` datetime(0) NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`keywordId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '关键词' ROW_FORMAT = Dynamic;


INSERT INTO `base_menu` VALUES (405, 400, 'keyword', '关键词管理', '关键词管理', '/', 'system/keyword/index', 'bars', '_self', 'PageView', 1, 99, 1, 0, 'meida-base-provider', '2020-03-22 10:14:56', '2020-03-22 10:14:56');
INSERT INTO `base_authority` VALUES (405, 'MENU_keyword', 405, NULL, NULL, 1, '2020-03-22 10:14:56', '2020-03-22 10:14:56');

SET FOREIGN_KEY_CHECKS = 1;
