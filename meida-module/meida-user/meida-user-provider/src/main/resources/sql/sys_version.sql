/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:55:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_version
-- ----------------------------
DROP TABLE IF EXISTS `sys_version`;
CREATE TABLE `sys_version`  (
  `versionId` bigint(20) NOT NULL COMMENT '主键',
  `verName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本名称',
  `verNo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `verTitle` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本标题',
  `verContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新内容',
  `verUrl` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新地址',
  `verCode` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本类型（android，ios）',
  `forceUpdate` int(11) NULL DEFAULT NULL COMMENT '强制更新（1强制更新2非强制）',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`versionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '版本控制' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_version
-- ----------------------------
INSERT INTO `sys_version` VALUES (1, '安卓上线版本', '1.0.2', '', '1、商家审核通过之后在我的》商家信息查看商家管理后台地址。\n2、修复完善店铺信息选择营业时间文字描述不清楚\n3、优化网络请求', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/ada59f591f504e60846b0222384c5b2f.apk', 'android', 1, '2020-01-05 15:09:36', '2020-04-02 16:13:55');
INSERT INTO `sys_version` VALUES (2, '苹果上线版本', '1.2', '', '1、商家审核通过之后在我的》商家信息查看商家管理后台地址。\n2、修复完善店铺信息选择营业时间文字描述不清楚\n3、优化网络请求', 'https://itunes.apple.com/cn/app/id1488558137?l=zh&ls=1&mt=8', 'ios', 1, '2020-01-07 22:29:49', '2020-04-03 08:38:23');

INSERT INTO `base_menu` VALUES (170, 1, 'version', '版本更新', '', '/', 'system/version/index', 'cloud-download', '_self', 'PageView', 1, 14, 1, 0, 'meida-base-provider', '2020-01-01 17:29:35', '2020-01-04 17:30:04');
INSERT INTO `base_authority` VALUES (170, 'MENU_version', 170, NULL, NULL, 1, '2020-01-01 17:29:35', '2020-01-04 17:30:05');

SET FOREIGN_KEY_CHECKS = 1;
