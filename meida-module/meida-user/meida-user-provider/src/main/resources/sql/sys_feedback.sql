/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:05:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_feedback
-- ----------------------------
DROP TABLE IF EXISTS `sys_feedback`;
CREATE TABLE `sys_feedback`  (
  `feedBackId` bigint(20) NOT NULL COMMENT '主键',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `feedTitle` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `feedContent` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '反馈内容',
  `feedScore` int(10) NULL DEFAULT NULL COMMENT '评分',
  `feedType` int(11) NULL DEFAULT NULL COMMENT '反馈类型',
  `feedImages` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `feedState` int(11) NULL DEFAULT NULL COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`feedBackId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '意见反馈' ROW_FORMAT = Dynamic;


INSERT INTO `base_menu` VALUES (180, 1, 'feedBack', '意见反馈', '', '/', 'system/feedback/index', 'form', '_self', 'PageView', 1, 8, 1, 0, 'meida-base-provider', '2020-01-03 08:57:50', '2020-01-05 15:18:54');
INSERT INTO `base_authority` VALUES (180, 'MENU_feedBack', 180, NULL, NULL, 1, '2020-01-03 08:57:50', '2020-01-05 15:18:54');


SET FOREIGN_KEY_CHECKS = 1;
