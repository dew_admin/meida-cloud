/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 02/06/2020 17:32:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_slider
-- ----------------------------
DROP TABLE IF EXISTS `sys_slider`;
CREATE TABLE `sys_slider`  (
  `sliderId` bigint(20) NOT NULL COMMENT '主键',
  `sliderTitle` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `sliderImg` varchar(1280) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `linkUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `jumpType` int(11) NULL DEFAULT NULL COMMENT '跳转方式 (1:外部链接,2:内部模块跳转,3:文本跳转)',
  `isShow` int(11) NULL DEFAULT 0 COMMENT '是否展示',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `moduleType` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内部跳转模块',
  `sendStatus` int(11) NULL DEFAULT 0 COMMENT '发布状态 0:未发布 1:已发布',
  `sliderType` int(11) NULL DEFAULT NULL COMMENT '轮播图类型 1app,2pc,3APP启动图',
  `moduleId` bigint(20) NULL DEFAULT NULL COMMENT '模块id',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`sliderId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '轮播图' ROW_FORMAT = Dynamic;

INSERT INTO `base_menu` VALUES (400, 0, 'content', '内容管理', '内容管理', '/', '', 'html5', '_self', 'PageView', 1, 8, 1, 0, 'meida-base-provider', '2018-07-29 21:20:10', '2020-01-04 17:59:03');
INSERT INTO `base_authority` VALUES (400, 'MENU_content', 400, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-04 17:59:03');
INSERT INTO `base_menu` VALUES (401, 400, 'slider', '轮播图管理', '轮播图管理', '/', 'content/slider/index', 'picture', '_self', 'PageView', 1, 1, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-01-04 17:21:55');
INSERT INTO `base_authority` VALUES (401, 'MENU_slider', 401, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-04 17:21:55');

SET FOREIGN_KEY_CHECKS = 1;
