/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 17:30:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_dept
-- ----------------------------
DROP TABLE IF EXISTS `app_dept`;
CREATE TABLE `app_dept`  (
  `deptId` bigint(20) NOT NULL COMMENT '主键',
  `deptName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父ID',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序',
  `headerId` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部分负责人',
  `isParent` int(11) NULL DEFAULT NULL COMMENT '是否是父节点',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改日期',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`deptId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
