/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 10:02:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_html
-- ----------------------------
DROP TABLE IF EXISTS `sys_html`;
CREATE TABLE `sys_html`  (
  `htmlId` bigint(20) NOT NULL COMMENT '主键',
  `htmlTitle` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父ID',
  `htmlCode` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '编码',
  `htmlImage` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `htmlType` int(11) NULL DEFAULT NULL COMMENT '类型',
  `htmlContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容1:外部链接,2:内部模块跳转,3:内部H5跳转',
  `moduleName` int(11) NULL DEFAULT NULL COMMENT '内部跳转模块名称（自定义模块名称）',
  `isParent` int(11) NULL DEFAULT NULL COMMENT '是否父节点',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序',
  `htmlStatus` int(11) NULL DEFAULT NULL COMMENT '状态0：未发布1：已发布',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`htmlId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '服务协议' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_html
-- ----------------------------
INSERT INTO `sys_html` VALUES (1213725056912834561, '会员注册协议', 0, 'reg', NULL, 1, '', NULL, NULL, NULL, NULL, '2020-01-05 15:32:58', '2020-03-11 22:05:07');
INSERT INTO `sys_html` VALUES (1213725654918361089, '关于我们', 0, 'about', NULL, 1, '<p>关于我们</p>', NULL, NULL, NULL, NULL, '2020-01-05 15:35:20', '2020-01-05 15:56:20');
INSERT INTO `sys_html` VALUES (1213731392759787522, '帮助中心', 0, 'help', NULL, 1, '', NULL, NULL, NULL, NULL, '2020-01-05 15:58:08', '2020-03-26 10:20:25');
INSERT INTO `sys_html` VALUES (1213731697475973122, '用户注册及隐私保护协议', 0, 'yinsi', NULL, 1, '', NULL, NULL, NULL, NULL, '2020-01-05 15:59:21', '2020-03-11 11:32:12');
INSERT INTO `sys_html` VALUES (1213731988111880194, '商家服务协议', 0, 'shop', NULL, 1, '', NULL, NULL, NULL, NULL, '2020-01-05 16:00:30', '2020-03-26 10:18:49');


INSERT INTO `base_menu` VALUES (403, 400, 'service', '协议条款', '服务协议,会员条款等信息', '/', 'content/service/index', 'file-word', '_self', 'PageView', 1, 6, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-02-15 09:55:10');
INSERT INTO `base_authority` VALUES (403, 'MENU_service', 403, NULL, NULL, 1, NULL, '2020-02-15 09:55:10');

SET FOREIGN_KEY_CHECKS = 1;
