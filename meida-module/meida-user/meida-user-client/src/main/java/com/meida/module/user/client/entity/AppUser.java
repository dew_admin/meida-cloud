package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * APP用户-基础信息
 *
 * @author zyf
 */
@Data
@NoArgsConstructor
@TableName("app_user")
@TableAlias("user")
@ApiModel(value = "AppUser", description = "用户信息描述")
public class AppUser extends AbstractEntity {
    private static final long serialVersionUID = -735161640894047414L;

    @ApiModelProperty(hidden = true)
    @TableId(value = "userId", type = IdType.ASSIGN_ID)
    private Long userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty(hidden = true, value = "用户类型")
    private String userType;

    @ApiModelProperty(hidden = true, value = "公司")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long companyId;

    @ApiModelProperty(value = "角色")
    private Long roleId;

    @ApiModelProperty(value = "编号")
    private String userNo;

    @ApiModelProperty(value = "部门")
    private Long deptId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "支付密码")
    private String payPwd;

    @ApiModelProperty("区")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long areaId;

    @ApiModelProperty("市")
    private Long cityId;

    @ApiModelProperty("省")
    private Long proId;

    @ApiModelProperty("学校ID")
    private String schoolId;

    @ApiModelProperty("是否认证学校")
    private Integer schoolAuth;

    @ApiModelProperty("背景图")
    private String frontCoverImage;

    @ApiModelProperty("区域名称")
    private String areaName;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("国际区号")
    private String areaCode;

    @ApiModelProperty("生日")
    private String birthday;

    @ApiModelProperty("职位")
    private String position;

    @ApiModelProperty("职位ID")
    private Long positionId;

    @ApiModelProperty(hidden = true, name = "注册IP")
    private String registerIp;

    @ApiModelProperty(hidden = true, value = "状态", notes = "0-禁用 1-启用 2-锁定")
    private Integer status;

    @ApiModelProperty("推荐人")
    private Long inviterId;

    @ApiModelProperty("邀请码")
    private String shareCode;

    @ApiModelProperty("用户即时通讯token")
    private String userToken;

    @ApiModelProperty("微信号")
    private String wechatNo;

    @ApiModelProperty("个人简介")
    private String personProfile;

    @ApiModelProperty("个性签名")
    private String motto;

    @ApiModelProperty("标签")
    private String tags;

    @ApiModelProperty("业务数量")
    private Integer busNum;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("推送系统通知")
    private Integer pushSysMsg;

    @ApiModelProperty("推送订单通知")
    private Integer pushOrderMsg;

    @ApiModelProperty("推送其他通知")
    private Integer pushOtherMsg;


}
