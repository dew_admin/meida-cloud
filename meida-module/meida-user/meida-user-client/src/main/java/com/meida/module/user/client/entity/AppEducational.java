package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 教育经历
 *
 * @author flyme
 * @date 2021-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("app_educational")
@TableAlias("educational")
@ApiModel(value = "AppEducational对象", description = "教育经历")
public class AppEducational extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "educationalId", type = IdType.ASSIGN_ID)
    private Long educationalId;

    @ApiModelProperty(value = "父ID")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long parentId;

    @ApiModelProperty(value = "学校ID")
    private Long schoolId;

    @ApiModelProperty(value = "学历ID")
    private Long educationId;

    @ApiModelProperty(value = "专业")
    private String major;

    @ApiModelProperty(value = "审核状态")
    private Integer state;

    @ApiModelProperty(value = "显示状态")
    private Integer showState;

    @ApiModelProperty(value = "教育时间")
    private String educationDate;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

}
