package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * APP用户-登录账号
 */
@Data
@TableName("app_account")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@TableAlias("account")
public class AppAccount extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -4484479600033295192L;

    @TableId(value = "accountId", type = IdType.ASSIGN_ID)
    private Long accountId;

    /**
     * 系统用户Id
     */
    private Long userId;

    /**
     * 标识：手机号、邮箱、 系统用户名、或第三方应用的唯一标识
     */
    private String account;

    /**
     * 密码凭证：站内的保存密码、站外的不保存或保存token）
     */
    private String password;

    /**
     * 登录类型:password-密码、mobile-手机号、email-邮箱、weixin-微信、weibo-微博、qq-等等
     */
    private String accountType;

    /**
     * 第三方登录昵称
     */
    private String nickName;
    /**
     * 第三方登录头像
     */
    private String avatar;

    /**
     * 账户域
     */
    private String domain;

    @TableField(exist = false)
    private Long companyId;


    public AppAccount(String account, String password, String accountType, String avatar, String nickName) {
        this.account = account;
        this.password = password;
        this.accountType = accountType;
        this.avatar = avatar;
        this.nickName = nickName;
    }

    public AppAccount(Long userId, String account, String password, String accountType, String avatar, String nickName) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.accountType = accountType;
        this.avatar = avatar;
        this.nickName = nickName;
    }

    public AppAccount(Long userId, String account, String password, String accountType) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.accountType = accountType;
    }

}
