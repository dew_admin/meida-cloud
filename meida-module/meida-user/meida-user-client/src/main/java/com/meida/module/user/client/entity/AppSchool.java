package com.meida.module.user.client.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 学校
 *
 * @author flyme
 * @date 2021-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("app_school")
@TableAlias("school")
@ApiModel(value="AppSchool对象", description="学校")
public class AppSchool extends AbstractEntity {

private static final long serialVersionUID=1L;

    @Excel(name = "id", fixedIndex = 0)
    @ApiModelProperty(value = "主键")
    @TableId(value = "schoolId", type = IdType.ASSIGN_ID)
    private Long schoolId;

    @Excel(name = "学校名称", fixedIndex = 0)
    @ApiModelProperty(value = "学校名称")
    private String schoolName;

    @ApiModelProperty(value = "学校Logo")
    private String schoolLogo;

    @ApiModelProperty(value = "英文缩写")
    private String schoolAbridge;

    @ApiModelProperty(value = "状态")
    private Integer state;

    @ApiModelProperty(value = "全拼")
    private String fullLetter;

    @ApiModelProperty(value = "首字母")
    private String firstLetter;

    @ApiModelProperty(value = "邮箱后缀")
    private String emailSuffix;

}
