package com.meida.module.user.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "RegisterParams", description = "注册参数描述")
@Data
public class RegisterParams implements Serializable {

    @ApiModelProperty(value = "账户名", required = true)
    private String accountName;

    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty(value = "验证码", required = true)
    private String smsCode;
}
