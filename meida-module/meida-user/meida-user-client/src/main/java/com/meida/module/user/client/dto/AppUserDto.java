package com.meida.module.user.client.dto;


import com.meida.module.user.client.entity.AppUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author: zyf
 * @date: 2018/11/12 11:35
 * @description:
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class AppUserDto extends AppUser implements Serializable {
    private static final long serialVersionUID = 6717800085953996702L;
    private Long accountId;
    private String account;
}
