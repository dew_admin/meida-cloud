package com.meida.module.user.client.constants;

/**
 * APP接口服务常量
 *
 * @author zyf
 */
public class AppConstants {

    /**
     * 服务名称
     */
    public static final String APP_SERVICE = "meida-app-server";

    /**
     * 自定义第三方登录请求头
     */
    public static final String HEADER_X_THIRDPARTY_LOGIN = "X-ThirdParty-Login";

    /**
     * 默认接口分类
     */
    public final static String DEFAULT_API_CATEGORY = "default";

    /**
     * 默认注册密码
     */
    public final static String DEF_PWD = "123456";
    /**
     * 状态:0-无效 1-有效
     */
    public final static int ENABLED = 1;
    public final static int DISABLED = 0;

    /**
     * 系统用户状态
     * 0:禁用、1:正常、2:锁定
     */
    public final static int USER_STATE_DISABLE = 0;
    public final static int USER_STATE_NORMAL = 1;
    public final static int USER_STATE_LOCKED = 2;

    /**
     * 系统用户类型:platform-平台、isp-服务提供商、dev-自研开发者
     */
    public final static String USER_TYPE_PLATFORM = "platform";
    public final static String USER_TYPE_ISP = "isp";
    public final static String USER_TYPE_DEVELOPER = "dev";


    /**
     * 应用类型
     */
    public final static String APP_TYPE_SERVER = "server";
    public final static String APP_TYPE_APP = "app";
    public final static String APP_TYPE_PC = "pc";
    public final static String APP_TYPE_WAP = "wap";

    /**
     * 操作系统
     */
    public final static String APP_IOS = "ios";
    public final static String APP_ANDROID = "android";


}
