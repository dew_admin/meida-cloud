package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 职位
 *
 * @author flyme
 * @date 2019-08-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("app_position")
@TableAlias("position")
@ApiModel(value = "AppPosition对象", description = "职位")
public class AppPosition extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "positionId", type = IdType.ASSIGN_ID)
    private Long positionId;

    @ApiModelProperty(value = "职位名称")
    private String positionName;

    @ApiModelProperty(value = "企业ID")
    private String companyId;

    @ApiModelProperty(value = "职位编号")
    private String positionNo;

    @ApiModelProperty(value = "排序")
    private String sortOrder;

}
