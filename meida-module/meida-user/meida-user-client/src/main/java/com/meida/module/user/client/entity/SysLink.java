package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.enums.StateEnum;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 友情链接
 *
 * @author flyme
 * @date 2019-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_link")
@TableAlias("link")
@ApiModel(value = "SysLink对象", description = "友情链接")
public class SysLink extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "linkId", type = IdType.ASSIGN_ID)
    private Long linkId;

    @ApiModelProperty(value = "链接名称")
    private String linkName;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

    @ApiModelProperty(value = "logo")
    private String linkLogo;

    @ApiModelProperty(value = "url前缀")
    private String linkPrefix;

    @ApiModelProperty(value = "状态")
    private StateEnum sendStatus;


}
