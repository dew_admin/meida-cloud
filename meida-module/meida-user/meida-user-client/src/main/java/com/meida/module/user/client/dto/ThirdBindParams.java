package com.meida.module.user.client.dto;

import com.meida.common.enums.ThirdAccountEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "ThirdBindParams", description = "第三方账户绑定参数描述")
public class ThirdBindParams implements Serializable {
    @ApiModelProperty("账户名,APP授权后获取到的第三方唯一标识")
    private String accountName;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("账户类型")
    private ThirdAccountEnum accountType;
}
