package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 服务协议
 *
 * @author flyme
 * @date 2019-07-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_html")
@TableAlias("html")
@ApiModel(value = "SysHtml对象", description = "服务协议")
public class SysHtml extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "htmlId", type = IdType.ASSIGN_ID)
    private Long htmlId;

    @ApiModelProperty(value = "标题")
    private String htmlTitle;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "编码")
    private String htmlCode;

    @ApiModelProperty(value = "图片")
    private String htmlImage;

    @ApiModelProperty(value = "类型1:首页，2：其他")
    private Integer htmlType;

    @ApiModelProperty(value = "分组")
    private Integer htmlGroup;

    @ApiModelProperty(value = "内容")
    private String htmlContent;

    @ApiModelProperty(value = "1:外部链接,2:内部模块跳转,3:内部H5跳转")
    private Integer jumpType;

    @ApiModelProperty(value = "内部跳转模块名称（自定义模块名称）")
    private Integer moduleName;

    @ApiModelProperty(value = "是否父节点")
    private Integer isParent;

    @ApiModelProperty(value = "排序")
    private Integer sortOrder;

    @ApiModelProperty(value = "状态0：未发布1：已发布")
    private Integer htmlStatus;

}
