package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 关键词
 *
 * @author flyme
 * @date 2020-05-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user_keyword")
@TableAlias("suk")
@ApiModel(value="SysUserKeyword对象", description="关键词")
public class SysUserKeyword extends AbstractEntity {

private static final long serialVersionUID=1L;

    @TableId(value = "userKeywordId", type = IdType.ASSIGN_ID)
    private Long userKeywordId;

    @ApiModelProperty(value = "关键词")
    private String content;

    private Long userId;

}
