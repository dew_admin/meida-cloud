package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 反馈意见
 *
 * @author flyme
 * @date 2019-09-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_feedback")
@TableAlias("feedback")
@ApiModel(value = "意见反馈对象", description = "反馈意见")
public class SysFeedback extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "feedBackId", type = IdType.ASSIGN_ID)
    private Long feedBackId;

    @ApiModelProperty(value = "反馈用户")
    private Long userId;

    @ApiModelProperty(value = "手机")
    private String mobile;

    @ApiModelProperty(value = "反馈标题")
    private String feedTitle;

    @ApiModelProperty(value = "反馈内容")
    private String feedContent;

    @ApiModelProperty(value = "评分")
    private Integer feedScore;

    @ApiModelProperty(value = "目标ID")
    private Long targetId;

    @ApiModelProperty(value = "反馈类型")
    private String feedType;

    @ApiModelProperty(value = "标签")
    private String feedTags;

    @ApiModelProperty(value = "图片")
    private String feedImages;

    @ApiModelProperty(value = "处理状态  0:未处理 1:已处理")
    private Integer feedState;

}
