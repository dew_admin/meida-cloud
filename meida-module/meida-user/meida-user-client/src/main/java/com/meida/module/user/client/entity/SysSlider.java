package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 轮播图
 *
 * @author flyme
 * @date 2019-07-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_slider")
@TableAlias("slider")
@ApiModel(value = "SysSlider对象", description = "轮播图")
public class SysSlider extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "sliderId", type = IdType.ASSIGN_ID)
    private Long sliderId;

    @ApiModelProperty(value = "标题")
    private String sliderTitle;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "图片")
    private String sliderImg;

    @ApiModelProperty(value = "创建人")
    private Long userId;

    @ApiModelProperty(value = "跳转方式 (1:外部链接,2:内部模块跳转,3:文本跳转)")
    private Integer jumpType;


    @ApiModelProperty(value = "跳转地址")
    private String linkUrl;

    @ApiModelProperty(value = "是否展示")
    private Integer isShow;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

    @ApiModelProperty(value = "内部跳转模块")
    private String moduleType;

    @ApiModelProperty(value = "发布状态 0:未发布 1:已发布")
    private Integer sendStatus;

    @ApiModelProperty(value = "轮播图类型 1app,2pc,3APP启动图")
    private Integer sliderType;

    @ApiModelProperty(value = "模块id")
    private Long moduleId;

}
