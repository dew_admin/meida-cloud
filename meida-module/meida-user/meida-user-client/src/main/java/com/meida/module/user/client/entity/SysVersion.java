package com.meida.module.user.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 版本控制
 *
 * @author flyme
 * @date 2019-10-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_version")
@TableAlias("version")
@ApiModel(value="SysVersion对象", description="版本控制")
public class SysVersion extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "versionId", type = IdType.ASSIGN_ID)
    private Long versionId;

    @ApiModelProperty(value = "版本名称")
    private String verName;

    @ApiModelProperty(value = "版本号")
    private String verNo;

    @ApiModelProperty(value = "版本标题")
    private String verTitle;

    @ApiModelProperty(value = "更新内容")
    private String verContent;

    @ApiModelProperty(value = "更新地址")
    private String verUrl;

    @ApiModelProperty(value = "版本类型（android，ios）")
    private String verCode;

    @ApiModelProperty(value = "强制更新（1强制更新2非强制）")
    private Integer forceUpdate;

}
