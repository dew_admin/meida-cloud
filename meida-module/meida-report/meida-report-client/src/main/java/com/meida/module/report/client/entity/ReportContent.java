package com.meida.module.report.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 报表设计
 *
 * @author flyme
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("report_content")
@TableAlias("report")
@ApiModel(value = "报表设计", description = "报表设计")
public class ReportContent extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "reportId", type = IdType.ASSIGN_ID)
    private Long reportId;

    @ApiModelProperty(value = "报表名称")
    private String reportName;

    @ApiModelProperty(value = "报表设计内容")
    private String reportContent;

    @ApiModelProperty(value = "报表路径")
    private String reportPath;


}
