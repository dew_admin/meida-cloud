package com.meida.module.report.provider.configuration;

import com.bstek.ureport.console.UReportServlet;
import com.bstek.ureport.definition.datasource.BuildinDatasource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author zyf
 */
@ImportResource("classpath:ureport-console-context.xml")
@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties(value = UReportConfig.class)
public class ReportServletConfig implements BuildinDatasource {
    @Resource
    private DataSource dataSource;
    private Logger log = LoggerFactory.getLogger(getClass());

    @Bean
    public ServletRegistrationBean ureportServlet() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new UReportServlet());
        servletRegistrationBean.addUrlMappings("/ureport/*");
        return servletRegistrationBean;
    }


    @Override
    public String name() {
        return "localhost";
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            log.error("UReport数据源获取连接失败！");
            e.printStackTrace();
        }
        return null;
    }

}
