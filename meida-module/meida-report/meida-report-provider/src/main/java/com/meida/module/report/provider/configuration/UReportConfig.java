package com.meida.module.report.provider.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义存储器的配置类
 *
 * @author zyf
 **/
@Data
@ConfigurationProperties(prefix = "ureport")
public class UReportConfig {
    /**
     * 文件名前缀，用于标识不同的存储器存储的文件
     * 否则在查看文件列表的时候只能看到其中一个存储器的文件
     */
    private String metaPrefix = "meta:";
    private String fullPrefix = "full:";

    /**
     * 是否禁用DBMetaReportProvider
     */
    private Boolean disableDBMetaProvider = false;

    /**
     * 是否禁用DBFullReportProvider
     */
    private Boolean disableDBFullProvider = false;

    /**
     * 是否禁用FileReportProvider
     */
    private Boolean disableFileProvider = false;

    private Boolean disableHttpSessionReportCache = false;

    /**
     * 报表模板文件存储路径
     */
    private String fileStoreDir = "ureportfiles";
    /**
     * 是否调试模式
     */
    private Boolean debug = true;
}