package com.meida.module.report.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.report.client.entity.ReportContent;


/**
 *
 * @author flyme
 */
public interface ReportContentMapper extends SuperMapper<ReportContent> {

}
