package com.meida.module.report.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.report.client.entity.ReportContent;

/**
 * 报表设计
 *
 * @author flyme
 */
public interface ReportContentService extends IBaseService<ReportContent> {
    /**
     * 按文件名称查找报表模板内容
     *
     * @param fileName 文件名称
     * @return 报表模板内容
     */
    ReportContent findByFileName(String fileName);

    /**
     * 按文件名称删除报表模板
     *
     * @param fileName 文件名称
     */
    boolean deleteByFileName(String fileName);
}
