package com.meida.module.report.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.report.client.entity.ReportContent;
import com.meida.module.report.provider.service.ReportContentService;
import com.meida.module.report.provider.mapper.ReportContentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zyf
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ReportContentServiceImpl extends BaseServiceImpl<ReportContentMapper, ReportContent> implements ReportContentService {
    @Override
    public ReportContent findByFileName(String fileName) {
        CriteriaQuery cq = new CriteriaQuery(ReportContent.class);
        cq.eq("reportName", fileName);
        return getOne(cq);
    }

    @Override
    public boolean deleteByFileName(String fileName) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq("fileName", fileName);
        return remove(cd);
    }
}
