package com.meida.module.report.provider.report;

import com.bstek.ureport.exception.ReportException;
import com.bstek.ureport.provider.report.ReportFile;
import com.bstek.ureport.provider.report.ReportProvider;
import com.meida.module.report.client.entity.ReportContent;
import com.meida.module.report.provider.configuration.UReportConfig;
import com.meida.module.report.provider.service.ReportContentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数据库报表存储器
 * - 所有的数据内容都存储到数据库，包括文件名及文件内容等
 **/
@Slf4j
@Component
public class MySQLProvider implements ReportProvider {

    private static final String NAME = "数据库存储";
    private static final String ENCODING = "UTF-8";

    @Resource
    private UReportConfig uReportConfig;
    @Resource
    private ReportContentService reportContentService;


    @Override
    public InputStream loadReport(String fileName) {
        log.info("load report file content by fileName: {}", fileName);
        ReportContent reportContent = reportContentService.findByFileName(substring(fileName));

        try {
            return IOUtils.toInputStream(reportContent.getReportContent(), ENCODING);
        } catch (IOException e) {
            throw new ReportException(e);
        }
    }

    @Override
    public void deleteReport(String fileName) {
        boolean result = reportContentService.deleteByFileName(substring(fileName));
        log.info("delete report file {} by fileName: {}", result ? "success" : "fail", fileName);
    }

    @Override
    public List<ReportFile> getReportFiles() {
        List<ReportContent> contents = reportContentService.list();
        log.info("报表文件数量: {}", contents.size());

        return contents.stream()
                .map(this::toReportFile)
                .sorted((f1, f2) -> f2.getUpdateDate().compareTo(f1.getUpdateDate()))
                .collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void saveReport(String fileName, String content) {
        ReportContent reportContent = new ReportContent();
        reportContent.setReportContent(content);
        reportContent.setReportName(substring(fileName));
        reportContentService.save(reportContent);
        log.info("save report file content success by fileName: {}", fileName);
    }

    @Override
    public boolean disabled() {
        return uReportConfig.getDisableDBFullProvider();
    }

    @Override
    public String getPrefix() {
        return uReportConfig.getFullPrefix();
    }

    /**
     * 去掉前缀
     */
    private String substring(String fileName) {
        String prefix = uReportConfig.getFullPrefix();
        if (fileName.startsWith(prefix)) {
            return fileName.substring(prefix.length());
        }
        return fileName;
    }

    private ReportFile toReportFile(ReportContent content) {
        return new ReportFile(content.getReportName(), content.getUpdateTime());
    }
}