package com.meida.module.report.provider.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/report/")
public class ReportController {


    /**
     * table列表
     *
     * @return
     */
    @GetMapping("/index")
    public String table() {
        return "index2";
    }

    /**
     * 生成配置
     *
     * @return
     */
    @GetMapping("/build")
    public String build() {
        return "build2";
    }

    /**
     * 老的登录获取用户信息接口
     *
     * @return
     */
    @GetMapping(value = "/china")
    @ResponseBody
    public String china() {
        return readJson("china.json");
    }

    /**
     * 读取json格式文件
     *
     * @param jsonSrc
     * @return
     */
    private String readJson(String jsonFile) {
        String json = "";
        try {
            //File jsonFile = ResourceUtils.getFile(jsonSrc);
            //json = FileUtils.re.readFileToString(jsonFile);
            //换个写法，解决springboot读取jar包中文件的问题
            String fileName = "classpath:/mock/"+jsonFile;
            ResourceLoader resourceLoader = new DefaultResourceLoader();
            InputStream inputStream = resourceLoader.getResource(fileName).getInputStream();
            json = IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {

        }
        return json;
    }


}
