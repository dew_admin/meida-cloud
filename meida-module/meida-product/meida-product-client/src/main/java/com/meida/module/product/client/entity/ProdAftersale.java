package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 售后保障
 *
 * @author flyme
 * @date 2020-01-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_aftersale")
@TableAlias("aftersale")
@ApiModel(value="ProdAftersale对象", description="售后保障")
public class ProdAftersale extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "afterSaleId", type = IdType.ASSIGN_ID)
    private Long afterSaleId;

    @ApiModelProperty(value = "保障标题")
    private String afterSaleTitle;

    @ApiModelProperty(value = "保障内容")
    private String afterSaleContent;

    @ApiModelProperty(value = "保障logo")
    private String afterSaleLogo;

    @ApiModelProperty(value = "所属企业")
    private Long companyId;

    @ApiModelProperty(value = "是否启用")
    private Integer afterSaleState;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

}
