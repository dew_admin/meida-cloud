package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品
 *
 * @author flyme
 * @date 2019-07-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_product")
@TableAlias("product")
@ApiModel(value = "ProdProduct对象", description = "产品")
public class ProdProduct extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品ID")
    @TableId(value = "productId", type = IdType.ASSIGN_ID)
    private Long productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "商品系列")
    private String productSeries;

    @ApiModelProperty(value = "产品现价")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "产品原价")
    private BigDecimal oldPrice;

    @ApiModelProperty(value = "产品编号")
    private String productNo;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "产品类型")
    private Integer prodcutType;

    @ApiModelProperty(value = "规格展现方式1:图文2文字")
    private Integer skuShowType;

    @ApiModelProperty(value = "企业Id")
    private Long companyId;

    @ApiModelProperty(value = "店铺Id")
    private Long shopId;

    @ApiModelProperty(value = "商品一级分类")
    private Long categoryId1;

    @ApiModelProperty(value = "商品二级分类")
    private Long categoryId2;

    @ApiModelProperty(value = "商品三级分类")
    private Long categoryId3;

    @ApiModelProperty(value = "分类全称")
    private  String categoryName;


    @ApiModelProperty(value = "店铺产品分类")
    private Long groupId;

    @ApiModelProperty(value = "品牌")
    private Long brandId;

    @ApiModelProperty(value = "商品产地")
    private Long place;

    @ApiModelProperty(value = "销量")
    private Integer saleCount;

    @ApiModelProperty(value = "评分")
    private BigDecimal productScore;


    @ApiModelProperty(value = "发货期")
    private Integer deliveryTime;

    @ApiModelProperty(value = "可见类型 1_公开,2_部分可见,3_不给谁看")
    private Integer visibleType;

    @ApiModelProperty(value = "可见配置")
    private String visibleConfig;

    @ApiModelProperty(value = "产品状态 0_未上架(仓库中),1_已上架,2_已下架,3_违规下架")
    private Integer onLineState;

    @ApiModelProperty(value = "上架时间")
    private Date onlineDate;

    @ApiModelProperty(value = "审核状态 0_未审核，1_已审核，2_审核失败")
    private Integer auditState;

    @ApiModelProperty(value = "是否推荐")
    private Integer recommend;

    @ApiModelProperty(value = "是否删除")

    private Integer deleted;

    /**
     * 运费
     */
    private BigDecimal freight;


}
