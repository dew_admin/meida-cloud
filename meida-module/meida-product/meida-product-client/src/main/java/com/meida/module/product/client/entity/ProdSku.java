package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8
 *
 * @author flyme
 * @date 2019-07-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_sku")
@TableAlias("sku")
@ApiModel(value = "ProdSku对象", description = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8")
public class ProdSku extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品规格ID")
    @TableId(value = "skuId", type = IdType.AUTO)
    private Long skuId;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "商品标题")
    private String title;

    @ApiModelProperty(value = "规格名称")
    private String skuTitle;

    @ApiModelProperty(value = "规格编号")
    private String productNo;

    @ApiModelProperty(value = "商品数量")
    private Integer num;

    @ApiModelProperty(value = "秒杀库存")
    private Integer seckillNum;

    @ApiModelProperty(value = "sku关联图片")
    @TableField(exist = false)
    private Long[] imageIds;


    @ApiModelProperty(value = "sku封面图")
    private String images;

    @ApiModelProperty(value = "销售价格，单位为分")
    private BigDecimal price;


    @ApiModelProperty(value = "运费")
    private BigDecimal freight;

    @ApiModelProperty(value = "特有规格属性在spu属性模板中的对应下标组合")
    private String indexes;

    @ApiModelProperty(value = "sku的特有规格参数键值对，json格式，反序列化时请使用linkedHashMap，保证有序")
    private String ownSpec;

    @ApiModelProperty(value = "是否有效，0无效，1有效")
    private Integer enable;

}
