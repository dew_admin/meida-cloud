package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 品牌
 *
 * @author flyme
 * @date 2019-07-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_brand")
@TableAlias("brand")
@ApiModel(value = "ProdBrand对象", description = "品牌")
public class ProdBrand extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "品牌ID")
    @TableId(value = "brandId", type = IdType.ASSIGN_ID)
    private Long brandId;

    @ApiModelProperty(value = "品牌名称")
    private String brandName;

    @ApiModelProperty(value = "品牌logo")
    private String brandLogo;

    @ApiModelProperty(value = "网址前缀")
    private String urlPrefix;
    @ApiModelProperty(value = "品牌网站")
    private String brandSite;

    @ApiModelProperty(value = "所属分类")
    private Long categoryId;

    @ApiModelProperty(value = "所属二级分类")
    private Long categoryId2;

    @ApiModelProperty(value = "所属企业")
    private Long companyId;

    @ApiModelProperty(value = "是否推荐")
    private Integer recommend;

    @ApiModelProperty(value = "品牌简介")
    private String brandDesc;

}
