package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

import java.math.BigDecimal;

/**
 * 店铺分类表
 *
 * @author flyme
 * @date 2019-11-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_shop_type")
@TableAlias("shopType")
@ApiModel(value = "ProdShopType对象", description = "店铺分类表")
public class ProdShopType extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "typeId", type = IdType.ASSIGN_ID)
    private Long typeId;

    @ApiModelProperty(value = "分类编码")
    private String typeCode;

    @ApiModelProperty(value = "分类名称")
    private String typeName;

    @ApiModelProperty(value = "分类logo")
    private String typeLogo;

    @ApiModelProperty(value = "父Id")
    private Long parentId;

    @ApiModelProperty(value = "费率")
    private BigDecimal typeRate;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

    @ApiModelProperty(value = "分组ID")
    private Long groupId;

    @ApiModelProperty(value = "状态")
    private Integer state;

}
