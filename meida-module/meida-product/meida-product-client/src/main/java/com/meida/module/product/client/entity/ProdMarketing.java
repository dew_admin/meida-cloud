package com.meida.module.product.client.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 商品营销
 *
 * @author flyme
 * @date 2019-12-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_marketing")
@TableAlias("marketing")
@ApiModel(value = "商品营销对象", description = "商品营销")
public class ProdMarketing extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "marketingId", type = IdType.ASSIGN_ID)
    private Long marketingId;

    @ApiModelProperty(value = "所属活动ID")
    private Long topicId;

    @ApiModelProperty(value = "商品Id")
    private Long productId;

    @ApiModelProperty(value = "企业Id")
    private Long companyId;

    @ApiModelProperty(value = "店铺Id")
    private Long shopId;

    @ApiModelProperty(value = "营销方式1:限时抢购 2:限时秒杀")
    private Integer marketingType;

    @ApiModelProperty(value = "活动开始日期")
    private Date beginTime;

    @ApiModelProperty(value = "活动结束日期")
    private Date endTime;

    @ApiModelProperty(value = "活动价格")
    private BigDecimal marketingPrice;

    @ApiModelProperty(value = "商品原价")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "商品标题")
    private String productTitle;

    @ApiModelProperty(value = "商品图片")
    private String productImage;

    @ApiModelProperty(value = "参与活动的规格ID,多个逗号隔开")
    private String skuIds;


    @ApiModelProperty(value = "参与活动的规格JSON参数")
    private String privateSpce;

    @ApiModelProperty(value = "发布状态")
    private Integer marketingState;



    @ApiModelProperty(value = "备注")
    private String marketingDesc;

    @ApiModelProperty(value = "已售数量")
    private Integer soldNum;

    @ApiModelProperty(value = "每人限制购买数")
    private Integer limitNum;


}
