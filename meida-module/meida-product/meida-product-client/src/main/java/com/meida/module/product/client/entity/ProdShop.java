package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author flyme
 * @date 2019-06-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_shop")
@TableAlias("shop")
@ApiModel(value = "店铺对象", description = "ProdShop对象")
public class ProdShop extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "店铺ID")
    @TableId(value = "shopId", type = IdType.ASSIGN_ID)
    private Long shopId;

    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    @ApiModelProperty(value = "店铺logo")
    private String shopLogo;

    @ApiModelProperty(value = "店铺分类Id")
    private Long typeId;

    @ApiModelProperty(value = "店铺图片")
    private String shopImage;

    @ApiModelProperty(value = "店铺门头")
    private String shopDoorway;

    @ApiModelProperty(value = "店铺视频")
    private String shopVideo;

    @ApiModelProperty(value = "联系人")
    private String linkMan;

    @ApiModelProperty(value = "联系人电话")
    private String linkTel;

    @ApiModelProperty(value = "店铺客服电话")
    private String postSaleTel;

    @ApiModelProperty(value = "经营范围")
    private String businessScope;

    @ApiModelProperty(value = "区ID")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Long areaId;

    @ApiModelProperty(value = "城市ID")
    private Long cityId;

    @ApiModelProperty(value = "省ID")
    private Long proId;

    @ApiModelProperty("省市区名称")
    private String areaName;

    @ApiModelProperty(value = "经度")
    private BigDecimal lon;

    @ApiModelProperty(value = "维度")
    private BigDecimal lat;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "店铺地址")
    private String shopAddress;

    @ApiModelProperty(value = "店铺简介")
    private String shopDesc;


    @ApiModelProperty(value = "店铺动态")
    private String shopDynamic;

    @ApiModelProperty(hidden = true, value = "认证状态")
    private Integer shopState;

    @ApiModelProperty(value = "销量")
    private Integer saleCount;

    @ApiModelProperty(value = "服务评分")
    private BigDecimal serviceScore;

    @ApiModelProperty(value = "店铺评分")
    private BigDecimal shopScore;

    @ApiModelProperty(value = "物流评分")
    private BigDecimal logisticsScore;


    @ApiModelProperty(hidden = true, value = "推荐状态")
    private Integer recommend;

    @ApiModelProperty(value = "营业时间")
    private String businessHours;


    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "审核意见")
    private String auditContent;


}
