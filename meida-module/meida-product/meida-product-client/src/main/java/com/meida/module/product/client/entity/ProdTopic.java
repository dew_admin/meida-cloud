package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 专题活动
 *
 * @author flyme
 * @date 2019-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_topic")
@TableAlias("topic")
@ApiModel(value="ProdTopic对象", description="专题活动")
public class ProdTopic extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "topicId", type = IdType.ASSIGN_ID)
    private Long topicId;

    @ApiModelProperty(value = "标题")
    private String topicTitle;

    @ApiModelProperty(value = "封面图")
    private String coverImage;

    @ApiModelProperty(value = "店铺")
    private Long shopId;

    @ApiModelProperty(value = "开始时间")
    private Date beginDate;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

    @ApiModelProperty(value = "结束日期")
    private Date endDate;

    @ApiModelProperty(value = "状态")
    private Integer state;

    @ApiModelProperty(value = "活动类型")
    private Integer topicType;

    @ApiModelProperty(value = "活动详情")
    private String topicDesc;

}
