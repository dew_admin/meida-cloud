package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.ParentId;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 店铺产品分类
 *
 * @author flyme
 * @date 2019-07-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_group")
@TableAlias("group1")
@ApiModel(value = "ProdGroup对象", description = "店铺产品分类")
public class ProdGroup extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类ID")
    @TableId(value = "groupId", type = IdType.ASSIGN_ID)
    private Long groupId;

    @ApiModelProperty(value = "分类名称")
    private String groupName;

    @ApiModelProperty(value = "父级ID")
    @ParentId
    private Long parentId;

    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;

    @ApiModelProperty(value = "分类状态 0_不显示，1_显示")
    private Integer groupState;

    @ApiModelProperty(value = "级别")
    private Integer groupLevel;

    @ApiModelProperty(value = "店铺ID")
    private Long shopId;

    @ApiModelProperty(value = "企业ID")
    private Long companyId;

}
