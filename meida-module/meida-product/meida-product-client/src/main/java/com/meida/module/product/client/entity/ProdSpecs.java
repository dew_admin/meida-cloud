package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 规格表
 *
 * @author flyme
 * @date 2019-10-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_specs")
@TableAlias("specs")
@ApiModel(value = "ProdSpecs对象", description = "规格表")
public class ProdSpecs extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "specsId", type = IdType.ASSIGN_ID)
    private Long specsId;

    @ApiModelProperty(value = "规格名称")
    private String specsName;

    @ApiModelProperty(value = "规格选项")
    private String specsOption;

    @ApiModelProperty(value = "规格类型")
    private Integer specsType;


    @ApiModelProperty(value = "规格描述")
    private String specsDesc;

    @ApiModelProperty(value = "启用状态")
    private Integer state;

    @ApiModelProperty(value = "是否是规格")
    private Boolean sku;


}
