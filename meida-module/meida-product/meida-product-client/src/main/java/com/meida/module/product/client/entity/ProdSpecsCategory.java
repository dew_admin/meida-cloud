package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 规格参数模板
 *
 * @author flyme
 * @date 2019-07-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_specs_category")
@TableAlias("specscategory")
@ApiModel(value = "ProdSpecification对象", description = "规格参数模板")
public class ProdSpecsCategory extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "specificationId", type = IdType.ASSIGN_ID)
    private Long specificationId;

    @ApiModelProperty(value = "规格模板所属商品分类id")
    private Long categoryId;

    @ApiModelProperty(value = "规格参数模板，json格式")
    private String specifications;

}
