package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 店铺产品分类和产品中间表
 *
 * @author flyme
 * @date 2019-07-02
 */
@Data
@Accessors(chain = true)
@TableName("prod_product_group")
@TableAlias("pgp")
@ApiModel(value = "ProdGroupProduct对象", description = "店铺产品分类和产品中间表")
public class ProdProductGroup {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分组ID")
    private Long groupId;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

}
