package com.meida.module.product.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 产品详细表
 *
 * @author flyme
 * @date 2019-07-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("prod_productdetails")
@TableAlias("productdetails")
@ApiModel(value = "ProdProductdetails对象", description = "产品详细表")
public class ProdProductdetails extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品ID")
    @TableId(value = "productDetailsId", type = IdType.ASSIGN_ID)
    private Long productDetailsId;

    @ApiModelProperty(value = "描述")
    private String productDesc;

    @ApiModelProperty(value = "全部规格参数数据")
    private String allSpec;


    @ApiModelProperty(value = "产品图片")
    private String productImage;





    @ApiModelProperty(value = "产品视频")
    private String productVideo;


    @ApiModelProperty(value = "特有规格参数")
    private String privateSpec;

    @ApiModelProperty(value = "包装清单")
    private String packinglist;

    @ApiModelProperty(value = "售后服务")
    private String afterService;

    @ApiModelProperty(value = "产品备注")
    private String remark;

}
