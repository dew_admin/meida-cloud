package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdAftersale;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 售后保障 Mapper 接口
 * @author flyme
 * @date 2020-01-09
 */
public interface ProdAftersaleMapper extends SuperMapper<ProdAftersale> {

}
