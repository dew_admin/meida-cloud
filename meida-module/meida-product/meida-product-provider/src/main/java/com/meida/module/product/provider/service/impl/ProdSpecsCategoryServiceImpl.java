package com.meida.module.product.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdSpecsCategory;
import com.meida.module.product.provider.mapper.ProdSpecsCategoryMapper;
import com.meida.module.product.provider.service.ProdSpecsCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 规格参数模板 实现类
 *
 * @author flyme
 * @date 2019-07-02
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdSpecsCategoryServiceImpl extends BaseServiceImpl<ProdSpecsCategoryMapper, ProdSpecsCategory> implements ProdSpecsCategoryService {


    private Long countByCategoryId(Long categoryId) {
        CriteriaQuery cq = new CriteriaQuery(ProdSpecsCategory.class);
        cq.eq(true, "categoryId", categoryId);
        return count(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public EntityMap getByCateGoryId(Long categoryId) {
        CriteriaQuery cq = new CriteriaQuery(ProdSpecsCategory.class);
        cq.select(ProdSpecsCategory.class, "*");
        cq.eq(ProdSpecsCategory.class, "categoryId", categoryId);
        return findOne(cq);
    }




}
