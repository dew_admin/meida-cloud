package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.product.client.entity.ProdProductGroup;

import java.util.List;

/**
 * 店铺产品分类和产品中间表 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdGroupProductService extends IBaseService<ProdProductGroup> {
    /**
     * 设置产品所属分类
     *
     * @param productId
     * @param groupIds
     * @return
     */
    Boolean setProductGroup(Long productId, String[] groupIds);

    /**
     * 设置产品所属分类
     *
     * @param productId
     * @return
     */
    Boolean delProductGroup(Long productId);

    /**
     * 根据产品查询分组ID
     *
     * @param productId
     * @return
     */
    List<Long> selectIds(Long productId);

    /**
     * 检测分组下是否有商品
     */
    Long countByGroupId(Long groupId);
}
