package com.meida.module.product.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.*;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdSpecsCategory;
import com.meida.module.product.provider.service.ProdSpecsCategoryService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 规格参数模板控制器
 *
 * @author flyme
 * @date 2019-07-02
 */
@RestController
@RequestMapping("/specscategory/")
@Api(tags = "商品模块-规格模板")
@ApiIgnore
public class ProdSpecsCategoryController extends BaseController<ProdSpecsCategoryService, ProdSpecsCategory> {

    @ApiOperation(value = "规格参数模板-分页列表", notes = "规格参数模板分页列表 ")
    @GetMapping(value = "pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "规格参数模板-列表", notes = "规格参数模板列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "规格参数模板-添加", notes = "添加规格参数模板")
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "规格参数模板-更新", notes = "更新规格参数模板")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {

        return bizService.edit(params);
    }

    @ApiOperation(value = "规格参数模板-删除", notes = "删除规格参数模板")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "规格参数模板-详情", notes = "规格参数模板详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "根据产品分类查询产品规格模板", notes = "根据产品分类查询产品规格模板")
    @GetMapping(value = "getSpecsCategory")
    public ResultBody getSpecsCategory(@RequestParam(value = "categoryId") Long categoryId) {
        EntityMap map = bizService.getByCateGoryId(categoryId);
        return ResultBody.ok(map);
    }
}
