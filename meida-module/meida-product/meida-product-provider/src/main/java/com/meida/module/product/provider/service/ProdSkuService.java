package com.meida.module.product.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.product.client.entity.ProdSku;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdSkuService extends IBaseService<ProdSku> {
    /**
     * 查询产品规格
     *
     * @param productId
     * @return
     */
    List<EntityMap> selectByProductId(Long productId);

    /**
     * 删除产品规格
     *
     * @param productId
     * @return
     */
    Boolean deleteByProductId(Long[] productId);

    /**
     * 删除规格
     *
     * @param productId
     * @return
     */
    Boolean deleteByProductId(Long productId);

    /**
     * 更新商品库存
     *
     * @return
     */
    Boolean updateNum(Long skuId, String numType, Integer num);

}
