package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.*;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdSku;
import com.meida.module.product.provider.service.ProdSkuService;
import springfox.documentation.annotations.ApiIgnore;

import javax.swing.*;
import java.util.Map;


/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8控制器
 *
 * @author flyme
 * @date 2019-07-02
 */
@RestController
@RequestMapping("/product/")
@Api(tags = "商品模块-商品规格")
@ApiIgnore
public class ProdSkuController extends BaseController<ProdSkuService, ProdSku> {

    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-分页列表", notes = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8分页列表 ")
    @GetMapping(value = "sku/pageList")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-列表", notes = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8列表 ")
    @GetMapping(value = "sku/list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }


    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-添加", notes = "添加sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8")
    @PostMapping(value = "sku/add")
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-更新", notes = "更新sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8")
    @PostMapping(value = "sku/update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-删除", notes = "删除sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8")
    @PostMapping(value = "sku/remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8-详情", notes = "sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8详情")
    @GetMapping(value = "sku/get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
}
