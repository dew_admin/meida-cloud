package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 商家专题活动列表
 *
 * @author Administrator
 */
@Component("shopTopicListHandler")
@Log4j2
public class ShopTopicListHandler implements PageInterceptor {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(true, "topic.companyId", companyId);
    }

}
