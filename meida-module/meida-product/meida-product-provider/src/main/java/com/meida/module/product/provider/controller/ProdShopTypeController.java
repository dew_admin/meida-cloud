package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdShopType;
import com.meida.module.product.provider.service.ProdShopTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 分类表控制器
 *
 * @author flyme
 * @date 2019-11-16
 */
@RestController
@RequestMapping("/system/type/")
@Api(tags = "基础模块-分类管理")
public class ProdShopTypeController extends BaseController<ProdShopTypeService, ProdShopType> {

    @ApiOperation(value = "分类表-分页列表", notes = "分类表分页列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "分类表-列表", notes = "分类表列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "分类表-添加", notes = "添加分类表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "分类表-更新", notes = "更新分类表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "分类表-删除", notes = "删除分类表")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "分类表-详情", notes = "分类表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "分类表-更新状态值", notes = "分类表更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
            return bizService.setState(map, "state", state);
    }
}
