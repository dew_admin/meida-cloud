package com.meida.module.product.provider.service;

import com.meida.common.enums.VisiableEnum;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.product.client.entity.ProdCategory;

import java.util.List;
import java.util.Map;

/**
 * 店铺产品分类 接口
 *
 * @author flyme
 * @date 2019-06-19
 */
public interface ProdCategoryService extends IBaseService<ProdCategory> {

    /**
     * 设置状态
     *
     * @param categoryId
     * @param visiableEnum
     * @return
     */
    ResultBody setState(Long categoryId, VisiableEnum visiableEnum);

    ResultBody listAll(Map params);

    /**
     * 查询一级分类
     */
    List<EntityMap> selectFirstCategory(String categoryName);

    /**
     * 查询二级分类
     */
    List<EntityMap> selectSecondCategory(Long categoryId, String categoryName);

    /**
     * 查询全部分类
     */
    ResultBody selectAllCategory();

    /**
     * 查询店铺商品分类
     */
    ResultBody selectShopCategory(Long shopId);

}
