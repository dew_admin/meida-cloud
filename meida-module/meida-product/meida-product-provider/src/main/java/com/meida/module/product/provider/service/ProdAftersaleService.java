package com.meida.module.product.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.product.client.entity.ProdAftersale;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 售后保障 接口
 *
 * @author flyme
 * @date 2020-01-09
 */
public interface ProdAftersaleService extends IBaseService<ProdAftersale> {

    /**
     * 查询店铺售后保障
     * @param companyId
     * @return
     */
    List<EntityMap> selectByCompanyId(Long companyId);

}
