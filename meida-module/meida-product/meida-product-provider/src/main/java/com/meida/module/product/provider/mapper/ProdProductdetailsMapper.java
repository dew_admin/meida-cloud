package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdProductdetails;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 产品详细表 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-03
 */
public interface ProdProductdetailsMapper extends SuperMapper<ProdProductdetails> {

}
