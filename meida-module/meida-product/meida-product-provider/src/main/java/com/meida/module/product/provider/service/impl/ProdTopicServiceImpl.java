package com.meida.module.product.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.module.product.client.entity.ProdMarketing;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.client.entity.ProdTopic;
import com.meida.module.product.provider.mapper.ProdTopicMapper;
import com.meida.module.product.provider.service.ProdMarketingService;
import com.meida.module.product.provider.service.ProdProductService;
import com.meida.module.product.provider.service.ProdTopicService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 专题活动接口实现类
 *
 * @author flyme
 * @date 2019-12-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdTopicServiceImpl extends BaseServiceImpl<ProdTopicMapper, ProdTopic> implements ProdTopicService {

    @Autowired
    private ProdProductService productService;

    @Autowired
    private ProdMarketingService marketingService;

    @Override
    public ResultBody beforePageList(CriteriaQuery<ProdTopic> cq, ProdTopic topic, EntityMap requestMap) {
        cq.select(ProdTopic.class, "topicId", "topicTitle", "coverImage", "shopId", "topicType", "state", "beginDate", "endDate");
        cq.eq(ProdTopic.class, "topicType");
        cq.likeByField(ProdTopic.class,"topicTitle");
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> afterPageList(CriteriaQuery<ProdTopic> cq, List<EntityMap> data, ResultBody resultBody) {
        for (EntityMap topic : data) {
            Long topicId = topic.getLong("topicId");
            List<Long> list = marketingService.selectProductIds(topicId);
            topic.put("productNum", list.size());
        }
        return super.afterPageList(cq,data,resultBody);
    }

    @Override
    public ResultBody beforeGet(CriteriaQuery<ProdTopic> cq, ProdTopic topic, EntityMap requestMap) {
        cq.select(ProdTopic.class, "*");
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ProdTopic topic, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        topic.setCompanyId(companyId);
        topic.setShopId(companyId);
        topic.setState(CommonConstants.ENABLED);
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterEdit(CriteriaUpdate cu, ProdTopic topic,EntityMap extra) {
        marketingService.updateByTopicId(topic.getTopicId());
        return ResultBody.ok();
    }

    @Override
    public void afterGet(CriteriaQuery cq, EntityMap result) {
        Long topicId = result.getLong("topicId");
        CriteriaQuery cq2 = new CriteriaQuery(ProdProduct.class);
        cq2.select(ProdMarketing.class, "*");
        cq2.eq(true, "topicId", topicId);
        cq2.createJoin(ProdMarketing.class).setJoinField("productId").setMainField("productId");
        List<EntityMap> list = productService.selectEntityMap(cq2);
        result.put("productList", list);

    }

}
