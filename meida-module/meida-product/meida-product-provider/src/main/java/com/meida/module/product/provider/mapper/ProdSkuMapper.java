package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdSku;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdSkuMapper extends SuperMapper<ProdSku> {

}
