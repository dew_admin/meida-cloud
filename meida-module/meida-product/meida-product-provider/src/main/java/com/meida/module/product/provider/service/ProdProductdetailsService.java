package com.meida.module.product.provider.service;

import com.meida.module.product.client.entity.ProdProductdetails;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 产品详细表 接口
 *
 * @author flyme
 * @date 2019-07-03
 */
public interface ProdProductdetailsService extends IBaseService<ProdProductdetails> {

}
