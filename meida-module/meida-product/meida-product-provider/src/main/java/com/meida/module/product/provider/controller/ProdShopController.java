package com.meida.module.product.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.provider.service.ProdShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.Map;


/**
 * 商家店铺管理
 *
 * @author flyme
 * @date 2019-06-17
 */
@RestController
@Api(tags = "商品模块-商家管理")
@RequestMapping("/shop/")
public class ProdShopController extends BaseController<ProdShopService, ProdShop> {

    @ApiOperation(value = "商家分页列表", notes = "商家分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    /**
     * 更新企业店铺信息
     */
    @ApiOperation(value = "更新企业店铺信息", notes = "更新企业店铺信息")
    @PostMapping(value = "company/update")
    public ResultBody companyUpdate(@RequestParam(required = false) Map params) {
        CriteriaSave cs = new CriteriaSave(params, ProdShop.class);
        return bizService.setProdShop(cs);
    }

    @ApiOperation(value = "店铺-列表", notes = "店铺列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    /**
     * 更新店铺
     */
    @ApiOperation(value = "店铺-更新", notes = "只更新非空字段")
    @PostMapping(value = "edit")
    public ResultBody edit(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "企业店铺-列表", notes = "企业店铺列表 ")
    @GetMapping(value = "company/list")
    public ResultBody companyList(@RequestParam(required = false) Map params) {
        return bizService.listByCompany(params);
    }


    @ApiOperation(value = "店铺-详细信息", notes = "查询店铺详细信息")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "删除店铺", notes = "删除店铺")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "店铺审核", notes = "店铺审核")
    @PostMapping(value = "setShopState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shopId", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "shopState", required = true, value = "店铺状态", paramType = "form"),
            @ApiImplicitParam(name = "auditContent", required = true, value = "审核内容", paramType = "form"),
            @ApiImplicitParam(name = "handlerName", value = "handlerName", paramType = "form")
    })
    public ResultBody setShopState(@RequestParam(required = false) Map params) {
        return bizService.setShopState(params);
    }


    @ApiOperation(value = "设置推荐状态", notes = "设置推荐状态")
    @PostMapping(value = "setRecommend")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "recommend", required = true, value = "推荐状态", paramType = "query")
    })
    public ResultBody setRecommend(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "recommend") Integer recommend) {
        return bizService.setRecommend(ids, recommend);
    }

}
