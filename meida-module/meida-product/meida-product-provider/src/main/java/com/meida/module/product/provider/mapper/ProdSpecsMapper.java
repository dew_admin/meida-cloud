package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdSpecs;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 规格表 Mapper 接口
 * @author flyme
 * @date 2019-10-21
 */
public interface ProdSpecsMapper extends SuperMapper<ProdSpecs> {

}
