package com.meida.module.product.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.service.BaseShopService;
import com.meida.common.enums.AuthStatusEnum;
import com.meida.common.enums.YesNoEnum;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.client.entity.ProdShopType;
import com.meida.module.product.provider.mapper.ProdShopMapper;
import com.meida.module.product.provider.service.ProdProductService;
import com.meida.module.product.provider.service.ProdShopService;
import com.meida.module.product.provider.service.ProdShopTypeService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysAreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 实现类
 *
 * @author flyme
 * @date 2019-06-17
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdShopServiceImpl extends BaseServiceImpl<ProdShopMapper, ProdShop> implements ProdShopService, BaseShopService {

    @Resource
    private SysAreaService areaService;

    @Resource
    private ProdShopTypeService typeService;

    @Resource
    private ProdProductService prodProductService;

    @Override
    public ResultBody setProdShop(CriteriaSave cs) {
        Long companyId = OpenHelper.getCompanyId();
        cs.put("companyId", companyId);
        //设置店铺ID和企业ID一致便于一个企业对应一个店铺
        cs.put("shopId", companyId);
        ProdShop prodShop = getById(companyId);
        ProdShop entity = cs.getEntity(ProdShop.class);
        if (FlymeUtils.isNotEmpty(entity)) {
            entity.setShopState(1);
        }
        if (FlymeUtils.isNotEmpty(prodShop)) {
            if (prodShop.getShopState().equals(2)) {
                entity.setShopState(10);
            }
            CriteriaUpdate cu = new CriteriaUpdate();
            cu.eq("shopId", companyId);
            update(entity, cu);
        } else {
            save(entity);
        }
        return ResultBody.msg("设置成功").data(entity);
    }

    @Override
    public ProdShop findByCompanyId(Long companyId) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(true, "companyId", companyId);
        return getOne(qw);
    }


    @Override
    public ProdShop findByShopName(String shopName) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(true, "shopName", shopName);
        return getOne(qw);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody listByCompany(Map params) {
        Long companyId = OpenHelper.getCompanyId();
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(params, ProdShop.class);
        cq.select(ProdShop.class, "*");
        cq.eq(ProdShop.class, "companyId", companyId);
        return baseList(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ProdShop> cq, ProdShop prodShop, EntityMap requestMap) {
        //维度
        String lat = requestMap.get("lat");
        //经度
        String lon = requestMap.get("lon");
        //排序字段
        String order = requestMap.get("order");
        //排序方式
        String sort = requestMap.get("sort");
        cq.select(ProdShop.class, "shopId", "shopName", "shopLogo", "shopImage", "saleCount", "shopScore", "businessHours", "serviceScore", "logisticsScore", "linkMan", "linkTel", "shopState", "recommend");
        cq.select(ProdShopType.class, "typeName");
        cq.select(SysCompany.class, "companyName");
        if (FlymeUtils.allNotNull(lon, lat)) {
            //计算距离
            cq.addSelect("IFNULL(distance(" + lat + ", " + lon + ", lat, lon),0) distance");
            //设置排序
            if (FlymeUtils.isNotEmpty(order) && "distance".equals(order)) {
                cq.orderBy(order, sort);
            } else {
                cq.orderByAsc("distance");
            }

        }
        if (FlymeUtils.isNotEmpty(order)) {
            cq.orderBy(order, sort);
        }
        cq.likeByField(ProdShop.class, "linkMan");
        cq.likeByField(ProdShop.class, "shopName");
        cq.createJoin(SysCompany.class);
        cq.createJoin(ProdShopType.class);
        cq.eq(ProdShop.class, "shopState");
        cq.eq(ProdShop.class, "typeId");
        return ResultBody.ok();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> listRecommend(CriteriaQuery cq, int count, boolean collecon) {
        cq.select(ProdShop.class, "shopId", "shopName", "shopLogo", "shopImage");
        if (collecon) {
            setCollecon(cq);
        }
        cq.eq("recommend", YesNoEnum.YES.getCode());
        cq.eq(ProdShop.class, "shopState", AuthStatusEnum.AUTHSUCCESS.getCode());
        cq.orderByDesc("createTime");
        cq.last("limit " + count);
        return selectEntityMap(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody setShopState(Map map) {
        CriteriaUpdate<ProdShop> cq = new CriteriaUpdate(map, entityClass);
        ProdShop shop = cq.getEntity(ProdShop.class);
        Long shopId = shop.getShopId();
        ApiAssert.isNotEmpty("主键不能为空", shopId);
        Integer shopState = FlymeUtils.getInteger(shop.getShopState(), 0);
        cq.set("shopState", shopState);
        if (shopState.equals(AuthStatusEnum.AUTHFAIL.getCode())) {
            cq.set("auditContent", shop.getAuditContent());
        } else {
            cq.set("auditContent", "审核成功");
        }
        ResultBody resultBody = baseUpdate(cq);
        if (resultBody.isOk()) {
            return ResultBody.ok("审核成功", shopState);
        } else {
            return ResultBody.failed("审核失败");
        }
    }

    @Override
    public List<EntityMap> listNearby(EntityMap params, int count, boolean collecon) {
        //维度
        String lat = params.get("lat");
        //经度
        String lon = params.get("lon");
        //排序字段
        String order = params.get("order");
        //排序方式
        String sort = params.get("sort");
        //是否推荐
        Integer recommend = params.getInt("recommend", null);
        CriteriaQuery cq = new CriteriaQuery(ProdShop.class);
        cq.select(ProdShop.class, "shopId", "shopName", "shopLogo", "shopImage", "saleCount", "shopScore", "businessHours");
        if (collecon) {
            setCollecon(cq);
        }
        if (FlymeUtils.allNotNull(lon, lat)) {
            cq.addSelect("IFNULL(distance(" + lat + ", " + lon + ", lat, lon),0) distance");
            if (FlymeUtils.isEmpty(order)) {
                //默认按距离正序排列
                cq.orderByAsc("distance");
            }
        }
        cq.eq("recommend", recommend);
        cq.eq(ProdShop.class, "shopState", AuthStatusEnum.AUTHSUCCESS.getCode());
        if (FlymeUtils.isNotEmpty(order)) {
            cq.orderBy(order, sort);
        }
        if (count > 0) {
            cq.last("limit " + count);
        }
        return selectEntityMap(cq);
    }

    /**
     * 设置收藏标记
     *
     * @param cq
     */
    private void setCollecon(CriteriaQuery<ProdProduct> cq) {
        //是否添加收藏标记
        Long userId = OpenHelper.getUserId();
        if (FlymeUtils.isNotEmpty(userId)) {
            cq.addSelect("(select count(targetId) from sys_collecon sc where sc.targetId=shop.shopId and targetEntity='ProdShop' and userId=" + userId + ") colleconTag");
        }
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ProdShop> cu, ProdShop shop,EntityMap extra) {
        if (FlymeUtils.isNotEmpty(shop)) {
            String areaName = areaService.getAreaName(shop.getProId(), shop.getCityId(), shop.getAreaId());
            shop.setAreaName(areaName);
        }
        return ResultBody.ok();
    }

    @Override
    public void afterGet(CriteriaQuery cq, EntityMap result) {
        Long typeId = result.getLong("typeId");
        String typeName = "";
        if (FlymeUtils.isNotEmpty(typeId)) {
            ProdShopType type = typeService.getById(typeId);
            typeName = type.getTypeName();
        }
        result.put("typeName", typeName);
    }

    @Override
    public ResultBody beforeDelete(CriteriaDelete<ProdShop> cd) {
        Long shopId = cd.getIdValue();
        Long productCount = prodProductService.countByShopId(shopId);
        if (productCount > 0) {
            return ResultBody.failed("店铺已添加商品,禁止删除");
        } else {
            return ResultBody.ok();
        }

    }

    @Override
    public Boolean deletByCompanyIds(Long[] companyIds) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.in(true, "companyId", companyIds);
        return remove(cd);
    }
}
