package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdMarketing;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 商品营销 Mapper 接口
 * @author flyme
 * @date 2019-12-05
 */
public interface ProdMarketingMapper extends SuperMapper<ProdMarketing> {

}
