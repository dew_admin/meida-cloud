package com.meida.module.product.provider.service;

import com.meida.module.product.client.entity.ProdSpecs;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 规格表 接口
 *
 * @author flyme
 * @date 2019-10-21
 */
public interface ProdSpecsService extends IBaseService<ProdSpecs> {

}
