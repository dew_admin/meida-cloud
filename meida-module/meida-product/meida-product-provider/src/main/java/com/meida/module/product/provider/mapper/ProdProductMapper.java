package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdProduct;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 产品 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdProductMapper extends SuperMapper<ProdProduct> {

}
