package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdGroup;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 店铺产品分类 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdGroupMapper extends SuperMapper<ProdGroup> {

}
