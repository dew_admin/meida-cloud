package com.meida.module.product.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdBrand;
import com.meida.module.product.client.entity.ProdCategory;
import com.meida.module.product.provider.mapper.ProdBrandMapper;
import com.meida.module.product.provider.service.ProdBrandService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 品牌 实现类
 *
 * @author flyme
 * @date 2019-07-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdBrandServiceImpl extends BaseServiceImpl<ProdBrandMapper, ProdBrand> implements ProdBrandService {


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforeListEntityMap(CriteriaQuery<ProdBrand> cq, ProdBrand prodBrand, EntityMap requestMap) {
        cq.select(ProdBrand.class, "brandId", "brandName", "brandLogo");
        cq.eq("categoryId");
        return super.beforeListEntityMap(cq, prodBrand, requestMap);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ProdBrand> cq, ProdBrand prodBrand, EntityMap requestMap) {
        cq.select(ProdBrand.class, "brandId", "brandName", "brandLogo", "categoryId", "recommend");
        cq.select(ProdCategory.class, "categoryName");
        cq.eq(ProdBrand.class, "categoryId");
        cq.likeByField(ProdBrand.class, "brandName");
        cq.createJoin(ProdCategory.class);
        return super.beforePageList(cq, prodBrand, requestMap);
    }




    @Override
    public List<EntityMap> selectByCategoryId(Long categoryId) {
        CriteriaQuery cq = new CriteriaQuery(ProdBrand.class);
        cq.eq("categoryId", categoryId);
        return selectEntityMap(cq);
    }

}
