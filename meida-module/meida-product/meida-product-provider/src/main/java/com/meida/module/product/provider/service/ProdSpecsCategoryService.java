package com.meida.module.product.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.module.product.client.entity.ProdSpecsCategory;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 规格参数模板 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdSpecsCategoryService extends IBaseService<ProdSpecsCategory> {

    EntityMap getByCateGoryId(Long categoryId);
}
