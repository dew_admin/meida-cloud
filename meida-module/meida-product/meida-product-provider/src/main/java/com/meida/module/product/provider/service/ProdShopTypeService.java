package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.module.product.client.entity.ProdShopType;

import java.util.List;

/**
 * 分类表 接口
 *
 * @author flyme
 * @date 2019-11-16
 */
public interface ProdShopTypeService extends IBaseService<ProdShopType> {
    /**
     * 根据groupId查询分类
     *
     * @param groupId
     * @return
     */
    List<EntityMap> listByGroupId(Long groupId);

    /**
     * 根据typeCode查询分类
     *
     * @param typeCode
     * @return
     */
    List<EntityMap> listByTypeCode(String typeCode);
}
