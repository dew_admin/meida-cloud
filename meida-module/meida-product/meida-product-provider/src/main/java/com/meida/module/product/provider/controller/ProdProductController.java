package com.meida.module.product.provider.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdProduct;
import com.meida.module.product.provider.service.ProdProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 商品控制器
 *
 * @author flyme
 * @date 2019-07-02
 */
@RestController
@RequestMapping("/product/")
@Api(tags = "商品模块-商品管理")
public class ProdProductController extends BaseController<ProdProductService, ProdProduct> {


    @ApiOperation(value = "商品-分页列表", notes = "商品分页列表 ")
    @GetMapping(value = "page")
    public ResultBody page(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }


    @ApiOperation(value = "商品-列表", notes = "商品列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "商品-添加", notes = "添加商品")
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map map) {
        return bizService.add(map);
    }

    @ApiOperation(value = "商品-更新", notes = "更新商品")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "商品-删除", notes = "删除商品")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map map) {
        return bizService.delete(map);
    }

    @ApiOperation(value = "商品-上架下架", notes = "商品-上架下架")
    @PostMapping(value = "setOnLineState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "onlineState", required = true, value = "显示状态", paramType = "query")
    })
    public ResultBody setOnLineState(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "onlineState") Integer onlineState) {
        return bizService.setonLineState(ids, onlineState);
    }


    @ApiOperation(value = "商品-推荐设置", notes = "商品-推荐设置")
    @PostMapping(value = "setRecommend")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "recommend", required = true, value = "推荐状态", paramType = "query")
    })
    public ResultBody setRecommend(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "recommend") Integer recommend) {
        return bizService.setRecommend(ids, recommend);
    }


    @ApiOperation(value = "商品-详情", notes = "商品详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "商品可见设置", notes = "商品可见设置")
    @PostMapping(value = "setVisibleConfig")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "visibleType", required = true, value = "可见类型", paramType = "form"),
            @ApiImplicitParam(name = "visibleConfig", required = true, value = "可见设置", paramType = "form")
    })
    public ResultBody setVisibleConfig(@RequestParam(value = "ids") Long[] ids, @RequestParam("visibleType") Integer visibleType, @RequestParam(value = "visibleConfig") String visibleConfig) {
        return bizService.setonVisibleConfig(ids, visibleType, visibleConfig);
    }

    @ApiOperation(value = "规格展示数据", notes = "规格展示数据")
    @PostMapping(value = "skuDetails")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", value = "产品ID", paramType = "form")
    })
    public ResultBody skuDetails(@RequestParam(value = "productId") Long productId) {
        return bizService.findProductSkus(productId);
    }

    @ApiOperation(value = "商品数量统计", notes = "商品数量统计")
    @GetMapping(value = "totalProduct")
    public ResultBody totalProduct(@RequestParam(required = false) Map params) {
        EntityMap map = bizService.totalProduct(null);
        return ResultBody.ok(map);
    }
}
