package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdBrand;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 品牌 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-06
 */
public interface ProdBrandMapper extends SuperMapper<ProdBrand> {

}
