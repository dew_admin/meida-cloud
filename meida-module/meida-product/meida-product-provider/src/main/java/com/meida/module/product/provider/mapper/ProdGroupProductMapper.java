package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdProductGroup;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 店铺产品分类和产品中间表 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdGroupProductMapper extends SuperMapper<ProdProductGroup> {

}
