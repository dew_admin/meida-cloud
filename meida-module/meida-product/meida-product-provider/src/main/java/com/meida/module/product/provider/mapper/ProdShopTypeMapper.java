package com.meida.module.product.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.product.client.entity.ProdShopType;

/**
 * 分类表 Mapper 接口
 * @author flyme
 * @date 2019-11-16
 */
public interface ProdShopTypeMapper extends SuperMapper<ProdShopType> {

}
