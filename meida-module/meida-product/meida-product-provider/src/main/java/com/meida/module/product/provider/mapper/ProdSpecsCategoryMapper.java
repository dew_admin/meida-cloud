package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdSpecsCategory;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 规格参数模板 Mapper 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdSpecsCategoryMapper extends SuperMapper<ProdSpecsCategory> {

}
