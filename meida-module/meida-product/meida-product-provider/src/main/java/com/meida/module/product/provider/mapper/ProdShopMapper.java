package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdShop;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * Mapper 接口
 *
 * @author flyme
 * @date 2019-06-17
 */
public interface ProdShopMapper extends SuperMapper<ProdShop> {

}
