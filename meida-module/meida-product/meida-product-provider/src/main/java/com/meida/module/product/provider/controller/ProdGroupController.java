package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdGroup;
import com.meida.module.product.provider.service.ProdGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 店铺产品分类控制器
 *
 * @author flyme
 * @date 2019-07-02
 */
@RestController
@RequestMapping("/product/group/")
@Api(tags = "商品模块-店铺商品分类")
public class ProdGroupController extends BaseController<ProdGroupService, ProdGroup> {

    @ApiOperation(value = "店铺产品分类-分页列表", notes = "店铺产品分类分页列表 ")
    @GetMapping(value = "pageList")
    @ApiIgnore
    public ResultBody pageList(@RequestParam(required = false) Map map) {
        return bizService.pageList(map);
    }

    @ApiOperation(value = "我的店铺产品分类", notes = "我的店铺产品分类列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map map) {
        return bizService.listEntityMap(map);
    }

    @ApiOperation(value = "查看店铺产品全部分类", notes = "店铺产品分类 ")
    @GetMapping(value = "all")
    @ApiImplicitParam(name = "companyId", required = true, value = "企业ID", paramType = "form")
    public ResultBody all(@RequestParam(value = "companyId") Long companyId) {
        return bizService.selectProductGroup(companyId);
    }


    @ApiOperation(value = "店铺产品分类-添加", notes = "添加店铺产品分类")
    @PostMapping(value = "add")
    public ResultBody add(@RequestParam(required = false) Map map) {
        return bizService.add(map);
    }

    @ApiOperation(value = "店铺产品分类-更新", notes = "更新店铺产品分类")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map map) {
        return bizService.edit(map);
    }

    @ApiOperation(value = "店铺产品分类-删除", notes = "删除店铺产品分类")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map map) {
        return bizService.delete(map);
    }




    /**
     * 设置分类显示状态
     */
    @ApiOperation(value = "店铺产品分类-设置状态", notes = "店铺产品分类")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", required = true, value = "主键", paramType = "form")
    })
    public ResultBody setState(@RequestParam(value = "groupId") Long groupId) {
        return bizService.setState(groupId);
    }
}
