package com.meida.module.product.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.product.client.entity.ProdAftersale;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class ProdAfterSaleInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return ProdAftersale.class;
    }
}
