package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdShop;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 平台商品列表
 *
 * @author Administrator
 */
@Component("platformProductHandler")
@Log4j2
public class PlatformProductHandler implements PageInterceptor {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        cq.select(ProdShop.class, "shopName");
        cq.createJoin(ProdShop.class);
    }

}
