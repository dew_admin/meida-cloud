package com.meida.module.product.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.product.client.entity.ProdProductdetails;
import com.meida.module.product.provider.mapper.ProdProductdetailsMapper;
import com.meida.module.product.provider.service.ProdProductdetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品详细表 实现类
 *
 * @author flyme
 * @date 2019-07-03
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdProductdetailsServiceImpl extends BaseServiceImpl<ProdProductdetailsMapper, ProdProductdetails> implements ProdProductdetailsService {


}
