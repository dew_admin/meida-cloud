package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdSpecs;
import com.meida.module.product.provider.service.ProdSpecsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 规格表控制器
 *
 * @author flyme
 * @date 2019-10-21
 */
@RestController
@RequestMapping("/specs/")
@Api(tags = "商品模块-规格管理")
@ApiIgnore
public class ProdSpecsController extends BaseController<ProdSpecsService, ProdSpecs> {

    @ApiOperation(value = "规格表-分页列表", notes = "规格表分页列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "规格表-列表", notes = "规格表列表 ")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "规格表-添加", notes = "添加规格表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "规格表-更新", notes = "更新规格表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "规格表-删除", notes = "删除规格表")
    @PostMapping(value = "delete")
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "规格表-详情", notes = "规格表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "规格项-启用禁用状态设置", notes = "设置启用禁用状态")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    public ResultBody setState(@RequestParam(required = false) Map params, @RequestParam(value = "state") Integer state) {
        return bizService.setState(params, "state", state);
    }
}
