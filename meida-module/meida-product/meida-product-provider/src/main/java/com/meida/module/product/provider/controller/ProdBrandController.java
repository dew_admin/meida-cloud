package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.*;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdBrand;
import com.meida.module.product.provider.service.ProdBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 品牌控制器
 *
 * @author flyme
 * @date 2019-07-06
 */
@RestController
@RequestMapping("/brand/")
@Api(tags = "商品模块-品牌管理")
public class ProdBrandController extends BaseController<ProdBrandService, ProdBrand> {

    @ApiOperation(value = "品牌-分页列表", notes = "品牌分页列表 ")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "品牌-列表", notes = "品牌列表 ")
    @GetMapping(value = "list")
    public ResultBody listMaps(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "品牌-添加", notes = "添加品牌")
    @PostMapping(value = "add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "品牌-更新", notes = "更新品牌")
    @PostMapping(value = "update")
    @ApiIgnore
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "品牌-删除", notes = "删除品牌")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

    @ApiOperation(value = "品牌-详情", notes = "商品详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
    @ApiOperation(value = "品牌-推荐设置", notes = "品牌-推荐设置")
    @PostMapping(value = "setRecommend")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "recommend", required = true, value = "推荐状态", paramType = "query")
    })
    public ResultBody setRecommend(@RequestParam(value = "ids") Long[] ids, @RequestParam(value = "recommend") Integer recommend) {
        return bizService.setRecommend(ids, recommend);
    }



}
