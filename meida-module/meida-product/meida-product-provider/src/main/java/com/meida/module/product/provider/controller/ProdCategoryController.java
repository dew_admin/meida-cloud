package com.meida.module.product.provider.controller;


import com.meida.common.enums.VisiableEnum;
import com.meida.common.mybatis.model.*;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdCategory;
import com.meida.module.product.provider.service.ProdCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;


/**
 * 产品分类控制器
 *
 * @author flyme
 * @date 2019-06-19
 */
@RestController
@Api(tags = "商品模块-商品分类")
@RequestMapping("/product/")
public class ProdCategoryController extends BaseController<ProdCategoryService, ProdCategory> {


    /**
     * 添加产品分类
     */
    @ApiOperation(value = "产品分类--添加", notes = "添加分类")
    @PostMapping(value = "category/add")
    @ApiIgnore
    public ResultBody add(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }


    @ApiOperation(value = "产品分类--全部", notes = "全部分类 ")
    @GetMapping(value = "category/all")
    public ResultBody all() {
        return bizService.selectAllCategory();
    }

    @ApiOperation(value = "店铺产品分类", notes = "店铺商品分类 ")
    @GetMapping(value = "category/shopCategory")
    @ApiImplicitParam(name = "shopId", value = "店铺Id", paramType = "form")
    public ResultBody shopCategory(@RequestParam(value = "shopId") Long shopId) {
        return bizService.selectShopCategory(shopId);
    }

    @ApiOperation(value = "产品分类--列表", notes = "分类列表 ")
    @GetMapping(value = "category/list")
    public ResultBody listAll(@RequestParam(required = false) Map params) {
        return bizService.listAll(params);
    }


    @ApiOperation(value = "产品分类--一级", notes = "一级分类 ")
    @GetMapping(value = "category/first")
    @ApiImplicitParam(name = "categoryName", value = "分类名称", paramType = "form")
    public ResultBody first(@RequestParam(value = "categoryName", required = false) String categoryName) {
        List<EntityMap> firstList = bizService.selectFirstCategory(categoryName);
        return ResultBody.ok(firstList);
    }


    @ApiOperation(value = "产品分类-二级", notes = "二级分类 ")
    @GetMapping(value = "category/second")
    public ResultBody second(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    /**
     * 更新产品分类
     */
    @ApiOperation(value = "产品分类--更新", notes = "更新分类")
    @PostMapping(value = "category/edit")
    @ApiIgnore
    public ResultBody edit(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    /**
     * 设置分类显示状态
     */
    @ApiOperation(value = "产品分类--设置状态", notes = "设置状态")
    @PostMapping(value = "category/setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "categoryId", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "catagoryState", required = true, value = "显示状态", paramType = "query")
    })
    public ResultBody setState(@RequestParam(value = "categoryId") Long categoryId, @RequestParam(value = "catagoryState") VisiableEnum catagoryState) {
        return bizService.setState(categoryId, catagoryState);
    }

    /**
     * 产品分类
     */
    @ApiOperation(value = "产品分类--删除", notes = "删除分类")
    @PostMapping(value = "category/remove")
    @ApiIgnore
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }

}
