package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 商家专题营销商品列表
 *
 * @author Administrator
 */
@Component("shopMarketingListHandler")
@Log4j2
public class ShopMarketingListHandler implements PageInterceptor {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        Long topicId = params.getLong("topicId");
        cq.eq(true, "topicId", topicId);
        cq.eq(true, "marketing.companyId", companyId);
    }

}
