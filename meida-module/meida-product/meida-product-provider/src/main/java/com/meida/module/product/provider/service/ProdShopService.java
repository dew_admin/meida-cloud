package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.product.client.entity.ProdShop;

import java.util.List;
import java.util.Map;

/**
 * 接口
 *
 * @author flyme
 * @date 2019-06-17
 */
public interface ProdShopService extends IBaseService<ProdShop> {

    /**
     * 店铺信息设置(一个企业对应一个店铺)
     *
     * @param cs
     * @return
     */
    ResultBody setProdShop(CriteriaSave cs);

    /**
     * 根据企业ID查询店铺
     */
    ProdShop findByCompanyId(Long companyId);


    /**
     * 根据店铺名称查询店铺
     */
    ProdShop findByShopName(String shopName);


    /**
     * 店铺审核
     *
     * @param map
     * @return
     */
    ResultBody setShopState(Map map);

    /**
     * 查询企业店铺列表
     *
     * @param params
     * @return
     */
    ResultBody listByCompany(Map params);


    /**
     * 查询推荐店铺
     */
    List<EntityMap> listRecommend(CriteriaQuery cq, int count, boolean collecon);


    /**
     * 查询附近店铺
     */
    List<EntityMap> listNearby(EntityMap params, int count, boolean collecon);
}
