package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdCategory;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * 店铺产品分类 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-19
 */
public interface ProdCategoryMapper extends SuperMapper<ProdCategory> {

}
