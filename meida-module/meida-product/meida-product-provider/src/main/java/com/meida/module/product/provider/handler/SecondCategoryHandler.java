package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.client.entity.ProdCategory;
import com.meida.module.product.provider.service.ProdBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 产品二级分类扩展--附加品牌信息
 *
 * @author zyf
 */
@Component("secondCategoryHandler")
public class SecondCategoryHandler implements PageInterceptor<ProdCategory> {

    @Autowired
    private ProdBrandService brandService;

    @Override
    public void complete(CriteriaQuery<ProdCategory> cq, List<EntityMap> result, EntityMap extra) {
        Long categoryId = cq.getRequestMap().getLong("categoryId");
        List<EntityMap> brandList = brandService.selectByCategoryId(categoryId);
        if (FlymeUtils.isNotEmpty(brandList)) {
            extra.put("brandList", brandList);
        }
    }
}

