package com.meida.module.product.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.product.client.entity.ProdMarketing;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.product.client.entity.ProdTopic;

import java.util.List;
import java.util.Map;

/**
 * 商品营销 接口
 *
 * @author flyme
 * @date 2019-12-05
 */
public interface ProdMarketingService extends IBaseService<ProdMarketing> {
    /**
     * 查询营销商品规格
     *
     * @param marketingId
     * @return
     */
    ResultBody findProductSkus(Long marketingId);

    /**
     * 根据商品ID查询
     *
     * @param productId
     * @return
     */
    ProdMarketing getByProductId(Long productId);

    /**
     * 批量添加营销商品
     *
     * @param productIds
     * @param topicId
     * @return
     */
    ResultBody batchAdd(Long[] productIds, Long topicId);


    /**
     * 根据商品ID和专题活动ID查询
     *
     * @param productId
     * @param topicId
     * @return
     */
    ProdMarketing getByProductIdAndTopicId(Long productId, Long topicId);

    /**
     * 查询专题活动下已选择商品ID
     *
     * @param topicId
     * @return
     */
    List<Long> selectProductIds(Long topicId);

    /**
     * 更新营销商品
     *
     * @param topicId
     * @return
     */
    ResultBody updateByTopicId(Long topicId);


}
