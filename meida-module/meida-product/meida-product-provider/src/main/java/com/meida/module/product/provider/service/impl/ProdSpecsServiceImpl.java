package com.meida.module.product.provider.service.impl;

import com.meida.module.product.client.entity.ProdSpecs;
import com.meida.module.product.provider.mapper.ProdSpecsMapper;
import com.meida.module.product.provider.service.ProdSpecsService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 规格表 实现类
 *
 * @author flyme
 * @date 2019-10-21
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdSpecsServiceImpl extends BaseServiceImpl<ProdSpecsMapper, ProdSpecs> implements ProdSpecsService {


}
