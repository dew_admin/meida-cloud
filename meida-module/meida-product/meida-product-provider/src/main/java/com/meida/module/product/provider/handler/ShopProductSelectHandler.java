package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.provider.service.ProdMarketingService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商家专题活动商品选择列表
 *
 * @author Administrator
 */
@Component("shopProductSelectHandler")
@Log4j2
public class ShopProductSelectHandler implements PageInterceptor {

    @Autowired
    private ProdMarketingService marketingService;

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        Long topicId = params.getLong("topicId");
        List<Long> productIds = marketingService.selectProductIds(topicId);
        cq.notIn(FlymeUtils.isNotEmpty(productIds),"productId", productIds);
        cq.eq(true, "product.companyId", companyId);
    }

}
