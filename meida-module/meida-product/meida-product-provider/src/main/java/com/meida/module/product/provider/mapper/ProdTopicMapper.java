package com.meida.module.product.provider.mapper;

import com.meida.module.product.client.entity.ProdTopic;
import com.meida.common.mybatis.base.mapper.SuperMapper;

/**
 * 专题活动 Mapper 接口
 * @author flyme
 * @date 2019-12-07
 */
public interface ProdTopicMapper extends SuperMapper<ProdTopic> {

}
