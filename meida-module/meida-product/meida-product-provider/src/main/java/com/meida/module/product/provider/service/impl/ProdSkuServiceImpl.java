package com.meida.module.product.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.client.entity.ProdSku;
import com.meida.module.product.provider.mapper.ProdSkuMapper;
import com.meida.module.product.provider.service.ProdSkuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 实现类
 *
 * @author flyme
 * @date 2019-07-02
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdSkuServiceImpl extends BaseServiceImpl<ProdSkuMapper, ProdSku> implements ProdSkuService {

    @Override
    public Boolean deleteByProductId(Long[] productId) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.in(true, "productId", productId);
        baseDelete(cd);
        return true;
    }

    @Override
    public Boolean deleteByProductId(Long productId) {
        CriteriaDelete cd = new CriteriaDelete();
        cd.eq(true, "productId", productId);
        baseDelete(cd);
        return true;
    }

    @Override
    public Boolean updateNum(Long skuId, String numType, Integer num) {
        CriteriaUpdate cu = new CriteriaUpdate();
        cu.set(true, numType, num);
        cu.eq(true, "skuId", skuId);
        return update(cu);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> selectByProductId(Long productId) {
        CriteriaQuery cq = new CriteriaQuery(ProdSku.class);
        cq.select(ProdSku.class, "*");
        cq.eq(ProdSku.class, "productId", productId);
        return selectEntityMap(cq);
    }



    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforeListEntityMap(CriteriaQuery<ProdSku> cq, ProdSku prodSku, EntityMap requestMap) {
        cq.select(ProdSku.class, "*");
        cq.eq(ProdSku.class, "productId");
        return super.beforeListEntityMap(cq, prodSku, requestMap);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ResultBody beforeGet(CriteriaQuery<ProdSku> cq, ProdSku prodSku, EntityMap requestMap) {
        if (FlymeUtils.isNotEmpty(prodSku)) {
            cq.eq(true, "productId", prodSku.getProductId());
            cq.eq(true, "indexes", prodSku.getIndexes());
        }
        return ResultBody.ok();
    }
}
