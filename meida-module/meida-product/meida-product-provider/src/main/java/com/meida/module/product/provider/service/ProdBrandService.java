package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.product.client.entity.ProdBrand;

import java.util.List;

/**
 * 品牌 接口
 *
 * @author flyme
 * @date 2019-07-06
 */
public interface ProdBrandService extends IBaseService<ProdBrand> {
    /**
     * 根据分类查询品牌
     *
     * @param categoryId
     * @return
     */
    List<EntityMap> selectByCategoryId(Long categoryId);

    /**
     * 设置推荐状态
     *
     * @param brandIds
     * @param recommend
     * @return
     */
    ResultBody setRecommend(Long[] brandIds, Integer recommend);

}
