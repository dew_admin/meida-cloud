package com.meida.module.product.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.product.client.entity.ProdGroup;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 店铺产品分类 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdGroupService extends IBaseService<ProdGroup> {
    /**
     * 查询店铺分类
     */
    ResultBody selectProductGroup(Long companyId);

    /**
     * 设置状态
     *
     * @param groupId
     * @return
     */
    ResultBody setState(Long groupId);
}
