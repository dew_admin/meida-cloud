package com.meida.module.product.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

/**
 * 商家产品列表
 *
 * @author Administrator
 */
@Component("mineProductListHandler")
@Log4j2
public class MineProductListHandler implements PageInterceptor {


    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq("product.companyId", companyId);
    }

}
