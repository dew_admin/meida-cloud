package com.meida.module.product.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.product.client.entity.ProdMarketing;
import com.meida.module.product.provider.service.ProdMarketingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 商品营销控制器
 *
 * @author flyme
 * @date 2019-12-05
 */
@RestController
@RequestMapping("/product/marketing/")
@Api(tags = "商品模块-营销活动")
public class ProdMarketingController extends BaseController<ProdMarketingService, ProdMarketing> {

    @ApiOperation(value = "商品营销-分页列表", notes = "商品营销分页列表")
    @GetMapping(value = "page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "marketingType", required = true, value = "主键", paramType = "form")
    })
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "商品营销-列表", notes = "商品营销列表")
    @GetMapping(value = "list")
    @ApiIgnore
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "商品营销-添加", notes = "添加商品营销")
    @PostMapping(value = "save")
    @ApiIgnore
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "营销商品-更新", notes = "更新营销商品")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "商品营销-批量添加", notes = "批量添加商品营销")
    @PostMapping(value = "batchAdd")
    @ApiIgnore
    public ResultBody batchAdd(@RequestParam(value = "productIds") Long[] productIds, @RequestParam(value = "topicId") Long topicId) {
        return bizService.batchAdd(productIds, topicId);
    }

    @ApiOperation(value = "商品营销-删除", notes = "删除商品营销")
    @PostMapping(value = "delete")
    @ApiIgnore
    public ResultBody delete(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "商品营销-更新状态值", notes = "商品营销更新某个状态字段")
    @PostMapping(value = "setState")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "主键", paramType = "form"),
            @ApiImplicitParam(name = "state", required = true, value = "状态值", paramType = "form")
    })
    @ApiIgnore
    public ResultBody setState(@RequestParam(required = false) Map map, @RequestParam(value = "state") Integer state) {
        return bizService.setState(map, "marketingState", state);
    }

    @ApiOperation(value = "营销商品规格展示", notes = "规格展示数据")
    @PostMapping(value = "skuDetails")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "marketingId", value = "营销产品ID", paramType = "form")
    })
    public ResultBody skuDetails(@RequestParam(value = "marketingId") Long marketingId) {
        return bizService.findProductSkus(marketingId);
    }
}
