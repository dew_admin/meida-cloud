package com.meida.module.product.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.module.product.client.entity.ProdAftersale;
import com.meida.module.product.provider.mapper.ProdAftersaleMapper;
import com.meida.module.product.provider.service.ProdAftersaleService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 售后保障接口实现类
 *
 * @author flyme
 * @date 2020-01-09
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdAftersaleServiceImpl extends BaseServiceImpl<ProdAftersaleMapper, ProdAftersale> implements ProdAftersaleService {


    @Override
    public ResultBody beforePageList(CriteriaQuery<ProdAftersale> cq, ProdAftersale prodAftersale, EntityMap requestMap) {
        Long companyId = OpenHelper.getCompanyId();
        cq.eq(ProdAftersale.class, "companyId", companyId);
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> selectByCompanyId(Long companyId) {
        CriteriaQuery<EntityMap> cq = new CriteriaQuery(ProdAftersale.class);
        cq.select(ProdAftersale.class, "afterSaleTitle title", "afterSaleId value", "afterSaleLogo logo");
        cq.eq(true, "companyId", companyId);
        cq.eq("afterSaleState", CommonConstants.ENABLED);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ProdAftersale prodAftersale, EntityMap extra) {
        Long companyId = OpenHelper.getCompanyId();
        prodAftersale.setCompanyId(companyId);
        return ResultBody.ok();
    }
}
