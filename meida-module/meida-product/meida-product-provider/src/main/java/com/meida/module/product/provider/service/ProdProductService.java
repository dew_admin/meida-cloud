package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.product.client.entity.ProdProduct;

import java.util.List;

/**
 * 商品 接口
 *
 * @author flyme
 * @date 2019-07-02
 */
public interface ProdProductService extends IBaseService<ProdProduct> {


    /**
     * 设置状态
     *
     * @param productIds
     * @param onLineState
     * @return
     */
    ResultBody setonLineState(Long[] productIds, Integer onLineState);

    /**
     * 设置推荐状态
     *
     * @param productIds
     * @param recommend
     * @return
     */
    ResultBody setRecommend(Long[] productIds, Integer recommend);

    /**
     * 查询最新推荐产品
     */
    List<EntityMap> listRecommend(int count, boolean collecon);

    /**
     * 查询店铺最新推荐产品
     */
    List<EntityMap> listShopRecommend(Long shopId, int count, boolean collecon);


    /**
     * 设置可见配置
     *
     * @param ids
     * @param visibleType
     * @param visibleConfig
     * @return
     */
    ResultBody setonVisibleConfig(Long[] ids, Integer visibleType, String visibleConfig);

    /**
     * 产品规格展示
     *
     * @param productId
     * @return
     */
    ResultBody findProductSkus(Long productId);

    /**
     * 根据商品ID查询商品
     *
     * @param productIds
     * @return
     */
    List<ProdProduct> selectByProductIds(Long[] productIds);


    /**
     * 统计店铺产品数量
     *
     * @param shopId
     * @return
     */
    Long countByShopId(Long shopId);

    EntityMap totalProduct(Long comapnyId);
}
