package com.meida.module.product.provider.listener;

import com.meida.common.constants.QueueConstants;
import com.meida.common.enums.AuthStatusEnum;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.product.client.entity.ProdShop;
import com.meida.module.product.provider.service.ProdShopService;
import com.meida.mq.annotation.MsgListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 店铺监听器
 *
 * @author zyf
 */
@Component
public class ShopRabbitMqListener {

    @Autowired
    private ProdShopService shopService;

    /**
     * 监听店铺注册
     *
     * @return
     */
    @MsgListener(queues = QueueConstants.QUEUE_SHOP_REG)
    public void process(CriteriaSave cs) {
        Long companyId = cs.getLong("companyId");
        Integer shopState = cs.getInt("shopState", null);
        
        try {
            ProdShop prodShop = cs.getEntity(ProdShop.class);
            prodShop.setShopId(companyId);
            prodShop.setCompanyId(companyId);
            prodShop.setLogisticsScore(new BigDecimal("0"));
            prodShop.setShopLogo(prodShop.getShopDoorway());
            prodShop.setServiceScore(new BigDecimal("0"));
            prodShop.setSaleCount(0);
            prodShop.setShopScore(new BigDecimal("0"));
            prodShop.setShopState(Optional.ofNullable(shopState).orElse(AuthStatusEnum.AUTHING.getCode()));
            String shopName = prodShop.getShopName();
            ProdShop check = shopService.findByShopName(shopName);
            if (FlymeUtils.isEmpty(check)) {
                shopService.save(prodShop);
            }
        } catch (Exception e) {
            ApiAssert.failure("服务器异常");
        }

    }

}
