package com.meida.module.product.provider.service.impl;

import com.meida.common.base.constants.CommonConstants;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.product.client.entity.ProdShopType;
import com.meida.module.product.provider.mapper.ProdShopTypeMapper;
import com.meida.module.product.provider.service.ProdShopTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 分类表 实现类
 *
 * @author flyme
 * @date 2019-11-16
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ProdShopSysTypeServiceImpl extends BaseServiceImpl<ProdShopTypeMapper, ProdShopType> implements ProdShopTypeService {

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> listByGroupId(Long groupId) {
        CriteriaQuery<ProdShopType> cq = new CriteriaQuery(ProdShopType.class);
        cq.select(ProdShopType.class, "typeId", "typeName", "typeLogo");
        cq.eq(true, "state", CommonConstants.ENABLED);
        cq.eq("groupId", groupId);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EntityMap> listByTypeCode(String typeCode) {
        CriteriaQuery<ProdShopType> cq = new CriteriaQuery(ProdShopType.class);
        cq.select(ProdShopType.class, "typeId", "typeName", "typeLogo");
        cq.eq(true, "state", CommonConstants.ENABLED);
        cq.eq("typeCode", typeCode);
        cq.orderByAsc("sortOrder");
        return selectEntityMap(cq);
    }
}
