package com.meida.module.product.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.product.client.entity.ProdTopic;

/**
 * 专题活动 接口
 *
 * @author flyme
 * @date 2019-12-07
 */
public interface ProdTopicService extends IBaseService<ProdTopic> {


}
