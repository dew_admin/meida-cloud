/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 09:49:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_shop
-- ----------------------------
DROP TABLE IF EXISTS `prod_shop`;
CREATE TABLE `prod_shop`  (
  `shopId` bigint(20) NOT NULL COMMENT '店铺ID',
  `shopName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `shopLogo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺logo',
  `shopImage` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺图片(多张用json)',
  `shopVideo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺视频',
  `shopDoorway` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺门头',
  `linkMan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `linkTel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `businessScope` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '经营范围',
  `areaId` bigint(20) NULL DEFAULT NULL COMMENT '区ID',
  `cityId` bigint(20) NULL DEFAULT NULL COMMENT '城市ID',
  `proId` bigint(20) NULL DEFAULT NULL COMMENT '省ID',
  `areaName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `lat` decimal(10, 6) NULL DEFAULT NULL COMMENT '维度',
  `lon` decimal(10, 6) NULL DEFAULT NULL COMMENT '经度',
  `typeId` bigint(20) NULL DEFAULT NULL COMMENT '分类ID',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `postSaleTel` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '客服电话(多个使用json存储)',
  `shopAddress` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺地址',
  `shopDesc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '店铺简介',
  `shopDynamic` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺动态',
  `recommend` smallint(1) NULL DEFAULT 0 COMMENT '推荐状态',
  `businessHours` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '营业时间(json存储)',
  `shopState` int(11) NULL DEFAULT NULL COMMENT '认证状态',
  `shopScore` decimal(10, 2) NULL DEFAULT NULL COMMENT '店铺评分',
  `saleCount` int(11) NULL DEFAULT NULL COMMENT '销量',
  `serviceScore` decimal(10, 1) NULL DEFAULT NULL COMMENT '服务评分',
  `logisticsScore` decimal(10, 2) NULL DEFAULT NULL COMMENT '物流评分',
  `auditContent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核意见',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`shopId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '店铺' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_shop
-- ----------------------------
INSERT INTO `prod_shop` VALUES (1, '玩了么自营店', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7fc7304c3bde4e9e9abb5d0744cd626c.jpg', '', 'https://vod.300hu.com/4c1f7a6atransbjngwcloud1oss/379d6a5b248449608668749825/v.f30.mp4?dockingId=a9fb4a0a-1fc1-47f3-98b1-f97994c5577c&storageSource=3', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7fc7304c3bde4e9e9abb5d0744cd626c.jpg', '何振洲', ' 0755-33170177', '', NULL, 4403, 440, '', 22.523802, 113.955605, 1, 1, '订单', '前海深港合作区前湾一路1号A栋201室', '<p>深圳何氏户外用品有限公司成立于2018年，公司位于深圳市南山区前海自贸区，是集产品研发，设计，代理，销售，互联网营销，户外运动等方面的相关业务。</p><p>何氏户外用品有限公司在为客户提供更多价值的同时，更是致力于为不同需求的客户提供全方位的服务，极力推动我国户外运动的发展与普及，在品牌规划及产品开发中，何氏户外不断注入新的设计理念。何氏户外拥有强有力的技术团队和销售团队，勇敢，诚信，敬业，奉献，是何氏户外追求的前进目标。</p>', '春节优惠大酬宾活动开始啦！！\n年前最有一波千万不要错过哦~~~~', 1, '8:00-21:00', 2, 5.00, 50, 5.0, 5.00, '', '', NULL, '2020-04-27 16:53:18');
INSERT INTO `prod_shop` VALUES (1225024779286646785, 'TAT射箭', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/3a1a394f0a4f4e23964dfb95a1496ff2.jpeg', NULL, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/dd11b2f3941543cebc80e32c9620e12b.png', '何振洲', '18682387185', NULL, NULL, NULL, NULL, '', 22.523802, 113.955605, 2, 1225024779286646785, '18682387185', '春茧体育馆', NULL, '', 0, '09:00-18:00', 2, 0.00, 0, 0.0, 0.00, '审核成功', NULL, '2020-02-05 19:54:01', '2020-04-27 13:02:24');
INSERT INTO `prod_shop` VALUES (1240560964004134913, '宏图钓鱼场', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/d2651ff48c074c119048998c942c4673.png', NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/d2651ff48c074c119048998c942c4673.png', '欧阳', '15818758958', NULL, NULL, NULL, NULL, NULL, 22.837161, 113.763736, 3, 1240560964004134913, NULL, '怀德新村新区', NULL, NULL, 0, NULL, 10, 0.00, 0, 0.0, 0.00, '资料不全，需补充！', NULL, '2020-03-19 16:49:17', NULL);
INSERT INTO `prod_shop` VALUES (1244267936528437250, '雪乐山', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/6daaf0dfd813474aa711a019dfcca08a.jpeg', NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/fe57b39bceee4fc2867c7b62e9d2eb25.png', '刘先生', '1868888888', NULL, NULL, NULL, NULL, '', 22.499429, 113.892958, 8, 1244267936528437250, '1868888888', '掬月半山', '', '', 0, '', 10, 0.00, 0, 0.0, 0.00, '审核成功', NULL, '2020-03-29 22:19:28', '2020-04-02 19:17:05');
INSERT INTO `prod_shop` VALUES (1244456594975985666, '上品户外', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7d96925a0832429d9d0af2857fa8b4bb.jpg', NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7d96925a0832429d9d0af2857fa8b4bb.jpg', '赵子香', '18838125259', NULL, NULL, NULL, NULL, '', 34.702569, 113.666001, 2, 1244456594975985666, NULL, '五金塑料城(郭庄)', '上品户外', NULL, 0, '09:00-23:00', 10, 0.00, 0, 0.0, 0.00, '需要补充资料', NULL, '2020-03-30 10:49:07', '2020-04-02 18:25:35');
INSERT INTO `prod_shop` VALUES (1245188173499166721, '上品户外', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/dac9fb30e71748b4b5c5c4f3ad679768.png', '', '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/4a6dcdc523024a6d9a47c478c677803b.png', '赵子香', '15937125258', '弓箭以及周边附件', NULL, NULL, NULL, '', 34.765348, 113.783050, 2, 1245188173499166721, '', '郑州火车东站-西广场进站口C', '<p>上品户外15937125258专营进口弓箭，信誉保证品种齐全， 巴力列兵，巴力野猫C6，巴力迅猛龙，巴力库德，巴力兄弟连，巴力释放者，巴力复仇者，巴力鬼怪385，巴力鬼怪410，巴力刀锋，美国PSE精英，PSE僵尸猎手，天魄碳融合，天魄蒸汽机，天魄毒液，天魄风暴，达顿反叛军，碳快递碳拦截，三利达系列追月150A，追风2010AM，森林之鹰，森林之鹰二代，森林之豹，大黑鹰，迷彩大黑鹰，黑旋风，小飞狼，小黑豹，小飞虎，赵氏猎鹰，黑曼巴C，猎豹等等系列进口国产户外弓箭<br>周边产品，附件等</p>', '', 0, '09:00-23:00', 2, 0.00, 0, 0.0, 0.00, '审核成功', '', '2020-04-01 11:16:09', '2020-04-02 21:54:37');
INSERT INTO `prod_shop` VALUES (1245701999009095681, '美达铺子', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/177d03cc2d2a44bb811471eb72e6ac25.png', NULL, NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/177d03cc2d2a44bb811471eb72e6ac25.png', '陈先生', '18001250778', NULL, NULL, NULL, NULL, NULL, 22.521231, 113.934495, 3, 1245701999009095681, NULL, '保利大厦', NULL, NULL, 0, NULL, 0, 0.00, 0, 0.0, 0.00, '审核成功', NULL, '2020-04-02 21:17:55', NULL);
INSERT INTO `prod_shop` VALUES (1245920330341310466, '火鼎户外', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7e17d214c4814353b34f862141b3faad.png', '', '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/77adfed4101348c39853ab644c886675.jpg', '颜女士', '13554843127', '', NULL, NULL, NULL, '', 22.631666, 113.871416, 1224567023958134785, 1245920330341310466, '', '深业U中心', '<h1><font face=\"微软雅黑\"><font color=\"#c24f4a\">火鼎户外</font></font></h1><p><font face=\"微软雅黑\"><font color=\"#c24f4a\"></font>生活历经</font>酸甜苦辣，才是完整的生活。</p><p><br>唯有将自己从压抑的氛围中“解救出来”，跟随我们的脚步，感受大自然带给我们的惊喜和美好。</p><p><br>回归生活的本质，不忘初心，找回自己......</p>', '', 0, '08:00-24:00', 2, 0.00, 0, 0.0, 0.00, '审核成功', '', '2020-04-03 11:45:29', '2020-04-06 21:42:07');
INSERT INTO `prod_shop` VALUES (1251766594329022466, '新领域数码', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/90c30846a0204e57a222999d0a6b169d.jpg', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/0a452505cd0c46ffa807c1d9ed95c776.mp4', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/6d40ad2013324ca78dd75f5ae758228c.png', '吕福浪', '18218140986', NULL, NULL, 4403, 440, '', 22.649236, 114.212108, 1247565022506233858, 1251766594329022466, '18218140986', '横岗村', NULL, '', 0, '9:00-22:00', 2, 0.00, 0, 0.0, 0.00, '审核成功', NULL, '2020-04-19 14:56:27', '2020-04-27 16:56:25');
INSERT INTO `prod_shop` VALUES (1252460009941299202, '深圳市玩家国际俱乐部', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/aa3ac191c9384924aa4a0d01487dc05f.png', NULL, 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/dcbb0d24b62a45e6b18fb19455078576.mp4', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/aa3ac191c9384924aa4a0d01487dc05f.png', '杨紫淇', '13554945791', NULL, NULL, NULL, NULL, '', 22.543155, 114.054320, 1225853582732857346, 1252460009941299202, '13554945791', '五洲宾馆', NULL, '国际玩家俱乐部是一个综合类的社交平台', 0, '', 2, 0.00, 0, 0.0, 0.00, '审核成功', NULL, '2020-04-21 12:51:50', '2020-04-27 13:07:12');

INSERT INTO `base_menu` VALUES (350, 0, 'shopmanager', '店铺管理', '店铺管理', '/', '', 'shop', '_self', 'PageView', 1, 3, 1, 0, 'meida-base-provider', '2018-07-29 21:20:10', '2020-01-04 17:58:22');
INSERT INTO `base_menu` VALUES (351, 350, 'shop', '店铺管理', '店铺管理', '/', 'product/shop/index', 'shop', '_self', 'PageView', 1, 2, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-01-04 17:32:22');
INSERT INTO `base_menu` VALUES (352, 350, 'shopType', '分类管理', '分类管理', '/', 'product/shopType/index', 'appstore', '_self', 'PageView', 1, 1, 1, 0, 'meida-base-provider', '2019-12-29 18:18:21', '2020-01-04 17:47:29');


SET FOREIGN_KEY_CHECKS = 1;
