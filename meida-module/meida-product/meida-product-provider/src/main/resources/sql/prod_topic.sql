/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 09:50:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_topic
-- ----------------------------
DROP TABLE IF EXISTS `prod_topic`;
CREATE TABLE `prod_topic`  (
  `topicId` bigint(20) NOT NULL COMMENT '主键',
  `topicTitle` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `coverImage` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺',
  `beginDate` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `endDate` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  `state` int(255) NULL DEFAULT NULL COMMENT '状态',
  `topicDesc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '活动详情',
  `topicType` int(255) NULL DEFAULT NULL COMMENT '活动类型1秒杀，2抢购，3每周精选',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`topicId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专题活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_topic
-- ----------------------------
INSERT INTO `prod_topic` VALUES (1213733628881661954, '春节精选商品周', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/6844d3a8d41643a580e75dbf38403fb5.jpg', 1, 1, '2019-12-31 00:00:00', '2020-01-02 00:00:00', 1, '<p>巴黎世家 (Balenciaga) 全新推出的托特包被网友喷长得像宜家的购物袋Frakta一样，编编看了下对比图，还真是~但同样价格却可以买三千多个宜家购物袋。贵就贵在了<a href=\"http://fashion.haibao.com/fashion/9044/\" target=\"_blank\">材质</a>，托特包采用的可是釉面褶皱<a href=\"http://fashion.haibao.com/fashion/8080/\" target=\"_blank\">皮革</a>。最搞笑的是，宜家英国销售主管Stella Monte<a href=\"http://brand.haibao.com/brands/8643/\" target=\"_blank\">iro</a>看到之后受宠若惊，为此还特别发布了一篇“教你如何鉴别真的宜家包”的<a href=\"http://stars.haibao.com/star/5562/\" target=\"_blank\">文章</a>，这么写的——</p><p><br></p><p>1.摇一下：如果沙沙地响的话，就是真的</p><p>2.多功能性：可以装曲棍球装备、砖头、甚至是水</p><p>3.扔进脏东西里：真的 FRAKTA 包可以用花园里的冲水管将污渍清洗干净</p><p>4.价格标签：只卖 0.99 美元</p><p><br></p><p>相信赶时髦的报友们已经在去宜家的路上了吧~</p>', 3, '2020-01-05 16:07:01', '2020-01-08 10:49:14');
INSERT INTO `prod_topic` VALUES (1213734718129827842, '2020年优惠限时抢购', '', 1, 1, '2020-01-03 00:00:00', '2020-01-10 00:00:00', 1, '<p>优惠限时抢购</p>', 1, '2020-01-05 16:11:21', '2020-01-07 16:08:19');
INSERT INTO `prod_topic` VALUES (1214454806545948674, '七天限制秒杀', '', 1, 1, '2020-01-05 00:00:00', '2020-01-12 00:00:00', 1, '<p>七天限制秒杀</p>', 2, '2020-01-07 15:52:44', '2020-01-07 16:09:29');
INSERT INTO `prod_topic` VALUES (1214712321657532418, '2020年秒杀活动节', '', 1, 1, '2020-01-08 00:00:00', '2020-01-09 00:00:00', 1, '<p>2020年秒杀活动节2020年秒杀活动节</p>', 2, '2020-01-08 08:56:00', '2020-01-08 12:19:40');
INSERT INTO `prod_topic` VALUES (1214763941858635777, '最近一波限时秒杀', '', 1, 1, '2020-01-10 00:00:00', '2020-01-12 00:00:00', 1, '<p>最近一波限时秒杀</p>', 2, '2020-01-08 12:21:07', NULL);

SET FOREIGN_KEY_CHECKS = 1;
