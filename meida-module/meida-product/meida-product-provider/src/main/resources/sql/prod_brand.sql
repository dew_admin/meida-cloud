/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 09:51:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_brand
-- ----------------------------
DROP TABLE IF EXISTS `prod_brand`;
CREATE TABLE `prod_brand`  (
  `brandId` bigint(20) NOT NULL COMMENT '品牌ID',
  `categoryId` bigint(20) NULL DEFAULT NULL COMMENT '一级分类ID',
  `categoryId2` bigint(20) NULL DEFAULT NULL COMMENT '二级分类ID',
  `brandName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '品牌名称',
  `brandLogo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '品牌logo',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '是否推荐',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `brandDesc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业简介',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `urlPrefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '协议',
  `brandSite` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '网站',
  PRIMARY KEY (`brandId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '品牌' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
