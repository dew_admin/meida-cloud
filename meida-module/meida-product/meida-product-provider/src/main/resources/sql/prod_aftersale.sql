/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 09:42:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_aftersale
-- ----------------------------
DROP TABLE IF EXISTS `prod_aftersale`;
CREATE TABLE `prod_aftersale`  (
  `afterSaleId` bigint(20) NOT NULL COMMENT '主键',
  `afterSaleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '保障标题',
  `afterSaleContent` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '保障内容',
  `afterSaleLogo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '保障logo',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '所属企业',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `afterSaleState` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`afterSaleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '售后保障' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_aftersale
-- ----------------------------
INSERT INTO `prod_aftersale` VALUES (1215090353578139650, '7天无理由退货', '<p>就是无理由</p>', '', 1, 2, 1, '2020-01-09 09:58:10', '2020-01-09 13:13:53');
INSERT INTO `prod_aftersale` VALUES (1215139577060085761, '玩了么商城发货&售后', '', '', 1, 1, 1, '2020-01-09 13:13:46', '2020-01-09 13:22:17');
INSERT INTO `prod_aftersale` VALUES (1215139776310497281, '可配送海外', '', '', 1, 3, 0, '2020-01-09 13:14:33', NULL);
INSERT INTO `prod_aftersale` VALUES (1215207296212099074, '7天无理由退款（拆开不可退）', '<p>7天无理由退款</p>', '', 1215574966891577346, 1, 1, '2020-01-09 17:42:51', '2020-01-14 11:27:27');
INSERT INTO `prod_aftersale` VALUES (1215207357398605825, '可配送海外', '', '', 1215574966891577346, 2, 1, '2020-01-09 17:43:06', '2020-01-14 11:27:33');

SET FOREIGN_KEY_CHECKS = 1;
