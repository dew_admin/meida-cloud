/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 09:49:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_shop_type
-- ----------------------------
DROP TABLE IF EXISTS `prod_shop_type`;
CREATE TABLE `prod_shop_type`  (
  `typeId` bigint(20) NOT NULL COMMENT '主键',
  `typeCode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类编码',
  `typeName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `typeLogo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类logo',
  `parentId` bigint(20) NULL DEFAULT NULL COMMENT '父Id',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `groupId` bigint(20) NULL DEFAULT NULL COMMENT '分组ID',
  `typeRate` decimal(5, 2) NULL DEFAULT NULL COMMENT '费率',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`typeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_shop_type
-- ----------------------------
INSERT INTO `prod_shop_type` VALUES (1, 'ProdShop', '高尔夫', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7c2a82ff54bc4f32be0baf9107e325f5.jpg', NULL, 1, NULL, 0.60, 1, NULL, '2020-02-26 19:26:27');
INSERT INTO `prod_shop_type` VALUES (2, 'ProdShop', '射箭馆', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/65486bdedc8e4a42be9f4739105b19b1.jpg', NULL, 2, NULL, 0.60, 1, NULL, '2020-02-23 21:52:27');
INSERT INTO `prod_shop_type` VALUES (3, 'ProdShop', '钓鱼', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/a02909016b51489891c711b934e5764b.jpg', NULL, 3, NULL, 0.60, 1, NULL, '2020-02-23 21:52:33');
INSERT INTO `prod_shop_type` VALUES (4, 'ProdShop', '潜水', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/6c3c1d47441949f0a6bd7845c55295a2.jpeg', NULL, 4, NULL, 0.60, 1, NULL, '2020-02-23 21:52:41');
INSERT INTO `prod_shop_type` VALUES (5, 'ProdShop', '登山', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/8f729221210d46378fdcdc71dc60dd84.jpg', NULL, 5, NULL, 0.60, 1, NULL, '2020-02-23 21:52:47');
INSERT INTO `prod_shop_type` VALUES (6, 'ProdShop', '儿童乐园', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/a49491189a9d42388d053866d50b48fe.jpg', NULL, 8, NULL, 0.60, 1, NULL, '2020-02-23 21:52:58');
INSERT INTO `prod_shop_type` VALUES (7, 'ProdShop', '游乐场', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/09edcdc5b8f94432b069bdcf9eaa1f39.jpg', NULL, 9, NULL, 0.60, 1, NULL, '2020-02-23 21:52:53');
INSERT INTO `prod_shop_type` VALUES (8, 'ProdShop', '滑雪', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/570882f73187457c967ec8a9a55d8f6e.jpg', NULL, 10, NULL, 0.60, 1, NULL, '2020-02-23 21:53:05');
INSERT INTO `prod_shop_type` VALUES (9, 'ProdShop', '骑行', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/8f55c38770ee4929a7a4ec418f2a2e59.jpg', NULL, 11, NULL, 0.60, 1, NULL, '2020-02-23 21:53:10');
INSERT INTO `prod_shop_type` VALUES (10, 'ProdShop', '赛车场', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c2ad0cde7ef54279bf2329f9ed3eff90.jpg', NULL, 12, NULL, 0.60, 1, NULL, '2020-02-23 21:53:15');
INSERT INTO `prod_shop_type` VALUES (11, 'ProdShop', '摩旅', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/9bbe62af020846149ebf6f674e33515a.jpg', NULL, 13, NULL, 0.60, 1, NULL, '2020-02-23 21:53:52');
INSERT INTO `prod_shop_type` VALUES (1224314179388608514, 'ProdShop', '车旅', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/4b351a026d1c4c22b6e5ba7318af1474.jpg', 0, 14, NULL, 0.60, 1, '2020-02-03 20:50:21', '2020-02-23 21:53:57');
INSERT INTO `prod_shop_type` VALUES (1224315073626804226, 'ProdShop', '游艇会', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/640d1ce31f814410bc838a11d999816a.jpg', 0, 16, NULL, 0.60, 1, '2020-02-03 20:53:54', '2020-02-23 21:54:09');
INSERT INTO `prod_shop_type` VALUES (1224319125781647361, 'ProdShop', '游泳', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b6c5c84b6a2548b5967e7d332021df8a.jpg', 0, 17, NULL, 0.60, 1, '2020-02-03 21:10:00', '2020-02-23 21:54:15');
INSERT INTO `prod_shop_type` VALUES (1224389333842038786, 'ProdShop', '摩托艇', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/4b49465776d84cd0815a71ba7e5d73f9.jpg', 0, 18, NULL, 0.60, 1, '2020-02-04 01:48:59', '2020-02-23 21:54:20');
INSERT INTO `prod_shop_type` VALUES (1224390233717379073, 'ProdShop', '攀岩', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/d774d5842b97491e879c0154d9188861.jpg', 0, 19, NULL, 0.60, 1, '2020-02-04 01:52:34', '2020-02-23 21:54:26');
INSERT INTO `prod_shop_type` VALUES (1224566961043574785, 'ProdShop', '极限运动', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/236cf3099f38458aa197a84da9a6d20c.jpg', 0, 20, NULL, 0.60, 1, '2020-02-04 13:34:49', '2020-02-23 21:54:33');
INSERT INTO `prod_shop_type` VALUES (1224567023958134785, 'ProdShop', '户外拓展', '', 0, 21, NULL, 0.60, 1, '2020-02-04 13:35:04', '2020-02-23 21:54:38');
INSERT INTO `prod_shop_type` VALUES (1225267082655862786, 'ProdShop', '羽毛球馆', '', 0, 22, NULL, 0.60, 1, '2020-02-06 11:56:51', '2020-02-23 21:54:43');
INSERT INTO `prod_shop_type` VALUES (1225267232145051650, 'ProdShop', '马术俱乐部', '', 0, 23, NULL, 0.60, 1, '2020-02-06 11:57:27', '2020-03-31 18:00:55');
INSERT INTO `prod_shop_type` VALUES (1225324675231289346, 'ProdShop', '露营地', '', 0, 24, NULL, 0.60, 1, '2020-02-06 15:45:42', '2020-02-23 21:54:57');
INSERT INTO `prod_shop_type` VALUES (1225324799684677633, 'ProdShop', '房车营地', '', 0, 25, NULL, 0.60, 1, '2020-02-06 15:46:12', '2020-02-23 21:55:02');
INSERT INTO `prod_shop_type` VALUES (1225770254826639361, 'ProdShop', '运动公园', '', 0, 26, NULL, 0.60, 1, '2020-02-07 21:16:17', '2020-04-09 22:38:16');
INSERT INTO `prod_shop_type` VALUES (1225853582732857346, 'ProdShop', '徒步路线', '', 0, 27, NULL, 0.60, 1, '2020-02-08 02:47:24', '2020-03-16 19:55:36');
INSERT INTO `prod_shop_type` VALUES (1236607727349719041, 'ProdShop', '特色民宿', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c4389bf9b7104ebd8c793ea966519a60.png', 0, 9, NULL, 0.06, 1, '2020-03-08 19:00:31', '2020-03-16 19:55:06');
INSERT INTO `prod_shop_type` VALUES (1247565022506233858, 'ProdShop', '飞行营地', '', 0, 99, NULL, 0.05, 1, '2020-04-08 00:40:54', '2020-04-08 00:40:57');
INSERT INTO `prod_shop_type` VALUES (1262633174914957313, 'ProdShop', '冲浪', '', 0, 99, NULL, 0.05, 1, '2020-05-19 14:36:22', '2020-05-21 01:44:26');

SET FOREIGN_KEY_CHECKS = 1;
