/*
 Navicat Premium Data Transfer

 Source Server         : 玩了么
 Source Server Type    : MariaDB
 Source Server Version : 100316
 Source Host           : 118.31.77.198:3300
 Source Schema         : wanleme_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/06/2020 09:50:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_product
-- ----------------------------
DROP TABLE IF EXISTS `prod_product`;
CREATE TABLE `prod_product`  (
  `productId` bigint(20) NOT NULL COMMENT '产品ID',
  `productName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `productSeries` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品系列',
  `productPrice` decimal(18, 2) NULL DEFAULT NULL COMMENT '产品价格',
  `oldPrice` decimal(18, 2) NULL DEFAULT NULL COMMENT '产品原价',
  `productNo` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品编号',
  `coverImage` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `prodcutType` int(11) NULL DEFAULT NULL COMMENT '类型 1_产品，2_物料',
  `skuShowType` int(2) NULL DEFAULT NULL COMMENT '规格展示方式1:图文2文字',
  `shopId` bigint(20) NULL DEFAULT NULL COMMENT '店铺ID',
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业Id',
  `categoryId1` bigint(20) NULL DEFAULT NULL COMMENT '商品一级分类',
  `categoryId2` bigint(20) NULL DEFAULT NULL COMMENT '商品二级分类',
  `categoryId3` bigint(20) NULL DEFAULT NULL COMMENT '商品三级分类',
  `categoryName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品分类全称',
  `groupId` bigint(20) NULL DEFAULT NULL COMMENT '店铺产品分类',
  `brandId` bigint(20) NULL DEFAULT NULL COMMENT '品牌',
  `place` bigint(20) NULL DEFAULT NULL COMMENT '商品产地',
  `saleCount` int(11) NULL DEFAULT NULL COMMENT '销量',
  `productScore` decimal(10, 1) NULL DEFAULT NULL COMMENT '商品评分',
  `deliveryTime` int(200) NULL DEFAULT NULL COMMENT '发货期',
  `visibleType` int(11) NULL DEFAULT NULL COMMENT '可见类型 1_公开,2_部分可见,3_不给谁看',
  `visibleConfig` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '可见配置',
  `onLineState` int(11) NULL DEFAULT NULL COMMENT '产品状态 0_未上架,1_已上架,2_已下架',
  `onlineDate` date NULL DEFAULT NULL COMMENT '上架时间',
  `auditState` int(11) NULL DEFAULT NULL COMMENT '审核状态 0_未审核，1_已审核，2_审核失败',
  `recommend` int(11) NULL DEFAULT NULL COMMENT '是否推荐',
  `deleted` int(255) NULL DEFAULT NULL COMMENT '是否删除',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `freight` decimal(11, 2) NULL DEFAULT NULL COMMENT '运费',
  PRIMARY KEY (`productId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_product
-- ----------------------------
INSERT INTO `prod_product` VALUES (1213309586262945793, 'Keep 电镀哑铃4kg 家用健身运动增肌男 一体式竹节设计', '', 998.00, 1000.00, '5091809', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/e97c814e35d543d5aa6422df8dae6c15.png', 0, NULL, 1, 1, 1000000, 10000001, 100000011, '户外运动-健身训练-哑铃', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-01-04 12:02:02', '2020-01-09 15:38:14', 5.00);
INSERT INTO `prod_product` VALUES (1213316718559457281, '奥义瑜伽球 75cm加厚防滑健身球', '', 88.00, 100.00, '1302316', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/a20c09928ff7493fbad6cb3d6904c463.jpg', 0, NULL, 1, 1, 1000000, 10000001, 3, '户外运动-健身训练-瑜伽用品', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-01-04 12:30:22', '2020-01-09 15:38:00', 5.00);
INSERT INTO `prod_product` VALUES (1213322759489679361, '朗威杠铃套装 家用健身包胶举重深蹲练臂肌环保', '', 80.00, 99.00, '21042468138', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/fcb4e345fc224813a5e94ab8e053cc9f.jpg', 0, NULL, 1, 1, 1000000, 10000001, 1, '户外运动-健身训练-其他器材', NULL, NULL, NULL, 7, 0.0, 0, 1, NULL, 1, NULL, 0, 1, 0, '2020-01-04 12:54:22', '2020-01-09 15:37:42', 8.00);
INSERT INTO `prod_product` VALUES (1213367946746785793, 'YNKOO 绑好的鱼线主线组套装全套成品斑点线正品钓鱼线超强拉力尼龙线台钓钓线垂钓渔具用品 4.5米线组标准款(刻度铅)【5卷装，秒换子线】 1.5号', '', 28.50, 33.50, '5566732323', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/24a85789cd4c4226a264696560ffb230.jpg', 0, NULL, 1, 1, 1000000, 10000004, 12, '户外运动-垂钓用品-钓钩配件', NULL, NULL, NULL, 7, 0.0, 0, 1, NULL, 1, NULL, 0, 1, 0, '2020-01-04 15:53:56', '2020-01-09 15:51:49', 5.00);
INSERT INTO `prod_product` VALUES (1213370133501702145, '渔之源（Yuzhiyuan） 抛饵勺可换头不锈钢打窝勺 钓鱼远投打窝器定点饵料勺大号渔具用品 抛饵勺 打窝勺【主图一为伸缩展示】', '', 12.90, 22.00, '21221212', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/cdacebcf51e44fca8534b69e670f1da5.jpg', 0, NULL, 1, 1, 1000000, 10000004, 12, '户外运动-垂钓用品-钓钩配件', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, NULL, 0, '2020-01-04 16:02:37', '2020-01-09 15:36:50', 5.00);
INSERT INTO `prod_product` VALUES (1213372109341847553, 'Hicreat8字环八字环连接器强拉力不锈钢转环路亚快速别针八子环渔具用品 4号美式增强别针 50枚', '', 10.80, 34.00, '34353535', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/7359b3981d294fa9b505baf4d5be0750.jpg', 0, NULL, 1, 1, 1000000, 10000004, 12, '户外运动-垂钓用品-钓钩配件', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, NULL, 0, '2020-01-04 16:10:28', '2020-01-10 11:29:26', 8.00);
INSERT INTO `prod_product` VALUES (1213374330737520641, '加加林（JAJALIN）户外快挂 加锁登山扣 颜色随机（4个装）', '', 18.00, 22.90, '45454645', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/786763fd6b5c474fb628a1a494d35592.jpg', 0, NULL, 1, 1, 1000000, 10000003, 9, '户外运动-户外装备-登山攀岩', NULL, NULL, NULL, 1, 0.0, 0, 1, NULL, 1, NULL, 0, NULL, 0, '2020-01-04 16:19:18', '2020-01-10 11:29:49', 8.00);
INSERT INTO `prod_product` VALUES (1213386617342795778, '九头鸟 登山绳 安全绳带钢丝 救援绳 救生绳 火灾逃生绳索 速降绳子 野营 户外攀岩爬山10米带双钩8mm', '', 29.00, 55.70, '3343424', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/aacd4b47c4c44fb88dedec90b93ce27c.jpg', 0, NULL, 1, 1, 1000000, 10000003, 11, '户外运动-户外装备-救援装备', NULL, NULL, NULL, 2, 0.0, 0, 1, NULL, 1, NULL, 0, NULL, 0, '2020-01-04 17:08:07', '2020-01-09 15:51:05', 5.00);
INSERT INTO `prod_product` VALUES (1213389570707431425, '户外冰爪大小号 雪爪 防滑鞋套雪地 泥地徒步 沙漠 登山鞋钉 24齿加强 送收纳包 M 35-40码', '', 39.90, 45.80, '546465', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c3819fb73642409bb27d6ae46f8155f1.jpg', 0, NULL, 1, 1, 1000000, 10000003, 9, '户外运动-户外装备-登山攀岩', NULL, NULL, NULL, 1, 0.0, 0, 1, NULL, 1, NULL, 0, NULL, 0, '2020-01-04 17:19:52', '2020-01-09 15:50:51', 8.00);
INSERT INTO `prod_product` VALUES (1213404745074352129, '鲁滨逊登山杖折叠铝合金超轻伸缩户外五节超短越野跑拐杖徒步手杖 黑色', '', 79.00, 85.50, '23234234', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b2892faf285b4e59b85c48016a7c4acb.jpg', 0, NULL, 1, 1, 1184660151815544834, 3001, 30011, '户外运动-户外装备-极限户外', NULL, NULL, NULL, 6, 0.0, 0, 1, NULL, 1, NULL, 0, 1, 0, '2020-01-04 18:20:09', '2020-02-03 21:52:06', 8.00);
INSERT INTO `prod_product` VALUES (1244476644709560322, 'Apple/苹果 iPhone 11 por Max', '', 9599.00, 10899.00, '1001', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/bdfa6121064b44e1a7200c60f85855a4.jpeg', NULL, NULL, 1, 1, 1000002, 26, 1224161374539587585, '通讯器材-品牌-iPhone', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, '2020-03-30', 0, 0, 0, '2020-03-30 12:08:48', NULL, 0.00);
INSERT INTO `prod_product` VALUES (1245714331831578626, '大黑弓弦钢丝弦', '', 1.00, 50.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c6cc4ac119674b1689f21478541f5fea.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238780770608164865, '射箭-弓附件-弓弦', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:06:55', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245714833168347138, '大黑专用滑轮', '', 1.00, 20.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/5d1c7b6107b548ada4e44c1680b0244c.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:08:55', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245715203265343490, '大黑后导轨', '', 1.00, 150.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/8c1621caee7245adaaf43c50328903ca.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238780770608164865, '射箭-弓附件-弓弦', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:10:23', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245715599773872129, '338剑玻纤剑', '', 1.00, 5.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/97db307d3fdc4b6dadfd7430f8169eb0.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:11:57', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245715861372612609, '大黑专用T包', '', 1.00, 100.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/243f316af28547deb0d3228ec229c459.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:13:00', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245716335995858946, '大黑机械瞄', '', 1.00, 50.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b8a8a208833e43199187818e384501ec.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:14:53', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245716829006934018, '大黑脚踏', '', 1.00, 40.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/837a672e9f7a4b129b5544dad64b0deb.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:16:50', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245717063002959874, '大黑专用扳机组', '', 1.00, 150.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/bd0a9d29a21c4c0f8c3fc5d37c630253.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238780770608164865, '射箭-弓附件-弓弦', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:17:46', NULL, 10.00);
INSERT INTO `prod_product` VALUES (1245718721195880449, '20英寸混碳剑', '', 1.00, 25.00, '', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b60f5e8cf2984b40952bcb36820a0862.jpg', NULL, NULL, 1245188173499166721, 1245188173499166721, 1184657969171046401, 1224183492346093569, 1238781082521776130, '射箭-弓附件-箭支', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-02 22:24:22', NULL, 0.00);
INSERT INTO `prod_product` VALUES (1246320044425498626, '时尚多色双扣臂带可调节可触屏', '', 19.90, NULL, 'HD20', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_6f4dbe6b029241ddb63c2d6a6d3987a5.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247430572111724546, '户外装备-户外装备', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-04 14:13:48', '2020-04-14 20:41:15', 0.00);
INSERT INTO `prod_product` VALUES (1246699920336789506, '显示屏腰包跑步健身登山骑行休闲可调节便携', '', 32.89, NULL, 'HD01', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_1f15b22e3ed045ae997f5bb2d08c3b68.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 15:23:18', '2020-04-14 17:40:50', 0.00);
INSERT INTO `prod_product` VALUES (1246709392534839298, '多色单口腰包跑步健身骑行徒步休闲可调节便携', '', 23.89, NULL, 'HD03', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/30397803079b4b8795f6a5d69ef853fa.png', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 16:00:56', '2020-04-07 17:41:38', 0.00);
INSERT INTO `prod_product` VALUES (1246721068034334721, '双口水壶腰包跑步健身运动骑行徒步可调节便携', '', 73.89, NULL, 'HD08', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_e844687cfa4f4341aadabc8058a717e5.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 16:47:20', '2020-04-14 20:59:40', 0.00);
INSERT INTO `prod_product` VALUES (1246755074020909057, '折叠双肩包旅游购物登山休闲集体出游便携耐用', '', 30.89, NULL, 'HD17', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_55348afa8536487fbfeaa9ee46a3b8fa.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 19:02:27', '2020-04-15 11:16:00', 0.00);
INSERT INTO `prod_product` VALUES (1246773267305242626, '超薄透气臂带跑步夜跑健身休闲可调节可触屏', '', 47.89, NULL, 'HD21', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/18b0e17204c9412f82142debb9f080e0.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247430572111724546, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 20:14:45', '2020-04-15 11:18:13', 0.00);
INSERT INTO `prod_product` VALUES (1246782990381391874, '3D遮光眼罩睡觉休息户外遮光', '', 17.89, NULL, 'HD28', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/2f8a807b539c4de581b49039c745dbcd.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247415403902042113, '户外装备-户外装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-05 20:53:23', '2020-04-14 22:10:42', NULL);
INSERT INTO `prod_product` VALUES (1247450704032727042, '贴心双口触屏腰包健身跑步徒步骑行休闲听音乐大容量便携', '', 45.89, NULL, 'HD02', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/84837e512a5c4e59a82c7f2bf7df080a.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 17:06:39', '2020-04-15 15:14:45', NULL);
INSERT INTO `prod_product` VALUES (1247469763650551809, '单水壶腰包跑步健身徒步休闲可调节便携', '', 58.89, 58.89, 'HD07', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_8fe4fdbc02a04aab8dfa6e9aa1bbc344.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 18:22:23', '2020-04-14 21:09:44', NULL);
INSERT INTO `prod_product` VALUES (1247482872557633538, '超薄双拉链腰包跑步登山徒步休闲听音乐', '', 48.89, NULL, 'HD09', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/20511aa5431542919d65061678d3d285.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 19:14:28', NULL, NULL);
INSERT INTO `prod_product` VALUES (1247495313601495041, '高级折叠包运动户外休闲登山集体出游', '', 107.89, NULL, 'HD18', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/4b22e9f86212480db64741df91413835.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 20:03:54', '2020-04-15 11:17:06', NULL);
INSERT INTO `prod_product` VALUES (1247499434949771266, '折叠登山包运动户外登山旅游休闲集体出游便携', '', 65.89, NULL, 'HD16', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b4e75db0f8724476963483e03d3eb1e0.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 20:20:17', '2020-04-07 20:24:49', NULL);
INSERT INTO `prod_product` VALUES (1247510158795243521, '时尚多色旋转臂带跑步健身骑行休闲听音乐可调节可触屏', '', 94.89, NULL, 'HD24', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/9cc4e46c2fd841f6b372451ce94d06b9.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247430572111724546, '户外装备-个人装备-分类名称15', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 21:02:54', '2020-04-10 17:11:37', NULL);
INSERT INTO `prod_product` VALUES (1247515912788934658, '多色速干巾运功擦汗旅游休闲冲凉便携', '', 26.89, NULL, 'HD27', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/ad0e46dacf01451aad27e46452c926e0.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1246479548966412289, '户外装备-个人装备-毛巾', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 21:25:45', '2020-04-07 21:33:37', NULL);
INSERT INTO `prod_product` VALUES (1247523784646098945, '夜光手机防水袋水上活动游泳潜水温泉水上乐园海极度防水', '', 21.89, NULL, 'HD31', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b677e06e8f5d48d792fdbb266086cb12.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247421559886217218, '户外装备-个人装备-防水袋', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 21:57:02', '2020-04-15 11:20:55', NULL);
INSERT INTO `prod_product` VALUES (1247527957911470081, '压纹防水袋水上活动游泳潜水水上乐园温泉海滩超强防水', '', 22.89, NULL, 'HD32', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_6236262da81a4158ae36ec4c8dc03fac.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247421559886217218, '户外装备-个人装备-防水袋', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 22:13:37', '2020-04-15 11:22:04', NULL);
INSERT INTO `prod_product` VALUES (1247532038650494977, '冰感酷爽户外冰袖防晒徒步骑行高尔夫钓鱼开车旅游休闲', '', 16.89, NULL, 'HD33', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/b89bf20635b34e29ab7a7fc7040c2c61.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247430583474094082, '户外装备-个人装备-分类名称16', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 22:29:50', '2020-04-10 16:36:48', NULL);
INSERT INTO `prod_product` VALUES (1247540815802040321, '户外可折叠水壶运动骑行跑步健身登山便携补水', '', 19.90, NULL, 'HD36', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/909c731b39064491a9b4f031e17cceec.jpg', NULL, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247430567380549633, '户外装备-个人装备-水壶', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-07 23:04:43', '2020-04-10 15:20:05', NULL);
INSERT INTO `prod_product` VALUES (1248563165179895809, '旅游速干巾户外擦汗擦水游泳冲凉住酒店便捷', '', 58.90, NULL, 'HD28', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/5dcd156bf9a54356aec6e6972b94a776.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1246479548966412289, '户外装备-个人装备-毛巾', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-10 18:47:10', '2020-04-15 11:25:27', NULL);
INSERT INTO `prod_product` VALUES (1248584375477428225, '时尚冷感毛巾健身跑步户外吸汗降温便携', '', 23.89, NULL, 'HD25', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/small_d43fbf473dce4f438844dcd9f839d6eb.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1246479548966412289, '户外装备-个人装备-毛巾', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-10 20:11:27', '2020-04-15 16:13:09', NULL);
INSERT INTO `prod_product` VALUES (1248592511605104642, ' 中号野餐垫沙滩垫公园垫户外防水防潮便携', '', 64.89, NULL, 'HD34', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/e77b123bc37e410f81fb973a288110ea.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247420658211520513, '户外装备-个人装备-野餐垫', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-10 20:43:47', '2020-04-10 20:44:50', NULL);
INSERT INTO `prod_product` VALUES (1248596406741983233, '大号野餐垫沙滩垫公园垫户外防水防潮便携', '', 69.89, NULL, 'HD35', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/2b4b7d24bbc04a8eb809e0d5f9106f28.png', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247420658211520513, '户外装备-个人装备-野餐垫', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 1, 0, '2020-04-10 20:59:15', '2020-04-15 20:52:26', NULL);
INSERT INTO `prod_product` VALUES (1250274215126327297, '超薄斜挎包跑步时尚运动风登山徒步旅游休闲便携', '', 68.89, NULL, 'HD12', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/c7ac73b967894b028898204cb35d1830.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 12:06:16', '2020-04-15 14:50:37', NULL);
INSERT INTO `prod_product` VALUES (1250282646012911617, '旅游洗漱包多彩好心情收纳化妆品洗漱用品毛巾', '', 44.89, NULL, 'HD19', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/146f06bec8ab48c7a34833764cbe5eb6.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 12:39:46', '2020-04-15 12:43:45', NULL);
INSERT INTO `prod_product` VALUES (1250298812274008066, '手按充气头枕办公旅行坐车坐船坐飞机户外便携', '', 66.89, NULL, 'HD29', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/ef469afacc044c01ace029eae6e6edf2.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247415403902042113, '户外装备-个人装备-寝具', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 13:44:00', NULL, NULL);
INSERT INTO `prod_product` VALUES (1250305765893169153, '户外充气垫户外野营徒步登山沙滩睡垫公园睡垫家庭使用便携', '', 269.90, NULL, 'HD30', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/9e3d5ef1f71044fb964216bfda74c861.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1247415403902042113, '户外装备-个人装备-寝具', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 14:11:38', NULL, NULL);
INSERT INTO `prod_product` VALUES (1250326827565539329, '便携可折叠腰包健身跑步登山旅游集体出游便携', '', 43.89, NULL, 'HD10', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/d0253d584fbd45f68e01dcc6958226d6.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 15:35:20', NULL, NULL);
INSERT INTO `prod_product` VALUES (1250335537251704834, '旅行者腰包健身跑步骑行登山旅游休闲集体出游', '', 73.89, NULL, 'HD11', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/2190ca16cadc48b2bed59aafeb7d0bf0.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1224185415631609857, 1239165508275613697, 1239165521391202306, '户外装备-个人装备-背包', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-15 16:09:56', NULL, NULL);
INSERT INTO `prod_product` VALUES (1253977938440679426, '苹果X iPhoneXR原装正品全网通4G手机iPhoneXS MAX三网双卡XR', '', 7100.00, 7100.00, '2652', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/e1c7443d5f6840a1ad50be32bbcb0f40.jpg', 0, NULL, 1251766594329022466, 1251766594329022466, 1224185415631609857, 1224344167416569857, 1224970302752006145, '户外装备-运动鞋靴-跑步鞋', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-25 17:23:32', '2020-04-25 18:38:44', 20.00);
INSERT INTO `prod_product` VALUES (1254068668387487745, 'Nikki智能运动手环蓝牙耳机二合一【亚马逊热销爆款】', '', 499.00, 699.00, 'M3', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/f441c65bc0b145f5818f4cde0226b7b9.jpg', 0, NULL, 1, 1, 1000002, 1224170587718393857, 1254057803600818178, '通讯器材-耳机-蓝牙耳机', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, '2020-04-25', 0, 0, 0, '2020-04-25 23:24:04', NULL, 0.00);
INSERT INTO `prod_product` VALUES (1254314725281296386, '智能蓝牙耳机手环时尚未来感带APP监测睡眠和运动身体指数拍照听歌接听电话', '', 489.90, 499.00, 'HD40', 'http://haveplayed.oss-cn-shenzhen.aliyuncs.com/84dc83458f98429c8e32713ffa813159.jpg', 0, NULL, 1245920330341310466, 1245920330341310466, 1000002, 1224170587718393857, 1239171083130580993, '通讯器材-耳机-运动手环', NULL, NULL, NULL, 0, 0.0, 0, 1, NULL, 1, NULL, 0, 0, 0, '2020-04-26 15:41:49', '2020-04-26 15:48:40', NULL);

INSERT INTO `base_menu` VALUES (300, 0, 'prod', '商品管理', '商品管理', '/', '', 'shopping', '_self', 'PageView', 1, 1, 1, 0, 'meida-base-provider', '2018-07-29 21:20:10', '2020-01-09 11:24:20');
INSERT INTO `base_menu` VALUES (301, 300, 'product', '店铺商品管理', '商品管理', '/', 'product/product/index', 'shopping', '_self', 'PageView', 1, 2, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-01-08 22:27:58');
INSERT INTO `base_menu` VALUES (302, 300, 'platformProduct', '自营商品管理', '', '/', 'product/platform/index', 'shopping', '_self', 'PageView', 1, 6, 1, 0, 'meida-base-provider', '2020-01-08 22:38:05', '2020-01-09 11:29:19');
INSERT INTO `base_menu` VALUES (303, 300, 'brand', '品牌管理', '品牌管理', '/', 'product/brand/index', 'crown', '_self', 'PageView', 1, 4, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-01-04 17:56:12');
INSERT INTO `base_menu` VALUES (304, 300, 'specs', '规格管理', '规格管理', '/', 'product/specs/index', 'bars', '_self', 'PageView', 1, 3, 1, 0, 'meida-base-provider', '2018-12-27 15:46:29', '2020-01-04 16:55:45');
INSERT INTO `base_menu` VALUES (305, 300, 'evaluate', '商品评价', '', '/', 'product/evaluate/index', 'wechat', '_self', 'PageView', 1, 5, 1, 0, 'meida-base-provider', '2020-01-02 17:39:01', '2020-01-04 16:56:04');
INSERT INTO `base_menu` VALUES (306, 300, 'aftersale', '售后保障', '', '/', 'product/aftersale/index', 'safety-certificate', '_self', 'PageView', 1, 7, 1, 0, 'meida-base-provider', '2020-01-09 09:56:36', '2020-01-09 13:17:28');



INSERT INTO `base_authority` VALUES (300, 'MENU_prod', 300, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-09 11:24:20');
INSERT INTO `base_authority` VALUES (301, 'MENU_product', 301, NULL, NULL, 1, '2019-07-30 15:43:15', '2020-01-08 22:27:59');
INSERT INTO `base_authority` VALUES (302, 'MENU_platformProduct', 302, NULL, NULL, 1, '2020-01-08 22:38:05', '2020-01-09 11:29:19');
INSERT INTO `base_authority` VALUES (303, 'MENU_brand', 303, NULL, NULL, 1, NULL, '2020-01-04 17:56:12');
INSERT INTO `base_authority` VALUES (304, 'MENU_specs', 304, NULL, NULL, 1, NULL, '2020-01-04 16:55:45');
INSERT INTO `base_authority` VALUES (305, 'MENU_evaluate', 305, NULL, NULL, 1, '2020-01-02 17:39:01', '2020-01-04 16:56:04');
INSERT INTO `base_authority` VALUES (306, 'MENU_aftersale', 306, NULL, NULL, 1, '2020-01-09 09:56:37', '2020-01-09 13:17:28');


SET FOREIGN_KEY_CHECKS = 1;
