/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 09:57:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for prod_specs
-- ----------------------------
DROP TABLE IF EXISTS `prod_specs`;
CREATE TABLE `prod_specs`  (
  `specsId` bigint(20) NOT NULL COMMENT '主键',
  `specsName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格名称',
  `specsOption` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格选项',
  `specsType` int(11) NULL DEFAULT NULL COMMENT '规格类型',
  `specsDesc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格描述',
  `sku` int(11) NULL DEFAULT NULL COMMENT '是否sku',
  `state` int(11) NULL DEFAULT NULL COMMENT '启用状态',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`specsId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '规格表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prod_specs
-- ----------------------------
INSERT INTO `prod_specs` VALUES (1186595545947713537, '颜色', '[{\"name\":\"酒红色\",\"value\":\"#c21401\"},{\"name\":\"橙色\",\"value\":\"#ffc12a\"}]', 1, NULL, 1, 1, '2019-10-22 18:49:58', '2019-10-23 14:23:22');

SET FOREIGN_KEY_CHECKS = 1;
