/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 09:58:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vip_price
-- ----------------------------
DROP TABLE IF EXISTS `vip_price`;
CREATE TABLE `vip_price`  (
  `vipPriceId` bigint(20) NOT NULL COMMENT '主键',
  `priceName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '价格名称',
  `vipTypeId` bigint(20) NULL DEFAULT NULL COMMENT '会员类型',
  `vipPrice` decimal(32, 2) NULL DEFAULT NULL COMMENT '安卓现价',
  `iosId` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IOS产品ID',
  `vipTime` int(11) NULL DEFAULT NULL COMMENT '持续时间（单位月）',
  `sortOrder` int(11) NULL DEFAULT NULL COMMENT '价格排序',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`vipPriceId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'VIP价格配置' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
