/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 09:58:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vip_user
-- ----------------------------
DROP TABLE IF EXISTS `vip_user`;
CREATE TABLE `vip_user`  (
  `vipId` bigint(20) NOT NULL COMMENT '主键',
  `vipTypeId` bigint(20) NULL DEFAULT NULL COMMENT '会员类型',
  `userId` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `version` int(11) NULL DEFAULT NULL,
  `companyId` bigint(20) NULL DEFAULT NULL COMMENT '企业ID',
  `expiryDate` date NULL DEFAULT NULL COMMENT '会员有效期',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`vipId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'vip用户表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
