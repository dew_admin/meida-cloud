/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100315
 Source Host           : localhost:3300
 Source Schema         : meida_cloud

 Target Server Type    : MariaDB
 Target Server Version : 100315
 File Encoding         : 65001

 Date: 03/06/2020 09:58:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vip_type
-- ----------------------------
DROP TABLE IF EXISTS `vip_type`;
CREATE TABLE `vip_type`  (
  `vipTypeId` bigint(20) NOT NULL COMMENT '主键',
  `vipName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员名称',
  `vipCode` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员编码',
  `duration` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '持续时间',
  `prePrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '现价',
  `oriPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `vipLogo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'vipLogo',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`vipTypeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'vip表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vip_type
-- ----------------------------
INSERT INTO `vip_type` VALUES (1, '月会员', 'Month', '1', NULL, 159.00, '', '2019-06-24 16:35:26', '2019-05-22 08:30:56');
INSERT INTO `vip_type` VALUES (2, '季会员', NULL, '3', NULL, 599.00, NULL, NULL, NULL);
INSERT INTO `vip_type` VALUES (3, '年会员', 'Year', '12', NULL, 899.00, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
