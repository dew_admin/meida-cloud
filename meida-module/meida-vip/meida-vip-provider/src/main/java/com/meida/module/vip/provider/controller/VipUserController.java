package com.meida.module.vip.provider.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.vip.client.entity.VipUser;
import com.meida.module.vip.provider.service.VipUserService;


/**
 * vip用户表控制器
 *
 * @author flyme
 * @date 2019-06-24
 */
@RestController
@RequestMapping("/vip/")
@Api(tags = "会员服务-Vip")
public class VipUserController extends BaseController<VipUserService, VipUser> {

}
