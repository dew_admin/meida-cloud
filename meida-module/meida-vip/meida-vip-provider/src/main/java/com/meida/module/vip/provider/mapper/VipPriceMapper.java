package com.meida.module.vip.provider.mapper;

import com.meida.module.vip.client.entity.VipPrice;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * VIP价格配置 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipPriceMapper extends SuperMapper<VipPrice> {

}
