package com.meida.module.vip.provider.mapper;

import com.meida.module.vip.client.entity.VipType;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * vip表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipTypeMapper extends SuperMapper<VipType> {

}
