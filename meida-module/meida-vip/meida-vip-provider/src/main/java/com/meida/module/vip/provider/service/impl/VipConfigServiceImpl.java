package com.meida.module.vip.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.vip.client.entity.VipConfig;
import com.meida.module.vip.provider.mapper.VipConfigMapper;
import com.meida.module.vip.provider.service.VipConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * vip配置表 实现类
 *
 * @author flyme
 * @date 2019-06-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class VipConfigServiceImpl extends BaseServiceImpl<VipConfigMapper, VipConfig> implements VipConfigService {

}
