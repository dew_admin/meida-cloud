package com.meida.module.vip.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.vip.client.entity.VipType;

/**
 * vip表 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipTypeService extends IBaseService<VipType> {


}
