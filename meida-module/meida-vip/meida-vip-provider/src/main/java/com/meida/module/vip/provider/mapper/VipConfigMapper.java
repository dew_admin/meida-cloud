package com.meida.module.vip.provider.mapper;

import com.meida.module.vip.client.entity.VipConfig;
import com.meida.common.mybatis.base.mapper.SuperMapper;


/**
 * vip配置表 Mapper 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipConfigMapper extends SuperMapper<VipConfig> {

}
