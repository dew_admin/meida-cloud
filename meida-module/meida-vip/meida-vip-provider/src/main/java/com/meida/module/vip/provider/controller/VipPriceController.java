package com.meida.module.vip.provider.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.vip.client.entity.VipPrice;
import com.meida.module.vip.provider.service.VipPriceService;


/**
 * VIP价格配置控制器
 *
 * @author flyme
 * @date 2019-06-24
 */
@RestController
@Api(tags = "会员服务-Vip")
@RequestMapping("/vip/")
public class VipPriceController extends BaseController<VipPriceService, VipPrice> {

}
