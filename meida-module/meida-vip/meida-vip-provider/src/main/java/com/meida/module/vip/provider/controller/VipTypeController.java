package com.meida.module.vip.provider.controller;


import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.vip.client.entity.VipType;
import com.meida.module.vip.provider.service.VipTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * vip表控制器
 *
 * @author flyme
 * @date 2019-06-24
 */
@RestController
@Api(tags = "公共服务")
public class VipTypeController extends BaseController<VipTypeService, VipType> {

    @ApiOperation(value = "会员类型", notes = "会员类型 ")
    @GetMapping(value = "/viptype/list")
    public ResultBody listEntityMap(@RequestParam(required = false) Map param) {
        return bizService.listEntityMap(param);
    }


}
