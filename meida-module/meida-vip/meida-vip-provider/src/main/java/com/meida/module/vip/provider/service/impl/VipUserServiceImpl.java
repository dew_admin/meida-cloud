package com.meida.module.vip.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.DateUtils;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.module.vip.client.entity.VipType;
import com.meida.module.vip.client.entity.VipUser;
import com.meida.module.vip.provider.mapper.VipUserMapper;
import com.meida.module.vip.provider.service.VipTypeService;
import com.meida.module.vip.provider.service.VipUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * vip用户表 实现类
 *
 * @author flyme
 * @date 2019-06-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class VipUserServiceImpl extends BaseServiceImpl<VipUserMapper, VipUser> implements VipUserService {
    @Autowired
    private VipTypeService vipTypeService;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public VipUser getByCompanyId(Long companyId) {
        CriteriaQuery cq = new CriteriaQuery(VipUser.class);
        cq.eq(true, "companyId", companyId);
        return getOne(cq);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public VipUser getByUserId(Long userId) {
        CriteriaQuery cq = new CriteriaQuery(VipUser.class);
        cq.eq(true, "userId", userId);
        return getOne(cq);
    }

    @Override
    public Boolean setCompanyVip(Long companyId, Long vipTypeId) {
        VipUser vipUser = getByCompanyId(companyId);
        VipType vipType = vipTypeService.getById(vipTypeId);
        Integer month = vipType.getDuration();
        if (FlymeUtils.isEmpty(vipUser)) {
            String expiryDate = DateUtils.format(DateUtils.plusMonths(month));
            vipUser = new VipUser();
            vipUser.setCompanyId(companyId);
            vipUser.setVipTypeId(vipTypeId);
            vipUser.setExpiryDate(DateUtils.parseDate(expiryDate));
            return save(vipUser);
        } else {
            String date = Optional.ofNullable(DateUtils.formatDate(vipUser.getExpiryDate())).orElse(DateUtils.formatDate());
            String expiryDate = DateUtils.plusMonth(DateUtils.parseLocalDate(date), month);
            vipUser.setExpiryDate(DateUtils.parseDate(expiryDate));
            return updateById(vipUser);
        }
    }

    @Override
    public Boolean setUserVip(Long userId, Long vipTypeId) {
        VipUser vipUser = getByUserId(userId);
        VipType vipType = vipTypeService.getById(vipTypeId);
        Integer month = vipType.getDuration();
        if (FlymeUtils.isEmpty(vipUser)) {
            String expiryDate = DateUtils.format(DateUtils.plusMonths(month));
            vipUser = new VipUser();
            vipUser.setUserId(userId);
            vipUser.setVipTypeId(vipTypeId);
            vipUser.setExpiryDate(DateUtils.parseDate(expiryDate));
            return save(vipUser);
        } else {
            String date = Optional.ofNullable(DateUtils.formatDate(vipUser.getExpiryDate())).orElse(DateUtils.formatDate());
            String expiryDate = DateUtils.plusMonth(DateUtils.parseLocalDate(date), month);
            vipUser.setExpiryDate(DateUtils.parseDate(expiryDate));
            return updateById(vipUser);
        }
    }

}
