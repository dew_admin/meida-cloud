package com.meida.module.vip.provider.service;

import com.meida.module.vip.client.entity.VipUser;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * vip用户表 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipUserService extends IBaseService<VipUser> {
    VipUser getByCompanyId(Long companyId);

    VipUser getByUserId(Long userId);

    /**
     * 设置企业会员
     *
     * @param companyId
     * @param vitTypeId
     * @return
     */
    Boolean setCompanyVip(Long companyId, Long vitTypeId);

    /**
     * 设置用户会员
     *
     * @param userId
     * @param vitTypeId
     * @return
     */
    Boolean setUserVip(Long userId, Long vitTypeId);
}
