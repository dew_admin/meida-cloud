package com.meida.module.vip.provider.service;

import com.meida.module.vip.client.entity.VipPrice;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * VIP价格配置 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipPriceService extends IBaseService<VipPrice> {

}
