package com.meida.module.vip.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.vip.client.entity.VipType;
import com.meida.module.vip.provider.mapper.VipTypeMapper;
import com.meida.module.vip.provider.service.VipTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * vip表 实现类
 *
 * @author flyme
 * @date 2019-06-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class VipTypeServiceImpl extends BaseServiceImpl<VipTypeMapper, VipType> implements VipTypeService {




    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<VipType> cq, VipType vipType, EntityMap requestMap) {
        cq.select("type.vipTypeId", "type.vipName", "type.prePrice", "type.oriPrice", "type.vipLogo");
        return super.beforeListEntityMap(cq, vipType, requestMap);
    }
}
