package com.meida.module.vip.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.vip.client.entity.VipConfig;

/**
 * vip配置表 接口
 *
 * @author flyme
 * @date 2019-06-24
 */
public interface VipConfigService extends IBaseService<VipConfig> {

}
