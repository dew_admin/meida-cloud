package com.meida.module.vip.provider.init;

import com.meida.common.dbinit.DbInitializer;
import com.meida.module.vip.client.entity.VipPrice;
import org.springframework.stereotype.Component;

/**
 * @author flyme
 * @date 2019/10/7 18:37
 */
@Component
public class VipPriceInitializer extends DbInitializer {

    @Override
    public Class<?> getEntityClass() {
        return VipPrice.class;
    }
}
