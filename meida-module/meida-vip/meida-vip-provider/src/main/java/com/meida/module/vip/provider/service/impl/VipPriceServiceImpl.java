package com.meida.module.vip.provider.service.impl;

import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.module.vip.client.entity.VipPrice;
import com.meida.module.vip.provider.mapper.VipPriceMapper;
import com.meida.module.vip.provider.service.VipPriceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * VIP价格配置 实现类
 *
 * @author flyme
 * @date 2019-06-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class VipPriceServiceImpl extends BaseServiceImpl<VipPriceMapper, VipPrice> implements VipPriceService {


}
