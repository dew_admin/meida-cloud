package com.meida.module.vip.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * vip配置表
 *
 * @author flyme
 * @date 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("vip_config")
@TableAlias("config")
@ApiModel(value = "VipConfig对象", description = "vip配置表")
public class VipConfig extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "configId", type = IdType.ASSIGN_ID)
    private Long configId;

    @ApiModelProperty(value = "会员ID")
    private Long vipTypeId;

    @ApiModelProperty(value = "特权标题")
    private String configTitle;

    @ApiModelProperty(value = "特权内容")
    private String configContent;

    @ApiModelProperty(value = "排序")
    private Integer sortOrder;

}
