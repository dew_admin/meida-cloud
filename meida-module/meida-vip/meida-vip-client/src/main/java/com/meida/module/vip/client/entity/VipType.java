package com.meida.module.vip.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * vip表
 *
 * @author flyme
 * @date 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("vip_type")
@TableAlias("type")
@ApiModel(value = "VipType对象", description = "vip表")
public class VipType extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "vipTypeId", type = IdType.ASSIGN_ID)
    private Long vipTypeId;

    @ApiModelProperty(value = "会员名称")
    private String vipName;

    @ApiModelProperty(value = "会员编码")
    private String vipCode;

    @ApiModelProperty(value = "持续时间")
    private Integer duration;

    @ApiModelProperty(value = "现价")
    private BigDecimal prePrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal oriPrice;

    @ApiModelProperty(value = "vipLogo")
    private String vipLogo;

}
