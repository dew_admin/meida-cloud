package com.meida.module.vip.client.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * VIP价格配置
 *
 * @author flyme
 * @date 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("vip_price")
@TableAlias("price")
@ApiModel(value = "VipPrice对象", description = "VIP价格配置")
public class VipPrice extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "vipPriceId", type = IdType.ASSIGN_ID)
    private Long vipPriceId;

    @ApiModelProperty(value = "价格名称")
    private String priceName;

    @ApiModelProperty(value = "会员类型")
    private Long vipTypeId;

    @ApiModelProperty(value = "安卓现价")
    private BigDecimal vipPrice;

    @ApiModelProperty(value = "IOS产品ID")
    private String iosId;

    @ApiModelProperty(value = "持续时间（单位月）")
    private Integer vipTime;

    @ApiModelProperty(value = "价格排序")
    private Integer sortOrder;

}
